using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebDAVClient.HttpClient
{
    public class HttpClientWrapper : IHttpClientWrapper
    {
        private readonly System.Net.Http.HttpClient _httpClient;
        private readonly System.Net.Http.HttpClient _uploadHttpClient;

        public HttpClientWrapper(System.Net.Http.HttpClient httpClient, System.Net.Http.HttpClient uploadHttpClient)
        {
            _httpClient = httpClient;
            _uploadHttpClient = uploadHttpClient;
        }
        int j = 0;
        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, HttpCompletionOption responseHeadersRead)
        {

            try
            {
                // ServicePointManager.DefaultConnectionLimit = 15;
                ///string path = "C:\\Users\\� � � � �\\source\\repos\\NewHH-SoftClientWPF\\NewHH-SoftClientWPF\\bin\\Debug\\error.txt";
                // string createText = responseHeadersRead+"   "+request.Headers +"   "+request.RequestUri+"  \n  " + j++;
                // File.WriteAllText(path, createText);

                
                return _httpClient.SendAsync(request, responseHeadersRead);
            }
            catch (HttpRequestException g )
            {

               /// string path = "C:\\Users\\� � � � �\\source\\repos\\NewHH-SoftClientWPF\\NewHH-SoftClientWPF\\bin\\Debug\\error.txt";
               // string createText = g.ToString();
               // File.WriteAllText(path, createText);

                return null;
            }
            catch (TimeoutException c)
            {

              //  string path = "C:\\Users\\� � � � �\\source\\repos\\NewHH-SoftClientWPF\\NewHH-SoftClientWPF\\bin\\Debug\\error.txt";
              //  string createText = c.ToString();
              //  File.WriteAllText(path, createText);

                return null;
            }
            catch (TaskCanceledException h)
            {
              //  string path = "C:\\Users\\� � � � �\\source\\repos\\NewHH-SoftClientWPF\\NewHH-SoftClientWPF\\bin\\Debug\\error.txt";
              //  string createText = h.ToString();
              //  File.WriteAllText(path, createText);
                return null;
            }


        }

        public Task<HttpResponseMessage> SendUploadAsync(HttpRequestMessage request)
        {
            return _uploadHttpClient.SendAsync(request);
        }
    }
}