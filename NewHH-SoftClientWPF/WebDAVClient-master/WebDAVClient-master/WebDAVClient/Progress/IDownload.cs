﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Progress.model;

namespace WebDAVClient.Progress
{
    public interface IDownload
    {
        event EventHandler<DownloadStateEventArgs> StateChanged;
        event EventHandler<DownloadStateEventArgs> Completed;
        event EventHandler<DownloadStateEventArgs> StateError;

        void setMre(ManualResetEvent mre);
        void setRow_id(long row_id);
        void ChangeState(long uploadBytes, long size, string state, bool isComplete);
        void ChangeError();
        bool getCancel();
        void setCancel(CancellationToken cancellation);
        
    }
}
