﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Progress.model;

namespace WebDAVClient.Progress
{
    public class Download : IDownload
    {
  
        public event EventHandler<DownloadStateEventArgs> StateChanged;
        public event EventHandler<DownloadStateEventArgs> Completed;
        public event EventHandler<DownloadStateEventArgs> StateError;
        public long row_id = 0;
        public ManualResetEvent mre;
        public CancellationToken cancellation;

        public void setRow_id(long row_id)
        {
            this.row_id = row_id;
        }

        public void setMre(ManualResetEvent mre)
        {
            this.mre = mre;
        }

        public void checkPause()
        {
            if(mre != null)
            { mre.WaitOne(); }
           
        }

        public bool getCancel()
        {
            if(cancellation == null)
            {
                
                return false;
            }
            else
            {
                
                return cancellation.IsCancellationRequested;
            }
            
        }

      

        public long getRow_id()
        {
            return row_id;
        }

        public Task DownloadAsync()
        {
            throw new NotImplementedException();
        }
     
        public void ChangeState(long uploadBytes , long size , string state , bool isComplete)
        {
            if(isComplete)
            {
                DownloadStateEventArgs args = new DownloadStateEventArgs();
                args.percent = 100;
                args.bytes = uploadBytes;
                args.row_id = getRow_id();
                args.isStop = getCancel();
                args.lenght = size;

                Completed?.Invoke(this, args);

            }
            else
            {
                checkPause();
                DownloadStateEventArgs args = new DownloadStateEventArgs();
                args.bytes = uploadBytes;
                args.percent = GetPercent(size, uploadBytes);
                args.row_id = getRow_id();
                args.isStop = getCancel();
                args.lenght = size;

                StateChanged?.Invoke(this, args);
            }
           
        }

        public void ChangeError()
        {
                DownloadStateEventArgs args = new DownloadStateEventArgs();
                args.percent = 0;
                args.row_id = getRow_id();
                args.isStop = getCancel();
                args.bytes = 0;
                args.lenght = 0;

            StateError?.Invoke(this, args);
        }

        public static long GetPercent(long b, long a)
        {
            if (b == 0) return 0;

            return (long)(a / (b / 100M));
        }

        public void setCancel(CancellationToken cancellation)
        {
            this.cancellation = cancellation;
        }
    }
}
