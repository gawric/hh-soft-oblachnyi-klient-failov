﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDAVClient.Progress.model
{
    public class DownloadStateEventArgs : EventArgs
    {
        public double percent { get; set; }
        public bool isComplete { get; set; }
        public long row_id { get; set; }
        public bool isStop {get;set;}
        public long bytes { get; set; }
        public long lenght { get; set; }

    }
}
