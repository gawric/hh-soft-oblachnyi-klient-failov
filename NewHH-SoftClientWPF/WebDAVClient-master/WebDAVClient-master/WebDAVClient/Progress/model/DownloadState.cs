﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDAVClient.Progress.model
{
    public static class DownloadState
    {
        public static  string Upload = "upload";
        public static  string Download = "download";
        public static  string PendingResponse = "pendingresponse";
        public static  string PendingUpload = "pendingupload";
        public static  string Uploading = "uploading";
    }
}
