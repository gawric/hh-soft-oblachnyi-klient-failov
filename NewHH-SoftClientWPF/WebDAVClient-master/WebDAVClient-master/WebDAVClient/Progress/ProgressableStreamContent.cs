﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Progress.model;

namespace WebDAVClient.Progress
{
    internal class ProgressableStreamContent : HttpContent
    {
        private const int defaultBufferSize = 4096;
        private Stream content;
        private int bufferSize;
        private bool contentConsumed;
        private IDownload downloader;

        public ProgressableStreamContent(Stream content, IDownload downloader) : this(content, defaultBufferSize, downloader) { }

        public ProgressableStreamContent(Stream content, int bufferSize, IDownload downloader)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }
            if (bufferSize <= 0)
            {
                throw new ArgumentOutOfRangeException("bufferSize");
            }

            this.content = content;
            this.downloader = downloader;
        }

       
     
        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
           
            Contract.Assert(stream != null);
            PrepareContent();

            return Task.Run(() => Go(stream));
        }

        private async Task Go(Stream stream)
        {
            try
            {

                var buffer = new Byte[4096];
                var size = content.Length;
                long uploaded = 0;


                downloader.ChangeState(0, size, DownloadState.PendingUpload, false);
           
                using (content) while (true)
                {

                        var length = content.Read(buffer, 0, buffer.Length);

                        if (length <= 0) break;


                        uploaded = uploaded + length;
                  
                        stream.Write(buffer, 0, length);

                       
                        //отмена операции
                        if (downloader.getCancel())
                        {
                            stream.Close();
                            stream.Dispose();
                            
                        }


                        downloader.ChangeState(uploaded, size, DownloadState.Uploading, false);
                }

                downloader.ChangeState(uploaded, size ,DownloadState.PendingResponse, true);
             

            }
            catch (System.Net.Http.HttpRequestException c)
            {
                Console.WriteLine("WebDavClient Error Upload->SerializeToStreamAsync  " + c);
                downloader.ChangeError();
            }
            catch (System.AggregateException z)
            {
                Console.WriteLine("WebDavClient Error Upload->SerializeToStreamAsync  " + z);
                downloader.ChangeError();
            }
            catch (System.NullReferenceException f)
            {
                Console.WriteLine("WebDavClient Error Upload->SerializeToStreamAsync  " + f);
                downloader.ChangeError();
            }
            catch (System.Net.WebException g)
            {
                Console.WriteLine("WebDavClient Error Upload->SerializeToStreamAsync  " + g);
                downloader.ChangeError();
            }
            catch (WebDAVClient.Helpers.WebDAVException g)
            {
                Console.WriteLine("WebDAVClient.Helpers.WebDAVException Error Upload->SerializeToStreamAsync  " + g);
                downloader.ChangeError();
            }
        }

        protected override bool TryComputeLength(out long length)
        {
            length = content.Length;
            return true;
        }

        private void PrepareContent()
        {
            if (contentConsumed)
            {
                // If the content needs to be written to a target stream a 2nd time, then the stream must support
                // seeking (e.g. a FileStream), otherwise the stream can't be copied a second time to a target 
                // stream (e.g. a NetworkStream).
                if (content.CanSeek)
                {
                    content.Position = 0;
                }
                else
                {
                    throw new InvalidOperationException("SR.net_http_content_stream_already_read");
                }
            }

            contentConsumed = true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                content.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
