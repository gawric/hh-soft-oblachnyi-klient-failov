﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDAVClient.Helpers
{
    public static class WriteLog
    {

        public static async Task Write(string text)
        {
            
            string folder = Environment.CurrentDirectory;
            string writePath = folder+"finalLog.txt";
            Console.WriteLine("Папка для записи логов  "+  writePath);
            try
            {
                //using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
               // {
               //     await sw.WriteLineAsync(text);
               // }

                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    await sw.WriteLineAsync("Дозапись");
                    await sw.WriteAsync("4,5");
                }
                Console.WriteLine("Запись выполнена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

