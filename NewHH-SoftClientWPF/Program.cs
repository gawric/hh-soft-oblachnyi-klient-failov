﻿
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.json;

using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationClickButton.doubleclick;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.delete;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.paste;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.sync;
using NewHH_SoftClientWPF.contoller.treeview;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.subscription;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.support.observable.viewfoldertransfer;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.onlyCloud;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.renameJsonModel;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.supportForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm;
using NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var container = Bootstrap();

            // Any additional other configuration, e.g. of your desired MVVM toolkit.

            RunApplication(container);
        }

       
        private static Container Bootstrap()
        {
            

            // Create the container as usual.
            var container = new Container();

            // Register your types, for instance:
            // container.Register<IQueryProcessor, QueryProcessor>(Lifestyle.Singleton);
            // container.Register<IUserContext, WpfUserContext>();
            
            container.Register<StatusSqlliteTransaction>(Lifestyle.Singleton);
          
            container.Register<ActiveMqSubscribleModel>(Lifestyle.Singleton);
            container.Register<OverwriteWindowModel>(Lifestyle.Singleton);
            container.Register<FileInfoModelSupport>(Lifestyle.Singleton);
            container.Register<LoginFormViewModel>(Lifestyle.Singleton);
            container.Register<MainWindowViewModel>(Lifestyle.Singleton);
            container.Register<MainWindowContextMenu>(Lifestyle.Singleton);
            container.Register<ViewFolderContextMenu>(Lifestyle.Singleton);
            container.Register<CopyMainWindowViewModel>(Lifestyle.Singleton);
            container.Register<NavigationMainWindowsController>(Lifestyle.Singleton);
            container.Register<ViewFolderViewModel>(Lifestyle.Singleton);
            container.Register<ViewTransferViewModel>(Lifestyle.Singleton);
            container.Register<ViewTransfer>(Lifestyle.Singleton);
            container.Register<ViewFolder>(Lifestyle.Singleton);
            container.Register<FirstSyncFilesController>(Lifestyle.Singleton);
            container.Register<ObserverLoginFormUpdate>(Lifestyle.Singleton);
            container.Register<ObserverMainWindowFormUpdate>(Lifestyle.Singleton);
            container.Register<ObserverUpdateTreeView>(Lifestyle.Singleton);
            container.Register<CreateSubscriptionMainForm>(Lifestyle.Singleton);
            container.Register<CreateSubscriptionLoginForm>(Lifestyle.Singleton);
            container.Register<CreateSubscriptionTreeView>(Lifestyle.Singleton);
            container.Register<CreateSubscriptionViewTransfer>(Lifestyle.Singleton);
           
            container.Register<CopyViewModel>(Lifestyle.Singleton);
            container.Register<SupportPasteControllerAllFunction>(Lifestyle.Singleton);

            container.Register<ObserverViewTransferUpdate>(Lifestyle.Singleton);
            container.Register<ObserverUpdateTree>(Lifestyle.Singleton);
            container.Register<ObserverLoginUpdate>(Lifestyle.Singleton);
            container.Register<ObserverMainUpdate>(Lifestyle.Singleton);
            container.Register<UpdateViewTransferController>(Lifestyle.Singleton);
            container.Register<UpdateFormLoginController>(Lifestyle.Singleton);
            container.Register<UpdateFormMainController>(Lifestyle.Singleton);


 
            container.Register<ExceptionController>(Lifestyle.Singleton);
            container.Register<SqliteController>(Lifestyle.Singleton);
            container.Register<TransportStatusMqClient>(Lifestyle.Singleton);
            container.Register<ScannerConnectServerController>(Lifestyle.Singleton);
            
            container.Register<MqClientController>(Lifestyle.Singleton);

            container.Register<ObserverNetwokUpdate>(Lifestyle.Singleton);
            container.Register<ObserverNetwokSendJson>(Lifestyle.Singleton);
            container.Register<UpdateNetworkController>(Lifestyle.Singleton);
            container.Register<UpdateNetworkJsonController>(Lifestyle.Singleton);


            container.Register<WebDavClientController>(Lifestyle.Singleton);
            container.Register<WebDavClientExist>(Lifestyle.Singleton);
            
            container.Register<DeleteController>(Lifestyle.Singleton);
            container.Register<PasteFilesController>(Lifestyle.Singleton);
            container.Register<PasteFolderController>(Lifestyle.Singleton);
            container.Register<CreateFolderController>(Lifestyle.Singleton);
            container.Register<PasteWindowsController>(Lifestyle.Singleton);
            //Статус копирования файлов
            container.Register<CopyStatusController>(Lifestyle.Singleton);

            container.Register<SupportEventPart1MainForm>(Lifestyle.Singleton);
            container.Register<SupportEventPart2MainForm>(Lifestyle.Singleton);
            container.Register<SupportEventPart1ViewForm>(Lifestyle.Singleton);
            container.Register<SupportEventPart2ViewForm>(Lifestyle.Singleton);
            container.Register<SupportEventPart1ViewTransfer>(Lifestyle.Singleton);
            container.Register<MainWindow>(Lifestyle.Singleton);
            container.Register<CopyMainWindow>(Lifestyle.Singleton);
            container.Register<OverwriteWindow>();
            //container.Register<LoginForm>(Lifestyle.Singleton);

            container.Register<UpdateFormCopyWindowsController>(Lifestyle.Singleton);
            container.Register<ObserverCopyWindowUpdate>(Lifestyle.Singleton);
            container.Register<ObserverCopyFormUpdate>(Lifestyle.Singleton);

            //срабатывает когда подписка получает новые данные
            //обновляет базу sqlite
            container.Register<UpdateDataController>(Lifestyle.Singleton);

            container.Register<RenameFolderController>(Lifestyle.Singleton);

            //operationClickButton - реагируем на нажатие кнопок меню
            container.Register<DoubleClick>(Lifestyle.Singleton);
            container.Register<ButtonUploadClick>(Lifestyle.Singleton);
            container.Register<CancellationController>(Lifestyle.Singleton);

            //для Блокировки бесконечных потоков(Пример: поток который срабатывает каждые n минут для пересканирования файлов и activemq)
            container.Register<BlockingEventController>(Lifestyle.Singleton);
            container.Register<ReadJsonToDiskController>(Lifestyle.Singleton);
            container.Register<SaveJsonToDiskContoller>(Lifestyle.Singleton);
            container.Register<ConvertIconType>(Lifestyle.Singleton);

            //Обновление данных без удаления в равные промежутки времени
            container.Register<AutoSyncController>(Lifestyle.Singleton);
            container.Register<StatusAllOperationModel>(Lifestyle.Singleton);
            container.Register<ViewTransferContextMenu>(Lifestyle.Singleton);
            container.Register<StackOperationUpDown>(Lifestyle.Singleton);
            container.Register<ErrorAlertWindowViewModel>(Lifestyle.Singleton);


            container.Verify();

            return container;
        }

        private static void RunApplication(Container container)
        {
            try
            {
                var app = new App();
              
                var mainWindow = container.GetInstance<MainWindow>();
                app.Run(mainWindow);
            }
            catch (Exception)
            {
            
            }
        }
    }
}
