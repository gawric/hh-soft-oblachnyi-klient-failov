﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;

namespace UnitTestNewHH_SoftClientWpf
{
    [TestClass]
    public static class InitalizingUnitTest
    {
        public static Container container;

        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            container = Bootstrap();
        }

        private static Container Bootstrap()
        {
            // Create the container as usual.
            var container = new Container();
            container.Register<SupportPasteControllerAllFunction>(Lifestyle.Singleton);
            container.Register<CopyMainWindowViewModel>(Lifestyle.Singleton);
            container.Register<ObserverCopyFormUpdate>(Lifestyle.Singleton);
            container.Register<CreateSubscriptionLoginForm>(Lifestyle.Singleton);
            container.Register<FileInfoModelSupport>(Lifestyle.Singleton);
         //   container.Register<ErrorController>(Lifestyle.Singleton);
            container.Register<UpdateFormMainController>(Lifestyle.Singleton);
            container.Register<StatusSqlliteTransaction>(Lifestyle.Singleton);
            container.Register<UpdateViewTransferController>(Lifestyle.Singleton);
            container.Register<ObserverNetwokSendJson>(Lifestyle.Singleton);
            container.Register<SqliteController>(Lifestyle.Singleton);
            container.Register<FirstSyncFilesController>(Lifestyle.Singleton);
            container.Register<UpdateNetworkJsonController>(Lifestyle.Singleton);
            container.Register<UpdateFormCopyWindowsController>(Lifestyle.Singleton);
            container.Register<TransportStatusMqClient>(Lifestyle.Singleton);
            container.Register<ScannerConnectServerController>(Lifestyle.Singleton);
            container.Register<WebDavClientController>(Lifestyle.Singleton);
            container.Register<MqClientController>(Lifestyle.Singleton);


            container.Register<CopyStatusController>(Lifestyle.Singleton);
            container.Register<OverwriteWindowModel>(Lifestyle.Singleton);
            container.Register<OverwriteWindow>(Lifestyle.Singleton);
            container.Register<MainWindowViewModel>(Lifestyle.Singleton);
            container.Verify();

            return container;
        }

    }
}
