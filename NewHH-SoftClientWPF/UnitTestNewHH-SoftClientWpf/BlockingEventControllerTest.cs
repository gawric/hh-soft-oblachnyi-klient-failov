﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.blockThread.manualReset;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestNewHH_SoftClientWpf
{
    [TestClass]
    public class BlockingEventControllerTest
    {
        [TestMethod]
        private void Test()
        {
            BlockingEventController blockingTest = new BlockingEventController(getContainer());
            blockingTest.createManualResetEvent("Test");
            ManualReset mr =  blockingTest.getManualReset("Test");

            bool chec = false;
            if (mr != null) chec = true;

            Assert.AreEqual(true, chec);
        }
        private Container getContainer()
        {
            
            return InitalizingUnitTest.container;
        }
    }

    
}
