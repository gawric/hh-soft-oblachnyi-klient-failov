﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm
{
    public class ViewTransferContextMenu : INotifyPropertyChanged
    {
        
        string headerPause { get; set; }
       
        BitmapSource headerPauseIcon { get; set; }

        string headerStop { get; set; }
        BitmapSource headerStopIcon { get; set; }

        string headerClearAll { get; set; }
        BitmapSource headerClearAllIcon { get; set; }

        string headerClearSingle { get; set; }
        BitmapSource headerClearSingleIcon { get; set; }



        public string HeaderPause
        {
            get { return headerPause; }
            set
            {
                if (headerPause != value)
                {
                    headerPause = value;
                    OnPropertyChange("HeaderPause");

                }
            }
        }

        public BitmapSource HeaderPauseIcon
        {
            get { return headerPauseIcon; }
            set
            {
                if (headerPauseIcon != value)
                {
                    headerPauseIcon = value;
                    OnPropertyChange("HeaderPauseIcon");

                }
            }
        }


        public string HeaderStop
        {
            get { return headerStop; }
            set
            {
                if (headerStop != value)
                {
                    headerStop = value;
                    OnPropertyChange("headerStop");

                }
            }
        }

        public BitmapSource HeaderStopIcon
        {
            get { return headerStopIcon; }
            set
            {
                if (headerStopIcon != value)
                {
                    headerStopIcon = value;
                    OnPropertyChange("HeaderStopIcon");

                }
            }
        }


        public string HeaderClearAll
        {
            get { return headerClearAll; }
            set
            {
                if (headerClearAll != value)
                {
                    headerClearAll = value;
                    OnPropertyChange("HeaderClearAll");

                }
            }
        }

        public BitmapSource HeaderClearAllIcon
        {
            get { return headerClearAllIcon; }
            set
            {
                if (headerClearAllIcon != value)
                {
                    headerClearAllIcon = value;
                    OnPropertyChange("HeaderClearAllIcon");

                }
            }
        }

        public string HeaderClearSingle
        {
            get { return headerClearSingle; }
            set
            {
                if (headerClearSingle != value)
                {
                    headerClearSingle = value;
                    OnPropertyChange("HeaderClearSingle");

                }
            }
        }

        public BitmapSource HeaderClearSingleIcon
        {
            get { return headerClearSingleIcon; }
            set
            {
                if (headerClearSingleIcon != value)
                {
                    headerClearSingleIcon = value;
                    OnPropertyChange("HeaderClearSingleIcon");

                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
