﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm
{
    public class ErrorAlertWindowViewModel : INotifyPropertyChanged
    {
        private string textBlock;
        private string errorText;
        private string nameWindow;
        public event PropertyChangedEventHandler PropertyChanged;
        BitmapSource imageAttention { get; set; }
       


        public BitmapSource ImageAttention
        {
            get { return imageAttention; }
            set
            {
                if (imageAttention != value)
                {
                    imageAttention = value;
                    OnPropertyChange("ImageAttention");

                }
            }
        }


        public string TextBlock
        {
            get { return textBlock; }
            set
            {
                if (textBlock != value)
                {
                    textBlock = value;
                    OnPropertyChange("TextBlock");

                }
            }
        }

        

        public string NameWindow
        {
            get { return nameWindow; }
            set
            {
                if (nameWindow != value)
                {
                    nameWindow = value;
                    OnPropertyChange("NameWindow");

                }
            }
        }

        public string ErrorText
        {
            get { return errorText; }
            set
            {
                if (errorText != value)
                {
                    errorText = value;
                    OnPropertyChange("ErrorText");

                }
            }
        }


        protected void OnPropertyChange(string propertyName)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            catch(System.ArgumentException z)
            {
                Console.WriteLine("ErrorAlertWindowsViewModel-> OnPropertyChange: Критическая ошибка !!!! " +z.ToString());
            }
          
        }
    }
}
