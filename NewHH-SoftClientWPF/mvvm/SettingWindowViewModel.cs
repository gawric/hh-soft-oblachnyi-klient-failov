﻿using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm
{
    public class SettingWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string pathDisk;
        private string timeAutoScan;
        private string username;
        private string webDavServer;
        private string activeMqServer;
        public ObservableCollection<ScanFolderModel> obsScanFol { get; set; }

        private bool isEnbledButtonOk;
        private bool isEnbledButtonCancel;
        private bool isEnbledButtonCommit;
        private bool isCheckBoxAutoStartChecked;
        private bool isCheckBoxScanFolderChecked;

        //Дополнительно Статистика
        private string timeSpent;
        private string lastDataScan;
        private string fileAndFolder;
        private string allCount;


        public bool IsCheckBoxAutoStartChecked
        {

            get { return isCheckBoxAutoStartChecked; }
            set
            {
                if (isCheckBoxAutoStartChecked != value)
                {
                    isCheckBoxAutoStartChecked = value;
                    OnPropertyChange("IsCheckBoxAutoStartChecked");

                }
            }
        }

        public bool IsCheckBoxScanFolderChecked
        {

            get { return isCheckBoxScanFolderChecked; }
            set
            {
                if (isCheckBoxScanFolderChecked != value)
                {
                    isCheckBoxScanFolderChecked = value;
                    OnPropertyChange("IsCheckBoxScanFolderChecked");

                }
            }
        }

        public bool IsEnbledButtonOk
        {

            get { return isEnbledButtonOk; }
            set
            {
                if (isEnbledButtonOk != value)
                {
                    isEnbledButtonOk = value;
                    OnPropertyChange("IsEnbledButtonOk");

                }
            }
        }
        public bool IsEnbledButtonCancel
        {

            get { return isEnbledButtonCancel; }
            set
            {
                if (isEnbledButtonCancel != value)
                {
                    isEnbledButtonCancel = value;
                    OnPropertyChange("IsEnbledButtonCancel");

                }
            }
        }
        public bool IsEnbledButtonCommit
        {

            get { return isEnbledButtonCommit; }
            set
            {
                if (isEnbledButtonCommit != value)
                {
                    isEnbledButtonCommit = value;
                    OnPropertyChange("IsEnbledButtonCommit");

                }
            }
        }

        public string TimeSpent
        {

            get { return timeSpent; }
            set
            {
                if (timeSpent != value)
                {
                    timeSpent = value;
                    OnPropertyChange("TimeSpent");

                }
            }
        }

        public string LastDataScan
        {

            get { return lastDataScan; }
            set
            {
                if (lastDataScan != value)
                {
                    lastDataScan = value;
                    OnPropertyChange("LastDataScan");

                }
            }
        }

        public string FileAndFolder
        {

            get { return fileAndFolder; }
            set
            {
                if (fileAndFolder != value)
                {
                    fileAndFolder = value;
                    OnPropertyChange("FileAndFolder");

                }
            }
        }

        public string AllCount
        {

            get { return allCount; }
            set
            {
                if (allCount != value)
                {
                    allCount = value;
                    OnPropertyChange("AllCount");

                }
            }
        }

        public string WebDavServer
        {

            get { return webDavServer; }
            set
            {
                if (webDavServer != value)
                {
                    webDavServer = value;
                    OnPropertyChange("WebDavServer");

                }
            }
        }

        public string ActiveMqServer
        {

            get { return activeMqServer; }
            set
            {
                if (activeMqServer != value)
                {
                    activeMqServer = value;
                    OnPropertyChange("ActiveMqServer");

                }
            }
        }

        public string UserName
        {

            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    OnPropertyChange("UserName");

                }
            }
        }

        public string TimeAutoScan
        {
            
            get { return timeAutoScan; }
            set
            {
                if (timeAutoScan != value)
                {
                    timeAutoScan = value;
                    OnPropertyChange("TimeAutoScan");

                }
            }
        }

        public string PathDisk
        {

            get { return pathDisk; }
            set
            {
                if (pathDisk != value)
                {
                    pathDisk = value;
                    OnPropertyChange("PathDisk");

                }
            }
        }




        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
