﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.autoSaveFolder
{
    public class HrefModel
    {
        public string href { get; set; }
        public string filename { get; set; }
        public string pasteHref { get; set; }

        public string rootHref { get; set; }
        public string lastOpenDate { get; set; }
        public string createDate { get; set; }
        public string changeDate { get; set; }

        public long size { get; set; }

        public string parentHref { get; set; }

        public string type { get; set; }
    }
}
