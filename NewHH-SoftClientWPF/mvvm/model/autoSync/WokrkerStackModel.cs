﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.mvvm.model.autoSync
{
    public class WokrkerStackModel
    {
        public Item modelWebDav { get; set; }
        public Dictionary<long, long> sqlArray { get; set; }
        public Dictionary<string, long> sqlArrayHref { get; set; }
        public Dictionary<string, long> parentArray { get; set; }
        public string parentHref { get; set; }
        public FileInfoModel[] containerAllFiles { get; set; }
    }
}
