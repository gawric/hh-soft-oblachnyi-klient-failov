﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.scanner.autoscan;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.autoSync
{
    public class AutoSyncToTimeObj
    {
        public CancellationController _cancellationController { get; set; }
        public UpdateNetworkJsonController _updateNetworkJsonController { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public WebDavClientController _webDavClient { get; set; }
        public SupportServerTopicModel _supportTopicModel { get; set; }
        public BlockingEventController _blockingEvent { get; set; }
        public ScannerWebDavAndSendToServer _scanner { get; set; }
        public MainWindowViewModel _viewModel { get; set; }
        public StatusSqlliteTransaction statusTransactionSave { get; set; }
        public bool isStop { get; set; }
        //для считывания с диска есть файлы или нет
        //так мы пытаемся определить что-то пишется в данный момент в базу или нет!
        public SaveJsonToDiskContoller saveJsonToDiskContoller { get; set; }

        public string[] usernameAndPassword { get; set; }
        public long version { get; set; }
        public Dictionary<long, string> doubleHref { get; set; }
        public StatusAllOperationModel _allStatus {get;set;}
        public ExceptionController _exceptionController { get; set; }

    }
}
