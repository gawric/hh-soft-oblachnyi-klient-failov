﻿
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList
{
    public class SyncSinglListModel
    {
        public MainWindowViewModel _viewModelMain { get; set; }
        public UpdateDataController _updateDataController { get; set; }
        public UpdateFormMainController _updateMainForm { get; set; }
        public ServerTopicModel responceServer { get; set; }
        public ObserverMainWindowFormUpdate _mainUpdateForm { get; set; }
        public CopyStatusController _copyStatus { get; set; }
        public SqliteController _sqlliteController { get; set; }
        public StackUpdateTreeViewIcon _stackUpdateTreeViewIcon {get;set;}
        public SortableObservableCollection<FolderNodes> rootNodesListView { get; set; }
        public ConvertIconType _convertIcon { get; set; }
    }
}
