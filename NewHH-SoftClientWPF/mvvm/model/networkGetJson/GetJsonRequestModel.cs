﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.sync;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.networkGetJson
{
    public class GetJsonRequestModel
    {
        public ObserverMainWindowFormUpdate mainUpdateForm { get; set; }
        public UpdateDataController updateDataController { get; set; }
        public MainWindowViewModel viewModelMain { get; set; }
        public UpdateFormMainController updateMainForm { get; set; }

        public SqliteController sqlliteController { get; set; }

        public CopyStatusController copyStatus { get; set; }

        public ViewFolderViewModel viewModelViewFolder { get; set; }

        public ConvertIconType convertIcon { get; set; }

        public UpdateNetworkJsonController updateSendController { get; set; }

        public AutoScanInfoModel autoScanInfoModel { get; set; }
        public SaveJsonToDiskContoller saveJsonToDisk { get; set; }
        public ReadJsonToDiskController readJsonToDisk { get; set; }
        public AutoSyncController autoSync { get; set; }
        public FirstSyncFilesController firstSyncController { get; set; }
        public BlockingEventController blockingEvent {get;set;}
        public WebDavClientController webDavClient { get; set; }

        public CancellationController cancellationController { get; set; }
    }
}
