﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public  class ArgsDataSetIcon : EventArgs
    {
            private Icon _newIcon;
            public ArgsDataSetIcon(Icon newIcon)
            {
                this._newIcon = newIcon;
            }

          
            public Icon getIcon
            {
                get { return _newIcon; }
            }

           
    }
}
