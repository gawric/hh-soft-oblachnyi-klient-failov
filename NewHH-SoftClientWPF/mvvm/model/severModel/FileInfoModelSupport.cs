﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public class FileInfoModelSupport
    {
        private int countIteration = 0;

        public FileInfoModel createFileInfoModel(long Row_id, string Filename, string CreateDate, string ChangeDate, string lastOpenDate, string Attribute, string Location, long Parent, string Type, long sizebytes, long versionupdatebases, long versionupdaterows, long User_id, string ChangeRows , int saveTopc)
        {
            FileInfoModel model = new FileInfoModel();
            try
            {
                //нежелательно начинать rowid c 0
                model.row_id = Row_id;
                model.filename = Filename;
                model.createDate = CreateDate;
                model.changeDate = ChangeDate;
                model.lastOpenDate = lastOpenDate;
                model.attribute = Attribute;
                model.location = Location;
                model.parent = Parent;
                model.type = Type;
                model.sizebyte = sizebytes;
                model.versionUpdateBases = versionupdatebases;
                model.versionUpdateRows = versionupdaterows;
                model.user_id = staticVariable.Variable.getUser_id();
                model.changeRows = ChangeRows;
                model.saveTopc = saveTopc;
            }
            catch (System.InvalidOperationException j)
            {
                Console.WriteLine(j);
            }
           
            return model;
        }


        public string ConvertDisplayNameToType(string displayName, int checkFolderType)
        {
            if (checkFolderType == -1)
            {
                return "folder";
            }
            else
            {
                try
                {
                    //  int lenght = displayName.Length;
                    return displayName.Substring(checkFolderType+1);
                }
                catch (System.ArgumentOutOfRangeException g)
                {
                    Console.WriteLine(g);
                    return "txt";
                }

            }
        }
        //modelServer текущая модель от сервера
        //containerAllFiles - конечный архив хранит все модели FileInfoModel для передачи на сервера
        //parentArray - Хранит все parentName - Name.Href - что-бы выдать им id номера для заполнения по индексу массива
        //networkJson - observer для отправки серверу сообщений
        //progress - ход выполнения 
        public void convertModelToFileInfoModel(Dictionary<string, int> parentArray, FileInfoModel[] containerAllFiles, UpdateNetworkJsonController networkJson , Item modelServer, FileInfoModelSupport modelSupport , long newVersionBases , int[] CountArray , string username , IProgress<double> progress, SupportServerTopicModel supportTopicModel)
        {
            int s2;


            //путь у папок заканчивается "/" его нужно убрать т.к файлы заканичиваются расширением
            if (staticVariable.Variable.isFolder(modelServer.ContentType))
            {
                //находим последнюю / 
                int s = modelServer.Href.LastIndexOf("/") - 1;
                //убираем ее
                string newparent = modelServer.Href.Substring(0, s);

                //находим следущую
                s2 = newparent.LastIndexOf("/") + 1;
            }
            else
            {
                s2 = modelServer.Href.LastIndexOf("/") + 1;
            }

            string parentHref = modelServer.Href.Substring(0, s2);

            //Добавляет папки и файлы в массив. Что-бы по индексу массива выдать им row_id 
            //дальше добавить их в поле FileInfoFolder Row_id и передать в базу данных для заполнения
            addFilesTempCollection(parentArray, parentHref, modelServer.Href);



            try
            {

                //если папка не определенно кол-вор байтов
                if (modelServer.ContentLength == null)
                {
                    modelServer.ContentLength = 0;
                    
                }

                //Console.WriteLine(newVersionBases);
                FileInfoModel modelFileInfo = modelSupport.createFileInfoModel(parentArray[modelServer.Href], modelServer.DisplayName, modelServer.CreationDate.ToString(), modelServer.LastModified.ToString(), modelServer.LastModified.ToString(), modelServer.ContentType, modelServer.Href, parentArray[parentHref], modelServer.ContentType, (long)modelServer.ContentLength, newVersionBases, 0, 0, "OK", 0);

                try
                {

                    if (CountArray[0] == staticVariable.Variable.CountFilesTrasferServer)
                    {

                        CountArray[0] = 0;
                        networkJson.updateSendJsonPARTIES(containerAllFiles , newVersionBases);
                        
                        progress.Report(0);
                        

                        for (int k = 0; k < containerAllFiles.Length; k++)
                        {
                            containerAllFiles[k] = null;
                        }

                    }

                    double procent = GetPercent(staticVariable.Variable.CountFilesTrasferServer, CountArray[0]);
                 
                    progress.Report(procent);
                    containerAllFiles[CountArray[0]] = modelFileInfo;
                }
                catch(System.IndexOutOfRangeException sr)
                {
                    Console.WriteLine(sr);
                }


               


                CountArray[0]++;




            }
            catch (System.InvalidOperationException k)
            {
                Console.WriteLine(k);
            }

        }


        public double GetPercent(long b, long a)
        {
            if (b == 0) return 0;

            return (double)(a / (b / 100M));
        }



        private void addFilesTempCollection(Dictionary<string, int> parentArray, string parentHref, string modelServerHref)
        {
            //Добавляем парент в базе данных т.к парент это тоже какой-то нод
            if (parentArray.ContainsKey(parentHref))
            {

            }
            else
            {
                parentArray.Add(parentHref, parentArray.Count - 1);

            }
            //добавляем файл в базу данных если он был раньше добавлен, как парент мы его не перезаписываем
            if (parentArray.ContainsKey(modelServerHref))
            {

            }
            else
            {
                parentArray.Add(modelServerHref, parentArray.Count - 1);

            }


        }

        public FileInfoModel getRootFileInfoModel(long newVersionBase)
        {
            return  createFileInfoModel(-1, staticVariable.Variable.getRootDiskTree(), DateTime.Now.ToString(), DateTime.Now.ToString(), DateTime.Now.ToString(), "folder", "disk", -2, "folder", 0, newVersionBase, 0, 0, "OK", 0);
        }


    }
}
