﻿using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm.model.infoSystemModel;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.mqclient.support
{
    public class SupportServerTopicModel
    {
        //0 - версия базы
        //1 - запрос (перечитать базу или получить клиента)
        //2 - размер массива
        //3 - id номер если сервер присвоил
        //4  - номер партии используется когда обновляем все данные базы, что-бы передавать сообщения пачками(струтура BEGINPARTIES|PARTIES|ENDPARTIES)
        public ServerTopicModel CreateModelTopic(string request , string[] usernameAndPassword , long versionDatabases , long versionRows)
        {
            ServerTopicModel ServerModel = new ServerTopicModel();
            //versionRows - вычисляет не правильно т.к содержится ввиде строки, что мешает вычислению sqllite пока я буду отправлять только такие данные
            //если в будущем понадобится придется versionRows перевести в формат int
            ServerModel.connectionId = staticVariable.Variable.activemqConnectionId.Keys.ToArray();
            InfoSystemModel infoModel = getInfoSystem();
            ServerModel.username = usernameAndPassword[0];
            
            //0 - версия базы передаем
            string[] message = { versionDatabases.ToString(), request , infoModel.ipLocal, infoModel.ipNetwork, infoModel.macNetwork , infoModel.osUserName , infoModel.osVerison , versionRows.ToString()};
            ServerModel.textmessage = message;
            ServerModel.listFileInfo = null;
          

            return ServerModel;
        }

        private InfoSystemModel getInfoSystem()
        {
            InfoSystem infoSystem = new InfoSystem();
            return infoSystem.getInfoSystem();
        }
        public ServerTopicModel CreateModelTopicParties(string request, long newversionbases , string username, string parties)
        {
            ServerTopicModel ServerModel = new ServerTopicModel();
            ServerModel.username = username;
            ServerModel.connectionId = staticVariable.Variable.activemqConnectionId.Keys.ToArray();
            //0 - версия базы передаем
            string[] message = { newversionbases.ToString(), request, staticVariable.Variable.getUser_id().ToString(), "0" , parties };
            ServerModel.textmessage = message;
            ServerModel.listFileInfo = null;

            return ServerModel;
        }

        public ServerTopicModel CreateModelTopicPUBLISHER(string request,  string username, string parties)
        {
            ServerTopicModel ServerModel = new ServerTopicModel();
            ServerModel.username = username;
            //здесь используется как заглушка т.к PUBLISHER берет данные из string[] message, а остальные из ServerTopicModel
            ServerModel.connectionId = staticVariable.Variable.activemqConnectionId.Keys.ToArray();

            //0 - clientVersionBases
            //1 - имя запроса
            //2 - user_id
            //3 - заглушка версии базы
            //4 - заглушка запроса
            //5 - connectionId ActiveMq получаем вовремя создания коннекта к activeMq(не используется)
            string[] message = { 0.ToString(),  request, staticVariable.Variable.getUser_id().ToString(), "0", parties , ""};
            ServerModel.textmessage = message;
            ServerModel.listFileInfo = null;

            return ServerModel;
        }

        public ServerTopicModel CreateModelTopicGenLink(string request, string listFilesRow_id ,string username, string parties)
        {
            ServerTopicModel ServerModel = new ServerTopicModel();
            ServerModel.username = username;
            ServerModel.connectionId = staticVariable.Variable.activemqConnectionId.Keys.ToArray();

            //4 - заглушка запроса
            //5 - ListFilesRow_id
            string[] message = { 0.ToString(), request, staticVariable.Variable.getUser_id().ToString(), "0", parties, listFilesRow_id };
            ServerModel.textmessage = message;
            ServerModel.listFileInfo = null;

            return ServerModel;
        }

        public ServerTopicModel CreateModelTopicErroAlert(string request, string username, string errorAlert , string textError)
        {
            ServerTopicModel ServerModel = new ServerTopicModel();
            ServerModel.username = username;
            //здесь используется как заглушка т.к PUBLISHER берет данные из string[] message, а остальные из ServerTopicModel
            ServerModel.connectionId = staticVariable.Variable.activemqConnectionId.Keys.ToArray();

            //0 - clientVersionBases
            //1 - имя запроса
            //2 - user_id
            //3 - заглушка версии базы
            //4 - ErrorAlert
            //5 - TextError
            string[] message = { 0.ToString(), request, staticVariable.Variable.getUser_id().ToString(), "0", errorAlert, textError};
            ServerModel.textmessage = message;
            ServerModel.listFileInfo = null;

            return ServerModel;
        }


    }
}
