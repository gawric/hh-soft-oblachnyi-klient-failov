﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.servertopic
{
  
    public class ServerTopicModel : ICloneable
    {

        public string username { get; set; }
        //0 - version bases
        //1 - request server/ responce server | working bases
        //1 - статусы | ACTUAL - база актуальна | SYNCREATEALLLIST - перезаписать всю базу | SYNSINGLLIST - перезаписать часть базы
        //2 - List size (если она есть или 0 в противном случаи)
        //3 - user_id из сервера
        //4  - numberParties - разбиваем большие файлы на куски здесь статусы (BEGINPARTIES|PARTIES|ENDPARTIES)
        public string[] textmessage { get; set; }

        public String webdavnamespace { get; set; }

        //задается сервером при первом подключении клиента!
        public string[] connectionId { get; set; }

        public FileInfoModel[] listFileInfo { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
