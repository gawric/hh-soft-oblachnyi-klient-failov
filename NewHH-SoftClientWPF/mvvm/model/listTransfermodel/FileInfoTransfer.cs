﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.listtransfermodel
{
    public class FileInfoTransfer
    {
        public int row_id { get; set; }
        public int row_id_listFiles_users { get; set; }
        public string filename { get; set; }
        public string sizebyte { get; set; }
        public string type { get; set; }
        public double progress { get; set; }
        public string uploadStatus { get; set; }
        public string typeTransfer { get; set; }
    }
}
