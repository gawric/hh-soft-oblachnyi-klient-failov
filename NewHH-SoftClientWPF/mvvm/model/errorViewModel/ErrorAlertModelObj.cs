﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.errorViewModel
{
    public class ErrorAlertModelObj
    {
        public ConvertIconType _convertIcon { get; set; }
        public ErrorAlertWindow _errorAlertWindow { get; set; }
    
        public UpdateNetworkJsonController _sendJson { get; set; }
        public Thread _dispatcherThread { get; set; }
        public bool isUIThread {get;set;}
        
    }
}
