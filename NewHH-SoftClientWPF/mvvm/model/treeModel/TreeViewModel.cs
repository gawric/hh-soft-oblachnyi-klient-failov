﻿using NewHH_SoftClientWPF.contoller.sorted;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.treemodel
{
    public class TreeViewModel 
    {
        public SortableObservableCollection<FolderNodes> Items { get; set; }

        public TreeViewModel()
        {
            Items = new SortableObservableCollection<FolderNodes>();
        }
    }
}
