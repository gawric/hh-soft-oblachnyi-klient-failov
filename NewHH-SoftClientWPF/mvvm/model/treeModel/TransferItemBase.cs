﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm.model.treemodel
{
    public class TransferItemBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public double progress { get; set; }
        public double progressFiles { get; set; }
        public BitmapSource icon { get; set; }
        public BitmapSource iconTransfer { get; set; }
        public string uploadStatus { get; set; }
        public string sizebyte { get; set; }

        public TransferItemBase()
        {
           
        }

        //скрывает текст нода
        public double ProgressFiles
        {
            get { return this.progressFiles; }
            set
            {
                if (value != this.progressFiles)
                {
                    this.progressFiles = value;
                    NotifyPropertyChanged("ProgressFiles");
                }
            }
        }

        //скрывает текст нода
        public double Progress
        {
            get { return this.progress; }
            set
            {
                if (value != this.progress)
                {
                    this.progress = value;
                    NotifyPropertyChanged("Progress");
                }
            }
        }

        //скрывает текст нода
        public string UploadStatus
        {
            get { return this.uploadStatus; }
            set
            {
                if (value != this.uploadStatus)
                {
                    this.uploadStatus = value;
                    NotifyPropertyChanged("UploadStatus");
                }
            }
        }

      
        public BitmapSource Icon
        {
            get { return this.icon; }
            set
            {
                if (value != this.icon)
                {
                    this.icon = value;
                    NotifyPropertyChanged("UploadStatus");
                }
            }
        }

      
        public BitmapSource IconTransfer
        {
            get { return this.iconTransfer; }
            set
            {
                if (value != this.iconTransfer)
                {
                    this.iconTransfer = value;
                    NotifyPropertyChanged("UploadStatus");
                }
            }
        }

        public string Sizebyte
        {
            get { return this.sizebyte; }
            set
            {
                if (value != this.sizebyte)
                {
                    this.sizebyte = value;
                    NotifyPropertyChanged("Sizebyte");
                }
            }
        }



        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}

