﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public class StakRunObjectModel
    {
        public SqliteController sqlLiteController { get; set; }
        public WebDavClientController webDavClient { get; set; }
        public NavigationMainWindowsController navigationMainController { get; set; }
        public ViewFolderViewModel viewModel { get; set; }
        public MainWindowViewModel viewMain { get; set; }
        public FileInfoModel[] copyArrStak { get; set; }
    }
}
