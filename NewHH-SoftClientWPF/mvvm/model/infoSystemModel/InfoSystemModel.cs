﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.infoSystemModel
{
    public class InfoSystemModel
    {
        public string osVerison { get; set; }
        public string osUserName { get; set; }
        public string macNetwork { get; set; }
        public string ipLocal { get; set; }
        public string ipNetwork { get; set; }
    }
}
