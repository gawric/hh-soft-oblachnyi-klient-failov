﻿using NewHH_SoftClientWPF.contoller.sorted;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.model.listViewModel
{
    public class ArgsDataListView : EventArgs
    {
        private SortableObservableCollection<FolderNodes> obs;
     

        public ArgsDataListView(SortableObservableCollection<FolderNodes> obs)
        {
            this.obs = obs;
        
        }

        public SortableObservableCollection<FolderNodes> getObs
        {
            get { return obs; }
        }

       

    }
}

