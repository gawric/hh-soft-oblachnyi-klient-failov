﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.listViewModel
{
    public class ReloadAllListViewModel
    {
        //Все дети
       public  List<FileInfoModel> children { get; set; }

        //Отсортированные дети на удаление
       public  List<FolderNodes> childrenToDelete { get; set; }

        //Все дети найденные в Observable ListView
       public  List<FileInfoModel> childrenToInsert { get; set; }

        public FileInfoModel modelServer { get; set; }
    }
}
