﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel
{
    public class TimerFimModel
    {
        public string[] listHref { get; set; }
        public FileInfoModel sourceFim { get; set; }

        public FileInfoModel pasteFim { get; set; }
    }
}
