﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel.stack
{
    public class StackNetworkModel
    {
        public string[] hrefUpload { get; set; }
        public FileInfoModel pasteFolder { get; set; }
        public bool isViewFolder { get; set; }
        public List<FolderNodes> listDownload { get; set; }
        public int countDownload { get; set; }
        public Dictionary<string, long> dictHrefUploadRowId { get; set; }
        public Dictionary<string, long> dictHrefDownloadRowId { get; set; }

        public bool isUploadScanFolder { get; set; }
        public InfoStackModel infoStack {get;set;}

        public List<FileInfoModel> tempFileSubsribleList { get; set; }
    }
}
