﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class MoveFilesAsyncModel
    {
        public string copyFolderName { get; set; }
        public string location { get; set; }
        public string pasteLocation { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public bool isFolder { get; set; }
        public string oldFolderName { get; set; }
    }
}
