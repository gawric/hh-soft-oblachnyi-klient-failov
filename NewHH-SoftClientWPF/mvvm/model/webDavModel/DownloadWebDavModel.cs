﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class DownloadWebDavModel
    {
        public WebDavClient _webDavclient { get; set; }
        public Client client { get; set; }
        public long donwloadSizeFiles { get; set; }
        public List<FolderNodes> _listDownload { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string locationDownload { get; set; }
        public SqliteController _sqlliteController { get; set; }
        public RecursiveLocalModel modelSizebyteAndCount { get; set; }
        public ObservableCollection<FolderNodesTransfer> _transferViewNodes { get; set; }
        public ViewFolderViewModel _viewFolderModel { get; set; }

        public int[] currentCountFiles { get; set; }
        //обычно используется для поиска нода в TreeView
        public NavigationMainWindowsController _navigationController { get; set; }
        public UpdateViewTransferController _updateTransferController { get; set; }
        //срабатывает когда происходит копирования файла из папки
        public EventHandler<UpdateViewTransferFolderPgArgs> folderTreeViewPgEventHandle { get; set; }
        public ConvertIconType convertIcon { get; set; }
        public CancellationController _cancellationController {get;set;}
        public BlockingEventController _blockingController { get; set; }
        public Dictionary<string, long> dictDownloadHrefRow_id { get; set; }
        public ExceptionController _exceptionController { get; set; }
        public StackUpdateTreeViewIcon _stackUpdateTreeViewIcon { get; set; }
        public MainWindowViewModel _viewMain { get; set; }
        public bool isViewVolder { get; set; }
        public FolderNodesTransfer modelSource { get; set; }

    }
}
