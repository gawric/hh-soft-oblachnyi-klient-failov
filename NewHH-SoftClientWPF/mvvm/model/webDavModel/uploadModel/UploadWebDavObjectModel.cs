﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel
{
    public class UploadWebDavObjectModel
    {
        public WebDavClient _webDavclient { get; set; }
        public UpdateNetworkJsonController networkJsonController { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public UpdateViewTransferController _updateTransferController { get; set; }
        public ObservableCollection<FolderNodesTransfer> _transferViewNodes { get; set; }
        public BlockingEventController _blockingEventController { get; set; }
        public CancellationController _cancellationController { get; set; }
        public ExceptionController _exceptionController { get; set; }
        public ConvertIconType convertIcon { get; set; }
        public WebDavClientExist webDavExist { get; set; }
    }
}
