﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav
{
    public class ScanUploadWebDavModel
    {
        public string clientLocalDirectory { get; set; }
        public string pasteRootWebDavDirectory { get; set; }
        public string clientRootLocalDirectory { get; set; }
        public string clientNameFolder { get; set; }
        public string hrefFolder { get; set; }
        public string folderName { get; set; }
        public string pasteHref { get; set; }
        public long verison_dabases { get; set; }
        public FileInfoModel[] arr { get; set; }
        public UpdateNetworkJsonController networkJsonController { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public long[] new_id_container { get; set; }
        public FileInfoModel pasteModel { get; set; }
        public Dictionary<string, long> allFolderDictionary { get; set; }
        public Client client { get; set; }
        public string uploadFolderWebDav { get; set; }
        public UpdateViewTransferController _updateTransferController { get; set; }
        public ObservableCollection<FolderNodesTransfer> _transferViewNodes { get; set; }
        //срабатывает когда происходит копирования файла из папки
        public EventHandler<UpdateViewTransferFolderPgArgs> folderTreeViewPgEventHandle { get; set; }
        //для PG Folder общее количество файлов в папке
        public int allCountFilesToFolder { get; set; }
        public int allCountFilesToFiles { get; set; }
        public long allSizeBytesFolder { get; set; }
        //какой сейчас по счету копируется файл
        public int[] currentCountFilesToFolder { get; set; }
        public DirectoryInfo copyDirectory { get; set; }
        public List<FileInfoModel> parentFilesPasteFolder { get; set; }
        public SotredSourceHrefToDouble checkParent { get; set; }
        public ConvertIconType convertIcon { get; set; }
        public WebDavClientExist webDavExist { get; set; }
        //все папки из базы данных 
        public Dictionary<string , long> allHrefFolder { get; set; }
        public BlockingEventController _blockingEventController { get; set; }
        public CancellationController _cancellationController { get; set; }
        public ExceptionController _exceptionController { get; set; }
        public FolderNodesTransfer modelSource { get; set; }
        public EventHandler<UploadFolderEventHandler> updateEventHandler { get; set; }
    }
}
