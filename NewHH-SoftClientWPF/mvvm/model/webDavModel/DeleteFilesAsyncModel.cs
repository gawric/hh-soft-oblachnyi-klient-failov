﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class DeleteFilesAsyncModel
    {
       public UpdateNetworkJsonController updateNetworkJsonController { get; set; }
       public long newVersionBases { get; set; }
       public string username { get; set; }
       public string password { get; set; }
       public SupportServerTopicModel supportTopicModel { get; set; }
       public SqliteController _sqlLiteController { get; set; }
       public string location { get; set; }
       public string type { get; set; }
       public bool isMove { get; set; }
       public IProgress<CopyViewModel> progress { get; set; }
       public CopyViewModel status { get; set; }
       public string deleteStatus { get; set; }
       public bool deleteRootFolder { get; set; }
       public string progressLabel { get; set; }
       public FolderNodes[] allDeleteFiles { get; set; }
       public Dictionary<FolderNodes, bool> AllDeleteFilesReplaceStatus { get; set; }
       public Dictionary<FileInfoModel, bool> AllDeletePasteFilesReplaceStatus { get; set; }
       public WebDavClientExist _webdavClientExist { get; set; }
    }
}
