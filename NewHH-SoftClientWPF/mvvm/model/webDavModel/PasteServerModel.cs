﻿using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    //класс прокладка используется в ViewFolder что-бы передавать не 5 аргументов а 1
   public  class PasteServerModel
    {
        public Dictionary<FileInfoModel, bool> allPasteNodes { get; set; }
        public Dictionary<FolderNodes, bool> allCopyNodes { get; set; }
        public FolderNodes folderNodes { get; set; }
        public FolderNodes pasteNodes { get; set; }
        public bool replace { get; set; }
        //путь к папке откуда идет копирование (отец для всех копируемых нодов)
        public string copyOrCutRootLocation { get; set; }
        public ExceptionController _exceptionController { get; set; }
    }
}
