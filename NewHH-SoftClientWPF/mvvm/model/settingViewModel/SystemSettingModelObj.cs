﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.settingModel
{
    public class SystemSettingModelObj
    {
        public SqliteController _sqliteController { get; set; }
        public SettingWindowViewModel _swvm { get; set; }
        public ViewFolderViewModel _viewModelViewFolder { get; set; }
        public MainWindowViewModel _viewModelMain { get; set; }
        public ConvertIconType _convertIcon { get; set; }
        public BlockingEventController _blockingEventController { get; set; }
        public CancellationController _cc { get; set; }
        public ExceptionController _error { get; set; }

        public ButtonUploadClick _uploadClick { get; set; }
        public WebDavClientExist _wdce { get; set; }

        public CreateFolderController _cfc { get; set; }
        public ExceptionController _excepc { get; set; }
        public FirstSyncFilesController _fsfc { get; set; }
        public UpdateNetworkJsonController _unjc { get; set; }

        public StackOperationUpDown _soup { get; set; }

    }
}
