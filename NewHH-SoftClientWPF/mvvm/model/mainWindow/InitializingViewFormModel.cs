﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.mainWindow
{
    public class InitializingViewFormModel
    {
        public ViewFolder _viewFolder { get; set; }
        public ConvertIconType _convertIcon { get; set; }
        public ViewFolderContextMenu _viewFolderContextMenu { get; set; }
    

    }
}
