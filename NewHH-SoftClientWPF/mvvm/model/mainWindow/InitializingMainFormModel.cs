﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.supportForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.mainWindow
{
    public class InitializingMainFormModel
    {
        public MainWindow _mainWindow { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public ConvertIconType _convertIcon { get; set; }
        public bool isLoadingInitializing { get; set; }
        public MainWindowViewModel _viewModel { get; set; }
        public UpdateFormMainController _updateMainForm { get; set; }
        public SupportEventPart2MainForm _supportEventPart2MainForm { get; set; }
        public MainWindowContextMenu _mainWindowsContextMenu { get; set; }
    }
}
