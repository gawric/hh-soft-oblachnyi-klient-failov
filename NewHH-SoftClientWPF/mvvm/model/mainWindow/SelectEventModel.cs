﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.mvvm.supportForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.mainWindow
{
    public class SelectEventModel
    {
        public MainWindow _mainWindow { get; set; }
        public MainWindowViewModel _viewModel;
        public NavigationMainWindowsController _navigationMainController { get; set; }
        public SupportEventPart1MainForm _supportEventPart1MainForm { get; set; }
        public SupportEventPart2MainForm _supportEventPart2MainForm { get; set; }
        public RenameMainEvent _renameMainEvent { get; set; }
    }
}
