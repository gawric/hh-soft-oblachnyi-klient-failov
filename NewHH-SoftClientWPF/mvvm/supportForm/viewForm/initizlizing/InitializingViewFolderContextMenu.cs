﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.initizlizing
{
    public class InitializingViewFolderContextMenu
    {
        private ViewFolder _viewFolder;

        public InitializingViewFolderContextMenu(ViewFolder mainWindow)
        {
            _viewFolder = mainWindow;
        }

        public void Initializing(ViewFolderContextMenu _viewFolderContextMenu, ConvertIconType _convertIcon)
        {

            _viewFolderContextMenu.SaveOkText = "Сохранить на компьютере";
            _viewFolderContextMenu.SaveCloudText = "Оставить в облаке";
            _viewFolderContextMenu.CopyLinkText = "Скопировать ссылку";
            _viewFolderContextMenu.ContextMenuTreeViewCreate = _convertIcon.getIconType(".contextMenuCreate");
            _viewFolderContextMenu.ContextMenuTreeViewSaveOk = _convertIcon.getIconType(".contextMenuItemSaveOk");
            _viewFolderContextMenu.СontextMenuTreeViewSaveCloud = _convertIcon.getIconType(".cloud");
            _viewFolderContextMenu.ContextMenuTreeViewCutFolder = _convertIcon.getIconType(".contextMenuItemCutFolder");
            _viewFolderContextMenu.ContextMenuTreeViewCopyFolder = _convertIcon.getIconType(".contextMenuItemCopyFolder");
            _viewFolderContextMenu.ContextMenuTreeViewPasteFolder = _convertIcon.getIconType(".contextMenuItemPasteFolder");
            _viewFolderContextMenu.ContextMenuTreeViewRenameFolder = _convertIcon.getIconType(".contextMenuItemRenameFolder");
            _viewFolderContextMenu.ContextMenuTreeViewDeleteFolder = _convertIcon.getIconType(".contextMenuItemDeleteFolder");
            _viewFolderContextMenu.ContextMenuTreeViewCopyLink = _convertIcon.getIconType(".contextMenuItemCopyLink");
            _viewFolderContextMenu.ContextMenuTreeViewGoFolder = _convertIcon.getIconType(".contextMenuItemGoFolder");


            _viewFolder.ListView.ContextMenu.DataContext = _viewFolderContextMenu;
        }
    }
}
