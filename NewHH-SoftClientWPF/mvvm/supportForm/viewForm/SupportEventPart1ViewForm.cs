﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.operationClickButton.doubleclick;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.viewFolderModel;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.operationContext;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm
{
    public class SupportEventPart1ViewForm
    {
        
        private MainWindowViewModel _viewModelMain;
        private SqliteController _sqlLiteController;
        private ViewFolder _viewFolder;
        private ConvertIconType _convertIcon;
        private CopyStatusController _copyStatus;
        private PasteWindowsController _pasteController;
        private PasteOrMoveEventModel _moveModel;
        private DoubleClick _doubleClickEvent;
        private FirstSyncFilesController _firstSyncController;
        private ViewFolderViewModel _viewModel;
        private ListCopyNodesEventView _listCopyNodes;
        private CreateModel _createModel;
        private ExceptionController _exceptionController;
        private UpdateNetworkJsonController _sendJsonNewtwork;

        public SupportEventPart1ViewForm(Container cont)
        {
            _viewModelMain = cont.GetInstance<MainWindowViewModel>();
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _convertIcon = cont.GetInstance<ConvertIconType>();
            _copyStatus = cont.GetInstance<CopyStatusController>();
            _pasteController = cont.GetInstance<PasteWindowsController>();
            _doubleClickEvent = cont.GetInstance<DoubleClick>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            _viewModel = cont.GetInstance<ViewFolderViewModel>();
            _exceptionController = cont.GetInstance<ExceptionController>();
            _listCopyNodes = new ListCopyNodesEventView(_viewModelMain , _sqlLiteController);
            _sendJsonNewtwork = cont.GetInstance<UpdateNetworkJsonController>();
            _createModel = new CreateModel(_convertIcon);
        }

        public void injectElemetns(ViewFolder _viewFolder)
        {
            this._viewFolder = _viewFolder;
            _moveModel = new CreateModel(_convertIcon).createPateOrMoveEventModel(_copyStatus, _pasteController, _sqlLiteController, _viewFolder, _viewModelMain);
        }

        public void openExplorer(object sender, RoutedEventArgs e)
        {
            if ((FolderNodes)_viewFolder.ListView.SelectedItem != null)
            {
                List<FolderNodes> listGenLink = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToList();
                OpenExplorerViewFolder openExplorer = new OpenExplorerViewFolder(_sqlLiteController, _exceptionController);
                openExplorer.Open(listGenLink[0].Location , listGenLink[0].ParentID);
            }
            else
            {
                Console.WriteLine("CopyContextMenu_Click каталог или файл не выделен");
            }
        }

        public void listViewMouseDoubleClickEvent(object sender, MouseButtonEventArgs e)
        {
            MouseDoubleClickEvent doubleClickEvent = new MouseDoubleClickEvent( _doubleClickEvent,  _viewModelMain);
            doubleClickEvent.click( sender,  e);
        }

        public void cutContextMenuClickEvent(object sender, RoutedEventArgs e)
        {
            CutViewEvent cutEvent = new CutViewEvent( _viewModelMain,  _viewFolder,  _copyStatus, _listCopyNodes);
            cutEvent.cut();
        }

        public void copyContextMenuClickEvent(object sender, RoutedEventArgs e)
        {
            CopyViewEvent copyEvent = new CopyViewEvent( _viewModelMain,  _viewFolder, _listCopyNodes);
            copyEvent.copy();
        }

        public void pasteContextMenuClickEvent(object sender, RoutedEventArgs e)
        {
           
            PasteOrMoveEvent pasteEvent = new PasteOrMoveEvent(_moveModel);
            pasteEvent.paste( sender,  e);
        }

        public void removeContextMenuClickEvent(object sender, RoutedEventArgs e)
        {
            RemoveViewEvent removeEvent = new RemoveViewEvent( _viewFolder,  _firstSyncController);
            removeEvent.remove();
        }

        public void copyLinkContextMenuClickEvent(object sender, RoutedEventArgs e)
        {
            CopyLinkViewEvent copyLinkEvent = new CopyLinkViewEvent(_viewFolder , _sendJsonNewtwork);
            copyLinkEvent.copyLink();
        }

        //eventMainWindowsKeyEnter

        public void eventOnlyCloudContextMenu(object sender, RoutedEventArgs e)
        {
            OnlyCloudViewEvent onlyClodViewEvent = new OnlyCloudViewEvent(_createModel.createModel(_sqlLiteController, _convertIcon, _viewModel.NodesView.Items, _viewModel.NodesView.Items , _exceptionController));
            onlyClodViewEvent.cloud(sender, e , _viewFolder);
        }

        

    }
}
