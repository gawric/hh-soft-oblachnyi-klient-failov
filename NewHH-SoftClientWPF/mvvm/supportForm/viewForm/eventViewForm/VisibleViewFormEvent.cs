﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm
{
    public class VisibleViewFormEvent
    {
        private ViewFolder _viewFolder;
        private UpdateFormMainController _updateMainForm;

        public VisibleViewFormEvent(ViewFolder _viewFolder, UpdateFormMainController _updateMainForm)
        {
            this._viewFolder = _viewFolder;
            this._updateMainForm = _updateMainForm;
        }

        public void visible(TextBox renameTextbox  , DependencyPropertyChangedEventArgs e , ref string oldFolderNodes)
        {
            


            //Visible - делаем видимым textBox
            if ((bool)e.NewValue == true)
            {
                renameTextbox.Focus();
                renameTextbox.SelectAll();
            }
            else
            {
                //Скрываем TextBox и переименовываем FolderNodes на переданный нам
                hideRenameSetFolderName(renameTextbox.Text , ref oldFolderNodes);
            }
        }


        //Обновляет FolderName что-бы изменить имя в коллекции
        public  void hideRenameSetFolderName(string strTextBox , ref string oldFolderNodes)
        {
            //Нужно тестировать ListView
            if (_viewFolder.ListView.SelectedItem != null)
            {
                FolderNodes nod = (FolderNodes)_viewFolder.ListView.SelectedItem;

                SortableObservableCollection<FolderNodes> ChildrenNode = new SortableObservableCollection<FolderNodes>();
                //string oldFolderNodes = String.Copy(nod.FolderName);
                nod.FolderName = strTextBox;
                ChildrenNode.Insert(0, nod);

                //Rename оповещает базы и webdav
                _updateMainForm.updateMainRenameTreeView(oldFolderNodes, ChildrenNode);



                //Изменяем имя в дереве
                nod.FolderName = strTextBox;
            }


        }
    }
}
