﻿using NewHH_SoftClientWPF.contoller.operationClickButton.doubleclick;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext
{
    public class MouseDoubleClickEvent
    {
        private DoubleClick _doubleClickEvent;
        private MainWindowViewModel _viewModelMain;

        public MouseDoubleClickEvent(DoubleClick _doubleClickEvent , MainWindowViewModel _viewModelMain)
        {
            this._doubleClickEvent = _doubleClickEvent;
            this._viewModelMain = _viewModelMain;
        }

        public void click(object sender, MouseButtonEventArgs e)
        {
            //все ноды в лисет
            ListView item = sender as ListView;

            ObservableCollection<FolderNodes> fol = _viewModelMain.NodesView.Items;


            if (item != null)
            {
                //текущий нод
                FolderNodes selectNodes = (FolderNodes)item.SelectedItem;

                if (selectNodes != null)
                {
                    //только папки
                    if (staticVariable.Variable.isFolder(selectNodes.Type))
                    {
                       // _viewModelMain.listSelectNodes
                        Task.Run(() => _doubleClickEvent.startDoubleCLick(selectNodes, fol));
                    }

                }

            }
        }

    }

}
