﻿using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext
{
    public class CutViewEvent
    {
        private MainWindowViewModel _viewModelMain;
        private ViewFolder _viewFolder;
        private CopyStatusController _copyStatus;
        private ListCopyNodesEventView _listCopyNodes;

        public CutViewEvent(MainWindowViewModel _viewModelMain , ViewFolder _viewFolder , CopyStatusController _copyStatus , ListCopyNodesEventView listCopyNodes)
        {
            this._viewModelMain = _viewModelMain;
            this._viewFolder = _viewFolder;
            this._copyStatus = _copyStatus;
            this._listCopyNodes = listCopyNodes;
        }

        public void cut()
        {
            //setListCopyNodes нужно проверять
            _viewModelMain.setListCopyNodes(null);
            _viewModelMain.CopyAndCutRootLocation = "";

            if ((FolderNodes)_viewFolder.ListView.SelectedItem != null)
            {
                _copyStatus.isMove = true;
                _copyStatus.isMoveNetwork = true;

                List<FolderNodes> allCopyFolder = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToList();
                _listCopyNodes.setEventListCopyNodes(allCopyFolder);


                for (int l = 0; l < _viewModelMain.getListCopyNodes().Count; l++)
                {
                    Console.WriteLine("Добавили: ViewFolder-> CutContextMenu_Click " + _viewModelMain.getListCopyNodes()[l].FolderName);
                }
            }
            else
            {
                Console.WriteLine("CutContextMenu_Click каталог или файл не выделен");
            }
        }
    }
}
