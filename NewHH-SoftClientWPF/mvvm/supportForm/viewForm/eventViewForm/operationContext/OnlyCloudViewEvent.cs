﻿using NewHH_SoftClientWPF.contoller.view.operationContextMenu.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext
{
    public class OnlyCloudViewEvent
    {

        
            private OnlyCloudController _onlyCloudController;
            private OnlyCloudMainModel _onlyCloudMainModel;
            private MainWindow _mainWindow;

            public OnlyCloudViewEvent(OnlyCloudMainModel onlyCloudMainModel)
            {
                _onlyCloudMainModel = onlyCloudMainModel;
                _onlyCloudController = new OnlyCloudController(onlyCloudMainModel);

            }

            public void cloud(object sender, RoutedEventArgs e, ViewFolder _viewFolder)
            {
                if (_viewFolder.ListView.SelectedItems != null)
                {
                    List<FolderNodes> nodes = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToList<FolderNodes>();

                    if(nodes.Count > 0)
                    {
                        cloud(nodes);
                    }

                   
                }
                else
                {
                    Console.WriteLine("OnlyCloudEvent->cloud: Попытка пустого выделения ViewFolder");
                }
            }

            public void cloud(string folderLocation)
            {
                Task.Run(() => _onlyCloudController.cloud(getList(folderLocation)));

            }

            public void cloud(List<FolderNodes> nodesList)
            {
                Task.Run(() => _onlyCloudController.cloud(convertListToStringList(nodesList)));

            }



            private List<string> getList(string folderLocation)
            {
                List<string> listFolderLocation = new List<string>();
                listFolderLocation.Add(folderLocation);
                return listFolderLocation;
            }

            private List<string > convertListToStringList(List<FolderNodes> nodesList)
            {
                List<string> listFolderLocation = new List<string>();

                foreach(FolderNodes item in nodesList)
                {
                    listFolderLocation.Add(item.Location);
                }

                return listFolderLocation;
            }

        
    }
}
