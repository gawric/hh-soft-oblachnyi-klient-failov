﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext
{
    public class DownloadViewEvent
    {
        private ViewFolder _viewFolder;
        private StackOperationUpDown _stackOperationUpDown;
        private ConvertIconType _convertIcon;

        public DownloadViewEvent(ViewFolder _viewFolder , StackOperationUpDown _stackOperationUpDown , ConvertIconType _convertIcon)
        {
            this._viewFolder = _viewFolder;
            this._stackOperationUpDown = _stackOperationUpDown;
            this._convertIcon = _convertIcon;
        }


        public void download(bool isViewFolder)
        {
            if (_viewFolder.ListView.SelectedItems != null)
            {
                List<FolderNodes> listDownload = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToList();

                _stackOperationUpDown.addStackDownload(listDownload , isViewFolder);

                // _webDavClient.downloadWebDav(_sqlLiteController, _navigationMainController, listDownload);
            }
        }

       
    }
}
