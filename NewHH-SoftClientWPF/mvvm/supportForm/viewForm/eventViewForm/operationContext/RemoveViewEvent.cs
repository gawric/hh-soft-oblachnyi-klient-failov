﻿using NewHH_SoftClientWPF.contoller.network.sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext
{
    public class RemoveViewEvent
    {
        private ViewFolder _viewFolder;
        private FirstSyncFilesController _firstSyncController;

        public RemoveViewEvent(ViewFolder _viewFolder , FirstSyncFilesController _firstSyncController)
        {
            this._viewFolder = _viewFolder;
            this._firstSyncController = _firstSyncController;
        }

        public void remove()
        {
            if (_viewFolder.ListView.SelectedItems != null)
            {
                FolderNodes[] nodes = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToArray();

                Task.Run(async () =>
                {
                    _firstSyncController.startAsyncDelete(nodes);
                });


            }
            else
            {
                Console.WriteLine("ViewFolder->RemoveContextListMenu_Click: Попытка пустого выделения ViewFolder");
            }

        }
    }
}
