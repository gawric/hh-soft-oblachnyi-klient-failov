﻿using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext
{
   public  class CopyViewEvent
   {
        private MainWindowViewModel _viewModelMain;
        private ViewFolder _viewFolder;
        private ListCopyNodesEventView _listCopyNodes;

        public CopyViewEvent(MainWindowViewModel _viewModelMain , ViewFolder _viewFolder , ListCopyNodesEventView listCopyNodes)
        {
            this._viewModelMain = _viewModelMain;
            this._viewFolder = _viewFolder;
            this._listCopyNodes = listCopyNodes;
        }

        public void copy()
        {
            _viewModelMain.setListCopyNodes(null);
            _viewModelMain.CopyAndCutRootLocation = "";

            if ((FolderNodes)_viewFolder.ListView.SelectedItem != null)
            {
                List<FolderNodes> allCopyFolder = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToList();
                _listCopyNodes.setEventListCopyNodes(allCopyFolder);


               // Console.WriteLine("ViewFolder->Вставляем в кэш копируемые файлы");

                for (int l = 0; l < _viewModelMain.getListCopyNodes().Count; l++)
                {
                    Console.WriteLine("Добавили: ViewFolder-> " + _viewModelMain.getListCopyNodes()[l].FolderName);
                }
            }
            else
            {
                Console.WriteLine("CopyContextMenu_Click каталог или файл не выделен");
            }
        }
    }
}
