﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext
{
    public class RenameViewEvent
    {
        private ViewFolderViewModel _viewModel;
        private ViewFolder _viewFolder;
      

        public RenameViewEvent(ViewFolderViewModel _viewModel , ViewFolder _viewFolder)
        {
            this._viewModel = _viewModel;
            this._viewFolder = _viewFolder;
        }

        public void rename(ref string oldFolderNodes)
        {
            _viewModel._isRenameActiveToListView = true;
            visibleRenameFolderNodes(ref oldFolderNodes);
        }

        //Открывает TextBox для ввода новых данных
        private void visibleRenameFolderNodes(ref string oldFolderNodes)
        {
            FolderNodes nod = (FolderNodes)_viewFolder.ListView.SelectedItem;
            nod.FolderNameVisibility = Visibility.Collapsed;
            nod.EditableTextBoxHeader = Visibility.Visible;

            if (staticVariable.Variable.isFolder(nod.Type))
            {
                oldFolderNodes = String.Copy(nod.FolderName) + "/";
            }
            else
            {
                oldFolderNodes = String.Copy(nod.FolderName);
            }
        }

    }
}
