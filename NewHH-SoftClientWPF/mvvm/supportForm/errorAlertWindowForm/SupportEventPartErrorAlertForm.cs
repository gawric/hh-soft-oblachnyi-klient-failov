﻿using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using NewHH_SoftClientWPF.mvvm.supportForm.errorAlertWindowForm.eventErrorAlertForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.errorAlertWindowForm
{
    public class SupportEventPartErrorAlertForm
    {
        private EventClickErrorAlert _eventClickErrorAlert;
        private ErrorAlertModelObj _errorAlertModelObj;
        private ErrorAlertWindowViewModel _viewModel;

        public SupportEventPartErrorAlertForm(ErrorAlertModelObj errorAlertModelObj , ErrorAlertWindowViewModel viewModel)
        {
            _errorAlertModelObj = errorAlertModelObj;
            _viewModel = viewModel;
            _eventClickErrorAlert = new EventClickErrorAlert(_errorAlertModelObj);
        }

        public void buttonClose(object sender, RoutedEventArgs e)
        {
            _eventClickErrorAlert.clickCancel();
        }

        public void buttonSendError(object sender, RoutedEventArgs e)
        {
            _eventClickErrorAlert.clickSend(_viewModel.ErrorText , _viewModel.TextBlock);
        }
    }
}
