﻿using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.errorAlertWindowForm.eventErrorAlertForm
{
    public class EventClickErrorAlert
    {
        private ErrorAlertModelObj _errorAlertModelObj;

        public EventClickErrorAlert(ErrorAlertModelObj errorAlertModelObj)
        {
            _errorAlertModelObj = errorAlertModelObj;
        }

        public void clickCancel()
        {
            Console.WriteLine("Здесь будем реализовывать логику простого закрытия окна!");
        }

        public void clickSend(string errorAlert, string textAlert)
        {
            _errorAlertModelObj._sendJson.updateSendJsonERRORALERT(errorAlert, textAlert);
        }
    }
}
