﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.operationContext;
using NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.transfer;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer
{
    public class SupportEventPart1ViewTransfer
    {
        private CancellationController _cancellationController;
        private ViewTransfer _viewTransfer;
        private SupportViewTransfer _supportViewTransfer;
        private BlockingEventController _blockingController;
        private ViewTransferViewModel _viewModel;
        private SqliteController _sqllite;
        private OpenContextMenuEvent _openContextMenu;
        private ViewTransferContextMenu _viewContextMenu;
        private ConvertIconType _convertIcon;

        public SupportEventPart1ViewTransfer(Container cont)
        {
            _cancellationController = cont.GetInstance<CancellationController>();
            _blockingController = cont.GetInstance<BlockingEventController>();
            _viewModel = cont.GetInstance<ViewTransferViewModel>();
            _sqllite = cont.GetInstance<SqliteController>();
            _viewContextMenu = cont.GetInstance<ViewTransferContextMenu>();
            _convertIcon = cont.GetInstance<ConvertIconType>();

        }

        public void injectElemetns(ViewTransfer viewTransfer , SupportViewTransfer supportViewTransfer)
        {
            _viewTransfer = viewTransfer;
            _supportViewTransfer = supportViewTransfer;
            _openContextMenu = new OpenContextMenuEvent(_viewContextMenu, _viewTransfer, _convertIcon);
        }

        public void StopUploadContextMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            StopUploadTransferEvent uploadEvent = new StopUploadTransferEvent(_cancellationController, _viewTransfer, _supportViewTransfer);
            uploadEvent.upload(menuItem);
        }

        public void PauseUploadContextMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            PauseUploadTransferEvent pauseEvent = new PauseUploadTransferEvent(_blockingController, _viewTransfer, _supportViewTransfer);
            pauseEvent.pause(menuItem);
        }

        public void DeleteSinglContextMenu_Click(object sender, RoutedEventArgs e)
        {
            DeleteSinglRowEvent deleteSinglEvent = new DeleteSinglRowEvent( _viewTransfer,  _viewModel,  _sqllite);
            deleteSinglEvent.delete();
        }

        public void DeleteAllContextMenu_Click(object sender, RoutedEventArgs e)
        {
            DeleteAllRowEvent deleteAllRows = new DeleteAllRowEvent( _viewModel,  _sqllite);
            deleteAllRows.delete();
        }

        public void OpenContextMenu(object sender, ContextMenuEventArgs e)
        {
            _openContextMenu.open();
        }
    }
}
