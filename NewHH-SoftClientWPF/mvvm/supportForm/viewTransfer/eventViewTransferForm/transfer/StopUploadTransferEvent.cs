﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.transfer
{
    public class StopUploadTransferEvent
    {
        private CancellationController _cancellationController;
        private ViewTransfer _viewTransfer;
        private SupportViewTransfer _supportViewTransfer;
         

        public StopUploadTransferEvent(CancellationController cancellationController , ViewTransfer viewTransfer , SupportViewTransfer supportViewTransfer)
        {
            this._cancellationController = cancellationController;
            this._viewTransfer = viewTransfer;
            this._supportViewTransfer = supportViewTransfer;

        }

        public void upload(MenuItem menuItem)
        {
            IList list = _viewTransfer.ListView.SelectedItems;
            FolderNodesTransfer itemList = (FolderNodesTransfer)_viewTransfer.ListView.SelectedItem;

            if (itemList.TypeTransfer.Equals("upload"))
            {
                IContextMenuStop stopUpload = new StopUpload(_cancellationController, _supportViewTransfer);
                bool[] stop = { stopUpload.isStopUpload(menuItem.Header.ToString()) };
                stopUpload.startTrainingStop(list, stop, itemList);
            }
            else
            {
                IContextMenuStop stopDownload = new StopDownload(_cancellationController, _supportViewTransfer);
                bool[] stop = { stopDownload.isStopUpload(menuItem.Header.ToString()) };
                stopDownload.startTrainingStop(list, stop, itemList);
            }
        }
    }
}
