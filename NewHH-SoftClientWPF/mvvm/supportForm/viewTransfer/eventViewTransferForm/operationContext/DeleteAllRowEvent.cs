﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.operationContext
{
    public class DeleteAllRowEvent
    {
       // private ViewTransfer _viewTransfer;
        private ViewTransferViewModel _viewModel;
        private SqliteController _sqllite;

        public DeleteAllRowEvent(ViewTransferViewModel viewModel , SqliteController sqllite)
        {
            _viewModel = viewModel;
            _sqllite = sqllite;
        }

        public void delete()
        {
            Task.Run(() =>
            {
                //В программе сейчас используется
                ObservableCollection<FolderNodesTransfer> serverList = _viewModel.ListView.Items;
                if (serverList != null)
                {
                    clearViewList(serverList);
                    _sqllite.clearTableslistTransfer();

                }

            });
        }



        private void clearViewList(ObservableCollection<FolderNodesTransfer> serverList)
        {
            App.Current.Dispatcher.Invoke(delegate
            {
                serverList.Clear();
            });
        }
    }
}
