﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.operationContext
{
    public class OpenContextMenuEvent
    {
        private ViewTransferContextMenu _viewContextMenu;
        private ViewTransfer _viewTransfer;
        private ConvertIconType _convertIcon;

        public OpenContextMenuEvent(ViewTransferContextMenu viewContextMenu , ViewTransfer viewTransfer , ConvertIconType convertIcon)
        {
            _viewContextMenu = viewContextMenu;
            _viewTransfer = viewTransfer;
            _convertIcon = convertIcon;
        }

        public void open()
        {
            //isFinish(string text)

            FolderNodesTransfer fnt = (FolderNodesTransfer)_viewTransfer.ListView.SelectedItem;

            if (fnt != null)
            {
                updatePause(fnt);
            }

        }

        private void updatePause(FolderNodesTransfer fnt)
        {
            if (fnt.UploadStatus.Equals("Пауза"))
            {
                _viewContextMenu.HeaderPause = "Продолжить";
                _viewContextMenu.HeaderPauseIcon = _convertIcon.getIconType(".contextMenuStart");
            }
            else
            {
                _viewContextMenu.HeaderPause = "Пауза";
                _viewContextMenu.HeaderPauseIcon = _convertIcon.getIconType(".contextMenuPause");
            }
        }

    }
}
