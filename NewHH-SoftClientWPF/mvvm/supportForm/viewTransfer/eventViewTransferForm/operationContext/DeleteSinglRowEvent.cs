﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.operationContext
{
    public class DeleteSinglRowEvent
    {

        private ViewTransfer _viewTransfer;
        private ViewTransferViewModel _viewModel;
        private SqliteController _sqllite;

        public DeleteSinglRowEvent(ViewTransfer viewTransfer , ViewTransferViewModel viewModel , SqliteController sqllite)
        {
            _viewTransfer = viewTransfer;
            _viewModel = viewModel;
            _sqllite = sqllite;
        }

        public void delete()
        {
            if ((FolderNodesTransfer)_viewTransfer.ListView.SelectedItem != null)
            {
                //список кого мы хотим удалить
                List<FolderNodesTransfer> singlDelete = _viewTransfer.ListView.SelectedItems.OfType<FolderNodesTransfer>().ToList();

                Task.Run(() =>
                {
                    //В программе сейчас используется
                    ObservableCollection<FolderNodesTransfer> serverList = _viewModel.ListView.Items;

                    for (int s = 0; s < singlDelete.Count; s++)
                    {
                        FolderNodesTransfer model = singlDelete[s];
                        removeSinglViewList(serverList, model);
                        _sqllite.removeListFilesTransferSingl(model.Row_id);
                    }
                });


            }
        }

        private void removeSinglViewList(ObservableCollection<FolderNodesTransfer> serverList, FolderNodesTransfer deleteFiles)
        {
            App.Current.Dispatcher.Invoke(delegate
            {
                serverList.Remove(deleteFiles);
            });

        }
    }
}
