﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher;
using NewHH_SoftClientWPF.contoller.util.sync.autosave;
using NewHH_SoftClientWPF.contoller.util.sync.autosave.desibleAutoSave;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.other
{
    public class EventCommit
    {
        private SqliteController _sqlliteController;
        private SettingWindowViewModel _settingWindowViewModel;
        private ConvertIconType _convertIcon;
        private IAutoSaveFolder _autoSaveFolder;
        private IDesAutoSave _desAutoSave;
        private IFSWatcher _fsWatcher;

        public EventCommit(SqliteController sqlliteController , SettingWindowViewModel settingWindowViewModel , ConvertIconType convertIcon , IAutoSaveFolder autoSaveFolder , IDesAutoSave desAutoSave , IFSWatcher fsWatcher)
        {
            _sqlliteController = sqlliteController;
            _settingWindowViewModel = settingWindowViewModel;
            _convertIcon = convertIcon;
            _autoSaveFolder = autoSaveFolder;
            _desAutoSave = desAutoSave;
            _fsWatcher = fsWatcher;


        }

        public void commit(ref bool isCommit , ref ViewFolderViewModel viewModelViewFolder, ref MainWindowViewModel viewModelMain , ref ManualResetEvent mre)
        {
            if(isCommit)
            {
                commitLocatioDownload(viewModelViewFolder, viewModelMain);
                commitTimeAutoScan(ref mre);
                commitAutoStartApplication(_settingWindowViewModel);
                commitAutoSaveFolder(_settingWindowViewModel);
                _settingWindowViewModel.IsEnbledButtonCommit = false;

            }
           
            isCommit = false;
        }
       
       private void commitAutoSaveFolder(SettingWindowViewModel _settingWindowViewModel)
       {
            bool isEnabled = _settingWindowViewModel.IsCheckBoxScanFolderChecked;
            updateListPathViewToSql(_settingWindowViewModel, _sqlliteController);

            if (isEnabled)
            {
                trainingUploadStart();
            }
            else
            {
                trainingDeleteStart();
            }
       }
   
       private void updateListPathViewToSql(SettingWindowViewModel _settingWindowViewModel , SqliteController _sqlliteController)
       {
            ObservableCollection<ScanFolderModel> listScanFolder = _settingWindowViewModel.obsScanFol;
            _sqlliteController.clearScanFolder();
            _sqlliteController.getInsert().insertListScanFolder(listScanFolder);
            
       }
       private void trainingUploadStart()
       {
            Task.Run(() =>
            {
                stopWatchAll();
                List<ScanFolderModel> pathList = getAllScanFolder(_sqlliteController);
                _autoSaveFolder.StartUploadFolder(pathList, null);
                startWatch(pathList);
            });
           
        }
        
        private void startWatch(List<ScanFolderModel> pathList)
        {
            foreach(ScanFolderModel model in pathList)
            {
                _fsWatcher.StartRunningWatch(model.fullPath); 
            }
         
        }

        private void stopWatchAll()
        {
            if(_fsWatcher.IsRun()) _fsWatcher.StartCanselWS();

        }

        private void stopWatchAllFix()
        {
            _fsWatcher.StartCanselWS();

        }

        private void trainingDeleteStart()
        {
            Task.Run(() =>
            {
                List<ScanFolderModel> pathList = getAllScanFolder(_sqlliteController);
                _desAutoSave.StartDisabledAutoSave();
                stopWatchAllFix();
            });
        }

        private List<ScanFolderModel> getAllScanFolder(SqliteController _sqlliteController)
        {
            return _sqlliteController.getSelect().getAllScanFolder();
        }
        private void commitAutoStartApplication(SettingWindowViewModel _settingWindowViewModel)
        {
            bool isCheckedAutoStart = _settingWindowViewModel.IsCheckBoxAutoStartChecked;
            bool isCheckedScanFolder = _settingWindowViewModel.IsCheckBoxScanFolderChecked;
            SystemSettingModel ssm = _sqlliteController.getSelect().getSystemSetting();

            if (isCheckedAutoStart)ssm.autoStartApplication = 1; else { ssm.autoStartApplication = 0; }
            if (isCheckedScanFolder)ssm.startFolder = 1; else { ssm.startFolder = 0; }


            _sqlliteController.getInsert().insertSystemSetting(ssm);
        }
        private void commitTimeAutoScan(ref ManualResetEvent mre)
        {
            int timeMin = Convert.ToInt32(_settingWindowViewModel.TimeAutoScan);
            int timeMill = (int)TimeSpan.FromMinutes(timeMin).TotalMilliseconds;
            staticVariable.Variable.startMillisecondAutoSync = timeMill;
            _sqlliteController.updateAutoStartMill(timeMill.ToString());
            mre.Set();
        }
        private void commitLocatioDownload(ViewFolderViewModel _viewModelViewFolder , MainWindowViewModel _viewModelMain)
        {
            string locationDownload = _settingWindowViewModel.PathDisk;
            string sqlLocationDownload = _sqlliteController.getSelect().getLocationDownload();

            if (!locationDownload.Equals(sqlLocationDownload))
            {
                existSavePc(_viewModelViewFolder, _viewModelMain);
                _sqlliteController.getInsert().insertLocationDownload(locationDownload);
            } 

        }

      

        //проверка какие файлы сох. на компьютере
        public void existSavePc(ViewFolderViewModel _viewModelViewFolder, MainWindowViewModel _viewModelMain)
        {
            ExistSavePcEvent existSavePc = new ExistSavePcEvent(_convertIcon, _sqlliteController);
            existSavePc.existStart(_viewModelMain.NodesView.Items, _viewModelViewFolder.NodesView.Items);
        }

    }
}
