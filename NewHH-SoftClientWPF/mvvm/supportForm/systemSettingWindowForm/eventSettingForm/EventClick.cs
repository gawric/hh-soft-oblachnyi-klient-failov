﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm;
using SHDocVw;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm
{
    public class EventClick
    {
        private SettingWindowViewModel _swvm;


        public EventClick(SettingWindowViewModel swvm)
        {
 
            _swvm = swvm;
        }
        public void clickOk()
        {

        }
        public void clickCancel()
        {

        }
        
        public void clickChangeDisk(ref bool isCommit)
        {
            _swvm.IsEnbledButtonCommit = true;
            changeDisk();
            isCommit = true;
        }

      

        public void clickMountDisk(ref bool isCommit , ref SqliteController sqlLiteControlle)
        {
            _swvm.IsEnbledButtonCommit = true;

            EventMountDiskClick eventDiskClick = new EventMountDiskClick();
            List<Uri>  listWindows = eventDiskClick.getOpenWindowsExplorer();
            mount(ref sqlLiteControlle , ref eventDiskClick);

            eventDiskClick.waitExplorer();
            eventDiskClick.createNewWindows(listWindows,  ref sqlLiteControlle);

            isCommit = true;
        }

        public void clickUnmountDisk(ref bool isCommit , ref SqliteController sqlLiteControlle)
        {
            _swvm.IsEnbledButtonCommit = true;

            EventMountDiskClick eventDiskClick = new EventMountDiskClick();
            List<Uri> listWindows = eventDiskClick.getOpenWindowsExplorer();
            unmount(ref sqlLiteControlle , ref eventDiskClick);

            eventDiskClick.waitExplorer();
            eventDiskClick.createNewWindows(listWindows,  ref sqlLiteControlle);


            isCommit = true;


        }

     
        private void mount(ref SqliteController sqlLiteControlle , ref EventMountDiskClick eventDiskClick)
        {
            string hrefMount = sqlLiteControlle.getSelect().getLocationDownload();
            string hrefbat = staticVariable.Variable.getBATfolder() + "mount.bat";
            eventDiskClick.useBat(hrefMount, hrefbat);
        }

        private void unmount( ref SqliteController sqlLiteControlle , ref EventMountDiskClick eventDiskClick)
        {
           
            string hrefMount = sqlLiteControlle.getSelect().getLocationDownload();
            string hrefbat = staticVariable.Variable.getBATfolder() + "unmount.bat";
            eventDiskClick.useBat(hrefMount, hrefbat);
   
        }

       

        private void changeDisk()
        {
            IOpenDialog dialog = new OpenDialog();
            dialog.CreateFolderDialog(_swvm.PathDisk);
            _swvm.PathDisk = dialog.FileFolderPath;
        }

       

        
    }
}
