﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.changeIconViewModel;
using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.windows;
using NewHH_SoftClientWPF.staticVariable;
using NewHH_SoftClientWPF.view;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm
{
    public class SupportEventPart2MainForm
    {
        private InitializingMainForm initializingMainForm;
        private MainWindow _mainWindow;
        private string oldFOlderName1 = "";
        private RenameMainEvent _renameMainEvent;
        private ViewFolderViewModel _viewModelViewFolder;
        private MainWindowViewModel _viewModel;
        private FolderNodes unSelectedFolderNodes;
        private NavigationMainWindowsController _navigationMainController;
        private UpdateFormMainController _updateMainForm;
        private SqliteController _sqlliteController;
        //текущая позиция вставки
        private BackNodesModel insert = new BackNodesModel();
        private LoginForm _login;

        private ViewTransfer _viewTransfer;
        private CancellationController _cancellationController;
        private ConvertIconType _convertIcon;
     
        private FirstSyncFilesController _firstSyncController;
        private SupportEventPart1MainForm _supportPart1MainForm;
        private ClearScanEvent _clearScanEvent;
        private SelectEventModel _model;
        private SelectEvent _selectEvent;
        private UpdateNetworkJsonController _sendJson;
        private MqClientController _mqClientController;
        private BlockingEventController _blockingEventController;
        private WebDavClientController _webDavClient;
        private UpdateFormLoginController _updateForm;
        private MqClientController _mqClient;
        private LoginFormViewModel _loginFormViewModel;
        private ExceptionController _exceptionController;
        private ButtonUploadClick _uploadClick;
        private WebDavClientExist _wdce;
        private CreateFolderController _cfc;
        private CancellationController _cc;
        private StackOperationUpDown _soup;

        public SupportEventPart2MainForm(Container cont)
        {
            _viewModelViewFolder = cont.GetInstance<ViewFolderViewModel>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _sqlliteController = cont.GetInstance<SqliteController>();
            _navigationMainController = cont.GetInstance<NavigationMainWindowsController>();
            _updateMainForm = cont.GetInstance<UpdateFormMainController>();
            _viewTransfer = cont.GetInstance<ViewTransfer>();
            _cancellationController = cont.GetInstance<CancellationController>();
            _convertIcon = cont.GetInstance<ConvertIconType>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            _supportPart1MainForm = cont.GetInstance<SupportEventPart1MainForm>();
            _supportPart1MainForm = cont.GetInstance<SupportEventPart1MainForm>();
            _sendJson = cont.GetInstance<UpdateNetworkJsonController>();
     
            _mqClientController = cont.GetInstance<MqClientController>();
            _blockingEventController = cont.GetInstance<BlockingEventController>();
            _updateForm = cont.GetInstance<UpdateFormLoginController>();
            _webDavClient = cont.GetInstance<WebDavClientController>();
            _mqClient = cont.GetInstance<MqClientController>();
            _loginFormViewModel = cont.GetInstance<LoginFormViewModel>();
            _exceptionController = cont.GetInstance<ExceptionController>();
            _cc = cont.GetInstance<CancellationController>();

            //Для AutoSaveFolder
            _uploadClick = cont.GetInstance<ButtonUploadClick>();
            _wdce = cont.GetInstance<WebDavClientExist>();
            _cfc = cont.GetInstance<CreateFolderController>();
            _soup = cont.GetInstance<StackOperationUpDown>();


            insert.index = -1;



        }

        public void injectElemetns(InitializingMainForm initializingMainForm, MainWindow _mainWindow)
        {
            this.initializingMainForm = initializingMainForm;
            this._mainWindow = _mainWindow;
            _clearScanEvent = new ClearScanEvent(_mainWindow, _cancellationController, _convertIcon, _firstSyncController);
            _renameMainEvent = new RenameMainEvent(ref _viewModelViewFolder, ref _viewModel, ref _mainWindow);
            _model = createSelectModel(_mainWindow, _viewModel, _navigationMainController, _supportPart1MainForm, this, _renameMainEvent);
            _selectEvent = new SelectEvent(_model , _exceptionController);
          
        }


        public void eventOpenFolder(object sender, RoutedEventArgs e)
        {
           
            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                FolderNodes nodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                if (UtilMethod.isRootId(nodes.Row_id)) return;
                OpenExplorer openExplorer = new OpenExplorer(_sqlliteController , _exceptionController);
                openExplorer.Open(nodes.Location);
            }
            else
            {
                Console.WriteLine("CopyContextMenu_Click каталог или файл не выделен");
            }
            
        }
        public void eventTreeViewUnselect(object sender, RoutedEventArgs e)
        {
            UnselectEvent unselectEvent = new UnselectEvent(_mainWindow, _renameMainEvent);
            unselectEvent.Unselect(ref unSelectedFolderNodes, ref e);
        }


        public void eventTreeViewExpended(object sender, RoutedEventArgs e)
        {
            if (initializingMainForm.isLoadingInitializing() == false)
            {
                //для rename если после rename нажали на открытие нодов
                _renameMainEvent.hideRenameFolderNodes();
                TreeViewItem item = e.OriginalSource as TreeViewItem;
                _supportPart1MainForm.UpdateLazyLodf(item, "LazyTreeView");
            }
        }

        public void eventTreeViewCollapsed(object sender, RoutedEventArgs e)
        {
            TreeViewItem tvi = e.OriginalSource as TreeViewItem;
            CollapsedEvent collapsedEvent = new CollapsedEvent();
            collapsedEvent.collapsed(tvi);
        }

        public void eventTreeViewRenameClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("MainWindows->TreeViewRename_Click->Выбран из контекстного меню переименовать");
            _renameMainEvent.visibleRenameFolderNodes(ref oldFOlderName1);
        }

        public void eventTreeViewVisible(object sender, DependencyPropertyChangedEventArgs e)
        {
            TextBox renameTextbox = (TextBox)sender;
            VisibleTreeViewEvent visibleTreeViewEvent = new VisibleTreeViewEvent(ref oldFOlderName1, _mainWindow, _updateMainForm);
            visibleTreeViewEvent.visible(renameTextbox, e);


        }

        public void eventMainWindowsMouseDown_click(object sender, MouseButtonEventArgs e)
        {
            Console.WriteLine("MainWIndows->MainWindowsMouseDown_Click: Клик срабатывания на всей форме");
            _renameMainEvent.hideRenameFolderNodes();
        }

        public void eventMainWindowsKeyEnter()
        {
            Console.WriteLine("MainWIndows->MainWindowsMouseDown_Click: Клик срабатывания на всей форме");
            _renameMainEvent.hideRenameFolderNodes();
        }

        public void eventTreeViewFolderNodeLostFocus(object sender, RoutedEventArgs e)
        {
            _renameMainEvent.hideRenameFolderNodes();

        }

        public void eventTreeViewItemSelected(object sender, RoutedEventArgs e)
        {
            //идет загрузка узлов
            if (initializingMainForm.isLoadingInitializing() == false)
            {
                //происходит когда мы делаем селект мышкой, а не через код
                if (!insert.isSelectBack)
                {
                    if(!insert.isSelectForward)
                    {
                        isDisabledButtonBack(false);
                        insert.index = -1;
                        isDisabledButtonForward(true);
                    }
                   
                }
               
                _selectEvent.Select(ref insert, e, _viewModel.getListSelectNodes(), ref unSelectedFolderNodes);
               
            }
        }

       


        public void eventOpenViewTransferPages(object sender, RoutedEventArgs e)
        {
            FolderNodes item = _mainWindow.treeView1.SelectedItem as FolderNodes;
            if (item != null)
            {
                _mainWindow.treeView1.Focus();
                item.IsSelected = false;
            }
            _mainWindow.FrameViewFolder.Navigate(_viewTransfer);
        }


        public void eventScanClear(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            _clearScanEvent.start(menuItem);
        }

        public void changeIconMenuClearScan(object sender, ArgsDataChangeIconMenuViewModel e)
        {
            _clearScanEvent.changeIconMenuClearScan( sender,  e);
        }

        public void eventAutoScan(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            AutoScanEvent autoScanEvent = new AutoScanEvent(_cancellationController);
            autoScanEvent.start(menuItem);
        }

        public void eventButtonClickBack(object sender, RoutedEventArgs e)
        {
            _navigationMainController.StartBack(ref insert, _viewModel.getListSelectNodes(), this);
        }

        public void eventButtonClickForward(object sender, RoutedEventArgs e)
        {
            _navigationMainController.StartForward(ref insert, _viewModel.getListSelectNodes() , this);
        }

        public void eventOpenSettingWindow(object sender, RoutedEventArgs e)
        {
            SystemSettingEvent settingWindowEvent = new SystemSettingEvent(_sqlliteController, _viewModelViewFolder, _viewModel , _blockingEventController , _cc);
            settingWindowEvent.InjectAutoSaveFolderObj(_uploadClick, _wdce , _cfc , _exceptionController , _firstSyncController);
            settingWindowEvent.InjectSendTimeObj(_sendJson , _soup);

            settingWindowEvent.openWindowsSystemSetting(_convertIcon , _mainWindow , _exceptionController);
        }

        public void eventOpenErrorAlert()
        {
            ErrorAlertModelObj errorAlertModelObj = new ErrorAlertModelObj();
            errorAlertModelObj._convertIcon = _convertIcon;
            errorAlertModelObj._sendJson = _sendJson;

            ErrorAlertWindowViewModel _viewModel = new ErrorAlertWindowViewModel();
            _viewModel.NameWindow = "Отчет о работе";
            _viewModel.ErrorText = "Уведомление для разработчиков!";
            _viewModel.ImageAttention = _convertIcon.getIconType(".attention");

            ErrorAlertWindow errorAlert = new ErrorAlertWindow(errorAlertModelObj , _viewModel);
            errorAlert.Show();
        }


        public void eventOpenChangeUser(object sender, RoutedEventArgs e)
        {
            ChangeUserEventModel changeModel = createChangeEventModel();
            ChangeUserEvent changeEvent = new ChangeUserEvent(changeModel);
            changeEvent.change(_mainWindow);
        }

        public void eventWindowsState(object sender, EventArgs e)
        {
            WindowsStateEvent winStateEvent = new WindowsStateEvent(_mainWindow , _viewModel);
            winStateEvent.Window_StateChanged( sender,  e);
        }

        public void eventAutoUpdater()
        {
            AutoUpdaterEvent autoUdaterEvent = new AutoUpdaterEvent();
            autoUdaterEvent.startAutoUpdater();
        }


        //отключает кнопку назад или вкл
        public void isDisabledButtonBack(bool flag)
        {
            hideButton(flag, _mainWindow.ImgNavigationBack, _mainWindow.backButton);
        }

        //отключает кнопку вперед или вкл
        public void isDisabledButtonForward(bool flag)
        {
            hideButton(flag, _mainWindow.ImgNavigationForward, _mainWindow.forwardButton);
        }


        private void hideButton(bool flag , Image imageButton , Button button)
        {

            if (flag)
            {
                if(button.IsEnabled)
                {
                    imageButton.Opacity = 0.4;
                    button.IsEnabled = false;
                    button.Opacity = 0.6;
                }
               
            }
            else
            {
                if (!button.IsEnabled)
                {
                    imageButton.Opacity = 1;
                    button.IsEnabled = true;
                    button.Opacity = 1;
                }
                    
            }
        }

        private ChangeUserEventModel createChangeEventModel()
        {
            ChangeUserEventModel model = new ChangeUserEventModel();

            model._blockingEventController = _blockingEventController;

            model._mqClientController = _mqClientController;
            model._sqlliteController = _sqlliteController;
            model._viewModel = _viewModel;
            model._viewModelViewFolder = _viewModelViewFolder;
            model._firstSyncController = _firstSyncController;
            model._webDavClient = _webDavClient;
            model._updateForm = _updateForm;
            model._mqClient = _mqClient;
            model._loginFormViewModel = _loginFormViewModel;
            model._cancellationController = _cancellationController;
            return model;
        }
        private SelectEventModel createSelectModel(MainWindow _mainWindow , MainWindowViewModel _viewModel , NavigationMainWindowsController _navigationMainController , SupportEventPart1MainForm _supportEventPart1MainForm , SupportEventPart2MainForm _supportEventPart2MainForm , RenameMainEvent _renameMainEvent)
        {
            SelectEventModel selectModel = new SelectEventModel();
            selectModel._mainWindow = _mainWindow;
            selectModel._viewModel = _viewModel;
            selectModel._navigationMainController = _navigationMainController;
            selectModel._supportEventPart1MainForm = _supportEventPart1MainForm;
            selectModel._supportEventPart2MainForm = _supportEventPart2MainForm;
            selectModel._renameMainEvent = _renameMainEvent;

            return selectModel;
        }

     
    }
}
