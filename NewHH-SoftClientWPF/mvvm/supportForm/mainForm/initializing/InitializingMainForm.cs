﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.taskBar;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.initializing;
using NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm
{
    public class InitializingMainForm
    {
        private InitializingMainFormModel _objForm;

        public  InitializingMainForm(InitializingMainFormModel objForm)
        {
            _objForm = objForm;
        }

        public bool isLoadingInitializing()
        {
            return _objForm.isLoadingInitializing;
        }

        public void MainWindow_load(object sender, EventArgs e)
        {
            addAdminPriv();
            _objForm.isLoadingInitializing = true;
            createSystemSetting();
            ThreadPool.SetMinThreads(staticVariable.Variable.minThreadPool, staticVariable.Variable.minThreadPool);
            createRootTreeViewItems();
            initializingContextMenuTreeView();


            //оповещаем что окно постоено для клиента mq(пора начинать синхронизацию)
            _objForm._viewModel.isStartMainWindows = true;

            //Устанавилваем картинки трем кнопка - В облако/Создать ссылку/Сох. На компьютере
            _objForm._mainWindow.ImageButtonInCloud.Source = _objForm._convertIcon.getIconType(".buttonCloud");
            _objForm._mainWindow.Transfer.Source = _objForm._convertIcon.getIconType(".buttonSync");
            _objForm._mainWindow.ImageButtonCreateLink.Source = _objForm._convertIcon.getIconType(".buttonLink");
            _objForm._mainWindow.ImageButtonSavePC.Source = _objForm._convertIcon.getIconType(".buttonCloud");
            _objForm._mainWindow.ImgNavigationBack.Source = _objForm._convertIcon.getIconType(".buttonNavigationBack");
            _objForm._mainWindow.ImgNavigationForward.Source = _objForm._convertIcon.getIconType(".buttonNavigationForward");
            _objForm._mainWindow.ChangeUsers.Source = _objForm._convertIcon.getIconType(".menuItemChangeUser");
            _objForm._mainWindow.ExitProgramm.Source = _objForm._convertIcon.getIconType(".menuItemExitProgramm");
            _objForm._mainWindow.SettingIcon.Source = _objForm._convertIcon.getIconType(".menuItemSystemSetting");
            _objForm._mainWindow.SettingAllScan.Source = _objForm._convertIcon.getIconType(".menuItemAllScan");
            _objForm._mainWindow.StartClearScan.Source = _objForm._convertIcon.getIconType(".contextMenuStart");
            _objForm._mainWindow.StartAutoScan.Source = _objForm._convertIcon.getIconType(".contextMenuStart");
            _objForm._viewModel.SetNewIconTaskBar(_objForm._convertIcon.streamToIcon(".notifyIconBarOk"));

            //При запуске скрываем NotifyIcon
            _objForm._viewModel.TaskBarIconVisability = Visibility.Collapsed;
            _objForm._viewModel.TaskBarDoubleClick = new TaskBarDoubleClickCom(_objForm._mainWindow , _objForm._viewModel);

            _objForm._supportEventPart2MainForm.isDisabledButtonBack(true);
            _objForm._supportEventPart2MainForm.isDisabledButtonForward(true);

            _objForm.isLoadingInitializing = false;
            //Disk всегда имеет id -1 выбираем ее при запуске приложения
            _objForm._viewModel.ActiveNodeID[0] = -1;
            checkLocationDownload(_objForm._sqlLiteController);

            //очистка временной таблице если программа была завершена аварийно
            _objForm._sqlLiteController.clearTableslistFilesTemp();
            _objForm._sqlLiteController.clearTablesPcTemp();
            setSizeDisk();



        }
        
       
        private void setSizeDisk()
        {
            Task.Run(() => 
            {
                InfoSqlSize infoSql = new InfoSqlSize(_objForm._sqlLiteController);
                App.Current.Dispatcher.Invoke(new System.Action(() => _objForm._viewModel.PgSizeDisk = infoSql.getSize()));
                SqliteController sqliteController = _objForm._sqlLiteController;
                injectAutoStartApplication(_objForm._convertIcon ,  null ,  sqliteController);
            });
           
        }
        private void injectAutoStartApplication(ConvertIconType convertIcon , ExceptionController error , SqliteController _sqlLiteController)
        {
            EventAutoStartApplication eventAutoStart = new EventAutoStartApplication(convertIcon, error);
            eventAutoStart.clickCheckBoxAutoStart(ref _sqlLiteController, getAutoStartApplcationChecked(_sqlLiteController));
        }

        private bool getAutoStartApplcationChecked(SqliteController _sqlLiteController)
        {
           return  _sqlLiteController.getSelect().getAutoStartApplication();
        }
        private void createSystemSetting()
        {
            if(_objForm._sqlLiteController.getSelect().existSystemSetting())
            {
                int autoScanMill = Convert.ToInt32(_objForm._sqlLiteController.getSelect().getSystemSettingAutoScan());
                staticVariable.Variable.startMillisecondAutoSync = Convert.ToInt32(autoScanMill);
            }
            else
            {
                _objForm._sqlLiteController.getInsert().insertAutoScanTimemill(4000000.ToString());
                staticVariable.Variable.startMillisecondAutoSync = 4000000;
            }
        }
        private void initializingContextMenuTreeView()
        {
            InitizlizingMainFormContextMenu contextMenu = new InitizlizingMainFormContextMenu(_objForm._mainWindow);
            contextMenu.Initializing(_objForm._mainWindowsContextMenu , _objForm._convertIcon);
        }


        private void checkLocationDownload(SqliteController sqliteController)
        {
            string locationDownload = sqliteController.getSelect().getLocationDownload();
            

            if (locationDownload.Equals("") | locationDownload == null)
            {
                string defaultDownloadPath = getDefaultPathLocationDownload();
                sqliteController.getInsert().insertLocationDownload(defaultDownloadPath);
            };
        }

        private string getDefaultPathLocationDownload()
        {
            string defaultDownloadPath = staticVariable.Variable.defaultPathDownload+"\\" + staticVariable.Variable.defaultPathNameDownload;
            if (!isExistFolder(defaultDownloadPath)) createFolder(defaultDownloadPath);
            return defaultDownloadPath;
        }

        private bool isExistFolder(string directory)
        {
            return Directory.Exists(directory);
        }

        private void  createFolder(string directory)
        {
            Directory.CreateDirectory(directory);
        }

        private void createRootTreeViewItems()
        {
            _objForm._updateMainForm.updateMainInitializingTreeView();
        }

        private void addAdminPriv()
        {
            //пока отключаем возможно нам понадобится код для запуска с правами админа
            //bool isRole = IsRunningAsAdministrator();
            //if (!isRole)
            //{
               // string c = Assembly.GetEntryAssembly().CodeBase;
          
               // ProcessStartInfo processStartInfo = new ProcessStartInfo("D:/c#/source/repos/NewHH-SoftClientWPF/NewHH-SoftClientWPF/NewHH-SoftClientWPF/NewHH-SoftClientWPF/bin/Debug/NewHH-SoftClientWPF.exe");

          
                //processStartInfo.UseShellExecute = true;
                //processStartInfo.Verb = "runas";

          
                //Process.Start(processStartInfo);


                //Environment.Exit(0);
           // }


        }

        /// <summary>
        /// Function that check's if current user is in Aministrator role
        /// </summary>
        /// <returns></returns>
        public static bool IsRunningAsAdministrator()
        {
            // Get current Windows user
            WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();

            // Get current Windows user principal
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(windowsIdentity);

            // Return TRUE if user is in role "Administrator"
            return windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    

       
    }
}
