﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.transfer
{
    public class InCloudEventFolder
    {

        private ButtonUploadClick _uploadButton;
        private ExceptionController _exceptionController;

        public InCloudEventFolder(ButtonUploadClick _uploadButton, ExceptionController exceptionController)
        {
            this._uploadButton = _uploadButton;
            _exceptionController = exceptionController;
        }

        public void inCloudFolder()
        {
            IOpenDialog dialog = _uploadButton.CreateFolderDialog();

            if (dialog.FolderPath != null)
            {
                try
                {
                    string[] selectHref = dialog.FolderPath;

                    FileInfoModel pasteModel = _uploadButton.getPasteModel();
                    List<FileInfoModel> parentListPasteModel = _uploadButton.getChildrenPasteModel();
                    bool check = _uploadButton.isDublication(selectHref, parentListPasteModel);

                    _uploadButton.startTrainingUpload(check, selectHref, pasteModel);
                }
                catch (System.IO.DirectoryNotFoundException ex)
                {
                    Console.WriteLine("InCloudEventFolder->inCLoud: критическая ошибка загрузки файлов " + ex.ToString());
                    _exceptionController.sendError((int)CodeError.FILE_SYSTEM_EXCEPTION, "InCloudEventFolder->inCLoud: критическая ошибка загрузки файлов", ex.ToString());

                }


            }
        }
    }
}
