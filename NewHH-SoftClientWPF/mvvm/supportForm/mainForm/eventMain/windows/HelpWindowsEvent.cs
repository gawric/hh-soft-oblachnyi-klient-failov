﻿using NewHH_SoftClientWPF.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.windows
{
    public class HelpWindowsEvent
    {

        public void openWindowsHelp(MainWindow _mainWin)
        {
            HelpWindow helpWin = new HelpWindow();
            _mainWin.Topmost = false;
            helpWin.ShowDialog();
        }
    }
}
