﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.windows
{
    public class SystemSettingEvent
    {
        private SystemSettingModelObj _ssmo;

        public SystemSettingEvent(SqliteController sqliteController , ViewFolderViewModel viewModelViewFolder , MainWindowViewModel viewModel , BlockingEventController bec , CancellationController _cc)
        {
            _ssmo = new SystemSettingModelObj();
            _ssmo._sqliteController = sqliteController;
            _ssmo._viewModelMain = viewModel;
            _ssmo._viewModelViewFolder = viewModelViewFolder;
            _ssmo._blockingEventController = bec;
            _ssmo._cc = _cc;
        }

        public void InjectAutoSaveFolderObj(ButtonUploadClick uploadClick , WebDavClientExist wdce , CreateFolderController cfc , ExceptionController excepc , FirstSyncFilesController fsfc)
        {
            _ssmo._uploadClick = uploadClick;
            _ssmo._wdce = wdce;
            _ssmo._cfc = cfc;
            _ssmo._excepc = excepc;
            _ssmo._fsfc = fsfc;
        }

        public void InjectSendTimeObj(UpdateNetworkJsonController unjc , StackOperationUpDown soup)
        {
            _ssmo._unjc = unjc;
            _ssmo._soup = soup;
        }
        public void openWindowsSystemSetting(ConvertIconType convertIcon , MainWindow _mainWin , ExceptionController _exceptionController)
        {
            _ssmo._convertIcon = convertIcon;
            _ssmo._error = _exceptionController;
            _mainWin.Topmost = false;
            SystemSettingWindow windowsSetting = new SystemSettingWindow(_ssmo);
            windowsSetting.ShowDialog();
        }
    }
}
