﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.windows
{
    public class ExitWindowsEvent
    {
        MainWindowViewModel _mainViewModel;

        public ExitWindowsEvent(MainWindowViewModel mainModel)
        {
            _mainViewModel = mainModel;
        }

        public void exitClickButton(MainWindow main)
        {
            main.Close();
            exit(main);
        }

        public void exitClickForm(MainWindow main)
        {
            exit(main);
        }

        private void exit(MainWindow main)
        {
            Task.Run(() => Environment.Exit(0));
            _mainViewModel.TaskBarIconVisability = Visibility.Collapsed;
            main.taskBarIcon.Dispose();
        }
    }
}
