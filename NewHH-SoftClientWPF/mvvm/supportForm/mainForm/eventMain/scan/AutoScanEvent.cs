﻿using NewHH_SoftClientWPF.contoller.cancellation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class AutoScanEvent
    {
        private CancellationController _cancellationController;

        public AutoScanEvent(CancellationController _cancellationController)
        {
            this._cancellationController = _cancellationController;
        }

        public void start(MenuItem menuItem)
        {
            if (isStartAutoScan(menuItem))
            {
                //Здесь не реализован ручной запуск автосканирования
                //т.к он запускается автоматически!!! в цикле

            }
            else
            {
                _cancellationController.startCansel(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID);

            }
        }


        private bool isStartAutoScan(MenuItem menuItem)
        {
            bool isStart = false;

            if (menuItem.Header.Equals(staticVariable.IconVariable.textMenuStartAutoScan))
            {
                isStart = true;
            }

            return isStart;
        }
    }
}
