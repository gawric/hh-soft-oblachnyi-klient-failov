﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.taskBar
{
    public class TaskBarDoubleClickCom : ICommand
    {
        private MainWindow _mainWin;
        private MainWindowViewModel _viewModel;
        public event EventHandler CanExecuteChanged;

        public TaskBarDoubleClickCom(MainWindow mainWin  , MainWindowViewModel viewModel)
        {
            this._mainWin = mainWin;
            this._viewModel = viewModel;
        }

      

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _mainWin.WindowState = WindowState.Normal;
            _viewModel.TaskBarIconVisability = Visibility.Collapsed;
            _mainWin.ShowInTaskbar = true;
            _mainWin.Topmost = true;
        }
    }
}
