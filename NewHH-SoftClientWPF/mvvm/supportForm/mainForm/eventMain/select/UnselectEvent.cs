﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class UnselectEvent
    {
        private SupportEventPart2MainForm supportEventPart2MainForm;
        private MainWindow _mainWindow;
        private RenameMainEvent _renmeMainEvent;

        public UnselectEvent(MainWindow _mainWindow , RenameMainEvent _renmeMainEvent)
        {
           
            this._mainWindow = _mainWindow;
            this._renmeMainEvent = _renmeMainEvent;
        }

        public void Unselect(ref FolderNodes unSelectedFolderNodes ,ref  RoutedEventArgs e)
        {
            //для rename если после rename нажали на открытие нодов
            _renmeMainEvent.hideRenameFolderNodes();
            _mainWindow.treeView1.Tag = e.OriginalSource;

            TreeViewItem tvi = _mainWindow.treeView1.Tag as TreeViewItem;
            unSelectedFolderNodes = (FolderNodes)tvi.DataContext;
        }
    }
}
