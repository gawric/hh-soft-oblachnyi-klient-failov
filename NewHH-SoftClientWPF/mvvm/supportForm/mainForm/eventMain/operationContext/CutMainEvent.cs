﻿using NewHH_SoftClientWPF.contoller.statusWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class CutMainEvent
    {
        private CopyStatusController _copyStatus;
        private MainWindowViewModel _viewModel;
        private MainWindow _mainWindow;
        private SupportEventPart1MainForm _supportEventPart1MainForm;

        public CutMainEvent(CopyStatusController _copyStatus , MainWindowViewModel _viewModel, MainWindow _mainWindow , SupportEventPart1MainForm _supportEventPart1MainForm)
        {
            this._copyStatus = _copyStatus;
            this._viewModel = _viewModel;
            this._mainWindow = _mainWindow;
            this._supportEventPart1MainForm = _supportEventPart1MainForm;
        }

        public void cut()
        {
            _viewModel.setListCopyNodes(null);
            _viewModel.CopyAndCutRootLocation = "";

            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                _copyStatus.isMove = true;
                _copyStatus.isMoveNetwork = true;

                FolderNodes allCopyFolder = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                _supportEventPart1MainForm.setListCopyNodes(allCopyFolder);


                for (int l = 0; l < _viewModel.getListCopyNodes().Count; l++)
                {
                    Console.WriteLine("Добавили: ViewFolder-> CutContextMenu_Click " + _viewModel.getListCopyNodes()[l].FolderName);
                }
            }
            else
            {
                Console.WriteLine("CutContextMenu_Click каталог или файл не выделен");
            }
        }
    }
}
