﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other
{
    public class CollapsedEvent
    {
        public void collapsed(TreeViewItem tvi)
        {
            if(tvi != null)
            {
                FolderNodes rootFolder = (FolderNodes)tvi.DataContext;
                rootFolder.Children.Clear();
                rootFolder.Children.Add(getLoadingNodes());
               // Console.WriteLine("Event Collapsed Activate!");
            }
           
        }

        private FolderNodes getLoadingNodes()
        {
            FolderNodes loadNodes = new FolderNodes();
            loadNodes.FolderName = "Загрузка";
            loadNodes.Type = "txt";

            return loadNodes;
        }
    }
}
