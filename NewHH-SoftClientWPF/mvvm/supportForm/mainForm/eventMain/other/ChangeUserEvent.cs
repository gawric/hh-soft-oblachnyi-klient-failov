﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.loginWindow;
using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other
{
    public class ChangeUserEvent
    {
        private ChangeUserEventModel _changeUserEventModel;

        public ChangeUserEvent(ChangeUserEventModel changeUserEventModel)
        {
            _changeUserEventModel = changeUserEventModel;
        }

        public void change(MainWindow _mainWin)
        {
            var t = Task.Run(() => {clearDataViewModel();closeActiveMqConnection(); stopScanner(); });
            _mainWin.Topmost = false;
            LoginForm login = new LoginForm(convertChangeUserEventModelToLoginFormModel(_changeUserEventModel));
            login.Show();
        }

        private void closeActiveMqConnection()
        {
            _changeUserEventModel._mqClientController.closeMqClient();
            _changeUserEventModel._mqClientController.closeListerner();
        }
        private void stopScanner()
        {
            _changeUserEventModel._cancellationController.startAllCansel();
        }
        private void clearDataViewModel()
        {
            
            _changeUserEventModel._sqlliteController.clearTableslistFiles();
            _changeUserEventModel._sqlliteController.clearTableslistFilesTemp();
            _changeUserEventModel._sqlliteController.clearTablesPcTemp();
            App.Current.Dispatcher.Invoke(new System.Action(() => _changeUserEventModel._viewModel.NodesView.Items.Clear()));
            App.Current.Dispatcher.Invoke(new System.Action(() => _changeUserEventModel._viewModelViewFolder.NodesView.Items.Clear()));
            
        }

        private LoginWindowModel convertChangeUserEventModelToLoginFormModel(ChangeUserEventModel _changeUserEventModel)
        {
            LoginWindowModel loginFormModel = new LoginWindowModel();
            loginFormModel.sqliteControler = _changeUserEventModel._sqlliteController;
            loginFormModel._blockingEventController = _changeUserEventModel._blockingEventController;
            loginFormModel._firstSyncController = _changeUserEventModel._firstSyncController;
            loginFormModel._webDavClient = _changeUserEventModel._webDavClient;
            loginFormModel.updateForm = _changeUserEventModel._updateForm;
            loginFormModel._mqClient = _changeUserEventModel._mqClient;
            loginFormModel._loginFormViewModel = _changeUserEventModel._loginFormViewModel;
            loginFormModel._mainViewModel = _changeUserEventModel._viewModel;
            return loginFormModel;

        }
    }
}
