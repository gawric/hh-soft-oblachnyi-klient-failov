﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm
{
    public class CopyMainWindowViewModel : INotifyPropertyChanged
    {
        private CopyViewModel copyModel;

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler OnCloseWindowsCopy;
        public event EventHandler OnOpenWindowsCopy;

        public CopyMainWindowViewModel()
        {
            copyModel = new CopyViewModel();
            
        }


        public string title
        {
            get { return copyModel.title; }
            set
            {
                if (copyModel.title != value)
                {
                    copyModel.title = value;
                    OnPropertyChange("title");

                }
            }
        }

        public string status
        {
            get { return copyModel.StatusWorker; }
            set
            {
                if (copyModel.StatusWorker != value)
                {
                    copyModel.StatusWorker = value;
                    OnPropertyChange("status");

                }
            }
        }



        public string where
        {
            get { return copyModel.where; }
            set
            {
                if (copyModel.where != value)
                {
                    copyModel.where = value;
                    OnPropertyChange("where");
                }
            }
        }

        public string whence
        {
            get { return copyModel.whence; }
            set
            {
                if (copyModel.whence != value)
                {
                    copyModel.whence = value;
                    OnPropertyChange("whence");
                }
            }
        }

        public double pg
        {
            get { return copyModel.pg; }
            set
            {
                copyModel.pg = value;
                OnPropertyChange("pg");
            }
        }

        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        //Закрывает окно
        public void CloseWindowsCopy()
        {
            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(new System.Action(() => OnCloseWindowsCopy(this, new EventArgs())));
            }
                
        }

        public void OpenWindowsCopy()
        {
            if(App.Current != null)
            {
                App.Current.Dispatcher.Invoke(new System.Action(() => OnOpenWindowsCopy(this, new EventArgs())));
            }
            
        }

    }
}
