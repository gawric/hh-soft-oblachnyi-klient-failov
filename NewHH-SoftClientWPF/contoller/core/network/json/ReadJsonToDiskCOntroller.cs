﻿using NewHH_SoftClientWPF.mvvm.model;
using Newtonsoft.Json;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.json
{
    public class ReadJsonToDiskController
    {
       
        public FileInfoModel[] readLazyJsonArray(string tempHref)
        {

            return JsonConvert.DeserializeObject<FileInfoModel[]>(File.ReadAllText(tempHref));
        }

        public void deleteTempFiles(string tempHref)
        {

            File.Delete(tempHref);
        }


    }
}
