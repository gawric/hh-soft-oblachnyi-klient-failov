﻿using NewHH_SoftClientWPF.mvvm.model;
using Newtonsoft.Json;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.json
{
    public class SaveJsonToDiskContoller
    {
        List<string> tempSendLazyHrefFiles;
        List<string> tempGetLazyHrefFiles;

        public SaveJsonToDiskContoller(Container cont){

            tempSendLazyHrefFiles = new List<string>();
            tempGetLazyHrefFiles = new List<string>();
        }

        public void saveLazyJsonArray(FileInfoModel[] saveObj){

            string fileName = Path.GetTempFileName();
            save(saveObj, fileName);

            tempSendLazyHrefFiles.Add(fileName);

        }

        public void saveGetLazyJsonArray(FileInfoModel[] saveObj){
            string fileName = Path.GetTempFileName();
            save(saveObj, fileName);
            //FileInfo s = new FileInfo(fileName);
            tempGetLazyHrefFiles.Add(fileName);
        }

        public List<string> getListSendLazyTempHref()
        {
            return tempSendLazyHrefFiles;
        }

        public void setListSendLazyTempHref(List<string> tempHrefList)
        {
            tempSendLazyHrefFiles = tempHrefList;
        }

        public void setListGetLazyTempHref(List<string> tempHrefList)
        {
            tempGetLazyHrefFiles = tempHrefList;
        }

        public List<string> getListGetLazyTempHref()
        {
            return tempGetLazyHrefFiles;
        }


        private void save(FileInfoModel[] saveObj, string fileName)
        {
            File.WriteAllText(fileName, JsonConvert.SerializeObject(saveObj));
        }


    }
}
