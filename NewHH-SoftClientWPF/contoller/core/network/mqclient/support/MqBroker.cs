﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.mqclient
{
    class MqBroker
    {

        protected AutoResetEvent semaphore = new AutoResetEvent(false);
        protected AutoResetEvent semaphoreLazy = new AutoResetEvent(false);
        protected  ITextMessage message = null;
        protected  TimeSpan receiveTimeout = TimeSpan.FromSeconds(10);
        private UpdateNetworkController updateNetworkController;
        private CancellationController _cancellationController;
        private ExceptionController _exceptionController;

        public MqBroker(CancellationController cancellationController , ExceptionController exceptionController)
        {
            _cancellationController = cancellationController;
            _exceptionController = exceptionController;
        }

        public void destroyAllListener()
        {
            _cancellationController.startCansel(staticVariable.Variable.listenerServerTokenId);
            semaphore.Set();
            semaphoreLazy.Set();
        }

        public async Task SendAsyncMessage(ISession session , string jsonObject)
        {
            await Task.Run(() =>
            {
                if(session != null)
                {
                    ITopic topic = SessionUtil.GetTopic(session, staticVariable.Variable.getSendTopic());

                    using (IMessageProducer producer = session.CreateProducer(topic))
                    {
                        try
                        {
                            producer.DeliveryMode = MsgDeliveryMode.Persistent;
                            producer.RequestTimeout = receiveTimeout;

                            // Send a message
                            ITextMessage request = session.CreateTextMessage(jsonObject);
                            request.NMSCorrelationID = "abc";
                            request.Properties["_type"] = staticVariable.Variable.getModelSendTopic();
                            producer.Send(request);
                            producer.Dispose();

                        }
                        catch(Apache.NMS.ActiveMQ.RequestTimedOutException a)
                        {
                            //Console.WriteLine("Ошибка MqBroker->SendAsyncMessage "+a.ToString());
                            generatedError((int)CodeError.NETWORK_ERROR, "MqBroker->SendAsyncMessage критическая ошибка", a.ToString());
                        }
                        
                   
                    }
                }
               
            });

                
        }

        public async Task SendAsyncLazyMessage(ISession session, string jsonObject)
        {
            await Task.Run(() =>
            {
                if (session != null)
                {
                    ITopic topic = SessionUtil.GetTopic(session, staticVariable.Variable.sendTopicLazy);

                    using (IMessageProducer producer = session.CreateProducer(topic))
                    {
                        producer.DeliveryMode = MsgDeliveryMode.Persistent;
                        producer.RequestTimeout = receiveTimeout;

                        // Send a message
                        ITextMessage request = session.CreateTextMessage(jsonObject);
                        request.NMSCorrelationID = "abc";
                        request.Properties["_type"] = staticVariable.Variable.getModelSendTopic();
                        producer.Send(request);
                        producer.Dispose();

                    }
                }

            });


        }

        //не реализован это просто копия
        public async Task SendAsyncMessageByte(ISession session, string byteMessage)
        {
            await Task.Run(() =>
            {
                if (session != null)
                {
                    ITopic topic = SessionUtil.GetTopic(session, staticVariable.Variable.getSendTopic());
                    using (IMessageProducer producer = session.CreateProducer(topic))
                    {
                        producer.DeliveryMode = MsgDeliveryMode.Persistent;
                        producer.RequestTimeout = receiveTimeout;

                        // Send a message
                        IStreamMessage request = session.CreateStreamMessage();

                        request.NMSCorrelationID = "abc";
                        request.Properties["_type"] = staticVariable.Variable.getModelSendTopic();
                        producer.Send(request);

                        IStreamMessage s = producer.CreateStreamMessage();
                       
                    }
                }

            });


        }

       
        private void createCanselation()
        {
            CancellationTokenSource canselSource = new CancellationTokenSource();
            _cancellationController.removeObjectWarehouse(staticVariable.Variable.listenerServerTokenId);
            _cancellationController.addWarehouse(staticVariable.Variable.listenerServerTokenId, canselSource);
        }
        //updateForm - обновляем форму прямо из нового потока
        //ISession - передаем открытую ссессию для подписание на событие
        //username - и так понятно
        public async Task  StartAsyncListener(string username , ISession session , UpdateNetworkController updateNetworkController, ActiveMqSubscribleModel activestatus)
        {
            this.updateNetworkController = updateNetworkController;
            createCanselation();

            await Task.Run(() =>
            {

                try
                {
                    if (session != null)
                    {
                        ITopic destination = SessionUtil.GetTopic(session, "topic://VirtualTopic.User." + username);
                        
                        using (IMessageConsumer consumer = session.CreateConsumer(destination))
                        {
                            consumer.Listener += new MessageListener(listernerMqServer);

                            while (true)
                            {
                                activestatus.isSubscrible = true;
                               
                                // Wait for the message
                                semaphore.WaitOne((int)receiveTimeout.TotalMilliseconds, true);
                                if (isStopListener()) break;

                                if (message == null)
                                {
                                    // Console.WriteLine("No message received!");
                                }
                                else
                                {
                                    Console.WriteLine("Получено обновление with ID:   " + message.NMSMessageId);
                                    Console.WriteLine("Текст сообщения with text: " + message.Text);


                                }
                            }


                        }

                    }
                }
                catch(Apache.NMS.NMSSecurityException a)
                {
                    //Console.WriteLine("MqBroker-> Ошибка подписки на обновления " + a);
                    generatedError((int)CodeError.NETWORK_ERROR, "MqBroker->StartAsyncListener критическая ошибка", a.ToString());
                }

               

               
            });
              
        }

        //updateForm - обновляем форму прямо из нового потока
        //ISession - передаем открытую ссессию для подписание на событие
        //username - и так понятно
        public async Task StartAsyncLazyListener(string username, ISession session, UpdateNetworkController updateNetworkController , ActiveMqSubscribleModel activestatus)
        {
            this.updateNetworkController = updateNetworkController;

            await Task.Run(() =>
            {
                if (session != null)
                {
                    ITopic destination = SessionUtil.GetTopic(session, "topic://VirtualTopic.Lazy.User." + username);



                    using (IMessageConsumer consumer = session.CreateConsumer(destination))
                    {
                        consumer.Listener += new MessageListener(listernerLazyMqServer);

                        while (true)
                        {
                            
                            // Wait for the message
                            semaphoreLazy.WaitOne((int)receiveTimeout.TotalMilliseconds, true);
                            if (isStopListener()) break;



                            if (message == null)
                            {
                                // Console.WriteLine("No message received Lazy !");
                            }
                            else
                            {
                                Console.WriteLine("Получено обновление with ID:   " + message.NMSMessageId);
                                Console.WriteLine("Текст сообщения with text: " + message.Text);


                            }
                        }


                    }

                }

            });

        }

        private bool isStopListener()
        {
            return _cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.listenerServerTokenId).IsCancellationRequested;
           
        }

        //Все события передаем в форму обновления Form 
        //что-бы обновить данные в нашей программе
        public void listernerMqServer(IMessage receivedMsg)
        {
            ITextMessage message = receivedMsg as ITextMessage;
            semaphore.Set();
            updateNetworkController.updateMqClientSubscrible(message.Text);
           
        }

        //Все события передаем в форму обновления Form 
        //что-бы обновить данные в нашей программе
        public void listernerLazyMqServer(IMessage receivedMsg)
        {
            ITextMessage message = receivedMsg as ITextMessage;
            semaphoreLazy.Set();
            updateNetworkController.updateMqClientSubscrible(message.Text);
        
        }
       

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }
    }
}
