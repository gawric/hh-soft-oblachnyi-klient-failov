﻿using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient
{
    public class WebDavClientExist
    {
       // private WebDavClientController _webDavController;
        private SqliteController _sqllite;
        private WebDavClient _webDavClient;
        private ExceptionController _exceptionController;
        public WebDavClientExist(Container cont)
        {
            //не инжектится вызывает ошибку приходится обрашатся напрямую к клиенту
           // _webDavController = cont.GetInstance<WebDavClientController>();

            _sqllite = cont.GetInstance<SqliteController>();
            _exceptionController = cont.GetInstance<ExceptionController>();
            _webDavClient = new WebDavClient(_exceptionController);

        }
        //true - есть папка на сервере
        public async Task<bool> isExist(string location)
        {
            Client client = getClientWebDav();
            bool isExist = false;

            Item itemFolder = null;

            try
            {
                
                itemFolder =  await client.GetFolder(location);

            }
            catch(WebDAVClient.Helpers.WebDAVException z)
            {
                Console.WriteLine("WebDavClientExist->isExist: Проверка ошибки webDavExist->isExist   Директория проверки "+ location);
                client = null;
            }

            if(itemFolder != null)
                isExist = true;

            return isExist;
        }



        //true - все ок
        public async Task<bool> isExistAllDirectory(List<string>listLocation)
        {
            return await isExistList(listLocation);
        }

        //возвращает список на удаление в базе
        public async Task<List<string>> getListExist(List<string> listLocation)
        {
            return await getList(listLocation);
        }


        //true - все ок
        private async Task<List<string>> getList(List<string> listLocation)
        {
            List<string> deleteList = new List<string>();
            deleteList = await searchDelete(deleteList, listLocation);

            //удаляет найденные и оставляет только на удаление!
            foreach (string deleteLocation in deleteList)
            {
                listLocation.Remove(deleteLocation);
            }

            return listLocation;
          
        }
        //отдает список найденных location
        private async Task<List<string>> searchDelete(List<string> deleteList , List<string> listLocation)
        {
            foreach (string location in listLocation)
            {
                bool result = await isExist(location);

                if (result)
                {
                    deleteList.Add(location);
                }

            }

            return deleteList;
        }

        //true - все ок
        private async Task<bool> isExistList(List<string> listLocation)
        {
            bool check = true;

            foreach (string location in listLocation)
            {
                if(!location.Equals(staticVariable.Variable.nameRootLocationDisk))
                {
                    bool result = await isExist(location);

                    if (!result)
                    {
                        check = false;
                        break;
                    }
                }
                
            }

            return check;
        }

        


        public Client getClientWebDav()
        {
            string[] username = getUsername();
            return _webDavClient.CreateNewConnect(username[0], username[1]);
        }
        private string[] getUsername()
        {
            return _sqllite.getSelect().GetUserSqlite();
        }
    }
}
