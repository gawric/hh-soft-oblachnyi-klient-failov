﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.sync;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    class WebDavGetAllList
    {
        bool[] countStartUpdate = { false };
        bool[] countStartBegin = { false };

        public void ClearBasesRescanWebDavAllListStart(WebDavGetAllListModel model)
        {
            try
            {


                int z = 0;

                while (true)
                {
                    //300 раз проверяем, что база данных очищена
                    if (z >= 300)
                    {
                        Console.WriteLine("WebDavClientController->getListWebDavAsync: Ошибка отправки данных серверу! База не была очищена за 6000ms");
                        break;
                    }
                    else
                    {
                        bool checkExists = model._sqlLiteController.getSelect().isExistsEmptyTableFileList();
                        //база пустая запускаемся
                        if (checkExists)
                        {
                            if (countStartUpdate[0] == false)
                            {

                                changeStopMenuItemView(model);
                                checkUpdateScan(model).Wait();

                                
                                break;
                            }

                        }
                        else
                        {
                            changeStopMenuItemView(model);
                            //запускам очистку базы
                            checkBeginScan(model.updateNetworkJsonController, model.newVersionBases);
                        }


                    }
                    z++;
                    Thread.Sleep(200);

                }

                countStartBegin[0] = false;
                countStartUpdate[0] = false;




            }
            catch(System.Net.Http.HttpRequestException a)
            {
                //не работает т.к вызов идет асинхронно
                throw;
            }
            catch (System.InvalidOperationException a)
            {
                //не работает т.к вызов идет асинхронно
                throw;
            }

        }

        private void changeStopMenuItemView(WebDavGetAllListModel model)
        {
            model._viewModel.OnChangeImage(staticVariable.IconVariable.iconMenuStartClearScan,
                                    staticVariable.IconVariable.iconTypeStop,
                                    staticVariable.IconVariable.itemMenuStartClearScan,
                                    staticVariable.IconVariable.textMenuStopClearScan);

        }

        private void changeStartMenuItemView(WebDavGetAllListModel model)
        {
            model._viewModel.OnChangeImage(staticVariable.IconVariable.iconMenuStartClearScan,
                                    staticVariable.IconVariable.iconTypeStart,
                                    staticVariable.IconVariable.itemMenuStartClearScan,
                                    staticVariable.IconVariable.textMenuStartClearScan);

        }

        private async Task checkUpdateScan(WebDavGetAllListModel model)
        {
            try
            {
                //запускался не больше 1 раза
                if (countStartUpdate[0] == false)
                {
                    countStartUpdate[0] = true;
                    model._allOperationModel.isClearScan = true;
                    //Каждые 1000 итераций отправляем данные на сервер с помьщью UpdateNetworkJsonController
                    FileInfoModel[] list = await model._webdavclient.getAllFilesWebDavAsync(model);

                    //завершаем сканирование и отправляет последние данные на сервер
                    model.updateNetworkJsonController.updateSendJsonENDPARTIES(list, model.newVersionBases);
                    model._allOperationModel.isClearScan = false;
                    changeStartMenuItemView(model);
                }

            }
            catch(System.Net.Http.HttpRequestException z)
            {
                Console.WriteLine("WebDavGetAllList->CheckUpdateScan " + z);
                model._viewModel.Titlename = "Ошибка нет подключения к WebDav серверу";

            }
            catch (System.NullReferenceException z)
            {
                Console.WriteLine("WebDavGetAllList->CheckUpdateScan " + z);
                model._viewModel.Titlename = "Ошибка нет подключения к WebDav серверу";

            }
            catch (WebDAVClient.Helpers.WebDAVException z)
            {
                Console.WriteLine("WebDavGetAllList->CheckUpdateScan " + z);
                model._viewModel.Titlename = "Ошибка webdavserver disk не найден";

                Thread viewerThread = new Thread(delegate ()
                {
                    generatedError(model._exceptionController, (int)CodeError.NETWORK_ERROR, "WebDavGetAllList->CheckUpdateScan Критическая ошибка WebDavServer disk не найден", z.ToString());
                    System.Windows.Threading.Dispatcher.Run();
                });
                viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
                viewerThread.Start();


            }


        }
        private void checkBeginScan(UpdateNetworkJsonController updateNetworkJsonController , long newVersionBases)
        {
            if (countStartBegin[0] == false)
            {
                countStartBegin[0] = true;
                try
                {
                    //Отправляем серваку отчистить базу данных
                    updateNetworkJsonController.updateSendJsonBEGINPARTIES(null, newVersionBases);
                }
                catch (System.IO.IOException of)
                {
                    Console.WriteLine(of.ToString());
                }

            }
        }

        public async Task NoClearBasesRescanWebDavAllListStart(AutoSyncToTimeObj _autoSynObj, IProgress<double> progress)
        {
            try
            {
                //если произошло аварийное завершение
                _autoSynObj.isStop = false;
                //если мы нашли дубли
                _autoSynObj.doubleHref = new Dictionary<long, string>();

                // исполняем 2 запроса 
                // PASTE - 1 запрос добавление и изменение
                // PASTE - 2 запрос на удаление

                Dictionary<long, long> sqlArray = getAllRowsId(_autoSynObj);
                Dictionary<string, long> sqlArrayHref = getAllLocation(_autoSynObj , _autoSynObj.doubleHref);
                deleteAllDoubleHref(_autoSynObj.doubleHref, sqlArray, sqlArrayHref);

                //часть 1
                await ScanAndSendServerAllList( _autoSynObj, progress, sqlArray, sqlArrayHref);

                checkCancellation( _autoSynObj, sqlArray, sqlArrayHref);

                //удаляем value уже не понадобится
                long[] sqlArrayConvert = sqlArray.Keys.ToArray();

                clearTempList(sqlArrayHref, sqlArray);


                //isStop - по неизвесной причине остановили сканирование
                if (!_autoSynObj.isStop)
                {
                    //часть 2
                    await DeleteWebDavAndSendServer(_autoSynObj, progress, sqlArrayConvert);
                }


                
                sqlArrayConvert = null;

            }
            catch (System.IndexOutOfRangeException z)
            {
                throw;
            }

            
           
        }

        private void deleteAllDoubleHref(Dictionary<long, string> listDouble , Dictionary<long, long> sqlArray , Dictionary<string, long> sqlArrayHref)
        {
            foreach (KeyValuePair<long, string> kvp in listDouble)
            {
                
                if (sqlArrayHref.ContainsKey(kvp.Value))
                {
                    Console.WriteLine("WebDavGETALLLIST->NoClearBasesRescanWebDavAllListStart: Удалил дубликат из sqlArrayHref что-бы заново его пересоздать");
                    sqlArrayHref.Remove(kvp.Value);
                }
                    
            }
        }
        private void clearTempList(Dictionary<string, long> sqlArrayHref, Dictionary<long, long> sqlArray)
        {
            //чистка
            sqlArrayHref.Clear();
            sqlArray.Clear();
            sqlArrayHref = null;
            sqlArray = null;
        }
        private Dictionary<long, long> getAllRowsId(AutoSyncToTimeObj _autoSynObj)
        {
            //очень затратная операция в плане ram - 70 000 row_id будет стоить 15 мб памяти
            //получает все row_id из базы данных
            return  _autoSynObj._sqlLiteController.getSelect().getAllRow_id_long();

        }

        private Dictionary<string, long> getAllLocation(AutoSyncToTimeObj _autoSynObj , Dictionary<long, string> listDoubleHref)
        {
            //очень затратная операция в плане ram - 70 000 row_id будет стоить 15 мб памяти
            //получает все row_id из базы данных
            return _autoSynObj._sqlLiteController.getSelect().getAllHref(listDoubleHref);

        }


        private void checkCancellation(AutoSyncToTimeObj _autoSynObj , Dictionary<long, long> sqlArray , Dictionary<string, long> sqlArrayHref)
        {
          
            if (_autoSynObj._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID).IsCancellationRequested)
            {
                sqlArray.Clear();
                sqlArrayHref.Clear();
                sqlArray = null;
                sqlArrayHref = null;
            }

        }

        private async Task ScanAndSendServerAllList(AutoSyncToTimeObj _autoSynObj, IProgress<double> progress, Dictionary<long, long> sqlArray , Dictionary<string, long> sqlArrayHref)
        {
            try
            {
                staticVariable.Variable.allCountAutoScanInfo = 0;
                //часть 1
                _autoSynObj._updateNetworkJsonController.updateSendJsonLAZYSINGLBEGINUPDATEPARTIES(null, _autoSynObj.version);
                CancellationToken cancellation = _autoSynObj._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID);

                //Каждые 2500 итераций отправляем данные на сервер с помьщью UpdateNetworkJsonController
                // FileInfoModel[] list = await _webdavclient.getAllFilesWebDavAsyncNoDelete(newVersionBases, updateNetworkJsonController, progress, username, supportTopicModel, _sqlLiteController, sqlArray , _cancellationToken);

                FileInfoModel[] list = await new DemoSteckWalker().TraverseTree(_autoSynObj, _autoSynObj.version, progress, _autoSynObj.usernameAndPassword, sqlArray, sqlArrayHref, cancellation, _autoSynObj.doubleHref);

                if (cancellation.IsCancellationRequested)
                {

                    clearData(list, sqlArray, sqlArrayHref);
                    sendStopParties(_autoSynObj);

                }


                //завершаем сканирование и отправляет последние данные на сервер
                _autoSynObj._updateNetworkJsonController.updateSendJsonLAZYSINGLENDUPDATEPARTIES(list, _autoSynObj.version);
                list = null;
            }
            catch(System.IndexOutOfRangeException s)
            {
                Console.WriteLine("WebDavGetAllList->ScanAndSendServerAllList: критическая ошибка "+s.ToString());
            }
           
        }








        private void clearData(FileInfoModel[] list , Dictionary<long, long> sqlArray, Dictionary<string, long> sqlArrayHref)
        {
            sqlArray.Clear();
            sqlArrayHref.Clear();

            list = null;
        }

        private void sendStopParties(AutoSyncToTimeObj _autoSynObj)
        {
            _autoSynObj._updateNetworkJsonController.updateSendJsonLAZYSINGLUPDATEPARTIESTOP(null, _autoSynObj.version);
        }







        private NetworkException generatedError(ExceptionController _exceptionController , int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }




        private async Task DeleteWebDavAndSendServer(AutoSyncToTimeObj _autoSynObj , IProgress<double> progress, long[] sqlArray)
        {
            //часть 2
            _autoSynObj._updateNetworkJsonController.updateSendJsonLAZYSINGLBEGINUPDATEPARTIES(null, _autoSynObj.version);

            FileInfoModel[] listDelete = null;

            if (sqlArray != null)
            {
                //отправка на удаление
                listDelete = await _autoSynObj._webDavClient.getWebDavClient().sendWebDavDeleteNotExistsFiles(_autoSynObj.version, _autoSynObj._updateNetworkJsonController, progress, _autoSynObj._supportTopicModel, _autoSynObj._sqlLiteController, sqlArray);
                sqlArray = null;
            }


            //завершаем сканирование и отправляет последние данные на сервер
            _autoSynObj._updateNetworkJsonController.updateSendJsonLAZYSINGLENDUPDATEPARTIES(listDelete, _autoSynObj.version);
            
            listDelete = null;
        }
    }
}
