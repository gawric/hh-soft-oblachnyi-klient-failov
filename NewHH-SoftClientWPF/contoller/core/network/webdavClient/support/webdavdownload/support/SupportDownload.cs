﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload.support
{
    public class SupportDownload
    {
        //поиск родителей от начальной root до конца
        public FileInfoModel[] getPathRootToNodes(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            //поиск пути до нужной папки
            List<FileInfoModel> listAllPath = getPath(model._navigationController, downloadNodes.Row_id);
            FileInfoModel[] arr2 = convertListToArr(listAllPath);
            listAllPath = null;

            //реверс
            new SortedArrayToRevers().sortedArrayFileInfoToRevers(arr2);

            return arr2;
        }

        private FileInfoModel[] convertListToArr(List<FileInfoModel> allPath)
        {

            return allPath.ToArray();
        }


        private List<FileInfoModel> getPath(NavigationMainWindowsController _navigationController, long row_id_download_nodes)
        {
            List<FileInfoModel> listAllPath = new List<FileInfoModel>();
            //ищет путь до папки, которая вызывала действие 
            _navigationController.getPathViewFolder(row_id_download_nodes, listAllPath);

            return listAllPath;
        }

        public void createFoldersToLocalFolder(DownloadWebDavModel model , FileInfoModel[] arrPath, string locationDownload, bool isFiles)
        {
            string[] hrefParent = { "" };

            for (int d = 0; d < arrPath.Length; d++)
            {
                FileInfoModel sqlLiteModel = arrPath[d];


                if (d == 0)
                {
                    model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).WaitOne();
                    //создание рутовой папки
                    createRootFolder(sqlLiteModel.filename, locationDownload, hrefParent);
                }
                else
                {
                    //если это файл
                    if (isFiles)
                    {
                        //последний элемент будет выброшен из создание т.к это сам файл
                        if (d >= arrPath.Length - 1 != true)
                        {
                            model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).WaitOne();
                            createParentFolder(sqlLiteModel.filename, hrefParent);
                        }
                    }
                    else
                    {
                        model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).WaitOne();
                        createParentFolder(sqlLiteModel.filename, hrefParent);
                    }

                }

            }
        }

        private void createRootFolder(string filename, string locationDownload, string[] hrefParent)
        {
            string endLocation = locationDownload + "\\" + filename;

            if (Directory.Exists(endLocation) != true)
            {
                Directory.CreateDirectory(endLocation);
                inserHrefParent(hrefParent, endLocation);
           
            }
            else
            {
                inserHrefParent(hrefParent, endLocation);
            }

        }

      

        private void createParentFolder(string filename, string[] hrefParent)
        {
            string endLocation = hrefParent[0] + "\\" + filename;

            if (Directory.Exists(endLocation) != true)
            {
                Directory.CreateDirectory(endLocation);
                inserHrefParent(hrefParent, endLocation);

            }
            else
            {
                inserHrefParent(hrefParent, endLocation);
            }

        }

        private void inserHrefParent(string[] hrefParent, string endLocation)
        {
            hrefParent[0] = endLocation;
        }


    }


}
