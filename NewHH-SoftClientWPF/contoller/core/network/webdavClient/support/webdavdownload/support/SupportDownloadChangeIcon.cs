﻿using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavdownload.support
{
    public class SupportDownloadChangeIcon
    {

        public void cancelUpdateIconCloud(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
                List<long[][]> allListFull = new List<long[][]>();

                List<FolderNodes> list = getListFolderNodes(downloadNodes);
                allListFull = getAllChildrenList(list, model, getDownloadFolder());
                WebDavDownloadFolder downloadFolder = getDownloadFolder();

                addCloudIcon(ref model, ref downloadNodes, ref allListFull , ref downloadFolder);

        }

        public WebDavDownloadFolder getDownloadFolder()
        {
            return new WebDavDownloadFolder();
        }


        public List<FolderNodes> getListFolderNodes(FolderNodes downloadNodes)
        {
            List<FolderNodes> list = new List<FolderNodes>();
            list.Add(downloadNodes);

            return list;
        }

        public List<long[][]> getAllChildrenList(List<FolderNodes> list, DownloadWebDavModel model, WebDavDownloadFolder download)
        {
            List<long[][]> allListFull = new List<long[][]>();

            foreach (FolderNodes downloadNodes in list)
            {
                string location = downloadNodes.Location;
                ScannerSqlliteRecursiveAllChildrenModel ssracm = download.createChildrenModel(ref allListFull, ref location, model);
                allListFull = download.getAllChildren(ssracm);

            }

            return allListFull;
        }

        public void addCloudIcon(ref DownloadWebDavModel model, ref FolderNodes downloadNodes, ref List<long[][]> allListFull , ref WebDavDownloadFolder download)
        {
            List<long[][]> sortedList = getSortedRefreshList(ref model,  ref allListFull);
            addChangeIcon(model, downloadNodes, sortedList , download);
        }

        public void addChangeIcon(DownloadWebDavModel model, FolderNodes downloadNodes, List<long[][]> allListFull,  WebDavDownloadFolder download)
        {

            if (allListFull.Count >= 1)
            {
                long[][] arr = allListFull[0];


                foreach (long[] item in arr)
                {
                    long listFiles_RowId = item[0];
                    model._sqlliteController.deleteSavePcSinglRows(listFiles_RowId);
                }


                addCloudListChildren(downloadNodes, model, allListFull, download);
            }
        }

        public List<long[][]> getSortedRefreshList(ref DownloadWebDavModel model, ref List<long[][]> allListFull)
        {
            if(allListFull.Count >= 1)
            {
                long[][] arr = allListFull[0];
                List< long[][]>arr_sorted_refresh = new List<long[][]>();
                List<long[]> tempList = new List<long[]>();
      
                foreach (long[] item in arr)
                {
                    long row_id = item[0];

                    string type = model._sqlliteController.getSelect().getSaveToPcType(row_id);

                    if (staticVariable.Variable.isTypeRefresh(type))
                    {
                        tempList.Add(item);
                    }

                }
                arr_sorted_refresh.Add(tempList.ToArray());

                allListFull.Clear();
                allListFull = null;

                tempList = null;

                return arr_sorted_refresh;
            }
            else
            {
                return new List<long[][]>();
            }
           

           

        }


        public void addCloudListChildren(FolderNodes downloadnodes, DownloadWebDavModel model, List<long[][]> allListFull, WebDavDownloadFolder download)
        {
           
            addCloudList(model, downloadnodes, allListFull);
        }

        public void addCloudList(DownloadWebDavModel model, FolderNodes downloadNodes, List<long[][]> allListFull)
        {
            long[][] list = allListFull[0];

           // FolderNodes rootNodesTreeView = model._viewMain.NodesView.Items[0];
            SortableObservableCollection<FolderNodes> rootNodesListView = model._viewFolderModel.NodesView.Items;

           // model._stackUpdateTreeViewIcon.TraverseTree(rootNodesTreeView, list, staticVariable.Variable.normalIconId);
           // model._stackUpdateTreeViewIcon.TraverseViewList(rootNodesListView, staticVariable.Variable.okIconId, list);
        }

        public void addRefreshIcon(DownloadWebDavModel model, FolderNodes downloadNodes, List<long[][]> allListFull)
        {
            long[][] list = allListFull[0];

            //FolderNodes rootNodesTreeView = model._viewMain.NodesView.Items[0];
            SortableObservableCollection<FolderNodes> rootNodesListView = model._viewFolderModel.NodesView.Items;

           // model._stackUpdateTreeViewIcon.TraverseTree(rootNodesTreeView, list, staticVariable.Variable.refreshIconId);
            model._stackUpdateTreeViewIcon.TraverseViewList(rootNodesListView, staticVariable.Variable.refreshIconId, list);
        }



        public void addRefreshSqllite(DownloadWebDavModel model, List<long[][]> allListFull, int intId)
        {
            model._sqlliteController.getInsert().insertListSavePc(allListFull, intId);
        }

        public void addRefresh(DownloadWebDavModel model, FolderNodes downloadNodes, List<long[][]> allListFull)
        {
            deleteRefresh(model, allListFull);
            addRefreshSqllite(model, allListFull, staticVariable.Variable.refreshIconId);
            addRefreshIcon(model, downloadNodes, allListFull);

            changeTypePcSqllite(model, allListFull, staticVariable.Variable.refreshIconId);
        }



        private void changeTypePcSqllite(DownloadWebDavModel model, List<long[][]> allListFull, int intId)
        {
            long[][] list = allListFull[0];

            for (int a = 0; a < list.Length; a++)
            {

                if (list[a] != null)
                {
                    long row_id_listfiles = list[a][0];
                    string type = model._sqlliteController.getSelect().getSaveToPcType(row_id_listfiles);
                    string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, intId);

                    model._sqlliteController.updateListSaveToPc(row_id_listfiles, newtype, intId);
                }


            }


        }

        private void deleteRefresh(DownloadWebDavModel model, List<long[][]> allListFull)
        {
            model._sqlliteController.deletelistSaveToPc(allListFull);
        }


        public void addOkFolder(DownloadWebDavModel model, FolderNodes downloadNodes, FileInfoModel parentModel)
        {
            string type = model._sqlliteController.getSelect().getSaveToPcType(parentModel.row_id);
            string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.okIconId);

            model._sqlliteController.updateListSaveToPc(parentModel.row_id, newtype, staticVariable.Variable.okIconId);
           // model._stackUpdateTreeViewIcon.TraverseSingl(model._viewMain.NodesView.Items, parentModel.row_id, staticVariable.Variable.okIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model._viewFolderModel.NodesView.Items, parentModel.row_id, staticVariable.Variable.okIconId);

            Console.WriteLine("Закончили обработку файла  " + parentModel.filename);

        }

        public void addOkViewFolderFiles(DownloadWebDavModel model, FolderNodes downloadNodes, FileInfoModel parentModel)
        {
            string type = model._sqlliteController.getSelect().getSaveToPcType(parentModel.row_id);
            string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.okIconId);
            model._sqlliteController.updateListSaveToPc(parentModel.row_id, newtype, staticVariable.Variable.okIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model._viewFolderModel.NodesView.Items, parentModel.row_id, staticVariable.Variable.okIconId);
            // Console.WriteLine("Размер VieFoled "+ model._viewFolderModel.NodesView.Items.Count());
        }





    }
}
