﻿using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload
{
    public interface IWebDavDownload
    {
        void download(DownloadWebDavModel model);
        bool isCheckLocationDownload(string locationDownload);
    }
}
