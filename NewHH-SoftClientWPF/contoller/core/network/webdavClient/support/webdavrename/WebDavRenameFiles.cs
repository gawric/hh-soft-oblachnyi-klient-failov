﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavdownload.support;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.convert;
using NewHH_SoftClientWPF.contoller.util.filesystem.move;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Helpers;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    public class WebDavRenameFiles
    {

     
        private SupportWebDavRenameFiles _supportWebDavRenameFiles;
        private ExceptionController _exceptionContoller;
        private IRenameFiles _trainingRenameFiles;
        private IRenameFiles _startRenameToFs;
        public WebDavRenameFiles(ExceptionController exceptionController)
        {
            _exceptionContoller = exceptionController;
            _supportWebDavRenameFiles = new SupportWebDavRenameFiles();
            _trainingRenameFiles = new TrainingRenameFiles(ref _exceptionContoller);
            _startRenameToFs = new StartRenameToFileSystem();

        }

        //передает 2 запроса в базу на смену имен
        //1 запрос отправляет названия корня без детей
        //а в цикле собирает всех детей и отдельно отправляет их на сервер
        public async Task StartRenameWebDav(RenameWebDavModel renameModel , List<FileInfoModel> renameList, RenameFolderController _renameFolderController)
        {
            try
            {
                addRefreshIcon(renameModel);
                _trainingRenameFiles.startTraining(renameModel, renameList, _renameFolderController);
                _startRenameToFs.renameFileSystem(renameModel);
                //чистка
                renameList = null;
            }
            catch (Exception a)
            {
                throw;
            }
            

        }

        private void addRefreshIcon(RenameWebDavModel renameModel)
        {
            _supportWebDavRenameFiles.addRefresh(renameModel);
        }
       
        
       

        
        
       
       



    }
}
