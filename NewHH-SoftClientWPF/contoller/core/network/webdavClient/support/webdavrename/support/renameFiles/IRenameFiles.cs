﻿using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support
{
    public interface IRenameFiles
    {
        Task start(List<FileInfoModel> renameList, RenameWebDavModel renameModel, RenameFolderController _renameFolderController);
        void renameFileSystem(RenameWebDavModel renameModel);
        Task startTraining(RenameWebDavModel renameModel, List<FileInfoModel> renameList, RenameFolderController _renameFolderController);

    }
}
