﻿using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support
{
    public class SupportWebDavRenameFiles
    {
       
        public void addRefresh(RenameWebDavModel renameModel)
        {
            List<long[][]> allListFull = new List<long[][]>();
            string oldlocation = renameModel.oldLocation;
            ScannerSqlliteRecursiveAllChildrenModel ssracm = createChildrenModel(ref allListFull, ref oldlocation, renameModel);
            allListFull = getAllChildren(ssracm);
            Dictionary<long, long> allRow_idPcSave = getAllRowPcSave(renameModel._sqliteController);

            allListFull = getSortedRefreshList(ref allRow_idPcSave, ref allListFull);

            renameRefresh(renameModel , allListFull);
            allListFull = null;
        }

        private Dictionary<long, long> getAllRowPcSave(SqliteController sql)
        {
            Dictionary<long, long> allRow_idPcSave = new Dictionary<long, long>();
            if (sql != null) allRow_idPcSave = sql.getSelect().getAllSaveToPcListFilesRowId();

            return allRow_idPcSave;


        }
        private void renameRefresh(RenameWebDavModel renameModel , List<long[][]> allListFull)
        {
            deleteRefresh(renameModel._sqliteController, allListFull);
            addRefreshSqllite(renameModel._sqliteController, allListFull, staticVariable.Variable.refreshIconId);
            addRefreshIcon(renameModel, allListFull);

            changeTypePcSqllite(renameModel, allListFull, staticVariable.Variable.refreshIconId);
        }

     

        public void addOkFolder(ref SyncSinglListModel model, FileInfoModel parentModel)
        {
            string type = model._sqlliteController.getSelect().getSaveToPcType(parentModel.row_id);
            string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.okIconId);

            model._sqlliteController.updateTypeAndLocationSaveToPc(parentModel.row_id, newtype, parentModel.location, staticVariable.Variable.okIconId);

            model._stackUpdateTreeViewIcon.TraverseSingl(model._viewModelMain.NodesView.Items[0].Children , parentModel.row_id, staticVariable.Variable.okIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model.rootNodesListView, parentModel.row_id, staticVariable.Variable.okIconId);

          //  Console.WriteLine("Закончили обработку файла  " + parentModel.filename);

        }

       

        private void deleteRefresh(SqliteController sql, List<long[][]> allListFull)
        {
            if(sql != null) sql.deletelistSaveToPc(allListFull);
        }
        public void addRefreshSqllite(SqliteController sql, List<long[][]> allListFull, int intId)
        {
            if(sql != null) sql.getInsert().insertListSavePc(allListFull, intId);
        }

        private void addRefreshIcon(RenameWebDavModel model, List<long[][]> allListFull)
        {
            long[][] list = allListFull[0];

            model._stackUpdateTreeViewIcon.TraverseTree(model.rootNodesTreeView, list, staticVariable.Variable.refreshIconId);
            model._stackUpdateTreeViewIcon.TraverseViewList(model.rootNodesListView, staticVariable.Variable.refreshIconId, list);
        }

        private void changeTypePcSqllite(RenameWebDavModel model, List<long[][]> allListFull, int intId)
        {
            long[][] list = allListFull[0];

            for (int a = 0; a < list.Length; a++)
            {

                if (list[a] != null)
                {
                    long row_id_listfiles = list[a][0];
                    string type = model._sqliteController.getSelect().getSaveToPcType(row_id_listfiles);
                    string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, intId);

                    model._sqliteController.updateListSaveToPc(row_id_listfiles, newtype, intId);
                }


            }


        }

        public Dictionary<long, long> getAllPcSaveRow_id(SqliteController sqliteController , Dictionary<long , long> allPcSave)
        {
            if (allPcSave == null) allPcSave = sqliteController.getSelect().getAllSaveToPcListFilesRowId();

            return allPcSave;
        }

        public List<FileInfoModel> getSortedOkList(ref Dictionary<long, long> allPcSave, ref List<FileInfoModel> fimList)
        {
            List<FileInfoModel> sortedList = new List<FileInfoModel>();

            foreach(FileInfoModel item in fimList)
            {
                if(item != null)
                {
                    if (allPcSave.ContainsKey(item.row_id)) sortedList.Add(item);
                }
               
            }

            fimList = null;

            return sortedList;
        }

        public List<long[][]> getSortedRefreshList(ref Dictionary<long , long> allPcSave, ref List<long[][]> allListFull)
        {
            if (allListFull.Count >= 1)
            {
                long[][] arr = allListFull[0];
                List<long[][]> arr_sorted_refresh = new List<long[][]>();
                List<long[]> tempList = new List<long[]>();

                foreach (long[] item in arr)
                {
                    long row_id = item[0];

                    if (allPcSave.ContainsKey(row_id)) tempList.Add(item);
                }
                arr_sorted_refresh.Add(tempList.ToArray());

                allListFull.Clear();
                allListFull = null;
                tempList = null;
                allPcSave = null;

                return arr_sorted_refresh;
            }
            else
            {
                return new List<long[][]>();
            }




        }


        public ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(ref List<long[][]> allListFull, ref string location, RenameWebDavModel renameWebDavModel)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();

            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = renameWebDavModel._sqliteController;
            model.progress = new Progress<CopyViewModel>();
            model.progressLabel = "search children";
            //все записи из базы
            model.allParent = getAllParent(renameWebDavModel._sqliteController);
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();

            return model;
        }

        private Dictionary<long, List<long[]>> getAllParent(SqliteController sqllite)
        {
            Dictionary<long, List<long[]>> dict;

            if (sqllite != null){dict = sqllite.getSelect().getAllParentListRowId();}
            else
            {
                dict = new Dictionary<long, List<long[]>>();
            }
           

            return dict;
        }
        public List<long[][]> getAllChildren(ScannerSqlliteRecursiveAllChildrenModel ssracm)
        {
            ScannerSqlliteRecursiveAllChildren ssrach = new ScannerSqlliteRecursiveAllChildren();
            return ssrach.getScanAllChildren(ssracm);
        }
    }
}
