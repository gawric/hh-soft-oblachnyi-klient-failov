﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.download.supportDownload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    public class WebDavDownload : IWebDavDownload
    {
        //подписка на событие "Обновление ProgressBar"
        public event EventHandler<UpdateViewTransferFolderPgArgs> folderTreeViewPgEventHandle;

        public void download(DownloadWebDavModel model)
        {
          
                List<FolderNodes> list = model._listDownload;

                model.folderTreeViewPgEventHandle = folderTreeViewPgEventHandle;
                bool[] error = { false };

                 addRefreshList(list, model);

                for (int b = 0; b < list.Count; b++)
                {
                    FolderNodes downloadNodes = list[b];
                    if(downloadNodes != null)
                    {
                        string href = downloadNodes.Location;
                        deleteTempPb(ref model, ref href);
                        startDownload(downloadNodes, model);

                    }
                   
                }
         
           
        }
        private void addRefreshList(List<FolderNodes> list , DownloadWebDavModel model)
        {
            WebDavDownloadFolder download = new WebDavDownloadFolder();
            List<long[][]> listAllChildren =  getAllChildrenList(list, model, download);
            addRefreshListChildren(list, model, listAllChildren, download);
            
        }
       

        private void startDownload(FolderNodes downloadNodes , DownloadWebDavModel model)
        {
            if (staticVariable.Variable.isFolder(downloadNodes.Type))
            {
                WebDavDownloadFolder download = new WebDavDownloadFolder();
                download.trainingDownloadFolder(model, downloadNodes);
            }
            else
            {
                WebDavDownloadFiles download = new WebDavDownloadFiles();
                download.trainingDownloadFiles(model, downloadNodes);
            }
        }

        private void deleteTempPb(ref DownloadWebDavModel downloadModel, ref string href)
        {
            Dictionary<string, long> dic = downloadModel.dictDownloadHrefRow_id;
            downloadModel._updateTransferController.deleteTempProgressBar(dic[href], href);
        }

        //проверка что локация на компьютере существет или нет
        public bool isCheckLocationDownload(string locationDownload)
        {
            if(locationDownload.Equals(""))
            {
                return false;
            }
            else
            {
                if(Directory.Exists(locationDownload))
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
        }

        private void addRefreshListChildren(List<FolderNodes> list, DownloadWebDavModel model, List<long[][]> listAllChildren, WebDavDownloadFolder download)
        {
            for (int g = 0; g < list.Count; g++)
            {
                if (list[g] != null)
                {
                    FolderNodes downloadNodes = list[g];
                    List<long[][]> arr = new List<long[][]>();
                    arr.Add(listAllChildren[g]); ;
                    download.addRefresh(model, list[g], arr);

                    arr = null;

                }
            }
        }


        private List<long[][]> getAllChildrenList(List<FolderNodes> list, DownloadWebDavModel model, WebDavDownloadFolder download)
        {
            List<long[][]> allListFull = new List<long[][]>();

            foreach (FolderNodes downloadNodes in list)
            {
                string location = downloadNodes.Location;
                ScannerSqlliteRecursiveAllChildrenModel ssracm = download.createChildrenModel(ref allListFull, ref location, model);
                allListFull = download.getAllChildren(ssracm);
                //ssracm = null;
            }

            return allListFull;
        }

    }
}
