﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support.webdavevent;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    public class WebDavUpload
    {
        //срабатывает при вызове upload server с флагом true. Говорит о том, что данный компьютер хочет заливает папку с флагом scanfolder
        public event EventHandler<EventScanFolderItemModel> eventAddItem;
        //WebDavEventNormal e1 = new WebDavEventNormal();
        public void UploadFiles(UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadModelObj)
        {
            uploadModel.client = getClientWebDav(uploadModelObj._webDavclient, uploadModel.username, uploadModel.password);

            string[] hrefPath = uploadModel.allListHref;
            string pasteHref = uploadModel.pasteModel.location;

            pasteHref = CheckEqualsDisk(pasteHref);
            uploadModel.pasteHref = pasteHref;
            
            TrainingUpload(hrefPath, uploadModel , uploadModelObj);
            
        }

        public EventHandler<EventScanFolderItemModel> getEventAdditem()
        {
            return eventAddItem;
        }
        //Общая подготовка к отправке
        private void TrainingUpload(string[] hrefPath, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadModelObj)
        {
            FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            long[] new_idcontainer = { 0 };

            uploadModel.arr = arr;
            uploadModel.new_id_container = new_idcontainer;

            TrainingUploadFolder(hrefPath, uploadModel , uploadModelObj).Wait();

            TrainingUploadFiles(hrefPath, uploadModel , uploadModelObj).Wait();

        }





        //подготовка к отправке папок
        private async Task TrainingUploadFolder(string[]hrefPath , UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadModelObj)
        {
            if(IsFolder( hrefPath))
            {
                long version = GetVersion(uploadModelObj._sqlLiteController);

                Wait(uploadModelObj._blockingEventController);

                WebDavUploadFolders upload = new WebDavUploadFolders(uploadModelObj._exceptionController);
                SubscribleEvent(upload);

                await upload.uploadFolderFirst(hrefPath, uploadModel, version, uploadModelObj);
            }
            else
            {
                Debug.Print("WebDavUpload->TrainingUploadFolder: В hrefpath[] не нашли папок для запуска ");
            }
            
        }
        //если в списке присутствует хотя бы 1 папка
        private bool IsFolder(string[] hrefPath)
        {

            foreach(string href in hrefPath)
            {
                if(Directory.Exists(href))
                {
                    return true;
                }
            }

            return false;
        }

        //подготовка к отправке файлов
        public async Task TrainingUploadFiles(string[] hrefPath , UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadModelObj)
        {
            try
            {
                    uploadModel.arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
                    WebDavUploadFiles upload = new WebDavUploadFiles(uploadModelObj._exceptionController);
                    SubscribleEventFiles(upload , uploadModel.infoStack , uploadModel.tempFileSubsribleList);
                    upload.uplodFilesFirst(hrefPath, uploadModel , uploadModelObj);
            }
            catch (System.NullReferenceException z)
            {
                uploadModelObj._exceptionController.sendError((int)CodeError.NULL_POINT_EXCEPTION, "WEbDavUpload-> TrainingUploadFiles: Критическа ошибка NullReferenceException", z.ToString());
            }
            
        }

        
        private string CheckEqualsDisk(string pasteHref)
        {
            if(pasteHref.Equals(staticVariable.Variable.getRootLocationDisk()))
            {
                return staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace + "/";
            }
            else
            {
                return pasteHref;
            }

        }


        private void Wait(BlockingEventController _blockingEventController)
        {
            _blockingEventController.waitOneMRE(staticVariable.Variable.blockingUploadTokenId);
        }
        private long GetVersion(SqliteController sql)
        {
            return sql.getSelect().getSqlLiteVersionDataBasesClient();
        }

        private void SubscribleEvent(WebDavUploadFolders upload)
        {
            new WebDavEventNormal( upload , eventAddItem);
        }

        private void SubscribleEventFiles(WebDavUploadFiles upload , InfoStackModel infoStack , List<FileInfoModel> tempFileSubsribleList)
        {
            new WebDavEventPartiesFiles(upload, eventAddItem , ref infoStack , ref tempFileSubsribleList);
        }


        private Client getClientWebDav(WebDavClient _webDavclient, string username, string password)
        {
            return _webDavclient.CreateNewConnect(username, password);
        }

       

    }
}
