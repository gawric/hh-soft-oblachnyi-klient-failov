﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload
{
    public class WebDavUploadFiles : IWebDavUpload
    {
        public ExceptionController _exceptionController;
        private RunUploadFiles _runUploadFiles;

        public event EventHandler<UploadFolderEventHandler> beginEventHandler;
        public event EventHandler<UploadFolderEventHandler> updateEventHandler;
        public event EventHandler<UploadFolderEventHandler> endEventHandler;

        public WebDavUploadFiles(ExceptionController exceptionController)
        {
            _exceptionController = exceptionController;
        }

        public void uplodFilesFirst(string[] hrefPath, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadModelObj)
        {
           
            long version = getVersionBases(uploadModelObj._sqlLiteController);
            uploadModel.version_bases = version;
           

            int[] cnt = { 0 };

            if(hrefPath != null)
            {
                //есть ли  вообще файлы в массиве
                bool isExistsFiles = checkIsFiles(hrefPath);

                if (isExistsFiles)
                {
                    _runUploadFiles = new RunUploadFiles(_exceptionController);

                    sendBegin(ref uploadModelObj, ref uploadModel, ref version);

                    upload(hrefPath, uploadModel, uploadModelObj, ref cnt, ref version);

                    sendEndParties(ref uploadModelObj, ref uploadModel, ref version);

                }

               
            }


        }

        private void upload(string[] hrefPath, UploadWebDavModel uploadModel ,  UploadWebDavObjectModel uploadWebDavObjectModel ,  ref int[] cnt , ref long version)
        {

            //какие именно файлы
            for (int h = 0; h < hrefPath.Length; h++)
            {
                string href = hrefPath[h];
                start( href,  uploadModel, uploadWebDavObjectModel , ref  cnt, ref  version);
            }

        }
        private void start(string href, UploadWebDavModel uploadModel, UploadWebDavObjectModel uploadWebDavObjectModel , ref int[] cnt, ref long version)
        {
            if (href != null)
            {
                if (isFolder(href) != true)
                {
                    //IFileInfoProxy fi = getFileInfo(href);
                    _runUploadFiles.run(ref uploadModel, ref uploadWebDavObjectModel, ref href, ref cnt, ref version, getFileInfo(href));
                }
            }
        }
        private IFileInfoProxy getFileInfo(string Href)
        {
            return new FileInfoProxy(Href);
        }


        private void sendBegin(ref UploadWebDavObjectModel uploadModelObj , ref UploadWebDavModel uploadModel, ref long version)
        {
           
            //if(uploadModelObj.networkJsonController != null) uploadModelObj.networkJsonController.updateSendJsonSINGLBEGINUPDATEPARTIES(uploadModel.arr, version);
            if (uploadModelObj.networkJsonController != null)AddBeginEvent(version, uploadModel.arr, uploadModelObj.networkJsonController);
        }

        private void sendEndParties(ref UploadWebDavObjectModel uploadModelObj, ref UploadWebDavModel uploadModel , ref long version)
        {
            //if (uploadModelObj.networkJsonController != null) uploadModelObj.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(uploadModel.arr, version);
            if (uploadModelObj.networkJsonController != null) AddEndEvent(version, uploadModel.arr, uploadModelObj.networkJsonController);
        }

        private long getVersionBases(SqliteController sql)
        {
            long version = 0;
            if (sql != null) version = sql.getSelect().getSqlLiteVersionDataBasesClient();
            return version;
        }
        private bool checkIsFiles(string[] hrefPath)
        {
            bool check = false;

            for (int h = 0; h < hrefPath.Length; h++)
            {
                string href = hrefPath[h];

                if (isFolder(href) != true)
                {
                    check = true;
                }
            }

            return check;
        }


    
        private bool isFolder(string Href)
        {
            try
            {
                FileAttributes attr = File.GetAttributes(Href);

                if (attr.HasFlag(FileAttributes.Directory))
                {

                    return true;
                }
                else
                {

                    return false;
                }

            }
            catch(System.IO.FileNotFoundException)
            {
                Console.WriteLine("WebDavUploadFiles->isFolder: не найден файл на локальном диске!!!");
                return true;
            }
           

        }

        public void AddBeginEvent(long version, FileInfoModel[] arr, UpdateNetworkJsonController networkJsonController)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = networkJsonController;
            OnEventBegin(args);
        }



        public void AddEndEvent(long version, FileInfoModel[] arr, UpdateNetworkJsonController networkJsonController)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = networkJsonController;
            OnEventEnd(args);
        }

        public void AddEndUpdate(long version, FileInfoModel[] arr, UpdateNetworkJsonController networkJsonController)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = networkJsonController;
            OnEventEnd(args);
        }

        protected virtual void OnEventBegin(UploadFolderEventHandler e)
        {
            beginEventHandler?.Invoke(this, e);
        }


        protected virtual void OnEventEnd(UploadFolderEventHandler e)
        {
            endEventHandler?.Invoke(this, e);
        }

        protected virtual void OnEventUpdate(UploadFolderEventHandler e)
        {
            updateEventHandler?.Invoke(this, e);
        }

        public Task uploadFolderFirst(string[] hrefPath, UploadWebDavModel uploadModel, long version, UploadWebDavObjectModel uploadModelObj)
        {
            throw new NotImplementedException();
        }
    }
    
}
