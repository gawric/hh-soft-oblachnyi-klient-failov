﻿using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support.webdavevent
{
    public class WebDavEventPartiesFiles : IWebDavEvent
    {
        
        private EventHandler<EventScanFolderItemModel> eventAddItem;
        private InfoStackModel infoStack;
        List<FileInfoModel> tempFileSubsribleList;

        public WebDavEventPartiesFiles(WebDavUploadFiles upload, EventHandler<EventScanFolderItemModel> eventUploadAddItem, ref InfoStackModel infoStack , ref List<FileInfoModel> tempFileSubsribleList)
        {
            upload.beginEventHandler += c_BeginUpdateFolder;
            upload.updateEventHandler += c_UpdateFolder;
            upload.endEventHandler += c_EndUpdateFolder;
            this.eventAddItem = eventUploadAddItem;
            this.infoStack = infoStack;
            this.tempFileSubsribleList = tempFileSubsribleList;
        }




        void c_BeginUpdateFolder(object sender, UploadFolderEventHandler e)
        {
            EventBeginParties(e);
        }
        void c_UpdateFolder(object sender, UploadFolderEventHandler e)
        {
            EventUpdateParties(e);
        }
        void c_EndUpdateFolder(object sender, UploadFolderEventHandler e)
        {
            EventEndParties(e);
        }






        private void EventBeginParties(UploadFolderEventHandler e)
        {
            if (IsEmptyStack() | IsFullArr(e.arr))
            {
                e.networkJsonController.updateSendJsonSINGLBEGINUPDATEPARTIES(e.arr, e.version);
            }
            else
            {
                AddListArr(e.arr);
            }

        }



        private void EventUpdateParties(UploadFolderEventHandler e)
        {
            if (IsEmptyStack() | IsFullArr(e.arr))
            {
                e.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(e.arr, e.version);

                if (tempFileSubsribleList.Count > 0)
                {
                    SendLazyFiles(e);
                }
            }
            else
            {
                AddListArr(e.arr);
            }
        }
        private void EventEndParties(UploadFolderEventHandler e)
        {
            if (IsEmptyStack() | IsFullArr(e.arr))
            {
                e.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(e.arr, e.version);
                SendLazyFiles(e);
            }
            else
            {
                AddListArr(e.arr);
            }
        }

        private void SendLazyFiles(UploadFolderEventHandler e)
        {
            if (tempFileSubsribleList.Count > 0)
            {
                FileInfoModel[] arrTemp = tempFileSubsribleList.ToArray();
                PrintMessage( arrTemp);
                e.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(arrTemp, e.version);
                tempFileSubsribleList.Clear();
            }
        }

        private void PrintMessage(FileInfoModel[] arrTemp)
        {
            Debug.Print("WebDavEventPartiesFiles->SendLazyFiles->PrintMessage: Размер отправляемого обьекта json " + arrTemp.Length);
        }
        private void  AddListArr(FileInfoModel[] arr)
        {
            if (arr != null)
            {
                IterateArr(arr);
            }

        }
        private void IterateArr(FileInfoModel[] arr)
        {
            foreach (FileInfoModel item in arr)
            {
                AddItem(item);
            }
        }
        private void AddItem(FileInfoModel item)
        {
            if (item != null)
            {
                tempFileSubsribleList.Add(item);
            }
        }

   
        private bool IsFullArr(FileInfoModel[] arr)
        {
            if (arr != null)
            {
                int size = SizeArr(arr);
                if (size >= staticVariable.Variable.CountFilesTrasferServer) return true;
            }
            return false;
        }
        private bool IsEmptyStack()
        {
            if (infoStack.filescount == 0)
            {
                return true;
            }

            return false;
        }
        private int SizeArr(FileInfoModel[] arr)
        {
            if (arr == null) return 0;

            return arr.Count(s => s != null);

        }

  

        protected virtual void OnThresholdReachedScanFolder(EventScanFolderItemModel e)
        {
            eventAddItem?.Invoke(this, e);
        }
    }
}

