﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.ScanWebDav;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload
{
    public class WebDavUploadFolders : IWebDavUpload
    {
        public event EventHandler<UpdateViewTransferFolderPgArgs> folderTreeViewPgEventHandle;
        private ExceptionController _exceptionController;

        public event EventHandler<UploadFolderEventHandler> beginEventHandler;
        public event EventHandler<UploadFolderEventHandler> updateEventHandler;
        public event EventHandler<UploadFolderEventHandler> endEventHandler;
        public WebDavUploadFolders(ExceptionController exceptionController)
        {
            this._exceptionController = exceptionController;
        }

        public async Task uploadFolderFirst(string[] hrefPath, UploadWebDavModel uploadModel, long version , UploadWebDavObjectModel uploadModelObj)
        {
           // uploadModel.networkJsonController.updateSendJsonSINGLBEGINUPDATEPARTIES(uploadModel.arr, version);
            AddBeginEvent(version , uploadModel.arr , uploadModelObj.networkJsonController);

            InitArr(uploadModel);
            SubScribleUpdateEvent(uploadModel);

            await start(hrefPath, uploadModel , uploadModelObj);

            await checkDublication(uploadModel);

            AddEndEvent(version, uploadModel.arr, uploadModelObj.networkJsonController);
            //uploadModel.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(uploadModel.arr, version);
            uploadModel.arr = null;
        }


        private void InitArr(UploadWebDavModel uploadModel)
        {
            uploadModel.arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
        }
        private void SubScribleUpdateEvent(UploadWebDavModel uploadModel)
        {
            uploadModel.updateEventHandler = updateEventHandler;
        }

        private async Task checkDublication(UploadWebDavModel uploadModel)
        {


            SotredSourceHrefToDouble check = uploadModel.checkParent;

            for (int f = 0; f < uploadModel.arr.Length; f++)
            {
                FileInfoModel fi = uploadModel.arr[f];

                if (fi != null)
                {
                    bool check2 = check.checkPasteFolderToDouble(fi.location, uploadModel.allHrefFolderSqllite);

                    if (check2)
                    {
                        uploadModel.arr[f] = null;
                    }
                }

            }
        }

        private async Task start(string[] hrefPath, UploadWebDavModel uploadModel,  UploadWebDavObjectModel uploadModelObj)
        {
            if (hrefPath != null)
            {
                for (int h = 0; h < hrefPath.Length; h++)
                {
                    string href = hrefPath[h];

                    if (href != null)
                    {
                        if (isFolder(href))
                        {
                            deleteTempPb(ref uploadModel, ref uploadModelObj , ref href );
                            await startUploadFolder(href, uploadModel , uploadModelObj);
                        }
                    }

                }
            }

        }

        private void deleteTempPb(ref UploadWebDavModel uploadModel, ref UploadWebDavObjectModel uploadModelObj , ref string href)
        {
            Dictionary<string, long> dic = uploadModel.dictUploadHrefRow_id;
            uploadModelObj._updateTransferController.deleteTempProgressBar(dic[href], href);
        }

        private async Task StartFolderUpload(string href, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadWebDavModelObject)
        {
            ScanUploadWebDavModel uploadFolderModel = null;
            string hrefFolderOriginal = String.Copy(href);

            try
            {
           
                DirectoryInfo di = getDirectory(href);

       
                RecursiveLocalModel modelLocalDI = getCountLocalFolder(di);
                updatePb(uploadModel, modelLocalDI);

                FolderNodesTransfer modelSource = createTransfer(uploadModel, uploadWebDavModelObject, modelLocalDI, di);
                

                uploadFolderModel = createScanUploadWebModel(di, href, uploadModel , uploadWebDavModelObject);
                uploadFolderModel = injectModelSource(modelSource, uploadFolderModel);

                createProgress(uploadModel, uploadWebDavModelObject, modelSource , uploadFolderModel);
             

                await createFolder(uploadFolderModel, modelLocalDI, uploadWebDavModelObject, uploadModel, hrefFolderOriginal);

                //если не сделать return arr возвращается пустым
                uploadModel.arr = await uploadFiles(uploadFolderModel, uploadModel.client, modelLocalDI);

               

            }
            catch (System.Collections.Generic.KeyNotFoundException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->StartFolderUpload Критическая ошибка KeyNotFoundException ", z.ToString());
               // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + s);
            }
            catch (System.Net.Http.HttpRequestException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->StartFolderUpload Критическая ошибка HttpRequestException ", z.ToString());
               // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + b);
            }
            catch (WebDAVClient.Helpers.WebDAVException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->StartFolderUpload Критическая ошибка WebDAVException ", z.ToString());
               // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + z);
            }
            catch (System.AggregateException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->StartFolderUpload Критическая ошибка AggregateException ", z.ToString());
                //Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + w);
            }
            catch (System.IO.DirectoryNotFoundException z)
            {
                // insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->StartFolderUpload Критическая ошибка DirectoryNotFoundException ", z.ToString());
               // Console.WriteLine("WebDavUploadFOlder->StartFolderUpload: Критическая Ошибка " + w);
            }


        }

        public void AddUpdateEvent(long version, FileInfoModel[] arr, UploadWebDavObjectModel obj , UploadWebDavModel uploadModel)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = obj.networkJsonController;
            OnEventUpdate(args, uploadModel);
        }

        protected virtual void OnEventUpdate(UploadFolderEventHandler e, UploadWebDavModel uploadModel)
        {
            uploadModel.updateEventHandler?.Invoke(this, e);
        }

        private ScanUploadWebDavModel injectModelSource(FolderNodesTransfer modelSource , ScanUploadWebDavModel uploadFolderModel)
        {
            uploadFolderModel.modelSource = modelSource;

            return uploadFolderModel;
        }


        private void updatePb(UploadWebDavModel uploadModel , RecursiveLocalModel modelLocalDI)
        {
            //Для Progress бара
            uploadModel.sizeBytesFolder = modelLocalDI.sizeBytesFolder;
        }
       

        private FolderNodesTransfer createTransfer(UploadWebDavModel uploadModel, UploadWebDavObjectModel uploadModelObj, RecursiveLocalModel modelLocalDI, DirectoryInfo di)
        {
            CreateModel createModel = new CreateModel(uploadModelObj.convertIcon);
            //начинаем сканирование
            FolderNodesTransfer modelSource = createModel.createTransferModel(uploadModel.pasteModel.row_id, di.Name, modelLocalDI.sizeBytesFolder, "upload", "folder");
            modelSource.Row_id = uploadModelObj._sqlLiteController.getSelect().getLastRow_idToListFilesTransfer() + 1;



            return modelSource;
        }


        //Подписываемся на Progress, что-бы обновлять статус при копировании
        private void createProgress(UploadWebDavModel uploadModelWebDave, UploadWebDavObjectModel uploadModelObj, FolderNodesTransfer modelSource, ScanUploadWebDavModel uploadFolderModel)
        {
            //добавление в listTransfer
            uploadModelObj._updateTransferController.createProgressBar(modelSource, modelSource.FolderName);

            //подписка на изменения
            uploadModelObj._updateTransferController.UpdateFolderProgressBar(uploadFolderModel, uploadModelObj, modelSource, uploadModelWebDave);
           
        }

        //конвертация модели UploadWebDavModel в ScanUploadWebDavModel
        private ScanUploadWebDavModel createScanUploadWebModel(DirectoryInfo di, string href, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadWebDavModel)
        {
            ScanUploadWebDavModel uploadFolderModel = new ScanUploadWebDavModel();
            Dictionary<string, long> allFolderDictionary = new Dictionary<string, long>();

            uploadFolderModel.allFolderDictionary = allFolderDictionary;
            uploadFolderModel.networkJsonController = uploadWebDavModel.networkJsonController;

            uploadFolderModel.hrefFolder = href;
            uploadFolderModel.clientRootLocalDirectory = href;
            uploadFolderModel.pasteRootWebDavDirectory = uploadModel.pasteHref;
            uploadFolderModel.verison_dabases = uploadModel.version_bases;
            uploadFolderModel.new_id_container = uploadModel.new_id_container;
            uploadFolderModel._sqlLiteController = uploadWebDavModel._sqlLiteController;
            uploadFolderModel.pasteModel = uploadModel.pasteModel;
            uploadFolderModel.arr = uploadModel.arr;

            uploadFolderModel.client = uploadModel.client;
            uploadFolderModel._updateTransferController = uploadWebDavModel._updateTransferController;
            uploadFolderModel._transferViewNodes = uploadWebDavModel._transferViewNodes;

            uploadFolderModel.folderTreeViewPgEventHandle = folderTreeViewPgEventHandle;
            uploadFolderModel.copyDirectory = di;

            uploadFolderModel.checkParent = uploadModel.checkParent;
            uploadFolderModel.allSizeBytesFolder = uploadModel.sizeBytesFolder;
            uploadFolderModel.convertIcon = uploadWebDavModel.convertIcon;
            uploadFolderModel.webDavExist = uploadWebDavModel.webDavExist;
            uploadFolderModel._blockingEventController = uploadWebDavModel._blockingEventController;
            //Все папки из базы данных
            uploadFolderModel.allHrefFolder = uploadModel.allHrefFolderSqllite;
            uploadFolderModel._cancellationController = uploadWebDavModel._cancellationController;
            uploadFolderModel._exceptionController = uploadWebDavModel._exceptionController;
            uploadFolderModel.updateEventHandler = uploadModel.updateEventHandler;

            return uploadFolderModel;
        }



        private async Task<FileInfoModel[]> uploadFiles(ScanUploadWebDavModel uploadFolderModel, Client client, RecursiveLocalModel modelLocalDI)
        {
            int[] contFiles = { 0 };
            ScanUploadFilesToDirectory scanFiles = new ScanUploadFilesToDirectory(client);
            uploadFolderModel.allCountFilesToFiles = modelLocalDI.sizeCountFiles;
            uploadFolderModel.currentCountFilesToFolder[0] = 0;

            try
            {

                //теперь копируем файлы в созданные папки
                await scanFiles.UploadRecursiveFilesInClient(uploadFolderModel, contFiles);
                count0UpdatePb(uploadFolderModel);

                return uploadFolderModel.arr;
            
            }
            catch (System.Collections.Generic.KeyNotFoundException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->uploadFiles Критическая ошибка KeyNotFoundException ", z.ToString());
               // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + s);
                return new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            }
            catch (System.Net.Http.HttpRequestException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->uploadFiles Критическая ошибка HttpRequestException ", z.ToString());
                // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + b);
                return new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            }
            catch (System.IO.IOException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->uploadFiles Критическая ошибка IOException ", z.ToString());
                //  Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + z);
                return new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            }
            catch (WebDAVClient.Helpers.WebDAVException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->uploadFiles Критическая ошибка WebDAVException ", z.ToString());
                // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + z);
                return new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            }
            catch (System.AggregateException z)
            {
                insetErrorProgressBarFolder(uploadFolderModel);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavUploadFOlder->uploadFiles Критическая ошибка AggregateException ", z.ToString());
                // Console.WriteLine("WebDavUploadFOlder->uploadFiles: Ошибка " + w);
                return new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            }

        }

        private void count0UpdatePb(ScanUploadWebDavModel uploadFolderModel)
        {
            if (uploadFolderModel.allCountFilesToFiles == 0)
            {
                insetCount0ProgressBarFolder(uploadFolderModel);
            }

        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }
        //используем когда нужно заполнить progressbar полностью
        //к пример когда файлов 0, а папок много. 
        private void insetCount0ProgressBarFolder(ScanUploadWebDavModel uploadFolderModel)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            //странно я почему то не беру размеры из args а напрямую из модели!
            uploadFolderModel.currentCountFilesToFolder[0] = 5;
            uploadFolderModel.allCountFilesToFolder = 5;
            args.training = false;
            args.errorStatus = false;
            //отправка в обработку
            uploadFolderModel.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        private void insetErrorProgressBarFolder(ScanUploadWebDavModel uploadFolderModel)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = uploadFolderModel.currentCountFilesToFolder[0];
            args.sizeFilesFolder = uploadFolderModel.allCountFilesToFiles;
            args.training = false;
            args.errorStatus = true;
            //отправка в обработку
            uploadFolderModel.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        //отдельно только папки
        private async Task startUploadFolder(string href, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadWebDavModelObject)
        {
            await StartFolderUpload(href, uploadModel , uploadWebDavModelObject);

        }


        private bool isFolder(string Href)
        {
            // get the file attributes for file or directory
            FileAttributes attr = File.GetAttributes(Href);


            if (attr.HasFlag(FileAttributes.Directory))
            {

                return true;
            }
            else
            {

                return false;
            }

        }


        private DirectoryInfo getDirectory(string href)
        {
            return new System.IO.DirectoryInfo(href);
        }


        private RecursiveLocalModel getCountLocalFolder(DirectoryInfo di)
        {
            RecursiveLocalModel modelRecursive = new RecursiveLocalModel();
            modelRecursive.sizeBytesFolder = 0;
            modelRecursive.sizeCountFolder = 0;
            modelRecursive.sizeCountFiles = 0;

            ScannerRecursiveLocalFolder scannerLocalFolder = new ScannerRecursiveLocalFolder();

            scannerLocalFolder.WalkDirectoryTree(di, modelRecursive);

            return modelRecursive;
        }



        private async Task createFolder(ScanUploadWebDavModel uploadFolderModel, RecursiveLocalModel modelLocalDI, UploadWebDavObjectModel uploadModelObj, UploadWebDavModel uploadModel, string hrefFolderOriginal)
        {
           
            ScanUploadDirectory scanFolder = new ScanUploadDirectory(uploadModel.client, uploadModelObj.networkJsonController, uploadModelObj._exceptionController );
            
            int[] contFolder = updatePb(uploadFolderModel, modelLocalDI);

            //рабочая конструкция создает папки на webDavServer и генерирует дерево родителей
            uploadModel.arr = await scanFolder.uploadRecursiveScanFolderInClient(uploadFolderModel, contFolder);

            await sendJsonFolderOther(uploadModel , uploadModelObj);
            endUpdate(uploadFolderModel, hrefFolderOriginal);


        }

        private int[] updatePb(ScanUploadWebDavModel uploadFolderModel , RecursiveLocalModel modelLocalDI)
        {
            int[] currentCountFilesToFolder = { 0 };
            uploadFolderModel.currentCountFilesToFolder = currentCountFilesToFolder;

            int[] contFolder = { 0 };
            uploadFolderModel.allCountFilesToFolder = modelLocalDI.sizeCountFolder;

            return contFolder;
        }

        private void endUpdate(ScanUploadWebDavModel uploadFolderModel , string hrefFolderOriginal)
        {
            uploadFolderModel.hrefFolder = hrefFolderOriginal;

            insetCountToProgressBarFolder(uploadFolderModel);

            uploadFolderModel.allCountFilesToFolder = 0;

          
        }

       

        private void insetCountToProgressBarFolder(ScanUploadWebDavModel uploadFolderModel)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = uploadFolderModel.allCountFilesToFolder;
            args.sizeFilesFolder = uploadFolderModel.allCountFilesToFolder;
            args.training = true;

            if (uploadFolderModel._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.uploadCancelationTokenId).IsCancellationRequested)
            {
                args.isStop = true;
            }
            //отправка в обработку
            uploadFolderModel.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

      


        private async Task sendJsonFolderOther(UploadWebDavModel uploadModel , UploadWebDavObjectModel obj)
        {

           
            await checkDublication(uploadModel);

            //obj.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(uploadModel.arr, uploadModel.version_bases);
            AddUpdateEvent(uploadModel.version_bases, uploadModel.arr, obj , uploadModel);


            for (int s = 0; s < uploadModel.arr.Length; s++)
            {
                uploadModel.arr[s] = null;
            }
        }

       


        public void AddBeginEvent(long version , FileInfoModel[] arr , UpdateNetworkJsonController networkJsonController)
        {
           UploadFolderEventHandler args = new UploadFolderEventHandler();
           args.version = version;
           args.arr = arr;
           args.networkJsonController = networkJsonController;
           OnEventBegin(args);
        }

        

        public void AddEndEvent(long version, FileInfoModel[] arr , UpdateNetworkJsonController networkJsonController)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = networkJsonController;
            OnEventEnd(args);
        }

        protected virtual void OnEventBegin(UploadFolderEventHandler e)
        {
            beginEventHandler?.Invoke(this, e);
        }

       
        protected virtual void OnEventEnd(UploadFolderEventHandler e)
        {
            endEventHandler?.Invoke(this, e);
        }

        public void uplodFilesFirst(string[] hrefPath, UploadWebDavModel uploadModel, UploadWebDavObjectModel uploadModelObj)
        {
            throw new NotImplementedException();
        }
    }
}
