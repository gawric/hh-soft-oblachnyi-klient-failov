﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    class WebDavDeleteFiles
    {
        private ExceptionController _exceptionController;

        public WebDavDeleteFiles(ExceptionController exceptionController)
        {
            _exceptionController = exceptionController;
        }

    private void StartDeleteFilesWebDav(long[][]list , DeleteFilesAsyncModel DeleteModel , WebDavClient _webdavclient)
      {
            int deleteFileCount = 0;
            Client client = _webdavclient.CreateNewConnect(DeleteModel.username, DeleteModel.password);
            //в массив так же добавляется текущий файл
            //здесь удаляем только файлы
            //удаление 2534 файла выполняется 1:57 минут у яндекса 2:45
            //копирование 2534 файла выполняется ---- минут у яндекса 18 минут
           
         try
         {
                for (int k = 0; k < list.Length; k++)
                {
                    long[] modelServer = list[k];

                    Console.WriteLine("WebDavClientController -> getSingWebDavAsync -> Удаление файла " + deleteFileCount++ + " \\ из " + list.Length);

                    if (modelServer[1] == 0)
                    {
                        DeleteFileModel deleteServerModel = DeleteModel._sqlLiteController.getSelect().getSearchListTempByRow_IdDeleteFileModel(modelServer[0]);

                        if (deleteServerModel != null)
                        {
                            if (deleteServerModel.location.Equals("") != true)
                            {

                                _webdavclient.deleteFilesAsync(deleteServerModel.location, DeleteModel.username, DeleteModel.password, deleteServerModel.isFolder, client);
                            }
                            else
                            {
                                //Console.WriteLine("WebDavClientController -> getSingWebDavAsync ->  Ошибка удаление файла location не найден  row_id: " + modelServer[0]);
                                _exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> getSingWebDavAsync ->  Ошибка удаление файла location не найден :", "Id файла " + modelServer[0]);
                            }
                            list[k] = null;
                        }
                        else
                        {
                           // Console.WriteLine("WebDavClientController -> getSingWebDavAsync ->  Ошибка Удаление файла  row_id: " + modelServer[0]);
                            _exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> getSingWebDavAsync ->  Ошибка Удаление файла  row_id:", "Id файла "+ modelServer[0]);
                        }

                    }
                }
                
         }
         catch (AggregateException z)
         {
                //здесь нужно реализовать пересинхронизацию файлов если произойдет ошибка
                _exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> StartDeleteFilesWebDav ->  Критическая ошибка удаления файла AggregateException:", z.ToString());
         }
         catch (WebDAVClient.Helpers.WebDAVException z)
         {
                _exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> StartDeleteFilesWebDav -> Критическая ошибка удаления файла AggregateException:", z.ToString());
         }

          
        }

        private async Task checkDeleteFolder(DeleteFileModel delete, DeleteFilesAsyncModel DeleteModel , WebDavClient _webdavclient)
        {
            
                Client client = _webdavclient.CreateNewConnect(DeleteModel.username, DeleteModel.password);

                bool check = await DeleteModel._webdavClientExist.isExist(delete.location);
                if (check)
                {
                    _webdavclient.deleteFilesAsync(delete.location, DeleteModel.username, DeleteModel.password, true, client).Wait();
                }
           
            
        }

        private void StartDeleteFolder(string deleteLocation, DeleteFilesAsyncModel DeleteModel, WebDavClient _webdavclient)
        {
            try
            {
                Client client = _webdavclient.CreateNewConnect(DeleteModel.username, DeleteModel.password);

                //если мы выбирали удалить папку
                //после удаления всех файлов целиком сносим все папки которые остались + выбранная папка
                DeleteFileModel delete = DeleteModel._sqlLiteController.getSelect().getSearchListTempByLocationDeleteFileModel(deleteLocation);


                if (delete != null)
                {
                    if (delete.isFolder)
                    {
                        _webdavclient.deleteFilesAsync(delete.location, DeleteModel.username, DeleteModel.password, true, client).Wait();

                        //проверяем что папка точно удалилась если это не так мы пытаемся удалит ее повторно!
                        Task.Run(() => checkDeleteFolder( delete,  DeleteModel,  _webdavclient));
                        
                    }
                    else
                    {
                        //после удаления всех файлов нам нужно удалять только папки(файлы не трогаем)
                        //await _webdavclient.deleteFilesAsync(delete.location, username, password, false, client);
                    }
                }
                else
                {
                    Console.WriteLine("WebDavClientController - > StartDeleteFolder: - > Критическая ошибка рутовая папка не найденна нужна пересинхронизация");
                }
            }
            catch(WebDAVClient.Helpers.WebDAVException z)
            {
                _exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> StartDeleteFolder -> Критическая ошибка удаления файла WebDAVException:", z.ToString());
            }
           
        }

        private void StartDeleteFolderWebDav(DeleteFilesAsyncModel DeleteModel , WebDavClient _webdavclient)
        {
            Client client = _webdavclient.CreateNewConnect(DeleteModel.username, DeleteModel.password);

            FolderNodes[] allFolders = DeleteModel.allDeleteFiles;

            for(int k =0; k < allFolders.Length; k++)
            {
                string deleteLocation = allFolders[k].Location;
                StartDeleteFolder(deleteLocation, DeleteModel, _webdavclient);
            }
        }
        private void StartDeleteFilesWebDav(DeleteFilesAsyncModel DeleteModel, List<long[][]> allList, WebDavClient _webdavclient)
        {
            for (int w = 0; w < allList.Count; w++)
            {
                long[][] list = allList[w];
                StartDeleteFilesWebDav(list, DeleteModel, _webdavclient);
            }
        }
        
             //удаляем сначало всех детей с типом type- files
            //потом удаляем всех детей с типом folder
            public async Task StartDeleteWebDavClient(DeleteFilesAsyncModel deleteModel , List<long[][]> allList, int numberOperation , WebDavClient _webdavclient)
            {

                 bool check = await deleteModel._webdavClientExist.isExist(deleteModel.location);

                 if(check)
                 {
                        StartDeleteFilesWebDav(deleteModel, allList, _webdavclient);
                        StartDeleteFolderWebDav(deleteModel, _webdavclient);
                 }
               

                string Event = "DELETE";
                //очищаем таблицу от временных записей
                deleteModel._sqlLiteController.removeTablelistFileTempChangeRows(Event, numberOperation);
                Console.WriteLine("Удаление файлов и папок  завршенно! кол-во: " + allList[0].Length);
               
            }

        
    }
}
