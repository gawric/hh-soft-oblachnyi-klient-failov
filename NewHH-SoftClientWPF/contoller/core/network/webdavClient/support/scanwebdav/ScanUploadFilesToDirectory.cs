﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.scanupload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.contoller.view.update.updateProgressBarTransfer.upload.supportUpload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Progress;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.ScanWebDav
{
    class ScanUploadFilesToDirectory
    {
        private Client _client;
        private SupportScanUploadFilesDirectory supportScanUploadFilesDirectory;

        public ScanUploadFilesToDirectory(Client client)
        {
            _client = client;
            supportScanUploadFilesDirectory = new SupportScanUploadFilesDirectory();
        }

        public async Task  UploadRecursiveFilesInClient(ScanUploadWebDavModel uploadFolderModel, int[] cont)
        {
            try
            {
                IDirectoryInfoProxy di = getDirectory(uploadFolderModel.hrefFolder);

              //  IFileInfoProxy[] currentDiFilesArr = di.GetFiles();

                supportScanUploadFilesDirectory.insetCountToProgressBarFolder(uploadFolderModel);

                await supportScanUploadFilesDirectory.processFiles(di, uploadFolderModel, cont);

             

                if (di != null)
                {
                   

                    foreach (DirectoryInfo fi in di.GetDirectories())
                    {
                        IDirectoryInfoProxy fid = new DirectoryInfoProxy(fi.FullName);

                        if (fid.GetDirectories().Length > 0)
                        {
                            //Нашли еще одну дерикторию
                            reassignFiles(fid, uploadFolderModel);
                            await UploadRecursiveFilesInClient(uploadFolderModel, cont);

                        }
                        else
                        {

                            
                            //в дериктории больше нет других дерикторий
                            reassignFiles(fid, uploadFolderModel);
                            await supportScanUploadFilesDirectory.processFiles(fid, uploadFolderModel,  cont);

                        }

                    }
                }
            }
            catch (Exception npe)
            {
                throw;
            }
           

        }

        private void reassignFiles(IDirectoryInfoProxy fi , ScanUploadWebDavModel uploadFolderModel)
        {
            uploadFolderModel.hrefFolder = fi.FullName();
            uploadFolderModel.folderName = fi.Name();
        }
        private IDirectoryInfoProxy getDirectory(string href)
        {
            return new DirectoryInfoProxy(href);
        }

    }
}
