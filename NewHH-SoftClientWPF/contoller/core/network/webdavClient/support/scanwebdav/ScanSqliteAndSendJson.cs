﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.genNewId;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    public class ScanSqliteAndSendJson
    {
        private GenNewIdFim genNewId;
        public ScanSqliteAndSendJson()
        {
            genNewId = new GenNewIdFim();
        }
        //Добавляет файлы в JSON формат и отправляет их на сервер пачками по x шт
        //после обработки серваком они возвращаются и удаляются на сервере
        public void StartSendPaste(long[][][] list, PasteFilesAsyncModel PasteModel)
        {
 
            try
            {
                //массив для хранения
                FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
                SendBegin(ref PasteModel, ref arr);
                Start(ref list, ref PasteModel, ref arr);
                SendEnd(ref PasteModel, ref arr);


            }
            catch (Exception)
            {
                throw;
            }



        }

        
        private void Start(ref long[][][] list, ref PasteFilesAsyncModel pasteModel , ref FileInfoModel[] arr)
        {
            int cnt = 0;

            //list.lenght = начинает счет с 1, а массив с начинается с 0
            for (int k = 0; k < list.Length; k++)
            {

                StartSendCopy(cnt, pasteModel, list[k], arr);
                SendUpdate(ref pasteModel, ref arr);
                clearArr(ref arr);
            }
        }
        private void clearArr(ref FileInfoModel[] arr)
        {
            for(int k = 0; k < arr.Length; k++)
            {
                if(arr[k] != null)
                {
                    arr[k] = null;
                }
            }
        }
        private void StartSendCopy(int cnt , PasteFilesAsyncModel pasteModel , long[][]listIndex , FileInfoModel[] arr)
        {
            for(int k = 0; k < listIndex.Length; k++)
            {
                long[] modelId = listIndex[k];

                if (modelId != null)
                {


                    sendParties1000(ref cnt, ref arr, ref pasteModel);

                    long row_id = modelId[0];
                    long new_row_id = modelId[2];
                    long new_parent_id = modelId[3];

                    FileInfoModel currentNodes = GetCurrentNodes(pasteModel._sqlLiteController, ref row_id);

                    injectCurrent(ref arr, ref cnt, ref currentNodes);

                    //нужно создать новый путь
                    string g3 = createNewPath(ref currentNodes, pasteModel.copyLocation);
                    string copyOrCutRootLocation = pasteModel.copyOrCutRootLocation;

                    pasteModel.copyOrCutRootLocation = getRootLocation(staticVariable.UtilMethod.isDisk(ref copyOrCutRootLocation), ref copyOrCutRootLocation);
                    genNewId.genNewLocation(ref g3, ref arr, ref cnt, ref currentNodes, ref pasteModel , pasteModel.pasteFolder.Location);


                    injectNewParent(ref arr, ref cnt, ref new_row_id, ref new_parent_id);
                    cnt++;


                }


            }


        }

        private void injectNewParent(ref FileInfoModel[] arr , ref int cnt , ref long new_row_id , ref long new_parent_id)
        {
            //новый родитель
            arr[cnt].parent = new_parent_id;
            //новый id
            arr[cnt].row_id = new_row_id;
            arr[cnt].saveTopc = 0;
        }
        private string createNewPath(ref FileInfoModel currentNodes , string copyLocation)
        {
            return currentNodes.location.Replace(copyLocation, "");
        }
        private void injectCurrent(ref FileInfoModel[] arr , ref int cnt , ref FileInfoModel currentNodes)
        {
            //перезаписываем данные т.к это копия и у него новый id и новый location
            arr[cnt] = currentNodes;

            //метка, что это создание файла т.к вставка
            arr[cnt].changeRows = "CREATE";
        }

        private FileInfoModel GetCurrentNodes(SqliteController sqlLiteController, ref long row_id)
        {
            return sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_id);
        }

       private void sendParties1000(ref int cnt , ref FileInfoModel[] arr , ref PasteFilesAsyncModel pasteModel)
       {
            if (cnt == staticVariable.Variable.CountFilesTrasferServer)
            {

                SendUpdate(ref pasteModel, ref arr);

                //скидываем все файлы во временное хранилизе listFileTemp
                //что-бы потом их вытащить когда будет удалять файлы из webDav

                clearArr(ref arr);

                cnt = 0;

            }
        }
       

       

      

        private string getRootLocation(bool isDisk , ref string copyOrCutRootLocation)
        {
           
            if(isDisk)
            {
                copyOrCutRootLocation = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;
            }

            return copyOrCutRootLocation;
        }

        private void SendEnd(ref PasteFilesAsyncModel pasteModel, ref FileInfoModel[] arr)
        {
            pasteModel.updateNetworkJsonController.updateSendJsonPASTEENDPARTIES(arr, pasteModel.newVersionBases);
        }
        private void SendBegin(ref PasteFilesAsyncModel pasteModel, ref FileInfoModel[] arr)
        {
            pasteModel.updateNetworkJsonController.updateSendJsonPASTEBEGINPARTIES(arr, pasteModel.newVersionBases);
        }

        private void SendUpdate(ref PasteFilesAsyncModel pasteModel, ref FileInfoModel[] arr)
        {
            pasteModel.updateNetworkJsonController.updateSendJsonPASTEPARTIES(arr, pasteModel.newVersionBases);

        }
    }
}
