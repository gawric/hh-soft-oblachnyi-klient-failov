﻿using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.contoller.view.update.updateProgressBarTransfer.upload.supportUpload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.scanupload
{
    public class SupportScanUploadFilesDirectory
    {
        public async Task processFiles(IDirectoryInfoProxy di, ScanUploadWebDavModel uploadFolderModel, int[] cont)
        {
            if (cancel(uploadFolderModel))
            {
                uploadFolderModel.allFolderDictionary = new Dictionary<string, long>(0);
                // insetStopProgressBarFolder(uploadFolderModel);
                return;
            }

            IFileInfoProxy[] currentDiFilesArr3 = di.GetFiles();

            forDi(currentDiFilesArr3, uploadFolderModel , ref cont);

        }

        public void forDi(IFileInfoProxy[] currentDiFilesArr3 , ScanUploadWebDavModel uploadFolderModel , ref int[] cont)
        {
            
            try
            {

                for (int d = 0; d < currentDiFilesArr3.Length; d++)
                {
                    IFileInfoProxy fi = currentDiFilesArr3[d];

                    //Ставит на паузу поток. из ViewTransfer
                    uploadFolderModel._blockingEventController.waitOneMRE(staticVariable.Variable.blockingUploadTokenId);

                    if (fi.Exists())
                    {


                        if (cancel(uploadFolderModel))
                        {
                            clearTemp(ref currentDiFilesArr3, ref uploadFolderModel);
                            break;
                        }

                        run(ref fi, uploadFolderModel, ref cont);

                    }

                }
            }
            catch (System.Collections.Generic.KeyNotFoundException s)
            {
                throw;
            }
            //  catch (System.Net.Http.HttpRequestException b)
            // {
            //     throw;
            // }
            catch (System.IO.IOException z)
            {
                throw;

            }
            // catch (WebDAVClient.Helpers.WebDAVException z)
            // {
            //  throw;
            //}
            catch (System.AggregateException w)
            {
                if (!cancel(uploadFolderModel))
                {
                    throw;
                }

            }
        }


        public void run(ref IFileInfoProxy fi, ScanUploadWebDavModel uploadFolderModel, ref int[] cont)
        {
            FileInfoModel model_source = createNewModelSource(uploadFolderModel, fi);


            bool isExists = uploadFolderModel.checkParent.checkPasteFolderToDouble(model_source.location, uploadFolderModel.allHrefFolder);

            if (isExists != true)
            {
                sendJsonUpdate(cont, uploadFolderModel.verison_dabases, uploadFolderModel);
                upload(ref model_source, ref uploadFolderModel, ref cont, ref fi);
            }

            uploadFolderModel.currentCountFilesToFolder[0]++;
            insetCountToProgressBarFolder(uploadFolderModel);
        }


        private void upload( ref FileInfoModel model_source , ref ScanUploadWebDavModel uploadFolderModel, ref int[] cont , ref IFileInfoProxy fi)
        { 
            //такой же есть в начале перебора
           //т.к заливаются файлы 1 уровня, а здесь файлы из папок и подпапок
            uploadStartFiles(model_source, fi, uploadFolderModel);
            insertArr(model_source, uploadFolderModel, cont);
            cont[0]++;

        }
        private void clearTemp(ref IFileInfoProxy[] currentDiFilesArr3 , ref ScanUploadWebDavModel uploadFolderModel)
        {
            currentDiFilesArr3 = new IFileInfoProxy[0];
            uploadFolderModel.allFolderDictionary = new Dictionary<string, long>(0);
            insetStopProgressBarFolder(uploadFolderModel);
        }

        private void insertArr(FileInfoModel sourceModel, ScanUploadWebDavModel uploadFolderModel, int[] cont)
        {
            uploadFolderModel.arr[cont[0]] = sourceModel;
        }


        public void insetCountToProgressBarFolder(ScanUploadWebDavModel uploadFolderModel)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = uploadFolderModel.currentCountFilesToFolder[0];
            args.sizeFilesFolder = uploadFolderModel.allCountFilesToFiles;
            args.training = false;
            //отправка в обработку
            uploadFolderModel.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        private void uploadStartFiles(FileInfoModel model_source, IFileInfoProxy fi, ScanUploadWebDavModel uploadFolderModel)
        {
            if(File.Exists(fi.FullName()))
            {
                using (var fileStream = File.OpenRead(fi.FullName()))
                {
                    IDownload statusDownload = pb(model_source, uploadFolderModel);
                    try
                    {
                        uploadFolderModel.client.Upload(statusDownload, uploadFolderModel.uploadFolderWebDav, fileStream, fi.Name()).Wait();
                    }
                    catch (Exception s)
                    {
                        throw;
                    }

                }
            }
            

        }

        private IDownload pb(FileInfoModel model_source, ScanUploadWebDavModel uploadFolderModel)
        {
            IDownload statusDownload = createIDownload(model_source, uploadFolderModel);
            UpdateVtPgDetails updateVtPgDetails = createPb(statusDownload, model_source, uploadFolderModel);
            subsctiblePb(updateVtPgDetails, statusDownload);

            return statusDownload;
        }

        private UpdateVtPgDetails createPb(IDownload statusDownload, FileInfoModel model_source, ScanUploadWebDavModel uploadFolderModel)
        {
            return new UpdateVtPgDetails(statusDownload, uploadFolderModel.modelSource.Row_id, uploadFolderModel);
        }

        private void subsctiblePb(UpdateVtPgDetails update, IDownload download)
        {
            update.subsribleUpdateFiles(download);
        }


        private IDownload createIDownload(FileInfoModel fileInfoModel, ScanUploadWebDavModel uploadFolderModel)
        {
            IDownload statusDownload = new Download();

            statusDownload.setMre(uploadFolderModel._blockingEventController.getManualReset(staticVariable.Variable.blockingUploadTokenId).getManualResetEvent());
            statusDownload.setRow_id(fileInfoModel.row_id);
            statusDownload.setCancel(uploadFolderModel._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.uploadCancelationTokenId));

            return statusDownload;
        }

        private bool cancel(ScanUploadWebDavModel uploadFolderModel)
        {
            CancellationToken cancellation = uploadFolderModel._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.uploadCancelationTokenId);

            return cancellation.IsCancellationRequested;

        }

        private void insetStopProgressBarFolder(ScanUploadWebDavModel uploadFolderModel)
        {
            //-1 означает что скрипт был отменен
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = -1;
            args.sizeFilesFolder = -1;
            args.training = false;
            args.isStop = true;
            //отправка в обработку
            uploadFolderModel.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        private FileInfoModel createNewModelSource(ScanUploadWebDavModel uploadFolderModel, IFileInfoProxy fi)
        {
            WebDavHrefModel hrefModel = new WebDavHrefModel();
            hrefModel.changeDate = fi.LastWriteTime().ToString();
            hrefModel.lastOpenDate = fi.LastAccessTime().ToString();
            hrefModel.size = fi.Length();
            hrefModel.createDate = fi.CreationTime().ToString();
            hrefModel.filename = fi.Name();

            //FileInfoModel currentModel = createFileInfoModelToFolder(uploadFolderModel, hrefModel);
            uploadFolderModel.clientLocalDirectory = fi.FullName();


            return createFileInfoModelToFolder(uploadFolderModel, hrefModel);
        }

        private FileInfoModel createFileInfoModelToFolder(ScanUploadWebDavModel uploadModel, WebDavHrefModel hrefModel)
        {
            FileInfoModel model = new FileInfoModel();

            model.filename = hrefModel.filename;
            model.lastOpenDate = hrefModel.lastOpenDate;
            model.createDate = hrefModel.createDate;
            model.changeDate = hrefModel.changeDate;

            //Генерирует новые id номера
            model.row_id = generatedRowId(uploadModel);
            //generateNewIdUpload(uploadModel);
            model.type = "files";
            model.parent = uploadModel.pasteModel.row_id;
            model.location = uploadModel.pasteModel.location + staticVariable.Variable.EscapeUrlString(hrefModel.filename);
            model.sizebyte = hrefModel.size;
            model.changeRows = "CREATE";
            model.attribute = "";
            model.versionUpdateBases = uploadModel.verison_dabases;
            //генерация родителя
            string parentHref = CreateNewDirectoryToWebDav(uploadModel);
            //string currentparentHref = String.Copy(parentHref);
            //создание нового пути
            model.location = parentHref + staticVariable.Variable.EscapeUrlString(hrefModel.filename);
            uploadModel.uploadFolderWebDav = parentHref;
            model.parent = searchParentFileInfo(ref uploadModel, hrefModel, parentHref, model);
            model.user_id = existsUserIdListFiles(ref uploadModel);

          

            return model;
        }
        private long existsUserIdListFiles(ref ScanUploadWebDavModel uploadModel)
        {
            long user_id = 0;
            if(uploadModel._sqlLiteController != null) uploadModel._sqlLiteController.getSelect().existUserIdToListFiles();
            return user_id;
        }
        private string CreateNewDirectoryToWebDav(ScanUploadWebDavModel uploadFolderModel)
        {
            //полный путь к папке изменяем все на unix систему
            string clearLocalHref = uploadFolderModel.clientLocalDirectory.Replace("\\", "/");
            //папка откуда все копируется
            string clearRootHref = uploadFolderModel.clientRootLocalDirectory.Replace("\\", "/");
            //находим последнее вхождение обычно это имя файла
            int indexSl = clearLocalHref.LastIndexOf("/") + 1;

            //отсикаем конечное название оставляем путь до parent папки
            string clearLocalHrefNewEnd = clearLocalHref.Substring(0, indexSl);

            //находим главную папку где нажали кнопку копировать и меняем на рутовый путь сервера
            string newhref = clearLocalHrefNewEnd.Replace(clearRootHref, uploadFolderModel.pasteRootWebDavDirectory);
            string parentHref = staticVariable.Variable.EscapeUrlString(newhref);


            return equalsParazit(parentHref);

        }

        //поиск паразитной // - в конце за место 1
        private string equalsParazit(string parentHref)
        {
            int end = parentHref.Length;
            string endstr = parentHref.Substring(end - 2);

            if (endstr.Equals("//"))
            {
                parentHref = parentHref.Substring(0, parentHref.Length - 1);
                return parentHref;
            }
            else
            {
                return parentHref;
            }
        }

        private long generatedRowId(ScanUploadWebDavModel uploadModel)
        {

            long newRow_id = uploadModel.new_id_container[0] = getRowId(ref uploadModel);
            insertId(uploadModel, ref newRow_id);
            return newRow_id;
        }
        private void insertId(ScanUploadWebDavModel uploadModel , ref long newRow_id)
        {
            if(uploadModel._sqlLiteController != null) uploadModel._sqlLiteController.getInsert().insertListFilesTempIndex(newRow_id);

        }
        private long getRowId(ref ScanUploadWebDavModel uploadModel)
        {
            long id = 0;

            if(uploadModel._sqlLiteController != null) id = staticVariable.Variable.generatedNewRowId(uploadModel._sqlLiteController.getSelect().getLastRow_idFileInfoModel(), null);
            //если id не сгенерировался нормально пытаемся еще раз
            if (id == 0)
            {
                if (uploadModel._sqlLiteController != null)
                {
                    id = staticVariable.Variable.generatedNewRowId(uploadModel._sqlLiteController.getSelect().getLastRow_idFileInfoModel(), null);
                    Console.WriteLine("Попытка генерации нового id номера " + id);
                }
                   
            };
            

            return id;

        }

        private long searchParentFileInfo(ref ScanUploadWebDavModel uploadModel, WebDavHrefModel hrefModel, string parenthref, FileInfoModel model)
        {
            if(uploadModel._sqlLiteController != null)
            {
                FileInfoModel parentSql = uploadModel._sqlLiteController.getSelect().getSearchNodesByLocation(parenthref);

                if (parentSql != null)
                {
                    if (uploadModel.allFolderDictionary.ContainsKey(parenthref) != true)
                    {
                        uploadModel.allFolderDictionary.Add(parenthref, parentSql.row_id);
                    }

                    if (uploadModel.allFolderDictionary.ContainsKey(model.location) != true)
                    {
                        uploadModel.allFolderDictionary.Add(model.location, model.row_id);
                    }

                    return parentSql.row_id;
                }
                else
                {

                    if (uploadModel.allFolderDictionary.ContainsKey(model.location) != true)
                    {
                        uploadModel.allFolderDictionary.Add(model.location, model.row_id);
                    }

                    return uploadModel.allFolderDictionary[parenthref];
                }
            }
            else
            {
                return 0l;
            }
           
        }

        private void sendJsonUpdate(int[] cnt, long version, ScanUploadWebDavModel uploadModel)
        {
            if (cnt[0] == staticVariable.Variable.CountFilesTrasferServer)
            {


                //uploadModel.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(uploadModel.arr, version);
                AddUpdateEvent(version, uploadModel.arr, uploadModel);

                for (int s = 0; s < uploadModel.arr.Length; s++)
                {
                    uploadModel.arr[s] = null;
                }

                cnt[0] = 0;

            }
        }

       

        public void AddUpdateEvent(long version, FileInfoModel[] arr , ScanUploadWebDavModel uploadModel)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = uploadModel.networkJsonController;
            OnEventUpdate(args , uploadModel);
        }

        protected virtual void OnEventUpdate(UploadFolderEventHandler e , ScanUploadWebDavModel uploadModel)
        {
            uploadModel.updateEventHandler?.Invoke(this, e);
        }

    }
}
