﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support.StackRun;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support
{
    public class SupportUpDownStack
    {
        private SqliteController sql;
        private UpdateViewTransferController utc;
        private ConvertIconType ci;
        private MainWindowViewModel vm;
        public SupportUpDownStack(SqliteController sqlLiteController , UpdateViewTransferController utc , ConvertIconType ci , MainWindowViewModel viewMain)
        { 
            this.sql = sqlLiteController;
            this.utc = utc;
            this.ci = ci;
            this.vm = viewMain;

        }
        public void createUploadTempPb(string[] selectHref, ref StackNetworkModel modelUpload)
        {
            foreach (string item in selectHref)
            {
                if (File.Exists(item))
                {
                    FileInfo fi1 = new FileInfo(item);
                    long row_id = createTempProgress("upload", fi1.Name, "files");
                    modelUpload.dictHrefUploadRowId.Add(item, row_id);
                }
                else
                {
                    DirectoryInfo di = new DirectoryInfo(item);
                    long row_id = createTempProgress("upload", di.Name, "folder");
                    modelUpload.dictHrefUploadRowId.Add(item, row_id);
                }
            }
        }

        public StakRunObjectModel createModel(SqliteController sqlLiteController, WebDavClientController webDavClient, NavigationMainWindowsController navigationMainController, ViewFolderViewModel viewModel, MainWindowViewModel viewMain)
        {
            StakRunObjectModel model = new StakRunObjectModel();
            model.sqlLiteController = sqlLiteController;
            model.webDavClient = webDavClient;
            model.navigationMainController = navigationMainController;
            model.viewModel = viewModel;
            model.viewMain = viewMain;

            return model;
        }


        public long createTempProgress(string status, string nameNodes, string folder)
        {
            long newRowId = staticVariable.Variable.generatedNewRowId(sql.getSelect().getLastRow_idFileInfoModel(), null);
            sql.getInsert().insertListFilesTempIndex(newRowId);
            //-status - download | upload
            // folder = string folder или files
            FolderNodesTransfer modelSource = createTransferModel(staticVariable.Variable.generatedNewRowId(newRowId, null), nameNodes, 0, status, folder);
            utc.createProgressBar(modelSource, modelSource.FolderName);

            return modelSource.Row_id;
        }


        public FolderNodesTransfer createTransferModel(long row_id, string folderName, long sizeByteFolder, string typeTransfer, string type)
        {
            FolderNodesTransfer model = new FolderNodesTransfer();
            model.Row_id = row_id;
            model.Sizebyte = staticVariable.UtilMethod.FormatBytes(0);
            model.Type = type;
            model.uploadStatus = "Ожидает";
            model.Progress = 0;
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                model.Icon = ci.getIconType(type);
            });

            model.FolderName = folderName;
            model.TypeTransfer = typeTransfer;
            model.IconTransfer = ci.getIconTransfer(typeTransfer);
            model.Sizebyte = staticVariable.UtilMethod.FormatBytes(sizeByteFolder);



            return model;
        }

        //требует тест
        public void rebuildingSqlSize(object sender, DataEventHandler e)
        {
            if (vm != null)
            {
                InfoSqlSize infoSql = new InfoSqlSize(sql);
                App.Current.Dispatcher.Invoke(new System.Action(() => vm.PgSizeDisk = infoSql.getSize()));
            }

        }

        public void addItemScanFolder(object sender, EventScanFolderItemModel e)
        {
            if(e.arr != null)
            {
                sql.getInsert().InsertAutoSaveFileTransaction(e.arr, 0);
            }
        }

    }
}
