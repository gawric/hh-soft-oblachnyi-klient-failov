﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support
{
    public class StackRun
    {
        //пересчитывает сколько занято на диске
        public event EventHandler<DataEventHandler> eventRebuilding;
        private IInfoStak infoStack;
        private List<FileInfoModel> tempFileSubsribleList;
        private int size = 0;
        public StackRun()
        {
            infoStack = new InfoStak();
            tempFileSubsribleList = new List<FileInfoModel>();
        }
        public void startStackNetwork(ref BlockingCollection<StackNetworkModel> _dirs , ref bool isRunningStack , ref bool isStopStack, StakRunObjectModel modelObject)
        {
            try
            {
                while (true)
                {
                    StackNetworkModel dir = _dirs.Take();
                    isRunningStack = true;
                    
                    startNetwork(ref dir , modelObject ,ref _dirs);
                    Debug.Print("StackRun->startStackNetwork: обработанной очереди  "+ size++ + "в ней осталось "+ _dirs.Count);
                    if (isStopStack)
                    { break; }
                }
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("StackOperationUpDown->startStackNetwork: Бесконечный цикл завершен!");
            }

            isRunningStack = false;
        }

      

        private void startNetwork(ref StackNetworkModel model , StakRunObjectModel modelObject , ref BlockingCollection<StackNetworkModel> dirs)
        {
            if (model != null)
            {
                model.infoStack = infoStack.GetInfoStak(ref dirs);
                model.tempFileSubsribleList = tempFileSubsribleList;
                start(ref model, ref modelObject);
            }
        }
       
        private void start(ref StackNetworkModel model , ref StakRunObjectModel modelObject)
        {
            if (!isNullObjectUpload(ref model))
            {
                upload(ref  model, ref  modelObject);
            }
            else
            {
                if (!isNullObjectDownload(ref model))
                {

                    download(ref model, ref modelObject);
                }
            }
            
        }

        private void download(ref StackNetworkModel model , ref StakRunObjectModel modelObject)
        {
            if (model.countDownload > 0)
            {
                donwload(ref model, modelObject);
            }
        }

        private void upload(ref StackNetworkModel model, ref StakRunObjectModel modelObject)
        {
            if (model.hrefUpload.Length > 0)
            {
                upload(ref model, modelObject.sqlLiteController, modelObject.webDavClient , model.tempFileSubsribleList);
                clickEventRebuilding();
            }
        }

        private bool isNullObjectDownload(ref StackNetworkModel model)
        {
            bool check = true;
            if (model.listDownload != null) check = false;
            
            return check;
        }

        private bool isNullObjectUpload(ref StackNetworkModel model)
        {
           // bool check = true;
            if (model.hrefUpload != null) return false;
           
            return true;
        }

        private void upload(ref StackNetworkModel model, SqliteController sqlLiteController, WebDavClientController webDavClient , List<FileInfoModel> tempFileSubsribleList)
        {
            string[] selectHref = model.hrefUpload;
            FileInfoModel pasteModel = model.pasteFolder;
            if(webDavClient != null) webDavClient.uploadWebDav(sqlLiteController, selectHref, pasteModel, model.dictHrefUploadRowId , model.infoStack , tempFileSubsribleList);

        }

        private void donwload(ref StackNetworkModel model, StakRunObjectModel modelObject)
        {
            List<FolderNodes> listDownload = model.listDownload;
            if (modelObject.webDavClient != null) modelObject.webDavClient.downloadWebDav(modelObject.sqlLiteController, modelObject.navigationMainController, listDownload, model.dictHrefDownloadRowId, modelObject.viewModel, modelObject.viewMain, model.isViewFolder);
        }

      

        private void clickEventRebuilding()
        {
            DataEventHandler args = new DataEventHandler();
            OnThresholdReached(args);
        }

      
        protected virtual void OnThresholdReached(DataEventHandler e)
        {
            eventRebuilding?.Invoke(this, e);
        }

       

        public class DataEventHandler : EventArgs
        {
            public int data1 { get; set; }
            public DateTime data2 { get; set; }
        }

        

    }
}
