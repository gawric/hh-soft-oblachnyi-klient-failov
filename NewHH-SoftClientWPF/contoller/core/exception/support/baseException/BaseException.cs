﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using static NewHH_SoftClientWPF.contoller.core.exception.BaseErrorCode;

namespace NewHH_SoftClientWPF.contoller.core.exception.support.basesException
{
    public class BaseException : Exception
    {
        private BaseErrorCode errorCode;
        private String description;
        private String errorThrow;

        public BaseException() { }
        protected BaseException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public BaseException(string message):base(message){ }

        public BaseException(int errorCode, String message , String errorThrow) : base(message)
        {
            this.errorCode = new BaseErrorCode(errorCode);
            this.errorThrow = errorThrow;
            this.description = message;
        }
        
        public BaseErrorCode getErrorCode()
        {
            return errorCode;
        }
    }
}
