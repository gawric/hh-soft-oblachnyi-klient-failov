﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using NewHH_SoftClientWPF.view;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.contoller.error.support.observable
{
    /// <summary>
    /// Наблюдатель.
    /// </summary>
    public class Observer
    {
       
        private ErrorAlertModelObj _errorAlertModelObj;
        private ErrorAlertWindowViewModel _viewModelError;


        /// <param name="subject">Наблюдаемый объект.</param>
        public Observer(ExceptionHandler subject , ErrorAlertModelObj errorAlertModelObj , ErrorAlertWindowViewModel viewModelError)
        {
            _errorAlertModelObj = errorAlertModelObj;
            _viewModelError = viewModelError;
            subject.OnSaved += SaveErrorHandler;
        }

        /// <summary>
        /// Обработчик события сохранения наблюдаемого объекта.
        /// </summary>
        /// <param name="sender">Наблюдаемый объект.</param>
        /// <param name="e">Аргументы события.</param>
        private void SaveErrorHandler(object sender, EventArgs e)
        {
           
            CustomErrorEventArgs args = (CustomErrorEventArgs)e;
            int key = args.getErrorCode;

            string stringErrorName = Enum.GetName(typeof(CodeError), key);
         

            openErrorWindow(stringErrorName, args.getErrorText, args.getThrowError);
        }

       
        public void openErrorWindow(string stringErrorName , string errorText , string errorThrow)
        {
            try
            {
                _errorAlertModelObj.isUIThread = checkUIThread();

                _viewModelError.ErrorText = stringErrorName;
                _viewModelError.TextBlock = errorThrow;
                _viewModelError.NameWindow = "Критическая ошибка";

                if (_errorAlertModelObj.isUIThread)
                {
                    startUIThread(_errorAlertModelObj);
                }
                else
                {
                    startOtherThread(_errorAlertModelObj);
                }
               


            }
            catch(System.AggregateException z)
            {
                Console.WriteLine("Observer exception-> openErrorWindow Критическая ошибка "+z.ToString());
            }
            catch (System.InvalidOperationException z)
            {
                Console.WriteLine("Observer exception-> openErrorWindow Критическая ошибка " + z.ToString());
            }

        }

        private bool checkUIThread()
        {
            return  _errorAlertModelObj._dispatcherThread == Thread.CurrentThread;
        }
        private void startOtherThread(ErrorAlertModelObj _errorAlertModelObj)
        {
            try
            {
                Thread viewerThread = new Thread(delegate ()
                {
                    
                  //_viewModelError.NameWindow = "Отчет о работе";
                   // _viewModelError.ErrorText = "Уведомление для разработчиков!";

                    ErrorAlertWindow errorAlert = new ErrorAlertWindow(_errorAlertModelObj , _viewModelError);
                    errorAlert.Show();
                    System.Windows.Threading.Dispatcher.Run();
                });

                viewerThread.SetApartmentState(ApartmentState.STA); // needs to be STA or throws exception
                viewerThread.Start();

            }
            catch(System.InvalidOperationException s)
            {
                Console.WriteLine("Observer->startOtherThread: Критическая ошибка !!! " + s.ToString());
            }
            catch (System.Exception s)
            {
                Console.WriteLine("Observer->startOtherThread: Критическая ошибка !!! " + s.ToString());
            }

        }

        private void startUIThread(ErrorAlertModelObj _errorAlertModelObj)
        {
            try
            {
                ErrorAlertWindow errorAlert = new ErrorAlertWindow(_errorAlertModelObj , _viewModelError);
                errorAlert.Show();
            }
            catch (System.Exception s)
            {
                Console.WriteLine("Observer->startOtherThread: Критическая ошибка !!! " + s.ToString());
            }

        }


 

    }


}
