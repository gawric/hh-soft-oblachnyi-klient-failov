﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.exception
{
    public class  BaseErrorCode : ErrorCode
    {
        private int code;
   
        public BaseErrorCode(int code)
        {
            this.code = code;
        }
        public int getCode()
        {
            return code;
        }

        public void setCode(int code)
        {
            this.code = code;
        }
      
    }

    public enum CodeError
    {
        UNKNOWN = 99,
        ACTIVEMQ_NO_CONNECTED = 100,
        ACTIVEMQ_NOT_SEND = 101,
        NETWORK_NOT_RESPONDING = 102,
        OBJECT_IS_EMPTY = 103,
        DATABASE_ERROR = 104,
        NETWORK_ERROR = 105,
        BROKER_ERROR = 106,
        NULL_POINT_EXCEPTION = 107,
        FILE_SYSTEM_EXCEPTION = 108
    }


}
