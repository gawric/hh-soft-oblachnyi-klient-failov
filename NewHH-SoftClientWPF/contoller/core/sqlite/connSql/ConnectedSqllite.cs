﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.sqlite.connectdb;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.connSql
{
    public class ConnectedSqllite
    {
        private string fileBasesDir = staticVariable.Variable.getBasesDir();
        private CreateConnectedSqlLite classcon;
        private ExceptionController error;

        public ConnectedSqllite(ExceptionController error)
        {
            this.error = error;
        }
        public void CreateBases()
        {
            classcon.CreateBases();
        }
        public SQLiteConnection GetConnectionSqlite()
        {
            try
            {

                if (classcon == null)
                {
                    classcon = new CreateConnectedSqlLite(error);
                    return classcon.getConnected(fileBasesDir);

                }
                else
                {
                    return classcon.getConnected(fileBasesDir);
                }


            }
            catch (SqlException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetConnectionSqlite() Критическая Ошибка подключения к базе данных sqlite", ex.ToString());


                return null;
            }


        }

        private BaseException generatedError(int codeError, string textError, string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError);
        }


    }
}
