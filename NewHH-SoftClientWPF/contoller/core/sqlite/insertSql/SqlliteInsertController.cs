﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.insertSql
{
    public class SqlliteInsertController
    {
        
        private ExceptionController error;
        private ConnectedSqllite classcon;
        private string fileBasesDir = staticVariable.Variable.getBasesDir();
        private StatusSqlliteTransaction statusTransaction;
        private ConvertIconType convertIcon;
        private InsertSqlLite insertSqlite;

        public SqlliteInsertController(ExceptionController error, ConnectedSqllite classcon, StatusSqlliteTransaction statusTransaction, ConvertIconType convertIcon)
        {

            this.error = error;
            this.classcon = classcon;
            this.statusTransaction = statusTransaction;
            this.convertIcon = convertIcon;
            insertSqlite = new InsertSqlLite(statusTransaction);

        }


        //вставляет юзера в базу данных
        public void insertUsernameAndPassword(string username, string password)
        {
            try
            {
                statusTransaction.isTransaction = true;

                insertSqlite.insertUsernameAndPassword(username, password, classcon.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertUsernameAndPassword() Критическая Ошибка вставки данных", ex.ToString());

            }

        }


     
        public void insertAutoScanInfo(AutoScanInfoModel autoScanInfoModel)
        {
            try
            {
                statusTransaction.isTransaction = true;

                insertSqlite.insertAutoScanInfo(autoScanInfoModel , classcon.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertAutoScanInfo() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public void insertSystemSetting(SystemSettingModel ssm)
        {
            try
            {
                statusTransaction.isTransaction = true;
               

                insertSqlite.insertSystemSetting(ssm, classcon.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertAutoScanInfo() Критическая Ошибка вставки данных", ex.ToString());

            }

        }



        public void insertAutoScanTimemill(string autoScanMill)
        {
            try
            {
                statusTransaction.isTransaction = true;

                insertSqlite.insertAutoScanTime(autoScanMill, classcon.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertUsernameAndPassword() Критическая Ошибка вставки данных", ex.ToString());

            }

        }


        public void insertListSavePc(List<long[][]> allListFull, int intId)
        {
            try
            {
                statusTransaction.isTransaction = true;

                insertSqlite.insertListSavePc(allListFull, classcon.GetConnectionSqlite(), intId);

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertListSavePc() Критическая Ошибка вставки данных", ex.ToString());

            }

        }



        //вставляет юзера в базу данных
        public void insertLocationDownload(string locationDownload)
        {
            try
            {
                // clearLocation();
                insertSqlite.insertLocationDownload(locationDownload, classcon.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertLocationDownload() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public void insertScanFolder(string fullPath)
        {
            try
            {

                insertSqlite.insertScanFolder(fullPath, classcon.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertScanFolder() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public void insertListScanFolder(ObservableCollection<ScanFolderModel> listScanFolder)
        {
            try
            {

                insertSqlite.insertListScanFolder(listScanFolder, classcon.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertScanFolder() Критическая Ошибка вставки данных", ex.ToString());

            }

        }



        
        public void insertListFilesTempIndex(long index)
        {
            try
            {

                insertSqlite.insertListFilesTempIndex(index, classcon.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertLocationDownload() Критическая Ошибка вставки данных", ex.ToString());

            }

        }
       
        public void insertFileListSinglRows(FileInfoModel modelServer)
        {
            try
            {
                WaitingTransaction(statusTransaction);

                statusTransaction.isTransaction = true;

                insertSqlite.insertFileListSinglRows(modelServer, classcon.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                statusTransaction.isTransaction = false;
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileInfoTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }
        }
        public void InsertFileInfoTransaction(List<FileInfoModel> list, MainWindowViewModel _viewModelMain)
        {

            try
            {
                WaitingTransaction(statusTransaction);

                statusTransaction.isTransaction = true;

                insertSqlite.insertFileListTransaction(list, classcon.GetConnectionSqlite(), _viewModelMain);

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                statusTransaction.isTransaction = false;
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileInfoTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }
        }

        //Добавляет во временное хранилище 
        //Полученные файлы после сканирования папки "На удаление"
        public void InsertFileTempTransaction(FileInfoModel[] list, int numberOperation)
        {

            try
            {

                checkActiveTransaction();

                statusTransaction.isTransaction = true;

                insertSqlite.insertFileTempTransaction(list, classcon.GetConnectionSqlite() ,  numberOperation);

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileTempTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }

        }

        public void InsertAutoSaveFileTransaction(FileInfoModel[] list, int numberOperation)
        {

            try
            {

                checkActiveTransaction();

                statusTransaction.isTransaction = true;

                insertSqlite.insertAutoSaveFileTransaction(list, classcon.GetConnectionSqlite(), numberOperation);

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileTempTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }

        }

        //Добавляет в хранилище transfer
        public void InsertFileTransferTransaction(FileInfoTransfer[] list)
        {

            try
            {

                checkActiveTransaction();

                statusTransaction.isTransaction = true;

                insertSqlite.insertFileTransferTransaction(list, classcon.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileInfoTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }

        }

        private BaseException generatedError(int codeError, string textError, string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError);
        }

        public void checkActiveTransaction()
        {
            int b = 0;
            //проверка что больше писателей у нас нет
            while (true)
            {

                if (statusTransaction.isTransaction == false)
                {
                    break;
                }

                if (b == 300)
                {
                    break;
                }

                Thread.Sleep(10);
                b++;
            }
        }

        public void WaitingTransaction(StatusSqlliteTransaction _statusTransaction)
        {
            //проверяем что другие потоки не пишут в нашу базу дынных
            int v = 0;
            while (true)
            {
                Console.WriteLine("SqliteController->WaitingTransaction: Ждем освобождения для записи   " + v);
                Thread.Sleep(100);
                if (_statusTransaction.isTransaction == false)
                {
                    break;
                }

                if (v == 300)
                {
                    break;
                }
                v++;
            }
        }

    }
}
