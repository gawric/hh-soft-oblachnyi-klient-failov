﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
    public class SelectFilesTemp
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
     
        public SelectFilesTemp(ExceptionController error, ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }

        public bool selectFilesTemp(string Event, int paretis, SQLiteConnection con)
        {

            bool check = false;

            string stm = "SELECT * FROM listFilesTemp WHERE row_id='" + paretis + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {

                            check = true;

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }

        //int - id номер последней операции
        public int getLastOperaionNumber(SQLiteConnection con)
        {
            int numberOperation = 0;

            string stm = "SELECT MAX(numberOperation) AS numberOperation FROM listFilesTemp;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            try
                            {
                                if (rdr.GetValue(0) != DBNull.Value)
                                {
                                    // numberOperation = Convert.ToInt32(rdr["numberOperation"].ToString()); 
                                    numberOperation = int.Parse(rdr["numberOperation"].ToString());
                                }

                            }
                            catch (System.IndexOutOfRangeException)
                            {
                                throw;
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }


            return numberOperation;
        }

        //List<> получаем все возможные записи по parentID
        public DeleteFileModel getSqlLiteTempByRow_idDeleteFileModel(long row_id, SQLiteConnection con)
        {

            DeleteFileModel deleteFiles = null;

            string stm = "SELECT location , type FROM listFilesTemp WHERE row_id = '" + row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {


                            deleteFiles = new DeleteFileModel();
                            deleteFiles.location = rdr["location"].ToString();
                            string type = rdr["type"].ToString();

                            if (staticVariable.Variable.isFolder(type))
                            {
                                deleteFiles.isFolder = true;
                            }
                            else
                            {
                                deleteFiles.isFolder = false;
                            }



                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return deleteFiles;
        }

        //List<> получаем все возможные записи по location
        public DeleteFileModel getSqlLiteTempByLocationDeleteFileModel(string location, SQLiteConnection con)
        {

            DeleteFileModel deleteFiles = null;

            string stm = "SELECT location , type FROM listFilesTemp WHERE location = '" + location + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {


                            deleteFiles = new DeleteFileModel();
                            deleteFiles.location = rdr["location"].ToString();
                            string type = rdr["type"].ToString();

                            if (staticVariable.Variable.isFolder(type))
                            {
                                deleteFiles.isFolder = true;
                            }
                            else
                            {
                                deleteFiles.isFolder = false;
                            }



                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return deleteFiles;
        }

       


    }
}
