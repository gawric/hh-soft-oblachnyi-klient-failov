﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
    public  class SelectFileInfo_Users_2
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
        public SelectFileInfo_Users_2(ExceptionController error, ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }


        public FileInfoModel getSearchNodesByRow_IdFileInfoModel(long row_id, SQLiteConnection con)
        {

            FileInfoModel clientModel = new FileInfoModel();

            string stm = "SELECT * FROM listFiles_users WHERE row_id = '" + row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {


                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.filename = rdr["filename"].ToString();
                            clientModel.createDate = rdr["createDate"].ToString();
                            clientModel.changeDate = rdr["changeDate"].ToString();
                            clientModel.lastOpenDate = rdr["lastOpenDate"].ToString();
                            clientModel.attribute = rdr["attribute"].ToString();
                            clientModel.location = rdr["location"].ToString();
                            clientModel.type = rdr["type"].ToString();
                            clientModel.sizebyte = long.Parse(rdr["sizebyte"].ToString());
                            clientModel.parent = long.Parse(rdr["parent"].ToString());
                            clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                            clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                            clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                            clientModel.changeRows = rdr["changeRows"].ToString();
                            clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return clientModel;
        }


        public DeleteFileModel getSqlLiteNodesByRow_idDeleteFileModel(long row_id, SQLiteConnection con)
        {

            DeleteFileModel deleteFiles = new DeleteFileModel();

            string stm = "SELECT location,type FROM listFiles_users WHERE row_id = '" + row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {



                            deleteFiles.location = rdr["location"].ToString();
                            string type = rdr["type"].ToString();

                            if (staticVariable.Variable.isFolder(type))
                            {
                                deleteFiles.isFolder = true;
                            }
                            else
                            {
                                deleteFiles.isFolder = false;
                            }



                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return deleteFiles;
        }


        public List<FileInfoModel> getSqlLiteNewRootFileInfoModelListFilterToFOlder(long ParentId, SQLiteConnection con)
        {

            List<FileInfoModel> fileList = new List<FileInfoModel>();

            string stm = "SELECT * FROM listFiles_users WHERE parent <= '" + ParentId + "' AND type = 'folder' ORDER BY filename;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {

                            FileInfoModel clientModel = new FileInfoModel();

                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.filename = rdr["filename"].ToString();
                            clientModel.createDate = rdr["createDate"].ToString();
                            clientModel.changeDate = rdr["changeDate"].ToString();
                            clientModel.lastOpenDate = rdr["lastOpenDate"].ToString();
                            clientModel.attribute = rdr["attribute"].ToString();
                            clientModel.location = rdr["location"].ToString();
                            clientModel.type = rdr["type"].ToString();
                            clientModel.sizebyte = long.Parse(rdr["sizebyte"].ToString());
                            clientModel.parent = long.Parse(rdr["parent"].ToString());
                            clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                            clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                            clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                            clientModel.changeRows = rdr["changeRows"].ToString();
                            clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);
                            fileList.Add(clientModel);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }


        //Получаем последнюю запись из listFiles_users
        public long getLastRow_idListFiles_users(SQLiteConnection con)
        {

            long last_row_id = 0;
            long last_rows_id_temp = -1;

            string stm = "SELECT row_id FROM listFiles_users ORDER BY  row_id DESC LIMIT 1;";
            try
            {
                last_rows_id_temp = getListFilesTempId(con);

                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {

                            last_row_id = (Int64)rdr.GetInt64(0);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            if (last_rows_id_temp > last_row_id)
            {
                last_row_id = last_rows_id_temp;
            }

            return last_row_id;
        }




        public Dictionary<long, List<long[]>> getAllParentListRow_id(SQLiteConnection con)
        {

            Dictionary<long, List<long[]>> arrayParent = new Dictionary<long, List<long[]>>();


            string stm = "SELECT row_id,parent,type FROM listFiles_users;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            long row_id_sqllite = Convert.ToInt32(rdr["row_id"]);
                            long parent = Convert.ToInt32(rdr["parent"]);
                            int type = getType(rdr["type"].ToString());
                            //string location = rdr["location"].ToString();
                            if (row_id_sqllite.Equals(33590))
                            {
                                Console.WriteLine();
                            }


                            if (arrayParent.ContainsKey(parent))
                            {
                                List<long[]> listRowId = arrayParent[parent];
                                long[] rowArr = { row_id_sqllite, type };
                                listRowId.Add(rowArr);
                            }
                            else
                            {
                                List<long[]> listRowId = new List<long[]>();
                                long[] rowArr = { row_id_sqllite, type };
                                listRowId.Add(rowArr);
                                arrayParent.Add(parent, listRowId);
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return arrayParent;
        }

        private int getType(string typeSql)
        {
            int type = 0;

            string type_str = typeSql;

            if (staticVariable.Variable.isFolder(type_str))
            {
                type = 1;
            }
            else
            {
                type = 0;
            }

            return type;
        }




        private long getListFilesTempId(SQLiteConnection con)
        {
            long last_row_id = 0;

            string stm = "SELECT row_id_temp FROM listFilesRowIdTemp;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {

                            last_row_id = (Int64)rdr.GetInt64(0);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return last_row_id;

        }

        public Dictionary<string, long> getAllHrefFolder(SQLiteConnection con)
        {

            Dictionary<string, long> array = new Dictionary<string, long>();
            int index = 0;
            string stm = "SELECT row_id,location FROM listFiles_users where type='folder';";

            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            long row_id_sqllite = Convert.ToInt32(rdr["row_id"]);
                            string location = rdr["location"].ToString();

                            if (array.ContainsKey(location))
                            {
                                Console.WriteLine("SelectSqllite->getAllHrefFolder: Ошибка Такой LocationID найден в базе Будем ее удалять! Отправили в массив для удаления на сервере");
                                array.Add(location + index, row_id_sqllite);
                            }
                            else
                            {
                                array.Add(location, row_id_sqllite);
                            }
                            index++;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return array;
        }



        //long id - текущий версия базы данных
        public long getVersionBasesClient(SQLiteConnection con)
        {

            long versionClientBases = -1;

            string stm = "SELECT versionUpdateBases FROM listFiles_users LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {
                            versionClientBases = long.Parse(rdr["versionUpdateBases"].ToString());
                        }



                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return versionClientBases;
        }








    }
}
