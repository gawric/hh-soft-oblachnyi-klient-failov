﻿using NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb
{
    public class SelectSqlite
    {
        private SelectFileInfo_Users selectFileInfo_Users;
        public SelectSqlite(SelectFileInfo_Users selectFileInfo_Users)
        {
            this.selectFileInfo_Users = selectFileInfo_Users;
        }
        public string getPathDisk(SQLiteConnection con)
        {
            string pathDisk = "";

            string stm = "SELECT * FROM  location;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            try
                            {
                                if (rdr.GetValue(0) != DBNull.Value)
                                {
                                    // numberOperation = Convert.ToInt32(rdr["numberOperation"].ToString()); 
                                    pathDisk = rdr["locationDownload"].ToString();
                                }

                            }
                            catch (System.IndexOutOfRangeException)
                            {
                                throw;
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }


            return pathDisk;
        }


      
        



        
        public string  selectAutoScanSystemSetting(SQLiteConnection con)
        {
            string autoScanTime = "40000";

            string stm = "SELECT autoScan FROM systemSetting;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            autoScanTime = rdr["autoScan"].ToString();
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return autoScanTime;
        }

       
        public SaveToPcModel getSqlLiteFilesSavePcl(long row_id_listfiles, SQLiteConnection con)
        {

            SaveToPcModel client = new SaveToPcModel();

            string stm = "SELECT row_id_listfiles , location , parent , type ,saveToPc  FROM listSaveToPc_listFiles WHERE row_id_listfiles='" + row_id_listfiles + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {


                                client.row_id_listfiles = Convert.ToInt32(rdr["row_id_listfiles"]);
                                client.saveToPc = Convert.ToInt32(rdr["saveToPc"]);
                                client.type = rdr["type"].ToString();
                                client.location = rdr["location"].ToString();

                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return client;
        }


       

       

        //Получить все записи, что я качал
        public List<FolderNodesTransfer> getSqlLiteAllFileTransfer(SQLiteConnection con , ConvertIconType convertIcon)
        {

            List<FolderNodesTransfer> fileList = new List<FolderNodesTransfer>();
            

            string stm = "SELECT * FROM listFilesTransfer;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                
                                    FolderNodesTransfer clientModel = new FolderNodesTransfer();

                                    clientModel.Row_id = Convert.ToInt32(rdr["row_id"]);
                                    clientModel.Row_id_listFiles_users = Convert.ToInt32(rdr["row_id_listFiles_users"]);
                                    clientModel.Sizebyte = rdr["sizebyte"].ToString();
                                    clientModel.Type = rdr["type"].ToString();
                                    clientModel.progress = (double)rdr["progress"];
                                    clientModel.FolderName = rdr["filename"].ToString();
                                    clientModel.uploadStatus = rdr["uploadStatus"].ToString();
                                    clientModel.TypeTransfer = rdr["typeTransfer"].ToString();

                                    equalsStatus(clientModel , convertIcon);

                                fileList.Add(clientModel);
                               
                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }

        public AutoScanInfoModel getAutoScanInfoModel(SQLiteConnection con)
        {

            AutoScanInfoModel model = new AutoScanInfoModel();


            string stm = "SELECT * FROM autoScanInfo;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                model.folder = Convert.ToInt32(rdr["folder"]);
                                model.files = Convert.ToInt32(rdr["files"]);
                                model.lastDataScan = rdr["lastDataScan"].ToString();
                                model.timeSpent = rdr["timeSpent"].ToString();
                                model.allCount = Convert.ToInt32(rdr["allCount"]);

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return model;
        }

        //Получить все записи, что я качал
        public string getSqlLiteLocationDownload(SQLiteConnection con)
        {

            string locationDownload = "";

            string stm = "SELECT * FROM location;";

            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                locationDownload = rdr["locationDownload"].ToString();
                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return locationDownload;
        }
        //при загрузке файлов из базы данных
        private void equalsStatus(FolderNodesTransfer clientModel , ConvertIconType convertIcon)
        {
            

            if (staticVariable.Variable.isFolder(clientModel.Type))
            {
                clientModel.icon = convertIcon.getIconType("folder");
            }
            else
            {
                clientModel.icon = convertIcon.getIconType("files");
            }

            if (clientModel.TypeTransfer.Equals("upload"))
            {
                clientModel.iconTransfer = convertIcon.getIconType("upload");
            }
            else
            {
                clientModel.iconTransfer = convertIcon.getIconType("download");
            }

            if (clientModel.uploadStatus.Equals("Загрузка") | clientModel.uploadStatus.Equals("Подготовка"))
            {
                clientModel.uploadStatus = "Ошибка";
            }

            
        }

      

       


       




     
        //Получаем последнюю запись из listFilesTransfer
        public long getLastRow_idListFilesTransfer(SQLiteConnection con)
        {

            long last_row_id_1 = 0;

            string stm = "SELECT row_id FROM listFilesTransfer ORDER BY row_id DESC LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {

                            last_row_id_1 = (Int64)rdr.GetInt64(0);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return last_row_id_1;
        }



        
        public bool getAutoStartApplication(SQLiteConnection con)
        {

            int autoStart = 0;
            bool isAutoStartApplcation = false;

            string stm = "SELECT autoStartApplication FROM systemSetting;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {

                            autoStart = (Int32)rdr.GetInt32(0);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            if (autoStart == 1) isAutoStartApplcation = true;

            return isAutoStartApplcation;
        }

        public bool getScanFolderApplication(SQLiteConnection con)
        {

            int autoStart = 0;
            bool isScanFolderApplcation = false;

            string stm = "SELECT startFolder FROM systemSetting;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {

                            autoStart = (Int32)rdr.GetInt32(0);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            if (autoStart == 1) isScanFolderApplcation = true;

            return isScanFolderApplcation;
        }



        public SystemSettingModel getSystemSetting(SQLiteConnection con)
        {

            SystemSettingModel systemSettingModel = new SystemSettingModel();

            string stm = "SELECT * FROM systemSetting;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            systemSettingModel.autoScan = rdr["autoScan"].ToString();
                            systemSettingModel.autoStartApplication = Convert.ToInt32(rdr["autoStartApplication"]);
                            systemSettingModel.startFolder = Convert.ToInt32(rdr["startFolder"]);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

         

            return systemSettingModel;
        }




        //str[0]   > username
        //str[1]   > password
        public string[] getUser(SQLiteConnection con)
        {
            string[] array = new string[2];

            string stm = "SELECT * FROM users;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            string serverUsername = (string)rdr["username"];
                            string serverPassword = (string)rdr["password"];

                            array[0] = serverUsername;
                            array[1] = serverPassword;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return array;
        }

       

     
        
      

     
       

       

        //получаем массив и проходим по базе что-бы узнать размер копируемой папки и кол-во файлов и вложенных папок
        public RecursiveLocalModel getSqlliteSizeBiteAndCountFolder(SQLiteConnection con , long[][] allFiles)
        {
            RecursiveLocalModel sizeModel = new RecursiveLocalModel();

            foreach (long[] item in allFiles)
            {
                long row_id = item[0];
                long folder = item[1];

               if(folder == 1)
               {
                    sizeModel.sizeCountFolder = sizeModel.sizeCountFolder + 1;
               }
               else
               {
                    sizeModel.sizeCountFiles = sizeModel.sizeCountFiles + 1;

                    sizeModel.sizeBytesFolder = sizeModel.sizeBytesFolder + selectFileInfo_Users.getSizeBytesToRow_id(con, row_id);

                }
            }
        

            return sizeModel;
        }
       

       

      

       
       


    }



}
