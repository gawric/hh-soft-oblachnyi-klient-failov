﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.sqlite.connectdb;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite
{
    public  class SqlliteSelectController
    {
        private SelectSqlite selectSqlite;
        private ExceptionController error;
        private ConnectedSqllite classcon;
        private string fileBasesDir = staticVariable.Variable.getBasesDir();
        private StatusSqlliteTransaction statusTransaction;
        private ConvertIconType convertIcon;
        private SelectLocation selectLocation;
        private SelectExists selectExist;
        private SelectSaveToPc selectSaveToPc;
        private SelectFileInfo_Users selectFileInfo_users;
        private SelectFileInfo_Users_2 selectFileInfo_users_2;
        private SelectFilesTemp selectFilesTemp;
        private SelectScanFolder selectScanFolder;
        public SqlliteSelectController(ExceptionController error , ConnectedSqllite classcon , StatusSqlliteTransaction statusTransaction , ConvertIconType convertIcon)
        {
           
            this.error = error;
            this.classcon = classcon;
            this.statusTransaction = statusTransaction;
            this.convertIcon = convertIcon;
            this.selectLocation = new SelectLocation(error, classcon);
            this.selectExist = new SelectExists(error , classcon);
            this.selectSaveToPc = new SelectSaveToPc(error, classcon);
            this.selectFileInfo_users = new SelectFileInfo_Users(error, classcon);
            this.selectFileInfo_users_2 = new SelectFileInfo_Users_2(error, classcon);
            this.selectFilesTemp = new SelectFilesTemp(error, classcon);
            this.selectScanFolder = new SelectScanFolder(error, classcon);
            //получение данных
            selectSqlite = new SelectSqlite(selectFileInfo_users);
        }

        public FileInfoModel getSearchNodesByLocation(string location)
        {
            try
            {
                return selectLocation.getSearchByLocation(location, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesByLocation() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }

        public List<ScanFolderModel> getAllScanFolder()
        {
            try
            {
                return selectScanFolder.getAllScanFolder(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesByLocation() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }

        public bool isExistsRowId(long row_id)
        {
            try
            {
                return selectExist.existSinglRow_id(row_id , classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesByLocation() Критическая Ошибка выборки данных", ex.ToString());

                return false ;
            }

        }

        public bool isExistsClearListScanFolder()
        {
            try
            {
                return selectExist.existClearListScanFolder(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> isExistsClearListScanFolder() Критическая Ошибка база данных не была очищена", ex.ToString());

                return false;
            }

        }

        public TempFilesModel getSearchByLocationToTempFilesModel(string location)
        {
            try
            {
                return selectLocation.getSearchByLocationToTempFilesModel(location, classcon.GetConnectionSqlite());

            }
            catch (SqlException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchByLocationToTempFilesModel() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }

        public bool existListFilesRow_id(long row_id)
        {


            try
            {
                return selectExist.existRow_id(row_id, classcon.GetConnectionSqlite());
            }
            catch (System.Data.SQLite.SQLiteException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> existListFilesRow_id() Критическая Ошибка создания таблицы базы данных sqlite", ex.ToString());

                return false;
            }
        }

        public bool existSinglListFilesRow_id(long row_id)
        {


            try
            {
                return selectExist.existSinglRow_id(row_id, classcon.GetConnectionSqlite());
            }
            catch (System.Data.SQLite.SQLiteException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> existSinglListFilesRow_id() Критическая Ошибка создания таблицы базы данных sqlite", ex.ToString());
                return false;
            }
        }

        public bool existSystemSetting()
        {

            try
            {
                return selectExist.existSystemSetting(classcon.GetConnectionSqlite());
            }
            catch (System.Data.SQLite.SQLiteException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> existSinglListFilesRow_id() Критическая Ошибка создания таблицы базы данных sqlite", ex.ToString());
                return false;
            }
        }

        public bool isExistListTransferToRow_id(long row_id)
        {


            try
            {
                return selectExist.isExistsListTransferToRow_id(row_id, classcon.GetConnectionSqlite());
            }
            catch (System.Data.SQLite.SQLiteException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> isExistListTransferToRow_id() Критическая Ошибка создания таблицы базы данных sqlite", ex.ToString());


                return false;
            }
        }

        //проверяет существует ли пользователь и корректно введен пароль
        public bool[] GetCheckUserSqlite(string username, string password)
        {
            try
            {
                return selectExist.existUsernameAndCheckPassword(username, password, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetCheckUserSqlite() Критическая Ошибка выборки данных", ex.ToString());


                return null;
            }

        }



        public string GetPathDisk()
        {
            try
            {
                return selectSqlite.getPathDisk(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetPathDisk() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }



        public int GetLastNumberOperationListFileTemp()
        {
            try
            {
                return selectFilesTemp.getLastOperaionNumber(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetLastNumberOperationListFileTemp() Критическая Ошибка выборки данных", ex.ToString());
                return 0;
            }

        }

        public string getSystemSettingAutoScan()
        {
            try
            {
                return selectSqlite.selectAutoScanSystemSetting(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetLastNumberOperationListFileTemp() Критическая Ошибка выборки данных", ex.ToString());
                return "40000";
            }

        }

        public long getAllSize()
        {
            try
            {
                return selectFileInfo_users.getSizeListFiles(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSize() Критическая Ошибка выборки данных", ex.ToString());
                return 0;
            }

        }

        public void copySavePcInTemp()
        {
            try
            {
                selectSaveToPc.copySavePcToTempSavePc(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> copySaveToPcInSaveToPcTemp() Критическая Ошибка выборки данных", ex.ToString());
            }

        }

        public void copyTempInSavePc()
        {
            try
            {
                checkActiveTransaction();
                statusTransaction.isTransaction = true;
                selectSaveToPc.copyTempSavePcToSavePc(classcon.GetConnectionSqlite());
                statusTransaction.isTransaction = false;
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> copyTempInSavePc() Критическая Ошибка выборки данных", ex.ToString());
            }

        }


        

        public Dictionary<long, List<long[]>> getAllParentListRowId()
        {
            try
            {
                return selectFileInfo_users_2.getAllParentListRow_id(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesByLocation() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }


        public string getLocationDownload()
        {
            try
            {
                return selectSqlite.getSqlLiteLocationDownload(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getLocationDownload() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }

        //Поиск по таблице ListFiles_users по row_id
        public DeleteFileModel getSearchListFilesByRow_IdDeleteFileModel(long row_id)
        {
            try
            {
                return selectFileInfo_users_2.getSqlLiteNodesByRow_idDeleteFileModel(row_id, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {



                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchListFilesByRow_IdDeleteFileModel() Критическая Ошибка выборки данных", ex.ToString());


                return null;
            }

        }

        //Поиск по таблице listFileTemp по row_id
        public DeleteFileModel getSearchListTempByRow_IdDeleteFileModel(long row_id)
        {
            try
            {
                return selectFilesTemp.getSqlLiteTempByRow_idDeleteFileModel(row_id, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchListTempByRow_IdDeleteFileModel() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }

        //Поиск по таблице listFileTemp по location
        public DeleteFileModel getSearchListTempByLocationDeleteFileModel(string location)
        {
            try
            {
                return selectFilesTemp.getSqlLiteTempByLocationDeleteFileModel(location, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchListTempByLocationDeleteFileModel() Критическая Ошибка выборки данных", ex.ToString());


                return null;
            }

        }

        //Поиск нода по id номеру
        public long getLastRow_idToListFilesTransfer()
        {
            try
            {
                return selectSqlite.getLastRow_idListFilesTransfer(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getLastRow_idToListFilesTransfer() Критическая Ошибка выборки данных", ex.ToString());

                return 0;
            }

        }





        public FileInfoModel getSearchNodesByRow_IdFileInfoModel(long row_id)
        {
            try
            {
                return selectFileInfo_users_2.getSearchNodesByRow_IdFileInfoModel(row_id, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesByRow_IdFileInfoModel() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }


        public long getLastRow_idFileInfoModel()
        {
            try
            {
                return selectFileInfo_users_2.getLastRow_idListFiles_users(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getLastRow_idFileInfoModel() Критическая Ошибка выборки данных", ex.ToString());

                return 0;
            }
            catch (System.Data.SQLite.SQLiteException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getLastRow_idFileInfoModel() Критическая Ошибка выборки данных", ex.ToString());

                return 0;
            }

        }

        public string[] GetUserSqlite()
        {
            try
            {
                return selectSqlite.getUser(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetUserSqlite() Критическая Ошибка получение данных пользователя и пароля", ex.ToString());


                return null;
            }
        }


        public AutoScanInfoModel getAutoScanInfoModel()
        {
            try
            {
                return selectSqlite.getAutoScanInfoModel(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetUserSqlite() Критическая Ошибка получение данных пользователя и пароля", ex.ToString());


                return null;
            }
        }


        public bool isExistsEmptyTableFileList()
        {
            try
            {
                return selectExist.existsEmptyTables(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> isExistsEmptyTableFileList() Критическая Ошибка проверки пустоты таблицы ", ex.ToString());

                return false;
            }
        }

        public List<FileInfoModel> GetSqlLiteRootFileInfoList()
        {
            try
            {
                //-2 Значение для все рутовых папок где есть -2 и -1
                return selectFileInfo_users.getSqlLiteNewRootFileInfoModelList(-2, classcon.GetConnectionSqlite());

            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteRootFileInfoList() Критическая Ошибка получения данных ", ex.ToString());


                return new List<FileInfoModel>();
            }


        }

        public List<FileInfoModel> GetSqlLiteRootFileInfoListToFolder()
        {
            try
            {
                //-2 Значение для все рутовых папок где есть -2 и -1
                //return selectSqlite.getSqlLiteNewRootFileInfoModelListFilterToFOlder(-2, GetConnectionSqlite());
                return selectFileInfo_users.getSqlLiteNewRootFileInfoModelListFilterToFOlderMINIMAL(-2, classcon.GetConnectionSqlite());

            }
            catch (SqlException ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteFileInfoList() Критическая Ошибка получения данных ", ex.ToString());

                return new List<FileInfoModel>();
            }


        }
        private object locker = new object();

        public bool existSqlLiteParentNodes(long row_id)
        {
            try
            {

                return selectExist.existSqlLiteNodeChildren(row_id, classcon.GetConnectionSqlite());

            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> existSqlLiteParentNodes() Критическая Ошибка получения данных ", ex.ToString());

                return false;
            }
        }


        public FileInfoModel getSearchNodesUpdateVersionRows(long Row_id, long UpdateVersionRows)
        {
            try
            {
                //поиск в базе  по row_id и updateVersionRows (Использую для открытых нодов TreeView)
                return selectFileInfo_users.getFileInfoSearchVersionUpdateRows(Row_id, UpdateVersionRows, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesUpdateVersionRows() Критическая Ошибка получения данных ", ex.ToString());

                return null;
            }


        }

        public string getSaveToPcType(long row_id_listfiles)
        {
            try
            {
                return selectSaveToPc.getSqlLiteSavetoPcType(row_id_listfiles, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSaveToPcType() Критическая Ошибка получения данных ", ex.ToString());

                return "";
            }


        }


        public string getTypeAutoSaveFolder(long row_id_listfiles)
        {
            try
            {
                return selectSaveToPc.getSqlLiteAutoSaveFolder(row_id_listfiles, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSaveToPcType() Критическая Ошибка получения данных ", ex.ToString());

                return "";
            }


        }

        public string getSaveToPcLocationType(string row_Location)
        {
            try
            {
                return selectSaveToPc.getSqlLiteSavetoPcType(row_Location, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSaveToPcType() Критическая Ошибка получения данных ", ex.ToString());

                return "";
            }

        }

        public long getSaveToPcLocationRowId(string row_Location)
        {
            try
            {
                return selectSaveToPc.getSqlLiteSavetoPcRoid(row_Location, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSaveToPcType() Критическая Ошибка получения данных ", ex.ToString());

                return -9999;
            }

        }

        public List<FileInfoModel> GetSqlLiteParentIDFileInfoList(long ParentID)
        {
            try
            {
                //поиск значений по полю parentID
                return selectSaveToPc.getSqlLiteRootFileInfoModelList(ParentID, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {


                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDFileInfoList() Критическая Ошибка получения данных", ex.ToString());

                return new List<FileInfoModel>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDFileInfoList() Критическая Ошибка получения данных", ex.ToString());

                return new List<FileInfoModel>();
            }

        }

        public List<FileInfoModel> GetSqlLiteParentIDFileInfoListMinimal(long ParentID)
        {
            try
            {
                //поиск значений по полю parentID
                return selectFileInfo_users.getSqlLiteRootFileInfoModelListMINIMAL(ParentID, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDFileInfoListMinimal() Критическая Ошибка получения данных", ex.ToString());

                return new List<FileInfoModel>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDFileInfoListMinimal() Критическая Ошибка получения данных", ex.ToString());

                return new List<FileInfoModel>();
            }

        }

        public List<long> getAllSaveToPcListFilesId()
        {
            try
            {

                return selectSaveToPc.getAllSaveToPcListFilesId(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesId() Критическая Ошибка получения данных", ex.ToString());

                return new List<long>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesId() Критическая Ошибка получения данных", ex.ToString());

                return new List<long>();
            }

        }

        public Dictionary<string, long> getAllSaveToPcListFilesLocation()
        {
            try
            {

                return selectSaveToPc.getAllSaveToPcListFilesLocation(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesId() Критическая Ошибка получения данных", ex.ToString());

                return new Dictionary<string, long>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesId() Критическая Ошибка получения данных", ex.ToString());

                return new Dictionary<string, long>();
            }

        }


        public Dictionary<long, long> getAllSaveToPcListFilesRowId()
        {
            try
            {
                return selectSaveToPc.getAllSaveToPcListFilesRow_id(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesRowId() Критическая Ошибка получения данных", ex.ToString());

                return new Dictionary<long, long>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesRowId() Критическая Ошибка получения данных", ex.ToString());

                return new Dictionary<long, long>();
            }

        }

        public Dictionary<long, long> getAllAutoSaveListFilesRowId()
        {
            try
            {
                return selectSaveToPc.getAllAutoSaveListFilesRow_id(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesRowId() Критическая Ошибка получения данных", ex.ToString());

                return new Dictionary<long, long>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getAllSaveToPcListFilesRowId() Критическая Ошибка получения данных", ex.ToString());

                return new Dictionary<long, long>();
            }

        }

       

        public List<SaveToPcModel> GetSqlLiteParentIDSavePcMinimal(long ParentID)
        {
            try
            {
                return selectSaveToPc.getSqlLiteParentIDSavePcMinimal(ParentID, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDSavePcFileInfoListMinimal() Критическая Ошибка получения данных", ex.ToString());

                return new List<SaveToPcModel>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDFileInfoListMinimal() Критическая Ошибка получения данных", ex.ToString());

                return new List<SaveToPcModel>();
            }

        }

        public List<AutoSaveFolderModel> GetSqlLiteParentIDAutoSaveFolderMinimal(long parentID)
        {
            try
            {
                return selectSaveToPc.getSqlLiteParentIDAutoSaveFolderMinimal(parentID, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDSavePcFileInfoListMinimal() Критическая Ошибка получения данных", ex.ToString());

                return new List<AutoSaveFolderModel>();
            }
            catch (System.InvalidCastException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDFileInfoListMinimal() Критическая Ошибка получения данных", ex.ToString());

                return new List<AutoSaveFolderModel>();
            }

        }

        public SaveToPcModel getSqlLitePcSaveListFilesId(long listFilesRowId)
        {
            try
            {
                return selectSqlite.getSqlLiteFilesSavePcl(listFilesRowId, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLitePcSaveListFilesId() Критическая Ошибка получения данных", ex.ToString());
                return new SaveToPcModel();
            }


        }

        public List<long[]> GetSqlLiteParentIDLongList(long ParentID)
        {
            try
            {
                //поиск значений по полю parentID
                return selectFileInfo_users.getSqlLiteParentIdLong(ParentID, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> GetSqlLiteParentIDLongList() Критическая Ошибка получения данных", ex.ToString());


                return new List<long[]>();
            }

        }

        public List<FolderNodesTransfer> getSqlliteAllListFIlesTransfer()
        {
            try
            {
                //поиск значений по полю parentID
                return selectSqlite.getSqlLiteAllFileTransfer(classcon.GetConnectionSqlite(), convertIcon);
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSqlliteAllListFIlesTransfer() Критическая Ошибка получения данных", ex.ToString());


                return null;
            }

        }

        public long getSqlLiteVersionDataBasesClient()
        {
            try
            {
                //поиск значений по полю parentID
                return selectFileInfo_users_2.getVersionBasesClient(classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSqlLiteVersionDataBasesClient() Критическая Ошибка получения данных", ex.ToString());


                return 0;
            }
        }


        //проверяет получили свежую базу или уже тухлую
        public bool existsLastVersionBases(long lastVersionBases)
        {
            try
            {
                return selectExist.existVersionUpdateBases(lastVersionBases, classcon.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> existVersionUpdateBases() Критическая Ошибка проверки базы данных ", ex.ToString());
                return false;
            }
        }



        public long getSqlLiteVersionRowsClient()
        {
            try
            {
                //поиск значений по полю parentID
                return selectFileInfo_users.getVersionRows(classcon.GetConnectionSqlite());
                //return 0;
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSqlLiteVersionDataBasesClient() Критическая Ошибка получения данных", ex.ToString());


                return 0;
            }
        }


        public Dictionary<long, long> getAllRow_id_long()
        {
            try
            {

                return selectFileInfo_users.getAllRow_id_long(classcon.GetConnectionSqlite());

            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getAllRow_id_long: !!!Получение всех записей row_id ошибка!!! " + s1);

                return null;
            }
            catch (System.IndexOutOfRangeException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getAllRow_id_long: !!!Получение всех записей row_id ошибка!!! " + s1);

                return null;
            }
        }

        public Dictionary<string, long> getAllHref(Dictionary<long, string> listDoubleHref)
        {
            try
            {

                return selectFileInfo_users.getAllHref(classcon.GetConnectionSqlite(), listDoubleHref);

            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getAllRow_id_long: !!!Получение всех записей row_id ошибка!!!");

                return null;
            }
        }


        //получает всех детей данной папки + сама папка
        public RecursiveLocalModel getSqlliteSizeFolderAndCount(long[][] allFiles)
        {
            try
            {
                return selectSqlite.getSqlliteSizeBiteAndCountFolder(classcon.GetConnectionSqlite(), allFiles);
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getSqlliteSize: Получение размера и кол-ва папок и файлов");
                Console.WriteLine(s1.ToString());
                return null;
            }

        }

        public bool getAutoStartApplication()
        {
            try
            {
                return selectSqlite.getAutoStartApplication(classcon.GetConnectionSqlite());
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getAutoStartApplication: Не удалось прочитать состояние AutoStartApplication");
                Console.WriteLine(s1.ToString());
                return false;
            }

        }

        public bool getScanFolderApplication()
        {
            try
            {
                return selectSqlite.getScanFolderApplication(classcon.GetConnectionSqlite());
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getAutoStartApplication: Не удалось прочитать состояние AutoStartApplication");
                Console.WriteLine(s1.ToString());
                return false;
            }

        }

        public SystemSettingModel getSystemSetting()
        {
            try
            {
                return selectSqlite.getSystemSetting(classcon.GetConnectionSqlite());
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getSystemSetting: Не удалось прочитать состояние");
                Console.WriteLine(s1.ToString());
                return new SystemSettingModel();
            }

        }
        

        public Dictionary<string, long> getAllHrefFolder()
        {
            try
            {
                return selectFileInfo_users_2.getAllHrefFolder(classcon.GetConnectionSqlite());
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine("SqlliteCOntroller: getAllRow_id_long: !!!Получение всех записей row_id ошибка!!!");

                return null;
            }
        }


        public long existUserIdToListFiles()
        {
            return selectExist.existUserIdListFiles(classcon.GetConnectionSqlite());
        }



        public void checkActiveTransaction()
        {
            int b = 0;
            //проверка что больше писателей у нас нет
            while (true)
            {

                if (statusTransaction.isTransaction == false)
                {
                    break;
                }

                if (b == 300)
                {
                    break;
                }

                Thread.Sleep(10);
                b++;
            }
        }

        private BaseException generatedError(int codeError, string textError, string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError);
        }


      



    }
}
