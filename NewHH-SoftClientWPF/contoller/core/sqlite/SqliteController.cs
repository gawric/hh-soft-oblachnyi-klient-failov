﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.sqlite;
using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.core.sqlite.insertSql;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite.connectdb;
using NewHH_SoftClientWPF.contoller.sqlite.filesystemdb;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Model;
                                                        

namespace NewHH_SoftClientWPF.contoller.sqlite
{
    public class SqliteController
    {
        private ExceptionController error;
        private StatusSqlliteTransaction statusTransaction;
        private bool auth = false;
        
       // private InsertSqlLite insertSqlite;
        private DeleteSqllite deleteSqlite;
        private UpdateSqllite updateSqlite;
        private ConvertIconType convertIcon;
        private ConnectedSqllite connected;
        private SqlliteSelectController sqlliteSelectController;
        private SqlliteInsertController sqlliteInsertController;
        public SqliteController(Container cont)
        {
            error = cont.GetInstance<ExceptionController>();
            statusTransaction = cont.GetInstance<StatusSqlliteTransaction>();
            convertIcon = cont.GetInstance<ConvertIconType>();


           
            connected = new ConnectedSqllite( error);
            sqlliteSelectController = new SqlliteSelectController(error, connected, statusTransaction, convertIcon);
            sqlliteInsertController = new SqlliteInsertController(error, connected, statusTransaction, convertIcon);

            //внесение данных
           // insertSqlite = new InsertSqlLite();

            deleteSqlite = new DeleteSqllite();

            updateSqlite = new UpdateSqllite();

            //создаем подключение
            connected.GetConnectionSqlite();

        }
        public void CreateBases()
        {
            connected.CreateBases();
        }
       public SqlliteSelectController getSelect()
        {
            return sqlliteSelectController;
        }

        public SqlliteInsertController getInsert()
        {
            return sqlliteInsertController;
        }
        public void UpdateFilesTableAllRows(List<FileInfoModel> listFiles ,  MainWindowViewModel _viewModelMain)
        {
            //очистка всей таблицы
            clearTableslistFiles();

            //добавляем новые данные пачкой через транзакцию
            getInsert().InsertFileInfoTransaction(listFiles , _viewModelMain);
        }

        //Обновляет Progress для файлов
        public void updateListFilesProgress(long row_id_transfer, string sizebytes ,  double progress , string uploadStatus)
        {
            updateSqlite.UpdateTransferProgress(row_id_transfer, sizebytes , progress , uploadStatus , connected.GetConnectionSqlite());
        }
        
        public void updateListAllSaveToPc(int intSave , string type)
        {

            updateSqlite.updateListAllSaveToPc(intSave, type, connected.GetConnectionSqlite());
        }

        public void updateAutoStartMill(string autoStartMill)
        {

            updateSqlite.updateSystemSettingAutoStart(autoStartMill , connected.GetConnectionSqlite());
        }

        public void updateScanFolderLocation(string location, long row_id)
        {

            updateSqlite.updateScanFolderLocation(location,  row_id, connected.GetConnectionSqlite());
        }

        public void updateListSaveToPc(long row_id_listfiles , string type, int intSave)
        {
            
            updateSqlite.updateListSaveToPc(row_id_listfiles, intSave, type, connected.GetConnectionSqlite());
        }

        public void updateTypeAndLocationSaveToPc(long row_id_listfiles, string type, string location,int intSave)
        {

            updateSqlite.updateLocationAndTypeSaveToPc(row_id_listfiles, intSave, type, location, connected.GetConnectionSqlite());
        }

        public void updateTypeAndLocationAutoSaveFolder(long row_id_listfiles, string type, string location, int intSave)
        {
            updateSqlite.updateLocationAndTypeAutoSaveFolder(row_id_listfiles, intSave, type, location, connected.GetConnectionSqlite());
        }
        public void updateTypeListFiles(long row_id, string type)
        {
            updateSqlite.updateTypeListFiles( row_id,  type, connected.GetConnectionSqlite());
        }

        public void updateNewRow_IdSaveToPc(long newRow_id, long row_id)
        {

            updateSqlite.updateRow_idSaveToPc(newRow_id, row_id, connected.GetConnectionSqlite());
        }

        public void searchListFilesNewId(Dictionary<string, long> pcSaveListaDictionary , Dictionary<string, long> allListFilesLocation)
        {
            SQLiteTransaction transaction = connected.GetConnectionSqlite().BeginTransaction();
            int idex = 0;

            foreach (KeyValuePair<string, long> kvp in pcSaveListaDictionary)
            {
                Console.WriteLine("SqlliteController->searchListFilesNewId: "+ idex++);
                if (allListFilesLocation.ContainsKey(kvp.Key))
                {
                    //-9999 заглушка что-бы не нарушить консистенцию ключей, если там есть такой же
                   // updateNewRow_IdSaveToPc(-9999, location);
                    long listFilesRowId = allListFilesLocation[kvp.Key];
                    updateNewRow_IdSaveToPc(listFilesRowId, kvp.Value);
                }
                else
                {
                    deletePcSaveRowsLocation(kvp.Key);
                }
            }

            transaction.Commit();
            transaction.Dispose();

        }


        public void updateVeersionBases(long newVerSionServer)
        {
            updateSqlite.UpdateVersionBases( newVerSionServer, connected.GetConnectionSqlite());
        }

        public void RemoveFilesTableAllRows()
        {
            //очистка всей таблицы
            clearTableslistFiles();
        }

        public void RemoveFilesTableTempAllRows()
        {
            //очистка всей таблицы
            clearTableslistFilesTemp();
        }

        public void RemoveFilesTempTable(string Event)
        {
            //очищает временную таблицу по созданному event (DELETE/CREATE/RENAME)
            removeTableslistFilesTemp(Event);
        }

        public void InsertFilesTableAllRows(List<FileInfoModel> listFiles , MainWindowViewModel _viewModelMain)
        {
            //добавляем новые данные пачкой через транзакцию
            getInsert().InsertFileInfoTransaction(listFiles, _viewModelMain);
        }

        public List<FileInfoModel> UpdateRenameName(long row_id , string strTextBox)
        {
            List<FileInfoModel> listFiles = new List<FileInfoModel>();

            FileInfoModel update_version = sqlliteSelectController.getSearchNodesUpdateVersionRows(row_id, 0);

            update_version.filename = strTextBox;
            update_version.versionUpdateRows = update_version.versionUpdateRows + 1;
            update_version.changeRows = "UPDATE";
            update_version.versionUpdateBases = update_version.versionUpdateBases + 1;
            

            listFiles.Add(update_version);

            //Сразу обновляем базу и отправляем ее на обновление treeview
            UpdateFilesTableCollectionRows(listFiles);

            return listFiles;
        }

        //Обновляет базу данных кусками от сервера
        public void UpdateFilesTableCollectionRows(List<FileInfoModel> listFiles)
        {
            
           
            try
            {
                checkActiveTransaction();

               

               // SQLiteTransaction transaction = GetConnectionSqlite().BeginTransaction();
                List<FileInfoModel> listCreate = new List<FileInfoModel>();
                List<FileInfoModel> listDelete = new List<FileInfoModel>();

                for (int f = 0; f < listFiles.Count; f++)
                {
                    FileInfoModel modelServer = listFiles[f];

                    if (modelServer != null)
                    {
                        if (modelServer.changeRows.Equals("UPDATE"))
                        {
                            if(sqlliteSelectController.existSinglListFilesRow_id(modelServer.row_id))
                            {
                                statusTransaction.isTransaction = true;
                                //Обновляем если находим
                                updateSqlite.FileRows(modelServer, connected.GetConnectionSqlite());
                                statusTransaction.isTransaction = false;
                            }
                            else
                            {
                               
                                //Добавляем если не находим такой row_id
                                getInsert().insertFileListSinglRows(modelServer);
                        
                            }
                            
                        }
                        else if (modelServer.changeRows.Equals("CREATE"))
                        {

                           
                             

                            listCreate.Add(modelServer);


                        }
                        else if (modelServer.changeRows.Equals("DELETE"))
                        {
                            
                            listDelete.Add(modelServer);
                        }
                        else
                        {

                        }
                    }

                }

                //transaction.Commit();
                //transaction.Dispose();

                deleteSqlite.deleteFileListTransaction(listDelete, connected.GetConnectionSqlite());

              
                getInsert().InsertFileInfoTransaction(listCreate, new MainWindowViewModel());

               
            }
            catch (SQLiteException z)
            {
                
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> UpdateFilesTableCollectionRows() Ошибка Transaction Update попался уже используемый rowID " , z.ToString());
            }

          
        }

       

     

      
       
        //очищает такблицу users
        public void clearTablesUsers()
        {
            try
            {
                statusTransaction.isTransaction = true;
                deleteSqlite.DeleteAllRowsTables(connected.GetConnectionSqlite(), "users");

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTablesUsers() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

       
        public void clearLocation()
        {
            try
            {

                deleteSqlite.DeleteLocation(connected.GetConnectionSqlite());

            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTablesUsers() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        public void clearListScanFolder()
        {
            try
            {
                deleteSqlite.DeleteListScanFolder(connected.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearListFolder() Критическая Ошибка удаления данных", ex.ToString());
            }
        }
        //очищает таблицу где хранятся пути
        //к папкам для синхронизации
        public void clearScanFolder()
        {
            try
            {
                deleteSqlite.DeleteScanFolder(connected.GetConnectionSqlite());
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearListFolder() Критическая Ошибка удаления данных", ex.ToString());
            }
        }

        //очищает 1 запись в таблице listFilesTransfer
        public void removeListFilesTransferSingl(long row_id)
        {
            try
            {
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteListFilesTansferTableSinglRows(row_id , connected.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        public void removeSinglitemScanFolder(string fullPath)
        {
            try
            {
                statusTransaction.isTransaction = true;
                deleteSqlite.DeleteSinglItemScanFolder(fullPath, connected.GetConnectionSqlite());
                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
            }

        }


        public void deletePcSaveRowsLocation(string oldLocation)
        {
            try
            {
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteSavePcSinglRowsLocation(oldLocation, connected.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
            }

        }


        public void deleteSavePcSinglRows(long listFilesRow_id)
        {
            try
            {
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteSavePcSinglRows(listFilesRow_id, connected.GetConnectionSqlite());

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        //очищает таблицу listFiles_users
        public void clearTableslistFiles()
        {
            try
            {
                WaitingTransaction(statusTransaction);
                statusTransaction.isTransaction = true;
                deleteSqlite.DeleteAllRowsTables(connected.GetConnectionSqlite(), "listFiles_users");
                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistFiles() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        //очищает таблицу listPcSaveTemp
        public void clearTablesPcTemp()
        {
            try
            {
                WaitingTransaction(statusTransaction);
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteAllRowsTables(connected.GetConnectionSqlite(), "listSaveTopPcTemp");

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                statusTransaction.isTransaction = false;
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistFiles() Критическая Ошибка удаления данных", ex.ToString());

            }

        }

        //очищает таблицу listFilesTransfer
        public void clearTableslistTransfer()
        {
            try
            {
                WaitingTransaction(statusTransaction);
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteAllRowsTables(connected.GetConnectionSqlite(), "listFilesTransfer");

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistTransfer() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        //очищает таблицу listFilesTemp_users
        public void clearTableslistFilesTemp()
        {
            try
            {
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteAllRowsTablesTemp(connected.GetConnectionSqlite(), "listFilesTemp");

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistFilesTemp() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        //очищает таблицу listFiles_users
        public void removeTableslistFilesTemp(string Event)
        {
            try
            {
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteListFilesTempTable(connected.GetConnectionSqlite(), Event);

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeTableslistFilesTemp() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        //очищает таблицу listFilesTemp
        public void removeTablelistFileTempChangeRows(string Event , int numberOperation)
        {
            try
            {
                statusTransaction.isTransaction = true;

                deleteSqlite.DeleteListFileTempChangeRows(connected.GetConnectionSqlite(), Event , numberOperation);

                statusTransaction.isTransaction = false;
            }
            catch (Exception ex)
            {

                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeTablelistFileTempChangeRows() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

      
        public void checkActiveTransaction()
        {
            int b = 0;
            //проверка что больше писателей у нас нет
            while (true)
            {

                if (statusTransaction.isTransaction == false)
                {
                    break;
                }

                if (b == 300)
                {
                    break;
                }

                Thread.Sleep(10);
                b++;
            }
        }


      

        

        //получает всех детей переданной location
        public List<long[][]> getAllChildren(List<long[][]> allFiles , string copyLocation, SqliteController _sqlLiteController , IProgress<CopyViewModel> progress , string progressLabel)
        {
            //p количество файлов в заданном каталоге
            int[] p = { 0 };
            //arr[n](список всех записей)->arr->0 - id | 1 - type(0 - files/1 - folder)
            List<long[][]>  allFilesEnd =  getScanAllChildren(allFiles , copyLocation, p, _sqlLiteController , progress , progressLabel);

            return allFilesEnd;
        }

       

        static object lockerAllChildren = new object();
        //получает всех детей данной папки + сама папка
        public List<long[][]> getScanAllChildren(List<long[][]> allFiles , string location, int[] p, SqliteController _sqlLiteController , IProgress<CopyViewModel> progress , string progressLabel)
        {
            try
            {
                lock (lockerAllChildren)
                {
                    List<long[]> list = testAllRoot(location, _sqlLiteController, progress, progressLabel);
                    allFiles.Add(list.ToArray());

                    return allFiles;
                }
                

            }
            catch (WebDAVClient.Helpers.WebDAVException z)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getScanAllChildren() Критическая ошибка WebDAVException", z.ToString());
                return null;
            }

        }


       

       
        //рекурсивный проход - по рутовым папкам
        private List<long[]> testAllRoot(string location, SqliteController _sqlLiteController, IProgress<CopyViewModel> progress , string progressLabel)
        {
            //0 - id самого нода
            //1 - папка или файл
            List<long[]> allFile = new List<long[]>();

            //получаем текущий узел из базы
            TempFilesModel rootLocation = _sqlLiteController.sqlliteSelectController.getSearchByLocationToTempFilesModel(location);

            if (rootLocation != null)
            {
                //его id номер
                long root_id = rootLocation.row_id;

                long[] countFiles = { 0 };

                InsertRootTemp(allFile, rootLocation);
                CopyViewModel model = new CopyViewModel();

                return getAllSacn(allFile, root_id, _sqlLiteController, progress, model, countFiles, progressLabel);

            }
            else
            {
                //срабатывает когда мы не знаем его id
                return allFile;
            }
           
        }

      
        //Добавляет рутовую папку или файл во временное хранилище вместе с остальными
        //т.к раньше добавлялись только дети
        private void InsertRootTemp(List<long[]> list, TempFilesModel rootFiles)
        {


            long typeRoot;
            if (staticVariable.Variable.isFolder(rootFiles.type) == true)
            {
                typeRoot = 1;
            }
            else
            {
                typeRoot = 0;
            }
            long[] rootLong = { rootFiles.row_id, typeRoot };
            list.Add(rootLong);

        }
        int[]k = { 0};

        public List<long[]> getAllSacn(List<long[]> Alllist, long parent_id, SqliteController _sqlLiteController, IProgress<CopyViewModel> progress , CopyViewModel model , long[] countFiles , string progressLabel)
        {
            try
            {
                List<long[]> list = _sqlLiteController.sqlliteSelectController.GetSqlLiteParentIDLongList(parent_id);
               

                for (int j = 0; j < list.Count; j++)
                {
                    try
                    {

                        model.StatusWorker = progressLabel + "  " + countFiles[0]++;
                        progress.Report(model);

                        long[] modelServer = list[j];

                        long type = modelServer[1];

                        Alllist.Add(modelServer);

                        if (type == 1)
                        {
                            getAllSacn(Alllist, modelServer[0], _sqlLiteController, progress, model, countFiles, progressLabel);
                        }
                        else
                        {
                            //long[] arr = { }
                        }

                    }
                    catch (System.InvalidOperationException s1)
                    {
                        Console.WriteLine("SqlliteController-> getAllSacn: Сканирования Файлов на удаление потерпела не удачу");
                        Console.WriteLine(s1.ToString());
                        return Alllist;
                    }
                    catch (System.NullReferenceException s2)
                    {
                        Console.WriteLine("SqlliteController-> getAllSacn: Сканирования Файлов на удаление потерпела не удачу");
                        Console.WriteLine(s2.ToString());
                        return Alllist;
                    }



                }
                
                return Alllist;
            }
            catch (System.InvalidOperationException s1)
            {
                Console.WriteLine("SqlliteController-> getAllSacn: Сканирования Файлов на удаление потерпела не удачу");
                Console.WriteLine(s1.ToString());
                return Alllist;
            }
            catch (System.NullReferenceException s2)
            {
                Console.WriteLine("SqlliteController-> getAllSacn: Сканирования Файлов на удаление потерпела не удачу");
                Console.WriteLine(s2.ToString());
                return Alllist;
            }
            
        }

        private void InsertTemp(DeleteFilesAsyncModel DeleteModel, long[][]list, int numberOperation, FileInfoModel[] arr , int[]cnt)
        {
            if (list != null)
            {
                //list.lenght = начинает счет с 1, а массив с начинается с 0
                for (int k = 0; k < list.Length; k++)
                {

                    if (list[k] != null)
                    {


                        if (cnt[0] == staticVariable.Variable.CountFilesTrasferServer)
                        {

                            DeleteModel._sqlLiteController.getInsert().InsertFileTempTransaction(arr, numberOperation);

                            for (int s = 0; s < arr.Length; s++)
                            {
                                arr[s] = null;
                            }

                            cnt[0] = 0;

                        }


                        long row_id = list[k][0];

                        arr[cnt[0]] = DeleteModel._sqlLiteController.sqlliteSelectController.getSearchNodesByRow_IdFileInfoModel(row_id);
                        arr[cnt[0]].changeRows = "DELETE";

                        cnt[0]++;


                    }

                }
            }
              
        }
        private void clearArr(FileInfoModel[] arr)
        {
            for (int j = 0; j < arr.Length; j++)
            {
                arr[j] = null;
            }
        }
        private void StartInsertTemp(DeleteFilesAsyncModel DeleteModel, List<long[][]> alllist, int numberOperation, FileInfoModel[] arr)
        {
            

            for(int w = 0; w < alllist.Count; w++)
            {
                int[] cnt = { 0 };
                long[][] list = alllist[w];

                InsertTemp(DeleteModel, list, numberOperation, arr , cnt);
                DeleteModel._sqlLiteController.getInsert().InsertFileTempTransaction(arr, numberOperation);
                clearArr(arr);
            }

            clearArr(arr);
        }
        
        //Добавляет файлы по временное хранилище
        public void StartInsertFileTemp(DeleteFilesAsyncModel DeleteModel , List<long[][]> list , int numberOperation)
        {
            //массив для хранения
            FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            
            try
            {
                if(list != null)
                {
                    DeleteModel.updateNetworkJsonController.updateSendJsonDELETEBEGINPARTIES(arr, DeleteModel.newVersionBases);

                    StartInsertTemp(DeleteModel, list, numberOperation, arr);
                }
               

            }
            catch (System.IndexOutOfRangeException b5)
            {
               
                Console.WriteLine(b5.ToString());
            }
            catch (System.NullReferenceException npe)
            {
                Console.WriteLine(npe.ToString());

            }

            
        }

       

        public void UpdateUserIdToListFiles(long user_id)
        {
            updateSqlite.UpdateUserIdListFiles(user_id, connected.GetConnectionSqlite());
        }

        public void deleteListFilesTransaction(List<FileInfoModel>list)
        {
            deleteSqlite.deleteFileListTransaction(list, connected.GetConnectionSqlite());
        }

        public void deletelistSaveToPc(List<long[][]> allListFull)
        {
            deleteSqlite.deleteRowslistSaveToPc_listFiles(allListFull, connected.GetConnectionSqlite());
        }



        private BaseException generatedError(int codeError , string textError , string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError); 
        }


        public void WaitingTransaction(StatusSqlliteTransaction _statusTransaction)
        {
            //проверяем что другие потоки не пишут в нашу базу дынных
            int v = 0;
            while (true)
            {
                Console.WriteLine("SqliteController->WaitingTransaction: Ждем освобождения для записи   " + v);
                Thread.Sleep(100);
                if (_statusTransaction.isTransaction == false)
                {
                    break;
                }

                if (v == 300)
                {
                    break;
                }
                v++;
            }
        }

    }
}
