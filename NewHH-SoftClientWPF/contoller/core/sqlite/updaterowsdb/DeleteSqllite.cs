﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb
{
    class DeleteSqllite
    {


        public void deleteRowslistSaveToPc_listFiles(List<long[][]> allListFull, SQLiteConnection con)
        {
            SQLiteTransaction transaction = con.BeginTransaction();
            //long[][] list = allListFull[0];

            foreach(long[][] item in allListFull)
            {
                for (int a = 0; a < item.Length; a++)
                {
                    if (item[a] != null)
                    {
                        long row_id = item[a][0];

                        string sqlstr = "DELETE FROM listSaveToPc_listFiles WHERE row_id_listfiles = '" + row_id + "';";
                        SQLiteCommand command = new SQLiteCommand(sqlstr, con);

                        command.ExecuteNonQuery();
                        command.Dispose();
                    }
                }
            }

            

            transaction.Commit();
            transaction.Dispose();
        }

        

        public void deleteFileListTransaction(List<FileInfoModel> list, SQLiteConnection con)
        {



            SQLiteTransaction transaction = con.BeginTransaction();
            try
            {

                int k = 0;
                list.ForEach(delegate (FileInfoModel modelServer)
                {
                    if (modelServer != null)
                    {
                        double procent = k++ / 10;
                   

                        string deleteString = "DELETE FROM listFiles_users WHERE row_id = '" + modelServer.row_id + "';";
                        SQLiteCommand command = new SQLiteCommand(deleteString , con);
            
                        command.ExecuteNonQuery();
                        command.Dispose();
        
                    }
                    else
                    {
                        // Console.WriteLine("Одно из значений оказалось пустым Calss_ InsertSqlLite -> insertFileListTransaction");
                    }

                });


                Console.WriteLine("Пакет обработан 1000 Class DeleteSqllite -> deleteFileListTransaction");
                transaction.Commit();

                list = null;


            }
            catch (SQLiteException)
            {
                throw;
            }
        }

        public void DeleteAllRowsTables(SQLiteConnection m_dbConnection , string tableName)
        {
            string sql = "DELETE FROM " + tableName + ";";
            string sql1 = "VACUUM;";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
            try
            {
                command.ExecuteNonQueryAsync();
                command1.ExecuteNonQueryAsync();

            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

       

        public void DeleteLocation(SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM location";
            string sql1 = "VACUUM;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                command1.ExecuteNonQuery();


            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteListScanFolder(SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM listScanFolder_ListFiles";
            string sql1 = "VACUUM;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                command1.ExecuteNonQuery();


            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteScanFolder(SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM scanFolder";
            string sql1 = "VACUUM;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                command1.ExecuteNonQuery();


            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }



        public void DeleteAllRowsTablesTemp(SQLiteConnection m_dbConnection, string tableName)
        {
            string sql = "DELETE FROM " + tableName + ";";
           // string sql1 = "VACUUM;";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
           // SQLiteCommand command1 = new SQLiteCommand(sql1, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
               // command1.ExecuteNonQuery();


            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteListFilesTableSinglRows(long row_id , SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM listFiles_users WHERE row_id = '" + row_id + "';";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                
                command.Dispose();
                
            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteSavePcSinglRowsLocation(string oldlocation, SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM listSaveToPc_listFiles WHERE location = '" + oldlocation + "';";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();

                command.Dispose();

            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteSavePcSinglRows(long row_id_listfiles, SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM listSaveToPc_listFiles WHERE row_id_listfiles = '" + row_id_listfiles + "';";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();

                command.Dispose();

            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteListFilesTansferTableSinglRows(long row_id, SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM listFilesTransfer WHERE row_id = '" + row_id + "';";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                command.Dispose();

            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteSinglItemScanFolder(string fullPath, SQLiteConnection m_dbConnection)
        {
            string sql = "DELETE FROM scanFolder WHERE fullpath='"+fullPath+"';";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                command.Dispose();

            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }


        public void DeleteListFilesTempTable(SQLiteConnection m_dbConnection , string Event)
        {
            string sql = "DELETE FROM listFiles_users WHERE changeRows = '" + Event + "';";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            try
            {
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }

        public void DeleteListFileTempChangeRows(SQLiteConnection m_dbConnection, string Event , int numberOperation)
        {
            string sql = "DELETE FROM listFilesTemp WHERE changeRows = '" + Event + "' AND numberOperation='" + numberOperation + "';";

            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            commit(command);
        }

        private static void commit(SQLiteCommand command)
        {
            try
            {
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (System.Data.SQLite.SQLiteException)
            {
                throw;
            }
        }
    }
}
