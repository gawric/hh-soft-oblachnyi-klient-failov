﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.contoller.stream
{
    public static class StreamExtensionController
    {
        public static async Task CopyToAsync(Stream source, Stream destination, IDownload status , long size)
        {
            try
            {
                byte[] buffer = new byte[4096];
                int bytesRead;
                long totalRead = 0;

                status.ChangeState(totalRead, size, "start", false);

                while ((bytesRead = await source.ReadAsync(buffer, 0, buffer.Length)) > 0)
                {
                    await destination.WriteAsync(buffer, 0, bytesRead);
                    totalRead += bytesRead;
                    status.ChangeState(totalRead, size, "download", false);

                    //отмена
                    if (status.getCancel())
                        break;

                }

                status.ChangeState(totalRead, size, "complete", true);
                buffer = null;

            }
            catch(Exception)
            {
                throw;
            }
            finally
            {
                source.Close();
                destination.Close();
               
            }
           
        }
       


}
}
