﻿using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.infosystem
{
    public class InfoSqlSize
    {
        private SqliteController _sqlliteController;

        public InfoSqlSize(SqliteController sqlliteController)
        {
            _sqlliteController = sqlliteController;
        }

        public double getSize()
        {
            Console.WriteLine("InfoSqlSize->getSize  " + staticVariable.UtilMethod.FormatBytes(getSizeSqlBytes()));
            return staticVariable.UtilMethod.FormatBytesNoSuffix(getSizeSqlBytes());
        }

        private long getSizeSqlBytes()
        {
           return _sqlliteController.getSelect().getAllSize();
        }

     
    }
}
