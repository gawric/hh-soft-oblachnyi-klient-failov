﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.contoller.sorted
{
    public class SearchDublication
    {

        //проверка ObservableCollection на дубликаты, если они есть мы удаляем лишнее
        public void clearDublication(ObservableCollection<FolderNodes> ListViewNode)
        {
            Dictionary<long, long> duplicateNumbers = new Dictionary<long, long>();

            searchDublication(ListViewNode, duplicateNumbers);
            clear(ListViewNode, duplicateNumbers);

            duplicateNumbers = null;

        }
        private void searchDublication(ObservableCollection<FolderNodes> ListViewNode, Dictionary<long, long> duplicateNumbers)
        {
            for (int i = 0; i < ListViewNode.Count; i++)
            {

                FolderNodes original = ListViewNode[i];

                for (int d = 0; d < ListViewNode.Count; d++)
                {
                    FolderNodes element = ListViewNode[d];
                    equals(ref element, ref original, ref duplicateNumbers);

                }

            }
        }

        private void equals(ref FolderNodes element , ref FolderNodes original , ref Dictionary<long, long> duplicateNumbers)
        {
            if (original != null)
            {
                if (original.Row_id == element.Row_id)
                {
                    if (duplicateNumbers.ContainsKey(original.Row_id) == true)
                    {
                        duplicateNumbers[original.Row_id] = duplicateNumbers[original.Row_id] + 1;
                    }
                    else
                    {
                        duplicateNumbers.Add(original.Row_id, 1);
                    }
                }

            }
        }
        private void clear(ObservableCollection<FolderNodes> ListViewNode, Dictionary<long, long> duplicateNumbers)
        {
            for (int d = 0; d < ListViewNode.Count; d++)
            {
                FolderNodes original = ListViewNode[d];
                startClear(ref original, ref duplicateNumbers, ref ListViewNode, ref d);
            }

        }

        private void startClear(ref FolderNodes original , ref Dictionary<long, long> duplicateNumbers , ref ObservableCollection<FolderNodes> ListViewNode , ref int d)
        {
            if (duplicateNumbers.ContainsKey(original.Row_id) == true)
            {
                long duplication = duplicateNumbers[original.Row_id];

                if (duplication > 1)
                {
                    Console.WriteLine("UpdateNodesLazyListView -> clearDublication: Ошибка найден дубликат в нашем ListViewNodes ");

                    deleteFolderNodesListView(ListViewNode, d);
                    duplicateNumbers[original.Row_id] = 1;
                    d--;
                }

            }
        }

        private void deleteFolderNodesListView(ObservableCollection<FolderNodes> ListViewNode, int index)
        {
            //проверка существует поток GUI
            if (System.Windows.Application.Current != null)
            {
                //откуда запущен поток
                Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);

                if (dispatcher == null)
                {


                    try
                    {
                        App.Current.Dispatcher.Invoke(delegate // <--- HERE
                        {
                            ListViewNode.RemoveAt(index);
                        });


                    }
                    catch (System.InvalidOperationException ss)
                    {

                        Console.WriteLine("UpdateTreeViewNodes->deleteFolderNodesListView " + ss);
                    }


                }
                else
                {
                    ListViewNode.RemoveAt(index);
                }



            }
            else
            {
                ListViewNode.RemoveAt(index);
            }
        }


    }
}
