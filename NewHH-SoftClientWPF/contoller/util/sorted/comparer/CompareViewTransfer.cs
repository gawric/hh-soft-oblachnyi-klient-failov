﻿using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sorted.comparer
{
    public class CompareViewTransfer<T> : IComparer<T> where T : FolderNodesTransfer
    {
        public int Compare(T objX, T objY)
        {
            //0- равны
            //-1 y больше x
            //1 x больше y

            if (objX.uploadStatus.Equals("Ошибка") | objX.uploadStatus.Equals("Запуск"))
            {
                if (objX.UploadStatus.Equals("Запуск"))
                {
                    if (objY.UploadStatus.Equals("Запуск")) return 0;
                    else
                    {
                        return -1;
                    }
                }

                if (objY.uploadStatus.Equals("Ошибка"))
                {
                    return 0;
                }
                else
                {
                    return objXnoError(objX, objY);
                }
            }
            else
            {
                if (objY.uploadStatus.Equals("Ошибка"))
                {
                    return -1;
                }
                else
                {
                    return objYnoError(objX, objY);
                }
            }
            
    
        }

        

        private int objYnoError(T objX, T objY)
        {
            if (objY.uploadStatus.Equals("Выполнено"))
            {
                //в самом конце
                if (objX.UploadStatus.Equals("Ожидает"))
                {
                    return -1;
                }
                else
                {
                    return 0;
                }


            }
            else
            {

                if (objY.uploadStatus.Equals("Ожидает"))
                {

                    return objXWait( objX,  objY);

                }
                else
                {

                    if (objY.uploadStatus.Equals("Запуск"))
                    {

                       return  objXperformed(objX, objY);
                    }
                    else
                    {
                        return 0;

                    }
                }
            }
        }


     

        private int objXperformed(T objX, T objY)
        {
            if (objX.uploadStatus.Equals("Выполнено"))
            {
                return 0;
            }
            else
            {
                if (objX.UploadStatus.Equals("Запуск") & objY.UploadStatus.Equals("Запуск")) return 0;
                else { return 0; }
                //return 1;
            }
        }
        private int objXWait(T objX, T objY)
        {
            if (objX.uploadStatus.Equals("Ожидает"))
            {
                return 0;
            }
            else
            {
                if (objX.uploadStatus.Equals("Запуск") & objY.uploadStatus.Equals("Ожидает")) return 1;
                else
                {
                    return 0;
                }
              
            }
        }
        private int objXnoError(T objX, T objY)
        {
            if (objY.uploadStatus.Equals("Выполнено"))
            {

                return 1;
            }
            else
            {
                if (objY.UploadStatus.Equals("Ожидает"))
                {
                    return 1;
                }
                else
                {
                    if (objX.uploadStatus.Equals("Ошибка") & objY.uploadStatus.Equals("Запуск")) return 1;
                    else { return -1; }
                    //return -1;
                }

            }
        }

    }
}
