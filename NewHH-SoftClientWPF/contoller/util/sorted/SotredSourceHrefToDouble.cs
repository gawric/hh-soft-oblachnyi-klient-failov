﻿using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sorted
{
    public class SotredSourceHrefToDouble
    {
        public string[] deleteDoubleStr(string[] selectHref, List<FileInfoModel> parentListPasteModel)
        {
            List<string> sortArrHref = new List<string>();

            for (int j = 0; j < selectHref.Length; j++)
            {
                string sourcehref = selectHref[j];

                if (sourcehref != null)
                {
                    if (isFiles(sourcehref))
                    {

                        FileInfo fi = new FileInfo(sourcehref);
                        insertCheckHref(fi.Name, sourcehref, parentListPasteModel, sortArrHref);
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(sourcehref);
                        insertCheckHref(di.Name, sourcehref, parentListPasteModel, sortArrHref);
                    }

                }
            }

            //переназанчаем
            return  sortArrHref.ToArray();
        }

        public bool isExistPasteModel(string[] selectHref, List<FileInfoModel> parentListPasteModel)
        {
            bool check = false;
            for (int j = 0; j < selectHref.Length; j++)
            {
                string sourcehref = selectHref[j];

                if (sourcehref != null)
                {
                    if (isFiles(sourcehref))
                    {

                        FileInfo fi = new FileInfo(sourcehref);

                        if (searchDouble(fi.Name, parentListPasteModel)) { check = true; break; }
                        
                    }
                    else
                    {
                        DirectoryInfo di = new DirectoryInfo(sourcehref);
                        DirectoryInfo[] diParentDirectory = getDirectory(ref di);
                        FileInfo[] diParentFiles = getFiles(ref di);

                        //получаем корневую папку и проходим по всем ее подпапкам
                        searchFolder(ref diParentDirectory, ref check, ref parentListPasteModel);


                        //получаем корневую папку и проходим по всем ее файлам
                        searchFiles(ref diParentFiles, ref check, ref parentListPasteModel);

                        if (check == true)
                        {
                            break;
                        }

                    }

                }
            }

            return check;
        }

        private FileInfo[] getFiles(ref DirectoryInfo di)
        {
            return di.GetFiles();
        }
        private DirectoryInfo[] getDirectory(ref DirectoryInfo di)
        {
            return di.GetDirectories();
        }
        private void searchFolder(ref DirectoryInfo[] diParentDirectory, ref bool check, ref List<FileInfoModel> parentListPasteModel)
        {
            for (int d = 0; d < diParentDirectory.Length; d++)
            {
                DirectoryInfo diParent = diParentDirectory[d];

                if (searchDouble(diParent.Name, parentListPasteModel))
                {
                    check = true;
                    break;
                }
            }
        }

        private void searchFiles(ref FileInfo[] diParentFiles , ref bool check , ref List<FileInfoModel> parentListPasteModel)
        {
            for (int d = 0; d < diParentFiles.Length; d++)
            {
                FileInfo fiParentFiles = diParentFiles[d];

                if (searchDouble(fiParentFiles.Name, parentListPasteModel))
                {
                    check = true;
                    break;
                }
            }
        }
        private void insertCheckHref(string fileName, string sourcehref, List<FileInfoModel> parentListPasteModel, List<string> sortArrHref)
        {
            string sortHref = clearDouble(fileName, sourcehref, parentListPasteModel);

            if (sortHref.Equals("") != true)
            {
                sortArrHref.Add(sortHref);
            }
        }

        private string clearDouble(string filename, string href, List<FileInfoModel> parentListPasteModel)
        {
            string hrefSort = "";

            parentListPasteModel.ForEach(delegate (FileInfoModel item)
            {
                if (item.filename.Equals(filename) != true)
                {
                    hrefSort = href;
                }
            });

            return hrefSort;
        }

        private bool searchDouble(string filename, List<FileInfoModel> parentListPasteModel)
        {
            bool check = false;

            parentListPasteModel.ForEach(delegate (FileInfoModel item)
            {
                if (item.filename.Equals(filename))
                {
                    check = true;
                }
            });

            return check;
        }

        private bool isFiles(string path)
        {
            FileInfo fi = new FileInfo(path);

            bool isCheck = false;
            if (fi.Exists)
            {
                isCheck = true;
            }

            return isCheck;
        }

        //будем пропускать файлы если он не найденны в базе данных
        //false добавляет 
        //true пропускает
        public bool checkPasteFolderToDouble(string localNameSource,  Dictionary<string , long> allHrefFolder)
        {
            bool check = false;

            if(allHrefFolder != null)
            {
                if (allHrefFolder.ContainsKey(localNameSource))
                {
                    check = true;
                }
            }
            
            
            return check;
        }

        //true - есть
        //false = нету
        public async Task<bool> isExistsWebDavClient(string localNameSource, WebDavClientExist webDavExist)
        {
            //есть ли папка на сервере
            //true есть
            return  await webDavExist.isExist(localNameSource);
        }
       

       

    }



}
