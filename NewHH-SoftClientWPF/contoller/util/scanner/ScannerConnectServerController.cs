﻿using Apache.NMS;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.blockThread.manualReset;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    public class ScannerConnectServerController
    {
        //Главное подключение к серверу
        private MqClientController _mqclientController;
        //работа с кэшем в базе данных
        private SqliteController sqlLiteController;
 
        private WebDavClientController _webDavClient;
        private UpdateFormMainController updateMainForm;
        private UpdateFormLoginController updateLoginForm;
        private MainWindowViewModel _viewModelMain;
        private FirstSyncFilesController _firstSyncController;
        private BlockingEventController _blockingEventController;

        public ScannerConnectServerController(Container cont)
        {
            _mqclientController = cont.GetInstance<MqClientController>();
            sqlLiteController = cont.GetInstance<SqliteController>();

            _webDavClient = cont.GetInstance<WebDavClientController>();
            updateMainForm = cont.GetInstance<UpdateFormMainController>();
            updateLoginForm = cont.GetInstance<UpdateFormLoginController>();
            _viewModelMain = cont.GetInstance<MainWindowViewModel>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            _blockingEventController = cont.GetInstance<BlockingEventController>();
            createManulReset(_blockingEventController);
        }

        public async Task ScanStartAsync()
        {

            await Task.Run(() =>
            {
                CheckRunningAsync();
            });
        }

       

        private void CheckRunningAsync()
        {
            while(true)
            {
                bool statusMqClient = _mqclientController.isStatusClientMqServer();

                _blockingEventController.getManualReset(staticVariable.Variable.scannerConnectServerControllerTokenId).WaitOne();


                if (statusMqClient == true)
                {
                    
                    try
                    {
                        ISession client = _mqclientController.getClient(0);
                        IPHostEntry host = Dns.GetHostEntry(staticVariable.Variable.getHostExist());
                      
                        //если сессия не создана т.е мы не авторизавались
                        if(client == null) createConnect();
                       
                    }
                    catch (System.Net.Sockets.SocketException host)
                    {
                        Console.WriteLine(host.ToString());
                        string data = "Нет подключения к серверу";
                        updateMainForm.updateTitleMainWindow(data);
                    }
                  
                }
                else
                {
                    //проверяет идет в данный момент подключение или нет
                    if(_mqclientController.isConnectedRunning() != true)
                    {
                        //говорим что подключение началось
                        _mqclientController.setConnectedRunning(true);
                        if (_mqclientController.getClient(0) == null) createConnect();
                    }
                    
                }

                

                Thread.Sleep(8000);

            }
        }

        private void createManulReset(BlockingEventController _blockingEventController)
        {
            _blockingEventController.createManualResetEvent(staticVariable.Variable.scannerConnectServerControllerTokenId);
            _blockingEventController.getManualReset(staticVariable.Variable.scannerConnectServerControllerTokenId).setMre();
        }
        private void createConnect()
        {
            try
            {
                IPHostEntry host = Dns.GetHostEntry(staticVariable.Variable.getHostExist());
                string[] user = sqlLiteController.getSelect().GetUserSqlite();
                //не нашли логин и пароль
                if (user != null)
                {
                    //логин и пароль оказались пустыми
                    if (user[0] != null)
                    {
                        string username = user[0];
                        string password = user[1];

                        _mqclientController.CreateNewConnectMqClientAsync(username, password , _firstSyncController);
                        //_webDavClient.createNewConnect(username, password);

                    }
                    else
                    {
                        updateLoginForm.updateCreatLoginForm("");
                    }
                }
                else
                {
                    updateLoginForm.updateCreatLoginForm("");
                }



            }
            catch (System.Net.Sockets.SocketException host)
            {
                Console.WriteLine(host.ToString());
                string data = "Нет подключения к серверу";
                updateMainForm.updateTitleMainWindow(data);
            }
        }

        
    }
}
