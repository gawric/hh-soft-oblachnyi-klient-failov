﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner.reloadAllTreeView.support
{
    public class AllReload
    {
        private SqliteController sqlcont;
        private MassUpdate massUpdate;
        public AllReload(SqliteController sqlcont)
        {
            this.sqlcont = sqlcont;
            this.massUpdate = new MassUpdate(sqlcont);
        }

        public void AllReloadTreeView(ref FolderNodes treeNode,ref  Dictionary<string, string> allFiles, ref List<FolderNodes> deleteArray, ref ConvertIconType convertIcon, ref CreateModel createModel)
        {

            //Console.WriteLine("Рекурсивный цикл AllReloadTreeView: " + treeNode.FolderName);


            for (int f = 0; f < treeNode.Children.Count; f++)
            {
                FolderNodes modelCLient = treeNode.Children[f];
                FileInfoModel s = sqlcont.getSelect().getSearchNodesByLocation(modelCLient.Location);
                bool del = false;

                if (s != null)
                {
                    if (s.row_id != -1)
                    {
                        //есть ли такой элемент в нашем дереве(allFiles прошли раньше что-бы получить размер всего дерева)
                        if (allFiles.ContainsKey(treeNode.Children[f].Location) == true)
                        {


                            List<FileInfoModel> children = sqlcont.getSelect().GetSqlLiteParentIDFileInfoList(s.row_id);

                            treeNode.Children[f].Row_id = s.row_id;
                            treeNode.Children[f].ParentID = s.parent;
                            treeNode.Children[f].FolderName = s.filename;
                            treeNode.Children[f].Location = s.location;
                            treeNode.Children[f].VersionUpdateRows = s.versionUpdateRows;
                            treeNode.Children[f].Type = s.type;
                            treeNode.Children[f].lastOpenDate = s.lastOpenDate;
                            treeNode.Children[f].sizebyte = s.sizebyte.ToString();
                            treeNode.Children[f].changeDate = s.changeDate;

                            FolderNodes currentFolder = treeNode.Children[f];

                            if (children.Count > 0)
                            {

                                // Console.WriteLine(treeNode.Children[f].Children.Count);
                                //в текущем ноде что-то есть тогда
                                if (treeNode.Children[f].Children.Count > 0)
                                {

                                    //есть ли открытые дети то не будет имени "загрузка" 
                                    if (treeNode.Children[f].Children[0].FolderName.Equals("Загрузка") != true)
                                    {
                                        // ClearNodes(nodes[f]);
                                        //после всех проверок пытаемся перестроить дерево
                                        //очень тяжелая операция в кратце
                                        //получает из базы все ноды данного узла
                                        //получает все ноды из дерева клиента
                                        //сравнивает если есть различия удаляет если нет обновляет или вставляет данные в дерево
                                        massUpdate.MassivUpateReleases(children, treeNode.Children[f], createModel);

                                    }
                                    else
                                    {
                                        Console.WriteLine("Дети есть, нашли имя Загрузка не продолжаем сканирование данного узла");
                                    }

                                }
                                else
                                {

                                    //addFim(children, treeNode.Children[f]);
                                    addFim(children, treeNode.Children[f], convertIcon);
                                }


                            }


                        }

                    }
                }
                else
                {
                    //если в базе не содержится узел 
                    //значит его нужно удалить
                    //узлы с именем "Загрузка" не трогаем т.к по ним проверяем открыт нод или нет
                    if (treeNode.Children[f].FolderName.Equals("Загрузка") != true)
                    {
                        DeleteNodesIndex(treeNode.Children, f);
                        //т.к после удаление элемента весь ArrayList сдвигается на одну позицию
                        //что-бы не пропустить след элемент, мы сдвигаем индекс на -1 позицию
                        f--;
                        del = true;
                    }
                }

                if (del != true)
                {
                    FolderNodes fol = treeNode.Children[f];
                    AllReloadTreeView(ref fol, ref allFiles, ref deleteArray, ref convertIcon, ref createModel);
                }

            }

        }


        private void addFim(List<FileInfoModel> children, FolderNodes modelCLient, ConvertIconType convertIcon)
        {
            //если по фатку папка пустая и нет у ней детей
            //но база говорит что дети есть
            //мы добавляем флаг что здесь появились дети

            for (int k = 0; k < children.Count; k++)
            {
                if (staticVariable.Variable.isFolder(children[k].type))
                {
                    //создание нода
                    FolderNodes newNodes2 = new FolderNodes();
                    newNodes2.changeRows = children[k].changeRows;
                    newNodes2.FolderName = children[k].filename;
                    App.Current.Dispatcher.Invoke(delegate // <--- HERE
                    {
                        newNodes2.Icon = convertIcon.getIconType(children[k].type);
                    });

                    newNodes2.ParentID = children[k].parent;
                    newNodes2.Type = children[k].type;
                    newNodes2.VersionUpdateRows = children[k].versionUpdateRows;
                    newNodes2.Row_id = children[k].row_id;
                    newNodes2.Location = children[k].location;


                    newNodes2.changeDate = children[k].changeDate;
                    newNodes2.createDate = children[k].createDate;
                    newNodes2.lastOpenDate = children[k].lastOpenDate;
                    newNodes2.sizebyte = children[k].sizebyte.ToString();


                    // TreeNodeAdd(treeNode.Children[f], newNodes2);
                    TreeNodeAdd(modelCLient, newNodes2);
                }
            }
        }

        private void TreeNodeAdd(FolderNodes treeNode, FolderNodes newNode)
        {
            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {
                    treeNode.Children.Add(newNode);
                });
            }

        }

        private void DeleteNodesIndex(ObservableCollection<FolderNodes> treeNode, int index)
        {
            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {

                    treeNode.RemoveAt(index);

                });
            }


        }

    }
}
