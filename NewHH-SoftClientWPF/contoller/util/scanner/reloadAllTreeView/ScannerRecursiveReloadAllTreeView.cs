﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.scanner.reloadAllTreeView.support;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    public class ScannerRecursiveReloadAllTreeView
    {

        private SqliteController sqlcont;
        private AllReload allreload;
        private InsertRoot insertRoot;
        public ScannerRecursiveReloadAllTreeView(SqliteController sqlcont)
        {
            this.sqlcont = sqlcont;
            allreload = new AllReload(sqlcont);
            insertRoot = new InsertRoot(sqlcont);
        }
        //перезаписывает все узлы обычно из SYNCREATALLLIST
        public void CallReloadALlRecursiveTreeView(ObservableCollection<FolderNodes> nodes , ConvertIconType convertIcon)
        {
            //подсчтитываем общее кол-во открытых нодов
            //путем прохода рекурсей и записи каждого нода в лист
            Dictionary<string, string> list = new Dictionary<string, string>();

            //тестовый массив для хранения удаленных элементов
            //пока не использую, возможно понадобится
            //если нет нужно будет удалить
            List<FolderNodes> deleteArray = new List<FolderNodes>();
            startCount(ref  nodes , ref list);



            CreateModel createModel = new CreateModel(convertIcon);

            //новая рекурсия теперь  в ввиде дерева
            //полученный текущий нод сравниваем с полученным листом 
            //это сделано что-бы не заполнялись все все дети из базы
            //только те что были изначально открыты
            StartReconstruction(nodes , list , deleteArray , convertIcon , createModel);

        }

        private void startCount(ref ObservableCollection<FolderNodes> nodes , ref Dictionary<string, string> list)
        {
            for (int f = 0; f < nodes.Count; f++)
            {
                countRecursive(nodes, list);
            }
        }

        private void StartReconstruction(ObservableCollection<FolderNodes> nodes, Dictionary<string, string> allFiles , List<FolderNodes> deleteArray , ConvertIconType convertIcon , CreateModel createModel)
        {

            for (int f = 0; f < nodes.Count; f++)
            {
                FolderNodes modelClient = nodes[f];
                FileInfoModel s = sqlcont.getSelect().getSearchNodesByLocation(modelClient.Location);
                bool del = false;

                insert(ref s, ref modelClient, ref f, ref convertIcon, ref createModel, ref nodes, ref del);

                if (del != true)
                {
                    FolderNodes fol = nodes[f];
                    allreload.AllReloadTreeView(ref fol, ref allFiles, ref deleteArray, ref convertIcon, ref createModel);
                }
               
            }
        }

        private void insert(ref FileInfoModel s , ref FolderNodes modelClient , ref int f , ref ConvertIconType convertIcon , ref CreateModel createModel , ref ObservableCollection<FolderNodes> nodes , ref bool del)
        {
            if (s != null)
            {
                if (staticVariable.Variable.isFolder(s.type))
                {
                    if (s.row_id == -1) insertRoot.insertRootDirectory(ref modelClient, ref s, ref createModel);
                }

            }
            else
            {
                if (modelClient.FolderName.Equals("Загрузка") != true)
                {
                    DeleteNodesIndex(nodes, ref f);

                    del = true;
                    f--;
                }


            }
        }
       
     
        private void countRecursive(ObservableCollection<FolderNodes> nodes, Dictionary<string, string> list)
        {

            for (int s2 = 0; s2 < nodes.Count; s2++)
            {
                if(nodes[s2].Location != null)
                {
                    if (nodes[s2].Location.Equals("Загрузка") != true)
                    {

                        if (list.ContainsKey(nodes[s2].Location) != true)
                        {
                            list.Add(nodes[s2].Location, nodes[s2].Location);
                        }

                        countRecursive(nodes[s2].Children, list);
                    }

                }
                else
                {
                    Console.WriteLine("ScanRecursiveReloadALLTreeView->countRecursive -> Ошибка " + nodes[s2] + " location: вернул null");
                }
                
               
            }

        }


       


        private void DeleteNodesIndex(ObservableCollection<FolderNodes> treeNode , ref int index)
        {
            int ind = index;
            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {

                    treeNode.RemoveAt(ind);

                });
            }

               
        }

    

    }
}
