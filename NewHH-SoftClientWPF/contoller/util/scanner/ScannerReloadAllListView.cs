﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    class ScannerReloadAllListView
    {

        private SqliteController sqlcont;


        public ScannerReloadAllListView(SqliteController sqlcont)
        {
            this.sqlcont = sqlcont;
        }

        public void ReloadAllListView(ObservableCollection<FolderNodes>listViewCollection , long[] ActiveId , ConvertIconType convertIcon)
        {
            try
            {
                Console.WriteLine("ScannerReloadAllListView->ReloadAllListView: Перезапись ReloadAllListView ActiveID  " + ActiveId[0]);

                
                //Все дети
                List<FileInfoModel> children = sqlcont.getSelect().GetSqlLiteParentIDFileInfoList(ActiveId[0]);

                ReloadAllListViewModel modelObj = createModel();
                modelObj.children = children;

                scanBasesNodes(listViewCollection, modelObj);


                deleteExisting(children, modelObj.childrenToInsert);
                deleteOldNodes(modelObj.childrenToDelete, listViewCollection);
                insertNewNodes(modelObj.children, listViewCollection, convertIcon);

            }
            catch (System.Windows.Markup.XamlParseException s)
            {
                Console.WriteLine("ScannerReloadAllListView-> ReloadAllListViewОшибка обновления ObservableCollection " + s.ToString());
            }
            
        }

       

        private void scanBasesNodes(ObservableCollection<FolderNodes> listViewCollection , ReloadAllListViewModel modelObj)
        {
            List<FileInfoModel> children = modelObj.children;
            FileInfoModel modelServer = modelObj.modelServer;
            List<FolderNodes> childrenToDelete = modelObj.childrenToDelete;
            List<FileInfoModel> childrenToInsert = modelObj.childrenToInsert;

            //Проход по всем нодам listView
            for (int g = 0; g < listViewCollection.Count; g++)
            {
                //клиент
                FolderNodes modelClient = listViewCollection[g];
                bool del = true;
                bool insert = false;
                //проход по масству базы данных
                for (int s = 0; s < children.Count; s++)
                {
                    modelServer = children[s];
                    //сравнение есть ли такой row_id в базе
                    if (modelClient.Location == modelServer.location)
                    {
                        //если он содержится в базе мы его обновляем
                        modelClient.Row_id = modelServer.row_id;
                        modelClient.ParentID = modelServer.parent;
                        modelClient.FolderName = modelServer.filename;
                        modelClient.Location = modelServer.location;
                        modelClient.VersionUpdateRows = modelServer.versionUpdateRows;
                        modelClient.sizebyte = modelServer.sizebyte.ToString();
                        modelClient.changeDate = modelServer.changeDate;
                        modelClient.lastOpenDate = modelServer.lastOpenDate;
                        modelClient.createDate = modelServer.createDate;

                        del = false;
                        insert = true;

                    }


                }

                if (insert != true)
                {
                    if (modelServer != null)
                    {
                        childrenToInsert.Add(modelServer);
                    }
                }
                
                //удаляем узел из listview т.к в mysql его нету
                if (del == true)
                {
                    childrenToDelete.Add(modelClient);
                }
            }
        }

        private void deleteExisting(List<FileInfoModel> children , List<FileInfoModel> childrenToInsert)
        {
            //Очищаем children(все дети сервера) от children(listview)
            //оставшиеся требуют добавление в listview т.к не были найдены
            for (int y = 0; y < childrenToInsert.Count; y++)
            {

                children.Remove(childrenToInsert[y]);
            }

        }

        private void deleteOldNodes(List<FolderNodes> childrenToDelete , ObservableCollection<FolderNodes> listViewCollection)
        {
            //удаляет из коллекции listview не найденные варианты
            for (int j = 0; j < childrenToDelete.Count; j++)
            {

                DeleteNodesObject(listViewCollection, childrenToDelete[j]);
            }
        }

        private void insertNewNodes(List<FileInfoModel> children , ObservableCollection<FolderNodes> listViewCollection , ConvertIconType convertIcon)
        {
            //все оставшиеся ноды добавляем в listView т.к мы считаем их новыми узлами
            for (int l = 0; l < children.Count; l++)
            {

                FileInfoModel modelFileInfoServer = children[l];

                FolderNodes newNodes = convertFileInfoServerToNewNodes(modelFileInfoServer, convertIcon);

                //true - такой элемент содержится в listView
                //false - такого элемента нет
                if (CheckNodesInListView(modelFileInfoServer, listViewCollection) != true)
                {


                    TreeNodeAdd(listViewCollection, newNodes);

                }
                else
                {
                    Console.WriteLine("ScannerReloadAllListView->ReloadAllListView: Ошибка Node с именем " + modelFileInfoServer.filename + "  найден в активном окне и не будет добавлен");
                }

            }
        }

        private FolderNodes convertFileInfoServerToNewNodes(FileInfoModel modelFileInfoServer , ConvertIconType convertIcon)
        {
            FolderNodes newNodes = new FolderNodes();

       

            newNodes.changeRows = modelFileInfoServer.changeRows;
            newNodes.FolderName = modelFileInfoServer.filename;

            App.Current.Dispatcher.Invoke(delegate
            {
                newNodes.Icon = convertIcon.getIconType(modelFileInfoServer.type);
            });


            newNodes.ParentID = modelFileInfoServer.parent;
            newNodes.Type = modelFileInfoServer.type;
            newNodes.VersionUpdateRows = modelFileInfoServer.versionUpdateRows;
            newNodes.Row_id = modelFileInfoServer.row_id;
            newNodes.Location = modelFileInfoServer.location;
            newNodes.changeDate = modelFileInfoServer.changeDate;
            newNodes.createDate = modelFileInfoServer.createDate;
            newNodes.lastOpenDate = modelFileInfoServer.lastOpenDate;
            newNodes.sizebyte = modelFileInfoServer.sizebyte.ToString();

            return newNodes;
        }

        private bool CheckNodesInListView(FileInfoModel server , ObservableCollection<FolderNodes> listViewCollection)
        {
            bool check = false;
            foreach(FolderNodes item in listViewCollection)
            {
                if(server != null)
                {
                    if(server.location.Equals(item.Location))
                    {
                        check = true;
                    }
                }
            }

            return check;
        }

        private void TreeNodeAdd(ObservableCollection<FolderNodes> treeNode, FolderNodes newNode)
        {
           
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                treeNode.Add(newNode);
            });
        }

        private void DeleteNodesIndex(ObservableCollection<FolderNodes> treeNode, int index)
        {
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {

                treeNode.RemoveAt(index);

            });
        }

        private void DeleteNodesObject(ObservableCollection<FolderNodes> treeNode, FolderNodes obj)
        {
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {

                treeNode.Remove(obj);

            });
        }

        private ReloadAllListViewModel createModel()
        {
            ReloadAllListViewModel model = new ReloadAllListViewModel();

            //Отсортированные дети на удаление
            model.childrenToDelete = new List<FolderNodes>();

            //Все дети найденные в Observable ListView
            model.childrenToInsert = new List<FileInfoModel>();

            model.modelServer = null;

            return model;
        }

    }
}
