﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.mvvm.model.treemodel.insertTreeViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner.insertRecursive.support
{
    public interface IInsertRecursive
    {
        void insertNodesListView(ObservableCollection<FolderNodes> ListView, long[] parentListViewID, ConvertIconType convertIcon);
        void insertNodesRecursive(FolderNodes treeNode, ObservableCollection<FolderNodes> list, int index, InsertTreeViewModel insertObjModel);
    }
}
