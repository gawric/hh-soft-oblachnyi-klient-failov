﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.scanner.insertRecursive.support;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel.insertTreeViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    public class ScannerInsertRecursiveView
    {
        
        private IInsertRecursive insert;
        private IInsertRecursive insertListView;
        public ScannerInsertRecursiveView(SqliteController sqlcont, List<FileInfoModel> listFiles)
        {
            this.insert = new InsertNodesRecursive(sqlcont , listFiles);
            this.insertListView = new InsertListView(listFiles);
        }

        public void CallRecursiveTreeView(ObservableCollection<FolderNodes> nodes , InsertTreeViewModel insertModel)
        {
            //nodes - коллекция для перебора
            //nodes[f] - текущий элемент
            //f - индекс перебора на случай удаления элмента
            //operation - разные операции над нодами
            //CheckNodes - проверка на переименование-изменения в полях и удаление
            //СarryOver - перенос node из folder в другую folder
            for (int f = 0; f < nodes.Count; f++){insert.insertNodesRecursive(nodes[f], nodes, f , insertModel);}
            
        }

        public void insertNodesListView(ObservableCollection<FolderNodes> ListView, long[] parentListViewID, ConvertIconType convertIcon)
        {
            insertListView.insertNodesListView(ListView, parentListViewID,  convertIcon);
        }








    }


}



