﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.proxy
{
    public interface IDirectoryInfoProxy
    {
       
         DateTime LastWriteTime();
         DateTime LastAccessTimeUtc();
         DateTime LastAccessTime();
         DateTime CreationTimeUtc();
         DateTime CreationTime();
         bool Exists();
         DateTime LastWriteTimeUtc();
         string  FullName();
         string Name();
         DirectoryInfo[] GetDirectories();
         IFileInfoProxy[] GetFiles();
    }
}
