﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.convert
{
    public class ConvertLocationWebDavToFileSystem
    {

        public string convertLocationToHref(string locationWebDav, string locatioDownload)
        {
            try
            {
                string remoteWebDav = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;


                string hrefLocal = locationWebDav.Replace(remoteWebDav, locatioDownload + "\\");
                hrefLocal = hrefLocal.Replace("/", "\\");

                string hrefLocalDecode = staticVariable.Variable.DecodeUrlString(hrefLocal);


                return hrefLocalDecode;
            }
            catch(System.NullReferenceException)
            {
                return "";
            }
          
        }
    }
}
