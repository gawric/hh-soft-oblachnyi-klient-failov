﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.convert;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.delete
{
    public class DeleteFolderFileSystem
    {
        private ConvertLocationWebDavToFileSystem _convertHref;
        private SqliteController _sqlliteController;
        public event EventHandler<OnlyCloudDeleteFileSystem> eventDeleteFileSystem;

        public DeleteFolderFileSystem()
        {
      
        }

        public DeleteFolderFileSystem(SqliteController sqlliteController)
        {
            _convertHref = new ConvertLocationWebDavToFileSystem();
            _sqlliteController = sqlliteController;
        }
        public void deleteAllPcSave(List<long[][]> allListFull)
        {
            try
            {
                string locationDownload = _sqlliteController.getSelect().getLocationDownload();

                foreach (long[][] arr in allListFull)
                {
                    arrDeleteOnlyFiles(arr, ref locationDownload);
                }

                foreach (long[][] arr in allListFull)
                {
                    arrDeleteOnlyFolder(arr, ref locationDownload);
                }

                allListFull = null;
            }
            catch(Exception)
            {
                throw;
            }
            

        }
        //1-folder 
        //0 - files
        private void arrDeleteOnlyFolder(long[][] arr , ref string  locationDownload)
        {
            try
            {
                foreach (long[] item in arr)
                {
                    long typefolder = item[1];
                    long listFiles_rowid = item[0];

                    if (typefolder == 1)
                    {
                        SaveToPcModel stpm = getCurrentNodes(ref _sqlliteController, ref listFiles_rowid);

                        if (!isNullLocation(stpm, listFiles_rowid))
                        {
                            string locationWebDav = getLocation(stpm);
                            deleteFolder(ref locationWebDav, ref locationDownload, ref listFiles_rowid);
                        }
                          
                    }

                }
            }
            catch(System.IO.IOException)
            {
                throw;
            }
           
        }

        private void arrDeleteOnlyFiles(long[][] arr, ref string locationDownload)
        {
            try
            {
                foreach (long[] item in arr)
                {
                    long typefolder = item[1];
                    long listFiles_rowid = item[0];

                    if (typefolder == 0)
                    {
                        SaveToPcModel stpm = getCurrentNodes(ref _sqlliteController, ref listFiles_rowid);

                        if(!isNullLocation(stpm,  listFiles_rowid))
                        {
                            string locationWebDav = getLocation(stpm);
                            deleteFiles(ref locationWebDav, ref locationDownload, ref listFiles_rowid);
                        }
                       
                    }

                }
            }
            catch (System.IO.IOException)
            {
                throw;
            }

        }

        private bool isNullLocation(SaveToPcModel stpm , long listFiles_rowid)
        {
            if (stpm.location == null)
            {
                Console.WriteLine("DeleteFolderFileSystem->arrDeleteOnlyFiles: Не критическая ошибка не смогли найти location " + listFiles_rowid);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void deleteFolder(ref string locationWebDav , ref string  locationDownload , ref long listFiles_rowid)
        {
           delete(ref locationWebDav, ref locationDownload, listFiles_rowid, true);
        }

        private void deleteFiles(ref string locationWebDav, ref string locationDownload, ref long listFiles_rowid)
        {
           delete(ref locationWebDav, ref locationDownload, listFiles_rowid, false);  
        }

        private string getLocation(SaveToPcModel stpm)
        {
            return stpm.location;
        }

        private SaveToPcModel getCurrentNodes(ref SqliteController sqlliteController , ref long listFiles_rowid)
        {
           return  _sqlliteController.getSelect().getSqlLitePcSaveListFilesId(listFiles_rowid);
        }

        public void delete(ref string locationWebDav  , ref string locationDownload , long listFilesRowId , bool isFolder)
        {
            string fileSystemPath = _convertHref.convertLocationToHref(locationWebDav , locationDownload);
            delete(fileSystemPath , listFilesRowId, isFolder);
            
        }

        public void deleteFolderRecursive(string fileSystemPath)
        {
            try
            {
                if (Directory.Exists(fileSystemPath))
                {
                    Directory.Delete(fileSystemPath, true);
                }
                else
                {
                    Console.WriteLine("DeleteFolderFIleSystem-> deleteFolderRecursive: не найден путь к папке!!!");
                }
            }
            catch(System.IO.IOException z)
            {
                Console.WriteLine(z.ToString());
            }
           
            
        }

        private void delete(string fileSystemPath , long listFilesRowId, bool isFolder)
        {
            if(isFolder)
            {
                if(Directory.Exists(fileSystemPath)) Directory.Delete(fileSystemPath, true);
                notifyOnlyCloud(listFilesRowId);
            }
            else
            {
                if (File.Exists(fileSystemPath)) File.Delete(fileSystemPath);
                notifyOnlyCloud(listFilesRowId);
            }
        }
        
        private void notifyOnlyCloud(long listFilesRowId)
        {
            OnlyCloudDeleteFileSystem args = new OnlyCloudDeleteFileSystem();
            args.listFilesRowid = listFilesRowId;
            OnThresholdReached(args);
        }
        protected virtual void OnThresholdReached(OnlyCloudDeleteFileSystem e)
        {
            eventDeleteFileSystem?.Invoke(this, e);
        }

    }
}
