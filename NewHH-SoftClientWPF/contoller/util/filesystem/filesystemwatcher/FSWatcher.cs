﻿
using myoddweb.directorywatcher;
using myoddweb.directorywatcher.interfaces;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.stream.myoddweb.directorywatcher.sample;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher
{
    public class FSWatcher: IFSWatcher
    {
        private ISubScribleEvent sefsw;
        private CanselWatch cansel;
        private bool isRun = false;
        public FSWatcher(CancellationController cc , ISubScribleEvent sefsw)
        {
            this.sefsw = sefsw;
            cansel = new CanselWatch(cc);
            cansel.CreateCanselToken();
        }

        public void StartCanselWS()
        {
            cansel.StartCansel();
        }

        public bool IsRun()
        {
            return isRun;
        }
        public void StartRunningWatch(string watchfs)
        {
            Task.Run(() =>
            {
                try
                {
                    // var watcher = new FileSystemWatcher(watchfs);

                    //watcher.NotifyFilter = NotifyFilters.Attributes
                    // | NotifyFilters.CreationTime
                    // | NotifyFilters.DirectoryName
                    // | NotifyFilters.FileName
                    // | NotifyFilters.LastAccess
                    // | NotifyFilters.LastWrite
                    // | NotifyFilters.Security
                    // | NotifyFilters.Size;

                    //watcher.Changed += sefsw.OnChanged;
                    //watcher.Created += sefsw.OnCreated;
                    //watcher.Deleted += sefsw.OnDeleted;
                    //watcher.Renamed += sefsw.OnRenamed;
                    //watcher.Error += sefsw.OnError;
                    //watcher.InternalBufferSize = 64192;
                    //watcher.Filter = "*.*";
                    //watcher.IncludeSubdirectories = true;
                    //watcher.EnableRaisingEvents = true;
                    List<string> listRecFolder = new List<string>();
                    listRecFolder.Add(watchfs);
                    GetAllDirectory(watchfs , ref listRecFolder);
                     var watch = new Watcher();
                     //watch.Add(new Request(watchfs, false));
                    AddWatch(ref watch, ref listRecFolder);
                    //watch.OnRemovedAsync += sefsw.OnDeleted;
                    //watch.OnAddedAsync += sefsw.OnCreated;
                    //watch.OnRenamedAsync += sefsw.OnRenamed;
                    //watch.OnTouchedAsync += sefsw.OnChanged;
                    //watch.OnLoggerAsync += OnOnLoggerAsync;
                       var con = new ConsoleWatch(watch);
                      watch.Start();
                      while (true)
                      {
                        isRun = true;
                        Thread.Sleep(500);

                        if (cansel.IsCansel())
                        {
                            isRun = false;
                            Debug.Print("FSWatcher->StartRunningWatch: Предупреждение отключение слежение за папками Путь: " + watchfs);
                            break;
                        }
                      }


                    watch.Dispose();

                    Debug.Print("FSWatcher->StartRunningWatch: Остановили бесконечный цикл " + watchfs);

                }
                catch(System.InvalidOperationException s)
                {
                    Debug.Print("ошибка 23" + s.ToString());
                }
               
            });


            
        }

        private static readonly object Lock = new object();

        private async Task OnLoggerAsync(ILoggerEvent e, CancellationToken token)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Logger Aync First "+ e.Message);
        }

        private void GetAllDirectory(string watch , ref List<string > listRecFolder)
        {
            IEnumerable<string> allfolders = Directory.EnumerateDirectories(watch);
            foreach (string folder in allfolders)
            {
                GetAllDirectory(folder , ref listRecFolder);
                Console.WriteLine(folder);
                listRecFolder.Add(folder);
            }
            Debug.Print("");
        }

        private void AddWatch(ref Watcher watch, ref List<string> listRecFolder)
        {
            foreach(string path in listRecFolder)
            {
                watch.Add(new Request(path, false));
            }
        }
        private async Task OnStatisticsAsync(IStatistics e, CancellationToken token)
        {
            await AddMessage(ConsoleColor.DarkYellow, DateTime.UtcNow,
              $"Id:{e.Id}\n" +
              $"Number Of Events: {e.NumberOfEvents}\n" +
              $"Elapsed Time: {e.ElapsedTime}",
              token).ConfigureAwait(false);
        }

        private async Task AddMessage(ConsoleColor color, DateTime dt, string message, CancellationToken token)
        {
            await Task.Run(() =>
            {
                lock (Lock)
                {
                    try
                    {
                        Console.ForegroundColor = color;
                        Console.WriteLine($"[{dt:HH:mm:ss.ffff}]:{message}");
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(e.Message);
                    }
                    finally
                    {
                       // Console.ForegroundColor = _consoleColor;
                    }
                }
            }, token);
        }


    }
}
