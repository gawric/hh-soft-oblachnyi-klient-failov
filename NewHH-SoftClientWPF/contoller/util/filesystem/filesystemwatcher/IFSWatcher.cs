﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher
{
    public interface IFSWatcher
    {
        void StartRunningWatch(string watchfs);
        void StartCanselWS();
        bool IsRun();
    }
}
