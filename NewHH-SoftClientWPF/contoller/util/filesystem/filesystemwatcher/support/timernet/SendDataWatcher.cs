﻿using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.timernet
{
    public class SendDataWatcher
    {
        private CreateFolderController cfc;
        private StackOperationUpDown soud;
        private FirstSyncFilesController fsfc;
        public SendDataWatcher(CreateFolderController cfc, StackOperationUpDown soud , FirstSyncFilesController fsfc)
        {
            this.cfc = cfc;
            this.soud = soud;
            this.fsfc = fsfc;
        }

        public async Task CreateFolder(TimerFimModel[] arr)
        {
            foreach(TimerFimModel tfm in arr)
            {
                if(staticVariable.Variable.isFolder(tfm.sourceFim.type))
                {
                    if(tfm.sourceFim.changeRows.Equals("CREATE"))
                    {
                        await cfc.TrainingCreateFolder(tfm.pasteFim, tfm.sourceFim.filename);
                    }
                    
                }
               
            }
            
        }
        //int countUploadFiles = 0;
        public void UploadFiles(TimerFimModel[] arr)
        {
            foreach (TimerFimModel tfm in arr)
            {
                if (!staticVariable.Variable.isFolder(tfm.sourceFim.type))
                {
                    if(tfm.sourceFim.changeRows.Equals("UPDATE"))
                    {
                        DeleteFiles(tfm.sourceFim, "DELETE");
                        soud.addStackUpload(tfm.listHref, tfm.pasteFim);
                        //countUploadFiles++;
                    }
                    else
                    {
                        if(!tfm.sourceFim.changeRows.Equals("DELETE"))
                        {
                            soud.addStackUpload(tfm.listHref, tfm.pasteFim);
                            //countUploadFiles++;
                        }
                       
                    }
                    
                }
                   
            }
            //Debug.Print("SendDataWatcher->UploadFiles: Всего отправлено в стек на заливку "+ countUploadFiles);
        }

        public void Delete(TimerFimModel[] arr)
        {
            foreach (TimerFimModel tfm in arr)
            {
                if(tfm.sourceFim.changeRows.Equals("DELETE"))
                {
                    DeleteFiles(tfm.sourceFim, "DELETE");
                }
                
            }
        }

        private void DeleteFiles(FileInfoModel fim , string eventFiles)
        {
            FolderNodes[] folderArr = { ConvertFimtoFolderNodes(fim) };
            fsfc.DeleteTaskNoFormNoNewThread(ConvertFimtoFolderNodes(fim), folderArr, eventFiles);
        }

        private FolderNodes ConvertFimtoFolderNodes(FileInfoModel pasteModel)
        {
            FolderNodes fol = new FolderNodes();
            fol.Row_id = pasteModel.row_id;
            fol.FolderName = pasteModel.filename;
            fol.Location = pasteModel.location;
            fol.ParentID = pasteModel.parent;
            fol.Type = pasteModel.type;

            return fol;
        }


    }
}
