﻿
using myoddweb.directorywatcher.interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support
{
    public interface ISubScribleEvent
    {
        Task OnChanged(IFileSystemEvent fse, CancellationToken token);
        Task OnCreated(IFileSystemEvent fse, CancellationToken token);
        Task OnDeleted(IFileSystemEvent fse, CancellationToken token);
        Task OnRenamed(IRenamedFileSystemEvent rfse, CancellationToken token);
        Task OnError(object sender, ErrorEventArgs e);
    }
}
