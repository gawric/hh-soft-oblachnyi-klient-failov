﻿
using myoddweb.directorywatcher.interfaces;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack.deffered;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher
{
    public class SubsribeEventFsWatcher: ISubScribleEvent
    {

        public event EventHandler<SubscribeDataModel> eventAddData;
        private ITimeSendData timer;
        private IStackFSEvent stackEvent;
        private int createItem = 0;
        private int updateItem = 0;
        public SubsribeEventFsWatcher(SqliteController sql , StackOperationUpDown soud , CreateFolderController cfc , FirstSyncFilesController fsfc)
        {
            ISubscribeData subData = new SubscribeData(sql);
            IDelayedLaunch dl = new DelayedLaunch(subData);
            timer = new TimerSendData(cfc , soud, dl , sql , fsfc);
            stackEvent = new StackFSEvent(subData , timer , dl);
            stackEvent.StartStack();
        }
        public  async Task OnChanged(IFileSystemEvent fse, CancellationToken token)
        {
          //  if (fse.ChangeType != WatcherChangeTypes.Changed)
            //{
            //    return;
           // }
            //if (IsFiles(e.FullPath)) stackEvent.AddEventData(CreateModel(e.FullPath, "UPDATE", ""));
           // stackEvent.AddEventData(CreateModel(fse.FullName, "UPDATE", ""));
            Console.WriteLine($"Touched Event: {fse.FullName} + "+updateItem++);
        }

        private bool IsFiles(string fullPath)
        {
            if (File.Exists(fullPath))
                return true;

            return false;
        }

        public async Task OnCreated(IFileSystemEvent fse, CancellationToken token)
        {
            string value = $"Created Event: {fse.FullName} номер создания"+ createItem++;
            Console.WriteLine(value);
            
            //stackEvent.AddEventData(CreateModel(fse.FullName, "CREATE", ""));
        }

        public async  Task   OnDeleted(IFileSystemEvent fse, CancellationToken token)
        {
            //Console.WriteLine($"Deleted Event: {fse.FullName}");
            stackEvent.AddEventData(CreateModel(fse.FullName, "DELETE", ""));
        }
           

        public async Task OnRenamed(IRenamedFileSystemEvent rfse, CancellationToken token)
        {
           // Console.WriteLine($"Renamed:");
           // Console.WriteLine($"    Old: {rfse.PreviousFullName}");
           // Console.WriteLine($"    Rename Event: {rfse.FullName}");

            stackEvent.AddEventData(CreateModel(rfse.FullName, "RENAME", rfse.PreviousFullName));

        }

        public async Task OnError(object sender, ErrorEventArgs e)
        {
            PrintException(e.GetException());
        }
            

        public  void PrintException(Exception ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine();
                PrintException(ex.InnerException);
            }
        }




        public void AddEvent(SortedSet<FileInfoModel> sArr)
        {
            SubscribeDataModel mArgs = new SubscribeDataModel();
            OnThresholdReached(mArgs);
            
        }

        private StackFSModel CreateModel(string fullPath , string eventFs , string oldFullpath)
        {
            StackFSModel model = new StackFSModel();
            model.fullPath = fullPath;
            model.oldPath = oldFullpath;
            model.eventFS = eventFs;

            return model;
        }
        protected virtual void OnThresholdReached(SubscribeDataModel e)
        {
            eventAddData?.Invoke(this, e);
        }

       
    }
}
