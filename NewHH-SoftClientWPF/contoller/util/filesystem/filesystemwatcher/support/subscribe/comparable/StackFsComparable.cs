﻿using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.comparable
{
    public class StackFsComparable : IComparer<StackFSModel>
    {
        public int Compare(StackFSModel x, StackFSModel y)
        {
            var x_byte = x.fullPath.ToCharArray();
            var y_byte = y.fullPath.ToCharArray();

            if (x.fullPath.Equals(y.fullPath))
            {
                //var x_byte_change = x.sourceFim.changeRows.ToCharArray();
                //var y_byte_change = y.sourceFim.changeRows.ToCharArray();

                //if (x_byte_change.SequenceEqual(y_byte_change))
                //{
                return 0;
                // }
            }

            if (x.fullPath.Length > y.fullPath.Length) return 1;
            if (x.fullPath.Length < y.fullPath.Length) return -1;

            return 1;
        }
    }
}
