﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.comparable;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack.deffered
{
    public class DelayedLaunch : StackEvent, IDelayedLaunch
    {
        private SortedSet<StackFSModel> listDelayed;
        private SortedSet<TimerFimModel> verifiedList;
        private List<StackFSModel> removeList;
        private bool isClearArr = false;
        private bool isAddTemp = false;
        private volatile bool isRunThread;
        private System.Timers.Timer delayTimer;
        private int testCount = 0;
        public DelayedLaunch(ISubscribeData subData)
            : base(subData)
        {
            this.listDelayed = new SortedSet<StackFSModel>(new StackFsComparable());
            this.verifiedList = new SortedSet<TimerFimModel>(new FIMComparableId());
            this.removeList = new List<StackFSModel>();
            SetTimer();
            StartTimer();
        }

        public void AddDelay(StackFSModel item)
        {
            SafeThreadAddDelay(item);
        }

        public TimerFimModel[] VerifiedList()
        {
            Debug.Print("DelayedLaunch->DelayedStart: Попытка получить обработанные данные для отправки   " + verifiedList.Count);
            TimerFimModel[] arr = verifiedList.ToArray();
            ClearVerifiedList();
            return arr;
        }
        //если мы вставляем большой обьем данных
        //часто происходит одновременно удаление и добавление, что вызывает сбой доступа к колекции
        //поэтому мы ждем пока не закончится удаление данных и после этого добавляем
        private void SafeThreadAddDelay(StackFSModel item)
        {
                AddSave(item);
        }

        private void AddSave(StackFSModel item)
        {
            while (true)
            {
                if (!isClearArr)
                {
                    if (item != null) listDelayed.Add(item);
                    
                    break;
                }
                Thread.Sleep(50);
            }
        }

        private void AddDelayNormal(StackFSModel item)
        {
            if (item != null) listDelayed.Add(item);
        }

        private void SetTimer()
        {
            delayTimer = new System.Timers.Timer(10000);
            delayTimer.Elapsed += (sender, e) => DelayedStart(listDelayed);
        }

        private void StartTimer()
        {
            delayTimer.Start();
        }


        private void DelayedStart(SortedSet<StackFSModel> listDelayed)
        {
            Debug.Print("DelayedLaunch->DelayedStart: Размер массива на удержании " + listDelayed.Count());
            if (isRunThread) return;
           
            try
            {
                isRunThread = true;
                if (!isClearArr)
                {
                    StackFSModel[] arr = listDelayed.ToArray<StackFSModel>();

                    foreach (StackFSModel item in arr)
                    {
                        if (item != null)
                        {

                            if (item.eventFS.Equals("UPDATE"))
                            {
                                FileInfoModel curFim = GetFimToEventChange(item.fullPath);
                                if (curFim != null)
                                {
                                    AddFimList(item.fullPath, curFim);
                                    AddRemoveList(item);
                                    testCount = testCount + 1;
                                }


                            }
                            else if (item.eventFS.Equals("CREATE"))
                            {
                                FileInfoModel creFim = GetFimToEventCreate(item.fullPath);
                                if (creFim != null)
                                {
                                    AddFimList(item.fullPath, creFim);
                                    AddRemoveList(item);
                                    testCount = testCount + 1;
                                }


                            }
                            else if (item.eventFS.Equals("DELETE"))
                            {
                                FileInfoModel curFim = GetFimToEventDelete(item.fullPath);
                                if (curFim != null)
                                {
                                    AddFimList(item.fullPath, curFim);
                                    AddRemoveList(item);
                                    testCount = testCount + 1;
                                }


                            }
                            else if (item.eventFS.Equals("RENAME"))
                            {
                                FileInfoModel oldFim = GetFimToEventDelete(item.oldPath);
                                AddFimList(item.fullPath, oldFim);
                                AddRemoveList(item);


                                if (oldFim != null)
                                {
                                    FileInfoModel creFim = GetFimToEventCreate(item.fullPath);
                                    AddFimList(item.fullPath, creFim);
                                    AddRemoveList(item);
                                    testCount = testCount + 1;
                                }


                            }

                        }

                    }
                    //Debug.Print("DelayedLaunch->DelayedStart: Всего обработано и передано на обработку в гланый сервер   " + testCount);
                    arr = null;
                }


                RemoveItem(listDelayed);

                isRunThread = false;
            }
            catch(System.InvalidOperationException s)
            {
                Debug.Print("Ошибка " + s.ToString());
            }
            
        }

        private void RemoveItem(SortedSet<StackFSModel> listDelayed)
        {
            if (removeList.Count > 0)
            {
                SetClear(true);

                Remove();
                removeList.Clear();

                SetClear(false);
            }

           
        }

        
        private void Remove()
        {
            try
            {
                Debug.Print("Перед удалением размер listDelay " + listDelayed.Count + "   " + listDelayed.ToString());
                SortedSet<StackFSModel> listSort = new SortedSet<StackFSModel>(new StackFsComparable());
                SafeIterationCollection(ref listSort);

                List<StackFSModel> list  = listDelayed.ToList();
                Debug.Print("Пытаемся удалить кол-во элементов " + listSort.Count + " Имя потока " + Thread.CurrentThread.ManagedThreadId);

                foreach (StackFSModel item in listSort)
                {
                    list.RemoveAll(x => x.fullPath.Equals(item.fullPath));
                }
                Debug.Print("делаем listDelayed.Clear " + " Имя потока " + Thread.CurrentThread.ManagedThreadId);
                listDelayed.Clear();
                Debug.Print("делаем list.ForEach " + " Имя потока " + Thread.CurrentThread.ManagedThreadId);
                list.ForEach(x=>listDelayed.Add(x));
                Debug.Print("Закончили forEach " + " "+ Thread.CurrentThread.ManagedThreadId);
                Debug.Print("После удаление размер listDelay "+ listDelayed.Count +"   " +listDelayed.ToString());
            }
            catch(InvalidOperationException ex)
            {
                Debug.Print("DelayedLaunch->Remove критическая ошибка " + ex.ToString()); ;
            }
            

           
        }

        private void SafeIterationCollection(ref SortedSet<StackFSModel> listSort)
        {
            while(true)
            {
                if(!isAddTemp)
                {

                    Debug.Print("Итерация данных в listDelay");

                    foreach (StackFSModel itemDelay in listDelayed)
                    {
                        Debug.Print("Итерация на удаление "+ itemDelay.fullPath+ " Имя потока " + Thread.CurrentThread.ManagedThreadId);
                        if (removeList.Any(x => x.fullPath.Equals(itemDelay.fullPath)))
                        {
                            listSort.Add(itemDelay);
                        }
                    }
                    break;
                }
              

                Thread.Sleep(50);
            }
           

        }
        private void SetClear(bool isclear)
        {
            this.isClearArr = isclear;
        }
        private void AddRemoveList(StackFSModel item)
        {
            if(!removeList.Contains(item))
            {
                removeList.Add(item);
            }
          
        }

     
        private void AddFimList(string href , FileInfoModel item)
        {
            verifiedList.Add(ConvertTFM( href,  item));
        }

        private TimerFimModel ConvertTFM(string href, FileInfoModel item)
        {
            TimerFimModel tfm = new TimerFimModel();
            string[] pth = { href };
            tfm.listHref = pth;
            tfm.sourceFim = item;
            return tfm;
        }

        public void ClearVerifiedList()
        {
            verifiedList.Clear();
        }
    }
}
