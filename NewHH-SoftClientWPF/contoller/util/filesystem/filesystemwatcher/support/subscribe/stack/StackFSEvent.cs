﻿using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack.deffered;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack
{
    public class StackFSEvent : StackEvent, IStackFSEvent
    { 
        private BlockingCollection<StackFSModel> dirs;
        private bool isStackRunnins = false;
        private bool isStackStop = false;
        private ITimeSendData timer;
        private IDelayedLaunch dl;
        private int addCount = 0;
        private int fimTimerCount = 0;
        private int delayTimerCount = 0;
        public StackFSEvent(ISubscribeData subData , ITimeSendData timer, IDelayedLaunch dl)
            :base(subData)
        {
            this.timer = timer;
            this.dirs = new BlockingCollection<StackFSModel>();
            this.dl = dl;
        }
        public void StartStack()
        {
            try
            {
                Task.Run(() =>
                {
                    while (true)
                    {
                        isStackRunnins = true;

                        StackFSModel item = dirs.Take();
                       // Debug.Print(" Кол-во итемов в стеке FSEvent" + dirs.Count);
                        WorkEvent(ref item);

                        if (isStackStop) break;
                    }

                });
                
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("StackOperationUpDown->startStackNetwork: Бесконечный цикл завершен!");
            }

        }

        private void WorkEvent(ref StackFSModel item)
        {
            if(item.eventFS.Equals("UPDATE"))
            {
                if(!IsFolder(item.fullPath))
                {
                    FileInfoModel curFim = GetFimToEventChange(item.fullPath);
                    AddDelayOrFimNull(ref item, curFim);
                    AddFimTimer(curFim , item.fullPath);
                }
                
            }
            else if(item.eventFS.Equals("CREATE"))
            {
                FileInfoModel creFim = GetFimToEventCreate(item.fullPath);
                AddDelayOrFimNull(ref item, creFim);
                AddFimTimer(creFim , item.fullPath);
                //Debug.Print("Всего Добавлено в обработку " + addCount++);
            }
            else if(item.eventFS.Equals("DELETE"))
            {
                FileInfoModel curFim = GetFimToEventDelete(item.fullPath);
                AddDelayFimDelete(ref item, curFim);
                AddFimTimer(curFim , item.fullPath);
            }
            else if(item.eventFS.Equals("RENAME"))
            {
                EventRename(ref item);
            }
        }

        //если изменения касаются папок
        //мы их игнорируем т.к следим только за изменениями в файлах
        private bool IsFolder(string path)
        {
            if(Directory.Exists(path))
            {
                return true;
            }

            return false;
        }
        private void EventRename(ref StackFSModel item)
        {
            addFimRename(ref item);
        }

        private void AddDelayFimDelete(ref StackFSModel item, FileInfoModel curFim)
        {
            AddDelayItem(item, curFim);
        }
        private void AddDelayOrFimNull(ref StackFSModel item, FileInfoModel curFim)
        {
            AddDelayItem(item, curFim);
        }
        private void addFimRename(ref StackFSModel item)
        {
            FileInfoModel oldFim = GetFimToEventDelete(item.oldPath);
            AddDelayItem(item, oldFim);

            if (oldFim != null)
            {
                AddFimTimer(oldFim , item.fullPath);

                FileInfoModel creFim = GetFimToEventCreate(item.fullPath);
                AddFimTimer(creFim , item.fullPath);
            }
        }

        private void AddDelayItem(StackFSModel item , FileInfoModel fim)
        {
            if (fim == null)
            {
                //Debug.Print("Добавлено DelayItem " + delayTimerCount++);
                dl.AddDelay(item);
            }
            
        }
        private void AddFimTimer(FileInfoModel fim , string href)
        {
            TimerFimModel fModel = ConvertFimToTFM( fim,  href);
            if (fModel != null)
            {
                Debug.Print("Добавлено FIMTimer " + fimTimerCount++);
                timer.InsertItemArr(fModel);
            }
            
        }

        private TimerFimModel ConvertFimToTFM(FileInfoModel fim, string href)
        {
            if (fim == null) return null;

            TimerFimModel fModel =  new TimerFimModel();
            string[] hrefL = { href };
            fModel.listHref = hrefL;
            fModel.sourceFim = fim;

            return fModel;
        }
        public void SetStopStack(bool isStop)
        {
            isStackStop = isStop;
        }
        public bool IsRunningStack()
        {
            return isStackRunnins;
        }

        public void AddEventData(StackFSModel model)
        {
            dirs.Add(model);
        }
    }
}
