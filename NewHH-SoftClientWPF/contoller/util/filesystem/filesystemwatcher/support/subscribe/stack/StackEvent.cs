﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack
{
    public class StackEvent: ISubscribeData
    {
        private ISubscribeData subData;

        public StackEvent(ISubscribeData subData)
        {
            this.subData = subData;
        }

        public FileInfoModel GetFimToEventChange(string path)
        {
            FileInfoModel fim = subData.GetFimToEventChange(path);
            if (fim != null) fim.changeRows = "UPDATE";
            return fim;
        }

        public FileInfoModel GetFimToEventCreate(string path)
        {
            FileInfoModel fim = subData.GetFimToEventCreate(path);
            if (fim != null) fim.changeRows = "CREATE";

            return fim;
        }

        public FileInfoModel GetFimToEventDelete(string path)
        {
            FileInfoModel fim =  subData.GetFimToEventDelete(path);
            if(fim != null)fim.changeRows = "DELETE";
            return fim;
        }
    }
}
