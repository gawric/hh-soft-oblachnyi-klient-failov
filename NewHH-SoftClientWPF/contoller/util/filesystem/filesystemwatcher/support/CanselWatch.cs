﻿using NewHH_SoftClientWPF.contoller.cancellation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support
{
    public class CanselWatch
    {
        private CancellationController cc;
        public CanselWatch(CancellationController cc)
        {
            this.cc = cc;
        }

        public void CreateCanselToken()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            cc.removeObjectWarehouse(staticVariable.Variable.WatchFileSystemCancelationTokenId);
            cc.addWarehouse(staticVariable.Variable.WatchFileSystemCancelationTokenId, cts);
        }

        public bool IsCansel()
        {
            return cc.getCancellationTokenToWarehouse(staticVariable.Variable.WatchFileSystemCancelationTokenId).IsCancellationRequested;
        }
        public void StartCansel()
        {
            cc.startCansel(staticVariable.Variable.WatchFileSystemCancelationTokenId);
        }
    }
}
