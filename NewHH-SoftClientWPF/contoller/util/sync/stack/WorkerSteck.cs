﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Model;
namespace NewHH_SoftClientWPF.contoller.sync.stack
{
    public class WorkerSteck
    {
       
        private FileInfoModel[] containerAllFiles;
        private FileInfoModelSupport modelSupport;
        private Stack<string> dirs;
        private CreateItem create;
        private WokrkerStackModel workerModel;
        private int count = 0;

        public WorkerSteck(Dictionary<string, long> parentArray , FileInfoModel[] containerAllFiles , FileInfoModelSupport modelSupport , Stack<string> dirs)
        {
           
            this.containerAllFiles = containerAllFiles;
            this.modelSupport = modelSupport;
            this.dirs = dirs;

            workerModel = new WokrkerStackModel();
            workerModel.parentArray = parentArray;
            workerModel.containerAllFiles = containerAllFiles;
        

            create = new CreateItem();
        }

        public void goWorking(TraverseTreeModel treeObjModel, Dictionary<long, long> sqlArray, Dictionary<string, long> sqlArrayHref, IEnumerable<Item> webAll)
        {

            foreach (Item modelWebDav in webAll)
            {
                try
                {
                    if (treeObjModel._cancellationToken.IsCancellationRequested)
                    {
                        workerModel.sqlArray.Clear();
                        workerModel.sqlArrayHref.Clear();
                        containerAllFiles = null;

                        break;
                    }
                    // Perform whatever action is required in your scenario.
                   // Console.WriteLine("Количество: " + count + "+Пример поиска файла из webdav Stack Путь файла " + modelWebDav.DisplayName);

                    workerModel.sqlArray = sqlArray;
                    workerModel.sqlArrayHref = sqlArrayHref;
                    workerModel.modelWebDav = modelWebDav;

                    

                    if (modelWebDav.ContentLength != null)
                    {

                        modelWebDav.ContentType = Path.GetExtension(modelWebDav.DisplayName);
                        convertModelToFileInfoModelNoDelete(treeObjModel, workerModel);

                    }
                    else
                    {
                        modelWebDav.ContentType = "folder";
                        convertModelToFileInfoModelNoDelete(treeObjModel, workerModel);

                        dirs.Push(modelWebDav.Href);
                    }


                    count++;
                }
                catch (System.IO.FileNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

            }

        }


        //modelServer текущая модель от сервера
        //containerAllFiles - конечный архив хранит все модели FileInfoModel для передачи на сервера
        //parentArray - Хранит все parentName - Name.Href - что-бы выдать им id номера для заполнения по индексу массива
        //networkJson - observer для отправки серверу сообщений
        //progress - ход выполнения 
        public void convertModelToFileInfoModelNoDelete(TraverseTreeModel treeObjModel , WokrkerStackModel workerModel)
        {
            int s2;
            int[] countArray = treeObjModel.countArray;

            s2 = searchLastIndex(workerModel);

            workerModel.parentHref = workerModel.modelWebDav.Href.Substring(0, s2);
          

            //передаем сканированные файлы полученный от WebDavServer
            //проверяем есть ли он в базе данных (тогда его пропускаем)
            //если его не можем найти в бд, мы ищем в Dictionary parentHref где содержатся все новые файлы полученные от wevDavServer
            //после этого проходим по sqlArray в нем содержатся все row_id находящиеся в базе и удаляем из нее те файлы которые были найденны 
            //оставшиеся файлы sqlArray будут удалены из базы и переданы серверу на удаление
            FileInfoModel modelFileInfo = create.getFileInfoModelToWebDav(treeObjModel, workerModel,containerAllFiles ,  modelSupport);
            
           
            try
            {

                int f = countArray[0] / 10;

                updateReport(treeObjModel, f);
                sendToServer(countArray, treeObjModel);
                
            }
            catch (System.IndexOutOfRangeException sr)
            {
                Console.WriteLine(sr);
            }
            catch (System.NullReferenceException sr)
            {
                Console.WriteLine(sr);
            }


            containerAllFiles[countArray[0]] = modelFileInfo;
            countArray[0]++;
            staticVariable.Variable.allCountAutoScanInfo = staticVariable.Variable.allCountAutoScanInfo + 1;

        }

        private void sendToServer(int[] countArray, TraverseTreeModel treeObjModel)
        {

            if (countArray[0] == staticVariable.Variable.CountFilesTrasferServer)
            {

                countArray[0] = 0;

                SendSINGLUPDATE(treeObjModel);
                updateReport(treeObjModel, 0);

                Console.WriteLine("WorkerSteck->Отправка данных на сервер замер времени ======  " + DateTime.Now + "  =======");


                for (int k = 0; k < containerAllFiles.Length; k++)
                {
                    containerAllFiles[k] = null;
                }



            }

        }
        private int searchLastIndex(WokrkerStackModel workerModel)
        {
            //путь у папок заканчивается "/" его нужно убрать т.к файлы заканичиваются расширением
            if (staticVariable.Variable.isFolder(workerModel.modelWebDav.ContentType))
            {
                return   searchParentIndex(workerModel);
            }
            else
            {
                return   workerModel.modelWebDav.Href.LastIndexOf("/") + 1;
            }
        }

        private int searchParentIndex(WokrkerStackModel workerModel)
        {
            //находим последнюю / 
            int s = workerModel.modelWebDav.Href.LastIndexOf("/") - 1;
            //убираем ее
            string newparent = workerModel.modelWebDav.Href.Substring(0, s);

            //находим следущую
            return  newparent.LastIndexOf("/") + 1;
        }
        private void updateReport(TraverseTreeModel treeObjModel , double procent)
        {
            treeObjModel.progress.Report(procent);
        }

        private void SendSINGLUPDATE(TraverseTreeModel treeObjModel)
        {
            treeObjModel.networkJson.updateSendJsonLAZYSINGLUPDATEPARTIES(containerAllFiles, treeObjModel.newVersionBases);
        }

    }
}
