﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.sync.stack;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.sync
{
    public class DemoSteckWalker
    {
        private string rootDir = "/";
        private Stack<string> dirs;
        private Dictionary<string, long> parentArray;
        private FileInfoModel[] containerAllFiles;
        private FileInfoModelSupport modelSupport;
        //private int[] countArray;
        private TraverseTreeModel treeObjModel;
        private WorkerSteck worker;
      


        public DemoSteckWalker()
        {
            dirs = new Stack<string>(100);
            treeObjModel = new TraverseTreeModel();
            treeObjModel.countArray = new int[1];

            this.parentArray = new Dictionary<string, long>();
            this.containerAllFiles = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            this.modelSupport = new FileInfoModelSupport();

            worker = new WorkerSteck( parentArray, containerAllFiles,  modelSupport,  dirs);
            
        }


        private void addDoubleHrefToDeleteServer(Dictionary<long, string> doubleHref , FileInfoModel[] containerAllFiles , TraverseTreeModel treeObjModel)
        {
            //0 - всегда занят под disk root
            int index = 1;
            //мы их посылаем на удаление т.к в нашей базе мы нашли дубликаты href ссылок
            foreach (KeyValuePair<long, string> item in doubleHref)
            {
                FileInfoModel sqlliteItem = new FileInfoModel();

                sqlliteItem.filename = item.Value;
                sqlliteItem.location = item.Value;
                sqlliteItem.row_id = item.Key;
                sqlliteItem.changeRows = "DELETE";

                if(index <= containerAllFiles.Length)
                {
                    containerAllFiles[index] = sqlliteItem;
                    treeObjModel.countArray[0] = index;
                }
                else
                {
                    Console.WriteLine("DemoStakWalker->addDoubleHrefToDeleteServer: Критическая ошибка контейнер переполнен!!! больше 1000 записей");
                }
                

                index++;
            }

            doubleHref.Clear();
            doubleHref = null;

        }
        public  async Task<FileInfoModel[]> TraverseTree(AutoSyncToTimeObj _autoSynObj  , long newVersionBases,  IProgress<double> progress, string[] usernameandPassword, Dictionary<long, long> sqlArray, Dictionary<string, long> sqlArrayHref, CancellationToken _cancellationToken , Dictionary<long , string > doubleHref)
        {

            treeObjModel.client = _autoSynObj._webDavClient.getWebDavClient();
            treeObjModel.newVersionBases = newVersionBases;
            treeObjModel.networkJson = _autoSynObj._updateNetworkJsonController;
            treeObjModel.progress = progress;
            treeObjModel.usernameandPassword = usernameandPassword;
            treeObjModel.supportTopicModel = _autoSynObj._supportTopicModel;
            treeObjModel._sqlLiteController = _autoSynObj._sqlLiteController;
            treeObjModel.sqlArray = sqlArray;
            treeObjModel.sqlArrayHref = sqlArrayHref;
            treeObjModel._cancellationToken = _cancellationToken;

            addDoubleHrefToDeleteServer(doubleHref, containerAllFiles , treeObjModel);



            //Рутовая папка откуда будет все добавляться
            containerAllFiles[0] = modelSupport.getRootFileInfoModel(newVersionBases);

            if (_cancellationToken.IsCancellationRequested){

                treeObjModel.sqlArray.Clear();
                treeObjModel.sqlArrayHref.Clear();
               
                return new FileInfoModel[0];

            }

            Client webDavClient = createNewConnectWebDav(_autoSynObj._webDavClient.getWebDavClient(), usernameandPassword);
            Item item = await webDavClient.GetFolder(rootDir);

            if (item == null) {
                throw new ArgumentException();
            }

            
            dirs.Push(rootDir);

            while (dirs.Count > 0)
            {
                string dir = dirs.Pop();
                IEnumerable<Item> webAll;

                try
                {
                   
                    webAll = await webDavClient.List(dir);
                   
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(e.Message);
                    _autoSynObj.isStop = true;
                    break;
                }
                catch (System.IO.DirectoryNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                    _autoSynObj.isStop = true;
                    break;
                }
                catch (WebDAVClient.Helpers.WebDAVException e)
                {
                    Console.WriteLine(e.Message);
                    dirs.Clear();
                    sqlArray.Clear();
                    sqlArrayHref.Clear();
                    Console.WriteLine("DemoSteckWalker->TraverseTree: Отмена сканирования Инфо:  структура папок изменилась");
                    _autoSynObj.isStop = true;
                    break;
                }

               

                worker.goWorking(treeObjModel , sqlArray,  sqlArrayHref,  webAll);


            }

            clearTemp(treeObjModel);

        

            return containerAllFiles;
        }

        

        private Client createNewConnectWebDav(WebDavClient client , string[] usernameandPassword)
        {
           return  client.CreateNewConnect(usernameandPassword[0], usernameandPassword[1]);
        }

     
        private void clearTemp(TraverseTreeModel treeObjModel)
        {
            try
            {
                int[] countArray = treeObjModel.countArray;

                //в самом конце в массиве остается часть не перезаписанных данных их нужно обнулить
                for (countArray[0] = countArray[0]; countArray[0] < containerAllFiles.Length; countArray[0]++)
                {
                    containerAllFiles[countArray[0]] = null;
                }
            }
            catch (System.IndexOutOfRangeException s)
            {
                Console.WriteLine(s);
            }
            parentArray = null;

        }
    }
}

