﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network
{
    class SyncCreateAllList
    {
        private SyncCreateAllListModel syncCreateAllList;
        private UpdateNetworkJsonController _updateSendController;
        private AutoScanInfoModel _autoScanInfoModel;
        public SyncCreateAllList(SyncCreateAllListModel syncCreateAllList , UpdateNetworkJsonController _updateSendController , ref AutoScanInfoModel autoScanInfoModel)
        {
            this.syncCreateAllList = syncCreateAllList;
            this._updateSendController = _updateSendController;
            this._autoScanInfoModel = autoScanInfoModel;
        }


        public void SyncTraining()
        {
            if (syncCreateAllList.responceServer.textmessage.Length >= 5)
            {
                string statusParties = syncCreateAllList.responceServer.textmessage[4];
                string newIdVersionBases = syncCreateAllList.responceServer.textmessage[0];

                if (statusParties.Equals("BEGINPARTIES"))
                {
                    Console.WriteLine("Пришел пакет с запросом   BEGINPARTIES");
                    BeginParties();
                }
                else if (statusParties.Equals("ENDPARTIES"))
                {
                    Console.WriteLine("Пришел пакет с запросом ENDPARTIES");
                    EndParties(Int64.Parse(newIdVersionBases));
                }
                else if (statusParties.Equals("PARTIES"))
                {
                    Console.WriteLine("Пришел пакет с запросом  PARTIES");
                    Parties();
                }

            }
        }

        private void BeginParties()
        {
            syncCreateAllList._viewModelMain.pg = 15;

            Console.WriteLine("ObserverNetworkUpdate: -> SYNCREATEALLLIST : BEGINPARTIES");
            //syncCreateAllList._controlMq.DeleteMqQueue("BEGINPARTIES");
            //sqllite полностью удаляет все в таблице listFiles;
            syncCreateAllList.sqlliteController.getInsert().insertListFilesTempIndex(0);
            syncCreateAllList._updateDataController.removeAllData();
            syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
            clearAutoScanInfo(ref _autoScanInfoModel);
            syncCreateAllList._viewModelMain.SetNewIconTaskBar(syncCreateAllList.convertIcon.streamToIcon(".notifyIconBarRefresh"));
        }

        private void EndParties(long newVersionBasesServer)
        {
            List<FileInfoModel> listFiles = syncCreateAllList.responceServer.listFileInfo.ToList<FileInfoModel>();
            syncCreateAllList._viewModelMain.pg = 45;

            if (listFiles != null)
            {

                Console.WriteLine("ObservableNetworkUpdates->updateNetworkHandler->ENDPARTIES" + "SYNCREATEALLLIST : EndParties");
                //syncCreateAllList._controlMq.DeleteMqQueue("ENDPARTIES");


                syncCreateAllList._viewModelMain.pg = 55;
                saveCountLastArr(listFiles, ref _autoScanInfoModel);
                InsertSqlLite(listFiles);

                syncCreateAllList._viewModelMain.pg = 75;
                syncCreateAllList._viewModelMain.ActiveNodeID[0] = -1;

                //Console.WriteLine("Имя выполняемого потока: "+Thread.CurrentThread.Name);
                //полная перезапись дерева и listview
                //полная перезапись не подразумевает удаление всех узлов
                //просто обновление/удаление/вставка
                syncCreateAllList._mainUpdateForm.UpdateMainReloadALLTreeView();
                syncCreateAllList._mainUpdateForm.UpdateMainReloadALLListView();

                ObservableCollection<FolderNodes> folder = syncCreateAllList._viewModelMain.NodesView.Items;

                for (int d = 0; d < folder.Count; d++)
                {
                    if (folder[d].Row_id == -1)
                    {
                        folder[d].IsSelected = true;
                        folder[d].IsExpanded = true;
                    }
                }

                //syncCreateAllList._viewModelMain.pg = 85;
                //syncCreateAllList._viewModelMain.pg = 100;

                rebuildingSizeSql();
              

            }

            syncCreateAllList._updateDataController.updateVersionBasesSqlliteFileInfo(newVersionBasesServer);
            //очистка
            listFiles = null;
            copyTempToPcSave(syncCreateAllList.sqlliteController);
            existSavePc();
           
            saveAutoScanInfo(ref _autoScanInfoModel, syncCreateAllList.sqlliteController);
            showBallonTaskBar(ref _autoScanInfoModel, syncCreateAllList._viewModelMain);
            _updateSendController.updateSendJsonCHECKWORKINGSERVER();
        }

        private void saveCountLastArr(List<FileInfoModel> arrModel, ref AutoScanInfoModel autoScanInfoModel)
        {
            foreach(FileInfoModel item in arrModel)
            {
                if (item != null)
                {
                    FileInfoModel m = item;
                    addAutoScanInfo(ref m, ref autoScanInfoModel);
                }
            }
        }

        private void addAutoScanInfo(ref FileInfoModel m, ref AutoScanInfoModel autoScanInfoModel)
        {
            if (m != null)
            {
                if (staticVariable.Variable.isFolder(m.type))
                {
                    autoScanInfoModel.folder = autoScanInfoModel.folder + 1;
                }
                else
                {
                    autoScanInfoModel.files = autoScanInfoModel.files + 1;
                }
            }

        }

        private void showBallonTaskBar(ref AutoScanInfoModel autoScanInfoModel, MainWindowViewModel _viewModelMain)
        {
            string title = "Синхронизация";
            string text = "Завершена в " + autoScanInfoModel.endTimeScan;
            string dataStr = autoScanInfoModel.endTimeScan.ToString();

            if (!dataStr.Equals("01.01.0001 0:00:00"))
            {
                _viewModelMain.ShowBallonTaskBar(title, text);
            }

        }

        private void saveAutoScanInfo(ref AutoScanInfoModel autoScanInfoModel, SqliteController _sqlliteController)
        {

            autoScanInfoModel.endTimeScan = DateTime.Now;
            DateTime a = DateTime.Now;
            autoScanInfoModel.lastDataScan = a.ToString();
            TimeSpan interval = autoScanInfoModel.endTimeScan - autoScanInfoModel.beginTimeScan;
            autoScanInfoModel.timeSpent = interval.ToString();
            autoScanInfoModel.allCount = staticVariable.Variable.allCountAutoScanInfo;

            _sqlliteController.getInsert().insertAutoScanInfo(autoScanInfoModel);
        }

        private void clearAutoScanInfo(ref AutoScanInfoModel autoScanInfoModel)
        {
            autoScanInfoModel.folder = 0;
            autoScanInfoModel.files = 0;
            autoScanInfoModel.allCount = 0;
            autoScanInfoModel.beginTimeScan = DateTime.Now;
           
        }
        private void rebuildingSizeSql()
        {
            Task.Run(() =>
            {
                InfoSqlSize infoSql = new InfoSqlSize(syncCreateAllList.sqlliteController);
                App.Current.Dispatcher.Invoke(new System.Action(() => syncCreateAllList._viewModelMain.PgSizeDisk = infoSql.getSize()));
            });
        }

        private void copyTempToPcSave(SqliteController sqllitecontroller)
        {
         
            sqllitecontroller.getSelect().copyTempInSavePc();
            sqllitecontroller.clearTablesPcTemp();
        }
        //проверка какие файлы сох. на компьютере
        public void existSavePc()
        {
            ExistSavePcEvent existSavePc = new ExistSavePcEvent(syncCreateAllList.convertIcon, syncCreateAllList.sqlliteController);
            existSavePc.rebuildPcSaveTable();
            existSavePc.existStart(syncCreateAllList._viewModelMain.NodesView.Items, syncCreateAllList._viewModelViewFolder.NodesView.Items);
        }

        private void Parties()
        {
            Console.WriteLine("ObserverNetworkUpdate: -> SYNCREATEALLLIST : PArties");
           // syncCreateAllList._controlMq.DeleteMqQueue("PARTIES");
            syncCreateAllList._viewModelMain.pg = 23;
            List<FileInfoModel> listFiles = syncCreateAllList.responceServer.listFileInfo.ToList<FileInfoModel>();

            if (listFiles != null)
            {
                saveCountLastArr(listFiles, ref _autoScanInfoModel);
                syncCreateAllList._updateDataController.insertSqlliteFileInfo(listFiles, syncCreateAllList._viewModelMain);
            }
            //очистка
            listFiles = null;
            syncCreateAllList._viewModelMain.pg = 7;
        }

        private void InsertSqlLite(List<FileInfoModel> listFiles)
        {
            syncCreateAllList._updateDataController.insertSqlliteFileInfo(listFiles, syncCreateAllList._viewModelMain);
        }
    }
}
