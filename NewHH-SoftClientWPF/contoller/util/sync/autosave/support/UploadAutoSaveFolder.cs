﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave.support
{
    public class UploadAutoSaveFolder: IUploadAutoSaveFolder
    {
        private ExceptionController _excepc;
        private ButtonUploadClick _uploadClick;
        private CreateFolderController _cfc;
        private SupportCFData _cfData;


        public UploadAutoSaveFolder(SupportCFData cfData , ExceptionController excepc , ButtonUploadClick uploadClick , CreateFolderController cfc)
        {
            _excepc = excepc;
            _uploadClick = uploadClick;
            _cfc = cfc;
            _cfData = cfData;
        }
        public void UploadFolder(List<ScanFolderModel> listPath)
        {
            if (listPath != null & listPath.Count > 0)
            {
                try
                {
                    string[] selectHref = ConvertScfToPath(listPath);
                    FileInfoModel pasteModel = GetPasteModel(UtilMethod.getRootLocationAutoSaveFolder() + "/");
                    CheckParam(listPath , selectHref, pasteModel);
                }
                catch (System.IO.DirectoryNotFoundException ex)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "AutoSaveFolderController->UploadFolder: Критическая ошибка загрузки файлов ", ex.ToString());
                }


            }
        }

        private void CheckParam(List<ScanFolderModel> listPath , string[] selectHref, FileInfoModel pasteModel)
        {
            if (pasteModel != null)
            {
                List<ScanFolderModel> listPathFinal =  GetFolderChildren(listPath,  pasteModel.location);
                SetLocationToAutoSaveTable(listPathFinal);
                SubsctibleAddData(_cfc);
                CreateListPath(listPathFinal, pasteModel).Wait() ;
                if(LocationGetModel(listPathFinal)) Start(selectHref , listPathFinal);
            }
            else
            {
                generatedError((int)CodeError.NETWORK_ERROR, "AutoSaveFolderController->UploadFolder: Критическая ошибка загрузки файлов ", "Сервер не смог вернуть ответ в какую папку нужно отправлять");
            }
        }

        private void SubsctibleAddData(CreateFolderController _cfc)
        {
            if(!_cfc.isSubscribleData)
            {
                _cfc.isSubscribleData = true;
                _cfc.eventAddData += AddCreateFolderItem;
            }
            
        }

        public void AddCreateFolderItem(object sender, CFModel data)
        {
            if(data.arr != null)
            {
                _cfData.insertCreateFolderData(data.arr);
            }
           
        }

        private void SetLocationToAutoSaveTable(List<ScanFolderModel> listPathFinal)
        {
            foreach(ScanFolderModel model in listPathFinal)
            {
                _cfData.updateScanFolderLocation(model.location, model.row_id);
            }
           
            
        }
        private void Start(string[] selectHref , List<ScanFolderModel> listPathFinal)
        {
            StartAll(selectHref, listPathFinal);
        }

        private void StartAll(string[] selectHref, List<ScanFolderModel> listPathFinal)
        {

            foreach(ScanFolderModel model in listPathFinal)
            {
                FileInfoModel pasteModel = GetPasteModel(model.location);
                //List<FileInfoModel> parentListPasteModel = GetChildrenPasteModel(pasteModel);
                string[] path = { model.fullPath };
                StartFolderUpload(path, ref pasteModel);
            }

            

        }

        private void StartFolderUpload(string[] selectHref , ref FileInfoModel pasteModel)
        {
            _uploadClick.startTrainingUploadScanFolder(selectHref , pasteModel);
        }

        //Дождаться ответа от сервера
        private FileInfoModel GetPasteModel(string location)
        {
            FileInfoModel fim = null;
            return Wait(ref fim, ref location); 
        }

        private bool LocationGetModel(List<ScanFolderModel> listPathFinal)
        {
            FileInfoModel fim = null;
            bool isCreate = false;
            foreach(ScanFolderModel model in listPathFinal)
            {
                string location = model.location;
                FileInfoModel fimServer = Wait(ref fim, ref location);
                if (fimServer != null) isCreate = true;
            }

            return isCreate;
        }


        private FileInfoModel Wait(ref FileInfoModel fim , ref string location)
        {
            for (int i = 0; i < 5; i++)
            {
                fim = GetLocationFIM(ref fim, ref location);
                if (fim != null) break;
                WaitResponce();
            }

            return fim;
        }

        public List<FileInfoModel> GetChildrenPasteModel(FileInfoModel fim)
        {
            return _cfData.getChildrenAutoSaveModel(fim.row_id);
        }

        private FileInfoModel GetLocationFIM(ref FileInfoModel fim , ref string location)
        {
            fim = _cfData.getRootAutoSaveModel(location);
            Console.WriteLine("AutoSaveFolderController->GetLocationFIM->Ожидаем создания папки на сервере.....");
            return fim;
        }

        private void WaitResponce()
        {
            Thread.Sleep(1000);
        }
        private string[] ConvertScfToPath(List<ScanFolderModel> listPath)
        {
            string[] arr = new string[listPath.Count];

            for (int i = 0; i < listPath.Count; i++)
            {
                arr[i] = listPath[i].fullPath;
            }

            return arr;
        }

        private  async Task CreateListPath(List<ScanFolderModel> listPath, FileInfoModel pasteModel)
        {
            foreach (ScanFolderModel model in listPath)
            {
                await _cfc.TrainingCreateFolder(pasteModel , model.name);
            }

        }

     
        //формируем location на сервере и его имя
        private List<ScanFolderModel> GetFolderChildren(List<ScanFolderModel> listPath, string myPcRootFolder)
        {
            foreach (ScanFolderModel model in listPath)
            {
                string name = GetNamePath(model.fullPath);
                model.location = Variable.EscapeUrlString(myPcRootFolder + name + "/");
                model.name = name;
            }

            return listPath;
        }

        private string GetNamePath(string fullPath)
        {
            if (Directory.Exists(fullPath))
            {
                DirectoryInfo di = new DirectoryInfo(fullPath);
                return di.Name;
            }
            else
            {
                return "";
            }

        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _excepc.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }
    }
}
