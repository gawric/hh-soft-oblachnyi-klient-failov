﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.sync.autosave.support;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave
{
    public class AutoSaveFolderController : IAutoSaveFolder
    {
        private ButtonUploadClick uploadClick;
        private InitAutoSaveFolder _init;
        private ExceptionController _excepc;
        private IUploadAutoSaveFolder _uasf;
        private SupportCFData _scfData;
        public AutoSaveFolderController(SystemSettingModelObj _ssmo)
        {
            this.uploadClick = _ssmo._uploadClick;
            _init = new InitAutoSaveFolder(_ssmo._wdce, _ssmo._cfc, _excepc , _ssmo._fsfc);
            _scfData = new SupportCFData(_ssmo._sqliteController);
            this._uasf = new UploadAutoSaveFolder(_scfData, _excepc, uploadClick , _ssmo._cfc);
            this._excepc = _ssmo._excepc;
            
        }

       
        public void CreateFolder()
        {

        }

        public void IconOkAutoSaveFolder(List<FileInfoModel> listFim)
        {
            throw new NotImplementedException();
        }

        public async void StartUploadFolder(List<ScanFolderModel> listPath, List<FileInfoModel> parentListPasteModel)
        {
            try
            {
                _init.InitUpload(_scfData).Wait();
                _uasf.UploadFolder(listPath);
            }
            catch(System.ArgumentOutOfRangeException s)
            {
                Debug.Print("Ошибка AutoSaveFolderController->StartUploadFolder: "+s.ToString());
            }
            catch(System.AggregateException s)
            {
                Debug.Print("Ошибка AutoSaveFolderController->StartUploadFolder: " + s.ToString());
            }
         
        }

       

        
       

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _excepc.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }


    }
}
