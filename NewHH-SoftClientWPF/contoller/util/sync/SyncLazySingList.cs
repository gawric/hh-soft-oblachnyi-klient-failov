﻿using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sync
{
    class SyncLazySingList
    {

        public  SyncSinglListModel syncCreateAllList;
        private UpdateNetworkJsonController _updateSendController;
        private AutoScanInfoModel autoScanInfoModel;
        //private object responceServer;

        public SyncLazySingList(SyncSinglListModel syncCreateAllList, UpdateNetworkJsonController _updateSendController , ref AutoScanInfoModel autoScanInfoModel)
        {
            this.syncCreateAllList = syncCreateAllList;
            this._updateSendController = _updateSendController;
            this.autoScanInfoModel = autoScanInfoModel;
        }
        //responceServer - не используйте ListFiles (он всегда передается не актуальный) используйте массив переданный в аргументах!
        public void SyncLazySinglTraining(SaveJsonToDiskContoller _saveJsonToDisk , ReadJsonToDiskController _readJsonToDisk , FileInfoModel[] arrModel)
        {
            if (syncCreateAllList.responceServer.textmessage.Length >= 5)
            {
                string statusDeleteParties = syncCreateAllList.responceServer.textmessage[4];

               
                if (statusDeleteParties.Equals("LAZYSINGLBEGINPARTIES"))
                {

                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                    clearAutoScanInfo(ref autoScanInfoModel);
                    syncCreateAllList._viewModelMain.pg = 10;
                    Console.WriteLine("SyncLazySingList-> SyncLazySinglTraining*****<----Пришел запрос от сервера SINGLBEGINUPDATEPARTIES");
                    syncCreateAllList._viewModelMain.SetNewIconTaskBar(syncCreateAllList._convertIcon.streamToIcon(".notifyIconBarRefresh"));
                }
                else if (statusDeleteParties.Equals("LAZYSINGLUPDATEPARTIES"))
                {

                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                
                    syncCreateAllList._viewModelMain.pg = 10;
  
                    SinglLazySaveToDisk( _saveJsonToDisk, arrModel.ToList());

                    syncCreateAllList._viewModelMain.pg = 60;
                    syncCreateAllList._viewModelMain.pg = 90;

                    Console.WriteLine("SyncLazySingList-> SyncLazySinglTraining*****<----Пришел запрос от сервера LAZYSINGLUPDATEPARTIES и Мы сохронили его на диск");

                }
                else if (statusDeleteParties.Equals("LAZYSINGLENDUPDATEPARTIES"))
                {
                    List<string> _tempHref = _saveJsonToDisk.getListGetLazyTempHref();

                    saveAll(_tempHref,  _readJsonToDisk , arrModel , ref autoScanInfoModel);

                    ClearHref(_tempHref, _saveJsonToDisk);



                    Console.WriteLine("SyncLazySingList-> SyncLazySinglTraining*****<----LAZYSINGLENDUPDATEPARTIES *****Завершенно обновлени!!!*****");
               

                    _updateSendController.updateSendJsonCHECKWORKINGSERVER();
                    rebuildingSizeSql();
                    
                    saveAutoScanInfo(ref autoScanInfoModel , syncCreateAllList._sqlliteController);
                    showBallonTaskBar(ref autoScanInfoModel, syncCreateAllList._viewModelMain);
                }

             
            }


        }
        private void clearAutoScanInfo(ref AutoScanInfoModel autoScanInfoModel)
        {
            autoScanInfoModel.folder = 0;
            autoScanInfoModel.files = 0;
            autoScanInfoModel.allCount = 0;
            autoScanInfoModel.beginTimeScan = DateTime.Now;
          
        }
        private void showBallonTaskBar(ref AutoScanInfoModel autoScanInfoModel, MainWindowViewModel _viewModelMain)
        {
            string title = "Синхронизация";
            string text = "Завершена в " + autoScanInfoModel.endTimeScan;
            string dataStr = autoScanInfoModel.endTimeScan.ToString();

            if (!dataStr.Equals("01.01.0001 0:00:00"))
            {
                _viewModelMain.ShowBallonTaskBar(title, text);
            }
            
        }
        private void saveAutoScanInfo(ref AutoScanInfoModel autoScanInfoModel , SqliteController _sqlliteController)
        {
            
            autoScanInfoModel.endTimeScan = DateTime.Now;
            DateTime a = DateTime.Now;
            autoScanInfoModel.lastDataScan = a.ToString();
            TimeSpan interval =  autoScanInfoModel.endTimeScan - autoScanInfoModel.beginTimeScan;
            autoScanInfoModel.timeSpent = interval.ToString();
            autoScanInfoModel.allCount = staticVariable.Variable.allCountAutoScanInfo;
            
            _sqlliteController.getInsert().insertAutoScanInfo(autoScanInfoModel);
        }
        private void rebuildingSizeSql()
        {
            Task.Run(() =>
            {
                InfoSqlSize infoSql = new InfoSqlSize(syncCreateAllList._sqlliteController);
                App.Current.Dispatcher.Invoke(new System.Action(() => syncCreateAllList._viewModelMain.PgSizeDisk = infoSql.getSize()));
            });
        }

        private void ClearHref(List<string> _tempHref, SaveJsonToDiskContoller _saveJsonToDisk)
        {
            _tempHref.Clear();
            _saveJsonToDisk.setListGetLazyTempHref(_tempHref);
        }

        private void saveAll(List<string> _tempHref , ReadJsonToDiskController _readJsonToDisk , FileInfoModel[] arrModel , ref AutoScanInfoModel autoScanInfoModel)
        {
            //достаем из временного хранилиша и сохроняем в базу
            readToDiskAndSave(_tempHref, _readJsonToDisk , autoScanInfoModel);

            //сохроняем последние данные в базу
            SinglENDParties(arrModel , ref autoScanInfoModel);


        }

        //Обновляет базу не стирая все старые данные
        private void SinglENDParties(FileInfoModel[] arrModel , ref AutoScanInfoModel autoScanInfoModel)
        {
            syncCreateAllList._viewModelMain.pg = 45;

            if (arrModel != null)
            {
                saveCountLastArr(arrModel, ref autoScanInfoModel);
                List<FileInfoModel> listFiles = arrModel.ToList();

                syncCreateAllList._viewModelMain.pg = 55;

                //sqllite обновляем не удаляя базу целиком только отедльные узлы
                syncCreateAllList._updateDataController.UpdateSinglSqliteData(listFiles, syncCreateAllList._viewModelMain);

                syncCreateAllList._viewModelMain.pg = 65;


                syncCreateAllList._sqlliteController.checkActiveTransaction();
                //обновляем дерево (не полностью) только открытые ноды в дереве
                //или полностью если дерево не имеет нодов
                syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
                syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);
                syncCreateAllList._viewModelMain.pg = 93;
                listFiles = null;
            }


        }

        private void saveCountLastArr(FileInfoModel[] arrModel , ref AutoScanInfoModel autoScanInfoModel)
        {
            foreach(FileInfoModel item in arrModel)
            {
                if(item != null)
                {
                    FileInfoModel m = item;
                    addAutoScanInfo(ref m, ref autoScanInfoModel);
                }
            }
        }
        private void SinglLazySaveToDisk(SaveJsonToDiskContoller _saveJsonToDisk , List<FileInfoModel> listFileInfoModel)
        {
            syncCreateAllList._viewModelMain.pg = 25;
            
            FileInfoModel[] convertArray = listFileInfoModel.ToArray();

            syncCreateAllList._viewModelMain.pg = 55;

            //обновляем дерево (не полностью) только открытые ноды в дереве
            //или полностью если дерево не имеет нодов
            // syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFileInfoModel);
            // syncCreateAllList._mainUpdateForm.UpdateMainListView(listFileInfoModel);

            if(checkEmptyArr(convertArray))
                 _saveJsonToDisk.saveGetLazyJsonArray(convertArray);

            syncCreateAllList._viewModelMain.pg = 90;
            listFileInfoModel = null;
        }
        //Обновляет базу не стирая все старые данные
        private void SinglParties(ServerTopicModel responceServer)
        {
            syncCreateAllList._viewModelMain.pg = 25;
            List<FileInfoModel> listFiles = responceServer.listFileInfo.ToList();

            //sqllite обновляем не удаляя базу целиком только отедльные узлы
            syncCreateAllList._updateDataController.UpdateSinglSqliteData(listFiles, syncCreateAllList._viewModelMain);

            //обновляем дерево (не полностью) только открытые ноды в дереве
            //или полностью если дерево не имеет нодов
            syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
            syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);
            syncCreateAllList._viewModelMain.pg = 33;
            listFiles = null;
        }

        //Обновляет базу не стирая все старые данные
        private void SingLazyParties(FileInfoModel[] objTosave)
        {
            syncCreateAllList._viewModelMain.pg = 25;
            List<FileInfoModel> listFiles = objTosave.ToList();

            //sqllite обновляем не удаляя базу целиком только отедльные узлы
            syncCreateAllList._updateDataController.UpdateSinglSqliteData(listFiles, syncCreateAllList._viewModelMain);


   

            //обновляем дерево (не полностью) только открытые ноды в дереве
            //или полностью если дерево не имеет нодов
            syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
            syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);
            syncCreateAllList._viewModelMain.pg = 90;
            listFiles = null;
        }
        private void UpdateVersionBases(SqliteController sqliteController , ref long newversionbasesserver)
        {
            if(newversionbasesserver != 0)
            {
                sqliteController.updateVeersionBases(newversionbasesserver);
            }
        }
       
        long newversionbasesserver = 0;
        void readToDiskAndSave(List<string> tempHref , ReadJsonToDiskController _readJsonToDisk , AutoScanInfoModel autoScanInfoModel)
        {
            try
            {
                int[] count = { 0 };
    
                if (tempHref != null)
                {
                    FileInfoModel[] newArr = new FileInfoModel[4000];

                    for (int f = 0; f < tempHref.Count; f++)
                    {

                        FileInfoModel[] fileInfoArray = _readJsonToDisk.readLazyJsonArray(tempHref[f]);
                        convertFileInfoArrToNewArr(fileInfoArray, newArr, count , ref autoScanInfoModel ,ref  newversionbasesserver);
                        
                        saveToSqlLite(newArr, count);
                        _readJsonToDisk.deleteTempFiles(tempHref[f]);
                        fileInfoArray = null;

                        //Console.WriteLine("======Записали данные в нашем клиенте последняя инстанция====== "+f);
                    
                    }

                   
                    //проверку на пустой массив
                    //LAZYEND - приходит пустым, хотя там должны быть остатки для бд
                    SingLazyParties(newArr);
                    UpdateVersionBases(syncCreateAllList._sqlliteController, ref newversionbasesserver);

                    count[0] = 0;
                    newArr = null;


                }

              


            }
            catch (Newtonsoft.Json.JsonReaderException z)
            {
                Console.WriteLine("SyncLazySingList -> readToDiskAndSend: " + " критическая ошибка чтения файлов с диска");
            }
            

        }

        private void clearArr(FileInfoModel[] newArr)
        {
            for (int h = 0; h < newArr.Length; h++)
            {
                newArr[h] = null;
            }
        }

        private void convertFileInfoArrToNewArr(FileInfoModel[] fileInfoArray , FileInfoModel[] newArr , int[] count , ref AutoScanInfoModel autoScanInfoModel , ref long newversionbases)
        {
            for (int z = 0; z < fileInfoArray.Length; z++)
            {
                FileInfoModel m = fileInfoArray[z];

                addNewVersionBases(m, ref newversionbases);
                addAutoScanInfo(ref m , ref autoScanInfoModel);
                newArr[count[0]] = m;
                count[0] = count[0] + 1;
            }
        }

        private void addNewVersionBases(FileInfoModel m , ref long newversionbases)
        {
            if (m != null)
            {
                newversionbases = m.versionUpdateBases;
            }

        }
        private void addAutoScanInfo(ref FileInfoModel m , ref AutoScanInfoModel autoScanInfoModel)
        {
            if(m != null)
            {
                if (staticVariable.Variable.isFolder(m.type))
                {
                    autoScanInfoModel.folder = autoScanInfoModel.folder + 1;
                }
                else
                {
                    autoScanInfoModel.files = autoScanInfoModel.files + 1;
                }
            }
            
        }
        private void saveToSqlLite(FileInfoModel[] newArr  , int[] count)
        {
            if (count[0] >= staticVariable.Variable.countLazyParties)
            {
                SingLazyParties(newArr);
                clearArr(newArr);

                count[0] = 0;
            }

        }

        //что в массиве вообще что-то есть
        private bool checkEmptyArr(FileInfoModel[] fileInfoModelArray)
        {
            bool check = false;
            for (int h = 0; h < fileInfoModelArray.Length; h++)
            {
                if (fileInfoModelArray[h] != null)
                {
                    check = true;
                    break;
                }
                    
            }
            return check;
        }


    }
}
