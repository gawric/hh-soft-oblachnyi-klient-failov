﻿using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.progress
{
    class ProgressToCopy
    {
        private CopyMainWindowViewModel _copyMainWIndowsModel;
        private UpdateFormCopyWindowsController _copyForm;
        private CopyMainWindow _copyWindows;
        private CopyViewModel _copyViewModel;

        public ProgressToCopy(CopyMainWindowViewModel _copyMainWIndowsModel , UpdateFormCopyWindowsController _copyForm , CopyMainWindow _copyWindows , CopyViewModel _copyViewModel)
        {
            this._copyMainWIndowsModel = _copyMainWIndowsModel;
            this._copyForm = _copyForm;
            this._copyWindows = _copyWindows;
            this._copyViewModel = _copyViewModel;
           
        }

        //Отображает Progress для копирования
        public Progress<CopyViewModel> updateProgressToCopy()
        {
            Progress<CopyViewModel> progress = new Progress<CopyViewModel>();

            progress.ProgressChanged += (s, e) =>
            {
                CopyViewModel statusModel = (CopyViewModel)e;

                if (statusModel.StatusWorker.Equals("init"))
                {
                    _copyMainWIndowsModel.pg = 10;
                }
                else if (statusModel.StatusWorker.IndexOf("Поиск файлов") != -1)
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 35;
                }
                else if (statusModel.StatusWorker.Equals("Создание новых файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 42;
                }
                else if (statusModel.StatusWorker.Equals("Отправка файлов на сервер"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 55.2;
                }
                else if (statusModel.StatusWorker.Equals("Копирование файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 60;
                }
                else if (statusModel.StatusWorker.Equals("Поиск временных файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 75;
                }
                else if (statusModel.StatusWorker.Equals("Синхронизация"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 97;
                }
                else if (statusModel.StatusWorker.Equals("Копирование завершенно"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 95;
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }
                else if (statusModel.StatusWorker.Equals("Ошибка вставки файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 100;
                }
                else if (statusModel.StatusWorker.Equals("Выполнено!"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 100;
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }


            };

            return progress;
        }
        private void closeWindowsCopy(CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {

            _copyForm.updateCloseWindowsCopy();
        }
    }


}
