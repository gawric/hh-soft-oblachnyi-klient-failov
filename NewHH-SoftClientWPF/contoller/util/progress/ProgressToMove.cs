﻿using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.progress
{
    class ProgressToMove
    {
        private CopyMainWindowViewModel _copyMainWIndowsModel;
        private UpdateFormCopyWindowsController _copyForm;
        private CopyMainWindow _copyWindows;
        private CopyViewModel _copyViewModel;

        public ProgressToMove(CopyMainWindowViewModel _copyMainWIndowsModel , UpdateFormCopyWindowsController _copyForm, CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {
            this._copyMainWIndowsModel = _copyMainWIndowsModel;
            this._copyForm = _copyForm;
            this._copyWindows = _copyWindows;
            this._copyViewModel = _copyViewModel;
        }
        //Отображает Progress для копирования
        public Progress<CopyViewModel> updateProgressToMove()
        {
            Progress<CopyViewModel> progress = new Progress<CopyViewModel>();

            progress.ProgressChanged += (s, e) =>
            {
                CopyViewModel statusModel = (CopyViewModel)e;

                if (statusModel.StatusWorker.Equals("init"))
                {

                    _copyMainWIndowsModel.pg = 10;


                }
                else if (statusModel.StatusWorker.IndexOf("Поиск файлов") != -1)
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 35;
                }
                else if (statusModel.StatusWorker.Equals("Создание новых файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 42;
                }
                else if (statusModel.StatusWorker.Equals("Отправка файлов на сервер"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 55.2;
                }
                else if (statusModel.StatusWorker.Equals("Переносим файлы"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 60;
                }
                else if (statusModel.StatusWorker.Equals("Перенос завершен"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 80;

                }
                else if (statusModel.StatusWorker.IndexOf("Поиск временных файлов") != -1)
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 85;
                }
                else if (statusModel.StatusWorker.IndexOf("Ошибка синхронизации файлов") != -1)
                {
                    _copyMainWIndowsModel.status = "Синхронизация не закончена, закройте окно и дождитесь синхронизации";
                    _copyMainWIndowsModel.pg = 100;
                }
                else if (statusModel.StatusWorker.Equals("Очистка старых файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 90;
                    // closeWindowsCopy(_copyWindows, _copyViewModel);
                }
                else if (statusModel.StatusWorker.Equals("Очищение файлов в папке назначения"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 47;
                }
                else if (statusModel.StatusWorker.Equals("Синхронизация"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 95;
                }
                else if (statusModel.StatusWorker.Equals("Выполнено!"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 100;
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }
                else if (statusModel.StatusWorker.Equals("Ошибка вставки файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 100;
                }


            };

            return progress;
        }

        private void closeWindowsCopy(CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {

            _copyForm.updateCloseWindowsCopy();
        }
    }
}
