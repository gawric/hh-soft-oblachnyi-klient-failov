﻿using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.progress
{
    class ProgressToDelete
    {
        private CopyMainWindowViewModel _copyMainWIndowsModel;
        private UpdateFormCopyWindowsController _copyForm;
        private CopyMainWindow _copyWindows;
        private CopyViewModel _copyViewModel;

        public ProgressToDelete(CopyMainWindowViewModel _copyMainWIndowsModel, UpdateFormCopyWindowsController _copyForm, CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {
            this._copyMainWIndowsModel = _copyMainWIndowsModel;
            this._copyForm = _copyForm;
            this._copyWindows = _copyWindows;
            this._copyViewModel = _copyViewModel;
        }

        //Отображает Progress для удаления
        public Progress<CopyViewModel> updateProgressToDelete()
        {
            Progress<CopyViewModel> progress = new Progress<CopyViewModel>();

            progress.ProgressChanged += (s, e) =>
            {
                CopyViewModel statusModel = (CopyViewModel)e;

                if (statusModel.StatusWorker.Equals("init"))
                {

                    _copyMainWIndowsModel.pg = 10;


                }
                else if (statusModel.StatusWorker.IndexOf("Поиск файлов") != -1)
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 35;
                }
                else if (statusModel.StatusWorker.Equals("Добавление во временное хранилище"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 55;
                }
                else if (statusModel.StatusWorker.Equals("Отправка на удаление"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 65;
                }
                else if (statusModel.StatusWorker.Equals("Удаление"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 85;

                }
                else if (statusModel.StatusWorker.Equals("Синхронизация"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 97;
                }
                else if (statusModel.StatusWorker.IndexOf("Ошибка синхронизации файлов") != -1)
                {
                    _copyMainWIndowsModel.status = "синхронизация не закончена, закройте окно и дождитесь синхронизации";
                    _copyMainWIndowsModel.pg = 100;
                }
                else if (statusModel.StatusWorker.Equals("Ошибка вставки файлов"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 100;
                }
                else if (statusModel.StatusWorker.Equals("Выполнено!"))
                {
                    _copyMainWIndowsModel.status = statusModel.StatusWorker;
                    _copyMainWIndowsModel.pg = 100;
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }


            };

            return progress;
        }

        private void closeWindowsCopy(CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {

            _copyForm.updateCloseWindowsCopy();
        }

    }
}
