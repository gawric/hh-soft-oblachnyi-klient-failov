﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updatedata
{
    public class UpdateDataController
    {
       private UpdateSqliteData _updateSqlitedata;
       private UpdateTreeViewNodes _updateTreeViewNodes;
       private  SqliteController _sqliteController;
       private MainWindowViewModel _viewModel;
        private ConvertIconType convertIcon;

        public UpdateDataController(Container cont)
        {
            _sqliteController = cont.GetInstance<SqliteController>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            convertIcon = cont.GetInstance<ConvertIconType>();
            CreateObject();
        }

        public void CreateObject()
        {
            if(_updateSqlitedata == null)
            {
                _updateSqlitedata = new UpdateSqliteData(_sqliteController);
            }

            if(_updateTreeViewNodes == null)
            {
                _updateTreeViewNodes = new UpdateTreeViewNodes(_sqliteController , convertIcon);
            }
        }


       

        //Обновляет без удаления базы
        public void UpdateSingl(List<FileInfoModel> listFiles, MainWindowViewModel _viewModelMain)
        {
            _updateSqlitedata.UpdateSingl(listFiles, _viewModelMain);
        }

        //Обновляем когда приходят новые данные с сервера не удаляя старую базу
        //Обновляем только sql
        public void UpdateSinglSqliteData(List<FileInfoModel> listFiles, MainWindowViewModel _viewModelMain)
        {
            _updateSqlitedata.UpdateSingl(listFiles, _viewModelMain);
        }

        public void clearSqlliteData(List<FileInfoModel> listFiles , MainWindowViewModel _viewModelMain)
        {
            _updateSqlitedata.clearSqlliteAndAddFileInfo(listFiles ,  _viewModelMain);
        }

        public void removeAllData()
        {
            _updateSqlitedata.removeSqlliteBases();
        }

        public void insertSqlliteFileInfo(List<FileInfoModel> listFiles , MainWindowViewModel _viewModelMain)
        {
            _updateSqlitedata.insertSqlliteFileInfo(listFiles ,  _viewModelMain);
        }

        public void updateVersionBasesSqlliteFileInfo(long newVerSionServer)
        {
            _updateSqlitedata.updateVerSqlliteFileInfo(newVerSionServer);
        }


    }
}
