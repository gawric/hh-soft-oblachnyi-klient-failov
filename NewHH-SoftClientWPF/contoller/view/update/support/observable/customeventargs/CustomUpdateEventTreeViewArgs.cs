﻿using NewHH_SoftClientWPF.contoller.sorted;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
    public class CustomUpdateEventTreeViewArgs : EventArgs
    {
        private string nameForm;
        private string[] textMessage;
        private SortableObservableCollection<FolderNodes> nodes;
        private TreeViewItem tvi;

        //0 - name Form
        //1 - name Text
        public CustomUpdateEventTreeViewArgs(string nameForm, string[] textMessage , SortableObservableCollection<FolderNodes> nodes , TreeViewItem tvi)
        {
            this.nameForm = nameForm;
            this.textMessage = textMessage;
            this.nodes = nodes;
            this.tvi = tvi;
        }

        public TreeViewItem getTvi
        {
            get { return tvi; }
        }

        public string getNameForm
        {
            get { return nameForm; }
        }
        public string[] getTextMessage
        {
            get { return textMessage; }
        }
        public SortableObservableCollection<FolderNodes> getNodesTreeView
        {
            get { return nodes; }
        }
    }
}

