﻿using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs
{
    class CustomViewTransferArgs : EventArgs
    {
        public FolderNodesTransfer nodes;
        public string nameNodes;
      
        public CustomViewTransferArgs(FolderNodesTransfer nodesTransfer , string nameNodes)
        {
            this.nodes = nodesTransfer;
            this.nameNodes = nameNodes;
        }

        public FolderNodesTransfer getNodesTransfer
        {
            get { return nodes; }
        }
        public string getNameNodes
        {
            get { return nameNodes; }
        }
    }
}
