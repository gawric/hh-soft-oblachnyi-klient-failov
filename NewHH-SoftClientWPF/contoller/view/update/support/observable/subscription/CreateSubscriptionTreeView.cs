﻿using NewHH_SoftClientWPF.contoller.sorted;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.support
{
    public class CreateSubscriptionTreeView
    {

        public event EventHandler OnSaved;

        public void updateTreeView(string nameForm, string[] textMessage, SortableObservableCollection<FolderNodes> nodes , TreeViewItem tvi)
        {

            CustomUpdateEventTreeViewArgs custom = new CustomUpdateEventTreeViewArgs(nameForm, textMessage, nodes , tvi);
            OnSaved?.Invoke(this, custom);
        }
    }
}
