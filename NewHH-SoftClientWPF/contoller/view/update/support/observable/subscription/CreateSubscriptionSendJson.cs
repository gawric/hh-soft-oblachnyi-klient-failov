﻿using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.subscription
{
    //класс прокладка для отправки данных на обработку ObserverNetworkSendJson
    public class CreateSubscriptionSendJson
    {
        private string sendJson;
        private string[] textMessage;
        private FileInfoModel[] arr;

        public event EventHandler OnSaved;

        public void updateNetworkJson(string sendJson, string[] textMessage, FileInfoModel[] arr)
        {
            try
            {
                CustomSendJsonEventArgs custom = new CustomSendJsonEventArgs(sendJson, textMessage, arr);
                OnSaved?.Invoke(this, custom);
            }
            catch(System.InvalidCastException s)
            {
                Console.WriteLine(s.ToString());
            }
           
        }

    }
}
