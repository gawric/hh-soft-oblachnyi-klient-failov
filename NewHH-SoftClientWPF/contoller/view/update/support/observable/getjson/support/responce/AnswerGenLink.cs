﻿using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.getJson.support
{
    public class AnswerGenLink
    {
        private ServerTopicModel responceServer;
        private UpdateNetworkJsonController updateSendController;
        public AnswerGenLink(ref ServerTopicModel responceServer , ref UpdateNetworkJsonController updateSendController)
        {
            this.responceServer = responceServer;
            this.updateSendController = updateSendController;
        }
        public void ANSWER_GEN_LINK()
        {
            string[] connectionId = responceServer.connectionId;
            string servergeneratedlink = responceServer.textmessage[5];
            bool check = false;

            if (!servergeneratedlink.Equals("error"))
            {
                foreach (string item in connectionId)
                {
                    if (staticVariable.Variable.activemqConnectionId.ContainsKey(item))
                    {
                        check = true;
                        break;
                    }

                }
                App.Current.Dispatcher.Invoke(new System.Action(() => Clipboard.SetText(servergeneratedlink)));

                Console.WriteLine("ObserverNetworkUpdate->ANSWERGENLINK: запрос пришел от сервера к нам с connectionID:  " + check + " и сгенерировал ссылку " + servergeneratedlink);
                updateSendController.updateSendJsonCHECKWORKINGSERVER();
            }
            else
            {
                Console.WriteLine("ObserverNetworkUpdate->ANSWERGENLINK: проблемы с генерацией ссылки");
            }
        }
    }
}
