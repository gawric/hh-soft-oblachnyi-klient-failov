﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.getJson.support
{
    public class CheckWorkingServer
    {
        private ServerTopicModel responceServer;

        private MainWindowViewModel viewModelMain;

        public CheckWorkingServer(ref MainWindowViewModel viewModelMain , ref ServerTopicModel responceServer)
        {
            this.viewModelMain = viewModelMain;
            this.responceServer = responceServer;

        }
        public void CHECK_WORKING_SERVER(ref UpdateFormMainController updateMainForm , ref ConvertIconType convertIcon)
        {
            //приходит ответ в формате
            //0 - заглушка версия базы
            //1 - запрос
            //2 - userid
            //3 - заглушка
            //4 - запрос
            //5 - connectionID от клиента
            //6 - isPublication (true или false)
            //7 - isWorking - занят сервер или нет
            string connectionId = responceServer.textmessage[5];
            bool isPublicationServer = Convert.ToBoolean(responceServer.textmessage[6]);
            bool isWorkingServer = Convert.ToBoolean(responceServer.textmessage[7]);

            staticVariable.Variable.activemqIsWorking = isWorkingServer;

            if (!isWorkingServer)
            {
                viewModelMain.pg = 100;
                updateMainForm.updateTitleMainWindow("Синхронизирован");
                viewModelMain.SetNewIconTaskBar(convertIcon.streamToIcon(".notifyIconBarOk"));
            }


            Console.WriteLine("ObserverNetworkUpdate->CHECKWORKINGSERVER: пришел ответ от сервера -> сервер обрабатывает какой-то запрос?  " + isWorkingServer);
        }
    }
}
