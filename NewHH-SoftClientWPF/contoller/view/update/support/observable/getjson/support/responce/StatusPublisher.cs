﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.getJson.support
{
    public class StatusPublisher
    {
        private ServerTopicModel responceServer;
        public BlockingEventController blockingEvent;
        public StatusPublisher(ref ServerTopicModel responceServer , ref BlockingEventController blockingEvent)
        {
            this.responceServer = responceServer;
            this.blockingEvent = blockingEvent;
        }
        public void STATUS_PUBLISHSER_RESPONCE()
        {
            //приходит ответ в формате
            //0 - заглушка версия базы
            //1 - запрос
            //2 - userid
            //3 - заглушка
            //4 - запрос
            //5 - connectionID от клиента
            //6 - isPublication (true или false)

            string[] connectionId = responceServer.connectionId;
            bool isPublicationServer = Convert.ToBoolean(responceServer.textmessage[6]);
            bool isWorkingServer = Convert.ToBoolean(responceServer.textmessage[7]);

            staticVariable.Variable.activemqIsWorking = isWorkingServer;

            bool check = false;

            foreach (string item in connectionId)
            {
                if (staticVariable.Variable.activemqConnectionId.ContainsKey(item))
                {
                    staticVariable.Variable.activemqIsPublisher = isPublicationServer;

                    Console.WriteLine("ДАННЫЙ КЛИЕНТ: connectionId " + item + " isPublication: " + isPublicationServer + " isWorkingServer " + isWorkingServer + "Имя выполняемого потока " + Thread.CurrentThread.Name);

                    blockingEvent.setMRE(staticVariable.Variable.autoSyncToTimeBlockingId);
                    check = true;
                    break;
                }

            }

            if (!check)
                Console.WriteLine("Прилетел запрос но принадлежит не этому клиенту:  " + "  connectionId  " + connectionId[0] + " isPublication: " + isPublicationServer + " isWorkingServer " + isWorkingServer);

        }
    }
}
