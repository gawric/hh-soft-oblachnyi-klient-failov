﻿using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.mvvm.model.networkGetJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.getJson.support
{
    public class SupportDictionaryResponce
    {
        public delegate string OperationDelegate(string responce , ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data);
        private Dictionary<string, OperationDelegate> dictDelegatResponce;
        private GetReponce getReponce;
        private GetJsonRequestModel gjrm;

        public SupportDictionaryResponce(ref GetJsonRequestModel gjrm)
        {
            this.gjrm = gjrm;
            getReponce = new GetReponce(ref gjrm);
            initDictDelegat();
        }

        public Dictionary<string, OperationDelegate> getDictionary()
        {
            return dictDelegatResponce;
        }
        private void initDictDelegat()
        {
            dictDelegatResponce = new Dictionary<string, OperationDelegate>();

            dictDelegatResponce.Add("SYNCREATEALLLIST", getReponce.SyncCreateAllList);
            dictDelegatResponce.Add("SYNSINGLLIST", getReponce.SynSinglList);
            dictDelegatResponce.Add("LAZYSYNSINGLLIST", getReponce.LazySinSinglList);
            dictDelegatResponce.Add("ACTUAL", getReponce.Actual);
            dictDelegatResponce.Add("Empty", getReponce.Empty);
            dictDelegatResponce.Add("SEARCHBEGINFILES", getReponce.SearchBeginFiles);
            dictDelegatResponce.Add("SEARCHPARTIESFILES", getReponce.SearchPartiesFiles);
            dictDelegatResponce.Add("SEARCHENDFILES", getReponce.SearchEndFiles);
            dictDelegatResponce.Add("ANSWERGENLINK", getReponce.AnswerGenLink);
            dictDelegatResponce.Add("STATUSPUBLISHER", getReponce.StatusPublisher);
            dictDelegatResponce.Add("CHECKWORKINGSERVER", getReponce.CheckWorkingServer);
            dictDelegatResponce.Add("STOPPUBLISHERNODELETEBASES", getReponce.StopPublisherNoDeleteBases);
   
        }
    }
}
