﻿using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
    class ObserverCopyWindowUpdate
    {
        private ObserverCopyFormUpdate _copyUpdateForm;

        public ObserverCopyWindowUpdate(Container cont)
        {
            _copyUpdateForm = cont.GetInstance<ObserverCopyFormUpdate>();

        }


        public void ObserverCopyUpdateHandler(CreateSubscriptionLoginForm subject)
        {

            subject.OnSaved += updateViewHandler;
        }

        //data - nameForm
        //data - args[0] - viewName
        //date - args[1] - data
        private void updateViewHandler(object sender, EventArgs e)
        {

            CustomUpdateEventArgs data = (CustomUpdateEventArgs)e;
            

            if (data.getNameForm.Equals("CopyForm"))
            {
                Console.WriteLine("ObserverCopyWindowUpdate->updateViewHandler -> CopyForm: Сработало Update ObserverCopyWindowUpdate");

                string viewElemetns = data.getTextMessage[0];
                string viewNewData = data.getTextMessage[1];

                _copyUpdateForm.updateView(viewElemetns, viewNewData);

            }


        }
    }
}
