﻿using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
    class ObserverUpdateTree
    {

        private ObserverMainWindowFormUpdate _mainUpdateForm;
        private UpdateDataController _updateDataController;
        private ObserverUpdateTreeView _treeViewMainForm;
       

        public ObserverUpdateTree(Container cont)
        {
            
            _treeViewMainForm = cont.GetInstance<ObserverUpdateTreeView>();


        }

        public void ObserverUpdateHandlerTreeView(CreateSubscriptionTreeView subjectTreeView)
        {

            subjectTreeView.OnSaved += updateTreeViewHandler;
        }


        private void updateTreeViewHandler(object sender, EventArgs e)
        {

            CustomUpdateEventTreeViewArgs data = (CustomUpdateEventTreeViewArgs)e;


            if (data.getNameForm.Equals("TreeViewMainForm"))
            {
                string viewElemetns = data.getTextMessage[0];
                string viewNewData = data.getTextMessage[1];
                SortableObservableCollection<FolderNodes> nodes = data.getNodesTreeView;
                TreeViewItem tvi = data.getTvi;
                _treeViewMainForm.updateView(viewElemetns, viewNewData, nodes , tvi);

            }
           

              
         }

        }
    }

