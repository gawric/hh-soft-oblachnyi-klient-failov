﻿using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support.request;
using NewHH_SoftClientWPF.mvvm.model.newtworkSendJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support
{
    public class SendJsonRequest : IRequest
    {
        private SendJsonRequestModel _sjrm;

        public SendJsonRequest(ref SendJsonRequestModel sjrm)
        {
            this._sjrm = sjrm;

        }

        public string send(string request, string parties, CustomSendJsonEventArgs data)
        {
            try
            {
                bool isCheck = checkData(data);
                string newVersionBases = "0";
                string versionRequest = data.getTextMessage[1];

                saveVersionBase(ref isCheck, ref newVersionBases , ref versionRequest);

                string[] usernameAndPassword = getAuthData();
                string username = usernameAndPassword[0];
                ServerTopicModel serverModel = createModel(ref  request, ref  newVersionBases, ref username, ref  parties);

                array(ref data, ref serverModel);

                string json = JsonConvert.SerializeObject(serverModel);
                send(ref _sjrm, ref json);

                clearData(serverModel, data, json);

                return parties;

            }
            catch (System.IndexOutOfRangeException a)
            {
                Console.WriteLine("ObserverNetworkSendJson->send: Ошибка " + a);
                return parties;
            }


        }


        private void saveVersionBase(ref bool isCheck, ref string newVersionBases, ref string textVersion)
        {
            //false - проверка не пройдена
            if (!isCheck)
            {
                newVersionBases = 0.ToString();
            }
            else
            {
                newVersionBases = textVersion;
            }

        }



        private void send(ref SendJsonRequestModel _sjrm , ref string json)
        {
            if (_sjrm._mqclientController != null)
            {
                //отправляет данные на сервер
                _sjrm._mqclientController.SendMqJsonServer(json);
            }
        }
        private void array(ref CustomSendJsonEventArgs data, ref ServerTopicModel serverModel)
        {
            if (data.getFileInfoArr != null)
            {
                serverModel.listFileInfo = data.getFileInfoArr;
            }

         
        }

        private ServerTopicModel createModel(ref string request , ref string newVersionBases , ref string username , ref string parties)
        {
            return _sjrm._supportTopicModel.CreateModelTopicParties(request, Convert.ToInt64(newVersionBases), username, parties);
        }
        private string[] getAuthData()
        {
            return _sjrm._sqlLiteController.getSelect().GetUserSqlite();
        }
        private void clearData(ServerTopicModel serverModel, CustomSendJsonEventArgs data, string json)
        {
            serverModel = null;
            data.setFileInfoArr(null);
            json = null;

        }

        private bool checkData(CustomSendJsonEventArgs data)
        {
            bool check = false;

            if (2 < data.getTextMessage.Length)
            {
                check = true;
            }

            return check;
        }

        public string beginParties(string request, string parties, CustomSendJsonEventArgs data)
        {


            _sjrm._updateMainForm.updateTitleMainWindow("Синхронизация");


            send(request, parties, data);
            return request;
        }

        public string deleteBeginParties(string request, string parties, CustomSendJsonEventArgs data)
        {



            if (_sjrm._updateMainForm != null)
            {
                _sjrm._updateMainForm.updateTitleMainWindow("Синхронизация");
            }


            send(request, parties, data);

            return request;
        }

        public string LazyUpdateParties(string request, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }

        public string LazyStopParties(string request, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }

        public string LazyEndUpdateParties(string request, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }

        public string sendLazy(string request, string parties, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }
    }
}
