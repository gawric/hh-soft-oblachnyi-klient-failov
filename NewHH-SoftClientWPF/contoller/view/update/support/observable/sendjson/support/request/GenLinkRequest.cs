﻿using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.mvvm.model.newtworkSendJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support.request
{
    public class GenLinkRequest : IOtherRequest
    {
        private SendJsonRequestModel _sjrm;
        public GenLinkRequest(ref SendJsonRequestModel sjrm)
        {
            _sjrm = sjrm;
        }

        
        public string requestGenLink(string request, string parties, CustomSendJsonEventArgs data)
        {
            _sjrm._updateMainForm.updateTitleMainWindow("Синхронизация");
            sendRequestGenLink(request, parties, data);

            return request;
        }

      
        private string sendRequestGenLink(string request, string parties, CustomSendJsonEventArgs data)
        {
            try
            {


                string[] usernameAndPassword = _sjrm._sqlLiteController.getSelect().GetUserSqlite();
                string listFilesRow_id = data.getTextMessage[1];
                ServerTopicModel serverModel = _sjrm._supportTopicModel.CreateModelTopicGenLink(request, listFilesRow_id, usernameAndPassword[0], parties);
                string json = JsonConvert.SerializeObject(serverModel);

                if (_sjrm._mqclientController != null)
                {
                    //отправляет данные на сервер
                    _sjrm._mqclientController.SendMqJsonServer(json);
                }


                json = null;

            }
            catch (System.IndexOutOfRangeException a)
            {
                Console.WriteLine("ObserverNetworkSendJson->send: Ошибка " + a);
            }


            return request;

        }

        public string alertError(string request, string parties, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }

        public string sendPUBLISHER(string request, string parties, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }


    }
}
