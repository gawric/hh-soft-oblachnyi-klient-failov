﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.network.json;

using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.contoller.update.support.observable.subscription;
using NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.newtworkSendJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using Newtonsoft.Json;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.SupportDictionaryRequest;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
    //Работает с запросами на отправку JSON файлов
    public class ObserverNetwokSendJson
    {
        private SupportServerTopicModel _supportTopicModel;
        private SqliteController _sqlLiteController;

        private UpdateFormMainController _updateMainForm;
        private SaveJsonToDiskContoller _saveJsonToDisk;
        private ReadJsonToDiskController _readJsonToDisk;
        private SupportDictionaryRequest _supportDictionaryRequest;
        private SendJsonRequestModel _sjrm;


        public ObserverNetwokSendJson(Container cont)
        {
            _supportTopicModel = new SupportServerTopicModel();
            _saveJsonToDisk = cont.GetInstance<SaveJsonToDiskContoller>();
            _readJsonToDisk = cont.GetInstance<ReadJsonToDiskController>();
            
        }

        public void injectActiveMq(MqClientController mqclientController)
        {
            try
            {

              //  _mqclientController = mqclientController;
                _sjrm = createModel(this, mqclientController, _saveJsonToDisk, _readJsonToDisk, _sqlLiteController, _supportTopicModel);
                _supportDictionaryRequest = new SupportDictionaryRequest(_sjrm);
            }
            catch(System.ArgumentException b)
            {
                Console.WriteLine("ObserverNetworkSendJson->injectActiveMq:Критическая ошибка "+ b.ToString());
            }
        }

        public void injectSqllite(SqliteController sqlLiteController)
        {
            _sqlLiteController = sqlLiteController;
          
        }
        public void injectUpdateFormMainController(UpdateFormMainController _updateMainForm)
        {
            this._updateMainForm = _updateMainForm;
            _sjrm._updateMainForm = _updateMainForm;


        }

        public void ObserverNetworkSendJson(CreateSubscriptionSendJson subject)
        {
            subject.OnSaved += updateNetworkJsonHandler;
        }

        

        private void updateNetworkJsonHandler(object sender, EventArgs e)
        {

            CustomSendJsonEventArgs data = (CustomSendJsonEventArgs)e;
            try
            {


                //отправляет данные на сервер пришедшие из текущего клиента
                if (data.getNameForm.Equals("SendJson"))
                {
                    string networkFunction = data.getTextMessage[0];


                    if (getDictSend().ContainsKey(networkFunction))
                    {
                        string request = getDictRequest()[networkFunction];
                        string parties = networkFunction;

                        getDictSend()[networkFunction](request, parties, data);
                    }
                    else
                    {
                        if (getDictLazy().ContainsKey(networkFunction))
                        {
                            string request = getDictRequest()[networkFunction];
                            getDictLazy()[networkFunction](request, data);
                        }
                    }


                }


            }
            catch(System.ArgumentException a)
            {
                Console.WriteLine(a.ToString());
            }
           
            

        }
       
      
        
        
       private Dictionary<string, OperationDelegate> getDictSend()
       {
            return _supportDictionaryRequest.getModelDict()._dictDelegatSend;
       }

       private Dictionary<string, string> getDictRequest()
       {
            return _supportDictionaryRequest.getModelDict()._dictRequest;
       }
       private Dictionary<string, OperationDelegateLazy> getDictLazy()
       {
            return _supportDictionaryRequest.getModelDict()._dictDelegatLazy;
        }


        private SendJsonRequestModel createModel(ObserverNetwokSendJson onsj , MqClientController mqclientController , SaveJsonToDiskContoller saveJsonToDisk , ReadJsonToDiskController readJsonToDisk, SqliteController sqlLiteController , SupportServerTopicModel supportTopicModel)
        { 
            SendJsonRequestModel model = new SendJsonRequestModel();
            model.onsj = onsj;
            model._mqclientController = mqclientController;
            model._saveJsonToDisk = saveJsonToDisk;
            model._readJsonToDisk = readJsonToDisk;
            model._sqlLiteController = sqlLiteController;
            model._supportTopicModel = supportTopicModel;
            model._updateMainForm = _updateMainForm;
            return model;
        }

       

       


    }

}

