﻿using NewHH_SoftClientWPF.mvvm;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.support
{
    public class ObserverCopyFormUpdate
    {
        //обновление это модели приводит к изменениям во view
        private CopyMainWindowViewModel _viewModel;

        public ObserverCopyFormUpdate(Container cont)
        {
            _viewModel = cont.GetInstance<CopyMainWindowViewModel>();
        }

        public void updateView(string nameElemetns, string data)
        {
            if (nameElemetns.Equals("CloseWindows"))
            {
                _viewModel.CloseWindowsCopy();
            }
            else if (nameElemetns.Equals("OpenWindows"))
            {
                _viewModel.OpenWindowsCopy();
            }
           
        }
    }
}
