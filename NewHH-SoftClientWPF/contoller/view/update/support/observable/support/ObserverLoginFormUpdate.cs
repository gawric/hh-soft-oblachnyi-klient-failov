﻿using NewHH_SoftClientWPF.mvvm;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.support
{
    class ObserverLoginFormUpdate
    {
        //обновление этой модели приводит к изменениям во view
        private LoginFormViewModel _viewModel;

        public ObserverLoginFormUpdate(Container cont)
        {
            _viewModel = cont.GetInstance<LoginFormViewModel>();
        }

        public void updateView(string nameElemetns , string data)
        {
            if(nameElemetns.Equals("TextButton"))
            {
                _viewModel.TextButtonValue = data;
            }
            else if(nameElemetns.Equals("ProgressBarValue"))
            {
                 double procent = Double.Parse(data);
                _viewModel.ProgressBarValue = procent;
            }
            else if (nameElemetns.Equals("TextErrorValue"))
            {
                
                _viewModel.TextErrorValue = data;
            }
            else if (nameElemetns.Equals("CloseAction"))
            {
                //закрытие окна
                _viewModel.ClosingLoginForm();
            }
            else if (nameElemetns.Equals("CrateAction"))
            {
                //закрытие окна
                _viewModel.OpenLoginForm();
            }
        }
    }
}
