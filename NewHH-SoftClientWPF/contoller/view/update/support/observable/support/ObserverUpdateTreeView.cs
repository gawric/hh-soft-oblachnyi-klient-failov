﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.listview;
using NewHH_SoftClientWPF.contoller.listview.initializing;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.treeview;
using NewHH_SoftClientWPF.contoller.update.treeview;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.contoller.util.sorted.comparer;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.support
{
    public class ObserverUpdateTreeView
    {
        //обновление это модели приводит к изменениям во view
        private MainWindowViewModel _viewModel;
        private UpdateDataController _updateDataController;
        private SqliteController _sqlliteController;
        private ViewFolderViewModel _listViewFiles;
        private FirstSyncFilesController _firstSyncController;
        private bool isUpdateListViev = false;
        private NavigationMainWindowsController _navigationMainController;
        private ViewTransferViewModel _viewtModelTansfer;
        private InitializingTreeViewController _initializingTreeView;
        private InitializingViewTransferController _initializingViewTransfer;
        private InitializingListViewController _initializingListViewController;
        private UpdateListViewNodes _updateListViewNodes;
        private ConvertIconType _convert;
        private IComparer<FolderNodes> comparer = new CompareViewFiles<FolderNodes>();
        private StackUpdateTreeViewIcon _stackUpdateTreeViewIcon;


        public ObserverUpdateTreeView(Container cont)
        {
            _convert = cont.GetInstance<ConvertIconType>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _updateDataController = cont.GetInstance<UpdateDataController>();
            _sqlliteController = cont.GetInstance<SqliteController>();
            _listViewFiles = cont.GetInstance<ViewFolderViewModel>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            _navigationMainController = cont.GetInstance<NavigationMainWindowsController>();
            _viewtModelTansfer = cont.GetInstance<ViewTransferViewModel>();
            _initializingTreeView = new InitializingTreeViewController(_sqlliteController , _convert);
            _initializingViewTransfer = new InitializingViewTransferController();
            _initializingListViewController = new InitializingListViewController(_sqlliteController , _convert);
            _updateListViewNodes = new UpdateListViewNodes(_sqlliteController , _convert);
            _stackUpdateTreeViewIcon = new StackUpdateTreeViewIcon(_convert);


        }


        public void updateView(string nameElemetns, string data , SortableObservableCollection<FolderNodes> nodes , TreeViewItem tvi)
        {
           //срабатывает при инициализации TreeView - что-бы можно было заполнить Noda-ми дерево
            if (nameElemetns.Equals("InitializingTreeView"))
            {
                //инициализация ListView
                _initializingListViewController.initializingListView(_sqlliteController, _listViewFiles, _viewModel , _convert);

                //инициализация TreeView
                _initializingTreeView.initializingTreeView(_sqlliteController, _listViewFiles, _viewModel , _convert);

                //инициализация ViewTransfer
                _initializingViewTransfer.initializingViewTrasfer(_sqlliteController, _viewtModelTansfer);

               



            } //срабатывает когда пользователь открывает nod
            else if (nameElemetns.Equals("LazyTreeView"))
            {
                long parentID = long.Parse(data);
                SortableObservableCollection<FolderNodes> _treeViewNode = new SortableObservableCollection<FolderNodes>();
                SortableObservableCollection<FolderNodes> _rootNodes = nodes;
  
                _navigationMainController.setNewOpenNode(false);

                Task.Run(() =>
                {
                    List<FileInfoModel> listClient = _sqlliteController.getSelect().GetSqlLiteParentIDFileInfoListMinimal(parentID);
                    List<SaveToPcModel> listSavePc = _sqlliteController.getSelect().GetSqlLiteParentIDSavePcMinimal(parentID);
                    List<AutoSaveFolderModel> listAutoSaveFolder = _sqlliteController.getSelect().GetSqlLiteParentIDAutoSaveFolderMinimal(parentID);
                    setPcIconType(listSavePc, listClient);
                    setAutoSaveIconType(listAutoSaveFolder, listClient);


                    TreeViewLoadingLazyController _loadingLazyTreeview = new TreeViewLoadingLazyController(_sqlliteController , _convert );
                    _loadingLazyTreeview.UpdateLazyTreeView(listClient, _treeViewNode, parentID , _convert);
                    _treeViewNode.Sort((x => x), comparer);

                    _viewModel.RebindingList(_treeViewNode, tvi , _rootNodes);


                });

            }
            else if (nameElemetns.Equals("RenameTreeView"))
            {
                string oldFolderName = data;
                SortableObservableCollection<FolderNodes> TreeViewNode = nodes;
                
                Task.Run(async () =>
                {
                    _firstSyncController.renameServer(TreeViewNode,   _sqlliteController,  oldFolderName);
                });


            }
            else if (nameElemetns.Equals("LazyListView"))
            {
                _viewModel.pg = 35;
                long parentID = long.Parse(data);
                ObservableCollection<FolderNodes> TreeViewNode = nodes;
                SortableObservableCollection<FolderNodes> ListViewNode = new SortableObservableCollection<FolderNodes>();

                if(isUpdateListViev == false)
                {

                    Task.Run(() =>
                    {
                        try
                        {
                            isUpdateListViev = true;
                            //все данные из бд для root нодов поиск по -1
                            List<FileInfoModel> listClient = _sqlliteController.getSelect().GetSqlLiteParentIDFileInfoList(parentID);
                            List<SaveToPcModel> listSavePc = _sqlliteController.getSelect().GetSqlLiteParentIDSavePcMinimal(parentID);
                            List<AutoSaveFolderModel> listAutoSaveFolder = _sqlliteController.getSelect().GetSqlLiteParentIDAutoSaveFolderMinimal(parentID);
                            _viewModel.pg = 45;

                            setPcIconType(listSavePc, listClient);
                            setAutoSaveIconType(listAutoSaveFolder, listClient);

                            ListViewNode = _updateListViewNodes.UpdateNodesLazyListView(listClient, ListViewNode, parentID, _viewModel, _convert);
                            ListViewNode.Sort((x => x), comparer);


                            _listViewFiles.RebindingList(ListViewNode);
                            isUpdateListViev = false;
                        }
                        catch (System.Windows.Markup.XamlParseException a)
                        {
                            Console.WriteLine("ObserverUpdateTreeView->updateView->LazyListView "+ a);
                        }
                        
                    });

                }
                

                


            }

        }
        //если файлы должны быть сохр. на компьютере(говорим ноду что-бы отобразил иконки)
        private void setPcIconType(List<SaveToPcModel> listSavePc , List<FileInfoModel> listClient)
        {
            foreach(FileInfoModel item in listClient)
            {
                string type = getPcType(listSavePc, item.row_id, item.type);
                item.type = type;
            }
        }

        private void setAutoSaveIconType(List<AutoSaveFolderModel> listSavePc, List<FileInfoModel> listClient)
        {
            foreach (FileInfoModel item in listClient)
            {
                string type = getAutoSaveType(listSavePc, item.row_id , item.type);
                item.type = type;
            }
        }

        private string getAutoSaveType(List<AutoSaveFolderModel> listSavePc, long row_id , string originalType)
        {
            AutoSaveFolderModel item = listSavePc.FirstOrDefault(x => x.row_id_listfiles == row_id);
            if (item != null) return item.type;

            return originalType;
        }


        private string getPcType(List<SaveToPcModel> listSavePc, long row_id, string originalType)
        {
            SaveToPcModel item = listSavePc.FirstOrDefault(x => x.row_id_listfiles == row_id);
            if (item != null) return item.type;
            //originalType = original.type;
            //проверка 
            //foreach (SaveToPcModel item in listSavePc)
            //{
            // if (item.row_id_listfiles == row_id)
            // {
            //   originalType = item.type;
            //   break;
            //}
            //}

            return originalType;
        }






    }
}
