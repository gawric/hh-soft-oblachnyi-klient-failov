﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBar
{
    public class UpdateViewTransferFolderPgArgs : EventArgs
    {
        public long row_id { get; set; }
        public int sizeFilesFolder { get; set; }
        public int currentFilesFolder { get; set; }
        //подготовка или уже копирование
        public bool training { get; set; }
        public bool errorStatus { get; set; }
        public bool isStop { get; set; }

        public UpdateViewTransferFolderPgArgs()
        {
            errorStatus = false;
        }
    }
}
