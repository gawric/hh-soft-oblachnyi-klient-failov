﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBar.download.supportDownload
{
    public class UpdatePbDownloadFolder
    {
        private long row_id_transfer;
        private DownloadWebDavModel downloadModelWebDav;
        private FolderNodesTransfer nodes;
        private NodesPb nodesPb;
        private UpdatePb updatePb;

        public UpdatePbDownloadFolder(long row_id_transfer , DownloadWebDavModel downloadModelWebDav , FolderNodesTransfer nodes , NodesPb nodesPb)
        {
            this.row_id_transfer = row_id_transfer;
            this.downloadModelWebDav = downloadModelWebDav;
            this.nodes = nodes;
            this.nodesPb = nodesPb;
            updatePb = new UpdatePb( nodesPb, downloadModelWebDav._sqlliteController , nodes , downloadModelWebDav.convertIcon);
        }

        public void update(bool check)
        {
            long sizeFolderCount = downloadModelWebDav.modelSizebyteAndCount.sizeCountFolder;
            long sizeFilesCount = downloadModelWebDav.modelSizebyteAndCount.sizeCountFiles;
            int[] currentFilesCount = downloadModelWebDav.currentCountFiles;
            long sizeBytesFolder = downloadModelWebDav.modelSizebyteAndCount.sizeBytesFolder;


            updatePb.update(check,  sizeFilesCount,  currentFilesCount, sizeBytesFolder , sizeFolderCount);
        }

        public void updateStop()
        {
            updatePb.updateStopRelase();
        }

        public void updateListTransferSqlLite(FolderNodesTransfer nodes, SqliteController _sqliteController, string uploadStatus)
        {
            nodesPb.updateListTransferSqlLite(nodes, _sqliteController, nodes.UploadStatus);
        }


    }
}
