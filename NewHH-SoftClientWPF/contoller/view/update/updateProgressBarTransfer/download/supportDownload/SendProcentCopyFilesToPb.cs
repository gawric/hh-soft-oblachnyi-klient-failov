﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Progress;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.download.supportDownload
{
    public class SendProcentCopyFilesToPb : IDownload
    {
        public event EventHandler<DownloadStateEventArgs> StateChanged;
        public event EventHandler<DownloadStateEventArgs> Completed;
        public event EventHandler<DownloadStateEventArgs> StateError;
        private ManualResetEvent mre;
        private CancellationToken cancellation;

        private long row_id;

        public void ChangeError()
        {
            DownloadStateEventArgs args = new DownloadStateEventArgs();
            args.percent = GetPercent(0, 0);
            args.row_id = getRow_id();
            args.isStop = getCancel();

            StateError?.Invoke(this, args);
        }

        public void ChangeState(long uploadBytes, long size, string state, bool isComplete)
        {
            if (isComplete)
            {
                DownloadStateEventArgs args = new DownloadStateEventArgs();
                args.percent = 100;
                args.row_id = getRow_id();
                args.isStop = getCancel();

                Completed?.Invoke(this, args);
            }
            else
            {
                //ожидание
                mre.WaitOne();

                DownloadStateEventArgs args = new DownloadStateEventArgs();
                args.percent = GetPercent(size, uploadBytes);
                args.row_id = getRow_id();
                args.isStop = getCancel();

                StateChanged?.Invoke(this, args);
            }
              
        }

        public void setRow_id(long row_id)
        {
            this.row_id = row_id;
        }

        public long getRow_id()
        {
            return row_id;
        }

        public static long GetPercent(long b, long a)
        {
            if (b == 0) return 0;

            return (long)(a / (b / 100M));
        }

        public void setMre(ManualResetEvent mre)
        {
            this.mre = mre;
        }

        public bool getCancel()
        {
            if(cancellation != null)
            {
                return cancellation.IsCancellationRequested;
            }
            else
            {
                return false;
            }
        }

        public void setCancel(CancellationToken cancellation)
        {
            this.cancellation = cancellation;
        }
    }
}
