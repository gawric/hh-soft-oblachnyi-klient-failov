﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support
{
    public class UpdatePb
    {
        private NodesPb nodesPb;
        private FolderNodesTransfer nodes;
        private SqliteController _sqlliteController;
        private ConvertIconType convertIcon;
        //Общие методы для UploadWebDabFolder и DownloadWebDavFolder
        public UpdatePb(NodesPb nodesPb , SqliteController _sqlliteController , FolderNodesTransfer nodes, ConvertIconType convertIcon)
        {
            this.nodesPb = nodesPb;
            this._sqlliteController = _sqlliteController;
            this.nodes = nodes;
            this.convertIcon = convertIcon;
    }
        //long sizeFilesCount = downloadModelWebDav.modelSizebyteAndCount.sizeCountFiles;
        //int[] currentFilesCount = downloadModelWebDav.currentCountFiles;
        public void update(bool check, long sizeFilesCount, int[] currentFilesCount, long sizeBytesFolder , long sizeFolderCount)
        {
            //check true - подготовка т.е создание папок и сканирование папки на жестком диске
            if (check)
            {
                //если в папке нет файлов, а ход нужно отобразить
                if(sizeFilesCount == 0)
                {
                    if(sizeFolderCount != 0) training(sizeFolderCount, currentFilesCount, sizeBytesFolder);
                }
                else
                {
                    training(sizeFilesCount, currentFilesCount, sizeBytesFolder);
                }

            }
            else
            {
                noTraining(sizeFilesCount, currentFilesCount);
            }
        }

        public void updateStopRelase()
        {
                nodes.UploadStatus = "Остановлен";
                nodes.Progress = 0;
                nodes.Sizebyte = staticVariable.UtilMethod.FormatBytes(0);
        }

        //downloadModelWebDav.modelSizebyteAndCount.sizeBytesFolder
        private void training(long sizeFilesCount, int[] currentFilesCount , long sizeBytesFolder)
        {
            if (nodes != null)
            {
       
                    updateRunning(sizeFilesCount, currentFilesCount, sizeBytesFolder);
       
            }

        }
       

        private void updateRunning(long sizeFilesCount, int[] currentFilesCount, long sizeBytesFolder)
        {
            double percent = nodesPb.GetPercent(sizeFilesCount, currentFilesCount[0]);

            if (percent >= 100)
            {
                nodes.UploadStatus = "Запуск";
                nodes.Progress = percent;
                nodes.Sizebyte = staticVariable.UtilMethod.FormatBytes(sizeBytesFolder);

                nodesPb.updateListTransferSqlLite(nodes, _sqlliteController, nodes.UploadStatus);

            }
            else
            {
                //если еще не определили размер и кол-во файлов 
                if (sizeFilesCount <= 0)
                {
                    nodes.UploadStatus = "Поиск...";
                    nodes.Progress = percent;
                }
                else
                {
                    nodes.UploadStatus = "Подготовка";
                    nodes.Progress = percent;

                    nodes.Sizebyte = staticVariable.UtilMethod.FormatBytes(sizeBytesFolder);
                }

                nodesPb.updateListTransferSqlLite(nodes, _sqlliteController, nodes.UploadStatus);
            }
        }

        private void noTraining(long sizeFilesCount, int[] currentFilesCount)
        {
            if (nodes != null)
            {
               update(sizeFilesCount, currentFilesCount);
            }
        }

        private void update(long sizeFilesCount, int[] currentFilesCount)
        {
            double percent = nodesPb.GetPercent(sizeFilesCount, currentFilesCount[0]);

            if (percent >= 100)
            {
                nodes.UploadStatus = "Выполнено";
                nodes.Progress = percent;

                nodesPb.updateListTransferSqlLite(nodes, _sqlliteController, nodes.UploadStatus);

            }
            else
            {
                nodes.UploadStatus = "Загрузка";
                nodes.Progress = percent;
                nodesPb.updateListTransferSqlLite(nodes, _sqlliteController, nodes.UploadStatus);
            }
        }


    }
}
