﻿using NewHH_SoftClientWPF.contoller.update.updateProgressBar.download.supportDownload;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.upload.supportUpload;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Progress;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.view.update.updateProgressBarTransfer.upload.supportUpload
{
    //Срабатывает когда заливаем папку отоброжает статус заливки именно файла, а не папки целиком
    public class UpdateVtPgDetails
    {
    
        private ObservableCollection<FolderNodesTransfer> nodesTrasfer;
        private FolderNodesTransfer nodes;
        private long row_id_transfer;
        private NodesPb nodesPb;
        private UpdatePbDetails updatePbUploadFiles;


        public UpdateVtPgDetails(IDownload statusDownload, long row_id_transfer , ScanUploadWebDavModel uploadModel)
        {
 
            this.nodesTrasfer = uploadModel._transferViewNodes;
            this.row_id_transfer = row_id_transfer;
            nodesPb = new NodesPb(nodesTrasfer, row_id_transfer, uploadModel.convertIcon);

            updatePbUploadFiles = new UpdatePbDetails(nodesPb, nodes, row_id_transfer, uploadModel._sqlLiteController);
        }
        
        //Подписка на Обновление файлов
        public void subsribleUpdateFiles(IDownload statusDownload)
        {
            if (nodes == null)
            {
                nodes = new FolderNodesTransfer();
            }

            if (nodes != null)
            {
                nodes = nodesPb.getNodes(row_id_transfer);

                statusDownload.Completed += new EventHandler<DownloadStateEventArgs>(updatePbUploadFiles.c_StateComplete);
                statusDownload.StateChanged += new EventHandler<DownloadStateEventArgs>(updatePbUploadFiles.c_StateChanged);
                statusDownload.StateError += new EventHandler<DownloadStateEventArgs>(updatePbUploadFiles.c_StateError);
            }

        }

    }
}
