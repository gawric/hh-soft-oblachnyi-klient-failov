﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.upload.supportUpload;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.view.update.updateProgressBarTransfer.upload.supportUpload
{
    public class UpdatePbDetails
    {
        private NodesPb nodesPb;
        private FolderNodesTransfer nodes;
        private long row_id_transfer;
        private SqliteController sqlliteController;



        public UpdatePbDetails(NodesPb nodesPb, FolderNodesTransfer nodes, long row_id_transfer, SqliteController sqlliteController)
        {
            this.nodesPb = nodesPb;
            this.nodes = nodes;
            this.row_id_transfer = row_id_transfer;
            this.sqlliteController = sqlliteController;
        }

        public void c_StateComplete(object sender, DownloadStateEventArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {

                nodes.ProgressFiles = 100;
               // nodes.UploadStatus = "Выполнено";
               // nodesPb.updateListTransferSqlLite(nodes, sqlliteController, nodes.UploadStatus);
            }

        }

        public void c_StateError(object sender, DownloadStateEventArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {
                if (e.isStop)
                {
                    nodes.ProgressFiles = 0;
                   // nodes.UploadStatus = "Остановлен";
                   // nodesPb.updateListTransferSqlLite(nodes, sqlliteController, nodes.UploadStatus);
                }
                else
                {
                    nodes.ProgressFiles = 0;
                   // nodes.UploadStatus = "Ошибка";
                    //nodesPb.updateListTransferSqlLite(nodes, sqlliteController, nodes.UploadStatus);
                }


            }

        }

        public void c_StateChanged(object sender, DownloadStateEventArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {


                try
                {
                    if (e.isStop)
                    {
                        nodes.ProgressFiles = 0;
                       // nodes.UploadStatus = "Остановлен";
                      //  nodesPb.updateListTransferSqlLite(nodes, sqlliteController, nodes.UploadStatus);
                    }
                    else
                    {
                        if (nodes.progress != e.percent)
                        {
                            //nodes.UploadStatus = "Загрузка";
                            nodes.ProgressFiles = e.percent;
                           // nodesPb.updateListTransferSqlLite(nodes, sqlliteController, nodes.UploadStatus);
                        }
                    }


                }
                catch (Exception a)
                {
                    Console.WriteLine("UpdateViewTransferPg ->c_StateChanged  " + a);
                }

            }

        }
    }
}
