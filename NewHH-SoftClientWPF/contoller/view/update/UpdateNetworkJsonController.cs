﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.subscription;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update
{
   public class UpdateNetworkJsonController
    {

        private CreateSubscriptionSendJson updateHandler;
        private ObserverNetwokSendJson observer;

        public UpdateNetworkJsonController(Container cont)
        {
            observer = cont.GetInstance<ObserverNetwokSendJson>();
            updateHandler = new CreateSubscriptionSendJson();
            //Отдельный слушатель для отправки json запросов
            observer.ObserverNetworkSendJson(updateHandler);
        }

        //Отправляет данные из MqBroker -> в форму для обновления данных SqLite и TreeView -> MainForm
        public void updateSendJsonPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonPARTIES: Отправка флага PARTIES обновление всех файлов");
            sendUpdateNetwork("SendJson", getTextMessage("PARTIES", newVersionbases.ToString()), nodes);
        }

        
        public void updateSendJsonBEGINPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonBEGINPARTIES: Отправка флага BEGINPARTIES обновление всех файлов");
            sendUpdateNetwork("SendJson", getTextMessage("BEGINPARTIES", newVersionbases.ToString()), nodes);
        }
        public void updateSendJsonENDPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonENDPARTIES: обновление всех файлов");
            sendUpdateNetwork("SendJson", getTextMessage("ENDPARTIES", newVersionbases.ToString()), nodes);
        }
        //отправка Delete информации
        public void updateSendJsonDELETEBEGINPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonDELETEBEGINPARTIES: Отправка флага DELETEBEGINPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("DELETEBEGINPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonDELETEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonDELETEPARTIES Отправка флага DELETEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("DELETEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonDELETEENDPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonDELETEENDPARTIES Отправка флага DELETEENDPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("DELETEENDPARTIES", newVersionbases.ToString()), nodes);
        }

        //Отправка Copy/Paste Информации на сервер
        public void updateSendJsonPASTEBEGINPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonPASTEBEGINPARTIES Отправка флага PASTEBEGINPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("PASTEBEGINPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonPASTEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonPASTEPARTIES: Отправка флага PASTEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("PASTEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonPASTEENDPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonPASTEENDPARTIES: Отправка флага PASTEENDPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("PASTEENDPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonSINGLBEGINUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> SINGLBEGINUPDATEPARTIES: Отправка флага SINGLBEGINUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("SINGLBEGINUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonSINGLUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSINGLUPDATEPARTIES: Отправка флага SINGLUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("SINGLUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonSINGLENDUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSINGLENDUPDATEPARTIES: Отправка флага SINGLENDUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("SINGLENDUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }


        public void updateSendJsonLAZYSINGLBEGINUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> LAZYSINGLBEGINUPDATEPARTIES: Отправка флага LAZYSINGLBEGINUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("LAZYSINGLBEGINUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonLAZYSINGLUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonLAZYSINGLUPDATEPARTIES: Отправка флага LAZYSINGLUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("LAZYSINGLUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonLAZYSINGLENDUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonLAZYSINGLENDUPDATEPARTIES: Отправка флага LAZYSINGLENDUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("LAZYSINGLENDUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonLAZYSINGLUPDATEPARTIESTOP(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> LAZYSINGLUPDATEPARTIESTOP: Отправка флага LAZYSINGLUPDATEPARTIESTOP");
            sendUpdateNetwork("SendJson", getTextMessage("LAZYSINGLUPDATEPARTIESTOP", newVersionbases.ToString()), nodes);
        }



        public void updateSendJsonRENAMESINGLBEGINUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> SINGLBEGINUPDATEPARTIES: Отправка флага RENAMESINGLENDUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("RENAMESINGLBEGINUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonRENAMESINGLUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSINGLUPDATEPARTIES: Отправка флага RENAMESINGLENDUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("RENAMESINGLUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonRENAMESINGLENDUPDATEPARTIES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSINGLENDUPDATEPARTIES: Отправка флага RENAMESINGLENDUPDATEPARTIES");
            sendUpdateNetwork("SendJson", getTextMessage("RENAMESINGLENDUPDATEPARTIES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonERRORALERT(string errorAlert, string textAlert)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonERRORALERT: Отправка флага ERRORALERT");
            string network = "SendJson";
            string networkStatus = "ALERTERROR";
            string[] textMessage = { networkStatus ,  errorAlert, textAlert };
     
            sendUpdateNetwork(network, textMessage, null);

        }
        


        public void updateSendJsonSEARCHBEGINFILES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSEARCHBEGINFILES: Отправка флага SEARCHBEGINFILES");
            sendUpdateNetwork("SendJson", getTextMessage("SEARCHBEGINFILES", newVersionbases.ToString()), nodes);

        }
        public void updateSendJsonSEARCHPARTIESFILES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSEARCHPARTIESFILES: Отправка флага SEARCHPARTIESFILES");
            sendUpdateNetwork("SendJson", getTextMessage("SEARCHPARTIESFILES", newVersionbases.ToString()), nodes);
        }
        public void updateSendJsonSEARCHENDFILES(FileInfoModel[] nodes, long newVersionbases)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSEARCHENDFILES: Отправка флага SEARCHENDFILES");
            sendUpdateNetwork("SendJson", getTextMessage("SEARCHENDFILES", newVersionbases.ToString()), nodes);
        }

        public void updateSendJsonSTATUSPUBLISHER()
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSTATUSPUBLISHER: Отправка флага STATUSPUBLISHER");
            string network = "SendJson";
            string networkStatus = "STATUSPUBLISHER";
            string[] textMessage = {networkStatus};
  
            sendUpdateNetwork(network, textMessage, null);

        }

        public void updateSendJsonCHECKWORKINGSERVER()
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonSTATUSPUBLISHER: Отправка флага CHECKWORKINGSERVER");
            string network = "SendJson";
            string networkStatus = "CHECKWORKINGSERVER";
            string[] textMessage = { networkStatus };

            sendUpdateNetwork(network, textMessage, null);
        }

        public void updateSendJsonREQUESTGENLINK(long listFilesRowId)
        {
            Console.WriteLine("UpdateNetworkJsonController-> updateSendJsonREQUESTGENLINK: Отправка флага REQUESTGENLINK");
            string network = "SendJson";
            string networkStatus = "REQUESTGENLINK";
            string[] textMessage = { networkStatus  , listFilesRowId.ToString() };

            sendUpdateNetwork(network, textMessage, null);
        }





        public void sendToDeleteArr(List<string> locationList, SendDeleteArrModel sendDeleteModel)
        {
            FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            long newVersionbases = sendDeleteModel.sqliteController.getSelect().getSqlLiteVersionDataBasesClient();

            arr = addFileInfo(arr, locationList, sendDeleteModel.sqliteController);
            updateSendJsonDELETEBEGINPARTIES(null, newVersionbases);
            updateSendJsonDELETEENDPARTIES(arr, newVersionbases);
        }

        private FileInfoModel[] addFileInfo(FileInfoModel[] arr, List<string> locationList, SqliteController _sqlLiteController)
        {
            int index = 0;
            foreach (string location in locationList)
            {
                FileInfoModel model = _sqlLiteController.getSelect().getSearchNodesByLocation(location);
                model.filename = location;
                model.changeRows = "DELETE";
                arr[index] = model;
                index++;
            }

            return arr;
        }

        public void sendUpdateNetwork(string network, string[] textMessage, FileInfoModel[] arr)
        {
            if(updateHandler != null)
            {
                updateHandler.updateNetworkJson(network, textMessage, arr);
            }
           
        }

        private string[] getTextMessage(string networkStatus, string newVersionBases)
        {
            //string[] textMessage = { networkStatus, newVersionBases };
            return new string[] { networkStatus, newVersionBases };
        }

    }
}
