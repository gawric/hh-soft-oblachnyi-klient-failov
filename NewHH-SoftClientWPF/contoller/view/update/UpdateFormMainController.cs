﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.update
{
    public class UpdateFormMainController
    {
        //промежуточная подписка
        private CreateSubscriptionMainForm updateMainHandler;
        private CreateSubscriptionTreeView updateTreeViewHandler;

        //здесь происходит вся магия обновления
        private ObserverMainUpdate observer;
        private ObserverUpdateTree observerTree;

        public UpdateFormMainController(Container cont)
        {
            updateMainHandler = cont.GetInstance<CreateSubscriptionMainForm>();
            updateTreeViewHandler = cont.GetInstance<CreateSubscriptionTreeView>();
            observer = cont.GetInstance<ObserverMainUpdate>();
            observerTree = cont.GetInstance<ObserverUpdateTree>();

            //Регистрация кто будет слушать сообщения на форму
            observer.ObserverMainUpdateHandler(updateMainHandler);

            //Отдельный слушатель на Дерево
            observerTree.ObserverUpdateHandlerTreeView(updateTreeViewHandler);
        }




        //вызываем из нужно участка кода для обновления View 
        //[0] - nameViewElements
        //[1] - data Elements
        public void sendUpdateMainForm(string nameForm, string[] textMessage)
        {
            updateMainHandler.updateForm(nameForm, textMessage);
        }

        //вызываем из нужно участка кода для обновления View 
        //[0] - nameViewElements
        //[1] - data Elements
        public void sendUpdateTreeView(string nameForm, string[] textMessage, SortableObservableCollection<FolderNodes> nodes , TreeViewItem tvi)
        {
            //обновляет TreeViewNodes - пришлось создавать отдельного слушателя т.к искать кто вызвал действие слишком дорого
            //здесь мы передаем текущий ObservableCollection что-бы не проходить по всей TreeView
            updateTreeViewHandler.updateTreeView(nameForm, textMessage, nodes , tvi);
        }

      
        //обновляет MainWindowTitle
        public void updateTitleMainWindow(string data)
        {
            string nameForm = "MainForm";
            string nameElemetns = "Titlename";
            string[] textMessage = { nameElemetns, data };

            sendUpdateMainForm(nameForm, textMessage);
        }

        public void updateMainInitializingTreeView()
        {

            string nameForm = "TreeViewMainForm";
            string nameElemetns = "InitializingTreeView";
            string[] textMessage = { nameElemetns, "" };
            SortableObservableCollection<FolderNodes> nodes = new SortableObservableCollection<FolderNodes>();
            TreeViewItem tvi = new TreeViewItem();
            sendUpdateTreeView(nameForm, textMessage , nodes , tvi);
        }

        public void updateMainLazyTreeView(string ParentID , SortableObservableCollection<FolderNodes> nodes , TreeViewItem tvi)
        {
            string nameForm = "TreeViewMainForm";
            string nameElemetns = "LazyTreeView";
            string[] textMessage = { nameElemetns, ParentID };

            sendUpdateTreeView(nameForm, textMessage , nodes , tvi);
        }

        public void updateMainRenameTreeView(string ParentID, SortableObservableCollection<FolderNodes> nodes)
        {
            string nameForm = "TreeViewMainForm";
            string nameElemetns = "RenameTreeView";
            string[] textMessage = { nameElemetns, ParentID };
            TreeViewItem tvi = null;
            sendUpdateTreeView(nameForm, textMessage, nodes, tvi);
        }

        public void updateMainLazyListView(string ParentID, SortableObservableCollection<FolderNodes> nodes)
        {
            string nameForm = "TreeViewMainForm";
            string nameElemetns = "LazyListView";
            string[] textMessage = { nameElemetns, ParentID };
            TreeViewItem tvi = null;
            sendUpdateTreeView(nameForm, textMessage, nodes, tvi);
        }

        
    }
}
