﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.contoller.treeview
{
    class TreeViewLoadingLazyController
    {
        private SqliteController _sqlitecont;
        private object _myCollectionLock = new object();
        private CreateModel _createModel ;
    

        public TreeViewLoadingLazyController(SqliteController _sqlitecont, ConvertIconType convertIcon )
        {
            this._sqlitecont = _sqlitecont;
            this._createModel = new CreateModel(convertIcon);
        }

        //ленивая загрузка node в TreeView
        public void UpdateLazyTreeView(List<FileInfoModel> listFiles, ObservableCollection<FolderNodes> TreeViewNode, long ParentID , ConvertIconType convertIcon)
        {
            try
            {
                //обновляет gui даже из друкого потока
                 ClearTreeNodes(TreeViewNode);
                //TreeViewNode.Clear();
                // ListViewNode.Clear();
               
                if(listFiles.Count != 0)
                {
                    listFiles.ForEach(delegate (FileInfoModel modelServer)
                    {
                        SearchTreeViewLazy(modelServer, TreeViewNode, ParentID, convertIcon);
                    });

                }
                else
                {
                    App.Current.Dispatcher.Invoke(delegate
                    {
                        TreeViewNode.Clear();
                    });
                    
                }



            }
            catch (System.NotSupportedException h2)
            {
                Console.WriteLine("TreeViewLoadingLazyController - > UpdateLazyTreeView " + h2);
            }



        }

        private void SearchTreeViewLazy(FileInfoModel modelServer, ObservableCollection<FolderNodes> Node, long searh , ConvertIconType convertIcon)
        {
            try
            {
                
                if (modelServer.parent == searh)
                {
                    //Обновляет TreeView
                    //modelServer Текущий элемент перебора
                    //Node вся коллекция TreeView выбранного нода
                    UpdateNodesTreeView(modelServer, Node , convertIcon);
                }
            }
            catch (System.NullReferenceException s)
            {
                Console.WriteLine("TreeViewLoadingLazyController - > SearchTreeViewLazy " + s);
            }

        }

        private void ClearTreeNodes(ObservableCollection<FolderNodes> TreeViewNode)
        {

            //откуда запущен поток
            Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);

            if (dispatcher == null)
            {

                try
                {
                    App.Current.Dispatcher.Invoke(delegate // <--- HERE
                    {
                        TreeViewNode.Clear();
                    });

                }
                catch (System.InvalidOperationException ss)
                {

                    Console.WriteLine(ss);
                }


            }
            else
            {
                TreeViewNode.Clear();
            }
            
        }

        private void UpdateNodesTreeView(FileInfoModel modelServer, ObservableCollection<FolderNodes> Nodes , ConvertIconType convertIcon)
        {
            try
            {
                //только папки
                if (staticVariable.Variable.isFolder(modelServer.type))
                {

                    string FolderName = modelServer.filename;
                    long ParentID = modelServer.parent;
                    string Type = modelServer.type;
                    long VersionUpdateRows = modelServer.versionUpdateRows;
                    long Row_id = modelServer.row_id;
                    string changeRows = modelServer.changeRows;
                    string location = modelServer.location;
                    string changeDate = modelServer.changeDate;
                    string createDate = modelServer.createDate;
                    long syzbytes = modelServer.sizebyte;
                    string lastOpenDate = modelServer.lastOpenDate;

                    FolderNodes folderclient = _createModel.CreateFolderNodesModel(FolderName, ParentID, Type, VersionUpdateRows, Row_id, changeRows, location, createDate, changeDate, syzbytes, lastOpenDate, convertIcon);
                  

                    expandRootNode(folderclient);


                    //проверяет есть ли дети у данной папки дети
                    bool checkParent = _sqlitecont.getSelect().existSqlLiteParentNodes(Row_id);

                    createRootNodes(checkParent, Row_id, folderclient, convertIcon);
                    //Очищает у текущего нода "Загрузка"
                    ClearLoadingNodes(modelServer, Nodes);

                    bool checkExistInTreeview = SearchNodesInTreeView(modelServer, Nodes);
                    //Если у детей есть дети перезаполняем
                    createParent(checkParent, Row_id, folderclient, Nodes, convertIcon);

                    if (checkExistInTreeview != true)
                    {
                        insertTreeNodes(folderclient, Nodes);
                    }
                    else
                    {
                        UpdateSqlLiteNodesToTreeViewNodes(modelServer, Nodes);
                    }



                }
                else
                {
                    ClearLoadingNodes(modelServer, Nodes);
                }
            }
            catch(System.Windows.Markup.XamlParseException)
            {
                Console.WriteLine("TreeViewLoadingLazyController->UpdateNodesTreeView странная ошибка не ожиданно появилась");
            }
           

        }

      

        private void UpdateSqlLiteNodesToTreeViewNodes(FileInfoModel modelServer, ObservableCollection<FolderNodes> Nodes)
        {
            for (int h = 0; h < Nodes.Count; h++)
            {
                FolderNodes nodeTreeView = Nodes[h];

                if (modelServer.row_id == nodeTreeView.Row_id)
                {
                    nodeTreeView.Location = modelServer.location;
                    nodeTreeView.FolderName = modelServer.filename;
                    nodeTreeView.createDate = modelServer.createDate;
                    nodeTreeView.ParentID = modelServer.parent;
                    nodeTreeView.lastOpenDate = modelServer.lastOpenDate;
                    nodeTreeView.sizebyte = modelServer.sizebyte.ToString();
                    nodeTreeView.changeDate = modelServer.changeDate;
                }
            }
        }

        private void ClearLoadingNodes(FileInfoModel modelServer, ObservableCollection<FolderNodes> Nodes)
        {
            for (int h = 0; h < Nodes.Count; h++)
            {
                FolderNodes nodeTreeView = Nodes[h];

                if (nodeTreeView.FolderName.Equals("Загрузка"))
                {
                   
                    try
                    {
                        App.Current.Dispatcher.Invoke(delegate // <--- HERE
                        {
                            Nodes.RemoveAt(h);
                            
                        });


                    }
                    catch (System.InvalidOperationException ss)
                    {

                        Console.WriteLine("TreeViewLoadingLazyController -> ClearLoadingNodes " + ss);
                    }
                    catch (System.Windows.Markup.XamlParseException ss)
                    {

                        Console.WriteLine("TreeViewLoadingLazyController -> ClearLoadingNodes " + ss);
                    }
                    break;
                }

            }
        }
        //если такой нод уже есть, мы его не добавляем
        private bool SearchNodesInTreeView(FileInfoModel modelServer, ObservableCollection<FolderNodes> Nodes)
        {
            bool check = false;
            for(int h=0; h < Nodes.Count; h++)
            {
                FolderNodes nodeTreeView = Nodes[h];
                if(modelServer.row_id == nodeTreeView.Row_id)check = true;
            }

            return check;
        }

        private void createRootNodes(bool checkParent , long Row_id, FolderNodes folderclient , ConvertIconType convertIcon)
        {
            if(checkParent)
            {
                SortableObservableCollection<FolderNodes> listChildren = new SortableObservableCollection<FolderNodes>();
            
                //если disk и у него есть дети, загружаем их сразу
                if (folderclient.FolderName.Equals("Disk"))
                {
                    insertRootNodes(Row_id, listChildren, folderclient , convertIcon);
                }
            }
            
           
        }
        private void createParent(bool checkParent , long Row_id , FolderNodes folderclient , ObservableCollection<FolderNodes> Nodes , ConvertIconType convertIcon)
        {
            if (checkParent == true)
            {
                //если disk и у него есть дети, загружаем их сразу
                if (folderclient.FolderName.Equals("Disk") != true)
                {
                    if(folderclient.Children.Count == 0 )
                    {
                        insertLoadingNodes(folderclient, 0, Row_id , convertIcon);
                    }
                    
                }


            }
            Console.WriteLine();

        }

        private void expandRootNode(FolderNodes folderclient)
        {
            if (folderclient.FolderName.Equals("Disk") == true)
            {
                folderclient.IsExpanded = true;
                folderclient.IsSelected = true;
            }

        }

       

        private void insertRootNodes(long Row_id, SortableObservableCollection<FolderNodes> listChildren, FolderNodes folderclient , ConvertIconType convertIcon)
        {
            //для детей
            List<FileInfoModel> Children = _sqlitecont.getSelect().GetSqlLiteParentIDFileInfoList(Row_id);


            for (int a = 0; a < Children.Count(); a++)
            {
                //конвертация FileInfo to FolderNodes
                FileInfoModel modelServerChildren = Children[a];

                if (staticVariable.Variable.isFolder(modelServerChildren.type))
                {
                    //так же проверяем их на детей
                    bool checkChildren = _sqlitecont.getSelect().existSqlLiteParentNodes(Row_id);



                    string FolderNameChildren = modelServerChildren.filename;
                    long ParentIDChildren = modelServerChildren.parent;
                    string TypeChildren = modelServerChildren.type;
                    long VersionUpdateRowsChildren = modelServerChildren.versionUpdateRows;
                    long Row_idChildren = modelServerChildren.row_id;
                    string changeRowsChildren = modelServerChildren.changeRows;
                    string locationChildren = modelServerChildren.location;
                    string changeDateChildren = modelServerChildren.changeDate;
                    string createDateChildren = modelServerChildren.createDate;
                    long syzbytesChildren = modelServerChildren.sizebyte;
                    string lastOpenDateChildren = modelServerChildren.lastOpenDate;



                    FolderNodes folderclientChildren = _createModel.CreateFolderNodesModel(FolderNameChildren, ParentIDChildren, TypeChildren, VersionUpdateRowsChildren, Row_idChildren, changeRowsChildren, locationChildren, changeDateChildren, createDateChildren, syzbytesChildren, lastOpenDateChildren , convertIcon);



                    //если у root детей есть дети
                    if (checkChildren == true)
                    {
                        
                        insertLoadingNodes(folderclientChildren, VersionUpdateRowsChildren, Row_idChildren , convertIcon);
                    }

                    //Суммируем всех детей в коллекции
                    listChildren.Add(folderclientChildren);
                }

            }

            //полученных детей запихиваем в Disk
            folderclient.Children = listChildren;

      
        }

        //Вставляет заглушку для папок
        private void insertLoadingNodes(FolderNodes folder, long VersionUpdateRows, long Row_id , ConvertIconType convertIcon)
        {
            //Заглушка для папок
            SortableObservableCollection<FolderNodes> ChildrenCollection = new SortableObservableCollection<FolderNodes>();
        
            FolderNodes folderParent = _createModel.CreateFolderNodesModel("Загрузка", 1, "folder", VersionUpdateRows, Row_id, "OK", "Загрузка", "", "", 0, "" , convertIcon);
            ChildrenCollection.Add(folderParent);

            folder.Children = ChildrenCollection;

        }

        //folderclient - новый массив
        //RootNodes - текущий массив со всеми нодами
        //Добавляет Node в конец дерева
        private void insertTreeNodes(FolderNodes folderclient, ObservableCollection<FolderNodes> treeViewNode)
        {
            try
            {
                //проверка существует поток GUI
                if (System.Windows.Application.Current != null)
                {
                     //откуда запущен поток
                     Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);

                        if (dispatcher == null)
                        {
                             insertThread(folderclient, treeViewNode);
                        }
                        else
                        {
                             // App.Current.Dispatcher.Invoke(delegate // <--- HERE
                             // {
                              //   treeViewNode.Add(folderclient);
                                //});
                             insertThread(folderclient, treeViewNode);
                        }



                }
                else
                {
                    insert(folderclient, treeViewNode);
                }

            }
            catch (System.Windows.Markup.XamlParseException error)
            {
                Console.WriteLine("UpdateTreeViewNodes->insertTreeNodes Кривая ошибка обновления" + error);
            }
            
        }
        private void insert(FolderNodes folderclient, ObservableCollection<FolderNodes> treeViewNode)
        {
            treeViewNode.Add(folderclient);
        }
        private void insertThread(FolderNodes folderclient, ObservableCollection<FolderNodes> TreeViewNode)
        {
            try
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {
                    TreeViewNode.Add(folderclient);
                });


            }
            catch (System.InvalidOperationException ss)
            {

                Console.WriteLine("UpdateTreeViewNodes->insertTreeNodes " + ss);
            }
        }

      

      


      


    }
}
