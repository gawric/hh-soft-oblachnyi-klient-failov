﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.treeview;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using NewHH_SoftClientWPF.mvvm.model.treemodel.insertTreeViewModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.contoller.update.updatedata
{
    public class UpdateTreeViewNodes : IUpdateTreeView
    {
        private SqliteController _sqlitecont;
        private CreateModel _createModel;
        private ConvertIconType convertIcon;

        public UpdateTreeViewNodes(SqliteController sqllitecontroller, ConvertIconType convertIcon)
        {
            
            this._sqlitecont = sqllitecontroller;
            this.convertIcon = convertIcon;
            _createModel = new CreateModel(convertIcon);
        }
        //ленивая загрузка node в TreeView
        public void UpdateNodesLazyTreeView(List<FileInfoModel> listFiles, ObservableCollection<FolderNodes> TreeViewNode , ObservableCollection<FolderNodes> ListViewNode, long ParentID , ConvertIconType convertIcon)
        {
            try
            {
                //обновляет gui даже из друкого потока
                ClearTreeNodes(TreeViewNode);

                listFiles.ForEach(delegate (FileInfoModel modelServer)
                {
                    rootSearch(modelServer, TreeViewNode, ParentID, "TreeViewNodes" , convertIcon);
                });

            }
            catch(System.NotSupportedException h2)
            {
                Console.WriteLine("UpdateTreeViewNodes - > UpdateNodesLazy " + h2);
            }
           


        }
        
        //полное обновление
        //если пустое добавляем все что есть
        //если имеются Children мы делаем перезапись по каждому ноду
        public void Start(List<FileInfoModel> listFiles, ObservableCollection<FolderNodes> TreeViewNode , ObservableCollection<FolderNodes> ListViewNode , long[] ActiveNodeID , ConvertIconType convertIcon)
        {
            //если treeview является пустым
            if(TreeViewNode.Count == 0)
            {

                //Подмешивает root ноды если их нет в передаваемом куске нодов
                //повторно используем когда обновляются данные по расписанию, а не при рутовом запуске
                checkListRootNodes(listFiles);
                updateEmptyTreeViewNodes(listFiles , TreeViewNode , ListViewNode , convertIcon);
            }
            else
            {
                //Добавляем все FileInfoMode со статусом INSERT 
                //в наши открытые Ноды List и TreeView
                InsertRecursiveTreeViewNodes(listFiles , TreeViewNode , ActiveNodeID , convertIcon);
                
                //Сначало обновляем TreeView Удалять/переименовать/перенести
                UpdateRecursiveTreeViewNodes(TreeViewNode , convertIcon);
                
            }

        }

        private void checkListRootNodes(List<FileInfoModel> listFiles)
        {
            //root id номер мы не нашли
            if (isRootId(listFiles) != true)
            {
                //если в массиве нет корневых значений мы их находим в базе и заполняем в текущий массив
                //(пару функций с сервера не возращают корневые id)
                List<FileInfoModel> listSqlLite = _sqlitecont.getSelect().GetSqlLiteRootFileInfoList();
                listFiles.AddRange(listSqlLite);
            }
        }

        private bool isRootId(List<FileInfoModel> listFiles)
        {
            bool checkRootId = false;

            for (int h = 0; h < listFiles.Count; h++)
            {
                if (listFiles[h] != null)
                {
                    if (listFiles[h].row_id == -1)
                    {
                        checkRootId = true;

                    }
                }

            }

            return checkRootId;
        }
     
        private void UpdateRecursiveTreeViewNodes(ObservableCollection<FolderNodes> TreeViewNodes , ConvertIconType convertIcon)
        {
            ScannerRecursiveTreeView _Recursive = new ScannerRecursiveTreeView(_sqlitecont, TreeViewNodes);
           
            _Recursive.CallRecursive(TreeViewNodes , convertIcon);

        }
       

        


        //полное обновление всех данных используется после сканирования всего дерева SYNCREALALLLIST(запуск по расписанию)
        public void UpdateRecursiveReloadAllTreeView(ObservableCollection<FolderNodes> ListViewNode, ObservableCollection<FolderNodes> TreeViewNode, long[] ActiveID , ConvertIconType convertIcon)
        {
            //т.к биндится только рутовая папка ObservableList изменять и добавлять в нее нельзя,
            //можно добавлятб только детей в ее уровень добавить не даст!!!
            //если treeview является пустым
            if (TreeViewNode.Count == 0)
            {
                // startReloadALL(ListViewNode , TreeViewNode);
                //все данные из бд для root нодов поиск по -1
                List<FileInfoModel> listClient = _sqlitecont.getSelect().GetSqlLiteRootFileInfoListToFolder();
                
                //Подмешивает root ноды если их нет в передаваемом куске нодов
                //повторно используем когда обновляются данные по расписанию, а не при рутовом запуске
                checkListRootNodes(listClient);
                updateEmptyTreeViewNodes(listClient, TreeViewNode, ListViewNode , convertIcon);
            }
            else
            {
                ReloadAllTreeViewAndUpdateListView(ListViewNode, TreeViewNode, ActiveID , convertIcon);
            }

          
        }

        private void ReloadAllTreeViewAndUpdateListView(ObservableCollection<FolderNodes> ListViewNode, ObservableCollection<FolderNodes> TreeViewNode , long[] ActiveID , ConvertIconType convertIcon)
        {
            ReloadAllTreeView(TreeViewNode , convertIcon);
        }
        
        private void ReloadAllTreeView(ObservableCollection<FolderNodes> TreeViewNode , ConvertIconType convertIcon)
        {
            ScannerRecursiveReloadAllTreeView _Recursive = new ScannerRecursiveReloadAllTreeView(_sqlitecont);
            
            _Recursive.CallReloadALlRecursiveTreeView(TreeViewNode , convertIcon);

        }
        //Используем тот же класс т.к рекурсивно проходит все то же самое, что и treeview
        //просто проходит только 1 уровень
        private void ReloadAllListView(ObservableCollection<FolderNodes> ListViewNode , ConvertIconType convertIcon)
        {
            ScannerRecursiveReloadAllTreeView _Recursive = new ScannerRecursiveReloadAllTreeView(_sqlitecont);
           
            _Recursive.CallReloadALlRecursiveTreeView(ListViewNode , convertIcon);

        }


        //Добавляет значение в дерево tree
        private void InsertRecursiveTreeViewNodes(List<FileInfoModel> listFiles, ObservableCollection<FolderNodes> TreeViewNode , long[] ActiveNodeID , ConvertIconType convertIcon)
        {
            InsertTreeViewModel insertObjModel = new InsertTreeViewModel();

            insertObjModel.convertIcon = convertIcon;
            insertObjModel.searchDublication = new SearchDublication();

             ScannerInsertRecursiveView _RecursiveInsert = new ScannerInsertRecursiveView(_sqlitecont, listFiles);
            _RecursiveInsert.CallRecursiveTreeView(TreeViewNode  , insertObjModel);
            Console.WriteLine("***************************************************************************");
            Console.WriteLine("******UpdateTreeViewNodes->InsertRecursiveTreeView Завершенно успешно******");
            Console.WriteLine("***************************************************************************");
        }
     

        //если дерево пустое мы его перезаполняем
        private void updateEmptyTreeViewNodes(List<FileInfoModel> listFiles , ObservableCollection<FolderNodes> treeViewNode, ObservableCollection<FolderNodes>  ListViewNode , ConvertIconType convertIcon)
        {
            bool working_treeview = false;
            addRootNodes(listFiles, ref working_treeview, treeViewNode);

            clearDublication(treeViewNode);
            working_treeview = false;
        }

        private void addRootNodes(List<FileInfoModel> listFiles , ref bool working_treeview, ObservableCollection<FolderNodes> treeViewNode)
        {
            foreach(FileInfoModel modelServer in listFiles)
            {
                if (modelServer != null)
                {
                    add(modelServer, ref working_treeview, treeViewNode);
                }
            }
        }

        private void add(FileInfoModel modelServer , ref bool working_treeview , ObservableCollection<FolderNodes> treeViewNode)
        {
            // только -2 рутовое значение parent Disk -2
            if (modelServer.parent == -2)
            {

                if (working_treeview == false)
                {
                    working_treeview = true;
                    rootSearch(modelServer, treeViewNode, -2, "TreeViewNodes", convertIcon);
                }

            }
        }
        private void clearDublication(ObservableCollection<FolderNodes> TreeViewNode)
        {
            try
            {


                SearchDublication searchDublication = new SearchDublication();
                if (TreeViewNode != null)
                {
                    if (TreeViewNode.Count > 0)
                        searchDublication.clearDublication(TreeViewNode[0].Children);
                }


                searchDublication = null;
               
            }
            catch (System.ArgumentOutOfRangeException s)
            {
                Console.WriteLine("UpdateTreeViewNodes->UpdateEmptyTreeViewNodes: " + s);
            }
        }
        private void rootSearch(FileInfoModel modelServer , ObservableCollection<FolderNodes> Node, long searh , string UpdateListNodes , ConvertIconType convertIcon)
        {
            try
            {
                //-1 рутовое значение к нему закрпляются первичные папки
                if (modelServer.parent == searh)
                {
                    if (UpdateListNodes.Equals("TreeViewNodes"))
                    {

                        FolderNodes rootNodes = createRootNodes(modelServer, Node, searh , convertIcon);
       
                        bool checkParent = _sqlitecont.getSelect().existSqlLiteParentNodes(rootNodes.Row_id);
                        insert(rootNodes, checkParent, rootNodes.Row_id, rootNodes.VersionUpdateRows);
                        insertTreeNodes(rootNodes, Node);
                    }
                 

                }
            }
            catch(System.NullReferenceException s)
            {
                Console.WriteLine(s);
            }
           
        }

        private FolderNodes createRootNodes(FileInfoModel modelServer, ObservableCollection<FolderNodes> Nodes, long searh , ConvertIconType convertIcon)
        {
            FolderNodes folderclient = null ;
            if (staticVariable.Variable.isFolder(modelServer.type))
            {

                string FolderName = modelServer.filename;
                long ParentID = modelServer.parent;
                string Type = modelServer.type;
                long versionUpdateRows = modelServer.versionUpdateRows;
                long row_id = modelServer.row_id;
                string changeRows = modelServer.changeRows;
                string location = modelServer.location;
                string changeDate = modelServer.changeDate;
                string createDate = modelServer.createDate;
                long syzbytes = modelServer.sizebyte;
                string lastOpenDate = modelServer.lastOpenDate;

                folderclient = _createModel.CreateFolderNodesModel(FolderName, ParentID, Type, versionUpdateRows, row_id, changeRows, location, createDate, changeDate, syzbytes, lastOpenDate , convertIcon);

                if(folderclient.FolderName.Equals("Disk") == true)
                {
                    folderclient.IsExpanded = true;
                    folderclient.IsSelected = true;
                }
                
            }

            return folderclient;
        }

        private void insert(FolderNodes folderclient , bool checkParent , long row_id , long versionUpdateRows)
        {
            if (checkParent == true) goInsert(folderclient, versionUpdateRows, row_id);  
        }

        private void goInsert(FolderNodes folderclient , long versionUpdateRows , long row_id)
        {
            SortableObservableCollection<FolderNodes> listChildren = new SortableObservableCollection<FolderNodes>();
            //если disk и у него есть дети, загружаем их сразу
            if (folderclient.FolderName.Equals("Disk")) insertChildrenRootNodes(row_id, listChildren, folderclient, convertIcon);
            else
            {
                //если это не рутовая папка
                insertLoadingNodes(folderclient, versionUpdateRows, row_id, convertIcon);

            }
        }

        private void insertChildrenRootNodes(long Row_id , SortableObservableCollection<FolderNodes> listChildren , FolderNodes folderclient , ConvertIconType convertIcon)
        {
            //для детей
            List<FileInfoModel> children = _sqlitecont.getSelect().GetSqlLiteParentIDFileInfoList(Row_id);
           // List<SaveToPcModel> listPcSave = _sqlitecont.GetSqlLiteParentIDSavePcMinimal(-1);
           // setPcIconType(listPcSave, children);

            for (int a = 0; a < children.Count(); a++)
            {
                //конвертация FileInfo to FolderNodes
                FileInfoModel modelServerChildren = children[a];

                if (staticVariable.Variable.isFolder(modelServerChildren.type))
                {
                    //так же проверяем их на детей
                    bool checkChildren = _sqlitecont.getSelect().existSqlLiteParentNodes(Row_id);



                    string FolderNameChildren = modelServerChildren.filename;
                    long ParentIDChildren = modelServerChildren.parent;
                    string TypeChildren = modelServerChildren.type;
                    long VersionUpdateRowsChildren = modelServerChildren.versionUpdateRows;
                    long Row_idChildren = modelServerChildren.row_id;
                    string changeRowsChildren = modelServerChildren.changeRows;
                    string locationChildren = modelServerChildren.location;
                    string changeDateChildren = modelServerChildren.changeDate;
                    string createDateChildren = modelServerChildren.createDate;
                    long syzbytesChildren = modelServerChildren.sizebyte;
                    string lastOpenDateChildren = modelServerChildren.lastOpenDate;



                    FolderNodes folderclientChildren = _createModel.CreateFolderNodesModel(FolderNameChildren, ParentIDChildren, TypeChildren, VersionUpdateRowsChildren, Row_idChildren, changeRowsChildren, locationChildren, changeDateChildren, createDateChildren, syzbytesChildren, lastOpenDateChildren , convertIcon);



                    //если у root детей есть дети
                    if (checkChildren == true)insertLoadingNodes(folderclientChildren, VersionUpdateRowsChildren, Row_idChildren , convertIcon);
                    


                    //Суммируем всех детей в коллекции
                    listChildren.Add(folderclientChildren);
                }

            }

            //полученных детей запихиваем в Disk
            folderclient.Children = listChildren;
            
        }

     

        //Вставляет заглушку для папок
        private void insertLoadingNodes(FolderNodes folder , long VersionUpdateRows , long Row_id , ConvertIconType convertIcon)
        {
            //Заглушка для папок
            SortableObservableCollection<FolderNodes> ChildrenCollection = new SortableObservableCollection<FolderNodes>();
            FolderNodes folderParent = _createModel.CreateFolderNodesModel("Загрузка", 1, "folder", VersionUpdateRows, Row_id, "OK", "Загрузка", "", "", 0, "" , convertIcon);
            ChildrenCollection.Add(folderParent);

            folder.Children = ChildrenCollection;

        }

   
        

        //folderclient - новый массив
        //RootNodes - текущий массив со всеми нодами
        //Добавляет Node в конец дерева
        private void insertTreeNodes(FolderNodes folderclient, ObservableCollection<FolderNodes> TreeViewNode)
        {
            //проверка существует поток GUI
            if (System.Windows.Application.Current != null)
            {
                //откуда запущен поток
                Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);

                if (dispatcher == null)
                {


                    try
                    {
                        App.Current.Dispatcher.Invoke(delegate // <--- HERE
                        {
                            TreeViewNode.Add(folderclient);
                        });

                       
                    }
                    catch (System.InvalidOperationException ss)
                    {

                        Console.WriteLine("UpdateTreeViewNodes->insertTreeNodes "+ss);
                    }

                  
                }
                else
                {
                    TreeViewNode.Add(folderclient);
                }
               
                
     
            }
            else
            {
                TreeViewNode.Add(folderclient);
            }
        }

        private void ClearTreeNodes( ObservableCollection<FolderNodes> TreeViewNode)
        {
                Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);

                if (dispatcher == null)
                {
                    try
                    {
                        App.Current.Dispatcher.Invoke(delegate // <--- HERE
                        {
                            TreeViewNode.Clear();
                            
                        });


                    }
                    catch (System.InvalidOperationException ss)
                    {

                        Console.WriteLine(ss);
                    }
                
                }
                else
                {
                     TreeViewNode.Clear();
                }
                
        }

        private void setPcIconType(List<SaveToPcModel> listSavePc, List<FileInfoModel> listClient)
        {
            foreach (FileInfoModel item in listClient)
            {
                string type = getPcType(listSavePc, item.row_id, item.type);
                item.type = type;
            }
        }

        private string getPcType(List<SaveToPcModel> listSavePc, long row_id, string originalType)
        {
            foreach (SaveToPcModel item in listSavePc)
            {
                if (item.row_id_listfiles == row_id)
                {
                    originalType = item.type;
                    break;
                }
            }

            return originalType;
        }




    }
}
