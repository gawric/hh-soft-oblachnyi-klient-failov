﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.treeview;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.treeview
{
    public class InitializingTreeViewController
    {
        private IUpdateTreeView _updateTreeview;
        private StackUpdateTreeViewIcon _stackUpdateTreeViewIcon;

        public InitializingTreeViewController(SqliteController _sqlliteController, ConvertIconType convertIcon)
        {
            this._updateTreeview = new UpdateTreeViewNodes(_sqlliteController , convertIcon);
            _stackUpdateTreeViewIcon = new StackUpdateTreeViewIcon(convertIcon);
        }

        public void initializingTreeView(SqliteController _sqlliteController, ViewFolderViewModel _listViewFiles, MainWindowViewModel _viewModel , ConvertIconType convertIcon)
        {
            //все данные из бд для root нодов поиск по -1
            List<FileInfoModel> listClient = _sqlliteController.getSelect().GetSqlLiteRootFileInfoList();
            
         

            //коллекция для заполнения TreeView
            ObservableCollection<FolderNodes> NodesView = _viewModel.NodesView.Items;
            ObservableCollection<FolderNodes> ListViewFolder = _listViewFiles.NodesView.Items;

            //Обновляем TreeViewNodes -> items
          
            _updateTreeview.Start(listClient, NodesView, ListViewFolder, _viewModel.ActiveNodeID , convertIcon);

            listClient = null;
        }


      
    }
}
