﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.treeview
{
    interface IUpdateTreeView
    {
        //Полное обновление всего дерева с данными
        void UpdateRecursiveReloadAllTreeView(ObservableCollection<FolderNodes> ListViewNode, ObservableCollection<FolderNodes> TreeViewNode, long[] ActiveID, ConvertIconType convertIcon);

        //Обновляет при запуске приложения и когда приходят новые данные из сети
        void Start(List<FileInfoModel> listFiles, ObservableCollection<FolderNodes> TreeViewNode, ObservableCollection<FolderNodes> ListViewNode, long[] ActiveNodeID, ConvertIconType convertIcon);

    }
}
