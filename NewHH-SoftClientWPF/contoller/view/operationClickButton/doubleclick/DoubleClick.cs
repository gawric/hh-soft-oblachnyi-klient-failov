﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationClickButton.doubleclick
{
    public class DoubleClick
    {
        private SqliteController _sqlLiteController;
        private MainWindowViewModel _viewModelMain;
        private WebDavClientController _webDavClient;
        private ViewFolderViewModel _viewModel;
        private UpdateFormMainController _updateMainForm;
        private NavigationMainWindowsController _navigationMainWindows;

        public DoubleClick(Container cont)
        {
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _viewModelMain = cont.GetInstance<MainWindowViewModel>();
            _viewModel = cont.GetInstance<ViewFolderViewModel>();
            _webDavClient = cont.GetInstance<WebDavClientController>();
            _updateMainForm = cont.GetInstance<UpdateFormMainController>();
            _navigationMainWindows = cont.GetInstance<NavigationMainWindowsController>();

        }

        public async void startDoubleCLick(FolderNodes selectNodes, ObservableCollection<FolderNodes> fol)
        {
            insertPg(5);

            long CurrentRowId = insertActiveNodeIDToViewModel(selectNodes);
            searchPathToNodes(CurrentRowId, fol);
            
            insertPg(10);

            waitingLoad();

            insertPg(25);

            //lazy update
            _updateMainForm.updateMainLazyListView(CurrentRowId.ToString(), selectNodes.Children);

        }

        private void waitingLoad()
        {
            int b = 0;
            while (true)
            {
                if (_navigationMainWindows.isOpenNode() == true)
                {
                    break;
                }

                if (b >= 30)
                {
                    Console.WriteLine("ViewFolder->ListView_MouseDoubleClick: Ждем открытие нода TreeView " + b);
                    break;
                }

                b++;
                Thread.Sleep(50);
            }
        }

        private long insertActiveNodeIDToViewModel(FolderNodes selectNodes)
        {
            //его ид номер
            long CurrentRowId = selectNodes.Row_id;
            //зписываем в переменную для обмена
            _viewModel.ActiveViewFolderNodeID[0] = CurrentRowId;

            return CurrentRowId;
        }

        private void searchPathToNodes(long CurrentRowId , ObservableCollection<FolderNodes> fol)
        {
            List<FileInfoModel> _listPath2 = new List<FileInfoModel>();

            _navigationMainWindows.setOpenNode(false);
            //путь до папки в treeview
            _navigationMainWindows.getPathViewFolder(CurrentRowId, _listPath2);

            BackNodesModel insert = new BackNodesModel();
            //перемещаемся по дереву до нужной папки
            _navigationMainWindows.SearchTreeView2(fol, _listPath2 , insert);
            //очищаем 
            _listPath2.Clear();
        }

        private void insertPg(double procent)
        {
            _viewModelMain.pg = procent;
        }
    }
}
