﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.navigation.mainwindows.support
{
    public class StatusOpenNode
    {
        //статус открыли мы узел выбранный пользователем или нет, нужен что-бы listview открывался синхронно с TreeView
        public bool isOpenNode { get; set; }
        public bool isNewOpenNode { get; set; }
    }
}
