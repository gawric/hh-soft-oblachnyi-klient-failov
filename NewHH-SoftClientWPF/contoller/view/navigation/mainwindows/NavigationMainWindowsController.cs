﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows.support;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.navigation.mainwindows
{
    public class NavigationMainWindowsController
    {
        //работа с кэшем в базе данных
        private SqliteController _sqlLiteController;
     

        //содержит временный список всех нодов от начала до конца
        //0 - name 
        //1 - row_id
        private List<string[]> _listPath;
        private MainWindowViewModel _viewModel;
        private ViewFolderViewModel _viewModelViewFolder;
        private NavigationMainSupport _navigationSupport;
        private SearchTreeViewNavigation _navigationTreeView;
        private SearchTreeView2Navigation _navigationTreeView2;
        private StatusOpenNode _statusOpenNode;
        private SearchNextPosition _seatchNextPosition;
      

        

        public NavigationMainWindowsController(Container cont)
        {
            _listPath = new List<string[]>();
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _viewModelViewFolder = cont.GetInstance<ViewFolderViewModel>();

            _statusOpenNode = new StatusOpenNode();
            _navigationTreeView = new SearchTreeViewNavigation(_listPath , _statusOpenNode);
            _seatchNextPosition = new SearchNextPosition(_statusOpenNode , _viewModel , _viewModelViewFolder);
            _navigationTreeView2 = new SearchTreeView2Navigation(_navigationTreeView , _statusOpenNode , _seatchNextPosition);
            _navigationSupport = new NavigationMainSupport(_listPath , _navigationTreeView);
          


        }


        int[]ind = { 0 };

        //производит поиск root папки
        public void SearchTreeView(TreeView _treeView)
        {

            CheckMinus1();

   
            try
            {
                ItemCollection foundItem = _treeView.Items;
  
                if (foundItem != null)
                {


                    for(int e = 0; e < foundItem.Count; e++)
                    {
                        FolderNodes item = (FolderNodes)foundItem[e];
                        _navigationSupport.FolderRootScan( item, _listPath, ind);
                    }

                }
                
                _listPath.Clear();
            }
            catch (System.InvalidOperationException k5)
            {
                Console.WriteLine("NavigationMainWIndowsController -> SearchTreeView: Ошибка чтение массива ");
                Console.WriteLine(k5.Message);
            }
            catch (System.ArgumentOutOfRangeException k6)
            {
                Console.WriteLine("NavigationMainWIndowsController -> SearchTreeView: Ошибка чтение массива ");
                Console.WriteLine(k6);
            }
     

        }

        private void CheckMinus1()
        {
            //что-бы случайно не обратиться к обьекту -1 в массиве
            //и не выдал ошибку, что такого индекса не существует
            if (_listPath.Count == 0)
            {
                if (_listPath.Count <= 0)
                {
                    ind[0] = 0;
                }
                else
                {
                    ind[0] = _listPath.Count;
                }

            }
            else
            {
                ind[0] = _listPath.Count - 1;
            }
        }

        
        int[] l2 = { 0 };
        

        public void SearchTreeView2(ObservableCollection<FolderNodes> _list , List<FileInfoModel> _listPath2 , BackNodesModel insert)
        {
            insert.isFinish = false;
            _navigationTreeView2.SearchTreeView2(_list, _listPath2, l2, ind , insert);
          
        }
       
       

        //поиск полного 
        public void getPath(long row_id)
        {

            FileInfoModel model = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_id);

            if (model != null)
            {
               

                try
                {
                    if (model.row_id == -1)
                    {
                        Console.WriteLine("NavigationMainWIndowsController->getPath->Поиск закончен полного пути к ноду " + model.filename);

                    }
                    else
                    {
                        string[] item = { model.filename, model.row_id.ToString() };
                        _listPath.Add(item);
                        getPath(model.parent);
                    }
                }
                catch(System.ArgumentOutOfRangeException lm)
                {
                    Console.WriteLine("NavigationMainWindowsController->getPath Критическая ошибка ");
                    Console.WriteLine(lm.ToString());

                }
                
            }
        }

        //поиск полного пути
        public void getPathViewFolder(long row_id , List<FileInfoModel>_list)
        {

            FileInfoModel model = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_id);

            if (model != null)
            {
                //Console.WriteLine("Нашли нод в уровне " + model.filename);


                if (model.row_id == -1)
                {
                    Console.WriteLine("NavigationMainWIndowsController->getPathViewFolder: Поиск закончен полного пути к ноду " + model.filename);

                }
                else
                {
                    _list.Add(model);
                    getPathViewFolder(model.parent , _list);
                }
            }
            
        }

        public void SetSelect(ref List<long> listSelectNodes, FolderNodes fol , FolderNodes unSelectedFolderNodes)
        {
            try
            {
                listSelectNodes.Add(fol.Row_id);
            }
            catch (System.IndexOutOfRangeException g6)
            {
                Console.WriteLine(g6.ToString());
            }

            Console.WriteLine();
        }

      
        public async Task StartSelect(BackNodesModel insert , List<long> listSelectNodes)
        {

            int index = getIndexSelectBack(listSelectNodes, insert);

            long itemRow_id = getRowIdListSelect(listSelectNodes, index);
            FileInfoModel fim = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(itemRow_id);
           
            if (fim == null) return;

            List<FileInfoModel> listPath2 = getPatchFileInfoModel(fim);
            SortableObservableCollection<FolderNodes> items = getAllNodesTreeView(_viewModel);

          
            searchSelectBack(items, insert, listPath2);
            listPath2.Clear();
        }


        private long getRowIdListSelect(List<long> listSelectNodes , int index)
        {
            try
            {
                return listSelectNodes[index];
            }
            catch(System.ArgumentOutOfRangeException a)
            {
               
                Console.WriteLine("NavigationMainWindowsController-> getRowIdListSelect: "+a.ToString());
                return 0;
            }
           
        }

        private int getIndexSelectBack(List<long> listSelectNodes , BackNodesModel insert)
        {
            if (listSelectNodes.Count() == 0) return 0;

            int index = listSelectNodes.Count() + --insert.index;
            
            return index;
        }

        private int getIndexSelectForward(List<long> listSelectNodes, BackNodesModel insert)
        {
            int index = listSelectNodes.Count() + ++insert.index;
            if (insert.index >= listSelectNodes.Count) index = listSelectNodes.Count;
            
            return index;
        }

        private SortableObservableCollection<FolderNodes> getAllNodesTreeView(MainWindowViewModel _viewModel)
        {
            return  _viewModel.NodesView.Items;
        }
        private void searchSelectForward(SortableObservableCollection<FolderNodes> items , BackNodesModel insert , List<FileInfoModel> listPath2)
        {
            insert.isSelectForward = true;
            SearchTreeView2(items, listPath2 , insert);
        }

        private void searchSelectBack(SortableObservableCollection<FolderNodes> items, BackNodesModel insert, List<FileInfoModel> listPath2)
        {
            insert.isSelectBack = true;
            SearchTreeView2(items, listPath2 , insert);
        }
        private List<FileInfoModel>  getPatchFileInfoModel(FileInfoModel fim)
        {
            List<FileInfoModel> listPath2 = new List<FileInfoModel>();
            getPathViewFolder(fim.row_id, listPath2);

            return listPath2;
        }





        public async Task StartSelect2(BackNodesModel insert, List<long> listSelectNodes)
        {
            int index = getIndexSelectForward(listSelectNodes, insert);

            long itemRow_id = getRowIdListSelect(listSelectNodes, index);
            FileInfoModel fim = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(itemRow_id);

            if (fim == null) return;

            List<FileInfoModel> listPath2 = getPatchFileInfoModel(fim);
            SortableObservableCollection<FolderNodes> items = getAllNodesTreeView(_viewModel);
            searchSelectForward(items, insert, listPath2);
            listPath2.Clear();
        }


        //Выполянет расчет, куда переместиться по массиву вперед
        public void StartForward(ref BackNodesModel insert , List<long> listSelectNodes, SupportEventPart2MainForm mainPart2)
        {
            try
            {
                if (listSelectNodes == null) return;
                checkIsFinalForward(ref insert, ref mainPart2);
                SelectNodes2(insert, listSelectNodes);

            }
            catch (System.ArgumentOutOfRangeException z)
            {
                Console.WriteLine(z);
            }
        }

        public void StartBack(ref BackNodesModel insert , List<long> listSelectNodes,  SupportEventPart2MainForm mainPart2)
        {
          

            try
            {

                if (listSelectNodes == null) return;


                checkIsFinalBack(ref insert , ref mainPart2);
             
                mainPart2.isDisabledButtonForward(false);
               

                SelectNodes(insert, listSelectNodes);


            }
            catch (System.ArgumentOutOfRangeException z)
            {
                Console.WriteLine("NavigationMainWindowsController -> StartBack");
                Console.WriteLine(z);
            }
        }

        private void checkIsFinalBack(ref BackNodesModel insert , ref SupportEventPart2MainForm mainPart2)
        {
            int ff = insert.index;

            if (_viewModel.getListSelectNodes().Count + --ff <= 0)
            {
                //достигли конца массива
                if (_viewModel.getListSelectNodes().Count + ff == 0)
                {
                    mainPart2.isDisabledButtonBack(true);
                }
                else
                {
                    //пытаемся переместиться ниже конца т.е меньше 0
                    return;
                }

            };
        }

        private void checkIsFinalForward(ref BackNodesModel insert, ref SupportEventPart2MainForm mainPart2)
        {
            int ff = insert.index;
        
            if (++ff >= -1)
            {
               mainPart2.isDisabledButtonForward(true);
            }
            else
            {
                mainPart2.isDisabledButtonBack(false);
            }
        }



        //Для кнопки назад
        private void SelectNodes( BackNodesModel insert, List<long> listSelectNodes)
        {
         
            Task.Run(async () =>
            {
                StartSelect(insert, listSelectNodes).Wait();
            });
        }
        //Для кнопки вперед
        private void SelectNodes2(BackNodesModel insert, List<long> listSelectNodes)
        {
            Task.Run(async () =>
            {
               
                StartSelect2(insert, listSelectNodes).Wait();
            });
        }

        public bool isOpenNode()
        {
            return _statusOpenNode.isOpenNode;
        }

        public void setOpenNode(bool openNodes)
        {
            _statusOpenNode.isOpenNode = openNodes;
        }

        public bool isNewOpenNode()
        {
            return _statusOpenNode.isNewOpenNode;
        }

        public void setNewOpenNode(bool newOpenNodes)
        {
            _statusOpenNode.isNewOpenNode = newOpenNodes;
        }
    }


}
