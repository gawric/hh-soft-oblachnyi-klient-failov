﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack.updateIconSync
{
    public interface IUpdateALLIcon
    {
        void UpdateAllRefresh(ref SyncSinglListModel model, FileInfoModel parentModel);
        void UpdateAllNormal(ref SyncSinglListModel model, FileInfoModel parentModel);
    }
}
