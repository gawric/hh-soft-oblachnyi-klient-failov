﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.contoller.listview
{
    public class UpdateListViewNodes: IUpdateListView
    {
        private SqliteController _sqlitecont;
        private CreateModel createModel;
 

        public UpdateListViewNodes(SqliteController _sqlitecont , ConvertIconType convertIcon)
        {
            createModel = new CreateModel(convertIcon);
            this._sqlitecont = _sqlitecont;
        }
        public void Start(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> TreeViewNode, SortableObservableCollection<FolderNodes> ListViewNode, long[] ActiveNodeID, ConvertIconType convertIcon)
        {
            //если treeview является пустым НЕ МЕНЯТЬ т.к мы оринтируемся по дереву а не по LISTVIEW!!! 
            //т.к дерево может иметь детей а listVIEW может быть пустым!!!
            if (TreeViewNode.Count == 0)
            {
               
                //Подмешивает root ноды если их нет в передаваемом куске нодов
                //повторно используем когда обновляются данные по расписанию, а не при рутовом запуске
                CheckListRootNodes(listFiles);
                UpdateEmptyListViewNodes(listFiles, ListViewNode, convertIcon);
            }
            else
            {
                //Потом обновляем ListView Удалять/переименовать/перенести
                UpdateRecursiveListViewNodes(ListViewNode, convertIcon);

                InsertRecursiveListFileNodes(listFiles, ListViewNode, ActiveNodeID ,  convertIcon);

            }
        }
        public void UpdateEmptyListViewNodes(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> ListViewNode, ConvertIconType convertIcon)
        {
            SortedListToType sorted = new SortedListToType();
            //Сортировка перед проверками
            listFiles = sorted.sortedType(listFiles);
            listFiles.ForEach(delegate (FileInfoModel modelServer){ if(modelServer != null) updateLazyListView(modelServer, ListViewNode, -1, convertIcon); });
        }

        //ленивая загрузка node в ListView
        public SortableObservableCollection<FolderNodes> UpdateNodesLazyListView(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> ListViewNode, long ParentID, MainWindowViewModel _viewModel, ConvertIconType convertIcon)
        {
            try
            {

                clearListView(ListViewNode);

                updatePgViewModel(_viewModel, 65);

                listFiles.ForEach(delegate (FileInfoModel modelServer)
                {
                    updateLazyListView(modelServer, ListViewNode, ParentID, convertIcon);
                    _viewModel.pg = _viewModel.pg + 1;
                });

                //очистка от дублей если они попали в ListView
                new SearchDublication().clearDublication(ListViewNode);
                updatePgViewModel(_viewModel , 100);

                return ListViewNode;
            }
            catch (System.Windows.Markup.XamlParseException a)
            {
                Console.WriteLine("ObserverUpdateTreeView->updateView->LazyListView " + a);
                return new SortableObservableCollection<FolderNodes>();
            }

        }

        private void updatePg()
        { 
        }
        private void updatePgViewModel(MainWindowViewModel _viewModel , double procent)
        {
            _viewModel.pg = procent;
        }
        private void clearListView(SortableObservableCollection<FolderNodes> ListViewNode)
        {
            //TreeViewNode.Clear();
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                ListViewNode.Clear();
            });
        }

        private void updateLazyListView(FileInfoModel modelServer, SortableObservableCollection<FolderNodes> Node, long searh, ConvertIconType convertIcon)
        {
            try
            {
                //-1 рутовое значение к нему закрпляются первичные папки //Обновляет ListView
                if (modelServer.parent == searh) UpdateNodesListView(modelServer, Node, searh, convertIcon);
            }
            catch (System.NullReferenceException s)
            {
                Console.WriteLine(s);
            }

        }


        private void UpdateNodesListView(FileInfoModel modelServer, SortableObservableCollection<FolderNodes> Nodes, long searh, ConvertIconType convertIcon)
        {

            if (staticVariable.Variable.isFolder(modelServer.type))
            {

                string FolderName = modelServer.filename;
                long ParentID = modelServer.parent;
                string Type = modelServer.type;
                long VersionUpdateRows = modelServer.versionUpdateRows;
                long Row_id = modelServer.row_id;
                string changeRows = modelServer.changeRows;
                string location = modelServer.location;
                string changeDate = modelServer.changeDate;
                string createDate = modelServer.createDate;
                long syzbytes = modelServer.sizebyte;
                string lastOpenDate = modelServer.lastOpenDate;

                FolderNodes folderclient = createModel.CreateFolderNodesModel(FolderName, ParentID, Type, VersionUpdateRows, Row_id, changeRows, location, createDate, changeDate, syzbytes, lastOpenDate, convertIcon);

                //проверяет есть ли дети у данной папки дети
                //bool checkParent = _sqlitecont.getSelect().existSqlLiteParentNodes(Row_id);


                insertListViewNodes(folderclient, Nodes);
              
            }
            else
            {

                string FolderName = modelServer.filename;
                long ParentID = modelServer.parent;
                string Type = modelServer.type;
                long VersionUpdateRows = modelServer.versionUpdateRows;
                long Row_id = modelServer.row_id;
                string changeRows = modelServer.changeRows;
                string location = modelServer.location;
                string changeDate = modelServer.changeDate;
                string createDate = modelServer.createDate;
                long syzbytes = modelServer.sizebyte;
                string lastOpenDate = modelServer.lastOpenDate;

                FolderNodes folderclient = createModel.CreateFolderNodesModel(FolderName, ParentID, Type, VersionUpdateRows, Row_id, changeRows, location, createDate, changeDate, syzbytes, lastOpenDate, convertIcon);


                insertListViewNodes(folderclient, Nodes);

            }



        }

       
        

        //folderclient - новый массив
        //RootNodes - текущий массив со всеми нодами
        //Добавляет Node в конец дерева
        private void insertListViewNodes(FolderNodes folderclient, SortableObservableCollection<FolderNodes> ListViewNode)
        {
            try
            {
                //проверка из какого потока запускается данный скрипт
                if (System.Windows.Application.Current != null)
                {
                    App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                    {
                        try
                        {

                            ListViewNode.Add(folderclient);
                        }
                        catch (System.InvalidOperationException ss)
                        {
                            Console.WriteLine("UpdateListViewNodes->insertListViewNodes: Критическая ошибка " + ss);
                        }
                        catch (System.Windows.Markup.XamlParseException ss)
                        {
                            Console.WriteLine(ss);
                        }

                    });
                }
                else
                {

                    ListViewNode.Add(folderclient);
                }

            }
            catch (System.Windows.Markup.XamlParseException a)
            {
                Console.WriteLine(a.ToString());
            }

        }


        

        private void CheckListRootNodes(List<FileInfoModel> listFiles)
        {
            bool checkRootId = false;
            checkRootId = isRoot(ref listFiles, ref checkRootId);
            addRangeToBases(ref checkRootId, ref listFiles);
        }
        private bool isRoot(ref List<FileInfoModel> listFiles , ref bool checkRootId)
        {
            for (int h = 0; h < listFiles.Count; h++)
            {
                if (listFiles[h] != null)
                {
                    if (listFiles[h].row_id == -1)
                    {
                        checkRootId = true;
                    }
                }

            }

            return checkRootId;
        }
        private void addRangeToBases(ref bool checkRootId , ref List<FileInfoModel> listFiles)
        {
            //root id номер мы не нашли
            if (checkRootId != true)
            {
                //если в массиве нет корневых значений мы их находим в базе и заполняем в текущий массив
                //(пару функций с сервера не возращают корневые id)
                List<FileInfoModel> listSqlLite = _sqlitecont.getSelect().GetSqlLiteRootFileInfoList();
                listFiles.AddRange(listSqlLite);
            }
        }

        private void UpdateRecursiveListViewNodes(SortableObservableCollection<FolderNodes> ListViewNodes , ConvertIconType convertIcon)
        {
            ScannerRecursiveTreeView _Recursive = new ScannerRecursiveTreeView(_sqlitecont, ListViewNodes);
           

            _Recursive.CallRecursive(ListViewNodes, convertIcon);
           // Console.WriteLine("UpdateListViewNodes->UpdateRecursiveListViewNodes Завершенно успешно");
        }

        //Добавляет значение в форму listview
        private void InsertRecursiveListFileNodes(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> ListViewNode, long[] ActiveNodeID, ConvertIconType convertIcon)
        {
            ScannerInsertRecursiveView _RecursiveInsert = new ScannerInsertRecursiveView(_sqlitecont, listFiles);
            

            _RecursiveInsert.insertNodesListView(ListViewNode, ActiveNodeID, convertIcon);
        }

        //полное обновление всех данных используется после сканирования всего дерева SYNCREALALLLIST(запуск по расписанию)
        public void UpdateRecursiveReloadAllListView(SortableObservableCollection<FolderNodes> ListViewNode, SortableObservableCollection<FolderNodes> TreeViewNode, long[] ActiveID, ConvertIconType convertIcon)
        {
            //т.к биндится только рутовая папка ObservableList изменять и добавлять в нее нельзя,
            //можно добавлятб только детей в ее уровень добавить не даст!!!
            //если treeview является пустым
            if (TreeViewNode.Count == 0)
            {
                // startReloadALL(ListViewNode , TreeViewNode);
                //все данные из бд для root нодов поиск по -1
                List<FileInfoModel> listClient = _sqlitecont.getSelect().GetSqlLiteRootFileInfoListToFolder();



                //Подмешивает root ноды если их нет в передаваемом куске нодов
                //повторно используем когда обновляются данные по расписанию, а не при рутовом запуске
                CheckListRootNodes(listClient);
                UpdateEmptyListViewNodes(listClient, ListViewNode, convertIcon);
            }
            else
            {
                ReloadAllUpdateListView(ListViewNode, TreeViewNode, ActiveID, convertIcon);
            }


        }

        private void ReloadAllUpdateListView(ObservableCollection<FolderNodes> ListViewNode, ObservableCollection<FolderNodes> TreeViewNode, long[] ActiveID, ConvertIconType convertIcon)
        {
            //Потом обновляем ListView Удалять/переименовать/перенести
            UpdateListView(ListViewNode, ActiveID, convertIcon);
        }

        private void UpdateListView(ObservableCollection<FolderNodes> ListViewNodes, long[] ActiveID, ConvertIconType convertIcon)
        {
            ScannerReloadAllListView _ListViewUpdate = new ScannerReloadAllListView(_sqlitecont);
            _ListViewUpdate.ReloadAllListView(ListViewNodes, ActiveID, convertIcon);
        }

        
    }


}
