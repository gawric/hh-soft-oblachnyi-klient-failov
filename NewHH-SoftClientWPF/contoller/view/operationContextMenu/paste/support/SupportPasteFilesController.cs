﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support
{
    class SupportPasteFilesController
    {
        SupportPasteControllerAllFunction otherFunction;

        public SupportPasteFilesController()
        {
            otherFunction = new SupportPasteControllerAllFunction();
        }

        public void getDataFiles(UpdateNetworkJsonController updateNetworkJsonController, long newVersionBases, string username, string password, SupportServerTopicModel supportTopicModel, SqliteController _sqlLiteController, string Event, string type, string copyFolderName, string copyLocation, FolderNodes pasteFolder, IProgress<CopyViewModel> progress, CopyViewModel status, long[][] allChildrenList)
        {

            //создание копии с новыми id
            //получаем список всех детей arr[0] - arr2[0] - id
            //                                    arr2[1] - isFolder
            //                                    arr2[2] - newId
            //                                    arr2[3] - oldParent
            for (int k = 0; k < allChildrenList.Length; k++)
            {

                if (k == 0)
                {
                    //присваиваем новый row_id
                    long new_id = _sqlLiteController.getSelect().getLastRow_idFileInfoModel() + 1;
                    long oldId = allChildrenList[k][0];


                    allChildrenList[k] = otherFunction.addPasteFirstNewId(new_id, allChildrenList[k]);
                    //0 - текущий выбранный элемент ему присваиваем parent(Вставляемого обьекта)
                    allChildrenList[k][3] = pasteFolder.Row_id;
                }
                else
                {

                    allChildrenList[k] = otherFunction.addPasteNewId(allChildrenList[k], allChildrenList[k - 1]);
                    long oldId = allChildrenList[k][0];

                    //parent_id из базы присваиваем long массиву
                    allChildrenList[k][3] = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(oldId).parent;
                }

            }

            status.StatusWorker = "Создание новых файлов";
            progress.Report(status);
            //теперь присваиваем им новые parent т.к все id номера поменялись
            //создание копии с новыми id
            //получаем список всех детей arr[0] - arr2[0] - id
            //                                    arr2[1] - isFolder
            //                                    arr2[2] - newId
            //                                    arr2[3] - newParent
            for (int k = 0; k < allChildrenList.Length; k++)
            {
                // 0 -элемет самый первый мы его не трогаем только меняем ему id в прошлой итерации и уже присвоили нормальный парент id
                if (k == 0)
                {
                    //0 - это текущий выбранный элемент т.е copy
                }
                else
                {

                    // allChildrenList[k] = addPasteNewId(allChildrenList[k], allChildrenList[k - 1]);
                    long oldParent = allChildrenList[k][3];
                    long[] serchChild = otherFunction.serachOldId(allChildrenList, oldParent);
                    allChildrenList[k][3] = serchChild[2];
                }

            }
        }

    }
}
