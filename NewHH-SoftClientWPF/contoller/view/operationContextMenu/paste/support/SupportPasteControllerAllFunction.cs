﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support
{
    public class SupportPasteControllerAllFunction
    {
        public CopyViewModel CreateIProgressPasteFolder(PasteFilesAsyncModel PasteModel)
        {
            

            string copyFolderName = PasteModel.copyFolderName;
            IProgress<CopyViewModel> progress = PasteModel.progress;
            string folderName = PasteModel.pasteFolder.FolderName;

            //Передает отчет о ходе выполнения
            PasteModel.status = new CopyViewModel();
            PasteModel.status.title = "Копирование папки";
            PasteModel.status.whence = folderName;

            //statusWorker - init
            //where - copyFolderName
            updateInitProgressStatus(PasteModel.status, "init", copyFolderName);
            updateRunningProgressStatus(PasteModel , "Поиск файлов");

            return PasteModel.status;
        }


        public CopyViewModel CreateIProgressDeleteFolder(DeleteFilesAsyncModel DeleteModel,  string folderName)
        {
            //Передает отчет о ходе выполнения
            DeleteModel.status = new CopyViewModel();
            DeleteModel.status.title = "Удаление папки";
            DeleteModel.status.whence = folderName;

            //statusWorker - init
            //where - copyFolderName
            updateInitProgressStatus(DeleteModel.status, "init", "non");
            updateRunningDeleteProgressStatus(DeleteModel , "Поиск файлов");

            return DeleteModel.status;
        }



        public CopyViewModel CreateIProgressMoveFolder( PasteFilesAsyncModel PasteModel)
        {
            string folderName = PasteModel.pasteFolder.FolderName;
            string copyFolderName = PasteModel.copyFolderName;

            //Передает отчет о ходе выполнения
            PasteModel.status = new CopyViewModel();
            PasteModel.status.title = "Перенос папки";
            PasteModel.status.whence = folderName;

            //statusWorker - init
            //where - copyFolderName
            updateInitProgressStatus(PasteModel.status, "init", copyFolderName);
            updateRunningProgressStatus(PasteModel , "Поиск файлов");

            return PasteModel.status;
        }

        public CopyViewModel CreateIProgressPasteFiles(CopyViewModel status, PasteFilesAsyncModel PasteModel, IProgress<CopyViewModel> progress)
        {
            string folderName = PasteModel.pasteFolder.FolderName;
            string copyFolderName = PasteModel.copyFolderName;

            //Передает отчет о ходе выполнения
            status = new CopyViewModel();
            status.title = "Копирования файла";
            status.whence = folderName;

            //statusWorker - init
            //where - copyFolderName
            updateInitProgressStatus(status, "init", copyFolderName);
            updateRunningProgressStatus(PasteModel ,  "Отправка файлов на сервер");


            return status;
        }

        public CopyViewModel CreateIProgressMoveFiles(CopyViewModel status, PasteFilesAsyncModel PasteModel, IProgress<CopyViewModel> progress)
        {
            string folderName = PasteModel.pasteFolder.FolderName;
            string copyFolderName = PasteModel.copyFolderName;
            //Передает отчет о ходе выполнения
            status = new CopyViewModel();
            status.title = "Перенос файла";
            status.whence = folderName;

            //statusWorker - init
            //where - copyFolderName
            updateInitProgressStatus(status, "init", copyFolderName);
            updateRunningProgressStatus(PasteModel, "Отправка файлов на сервер");



            return status;
        }


        public void WaitingTransaction(StatusSqlliteTransaction _statusTransaction)
        {
            //проверяем что другие потоки не пишут в нашу базу дынных
            int v = 0;
            while (true)
            {
                Console.WriteLine("PasteFilesController->WaitingTransaction: Ждем освобождения для записи   " + v);
                Thread.Sleep(10);
                if (_statusTransaction.isTransaction == false)
                {
                    break;
                }

                if (v == 300)
                {
                    break;
                }
                v++;
            }
        }

        public void updateInitProgressStatus(CopyViewModel status, string StatusWorker, string where)
        {
            status.where = where;
            status.StatusWorker = "init";
        }

        public void updateRunningProgressStatus(PasteFilesAsyncModel PasteModel , string StatusWorker)
        {
            PasteModel.status.StatusWorker = StatusWorker;
            PasteModel.progress.Report(PasteModel.status);
        }

        public void updateRunningDeleteProgressStatus(DeleteFilesAsyncModel DeleteModel, string StatusWorker)
        {
            DeleteModel.status.StatusWorker = StatusWorker;
            DeleteModel.progress.Report(DeleteModel.status);
        }

        public long[] addPasteNewId(long[] currentElemetns, long[] lastModelServer)
        {
            //текущая модель
            long[] modelServer = currentElemetns;

            long[] newModelServer = new long[4];
            // - oldId
            newModelServer[0] = modelServer[0];
            // - oldisFolder
            newModelServer[1] = modelServer[1];
            //новый id номер (прошлый id + 1)
            newModelServer[2] = lastModelServer[2] + 1;

            return newModelServer;
        }

        public long[] addPasteFirstNewId(long new_id, long[] currentElemtns)
        {
            //текущая модель
            long[] modelServer = currentElemtns;
            long[] newModelServer = new long[4];

            newModelServer[0] = modelServer[0];
            newModelServer[1] = modelServer[1];

            //новый id номер (прошлый id + 1)
            newModelServer[2] = new_id;

            return newModelServer;

        }

        public long[] serachOldId(long[][] allChildren, long oldParent)
        {
            long[] currentOldId = null;

            for (int g = 0; g < allChildren.Length; g++)
            {
                long oldId = allChildren[g][0];
                if (oldParent == oldId)
                {
                    currentOldId = allChildren[g];
                }
            }

            return currentOldId;
        }
    }
}
