﻿using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.paste
{
    public class PasteWindowsController
    {
        private OverwriteWindow _overWindows;
        private OverwriteWindowModel _overWindowsModel;
        //идет копирование или нет
        private CopyStatusController _copyStatus;
        private FirstSyncFilesController _firstSyncController;
        private SqliteController _sqlLiteController;
        private MainWindowViewModel _viewMainModel;
        private StatusAllOperationModel _statusAll;
        private ExceptionController _exceptionController;

        public PasteWindowsController(Container cont)
        {
            _copyStatus = cont.GetInstance<CopyStatusController>();
            _overWindowsModel = cont.GetInstance<OverwriteWindowModel>();
            _overWindows = cont.GetInstance<OverwriteWindow>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _viewMainModel = cont.GetInstance<MainWindowViewModel>();
            _statusAll = cont.GetInstance<StatusAllOperationModel>();
            _exceptionController = cont.GetInstance<ExceptionController>();
        }

        public string insertEvent()
        {
            string Event;

            if (_copyStatus.isMove)
            {
                Event = "MOVE";
                //после опознавания event скидываем его обратно в false
                _copyStatus.isMove = false;
            }
            else
            {
                Event = "PASTE";

            }
            return Event;
        }
        public async Task PasteTask(string Event, FolderNodes PasteNodes , List<FolderNodes> ListCopyNodes)
        {
            try
            {
                if (ListCopyNodes.Count > 0)
                {
                    //Все ноды которые я хочу переместить или скопировать
                    Dictionary<FolderNodes, bool> folderNodesReplace = new Dictionary<FolderNodes, bool>();

                    //все FileInfoModel которые содержатся в папке куда я вставляю данные
                    Dictionary<FileInfoModel, bool> listPasteRaplace = new Dictionary<FileInfoModel, bool>();

                    //все отсортированные ноды, те что я хочу заменить т.е я нажал кнопку заменить!!!
                    Dictionary<FileInfoModel, bool> sortPasteRaplace = new Dictionary<FileInfoModel, bool>();

                    if (PasteNodes != null)
                    {
                        //Говорим сканерам, что мы сейчас копируем
                        _statusAll.isPaste = true;

                        if (ListCopyNodes.Count != 0)
                        {
                            checkReplaceFolderNodes(Event, PasteNodes, ListCopyNodes, folderNodesReplace , listPasteRaplace , sortPasteRaplace);
                        }

                        _copyStatus.isCopyStatus = false;
                    
                    }
                    else
                    {
                        Console.WriteLine("ViewFolder->PasteContextMenu_Click каталог или файл не выделен");
                    }
                }
            }
            catch(System.NullReferenceException s)
            {
                Console.WriteLine(s);
            }
            
        }

        public void checkReplaceFolderNodes(string Event , FolderNodes PasteNodes, List<FolderNodes> listCopy, Dictionary<FolderNodes, bool> listCopyRaplace , Dictionary<FileInfoModel, bool> listPasteRaplace, Dictionary<FileInfoModel, bool> sortPasteRaplace)
        {
            //получаем всех детей этой папки из базы т.к получить из ViewFolder затруднительно
            List<FileInfoModel> PasteChildren = _sqlLiteController.getSelect().GetSqlLiteParentIDFileInfoList(PasteNodes.Row_id);

            if (listCopy.Count > 0)
            {


                checkDublication(Event, listCopy, PasteChildren, listCopyRaplace, listPasteRaplace, sortPasteRaplace);
                runningPaste(Event, PasteNodes, listCopyRaplace , sortPasteRaplace);

                _copyStatus.isCopyStatus = true;
                //Console.WriteLine("ViewFolder->Выбрана ContextMenu Paste " + PasteNodes.FolderName);
            }
            else
            {
                Console.WriteLine("ViewFolder->PasteContextMenu_Click->PasteFolderNodes -> listCopyNodes: Отсутствуют Обьекты для вставки");
            }
        }


       

      private void checkDublication(string Event , List<FolderNodes> listCopy , List<FileInfoModel> PasteChildren , Dictionary<FolderNodes, bool> listCopyRaplace , Dictionary<FileInfoModel, bool> listPasteRaplace , Dictionary<FileInfoModel, bool> sortPasteRaplace)
       {
            for (int s = 0; s < listCopy.Count; s++)
            {
                if (_copyStatus.isCopyStatus == false)
                {

                    checkDublicationRelases(Event, listCopy[s], PasteChildren, listCopyRaplace, listPasteRaplace, sortPasteRaplace);
                }

            }

      }


        private void checkDublicationRelases(string Event , FolderNodes copyNodes, List<FileInfoModel> Children, Dictionary<FolderNodes, bool> listCopyRaplace , Dictionary<FileInfoModel, bool> listPasteRaplace, Dictionary<FileInfoModel, bool> sortPasteRaplace)
        {
            bool duplication = checkDublicated(Children, copyNodes , listPasteRaplace);
            try
            {

                if (duplication)
                {
                    openWindowsChoseReplace(Event , copyNodes, listCopyRaplace , listPasteRaplace , sortPasteRaplace);
                }
                else
                {
                    listCopyRaplace.Add(copyNodes, false);
                    Console.WriteLine("checkDublicationRelases->PasteTask: Копирование без совпадений  " + copyNodes.FolderName);
                    //await _firstSyncController.RensponceServerSinglEvent(copyNodes, Event, PasteNodes , replace);
                }

            }
            catch (System.InvalidOperationException s3)
            {
                Console.WriteLine("checkDublicationRelases->PasteTask");
                Console.WriteLine(s3.ToString());
            }
        }

        private Dictionary<FileInfoModel, bool> setReplaceListPaste(string searchCopyName, Dictionary<FileInfoModel, bool> listPasteRaplace, Dictionary<FileInfoModel, bool> sortPasteRaplace, bool isReplace)
        {
          

            foreach (var item in listPasteRaplace)
            {
              if(item.Key.filename.Equals(searchCopyName))
              {
                    sortPasteRaplace.Add(item.Key, isReplace);
              }
            }

            return sortPasteRaplace;
        }
        private void openWindowsChoseReplace(string Event  , FolderNodes copyNodes, Dictionary<FolderNodes, bool> listCopyRaplace , Dictionary<FileInfoModel, bool> listPasteRaplace, Dictionary<FileInfoModel, bool> sortPasteRaplace)
        {
            openOverwriteWindow(_overWindows);

            //разрешили замену файла или папки
            if (_overWindowsModel.isOverwrite == true)
            {
                Console.WriteLine("checkDublicationRelases->PasteTask: Разрешили заменить файл  " + copyNodes.FolderName);

                listCopyRaplace.Add(copyNodes, true);
                listPasteRaplace = setReplaceListPaste(copyNodes.FolderName , listPasteRaplace , sortPasteRaplace, true);
            }
            else
            {
                //listCopyRaplace.Add(copyNodes, false);
                Console.WriteLine("MainWindow->PasteTask: НЕ Разрешили заменить файл  " + copyNodes.FolderName);
            }
        }

        private void openOverwriteWindow(OverwriteWindow _windowsOverwrite)
        {
            if(App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {
                    if (_windowsOverwrite != null)
                    {
                        _windowsOverwrite.ShowDialog();
                    }

                });
            }
            
        }

        private void runningPaste(string Event,  FolderNodes PasteNodes, Dictionary<FolderNodes, bool> allCopyNodes , Dictionary<FileInfoModel, bool> listPasteRaplace )
        {
            if (allCopyNodes.Count != 0)
            {
                FolderNodes folderNodes = null;
                bool[] isFirstElement = { false };
                bool[] replace = { false };

                folderNodes =  sendStatusWindows(allCopyNodes,  isFirstElement,  folderNodes,  replace);


                PasteServerModel pasteServerModel = new PasteServerModel();
                pasteServerModel.allCopyNodes = allCopyNodes;
                pasteServerModel.folderNodes = folderNodes;
                pasteServerModel.replace = replace[0];
                pasteServerModel.pasteNodes = PasteNodes;
                pasteServerModel.copyOrCutRootLocation = _viewMainModel.CopyAndCutRootLocation;
                pasteServerModel._exceptionController = _exceptionController;
                //проверить это место работы
                pasteServerModel.allPasteNodes = listPasteRaplace;



                _firstSyncController.PasteServer(pasteServerModel, Event);
            }
        }

        public FolderNodes sendStatusWindows(Dictionary<FolderNodes, bool> allCopyNodes , bool[] isFirstElement , FolderNodes folderNodes , bool[] replace)
        {
            //первый попавший элемент вытаскиваем 
            //и отправляем в PasteController 
            //что-бы он отображался на визуальном окне хода операции
            //а все остальные висели в скрытом виде
            foreach (KeyValuePair<FolderNodes, bool> kvp in allCopyNodes)
            {
                if (isFirstElement[0] == false)
                {
                    isFirstElement[0] = true;

                    folderNodes = kvp.Key;
                    replace[0] = kvp.Value;

                    break;
                }

            }

            return folderNodes;
        }

        //ищет дубликаты в коллекции
        private bool checkDublicated(List<FileInfoModel> viewFolder, FolderNodes copyNodes , Dictionary<FileInfoModel, bool> listPasteRaplace)
        {
            bool duplicate = false;


            foreach (FileInfoModel item in viewFolder)
            {
                if (item.filename.Equals(copyNodes.FolderName))
                {
                    duplicate = true;
                    listPasteRaplace.Add(item , false);
                }
            }


            return duplicate;

        }

    }
}
