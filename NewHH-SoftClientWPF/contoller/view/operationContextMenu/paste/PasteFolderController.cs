﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;

using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.delete;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace NewHH_SoftClientWPF.contoller.paste
{
    public class PasteFolderController
    {
      
        private StatusSqlliteTransaction _statusTransaction;
        private WebDavClientController _webDavClientController;
        private SupportPasteControllerAllFunction _supportAllF;
        private DeleteController _deleteController;
  
        private UpdateFormCopyWindowsController _copyForm;
        private StatusAllOperationModel _allStatus;
        private ExceptionController _exceptionController;



        public PasteFolderController(Container cont)
        {
            _statusTransaction = cont.GetInstance<StatusSqlliteTransaction>();
            _webDavClientController = cont.GetInstance<WebDavClientController>();
            _supportAllF = new SupportPasteControllerAllFunction();
            _deleteController = cont.GetInstance<DeleteController>();
      
            _copyForm = cont.GetInstance<UpdateFormCopyWindowsController>();
            _allStatus = cont.GetInstance<StatusAllOperationModel>();
            _exceptionController = cont.GetInstance<ExceptionController>();
        }


        public void StartFolderPaste(PasteFilesAsyncModel PasteModel, string Event)
        {
            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            Task.Run(async () =>
            {
                try
                {

                    //реализация перемещения
                    PasteFolder(PasteModel , "PASTE");
  
                }
                catch (System.Data.SQLite.SQLiteException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "PasteFolderController->StartFolderPaste Критическая ошибка SQLiteException", z.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }
                catch (AggregateException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "PasteFolderController->StartFolderPaste Критическая ошибка AggregateException", z.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }
                catch (WebDAVClient.Helpers.WebDAVException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "PasteFolderController->StartFolderPaste Критическая ошибка WebDAVException", z.ToString()); ;

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }
                catch (System.InvalidOperationException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "PasteFolderController->StartFolderPaste Критическая ошибка InvalidOperationException", z.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }

                _allStatus.isPaste = false;

            });

           

        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }


        public void StartFolderMove(PasteFilesAsyncModel PasteModel , string Event)
        {
            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            Task.Run(async () =>
            {
                try
                {
                        //реализация перемещения
                        MoveFolder(PasteModel);
                }
                catch (System.Data.SQLite.SQLiteException g)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(g.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }
                catch (AggregateException e)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(e.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }
                catch (WebDAVClient.Helpers.WebDAVException s)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(s.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }
                catch (System.InvalidOperationException d)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(d.ToString());

                    status.StatusWorker = "Ошибка вставки файлов";
                    progress.Report(status);
                }

                _allStatus.isPaste = false;

            });

        }

        public async void PasteFolder(PasteFilesAsyncModel PasteModel , string Event)
        {

            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            PasteModel.status = _supportAllF.CreateIProgressPasteFolder(PasteModel);

            StatusUpdateForm(status, progress);
            bool check = await _webDavClientController.isExistLocation(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);
            
            if(check)
            {
                clearMoveSqlPasteFolder(PasteModel);
                long[][][] allEndChildren = createChildrebFilesListCopy(PasteModel, Event);
                //Подготовили все данные для копирования, отправляем на копирование
                _webDavClientController.StartCopy(PasteModel, allEndChildren, false);

               
                statusSuccesPasteFolder(status, progress);
            }
            else
            {
                statusErrorPasteFolder(status, progress);
                
                List<string> listLocationDelete = await _webDavClientController.getListNoExist(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);
                sendToDeleteArr(listLocationDelete, PasteModel);
            }
            
         
        }
        private void StatusUpdateForm(CopyViewModel status, IProgress<CopyViewModel> progress)
        {
            status.StatusWorker = "Синхронизация";
            progress.Report(status);
        }
        private void statusSuccesPasteFolder(CopyViewModel status , IProgress<CopyViewModel> progress)
        {
            
                Console.WriteLine("PasteFolderController-> PasteFolder: Синхронизация завершенна");

                status.StatusWorker = "Выполнено!";
                progress.Report(status);
                _copyForm.updateCloseWindowsCopy();
        }

        private void statusErrorPasteFolder(CopyViewModel status, IProgress<CopyViewModel> progress)
        {

            Console.WriteLine("PasteFilesController->StartFilesMove: Копирование завершилось аварийно не найден файл на диске");
            status.StatusWorker = "Ошибка вставки файлов";
            progress.Report(status);
            //_copyForm.updateCloseWindowsCopy();
        }


        public async void MoveFolder(PasteFilesAsyncModel PasteModel)
        {
            
            CopyViewModel _status=  PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            //Создание Progress для формы
            _status = _supportAllF.CreateIProgressMoveFolder(PasteModel);

            bool check = await _webDavClientController.isExistLocation(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);

            if(check)
            {
                //очистка вставляемых файлов т.е очищаем папку назначения
                //если выбрали замену
                clearMoveSqlPasteFolder(PasteModel);

                string Event = "DELETE";
                long[][][] allEndChildren = createChildrebFilesListCopy(PasteModel, Event);

                //Подготовили все данные для копирования, отправляем на копирование
                _webDavClientController.StartCopy(PasteModel, allEndChildren, true);

                _supportAllF.updateRunningProgressStatus(PasteModel, "Очистка старых файлов");

                //удаляем все файлы в папке откуда происходит вырезание файлов и папок
                ClearAllOtherTempFiles(PasteModel, Event);
            }
            else
            {
                _supportAllF.updateRunningProgressStatus(PasteModel, "Ошибка вставки файлов");

                List<string>listLocationDelete = await _webDavClientController.getListNoExist(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);
                sendToDeleteArr(listLocationDelete, PasteModel);

                Console.WriteLine("PasteFilesController->StartFilesMove: Копирование завершилось аварийно не найден файл на диске");
            }
           

         
        }

        private void sendToDeleteArr(List<string> locationList, PasteFilesAsyncModel PasteModel)
        {
            SendDeleteArrModel deleteModel = new SendDeleteArrModel();
            deleteModel.sqliteController = PasteModel._sqlLiteController;
            deleteModel.locationList = locationList;

            PasteModel.updateNetworkJsonController.sendToDeleteArr(locationList, deleteModel);
        }

        private FileInfoModel[] addFileInfo(FileInfoModel[] arr , List<string> locationList , SqliteController _sqlLiteController)
        {
            int index = 0;
            foreach (string location in locationList)
            {
                FileInfoModel model = _sqlLiteController.getSelect().getSearchNodesByLocation(location);
                model.filename = location;
                model.changeRows = "DELETE";
                arr[index] = model;
                index++;
            }

            return arr;
        }



        private long[][][] createChildrebFilesListCopy(PasteFilesAsyncModel PasteModel, string Event)
        {

            List<long[][]> allChildrenList = new List<long[][]>();

            //Проверка освобождение транзакции
            _supportAllF.WaitingTransaction(_statusTransaction);

            foreach (KeyValuePair<FolderNodes, bool> kvp in PasteModel.allCopyNodes)
            {

                FolderNodes copyNodes = kvp.Key;
                bool replace = kvp.Value;

                CreateTempFilesChildren(allChildrenList, PasteModel, copyNodes.Location, Event);
            }

            //3 уровень всегда равен 0
            long[][][] allFiles = allChildrenList.ToArray();

            long[][] allEnd = new long[allFiles.Length][];
            long[] new_id_container = { 0 };

            for (int z = 0; z < allFiles.Length; z++)
            {
                allFiles[z] = GeneratedNewIdFilesChildren(allFiles[z], PasteModel, new_id_container);
            }


            return allFiles;
        }


        private void CreateTempFilesChildren(List<long[][]> allFiles, PasteFilesAsyncModel PasteModel, string copyNodesLocation  , string Event)
        {
            string progressLabel = "Поиск файлов";

            //получаем список всех детей arr[0] - arr2[0] - id
            //                                    arr2[1] - isFolder
            allFiles =  PasteModel._sqlLiteController.getAllChildren(allFiles , copyNodesLocation, PasteModel._sqlLiteController , PasteModel.progress , progressLabel);

         
        }

        private long[][] GeneratedNewIdFilesChildren(long[][] allFiles, PasteFilesAsyncModel PasteModel , long[] new_id_container)
        {
            
            SupportPasteFolderController supportPaste = new SupportPasteFolderController();
            //подготовка всех данных для отправки на копирования "Создание новых id номеров и создание новых нодов для вставки" ->(изменяем allChildrenList)
            supportPaste.getDataFolder(PasteModel, allFiles, new_id_container);

            return allFiles;
        }



       

        //удаляет все файлы если мы выбрали "Заменить папку при копировании или перемещении"
        private void clearMoveSqlPasteFolder(PasteFilesAsyncModel PasteModel)
        {
            
            string deleteStatus = "Очищение файлов в папке назначения";
            //ждем освобождения Транзакции
            _supportAllF.WaitingTransaction(_statusTransaction);

            StartPasteMoveClear(PasteModel);


        }

        private string createPasteLocation(PasteFilesAsyncModel PasteModel)
        {
            string copyFolderName = PasteModel.copyFolderName;
            string pasteLocation = PasteModel.pasteFolder.Location;
            string convertUrlFilename = "";
            string locationPaste = "";

            if (staticVariable.Variable.isFolder(PasteModel.type))
            {
                 convertUrlFilename = staticVariable.Variable.EscapeUrlString(copyFolderName);
                 locationPaste = pasteLocation + convertUrlFilename + "/";
            }
            else
            {
                 convertUrlFilename = staticVariable.Variable.EscapeUrlString(copyFolderName);
                 locationPaste = pasteLocation + convertUrlFilename;
            }

            return locationPaste;
        }

        private void StartPasteMoveClear(PasteFilesAsyncModel PasteModel)
        {

            string locationPaste = createPasteLocation(PasteModel);



            //NodesPaste возвращает нод с захватом вставляемой папки
            //пример: D:\\java\\spring\\ - PasteFolder D:\\java\\spring\\копируемая папка - NodesPaste
            FileInfoModel NodesPaste = PasteModel._sqlLiteController.getSelect().getSearchNodesByLocation(locationPaste);
        

            if (NodesPaste != null)
            {
                //удаляем из базы все файлы PasteFolder т.к указали, что их нужно перезаписать
                _deleteController.deletePasteMoveSql(PasteModel , NodesPaste.location , true);
            }
            else
            {
                Console.WriteLine("PasteFolderController -> StartPasteMoveClear: Критическая ОШИБКА:");
            }
        }


       



        private void StartClearAllOther(PasteFilesAsyncModel PasteModel, string Event)
        {
            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;
            string deleteStatus = "non!";
            string EventDelete = "DELETE";
            string progressLabel = "Поиск временных файлов:";

          //  if (_mqControll.CheckMqPasteParties())
           // {
                bool _isDeleteRootFolder = false;
                bool _isMove = true;
                
                _supportAllF.WaitingTransaction(_statusTransaction);

                _deleteController.deleteSql(PasteModel,  deleteStatus, _isDeleteRootFolder,  progressLabel, _isMove, EventDelete);
                //проверка на выполнение 
                CheckClearAllOther(status, progress);
                
           // }
           // else
           // {
                //выводит ошибку, что синхронизация не завершена
           //     CheckClearErrorAllOther(status, progress);
           // }

        }

        private void CheckClearErrorAllOther(CopyViewModel status, IProgress<CopyViewModel> progress)
        {
            status.StatusWorker = "Ошибка синхронизации файлов";
            progress.Report(status);
            _copyForm.updateCloseWindowsCopy();
        }

        private void CheckClearAllOther(CopyViewModel status , IProgress<CopyViewModel> progress)
        {
           // if (_mqControll.CheckMqPasteParties())
           // {
                status.StatusWorker = "Выполнено!";
                progress.Report(status);
                _copyForm.updateCloseWindowsCopy();
           // }
           // else
           // {
            //    status.StatusWorker = "Ошибка синхронизации файлов";
           //     progress.Report(status);
            //    _copyForm.updateCloseWindowsCopy();
           // }
        }

        private void ClearAllOtherTempFiles(PasteFilesAsyncModel PasteModel , string Event)
        {
            UpdateFormStatus(PasteModel);
            StartClearAllOther(PasteModel, Event);
        }
        //Обновляем форму надписью "Синхронизация"
        private void UpdateFormStatus(PasteFilesAsyncModel PasteModel)
        {
            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;
            status.StatusWorker = "Синхронизация";
            progress.Report(status);
        }

        private string EscapeUrlString(string copyFolderName)
        {
            return HttpUtility.UrlEncode(copyFolderName);
        }
     
    }
}
