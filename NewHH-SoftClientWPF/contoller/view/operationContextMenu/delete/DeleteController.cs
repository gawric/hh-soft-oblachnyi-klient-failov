﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;

using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.delete;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.delete
{
    class DeleteController
    {
        private WebDavClientController _webDavController;
        private SupportPasteControllerAllFunction _supportAllFunction;
   
        private UpdateFormCopyWindowsController _copyForm;
        private CopyStatusController _copyStatus;
        private WebDavClientExist _webDavClientExist;
        private ExceptionController _exceptionController;
        private DeleteFolderFileSystem _deleteFolderFileSystem;
        private SqliteController _sqliteController;
        public DeleteController(Container cont)
        {
            _webDavController = cont.GetInstance<WebDavClientController>();
            _supportAllFunction = new SupportPasteControllerAllFunction();

            _copyForm = cont.GetInstance<UpdateFormCopyWindowsController>();
            _copyStatus = cont.GetInstance<CopyStatusController>();
            _webDavClientExist = cont.GetInstance<WebDavClientExist>();
            _exceptionController = cont.GetInstance<ExceptionController>();
            _sqliteController = cont.GetInstance<SqliteController>();
            _deleteFolderFileSystem = new DeleteFolderFileSystem(_sqliteController);

        }

        private void clearRootFolder(List<long[][]>list , FileInfoModel rootFolderSqllite)
        {
            for (int w = 0; w < list.Count; w++)
            {
                if(list[w] != null)
                {
                    long[][] listFirst = list[w];
                    serachRootId(listFirst, rootFolderSqllite.row_id);
                }
               
            }
        }

        private void serachRootId(long[][] listFirst , long row_id_root)
        {
            for (int u = 0; u < listFirst.Length; u++)
            {
                if (listFirst[u] != null)
                {
                    long[] testList = listFirst[u];

                    long row_id_delete = listFirst[u][0];

                    if (row_id_root == row_id_delete)
                    {
                        listFirst[u] = null;
                    }
                }

            }
        }

        public  void StartPasteDeleteIsMoveTrue(DeleteFilesAsyncModel DeleteModel, string Event)
        {
            releasesPasteDeleteMove(DeleteModel, Event);
        }
        public void StartPasteDeleteIsMoveFalse(DeleteFilesAsyncModel DeleteModel, string Event)
        {
            releasesDeletePaste(DeleteModel);
        }

        public void StartDeleteNoNewThread(DeleteFilesAsyncModel DeleteModel, string Event, bool isMove)
        {
            RunningDel(DeleteModel, Event, isMove);
        }
        public void StartDelete(DeleteFilesAsyncModel DeleteModel , string Event , bool isMove)
        {
            Task.Run(() =>{RunningDel( DeleteModel,  Event,  isMove);});
        }
        private void RunningDel(DeleteFilesAsyncModel DeleteModel, string Event, bool isMove)
        {
            bool deleteRootFolder = DeleteModel.deleteRootFolder;
            CopyViewModel status = DeleteModel.status;
            IProgress<CopyViewModel> progress = DeleteModel.progress;

            try
            {
                if (Event.Equals("DELETE"))
                {
                    //move если мы передаем флаг true то удалять сами файлы не будем только из базы
                    //если передаем false будем удалять все!
                    if (isMove)
                    {
                        releasesDeleteMove(DeleteModel, Event);
                    }
                    else
                    {
                        releasesDeletePaste(DeleteModel);
                    }


                }


            }
            catch (WebDAVClient.Helpers.WebDAVException z)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "DeleteController->StartDelete Критическая ошибка WebDAVException", z.ToString());
            }
            catch (System.NullReferenceException z)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "DeleteController->StartDelete Критическая ошибка NullReferenceException", z.ToString());
            }
        }
        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }


        private void releasesDeleteMove(DeleteFilesAsyncModel DeleteModel  , string Event)
        {
            bool deleteRootFolder = DeleteModel.deleteRootFolder;
            CopyViewModel status = DeleteModel.status;
            IProgress<CopyViewModel> progress = DeleteModel.progress;

            List<long[][]> allFiles = ScanAllChildrenDeleteMove(DeleteModel);

            int numberOperation = DeleteModel._sqlLiteController.getSelect().GetLastNumberOperationListFileTemp() + 1;


            DeleteRootFolder(DeleteModel, deleteRootFolder);
            
            //Отправка на удаление 
            //и добавляет все данные из рабочий базы во временную базу listFileTemp
            StartSendDeleteJson(DeleteModel, allFiles, numberOperation);

            //очищаем таблицу от временных записей listFilesTemp
            DeleteModel._sqlLiteController.removeTablelistFileTempChangeRows(Event, numberOperation);

            CheckDeleteMovePart2( DeleteModel,  status,  progress);

        }

        private void DeleteRootFolder(DeleteFilesAsyncModel DeleteModel, bool deleteRootFolder)
        {
            //пришел запрос на удаление рутовой папки вместе и файлами
            //false- удаляем все файлы а рутовую папку не трогаем
            if (deleteRootFolder == true)
            {
                if (DeleteModel.AllDeletePasteFilesReplaceStatus != null)
                {
                    foreach (KeyValuePair<FileInfoModel, bool> deleteReplace in DeleteModel.AllDeletePasteFilesReplaceStatus)
                    {
                        //нод взяз из viewfolders 
                        //value статус  заменять ее или нет
                        if (deleteReplace.Value == true)
                        {
                            // FileInfoModel copyNodes = deleteReplace.Key;
                            // FileInfoModel rootFolderSqllite = DeleteModel._sqlLiteController.getSearchNodesByLocation(copyNodes.location);
                            //clearRootFolder(allFiles, rootFolderSqllite);
                        }
                    }
                }



            }

        }

        private List<long[][]> ScanAllChildrenDeleteMove(DeleteFilesAsyncModel DeleteModel)
        {

            //p количество файлов в заданном каталоге
            int[] p = { 0 };
            List<long[][]> allFiles = new List<long[][]>();

            for (int a = 0; a < DeleteModel.allDeleteFiles.Length; a++)
            {
                FolderNodes deleteLocation = DeleteModel.allDeleteFiles[a];
                //arr[n](список всех записей)->arr->0 - id | 1 - type(0 - files/1 - folder)
                DeleteModel._sqlLiteController.getScanAllChildren(allFiles, deleteLocation.Location, p, DeleteModel._sqlLiteController, DeleteModel.progress, DeleteModel.progressLabel);
            }

            return allFiles;
        }

        private void CheckDeleteMovePart2(DeleteFilesAsyncModel DeleteModel , CopyViewModel status , IProgress<CopyViewModel> progress)
        {
        

            //проверяет что все пакеты отправлены и приняты клиентом и сервером
           // if (_controlMq.CheckMqDelete())
           // {
                Console.WriteLine("ControlMqController -> CheckMqDelete: Синхронизация прошла успешно всех отправленных запросов");

                status.StatusWorker = DeleteModel.deleteStatus;
                progress.Report(status);
                _copyForm.updateCloseWindowsCopy();
                _copyStatus.isCopyStatus = false;
           // }
           // else
           // {
           //     Console.WriteLine("ControlMqController -> CheckMqDelete: Ошибка синхронизации нарушена целостность запросов! 1 запроса не хватает!");

             //   status.StatusWorker = "Ошибка вставки файлов";
             //   progress.Report(status);
             //   _copyForm.updateCloseWindowsCopy();
             //   _copyStatus.isCopyStatus = false;
           // }
        }
        private string[] ConvertDictionaryToLocation(DeleteFilesAsyncModel DeleteModel)
        {
            string[] allSearch = new string[DeleteModel.AllDeletePasteFilesReplaceStatus.Count];
            int m = 0;

            foreach (var item in DeleteModel.AllDeletePasteFilesReplaceStatus)
            {
                if(item.Value == true)
                {
                    allSearch[m] = item.Key.location;
                    m++;
                }
            }

            return allSearch;
        }

        private void ScanGetAllChildren(DeleteFilesAsyncModel deleteModel, string[] allSearch , List<long[][]> allListFull)
        {
            //p количество файлов в заданном каталоге
            int[] p = { 0 };
            for (int a = 0; a < allSearch.Length; a++)
            {
                if(allSearch[a] != null)
                {
                    string deleteLocation = allSearch[a];
                    //arr[n](список всех записей)->arr->0 - id | 1 - type(0 - files/1 - folder)
                    //DeleteModel._sqlLiteController.getScanAllChildren(allFiles, deleteLocation, p, DeleteModel._sqlLiteController, DeleteModel.progress, DeleteModel.progressLabel);
                    ScannerSqlliteRecursiveAllChildrenModel ssracm = createChildrenModel(ref allListFull, ref deleteLocation, deleteModel);
                    allListFull = getAllChildren(ssracm);
                }
            
            }
        }
        private List<long[][]> getAllChildren(ScannerSqlliteRecursiveAllChildrenModel ssracm)
        {
            ScannerSqlliteRecursiveAllChildren ssrach = new ScannerSqlliteRecursiveAllChildren();
            return ssrach.getScanAllChildren(ssracm);
        }

        public ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(ref List<long[][]> allListFull, ref string location, DeleteFilesAsyncModel deleteModel)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();

            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = deleteModel._sqlLiteController;
            model.progress = deleteModel.progress;
            model.progressLabel = deleteModel.progressLabel;
            model.allParent = deleteModel._sqlLiteController.getSelect().getAllParentListRowId();
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();
            return model;
        }

        private void releasesPasteDeleteMove(DeleteFilesAsyncModel DeleteModel, string Event)
        {
            bool deleteRootFolder = DeleteModel.deleteRootFolder;
            CopyViewModel status = DeleteModel.status;
            IProgress<CopyViewModel> progress = DeleteModel.progress;
            
            List<long[][]> allFiles = new List<long[][]>();

            string[] allSearchLocation = ConvertDictionaryToLocation(DeleteModel);
            ScanGetAllChildren(DeleteModel, allSearchLocation, allFiles);
            
            int numberOperation = DeleteModel._sqlLiteController.getSelect().GetLastNumberOperationListFileTemp() + 1;
            
            //Отправка на удаление 
            //и добавляет все данные из рабочий базы во временную базу listFileTemp
            StartSendDeleteJson(DeleteModel, allFiles, numberOperation);

            //очищаем таблицу от временных записей listFilesTemp
            DeleteModel._sqlLiteController.removeTablelistFileTempChangeRows(Event, numberOperation);

            //проверка, что все обновления прошли
            CheckDeleteMove(DeleteModel, status, progress);
            
        }

        private void releasesPasteDeletePaste(DeleteFilesAsyncModel DeleteModel)
        {
            bool deleteRootFolder = DeleteModel.deleteRootFolder;
            CopyViewModel status = DeleteModel.status;
            IProgress<CopyViewModel> progress = DeleteModel.progress;

            DeleteModel.status = _supportAllFunction.CreateIProgressDeleteFolder(DeleteModel ,  "Тест");

            //p количество файлов в заданном каталоге
            int[] p = { 0 };



            List<long[][]> allFiles = new List<long[][]>();

            for (int a = 0; a < DeleteModel.allDeleteFiles.Length; a++)
            {
                FolderNodes deleteLocation = DeleteModel.allDeleteFiles[a];
                //arr[n](список всех записей)->arr->0 - id | 1 - type(0 - files/1 - folder)
                DeleteModel._sqlLiteController.getScanAllChildren(allFiles, deleteLocation.Location, p, DeleteModel._sqlLiteController, DeleteModel.progress, DeleteModel.progressLabel);

            }


            //пришел запрос на удаление рутовой папки вместе c файлами
            //false- удаляем все файлы а рутовую папку не трогаем
            if (deleteRootFolder == true)
            {
                FileInfoModel rootFolderSqllite = DeleteModel._sqlLiteController.getSelect().getSearchNodesByLocation(DeleteModel.location);
                clearRootFolder(allFiles, rootFolderSqllite);
            }


            int numberOperation = DeleteModel._sqlLiteController.getSelect().GetLastNumberOperationListFileTemp() + 1;
            _deleteFolderFileSystem.deleteAllPcSave(allFiles);

            //Отправка на удаление 
            //и добавляет все данные из рабочий базы во временную базу listFileTemp
            StartSendDeleteJson(DeleteModel, allFiles, numberOperation);

            status.StatusWorker = "Удаление";
            progress.Report(status);


          
            //location удаление его и всех его детей(выборка производится из sqllite базы, не из webdavclient)
            //type - тип выбранного нода
            _webDavController.StartDeleteWebDavClient(DeleteModel, allFiles, numberOperation).Wait();
            

            status.StatusWorker = "Синхронизация";
            progress.Report(status);



            CheckDeletePaste(status, progress);
            

        }

        private void CheckDeletePaste(CopyViewModel status, IProgress<CopyViewModel> progress)
        {
            
                //Console.WriteLine("ControlMqController -> CheckMqDelete: Синхронизация прошла успешно всех отправленных запросов");

                status.StatusWorker = "Выполнено!";
                progress.Report(status);
                _copyForm.updateCloseWindowsCopy();
           
        }

        private void CheckDeleteMove(DeleteFilesAsyncModel DeleteModel , CopyViewModel status , IProgress<CopyViewModel> progress)
        {
            
           
                //Console.WriteLine("ControlMqController -> CheckMqDelete: Синхронизация прошла успешно всех отправленных запросов");

                status.StatusWorker = DeleteModel.deleteStatus;
                progress.Report(status);
                _copyForm.updateCloseWindowsCopy();
                _copyStatus.isCopyStatus = false;
           
        }
        private void releasesDeletePaste(DeleteFilesAsyncModel DeleteModel)
        {
            bool deleteRootFolder = DeleteModel.deleteRootFolder;
            CopyViewModel status = DeleteModel.status;
            IProgress<CopyViewModel> progress = DeleteModel.progress;

            DeleteModel.status = _supportAllFunction.CreateIProgressDeleteFolder(DeleteModel ,  "Тест");

            //p количество файлов в заданном каталоге
            int[] p = { 0 };



            List<long[][]> allListFull = new List<long[][]>();

            for (int a = 0; a < DeleteModel.allDeleteFiles.Length; a++)
            {
                string deleteLocation = DeleteModel.allDeleteFiles[a].Location;
                ScannerSqlliteRecursiveAllChildrenModel ssracm = createChildrenModel(ref allListFull, ref deleteLocation, DeleteModel);
                allListFull = getAllChildren(ssracm);
                ssracm.allParent = null;
            }


            //пришел запрос на удаление рутовой папки и всех файлов
            //false- удаляем все файлы а рутовую папку не трогаем
            if (deleteRootFolder == true)
            {
                FileInfoModel rootFolderSqllite = DeleteModel._sqlLiteController.getSelect().getSearchNodesByLocation(DeleteModel.location);
                clearRootFolder(allListFull, rootFolderSqllite);
            }


            int numberOperation = DeleteModel._sqlLiteController.getSelect().GetLastNumberOperationListFileTemp() + 1;

            //удаляем из файловой системы если он там есть
            _deleteFolderFileSystem.deleteAllPcSave(allListFull);

            //Отправка на удаление 
            //и добавляет все данные из рабочий базы во временную базу listFileTemp
            StartSendDeleteJson(DeleteModel, allListFull, numberOperation);

            status.StatusWorker = "Удаление";
            progress.Report(status);
            
            //location удаление его и всех его детей(выборка производится из sqllite базы, не из webdavclient)
            //type - тип выбранного нода
            _webDavController.StartDeleteWebDavClient(DeleteModel, allListFull, numberOperation).Wait();
          

            status.StatusWorker = "Синхронизация";
            progress.Report(status);

           
               

            status.StatusWorker = "Выполнено!";
            progress.Report(status);
            _copyForm.updateCloseWindowsCopy();

            allListFull = null;
        }

        



        //Проходит 2 раза по одному и тому же массиву
        //Но если этого не сделать Sqllite не дает сразу добавлять и удалять значения
        private void StartSendDeleteJson( DeleteFilesAsyncModel DeleteModel , List<long[][]>list , int numberOperation)
        {
            try
            {
                CopyViewModel status = DeleteModel.status;
                IProgress<CopyViewModel> progress = DeleteModel.progress;

                status.StatusWorker = "Добавление во временное хранилище";
                progress.Report(status);
                //Добавление файлов во временное хранилище
                DeleteModel._sqlLiteController.StartInsertFileTemp(DeleteModel, list, numberOperation);

                status.StatusWorker = "Отправка на удаление";
                progress.Report(status);
                //реализация> отправка на удаление
                StartSendDelete(DeleteModel, list, numberOperation);
            }
            catch (System.IndexOutOfRangeException b5)
            {

                Console.WriteLine(b5.ToString());
            }



        }
        private void SendDelete(DeleteFilesAsyncModel DeleteModel, long[][] list , int[]cnt , FileInfoModel[] arr)
        {
            //list.lenght = начинает счет с 1, а массив с начинается с 0
            for (int k = 0; k < list.Length; k++)
            {

                if (list[k] != null)
                {
                    if (cnt[0] == staticVariable.Variable.CountFilesTrasferServer)
                    {

                        // _sqlLiteController.InsertFileTempTransaction(arr , numberOperation);
                        DeleteModel.updateNetworkJsonController.updateSendJsonDELETEPARTIES(arr, DeleteModel.newVersionBases);

                        //скидываем все файлы во временное хранилизе listFileTemp
                        //что-бы потом их вытащить когда будет удалять файлы из webDav


                        for (int s = 0; s < arr.Length; s++)
                        {
                            arr[s] = null;
                        }

                        cnt[0] = 0;

                    }


                    long row_id = list[k][0];

                    arr[cnt[0]] = DeleteModel._sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_id);
                    arr[cnt[0]].changeRows = "DELETE";
                    // Console.WriteLine("Удаляем файлик по row_id " + arr[cnt].row_id);


                    cnt[0]++;
                }




            }
        }
        //Добавляет файлы в JSON формат и отправляет их на сервер пачками по x шт
        //после обработки серваком они возвращаются и удаляются на сервере
        public void StartSendDelete(DeleteFilesAsyncModel DeleteModel  , List<long[][]>allList, int numberOperation)
        {
            //массив для хранения
            FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];

            
            try
            {
                DeleteModel.updateNetworkJsonController.updateSendJsonDELETEBEGINPARTIES(arr, DeleteModel.newVersionBases);

                for(int z = 0; z < allList.Count; z++)
                {
                    int[] cnt = { 0 };
                    long[][] list = allList[z];

                    if(list != null)
                    {
                        SendDelete(DeleteModel, list, cnt, arr);
                        DeleteModel.updateNetworkJsonController.updateSendJsonDELETEPARTIES(arr, DeleteModel.newVersionBases);
                    }
           
                    
                }
               
                //_sqlLiteController.InsertFileTempTransaction(arr , numberOperation);
                DeleteModel.updateNetworkJsonController.updateSendJsonDELETEENDPARTIES(arr, DeleteModel.newVersionBases);
            }
            catch (System.IndexOutOfRangeException)
            {
                throw;
            }
            catch (System.NullReferenceException npe)
            {
                Console.WriteLine("WebDavClientController->StartSendDelete  " + npe.ToString());
            }




        }

      

        public void deletePasteMoveSql(PasteFilesAsyncModel PasteModel , string location ,  bool deleteRootFolder)
        {
            Dictionary<FolderNodes, bool> allDictionaryCopyNodes = PasteModel.allCopyNodes;
            Dictionary<FileInfoModel, bool> allDictionaryPasteNodes = PasteModel.allPasteNodes;

            string deleteStatus = "non";
            string EventDelete = "DELETE";
            string progressLabel = "Поиск временных файлов";

            DeleteFilesAsyncModel DeleteModel = new DeleteFilesAsyncModel();
            DeleteModel.updateNetworkJsonController = PasteModel.updateNetworkJsonController;
            DeleteModel.newVersionBases = PasteModel.newVersionBases;
            DeleteModel.username = PasteModel.username;
            DeleteModel.password = PasteModel.password;
            DeleteModel.supportTopicModel = PasteModel.supportTopicModel;
            DeleteModel._sqlLiteController = PasteModel._sqlLiteController;
            DeleteModel.location = location;
            DeleteModel.type = PasteModel.type;
            DeleteModel.progress = PasteModel.progress;
            DeleteModel.status = PasteModel.status;
            DeleteModel.deleteRootFolder = deleteRootFolder;
            DeleteModel.deleteStatus = deleteStatus;
            DeleteModel.progressLabel = progressLabel;
            DeleteModel._webdavClientExist = _webDavClientExist;

            FolderNodes[] allDeleteFiles = ConvertDictionaryToArr(allDictionaryCopyNodes);
            DeleteModel.allDeleteFiles = allDeleteFiles;
            DeleteModel.AllDeleteFilesReplaceStatus = allDictionaryCopyNodes;
            DeleteModel.AllDeletePasteFilesReplaceStatus = allDictionaryPasteNodes;
  

            //true - означает что удалит  данные только в базе
            StartPasteDeleteIsMoveTrue(DeleteModel, EventDelete);
        }



        private FolderNodes[] ConvertDictionaryToArr(Dictionary<FolderNodes, bool> PasteModelDictionary)
        {
            int size = PasteModelDictionary.Count;
            FolderNodes[] folderNodes = new FolderNodes[size];
            int k = 0;
            foreach (var item in PasteModelDictionary)
            {
                folderNodes[k] = item.Key;
                k++;
            }

            return folderNodes;
        }
        public void deleteSql(PasteFilesAsyncModel PasteModel , string deleteStatus , bool deleteRootFolder, string progressLabel , bool isMove , string Event)
        {
            DeleteFilesAsyncModel DeleteModel = new DeleteFilesAsyncModel();
            DeleteModel.updateNetworkJsonController = PasteModel.updateNetworkJsonController;
            DeleteModel.newVersionBases = PasteModel.newVersionBases;
            DeleteModel.username = PasteModel.username;
            DeleteModel.password = PasteModel.password;
            DeleteModel.supportTopicModel = PasteModel.supportTopicModel;
            DeleteModel._sqlLiteController = PasteModel._sqlLiteController;
            DeleteModel.location = PasteModel.copyLocation;
            DeleteModel.type = PasteModel.type;
            DeleteModel.progress = PasteModel.progress;
            DeleteModel.status = PasteModel.status;
            DeleteModel.deleteRootFolder = deleteRootFolder;
            DeleteModel.deleteStatus = deleteStatus;
            DeleteModel.progressLabel = progressLabel;
            DeleteModel.allDeleteFiles = ConvertDictionaryToArr(PasteModel.allCopyNodes);
            DeleteModel._webdavClientExist = _webDavClientExist;
            //true - означает что удалит  данные только в базе
            StartDelete(DeleteModel, Event, isMove);
        }


    }
}
