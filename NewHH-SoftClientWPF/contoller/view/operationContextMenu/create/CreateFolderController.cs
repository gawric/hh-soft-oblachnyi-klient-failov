﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.create
{
    public class CreateFolderController : ICreateFolder
    {
        private WebDavClientController _webDavController;
        private SqliteController _sqlLiteController;
        private UpdateNetworkJsonController _updateNetworkJsonController;
        private CreateModel _createModel;
        public event EventHandler<CFModel> eventBegin;
        public event EventHandler<CFModel> eventEnd;
        public event EventHandler<CFModel> eventAddData;
        private bool isSubscrible = false;
        private SupportSubscribleEvent _supportSubsctibe;
        public bool isSubscribleData = false;
        public CreateFolderController(Container cont)
        {
            if(cont != null)
            {
                _webDavController = cont.GetInstance<WebDavClientController>();
                _sqlLiteController = cont.GetInstance<SqliteController>();
                _updateNetworkJsonController = cont.GetInstance<UpdateNetworkJsonController>();
                _createModel = new CreateModel(cont.GetInstance<ConvertIconType>());
                _supportSubsctibe = new SupportSubscribleEvent(this);
            }
            
        }

     

        public async Task TrainingCreateFolder(FileInfoModel sourceNodes , string folderName)
        {
            string[] usernameAndPassword = getUsername(_sqlLiteController);
            Client client = createConnect(usernameAndPassword[0], usernameAndPassword[1], _webDavController);
            Item folder = await GetFolder(client , sourceNodes);
            SubscribeCreateFolder();
            Create(ref folder , ref client , ref sourceNodes , ref folderName );
        }

        private void Create(ref Item folder , ref Client client , ref FileInfoModel sourceNodes , ref string folderName)
        {
            try
            {
                if (folder != null)
                {
                    client.CreateDir(sourceNodes.location, folderName).Wait();
                    FileInfoModel[] arr = updateNetworkClient(_sqlLiteController, sourceNodes, folderName);
                }
                else
                {
                    Console.WriteLine("CreateFolderController->TrainingCreateFolder: Ошибка создания папки!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CreateFolderController->TrainingCreateFolder: Ошибка WebDav не смог создать директорию    /n" + ex);
            }
        }

        private void SubscribeCreateFolder()
        {
            try
            {
                if(!isSubscrible)
                {
                    isSubscrible = true;
                    this.eventBegin += _supportSubsctibe.AddItemBegin;
                    this.eventEnd += _supportSubsctibe.AddItemEnd;
                }
               
            }
            catch(System.ArgumentException a)
            {
                Debug.Print("CreateFolderController->SubscribeCreateFolder: "+ a.ToString());
            }
         
        }


      
        public virtual void OnThresholdData(CFModel e)
        {
            eventAddData?.Invoke(this, e);
        }
    


    private async Task<Item> GetFolder(Client client , FileInfoModel sourceNodes)
        {
            if (client == null) return null;
                
            return await client.GetFolder(sourceNodes.location);
        }
        public string createNameTreeViewFolder(ObservableCollection<FolderNodes> listTree)
        {
            string nameFolder = "Новая папка";

            foreach(FolderNodes item in listTree)
            {
                if(item.FolderName.Equals("Новая папка"))
                {
                    nameFolder = "Новая папка 999";
                }

            }

            return nameFolder;

        }

        public string createNameListViewFolder(List<FileInfoModel> listTree)
        {
            string nameFolder = "Новая папка";

            foreach (FileInfoModel item in listTree)
            {
                if (item.filename.Equals("Новая папка"))
                {
                    nameFolder = "Новая папка 999";
                }

            }

            return nameFolder;

        }


       
        private FileInfoModel[] updateNetworkClient(SqliteController _sqlLiteController, FileInfoModel sourceNodes, string folderName)
        {
            long versionBases = getVersionBases();

            if(_updateNetworkJsonController != null)
            {
                // _updateNetworkJsonController.updateSendJsonSINGLBEGINUPDATEPARTIES(null, versionBases);
                AddBegin(versionBases, null);

                FileInfoModel[] arr = createModelArr(_sqlLiteController, sourceNodes, folderName);
                //_updateNetworkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(arr, versionBases);
                AddEnd(versionBases, arr);
                return arr;
            }


            return new FileInfoModel[0];
        }

        public void AddBegin(long version , FileInfoModel[] arr)
        {
            CFModel args = new CFModel();
            args.version = version;
            args.arr = arr;
            args.unjc = _updateNetworkJsonController;
            OnThresholdBegin(args);
        }

        public void AddEnd(long version, FileInfoModel[] arr )
        {
            CFModel args = new CFModel();
            args.version = version;
            args.arr = arr;
            args.unjc = _updateNetworkJsonController;
            OnThresholdEnd(args);
        }

      


        protected virtual void OnThresholdBegin(CFModel e)
        {
            eventBegin?.Invoke(this, e);
        }

        protected virtual void OnThresholdEnd(CFModel e )
        {
            eventEnd?.Invoke(this, e);
        }

     

        public FileInfoModel[] createModelArr(SqliteController _sqlLiteController , FileInfoModel sourceNodes, string folderName)
        {
            long new_row_id = _sqlLiteController.getSelect().getLastRow_idFileInfoModel() + 1;

            new_row_id  = staticVariable.Variable.generatedNewRowId(new_row_id, null);
            _sqlLiteController.getInsert().insertListFilesTempIndex(new_row_id);
            //приводим в url вид
            string convertNamefolderName = staticVariable.Variable.EscapeUrlString(folderName);
            string new_location = sourceNodes.location + convertNamefolderName + "/";

            FileInfoModel[] arr = createArr(new_row_id, folderName, new_location, sourceNodes.row_id);

            return arr;
        }
        private FileInfoModel[] createArr(long new_row_id , string filename , string newlocation , long parent_row_id)
        {
            long versionBases=  getVersionBases();
            string date1 = new DateTime().ToString();
           
            FileInfoModel fileInfo = _createModel.createFileInfoModel(new_row_id, filename, date1, date1, date1,"", newlocation, parent_row_id, "folder", 0, versionBases, 0, 0, "CREATE", 0);
            FileInfoModel[] arr = new FileInfoModel[1];
            arr[0] = fileInfo;

            return arr;
        }

        private string[] getUsername(SqliteController _sqlLiteController)
        {
            if(_sqlLiteController == null) return new string[2];

            return _sqlLiteController.getSelect().GetUserSqlite();
            
        }

        private Client createConnect(string login , string password , WebDavClientController _webDavController)
        {
            if (_webDavController == null) return null;

            return _webDavController.CreateClent(login, password);

        }

        private long getVersionBases()
        {
            if (_sqlLiteController != null) return _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient();

            return 0;

        }
    }
}
