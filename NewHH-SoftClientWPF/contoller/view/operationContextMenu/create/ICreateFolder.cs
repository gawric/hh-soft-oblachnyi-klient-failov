﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create
{
    public interface ICreateFolder
    {
        Task TrainingCreateFolder(FileInfoModel sourceNodes, string folderName);
        string createNameTreeViewFolder(ObservableCollection<FolderNodes> listTree);
        string createNameListViewFolder(List<FileInfoModel> listTree);

        event EventHandler<CFModel> eventAddData;

    }
}
