﻿
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.delete;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.renameJsonModel;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.rename
{
    public class RenameFolderController
    {
        private StatusSqlliteTransaction _statusTransaction;
        private SupportPasteControllerAllFunction _supportAllF;

      
        public RenameFolderController(Container cont)
        {
            if(cont != null)
            {
                _statusTransaction = cont.GetInstance<StatusSqlliteTransaction>();
                _supportAllF = cont.GetInstance<SupportPasteControllerAllFunction>();
            }
         
           
        }
        private FileInfoModel[] getArr()
        {
            return new FileInfoModel[1];
        }
        //получается отправка 2 запросами 
        //2 запрос отправляет всех детей в базу что были изменены
        public void StartSendJsonRenameFolder(RenameWebDavModel renameModel , string newLocation, long newVersionBases)
        {
            _supportAllF.WaitingTransaction(_statusTransaction);

            //заглушка не реализовано(пока не нужен)
            Progress<CopyViewModel> progress = new Progress<CopyViewModel>();
            UpdateProgress(renameModel , progress);

           
           // long[][] allChildrenListEnd = null;
            List<long[][]> allListFull = new List<long[][]>();

           // allChildrenListEnd = SearchChildren(renameModel, progress , allChildrenListEnd);
            string oldLocation = renameModel.oldLocation;
            ScannerSqlliteRecursiveAllChildrenModel ssracm = createChildrenModel(ref allListFull, ref oldLocation, renameModel._sqliteController);
            allListFull = getAllChildren(ssracm);
            ssracm = null;

            SendJsonRenameFolder(renameModel ,  newLocation, newVersionBases, allListFull[0]);
        }

        public List<long[][]> getAllChildren(ScannerSqlliteRecursiveAllChildrenModel ssracm)
        {
            ScannerSqlliteRecursiveAllChildren ssrach = new ScannerSqlliteRecursiveAllChildren();
            return ssrach.getScanAllChildren(ssracm);
        }

        public ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(ref List<long[][]> allListFull, ref string location, SqliteController _sqliteController)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();

            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = _sqliteController;
            model.progress = new Progress<CopyViewModel>();
            model.progressLabel = "search children";
            //все записи из базы
            model.allParent = _sqliteController.getSelect().getAllParentListRowId();
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();

            return model;
        }



        private long[][] SearchChildren(RenameWebDavModel renameModel, Progress<CopyViewModel> progress , long[][] allChildrenListEnd)
        {
            //заглушка не реализовано(пока не нужен)
            string progressLabel = "Заглушка Rename";

            renameModel._updateNetworkJsonController.updateSendJsonSEARCHBEGINFILES(null, 0);

            allChildrenListEnd =  GetAllChildren(renameModel, progress, progressLabel);

            renameModel._updateNetworkJsonController.updateSendJsonSEARCHENDFILES(getArr(), 0);

            return allChildrenListEnd;

        }

        private void UpdateProgress(RenameWebDavModel renameModel , Progress<CopyViewModel> progress)
        {
            int d = 0;
            bool hack = false;
            progress.ProgressChanged += (s, e) =>
            {
                CopyViewModel statusModel = e;

                if (d >= staticVariable.Variable.CountFilesTrasferServer)
                {
                    d = 0;
                    if (hack == false)
                    {
                        hack = true;
                        renameModel._updateNetworkJsonController.updateSendJsonSEARCHPARTIESFILES(getArr(), 0);
                        hack = false;
                    }

                }
                d++;

            };
        }

        private long[][] GetAllChildren(RenameWebDavModel renameModel , Progress<CopyViewModel> progress , string progressLabel)
        {
            List<long[][]> allFilesList = new List<long[][]>();
            //получаем список всех детей arr[0] - arr2[0] - id
            //                                    arr2[1] - isFolder
            List<long[][]> allChildrenList = renameModel._sqliteController.getAllChildren(allFilesList, renameModel.oldLocation, renameModel._sqliteController, progress, progressLabel);

            //allChildrenList[0] - всегда будет 0 т.к максимум за 1 раз можем переименовать 1 папку или файл
            return allChildrenList[0];

        }

        private void SendJsonRenameFolder(RenameWebDavModel renameModel , string newLocation ,long newVersionBases , long[][] list)
        {
            //массив для хранения
            FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];

            RenameJsonModel model = new RenameJsonModel();
            model.renameModel = renameModel;
            model.arr = arr;
            model.newLocation = newLocation;
            model.list = list;

            int[] cnt = { 0 };
            try
            {
                renameModel._updateNetworkJsonController.updateSendJsonRENAMESINGLBEGINUPDATEPARTIES(arr, newVersionBases);

                //list.lenght = начинает счет с 1, а массив с начинается с 0
                for (int k = 0; k < list.Length; k++)
                {
                    if (list[k] != null)
                    {

                        SendSinglUpdateParties(cnt, model);
                        AddFileInfoToArr(cnt[0], k, model);
                        cnt[0]++;

                    }

                }


                renameModel._updateNetworkJsonController.updateSendJsonRENAMESINGLENDUPDATEPARTIES(arr, newVersionBases);


            }
            catch (System.IndexOutOfRangeException sd)
            {
                Console.WriteLine(sd);
            }
            catch (System.NullReferenceException kl)
            {
                Console.WriteLine(kl);
            }


        }
        //Добавление в массив для отправки
        private void AddFileInfoToArr(int cnt , int k , RenameJsonModel model)
        {
            long row_id = model.list[k][0];
            FileInfoModel[] arr = model.arr;
    

            FileInfoModel currentNodes = model.renameModel._sqliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_id);

            string replaceLocation = currentNodes.location.Replace(model.renameModel.oldLocation, model.newLocation);
            currentNodes.location = replaceLocation;

            arr[cnt] = currentNodes;
            arr[cnt].changeRows = "UPDATE";
           
        }
        //Отправка данных на сервер
        private void SendSinglUpdateParties(int[] cnt , RenameJsonModel renameJsonModel)
        {
            if (cnt[0] == staticVariable.Variable.CountFilesTrasferServer)
            {

                FileInfoModel[] arr = renameJsonModel.arr;
                renameJsonModel.renameModel._updateNetworkJsonController.updateSendJsonRENAMESINGLUPDATEPARTIES(arr, renameJsonModel.newVersionBases);

                for (int s = 0; s < arr.Length; s++)
                {
                    arr[s] = null;
                }

                cnt[0] = 0;

            }

        }
    }
}
