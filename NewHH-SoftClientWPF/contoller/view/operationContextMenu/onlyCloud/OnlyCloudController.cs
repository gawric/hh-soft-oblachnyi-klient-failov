﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.convert;
using NewHH_SoftClientWPF.contoller.util.filesystem.delete;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.onlyCloud
{
    public class OnlyCloudController
    {
        private OnlyCloudMainModel _onlyCloudMainModel;
        private StackUpdateTreeViewIcon _stackUpateTreeViewIcon;
        private DeleteFolderFileSystem _deleteFolder;

        public OnlyCloudController(OnlyCloudMainModel onlyCloudMainModel)
        {
            _onlyCloudMainModel = onlyCloudMainModel;
            _deleteFolder = new DeleteFolderFileSystem(_onlyCloudMainModel.sqlliteController);
            _deleteFolder.eventDeleteFileSystem += delEvent_NewDel;
            _stackUpateTreeViewIcon = new StackUpdateTreeViewIcon(_onlyCloudMainModel.convertIcon);
            
        }

      
        public void cloud(List<string> listFolderPath)
        {
            try
            {
                List<long[][]> allListFull = getAllChildren(listFolderPath);
                _deleteFolder.deleteAllPcSave(allListFull);
            }
            catch(Exception z)
            {
                _onlyCloudMainModel._exceptionController.sendError((int)CodeError.FILE_SYSTEM_EXCEPTION, "OnlyCloudController->cloud: Критическая ошибка!!! ", "Проблема с файловой системой: " + z.ToString());
            }
            
        }
        
        void delEvent_NewDel(object sender, OnlyCloudDeleteFileSystem e)
        {
            //Console.WriteLine("Сработка на удаления файла или папки "+ e.listFilesRowid);
            deleteSqlPcSave(e.listFilesRowid);
            _stackUpateTreeViewIcon.TraverseTreeSingl(_onlyCloudMainModel.viewTreeList, e.listFilesRowid, staticVariable.Variable.normalIconId);
            _stackUpateTreeViewIcon.TraverseViewSingl(_onlyCloudMainModel.viewFolderList, e.listFilesRowid, staticVariable.Variable.normalIconId);
        }

        private void deleteSqlPcSave(long listFilesRowId)
        {
            _onlyCloudMainModel.sqlliteController.deleteSavePcSinglRows(listFilesRowId);
        }








        private List<long[][]> getAllChildren(List<string> listFolderPath)
        {
            List<long[][]> allListFull = new List<long[][]>();

            foreach (string folderPath in listFolderPath)
            {
                string path = folderPath;
                allListFull = getAllChildren(createChildrenModel(ref allListFull, ref path, _onlyCloudMainModel.sqlliteController));
            }

            return allListFull;
        }


        private List<long[][]> getAllChildren(ScannerSqlliteRecursiveAllChildrenModel ssracm)
        {
            ScannerSqlliteRecursiveAllChildren ssrach = new ScannerSqlliteRecursiveAllChildren();
            List<long[][]> list = ssrach.getScanAllChildren(ssracm);
            ssrach = null;
            ssracm = null;

            return list;

        }


        private ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(ref List<long[][]> allListFull, ref string location, SqliteController _sqlliteController)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();

            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = _sqlliteController;
            model.progress = new Progress<CopyViewModel>();
            model.progressLabel = "search children";
            //все записи из базы
            model.allParent = _sqlliteController.getSelect().getAllParentListRowId();
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();
            return model;
        }
    }
}
