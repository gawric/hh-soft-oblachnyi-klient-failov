﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer
{
    public  class PauseUpload : IContextMenuPause
    {
        private BlockingEventController _blockingController;
        private SupportViewTransfer _supportViewTransfer;

        public PauseUpload(BlockingEventController _blockingController , SupportViewTransfer _supportViewTransfer)
        {
            this._blockingController = _blockingController;
            this._supportViewTransfer = _supportViewTransfer;
        }

        private void startPause(bool[] block, FolderNodesTransfer itemList, BlockingEventController _blockingController)
        {
            
                if (block[0])
                {
                    //вызывает блокировку
                    _blockingController.getManualReset(staticVariable.Variable.blockingUploadTokenId).resetMre();
                    itemList.UploadStatus = "Пауза";
          
                }
                else
                {
                    //снимает блокировку
                    _blockingController.getManualReset(staticVariable.Variable.blockingUploadTokenId).setMre();
                    itemList.UploadStatus = "Загрузка";
                }

          
        }

        public void startTrainingPause(IList list, bool[] block , FolderNodesTransfer itemList)
        {
            try
            {

                if (list.Count == 1)
                {
                    

                    //false - не закончено и нет ошибок
                    if (!_supportViewTransfer.isFinish(itemList.UploadStatus))
                    {

                        startPause(block, itemList , _blockingController);
                    }
                    else
                    {
                        Console.WriteLine("ViewTransfer->PauseUploadContextMenu_Click: Невозможно остановить выполненую операцию");
                    }


                }
                else
                {
                    Console.WriteLine("ViewTransfer->PauseUploadContextMenu_Click: Ошибка !!! Нельзя выбрать больше 1 позиции");
                }
            }
            catch (System.InvalidCastException a)
            {
                Console.WriteLine("ViewTransfer->PauseUploadContextMenu_Click:  " + a.ToString());
            }

        }

      




        //false - отменить блокировку
        //true - вкл блокировку
        public bool isBlockingUpload(string text)
        {

            return _supportViewTransfer.isBlocking(text);
        }

        

    }
}
