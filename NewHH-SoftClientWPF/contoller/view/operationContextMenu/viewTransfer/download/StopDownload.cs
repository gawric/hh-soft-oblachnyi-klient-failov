﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer
{
    public class StopDownload : IContextMenuStop
    {
        
            private CancellationController _cancellationController;
            private SupportViewTransfer _supportViewTransfer;

            public StopDownload(CancellationController _cancellationController, SupportViewTransfer _supportViewTransfer)
            {
                this._cancellationController = _cancellationController;
                this._supportViewTransfer = _supportViewTransfer;
            }

            public void startTrainingStop(IList list, bool[] block, FolderNodesTransfer itemList)
            {
                if (list.Count == 1)
                {
                    // FolderNodesTransfer itemList = (FolderNodesTransfer)ListView.SelectedItem;

                    if (!_supportViewTransfer.isFinish(itemList.UploadStatus))
                    {
                        //contextMenuChangeStopName(block[0]);
                        startStop(block, itemList);
                    }
                    else
                    {
                        Console.WriteLine("ViewTransfer->StopDownloadContextMenu_Click: Невозможно остановить выполненую операцию");
                    }
                }
                else
                {
                    Console.WriteLine("ViewTransfer->StopDownloadContextMenu_Click: Ошибка !!! Нельзя выбрать больше 1 позиции");
                }
            }


            private void startStop(bool[] block, FolderNodesTransfer itemList)
            {

                if (block[0])
                {
                    //вызывает блокировку
                    _cancellationController.startCansel(staticVariable.Variable.downloadCancelationTokenId);
                    block = null;
                }


            }


            public bool isStopUpload(string text)
            {

                if (!text.Equals("Стоп"))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }



        }
    }

