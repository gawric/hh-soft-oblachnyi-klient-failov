﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NewHH_SoftClientWpfTest.controller.core.network
{
    [TestClass]
    public class SaveJsonToDiskContollerTest
    {
        [TestMethod]
        public void SaveFimToTemp()
        {
            SaveJsonToDiskContoller jsonDisk = new SaveJsonToDiskContoller(InitalizingUnitTest.container);
            jsonDisk.saveGetLazyJsonArray(GetFim());
            List<string> listhref = jsonDisk.getListGetLazyTempHref();

            Assert.AreEqual(true, isExist(listhref));
        }

        private bool isExist(List<string> listhref)
        {
            bool check = false;
            if (File.Exists(listhref[0])) check = true;

            return check;
        }

        private FileInfoModel[] GetFim()
        {
            FileInfoModel[] fimArr = new FileInfoModel[10];
            fimArr[0] = GetModel();

            return fimArr;
        }
           
        private FileInfoModel GetModel()
        {
            FileInfoModel model = new FileInfoModel();
            model.filename = "test";
            model.location = "test";
            model.parent = 2;
            model.row_id = 92;
            model.type = "folder";

            return model;
        }
    }
}
