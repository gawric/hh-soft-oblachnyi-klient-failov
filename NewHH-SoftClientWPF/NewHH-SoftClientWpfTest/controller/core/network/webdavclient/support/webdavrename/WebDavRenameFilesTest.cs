﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Text;


namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.webdavrename
{
 
    [TestClass]
    public class WebDavRenameFilesTest
    {


        [TestMethod]
        public void TrainingRename()
        {
            ExceptionController exceptionContoller = new ExceptionController(null);
            IRenameFiles trainingRenameFiles = new TrainingRenameFiles(ref exceptionContoller);
            RenameWebDavModel model = createRenameModel();
            trainingRenameFiles.startTraining(model, createList(), createController());
            Assert.AreEqual(false, model.allStatus.isRename);
        }

       //уже отдельно тестирует в бругих модульных тестах!!!
        public void MoveFS()
        {
            //ExceptionController exceptionContoller = new ExceptionController(null);
            //IRenameFiles trainingRenameFiles = new StartRenameToFileSystem(ref exceptionContoller);
           // RenameWebDavModel model = createRenameModel();
           // trainingRenameFiles.startTraining(model, createList(), createController());
           // Assert.AreEqual(false, model.allStatus.isRename);
        }

        private RenameWebDavModel createRenameModel()
        {
         
            RenameWebDavModel model = new RenameWebDavModel();
            model.oldLocation = "/admin/oldlocation";
            model.allStatus = createModelStatus();
            return model;
        }
        private StatusAllOperationModel createModelStatus()
        {
            StatusAllOperationModel model = new StatusAllOperationModel();

            return model;
        }
        private List<FileInfoModel> createList()
        {
            List<FileInfoModel> list = new List<FileInfoModel>();
            list.Add(getFiM());
            return list;
        }

        private RenameFolderController createController()
        {
            RenameFolderController rfc = new RenameFolderController(null);

            return rfc;
        }

        private FileInfoModel getFiM()
        {
            FileInfoModel fim = new FileInfoModel();
            fim.row_id = 1;
            fim.filename = "test";
            fim.parent = -1;

            return fim;
        }
    }
}
