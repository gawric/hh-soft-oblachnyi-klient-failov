﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.genNewId;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.scanwebdav.geNewId
{
    [TestClass]
    public class GenNewIdFimTest
    {
        [TestMethod]
        public void GenFiles()
        {
            string g3 = "DebugUpdate.zip";
            FileInfoModel[] arr = new FileInfoModel[1];
            int cnt = 0;
            FileInfoModel currentNodes = createFilesModel();
            arr[0] = currentNodes;
            PasteFilesAsyncModel pasteModel = createFilesPasteModel();
            string pasteLocation = "https://hh-soft.ru:9091/adminwebdav/admin/video/";

            new GenNewIdFim().genNewLocation(ref  g3, ref  arr, ref  cnt, ref currentNodes, ref pasteModel , pasteLocation);
            bool isOk = false;

            if (arr[0].location.Equals("https://hh-soft.ru:9091/adminwebdav/admin/video/DebugUpdate.zip")) isOk = true;

            Assert.AreEqual(true, isOk);
        }

        [TestMethod]
        public void GenFolder()
        {
            string g3 = "";
            FileInfoModel[] arr = new FileInfoModel[1];
            int cnt = 0;
            FileInfoModel currentNodes = createFolderModel();
            arr[0] = currentNodes;
            PasteFilesAsyncModel pasteModel = createFolderPasteModel();
            string pasteLocation = "https://hh-soft.ru:9091/adminwebdav/admin/video/";

            new GenNewIdFim().genNewLocation(ref g3, ref arr, ref cnt, ref currentNodes, ref pasteModel, pasteLocation);
            bool isOk = false;

            if (arr[0].location.Equals("https://hh-soft.ru:9091/adminwebdav/admin/public/mickrobackup/")) isOk = true;

            Assert.AreEqual(true , isOk);
        }
        private FileInfoModel createFolderModel()
        {
            FileInfoModel currentNodes = new FileInfoModel();
            currentNodes.changeRows = "CREATE";
            currentNodes.parent = 21;
            currentNodes.row_id = 22;
            currentNodes.user_id = 1;
            currentNodes.attribute = ".rar";
            currentNodes.type = "folder";
            currentNodes.location = "https://hh-soft.ru:9091/adminwebdav/admin/public/mickrobackup/";
            currentNodes.filename = "mickrobackup";
            return currentNodes;
        }

        private FileInfoModel createFilesModel()
        {
            FileInfoModel currentNodes = new FileInfoModel();
            currentNodes.changeRows = "CREATE";
            currentNodes.parent = 21;
            currentNodes.row_id = 22;
            currentNodes.user_id = 1;
            currentNodes.attribute = ".rar";
            currentNodes.type = ".rar";
            currentNodes.location = "https://hh-soft.ru:9091/adminwebdav/admin/public/DebugUpdate.zip";
            currentNodes.filename = "DebugUpdate.zip";
            return currentNodes;
        }

        private PasteFilesAsyncModel createFilesPasteModel()
        {
            PasteFilesAsyncModel pasteModel = new PasteFilesAsyncModel();
            pasteModel.copyOrCutRootLocation = "https://hh-soft.ru:9091/adminwebdav/admin/public/";
            pasteModel.copyLocation = "https://hh-soft.ru:9091/adminwebdav/admin/public/";
            pasteModel.copyFolderName = "DebugUpdate.rar";
            pasteModel.type = ".rar";

            return pasteModel;
        }

        private PasteFilesAsyncModel createFolderPasteModel()
        {
            PasteFilesAsyncModel pasteModel = new PasteFilesAsyncModel();
            pasteModel.copyOrCutRootLocation = "https://hh-soft.ru:9091/adminwebdav/admin/public/";
            pasteModel.copyLocation = "https://hh-soft.ru:9091/adminwebdav/admin/public/";
            pasteModel.copyFolderName = "mickrobackup";
            pasteModel.type = "folder";

            return pasteModel;
        }


    }

   
}
