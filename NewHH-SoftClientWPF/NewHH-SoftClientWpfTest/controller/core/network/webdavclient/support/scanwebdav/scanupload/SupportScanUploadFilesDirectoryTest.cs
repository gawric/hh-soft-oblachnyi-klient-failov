﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.scanupload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.scanwebdav.scanupload
{
    [TestClass]
    public class SupportScanUploadFilesDirectoryTest
    {
        [TestMethod]
        public void forDi()
        {
            SupportScanUploadFilesDirectory scanUpload = new SupportScanUploadFilesDirectory();
            ScanUploadWebDavModel uploadFolderModel = createModel();
            int[] cont = { 4 };

            scanUpload.forDi(getFi(),  uploadFolderModel, ref  cont);

         
        }

        [TestMethod]
        public void runTest()
        {
            SupportScanUploadFilesDirectory scanUpload = new SupportScanUploadFilesDirectory();
            ScanUploadWebDavModel uploadFolderModel = createModel();
            int[] cont = { 4 };


            scanUpload.run(ref getFi()[0], uploadFolderModel, ref cont);

             bool isOk = false;
             if (uploadFolderModel.arr[4] != null) isOk = true;

             Assert.AreEqual(true, isOk);
        }


        private ScanUploadWebDavModel createModel()
        {
            ScanUploadWebDavModel model = new ScanUploadWebDavModel();

            model.pasteModel = getFiM();
            model.clientLocalDirectory = "\\test\\admin";
            model.clientRootLocalDirectory = "\\test";
            model.checkParent = new SotredSourceHrefToDouble();
            model._blockingEventController = new BlockingEventController(InitalizingUnitTest.container);
            model.arr = new FileInfoModel[5];
            model.currentCountFilesToFolder = new int[1] { 0 };
            model._cancellationController = new CancellationController(InitalizingUnitTest.container);
            model.new_id_container = new long[1];
            return model;
        }


        private IFileInfoProxy[] getFi()
        {

            Mock<IFileInfoProxy> mock = new Mock<IFileInfoProxy>();

            mock.Setup(a => a.LastAccessTime())
                .Returns(new DateTime());

            mock.Setup(m => m.LastWriteTime())
               .Returns(new DateTime());

            mock.Setup(m => m.CreationTime())
               .Returns(new DateTime());

            mock.Setup(m => m.Name())
             .Returns("Test");

            mock.Setup(m => m.FullName())
           .Returns("C:\\Test");

            IFileInfoProxy[] ar = new IFileInfoProxy[1];
            ar[0] = mock.Object;

            return ar;
        }




        private FileInfoModel getFiM()
        {
            FileInfoModel fim = new FileInfoModel();
            fim.row_id = 1;
            fim.filename = "test";
            fim.parent = -1;

            return fim;
        }
      
       
        private IDirectoryInfoProxy getDi()
        {
            Mock<IDirectoryInfoProxy> mock = new Mock<IDirectoryInfoProxy>();

            mock.Setup(a => a.LastAccessTime())
                .Returns(new DateTime());

            mock.Setup(m => m.LastWriteTime())
               .Returns(new DateTime());

            mock.Setup(m => m.CreationTime())
               .Returns(new DateTime());

            mock.Setup(m => m.Name())
            .Returns("Test");

            return mock.Object;
        }
    }
}
