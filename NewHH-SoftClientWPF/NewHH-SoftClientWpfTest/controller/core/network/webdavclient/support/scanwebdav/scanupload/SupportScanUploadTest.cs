﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.scanupload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WebDAVClient;

namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.scanwebdav.scanupload
{
    [TestClass]
    public class SupportScanUploadTest
    {
        [TestMethod]
        public void multiUpdate()
        {
            bool[] stop = { false };
           // Client client = new Client();
            SupportScanUpload scanUpload = new SupportScanUpload(ref stop, null);
            ScanUploadWebDavModel uploadFolderModel = createModel();
            int[] cont = { 4 };

            scanUpload.multiUpdate(uploadFolderModel, getFi(),  cont, getDi()).Wait();


            bool isOk = false;
            if (uploadFolderModel.arr[4] != null) isOk = true;

            Assert.AreEqual(true, isOk);
        }


        [TestMethod]
        public void singlUpdate()
        {
            bool[] stop = { false };
            // Client client = new Client();
            SupportScanUpload scanUpload = new SupportScanUpload(ref stop, null);
            ScanUploadWebDavModel uploadFolderModel = createModel();
            int[] cont = { 4 };

            scanUpload.singleUpdate(uploadFolderModel, getFi(), cont).Wait();

            bool isOk = false;
            if (uploadFolderModel.arr[4] != null) isOk = true;

            Assert.AreEqual(true, isOk);
        }


        private ScanUploadWebDavModel createModel()
        {
            ScanUploadWebDavModel model = new ScanUploadWebDavModel();

            model.pasteModel = getFiM();
            model.clientLocalDirectory = "\\test\\admin";
            model.clientRootLocalDirectory = "\\test";
            model.checkParent = new SotredSourceHrefToDouble();
            model._blockingEventController = new BlockingEventController(InitalizingUnitTest.container);
            model.arr = new FileInfoModel[5];
            model.currentCountFilesToFolder = new int[1] { 0 };
            model._cancellationController = new CancellationController(InitalizingUnitTest.container);

            return model;
        }

       
       

        private FileInfoModel getFiM()
        {
            FileInfoModel fim = new FileInfoModel();
            fim.row_id = 1;
            fim.filename = "test";
            fim.parent = -1;

            return fim;
        }
        private Client getClient()
        {
            var mock = new Mock<Client>();

            return mock.Object;
        }
        private IDirectoryInfoProxy getFi()
        {
          
            Mock<IDirectoryInfoProxy> mock = new Mock<IDirectoryInfoProxy>();

            mock.Setup(a => a.LastAccessTime())
                .Returns(new DateTime());

            mock.Setup(m => m.LastWriteTime())
               .Returns(new DateTime());

            mock.Setup(m => m.CreationTime())
               .Returns(new DateTime());

            mock.Setup(m => m.Name())
             .Returns("Test");

            //IDirectoryInfoProxy s = new DirectoryInfoProxy("C:\\");
            return mock.Object;
        }

        private IDirectoryInfoProxy getDi()
        {
            Mock<IDirectoryInfoProxy> mock = new Mock<IDirectoryInfoProxy>();

            mock.Setup(a => a.LastAccessTime())
                .Returns(new DateTime());

            mock.Setup(m => m.LastWriteTime())
               .Returns(new DateTime());

            mock.Setup(m => m.CreationTime())
               .Returns(new DateTime());

            mock.Setup(m => m.Name())
            .Returns("Test");

            return mock.Object;
        }
    }
}
