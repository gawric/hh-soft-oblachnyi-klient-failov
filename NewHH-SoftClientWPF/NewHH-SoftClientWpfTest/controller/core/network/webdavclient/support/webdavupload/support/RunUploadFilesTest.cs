﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Text;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.webdavupload.support
{
    [TestClass]
    public class RunUploadFilesTest
    {
        [TestMethod]
        public void TestRunUpload()
        {
            
            RunUploadFiles ruf = new RunUploadFiles(new ExceptionController(null));
            UploadWebDavModel uploadModel = createModel();
            
            string href = "C:\\123.txt";
            int[] cnt = { 4 };
            long version = 4;
            ruf.run(ref uploadModel, ref href, ref cnt, ref version , creatFip());

            bool check = false;

            if(uploadModel._statusDownload != null)
            {
                check = true;
            }


            Assert.AreEqual(true, check);

        }
        private IFileInfoProxy creatFip()
        {
            Mock<IFileInfoProxy> mock = new Mock<IFileInfoProxy>();

            mock.Setup(a => a.Name()).Returns("123.txt");
            mock.Setup(a => a.LastWriteTime()).Returns(new DateTime());
            mock.Setup(a => a.LastAccessTime()).Returns(new DateTime());
            mock.Setup(a => a.CreationTime()).Returns(new DateTime());
            mock.Setup(a => a.Length()).Returns(13L);

            return mock.Object;

        }
        private UploadWebDavModel createModel()
        {
            UploadWebDavModel  model = new UploadWebDavModel();
            model.dictUploadHrefRow_id = new  Dictionary<string, long>();
            model.pasteModel = createFim();
            model.checkParent = new SotredSourceHrefToDouble();
            model._blockingEventController = new BlockingEventController(InitalizingUnitTest.container);
            model._cancellationController = new CancellationController(InitalizingUnitTest.container);
            return model;
        }

        private FileInfoModel createFim()
        {
            FileInfoModel fim = new FileInfoModel();
            fim.row_id = 0;
            fim.parent = -1;
            fim.filename = "123.txt";
            fim.location = "/test";

            return fim;
        }
    }
}
