﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.webdavupload
{
    [TestClass]
    public class WebDavUploadFilesTest
    {
        [TestMethod]
        public void TestRun1PathFiles()
        {
            IWebDavUpload wduf = new WebDavUploadFiles(new ExceptionController(null));
            wduf.uplodFilesFirst(getFiles() , createModel());
        }

        
        private UploadWebDavModel createModel()
        {
            return new UploadWebDavModel();
        }

        private string[] getFiles()
        {
            string[] arrStr = { "C:\\123.txt " };
            return arrStr;
        }
    }
}
