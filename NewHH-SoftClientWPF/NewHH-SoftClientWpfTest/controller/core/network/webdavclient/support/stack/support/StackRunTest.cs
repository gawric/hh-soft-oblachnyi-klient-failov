﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewHH_SoftClientWPF;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using static NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support.StackRun;

namespace NewHH_SoftClientWpfTest.controller.core.network.webdavclient.support.stack.support
{
    [TestClass]
    public class StackRunTest
    {
        bool isOkUpload = false;

       // [TestMethod]
        public void RunStackSize1Upload()
        {
            // Mock<Container> mock = new Mock<Container>();
            StackRun testRun = new StackRun();
            testRun.eventRebuilding += eventRebuilding;

            BlockingCollection<StackNetworkModel> _dirs = new BlockingCollection<StackNetworkModel>();
            _dirs.Add(createModelUpload());

            bool isRunningStack = false;
            bool isStopStack = false;
            
            testRun.startStackNetwork(ref _dirs, ref isRunningStack, ref isStopStack, createObjectModel());
            Assert.AreEqual(true, isOkUpload);
        }

     //   [TestMethod]
        public void RunStackSize1Download()
        {
            // Mock<Container> mock = new Mock<Container>();
            StackRun testRun = new StackRun();

            BlockingCollection<StackNetworkModel> _dirs = new BlockingCollection<StackNetworkModel>();
            _dirs.Add(createModelUpload());

            bool isRunningStack = false;
            bool isStopStack = false;

            testRun.startStackNetwork(ref _dirs, ref isRunningStack, ref isStopStack, createObjectModel());
            Assert.AreEqual(true, isOkUpload);
        }

        private StackNetworkModel createModelUpload()
        {
            Mock<List<FolderNodes>> mock = new Mock<List<FolderNodes>>();

            StackNetworkModel model = new StackNetworkModel();
            model.listDownload = mock.Object;
            model.isViewFolder = false;
            model.pasteFolder = createPasteModel();
            model.dictHrefUploadRowId = new Dictionary<string, long>();
            model.countDownload = 1;

            return model;
        }

      

        private StakRunObjectModel createObjectModel()
        {
            StakRunObjectModel modelobj = new StakRunObjectModel();

            return modelobj;
        }

        private FileInfoModel createPasteModel()
        {
            FileInfoModel modelPaste = new FileInfoModel();
            modelPaste.row_id = 1;
            modelPaste.location = "/pasteModel/location";
            modelPaste.parent = -1;

            return modelPaste;
        }

   

        void eventRebuilding(object sender, DataEventHandler e)
        {
            isOkUpload = true;
            Debug.Print("StackRunTest->eventRebuilding");
        }
    }
}
