﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.blockThread.manualReset;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest.controller.core.blockThread
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]
        public void TestMethod3()
        {
            Container cont = InitalizingUnitTest.container;
            BlockingEventController blockingTest = new BlockingEventController(cont);
            blockingTest.createManualResetEvent("Test");
            ManualReset mr = blockingTest.getManualReset("Test");

            bool chec = false;
            if (mr != null) chec = true;

            Assert.AreEqual(true, chec);
        }
    }
}
