﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.cancellation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace NewHH_SoftClientWpfTest.controller.core.cancellation
{
    [TestClass]
    public class CancellationControllerTest
    {
        string testName = "testCancellation";

        [TestMethod]
        public void AddCancellationTest()
        {
            CancellationController cancellation = GetCancellation();
            CancellationTokenSource canselSource = GetTokenObject(cancellation);
            cancellation.addWarehouse(testName, canselSource);
            CancellationToken token = cancellation.getCancellationTokenToWarehouse(testName);

            bool check = false;

            if (token != null) check = true;

            Assert.AreEqual(true, check);
        }

        [TestMethod]
        public void RemoveCancellationTest()
        {
            CancellationController cancellation = GetCancellation();
            CancellationTokenSource canselSource = GetTokenObject(cancellation);

            int nameObj = canselSource.Token.GetHashCode();
            cancellation.addWarehouse(testName, canselSource);
            cancellation.removeObjectWarehouse(testName);
            CancellationToken token = cancellation.getCancellationTokenToWarehouse(testName);

            int nameObjRemove = token.GetHashCode();
            bool check = false;

            if (nameObj != nameObjRemove) check = true;

            Assert.AreEqual(true, check);
        }

        [TestMethod]
        public void StartCancel()
        {
            CancellationController cancellation = GetCancellation();
            CancellationTokenSource canselSource = GetTokenObject(cancellation);
            cancellation.addWarehouse(testName, canselSource);
            cancellation.startCansel(testName);

            CancellationToken token = cancellation.getCancellationTokenToWarehouse(testName);

            Assert.AreEqual(true, token.IsCancellationRequested);
        }

        [TestMethod]
        public void StartAllCancel()
        {
            CancellationController cancellation = GetCancellation();
            CancellationTokenSource canselSource = GetTokenObject(cancellation);
            cancellation.addWarehouse(testName, canselSource);
            cancellation.startAllCansel();

            CancellationToken token = cancellation.getCancellationTokenToWarehouse(testName);

            Assert.AreEqual(true, token.IsCancellationRequested);
        }

        private CancellationController GetCancellation()
        {
            return   new CancellationController(InitalizingUnitTest.container);
        }
        private CancellationTokenSource GetTokenObject(CancellationController cancellation)
        {
            return new CancellationTokenSource();
        }
    }
}
