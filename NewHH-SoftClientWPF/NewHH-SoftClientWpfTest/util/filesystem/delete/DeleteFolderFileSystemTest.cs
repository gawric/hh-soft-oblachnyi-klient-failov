﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.util.filesystem.delete;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest.util.filesystem.delete
{
    [TestClass]
    public class DeleteFolderFileSystemTest
    {
        [TestMethod]
        public void Delete()
        {
            DeleteFolderFileSystem deleteFolder = new DeleteFolderFileSystem(null);
            string locationWebDav = "";
            string locationDownload = "C:\\download\\";
            long listFilesRowId = 19;

            deleteFolder.eventDeleteFileSystem += EventDelete;
            deleteFolder.delete(ref  locationWebDav, ref  locationDownload, listFilesRowId, true);
           
        }

        void EventDelete(object sender, OnlyCloudDeleteFileSystem e)
        {
            bool check = false;
            if (19 == e.listFilesRowid) check = true;

            Assert.AreEqual(true , check);
        }
    }
}
