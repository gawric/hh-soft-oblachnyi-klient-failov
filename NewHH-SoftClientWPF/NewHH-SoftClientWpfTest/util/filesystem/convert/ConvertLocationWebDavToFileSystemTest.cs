﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.blockThread.manualReset;
using NewHH_SoftClientWPF.contoller.util.filesystem.convert;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest.util.filesystem.convert
{
    [TestClass]
    public class ConvertLocationWebDavToFileSystemTest
    {

        [TestMethod]
        public void ConvertStrHttpToLocalPath()
        {
            ConvertLocationWebDavToFileSystem clwdtf = new ConvertLocationWebDavToFileSystem();
            string locationWebDav = "https://hh-soft.ru:9091/adminwebdav/admin/programm/revit/2017/";
            string locationdownload = "C:\\download";
            string convertFileSystem = clwdtf.convertLocationToHref(locationWebDav, locationdownload);
            bool check = false;

            if (convertFileSystem.Equals("C:\\download\\adminwebdav\\admin\\programm\\revit\\2017\\")) check = true;
           

            Assert.AreEqual(true, check);
        }
    }
}
