﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.sync.stack;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using System;
using System.Collections.Generic;
using System.Text;
using WebDAVClient.Model;

namespace NewHH_SoftClientWpfTest.util.sync.stack
{
    [TestClass]
    public class WorkerStackTest
    {
        [TestMethod]
        public void TestWorker()
        {
            FileInfoModel[] containerAllFiles = new FileInfoModel[5];
            WorkerSteck stack = createWorkerSteck(containerAllFiles);

            TraverseTreeModel treeObjModel = createTraverse();

            Dictionary<long, long> sqlArray = createSql();
            Dictionary<string, long> sqlArrayHref = createArrayHref();
            IEnumerable<Item> webAll = getWebAll();

            stack.goWorking(treeObjModel, sqlArray,  sqlArrayHref, webAll);



            Assert.IsNotNull(containerAllFiles[1]);
        }

        private Dictionary<string, long> createArrayHref()
        {
            return new Dictionary<string, long>();
        }
        private Dictionary<long, long> createSql()
        {
            return new Dictionary<long, long>();
        }
        private TraverseTreeModel createTraverse()
        {
            TraverseTreeModel treeObjModel = new TraverseTreeModel();
            treeObjModel.sqlArrayHref = new Dictionary<string, long>();

            treeObjModel.countArray = new int[1] { 1 };
            treeObjModel.progress = new Progress<double>();

            return treeObjModel;
        }
        private WorkerSteck createWorkerSteck(FileInfoModel[] containerAllFiles)
        {
            Dictionary<string, long> parentArray = new Dictionary<string, long>();
            parentArray.Add("/test/local/", 2);
      
            FileInfoModelSupport modelSupport = new FileInfoModelSupport();
            Stack<string> dirs = new Stack<string>();

            WorkerSteck stack = new WorkerSteck(parentArray, containerAllFiles, modelSupport, dirs);

            return stack;
        }

        private IEnumerable<Item> getWebAll()
        {
           
            Item it = new Item();
            it.Href = "/test/local/test12/local";
            it.DisplayName = "testDisplayName";
            it.ContentType = "files";

            IEnumerable<Item> values = new Item[] { it };


            return values;
        }
    }
}
