﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.staticVariable;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest.view.operationContextMenu.create
{
    [TestClass]
    public class TestCreateFolderController
    {
        [TestMethod]
        public void TestRunUpload()
        {
            //not support System.WebHttpException
            ICreateFolder cc = new CreateFolderController(null);
            cc.TrainingCreateFolder(GetSourceFim() , "Test");

        }

        private FileInfoModel GetSourceFim()
        {
            return UtilMethod.getRootFolderNodes();
        }
       

        
    }
}
