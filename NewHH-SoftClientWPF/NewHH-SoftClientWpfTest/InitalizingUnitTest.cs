﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Text;

namespace NewHH_SoftClientWpfTest
{
    [TestClass]
    public static class InitalizingUnitTest
    {
        public static Container container;

        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            container = Bootstrap();
        }

        private static Container Bootstrap()
        {
           
            // Create the container as usual.
            var container = new Container();
        

             // container.Register<MainWindowViewModel>(Lifestyle.Singleton);
             // container.Register<ViewFolderViewModel>(Lifestyle.Singleton);
            //  container.Register<ExceptionController>(Lifestyle.Singleton);
            //  container.Register<ConvertIconType>(Lifestyle.Singleton);
            // container.Register<ObserverCopyFormUpdate>(Lifestyle.Singleton);
            // container.Register<CreateSubscriptionLoginForm>(Lifestyle.Singleton);
            // container.Register<FileInfoModelSupport>(Lifestyle.Singleton);
            //   container.Register<ErrorController>(Lifestyle.Singleton);
            // container.Register<UpdateFormMainController>(Lifestyle.Singleton);
            // container.Register<StatusSqlliteTransaction>(Lifestyle.Singleton);
            // container.Register<UpdateViewTransferController>(Lifestyle.Singleton);
            // container.Register<ObserverNetwokSendJson>(Lifestyle.Singleton);
            // container.Register<SqliteController>(Lifestyle.Singleton);
            //container.Register<FirstSyncFilesController>(Lifestyle.Singleton);
            //container.Register<UpdateNetworkJsonController>(Lifestyle.Singleton);
            //container.Register<UpdateFormCopyWindowsController>(Lifestyle.Singleton);
            //container.Register<TransportStatusMqClient>(Lifestyle.Singleton);
            //container.Register<ScannerConnectServerController>(Lifestyle.Singleton);
            //container.Register<WebDavClientController>(Lifestyle.Singleton);
            //container.Register<MqClientController>(Lifestyle.Singleton);


            //container.Register<CopyStatusController>(Lifestyle.Singleton);
            // container.Register<OverwriteWindowModel>(Lifestyle.Singleton);
            //container.Register<OverwriteWindow>(Lifestyle.Singleton);
            //container.Register<MainWindowViewModel>(Lifestyle.Singleton);



            container.Verify();

            return container;
        }

        

    }

}
