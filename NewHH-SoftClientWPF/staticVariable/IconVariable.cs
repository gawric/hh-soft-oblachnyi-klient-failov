﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.staticVariable
{
    public static class IconVariable
    {
        public static string textMenuStopClearScan = "Остановить Cканирование";
        public static string textMenuStartClearScan = "Запустить Сканирование";
        public static string itemMenuStartClearScan = "StartClearScanMenuItem";
        public static string iconMenuStartClearScan = "StartClearScan";
        public static string iconTypeStop = ".contextMenuStop";
        public static string iconTypeStart = ".contextMenuStart";

        public static string textMenuStopAutoScan = "Остановить Автосканирование";
        public static string textMenuStartAutoScan = "Запустить Автосканирование";
        public static string itemMenuStartAutoScan = "StartAutoScanMenuItem";
        public static string iconMenuStartAutoScan = "StartAutoScan";

    }
}
