﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.staticVariable
{
    public static class UtilMethod
    {

        public static string GetMachineName()
        {
            return "MyPc"+Environment.MachineName;
        }
        //конвертор байтов
        public static string FormatBytes(long bytes)
        {
            string[] Suffix = { "Б", "КБ", "МБ", "ГБ", "ТБ" };
            int i;
            double dblSByte = bytes;
            for (i = 0; i < Suffix.Length && bytes >= 1024; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }

            return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
        }

        //конвертор байтов
        public static double FormatBytesNoSuffix(long bytes)
        {
                return bytes / 1024.0 / 1024.0 / 1024.0;
        }

        public static class BitmapConversion
        {
            public static BitmapImage BitmapToBitmapSource(Uri uriSource)
            {
                // return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                //  source.GetHbitmap(),
                //  IntPtr.Zero,
                //  Int32Rect.Empty,
                //  BitmapSizeOptions.FromEmptyOptions());

                BitmapImage source2 = new BitmapImage(uriSource);
                using (Stream stream = source2.StreamSource)
                {
                    try
                    {

                        source2.StreamSource = stream;
                        source2.CacheOption = BitmapCacheOption.None;    // not a mistake - see below

                    }
                    catch (System.InvalidOperationException a)
                    {
                        Console.WriteLine("Variable->BitmapToBitmapSource " + a);
                    }

                }

                return source2;
            }
        }

        public static UIElement GetByUid(DependencyObject rootElement, string uid)
        {
            foreach (UIElement element in LogicalTreeHelper.GetChildren(rootElement).OfType<UIElement>())
            {
                if (element.Uid == uid)
                    return element;
                UIElement resultChildren = GetByUid(element, uid);
                if (resultChildren != null)
                    return resultChildren;
            }
            return null;
        }

        public static bool isDisk(ref string copyOrCutRootLocation)
        {
            bool check = false;
            if (copyOrCutRootLocation.Equals(Variable.nameRootDisk) || copyOrCutRootLocation.Equals(Variable.nameRootLocationDisk)) check = true;

            return check;

        }

        public static bool isRootId(long row_id)
        {
            bool check = false;
            if (row_id.Equals(-1)) check = true;

            return check;
        }

        public static FileInfoModel getRootFolderNodes()
        {
            FileInfoModel fol = new FileInfoModel();
            fol.row_id = -1;
            fol.filename = "Disk";
            string empty = "";
            fol.location = getRootLocation(true, ref empty);
            fol.parent = -2;
            fol.type = "folder";

            return fol;
        }
        public static string getRootLocation(bool isDisk, ref string copyOrCutRootLocation)
        {

            if (isDisk)
            {
                copyOrCutRootLocation = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;
            }

            return copyOrCutRootLocation;
        }

        public static string getRootLocationAutoSaveFolder()
        {
           string machinename = GetMachineName();
           string empty = "";
           return Variable.EscapeUrlString(staticVariable.UtilMethod.getRootLocation(true, ref empty) + machinename);
        }

        public static string convertLocationToHref(string parentLocation, string location)
        {
            if (parentLocation != null)
            {
                string remoteWebDav = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;


                string hrefLocal = parentLocation.Replace(remoteWebDav, location + "\\");
                hrefLocal = hrefLocal.Replace("/", "\\");

                string hrefLocalDecode = staticVariable.Variable.DecodeUrlString(hrefLocal);


                return hrefLocalDecode;
            }
            else
            {
                Console.WriteLine("ExistSavePcEvent->convertLocationToHref: parentLocation не определен!!!" + parentLocation);
                return "";
            }

        }


    }
}
