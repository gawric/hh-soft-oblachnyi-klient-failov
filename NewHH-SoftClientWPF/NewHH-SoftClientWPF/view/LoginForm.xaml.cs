﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.loginWindow;
using NewHH_SoftClientWPF.staticVariable;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF
{
    //статический обьект с переменными
    //Variable
   

    public partial class LoginForm : Window
    {

        //MVVM - полная модель всей формы через нее можно обновлять модель формы
        private LoginWindowModel _loginWindowModel;
        private LoginFormViewModel _viewModel;

        public LoginForm(LoginWindowModel loginWindowModel)
        {
            _loginWindowModel = loginWindowModel;
            InitializeComponent();

          
            _viewModel = loginWindowModel._loginFormViewModel;
            DataContext = _viewModel;
           _viewModel.OnRequestClose += (s, e) => this.Hide();
           _viewModel.OnRequestCreate += (s, e) => this.ShowDialog();

            this.Loaded += loginWindow_load;





        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // e.Cancel = true;
            _loginWindowModel.updateForm.updateCloseLoginForm();
      
            //запускаем сканирование
            _loginWindowModel._blockingEventController.setMRE(staticVariable.Variable.scannerConnectServerControllerTokenId);
        }



        //Реализация подключения 
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _loginWindowModel._blockingEventController.resetMRE(staticVariable.Variable.scannerConnectServerControllerTokenId);
                updateLoginFormClear();

                string username = _viewModel.Username;
                string password = _viewModel.Password;

                updateLoginFormConnect();



                if (username != null & password != null)
                {
                    _loginWindowModel.updateForm.updatePgLoginForm("40");
                    bool isAuth = await _loginWindowModel._mqClient.CreateNewConnectMqClientAsync(username, password, _loginWindowModel._firstSyncController);

                    WebDavClientController _webDavClient = _loginWindowModel._webDavClient;
                    MqClientController _mqClient = _loginWindowModel._mqClient;
                    checkIsAuth(ref isAuth, ref _webDavClient, ref _mqClient, ref username, ref password);
                   

                }
                else
                {
                    updateLoginFormErrorLoginIsEmpty();
                }

                //не важно есть юзер или нет мы полностью очистим таблицу и внесем новую запись
                _loginWindowModel.sqliteControler.getDelete().ClearTablesUsers();
                _loginWindowModel.sqliteControler.getInsert().insertUsernameAndPassword(username, password);

             //   _loginWindowModel._blockingEventController.setMRE(staticVariable.Variable.scannerConnectServerControllerTokenId);
            }
            catch(System.NullReferenceException a)
            {
                Console.WriteLine("LoginForm->Button_Click: " + a.ToString());
            }
            
        }

        private void rebuildSizeMain()
        {
            Task.Run(() =>
            {
                InfoSqlSize infoSql = new InfoSqlSize(_loginWindowModel.sqliteControler);
                App.Current.Dispatcher.Invoke(new System.Action(() => _loginWindowModel._mainViewModel.PgSizeDisk = infoSql.getSize()));
            });
        }

        //открывает браузер и передает ссылку на сайт
        private void HandleLinkClick(object sender, RoutedEventArgs e)
        {
            Hyperlink hl = (Hyperlink)sender;
            string navigateUri = hl.NavigateUri.ToString();
            Process.Start(new ProcessStartInfo(navigateUri));
            e.Handled = true;
        }

        //получает из Event пароль и засовывает его в модель без шифрования 
        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            { ((LoginFormViewModel)this.DataContext).Password = ((PasswordBox)sender).Password; }
        }


       

        private void checkIsAuth(ref bool isAuth, ref WebDavClientController _webDavClient, ref MqClientController _mqClient, ref string username, ref string password)
        {
            if (isAuth)
            {
                staticVariable.Variable.setWebDavNameSpace(username);
                int clientSize = _mqClient.getClientSize();
                _webDavClient.createNewConnect(username, password);
                this.Close();
                rebuildSizeMain();
            }
            else
            {
                updateLoginFormErrorLoginNoAcces();
            }

        }

        
        private void updateLoginFormClear()
        {
            //сбрасываем ошибки на экране если они там есть
            _loginWindowModel.updateForm.updateErrorLoginForm("");
            _loginWindowModel.updateForm.updatePgLoginForm("0");
        }
        private void updateLoginFormConnect()
        {
            _loginWindowModel.updateForm.updateButtonLoginForm("Подкл.");
            _loginWindowModel.updateForm.updatePgLoginForm("10");
        }


        private void updateLoginFormErrorLoginIsEmpty()
        {
            _loginWindowModel.updateForm.updatePgLoginForm("0");
            _loginWindowModel.updateForm.updateButtonLoginForm("Login");
            _loginWindowModel.updateForm.updateErrorLoginForm("Ошибка: Логин или пароль не заполнены");
        }

        private void updateLoginFormErrorLoginNoAcces()
        {
            _loginWindowModel.updateForm.updatePgLoginForm("0");
            _loginWindowModel.updateForm.updateButtonLoginForm("Login");
            _loginWindowModel.updateForm.updateErrorLoginForm("Ошибка: Не верный логин или пароль");
        }

        public void loginWindow_load(object sender, EventArgs e)
        {
            _loginWindowModel._blockingEventController.resetMRE(staticVariable.Variable.scannerConnectServerControllerTokenId);
            PasswordTextBox.Password = "";
            LoginTextBox.Text = "";
        }





    }



}
