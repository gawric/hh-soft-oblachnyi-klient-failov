﻿using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF
{
    /// <summary>
    /// Логика взаимодействия для CopyMainWindow.xaml
    /// </summary>
    public partial class CopyMainWindow : Window
    {
        readonly CopyMainWindowViewModel _viewCopyWindowsModel;
        //Статус открыто окно копирования или нет
        private CopyViewModel _copyViewModel;

        public CopyMainWindow(Container cont)
        {
            

            InitializeComponent();

            _viewCopyWindowsModel = cont.GetInstance<CopyMainWindowViewModel>();
            _copyViewModel = cont.GetInstance<CopyViewModel>();

            _viewCopyWindowsModel.title = "Копирование";
            _viewCopyWindowsModel.whence = "Откуда";
            _viewCopyWindowsModel.where = "Куда";

            DataContext = _viewCopyWindowsModel;

            _viewCopyWindowsModel.OnCloseWindowsCopy += (s, e) => this.Hide();

            _viewCopyWindowsModel.OnOpenWindowsCopy += (s, e) =>
            {
                _copyViewModel.isOpen = true;
                Show();
            };


        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
           // updateForm.updateCloseLoginForm();
            this.Visibility = Visibility.Hidden;
        }

    }
}
