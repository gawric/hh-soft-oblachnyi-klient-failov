﻿using Apache.NMS;
using AutoUpdaterDotNET;
using Microsoft.Win32;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.shell.windows;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.sync;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.changeIconViewModel;
using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.supportForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain;
using NewHH_SoftClientWPF.view;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebDAVClient;
using static NewHH_SoftClientWPF.staticVariable.Variable;

namespace NewHH_SoftClientWPF
{
    //я делал исправление сделал mainWIndows Singlton загрузка ускорился в 2 раза

    public partial class MainWindow : Window
    {

        private SqliteController _sqlLiteController;
        private ExceptionController _errorController;

        private MainWindowViewModel _viewModel;
        private ScannerConnectServerController _startScan;
        private BackNodesModel insert = new BackNodesModel();
        private NavigationMainWindowsController _navigationMainController;

        private ViewFolder _viewFolder;
        private ViewFolderViewModel _viewModelViewFolder;
        private SupportEventPart1MainForm _supportEventPart1MainForm;
        private SupportEventPart2MainForm _supportEventPart2MainForm;
        private InitializingMainForm _initializingMainForm;
        private Rebinding _rebinding;
        private ObserverNetwokSendJson _networkSendJson;
        private MainWindowContextMenu _mainWindowContextMenu;
        private ConvertIconType _convertIcon;
        public MainWindow(Container cont)
        {

            createObject(cont);
            createView();
            createOther();

            InitializeComponent();
            createNavigate();
            

        }



        public void ViewTransferClick(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventOpenViewTransferPages(sender, e);
        }


        public TreeViewModel TreeModel
        {
            get
            {
                return _viewModel.NodesView;
            }
        }


        private void Button_Click_GO_Folder(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventOpenFolder(sender, e);
        }
        private void Button_Click_ClearScan(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventScanClear(sender, e);
        }

       

        private void Button_Click_System_Setting(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventOpenSettingWindow(sender, e);
        }



        //кнопка назад
        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {

            _supportEventPart2MainForm.eventButtonClickBack(sender, e);

        }
        //кнопка вперед
        private void Button_Click_Forward(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventButtonClickForward(sender, e);

        }


        private void Button_Click_AutoScan(object sender, RoutedEventArgs e)
        {

            _supportEventPart2MainForm.eventAutoScan(sender, e);
        }

        private void WindowsStateChanged(object sender, EventArgs e)
        {

            _supportEventPart2MainForm.eventWindowsState(sender , e);
        }


        private void Button_Click_TestError(object sender, RoutedEventArgs e)
        {
            var t = Task.Run(() =>
             {
               try
               {
                   throw new Exception("log");
              }
              catch (Exception)
              {
                 _errorController.sendError((int)CodeError.DATABASE_ERROR, "SqliteController -> createTableBases() Критическая Ошибка создания таблицы базы данных sqlite", "error log");
                 new BaseException((int)CodeError.DATABASE_ERROR, "SqliteController -> createTableBases() Критическая Ошибка создания таблицы базы данных sqlite", "error log");
             }
             });

            InfoSystem infoSystem = new InfoSystem();
            
        }

        //кнопка изменение
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventButtonInCloudFolderClick(sender, e);
        }

        
        private void Button_Click_Help(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventOpenWindowsHelp(this);
        }

        private void Button_Click_AutoUpdater(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventAutoUpdater();
        }



        private void RemoveContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventRemoveContextMenuClick(sender, e);
        }


        private void CopyContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventCopyContextMenu(sender, e);

        }


        private void СutContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventCutContextMenu(sender, e);
        }


        private void DownloadContextMenu_Click(object sender, RoutedEventArgs e)
        {

            _supportEventPart1MainForm.eventDownloadContextMenu(sender, e);


        }

        private void OnlyCloudContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventOnlyCloudContextMenu(sender, e);
        }

        private void CopyLink_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventCopyLinkContextMenu(sender, e);
        }

        private void CopyLinkContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventCopyLinkContextMenu(sender, e);
        }

        //реализация вставки файла
        private void PasteContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventPasteContextMenu(sender, e);
        }


        private void Button_Click_ErrorAlert(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventOpenErrorAlert();
        }

        private void ButtonChangeUsers(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventOpenChangeUser(sender, e);
        }

       

        //открывает выбор файла на загрузку
        private void ButtonInCloud_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventButtonInCloudClick(sender, e);
        }


        private void CreateFolderContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventCreateFolderContextMenu(sender, e);
        }

        //меняет картинку - на searchbox
        private void SearchTextBox_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            _supportEventPart1MainForm.eventSearchTextBoxLeftDown(sender, e);
        }
        //меняет картинку - на searchbox
        private void SearchTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventSearchTextBoxLostFocus(sender, e);
        }

        //происходит когда снимается выделение с нода
        private void TreeViewItems_UnSelected(object sender, RoutedEventArgs e)
        {

            _supportEventPart2MainForm.eventTreeViewUnselect(sender, e);

        }

        public void TreeViewItem_Selected(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventTreeViewItemSelected(sender, e);
        }

        public void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventTreeViewExpended(sender, e);
        }

       

        private void OnCollapsed(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventTreeViewCollapsed(sender, e);
        }


        private void Exit_button_click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1MainForm.eventExitButton(this);

        }

        private void Exit_form_click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _supportEventPart1MainForm.eventExitForm(this);

        }

        private void CheckTrasfer_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Заглушка для Контекстного меню Соедениение ");
        }


        private void TreeViewRename_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart2MainForm.eventTreeViewRenameClick(sender, e);

        }


        //срабатывает когда получаем Visible у TextBox (при срабатывании Rename)
        private void TreeViewVisible_Event(object sender, DependencyPropertyChangedEventArgs e)
        {
            _supportEventPart2MainForm.eventTreeViewVisible(sender, e);
        }

        //срабатывает когда получаем Hide у TextBox (при срабатывании Rename)
        private void TreeViewHide_Event(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void MainWindowsMouseDown_Click(object sender, MouseButtonEventArgs e)
        {
            _supportEventPart2MainForm.eventMainWindowsMouseDown_click(sender, e);
        }


        private void TreeViewFolderNodeLostFocus(object sender, RoutedEventArgs e)
        {

            _supportEventPart2MainForm.eventTreeViewFolderNodeLostFocus(sender, e);
        }

        private void openContextMenu(object sender, ContextMenuEventArgs e)
        {
            _supportEventPart1MainForm.eventOpenContextMenu( sender,  e);
        }


        private InitializingMainFormModel createInitModel(Container cont)
        {
            InitializingMainFormModel model = new InitializingMainFormModel();

            model._mainWindow = this;
            model._sqlLiteController = _sqlLiteController;
            model._convertIcon = cont.GetInstance<ConvertIconType>();
            model.isLoadingInitializing = false;
            model._viewModel = _viewModel;
            model._updateMainForm = cont.GetInstance<UpdateFormMainController>();
            model._supportEventPart2MainForm = _supportEventPart2MainForm;
            model._mainWindowsContextMenu = cont.GetInstance<MainWindowContextMenu>();
            return model;

        }

        private void createObject(Container cont)
        {
            _navigationMainController = cont.GetInstance<NavigationMainWindowsController>();
            _errorController = cont.GetInstance<ExceptionController>();
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _startScan = cont.GetInstance<ScannerConnectServerController>();
            _viewModel.NodesView = new TreeViewModel();
            _rebinding = new Rebinding(_navigationMainController);
            _supportEventPart1MainForm = cont.GetInstance<SupportEventPart1MainForm>();
            _supportEventPart2MainForm = cont.GetInstance<SupportEventPart2MainForm>();
            _initializingMainForm = new InitializingMainForm(createInitModel(cont));
            _viewFolder = cont.GetInstance<ViewFolder>();
            _viewModel._isRenameActiveToTreeView = false;
            _viewModelViewFolder = cont.GetInstance<ViewFolderViewModel>();
            _networkSendJson = cont.GetInstance<ObserverNetwokSendJson>();
            _supportEventPart1MainForm.injectElemetns(_initializingMainForm, this);
            _supportEventPart2MainForm.injectElemetns(_initializingMainForm, this);
            _networkSendJson.injectSqllite(_sqlLiteController);
            _mainWindowContextMenu = cont.GetInstance<MainWindowContextMenu>();
            _convertIcon = cont.GetInstance<ConvertIconType>();
        }

       

        private void createView()
        {
            //загружаем копонненты в форму
            this.Loaded += _initializingMainForm.MainWindow_load;

            MainWindow _mainWindow = this;
            RenameMainEvent _rename = new RenameMainEvent(ref _viewModelViewFolder, ref _viewModel, ref _mainWindow);


            _viewModel.OnRefreshTreeView += (s, e) => this.treeView1.Items.DeferRefresh();
            _viewModel.OnRenameTreeView += (s, e) => _rename.hideRenameFolderNodes();
            _viewModel.OnRebindingTreeView += _rebinding.rebindingTreeView;
            _viewModel.OnTaskBarSetIcon += _supportEventPart1MainForm.taskBarSetIcon;
            _viewModel.OnTaskBarShowBallon += _supportEventPart1MainForm.taskBarShowBallon;
            _viewModel.OnChangeImageMenuClearScan += _supportEventPart2MainForm.changeIconMenuClearScan;
        }

        private void createOther()
        {
            _viewModel.isStartMainWindows = false;
            long[] arr = new long[1];
            _viewModel.ActiveNodeID = arr;
            _startScan.ScanStartAsync();
            _sqlLiteController.CreateBases();
            DataContext = _viewModel;

        }

        private void createNavigate()
        {
            FrameViewFolder.Navigate(_viewFolder);
            FrameViewFolder.NavigationUIVisibility = NavigationUIVisibility.Hidden;
        }

        private void TreeView_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                _supportEventPart2MainForm.eventMainWindowsKeyEnter();

            }
           
        }
    }
}
