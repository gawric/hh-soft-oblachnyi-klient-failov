﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationClickButton.doubleclick;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.operationContext;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF
{

    public partial class ViewFolder : Page
    {
        private ViewFolderViewModel _viewModel;
      
        private CopyStatusController _copyStatus;
        private NavigationMainWindowsController _navigationMainController;
        private SupportEventPart1ViewForm _supportEventPart1ViewForm;
        private SupportEventPart2ViewForm _supportEventPart2ViewForm;
        private ConvertIconType _convertIcon;
        private ViewFolderContextMenu _viewFolderContextMenu;
        private string oldFolderNodes = "";

        public ViewFolder(Container cont)
        {

            createObject(cont);
            createView();
            createInitializing();
            
            InitializeComponent();
        }

        
        //DataContext для TreeView
        public TreeViewModel TreeModel
        {
            get
            {
                return _viewModel.NodesView;
            }
        }


        private void Button_Click_GO_Folder(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewForm.openExplorer(sender,  e);
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            _supportEventPart1ViewForm.listViewMouseDoubleClickEvent( sender,  e);
        }
       
        //реализация вырезания файла
        private void CutContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewForm.cutContextMenuClickEvent( sender,  e);

        }

        //реализация копирования файла
        private void CopyContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewForm.copyContextMenuClickEvent( sender,  e);
        }
        
        //реализация вставки  файла
        private void PasteContextMenu_Click(object sender, RoutedEventArgs e)
        {

            _supportEventPart1ViewForm.pasteContextMenuClickEvent( sender,  e);

        }

       
        private void RemoveContextListMenu_Click(object sender, RoutedEventArgs e)
        {

            _supportEventPart1ViewForm.removeContextMenuClickEvent( sender,  e);
            
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
          //  Console.WriteLine("Выбран новый элемент в ViewFolder");
        }

     
        private void CreateFolderContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart2ViewForm.createFolderContextMenuEvent(sender , e);
        }
        
       
     
        

        private void TreeViewFolderNodeLostFocus(object sender, RoutedEventArgs e)
        {
            _supportEventPart2ViewForm.treeViewLostFocusEvent(sender, e);
        }

        //срабатывает когда получаем Visible у TextBox (при срабатывании Rename)
        private void TreeViewVisible_Event(object sender, DependencyPropertyChangedEventArgs e)
        {
            _supportEventPart2ViewForm.treeViewVisibleEvent(sender, e , ref oldFolderNodes);
        }


        private void TreeViewFolderNodes_Click(object sender, RoutedEventArgs e)
        {
           // Console.WriteLine("Определяем запуск клика на TextBox");
        }

       
        private void TreeViewRename_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart2ViewForm.treeViewRenameEvent( _viewModel,  this , ref oldFolderNodes);

        }


      
        private void DownloadContextMenu_Click(object sender, RoutedEventArgs e)
        {

            _supportEventPart2ViewForm.downloadViewEvent();

        }

        private void OnlyCloudContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewForm.eventOnlyCloudContextMenu(sender, e);
        }

        private void CopyLinkContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewForm.copyLinkContextMenuClickEvent(sender, e);
        }


        private void TreeView_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                _supportEventPart2ViewForm.hideRenameFolderNodes();

            }

        }


        private InitializingViewFormModel createModel(ViewFolder _viewFolder, ConvertIconType _convertIcon , ViewFolderContextMenu viewFolderContextMenu)
        {

            InitializingViewFormModel model = new InitializingViewFormModel();
            model._viewFolder = _viewFolder;
            model._convertIcon = _convertIcon;
            model._viewFolderContextMenu = viewFolderContextMenu;

            return model;
        }

        private void createObject(Container cont)
        {
            _supportEventPart1ViewForm = cont.GetInstance<SupportEventPart1ViewForm>();
            _supportEventPart2ViewForm = cont.GetInstance<SupportEventPart2ViewForm>();
            _copyStatus = cont.GetInstance<CopyStatusController>();
            _navigationMainController = cont.GetInstance<NavigationMainWindowsController>();
            _convertIcon = cont.GetInstance<ConvertIconType>();
            _viewModel = cont.GetInstance<ViewFolderViewModel>();
            _viewFolderContextMenu = cont.GetInstance<ViewFolderContextMenu>();
        }

        private void createView()
        {
           
            _viewModel.ActiveViewFolderNodeID = new long[1];
            _viewModel.NodesView = new TreeViewModel();
            _viewModel.OnRefreshListView += (s, e) => this.ListView.Items.Refresh();
            _viewModel.OnRenameListView += (s, e) => _supportEventPart2ViewForm.hideRenameFolderNodes();
            _viewModel.OnRebindingListView += _supportEventPart2ViewForm.rebindingListViewEvent;
        }



        private void createInitializing()
        {
            InitializingViewFormModel modelInit = createModel(this, _convertIcon , _viewFolderContextMenu);
            InitializingViewForm init = new InitializingViewForm(modelInit);
            this.Loaded += init.ViewFolderWindow_load;
            _supportEventPart2ViewForm.injectElemetns(this);
            _supportEventPart1ViewForm.injectElemetns(this);
            _viewModel._isRenameActiveToListView = false;
        }



    }
}
