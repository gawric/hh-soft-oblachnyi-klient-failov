﻿using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF
{
    /// <summary>
    /// Логика взаимодействия для OverwriteWindow.xaml
    /// </summary>
    public partial class OverwriteWindow : Window
    {
        private OverwriteWindowModel _overModel;

        public OverwriteWindow(Container cont)
        {
            _overModel = cont.GetInstance<OverwriteWindowModel>();
            InitializeComponent();
        }

        //перезаписать
        private void Button_Click_OK(object sender, RoutedEventArgs e)
        {
            _overModel.isOverwrite = true;
            this.Hide();
        }

        //отменить
        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            _overModel.isOverwrite = false;
            this.Hide();
        }
    }
}
