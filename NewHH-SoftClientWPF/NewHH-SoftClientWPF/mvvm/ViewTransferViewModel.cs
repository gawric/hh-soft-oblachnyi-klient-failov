﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm
{
    public class ViewTransferViewModel : INotifyPropertyChanged
    {
        private ViewTransferModel model = new ViewTransferModel();
        

        public event PropertyChangedEventHandler PropertyChanged;

        public ListTransferViewModel ListView;
       

        public string Titlename
        {
            get { return model.titlename; }
            set
            {
                if (model.titlename != value)
                {
                    model.titlename = value;
                    OnPropertyChange("Titlename");

                }
            }
        }

      



        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
