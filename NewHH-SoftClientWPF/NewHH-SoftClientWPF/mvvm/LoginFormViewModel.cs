﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.mvvm
{
    // INotifyPropertyChanged notifies the View of property changes, so that Bindings are updated.
    public class LoginFormViewModel : INotifyPropertyChanged
    {
        private LoginFormModel loginModel;

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler OnRequestClose;
        public event EventHandler OnRequestCreate;
 
        private bool OpenWindow = false;


        public LoginFormViewModel()
        {
            loginModel = new LoginFormModel();
            loginModel.textbuttonvalue = "Login";

        }

        public void ClosingLoginForm()
        {
            if(OpenWindow == true)
            {
                OpenWindow = false;
                Application.Current.Dispatcher.Invoke(new System.Action(() => OnRequestClose(this, new EventArgs())));
            }
           
        }
        public void OpenLoginForm()
        {
            try
            {
                if(OpenWindow == false)
                {
                    OpenWindow = true;
                    Application.Current.Dispatcher.Invoke(new System.Action(() => OnRequestCreate(this, new EventArgs())));
                }
               
            }
            catch(System.InvalidOperationException b)
            {
                Console.WriteLine(b.ToString());
            }
           
        }

        public string Username
        {
            get { return loginModel.username; }
            set
            {
                if (loginModel.username != value)
                {
                    loginModel.username = value;
                    OnPropertyChange("Username");
                   
                }
            }
        }

        public string Password
        {
            get { return loginModel.password; }
            set
            {
                if (loginModel.password != value)
                {
                    loginModel.password = value;
                    OnPropertyChange("password");
                }
            }
        }

     

        public double ProgressBarValue
        {
            get { return loginModel.progressbarvalue; }
            set
            {
                if (loginModel.progressbarvalue != value)
                {
                    loginModel.progressbarvalue = value;
                    OnPropertyChange("ProgressBarValue");
                }
            }
        }

        public string TextButtonValue
        {
            get { return loginModel.textbuttonvalue; }
            set
            {
                if (loginModel.textbuttonvalue != value)
                {
                    loginModel.textbuttonvalue = value;
                    OnPropertyChange("TextButtonValue");
                }
            }
        }

        public string TextErrorValue
        {
            get { return loginModel.texterrorvalue; }
            set
            {
                if (loginModel.texterrorvalue != value)
                {
                    loginModel.texterrorvalue = value;
                    OnPropertyChange("TextErrorValue");
                }
            }
        }

      

        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
