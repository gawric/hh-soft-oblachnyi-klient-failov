﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm.supportForm
{
    public class MainWindowContextMenu : INotifyPropertyChanged
    {
        string saveOkText { get; set; }
        string saveCloudText { get; set; }
        string copyLinkText { get; set; }
        BitmapSource contextMenuTreeViewCreate { get; set; }
        BitmapSource contextMenuTreeViewCopyLink { get; set; }
        BitmapSource contextMenuTreeViewSaveOk { get; set; }
        BitmapSource contextMenuTreeViewSaveCloud { get; set; }
        BitmapSource contextMenuTreeViewCutFolder { get; set; }
        BitmapSource contextMenuTreeViewCopyFolder { get; set; }
        BitmapSource contextMenuTreeViewPasteFolder { get; set; }
        BitmapSource contextMenuTreeViewRenameFolder { get; set; }
        BitmapSource contextMenuTreeViewDeleteFolder { get; set; }
        BitmapSource contextMenuTreeViewGoFolder { get; set; }


        public string CopyLinkText
        {
            get { return copyLinkText; }
            set
            {
                if (copyLinkText != value)
                {
                    copyLinkText = value;
                    OnPropertyChange("CopyLinkText");

                }
            }
        }


        public string SaveOkText
        {
            get { return saveOkText; }
            set
            {
                if (saveOkText != value)
                {
                    saveOkText = value;
                    OnPropertyChange("SaveOkText");

                }
            }
        }

        public string SaveCloudText
        {
            get { return saveCloudText; }
            set
            {
                if (saveCloudText != value)
                {
                    saveCloudText = value;
                    OnPropertyChange("SaveCloudText");

                }
            }
        }


        public BitmapSource ContextMenuTreeViewCreate
        {
            get { return contextMenuTreeViewCreate; }
            set
            {
                if (contextMenuTreeViewCreate != value)
                {
                    contextMenuTreeViewCreate = value;
                    OnPropertyChange("ContextMenuTreeViewCreate");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewGoFolder
        {
            get { return contextMenuTreeViewGoFolder; }
            set
            {
                if (contextMenuTreeViewGoFolder != value)
                {
                    contextMenuTreeViewGoFolder = value;
                    OnPropertyChange("ContextMenuTreeViewGoFolder");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewCopyLink
        {
            get { return contextMenuTreeViewCopyLink; }
            set
            {
                if (contextMenuTreeViewCopyLink != value)
                {
                    contextMenuTreeViewCopyLink = value;
                    OnPropertyChange("ContextMenuTreeViewCopyLink");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewSaveCloud
        {
            get { return contextMenuTreeViewSaveCloud; }
            set
            {
                if (contextMenuTreeViewSaveCloud != value)
                {
                    contextMenuTreeViewSaveCloud = value;
                    OnPropertyChange("ContextMenuTreeViewSaveCloud");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewSaveOk
        {
            get { return contextMenuTreeViewSaveOk; }
            set
            {
                if (contextMenuTreeViewSaveOk != value)
                {
                    contextMenuTreeViewSaveOk = value;
                    OnPropertyChange("ContextMenuTreeViewSaveOk");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewCutFolder
        {
            get { return contextMenuTreeViewCutFolder; }
            set
            {
                if (contextMenuTreeViewCutFolder != value)
                {
                    contextMenuTreeViewCutFolder = value;
                    OnPropertyChange("ContextMenuTreeViewCutFolder");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewCopyFolder
        {
            get { return contextMenuTreeViewCopyFolder; }
            set
            {
                if (contextMenuTreeViewCopyFolder != value)
                {
                    contextMenuTreeViewCopyFolder = value;
                    OnPropertyChange("ContextMenuTreeViewCopyFolder");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewPasteFolder
        {
            get { return contextMenuTreeViewPasteFolder; }
            set
            {
                if (contextMenuTreeViewPasteFolder != value)
                {
                    contextMenuTreeViewPasteFolder = value;
                    OnPropertyChange("ContextMenuTreeViewPasteFolder");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewRenameFolder
        {
            get { return contextMenuTreeViewRenameFolder; }
            set
            {
                if (contextMenuTreeViewRenameFolder != value)
                {
                    contextMenuTreeViewRenameFolder = value;
                    OnPropertyChange("ContextMenuTreeViewRenameFolder");

                }
            }
        }

        public BitmapSource ContextMenuTreeViewDeleteFolder
        {
            get { return contextMenuTreeViewDeleteFolder; }
            set
            {
                if (contextMenuTreeViewDeleteFolder != value)
                {
                    contextMenuTreeViewDeleteFolder = value;
                    OnPropertyChange("ContextMenuTreeViewDeleteFolder");

                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
