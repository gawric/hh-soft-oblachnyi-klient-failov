﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;



namespace NewHH_SoftClientWPF.mvvm
{
  
    public class ViewFolderViewModel : INotifyPropertyChanged
    {
        private ViewFolderModel model;
        public event PropertyChangedEventHandler PropertyChanged;
        public TreeViewModel NodesView;

        public event EventHandler<ArgsDataListView> OnRebindingListView;
        public event EventHandler OnRefreshListView;
        public event EventHandler OnRenameListView;

        //срабатывает когда нажимаем кнопку переименовать
        public bool _isRenameActiveToListView { get; set; }

        //для parentID для удаленных обновлений через ACtiveMq, что-бы знать какой актуальный сейчас  NOD
        public long[] ActiveViewFolderNodeID { get; set; }

        public void RebindingList(SortableObservableCollection<FolderNodes> obs)
        {
            ArgsDataListView a = new ArgsDataListView(obs);
            OnRebindingListView(this , a);
        }

        //Обновляет дерево TreeView
        public void RefreshListView()
        {
            App.Current.Dispatcher.Invoke(new System.Action(() => OnRefreshListView(this, new EventArgs())));
        }

        //Обновляет дерево TreeView
        public void RenameListView()
        {
            App.Current.Dispatcher.Invoke(new System.Action(() => OnRenameListView(this, new EventArgs())));
        }


        public string Titlename
        {
            get { return model.titlename; }
            set
            {
                if (model.titlename != value)
                {
                    model.titlename = value;
                    OnPropertyChange("Titlename");

                }
            }
        }

    

        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
