﻿using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm
{
    public class InitializingSettingForm
    {
        SettingWindowViewModel _swvm;
        SystemSettingModelObj _ssmo;

        public InitializingSettingForm(SettingWindowViewModel swvm,  SystemSettingModelObj ssmo)
        {
            _swvm = swvm;
            _ssmo = ssmo;
            createScanFolderList();
        }

        public void SettingWindow_load(object sender, EventArgs e)
        {
            setAutoStartApplication();
            setScanFolderApplication();
            setPathDisk();
            setAutoScanTime();
            setUserName();
            setServer();
            setIsDisabledButton();
            setOtherStatistics();
            setAllScanFolder();

        }

        private void setIsDisabledButton()
        {
            _swvm.IsEnbledButtonOk = true;
            _swvm.IsEnbledButtonCancel = true;
            _swvm.IsEnbledButtonCommit = true;
            
        }

        private void setAllScanFolder()
        {
           List<ScanFolderModel> listSql = _ssmo._sqliteController.getSelect().getAllScanFolder();
            convertSqlToList(listSql);
        }

        private void convertSqlToList(List<ScanFolderModel> listSql)
        {
            foreach(ScanFolderModel item in listSql)
            {
                _swvm.obsScanFol.Add(item);
            }
           // _swvm.obsScanFol.Union(listSql);
        }
        private void createScanFolderList()
        {
            _swvm.obsScanFol = new ObservableCollection<ScanFolderModel>();
        }
        private void setServer()
        {
            _swvm.WebDavServer = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer();
            _swvm.ActiveMqServer = staticVariable.Variable.getDomenMqServer() + ":" + staticVariable.Variable.getPortMqServer();
        }
        private void setPathDisk()
        {
            string pathDisk = _ssmo._sqliteController.getSelect().GetPathDisk();
            _swvm.PathDisk = pathDisk;

        }

        private void setAutoScanTime()
        {
            int timeMin = (int)TimeSpan.FromMilliseconds(staticVariable.Variable.startMillisecondAutoSync).TotalMinutes;
            _swvm.TimeAutoScan = timeMin.ToString();
            
        }
        private void setAutoStartApplication()
        {
            bool isChecked = _ssmo._sqliteController.getSelect().getAutoStartApplication();
            _swvm.IsCheckBoxAutoStartChecked = isChecked;
        }

        private void setScanFolderApplication()
        {
            bool isChecked = _ssmo._sqliteController.getSelect().getScanFolderApplication();
            _swvm.IsCheckBoxScanFolderChecked = isChecked;
        }
        private void setUserName()
        {
            string username = _ssmo._sqliteController.getSelect().GetUserSqlite()[0];
            _swvm.UserName = username;
        }
    
        private void setOtherStatistics()
        {
            AutoScanInfoModel model = _ssmo._sqliteController.getSelect().getAutoScanInfoModel();
            _swvm.AllCount = model.allCount.ToString();
            TimeSpan span = TimeSpan.Parse(model.timeSpent);
            _swvm.TimeSpent = span.ToString("c");
            _swvm.FileAndFolder = "Файлов: "+model.folder.ToString() + "   "+"Папок: " + model.files.ToString();
            _swvm.LastDataScan = model.lastDataScan;

         
        }
    }
}
