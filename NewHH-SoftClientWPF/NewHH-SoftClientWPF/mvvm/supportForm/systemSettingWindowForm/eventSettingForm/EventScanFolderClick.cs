﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm
{
    public class EventScanFolderClick
    {
        private SettingWindowViewModel _swvm;
        private SystemSettingModelObj _ssmo;
        public EventScanFolderClick(SettingWindowViewModel swvm , SystemSettingModelObj ssmo)
        {
            _swvm = swvm;
            _ssmo =  ssmo;
        }

        public void AddFolderScan()
        {
            IOpenDialog dialog = OpenDialog();
            string fullPath = GetOpenDialogPath(dialog);
           

            if (!IsContent(fullPath))
            {
                AddSql(fullPath);
                AddList(fullPath);
            }
   
        }

        public void DeleteFolderScan(string fullPath)
        {
            if (IsContent(fullPath))
            {
                DeleteSql(fullPath);
                DeleteList(fullPath);
            }
        }


        private void DeleteSql(string fullPath)
        {
            _ssmo._sqliteController.getDelete().RemoveSinglItemScanFolder(fullPath);
        }
        private void DeleteList(string fullPath)
        {
            ObservableCollection<ScanFolderModel> listFolder = _swvm.obsScanFol;
            ScanFolderModel model = listFolder.FirstOrDefault(x => x.fullPath.Equals(fullPath));
            _swvm.obsScanFol.Remove(model);
        }

        private IOpenDialog OpenDialog()
        {
            IOpenDialog dialog = new OpenDialog();
            dialog.CreateFolderDialog();
            return dialog;
        }

        private string GetOpenDialogPath(IOpenDialog dialog)
        {
            return dialog.FolderPath[0];
        }

        private bool IsContent(string fullPath)
        {
            ObservableCollection<ScanFolderModel> listFolder = _swvm.obsScanFol;
            return listFolder.Any(x => x.fullPath.Equals(fullPath));
        }
        private void AddSql(string fullPath)
        {
            _ssmo._sqliteController.getInsert().insertScanFolder(fullPath);
        }
        private void AddList(string fullPath)
        {
           
            _swvm.obsScanFol.Add(GetModel(fullPath));
        }

        private ScanFolderModel GetModel(string fullPath)
        {
            ScanFolderModel sfm = new ScanFolderModel();
            sfm.fullPath = fullPath;
            return sfm;
        }
    }
}
