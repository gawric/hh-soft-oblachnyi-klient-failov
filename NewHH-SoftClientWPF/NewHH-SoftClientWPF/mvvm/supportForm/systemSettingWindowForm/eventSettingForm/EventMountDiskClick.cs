﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using SHDocVw;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm
{
    public class EventMountDiskClick
    {
        const int SW_SHOWMINNOACTIVE = 7;

       

        public List<Uri> getOpenWindowsExplorer()
        {
            List<Uri> listWindows = new List<Uri>();
            ShellWindows shellWindows = new SHDocVw.ShellWindows();
            string filename;

            foreach (SHDocVw.InternetExplorer ie in shellWindows)
            {
                filename = Path.GetFileNameWithoutExtension(ie.FullName).ToLower();
                if (filename.Equals("explorer"))
                {
                    Console.WriteLine("URI " + ie.LocationURL);
                    if (!ie.LocationURL.Equals(""))
                    {
                        listWindows.Add(new Uri(ie.LocationURL));
                    }

                }

            }

            return listWindows;
        }

        public void useBat(string hrefMount, string hrefbat)
        {
            if (Directory.Exists(hrefMount))
            {
                if (File.Exists(hrefbat))
                {
                    try
                    {
                        string cmd = "/C  \"" + "\"" + hrefbat + "\"  \"" + hrefMount + "\"\"";
                        var m_command = new System.Diagnostics.Process();
                        m_command.StartInfo.FileName = "C:\\Windows\\Sysnative\\cmd.exe";
                        m_command.StartInfo.Arguments = cmd;
                        m_command.OutputDataReceived += (sender1, argsx) => Console.WriteLine(argsx.Data); 

                        m_command.StartInfo.RedirectStandardOutput = true;
                        m_command.StartInfo.UseShellExecute = false;

                        m_command.Start();
                        m_command.BeginOutputReadLine();
                        m_command.WaitForExit(500);


                    }
                    catch (System.InvalidOperationException a)
                    {
                        Console.WriteLine("MainWindow->ButtonMoundHdd " + a.ToString());
                    }

                }
                else
                {
                    MessageBox.Show("MainWindow-> Скрипт для монтирования диска не найден");
                }

            }
            else
            {
                MessageBox.Show("MainWindow-> Папка для монтирования диска не найдена");
            }
        }


        // private void minimizeWindowExplorer()
        //{
        //   int indexe = 0;
        //   bool isNotEmpty = false;
        //   while (true)
        //  {
        //    Thread.Sleep(300);

        //   ShellWindows shellWindows = new SHDocVw.ShellWindows();
        //   string filename;
        //  Console.WriteLine("Проверка, что окна в explorer открылись " + shellWindows.Count);

        //   foreach (SHDocVw.InternetExplorer ie in shellWindows)
        // {
        //     isNotEmpty = true;
        // }
        //
        // if (isNotEmpty == true)
        // {
        // foreach (SHDocVw.InternetExplorer ie in shellWindows)
        // {
        //  filename = Path.GetFileNameWithoutExtension(ie.FullName).ToLower();
        // Console.WriteLine("Перебор внутри IE" + filename);

        // if (filename.Equals("explorer"))
        // {
        //    Console.WriteLine("URI " + ie.LocationURL);
        //   Console.WriteLine("HWND " + ie.HWND);

        //  MinimizeWindow((IntPtr)ie.HWND);

        //}

        // }

        // break;
        // }

        // if (indexe == 5) break;
        // indexe++;

        //}

        // }

        public void waitExplorer()
        {
            int index = 0;

            while (true)
            {
                Thread.Sleep(200);

               Process[] pname = Process.GetProcessesByName("explorer");
                if (pname.Length == 0)
                    Console.WriteLine("SystemSetting->MainWindow->Ожидание перезапуска Explorer");
                else
                {
                    Console.WriteLine("SystemSetting->MainWindow-> Explorer запустился");
                    break;
                }

                if (index == 20) break;
                index++;
            }

        }

        public void createNewWindows(List<Uri> listWindows, ref SqliteController sqlLiteControlle)
        {
            string hrefbat = staticVariable.Variable.getBATfolder() + "minimalWindows.bat";
            createBat(listWindows, hrefbat);
            string hrefMount = sqlLiteControlle.getSelect().getLocationDownload();
            useBat(hrefMount, hrefbat);
        }


        public void openWindowsMinimized(List<Uri> listWindows)
        {
            OpenExplorer openExplorer = new OpenExplorer(null, null);
            foreach (Uri path in listWindows)
            {
                Thread.Sleep(50);
                string localpath = path.LocalPath;
                openExplorer.OpenToPathMinimized(localpath);
            }
        }

        public void createBat(List<Uri> listWindows , string filePathBat)
        {
            try
            {
                deleteBat(filePathBat);

                using (FileStream fs = new FileStream(filePathBat, FileMode.OpenOrCreate, FileAccess.Write ))
                {
                   

                    StreamWriter write = new StreamWriter(fs , System.Text.Encoding.GetEncoding("cp866"));
                    write.BaseStream.Seek(0, SeekOrigin.End);


                    write.WriteLine(getBatStrCheckRunExplorer());

                    foreach (Uri uriItem in listWindows)
                    {
                        
                        write.WriteLine("start /min \"\" "+ "\""+uriItem.LocalPath+"\""+"\n");
                    }
                   
                    write.Flush();
                    write.Close();
                    fs.Close();
                }
            }
            catch(System.ArgumentException s)
            {
                Console.WriteLine("EventMountDiskClick->createBat: критическая ошибка "+s.ToString());
            }
        }

        private void deleteBat(string filePathBat)
        {
            if(File.Exists(filePathBat))
            {
                File.Delete(filePathBat);
            }
        }

        private string getBatStrCheckRunExplorer()
        {
            return ":LOOP \n tasklist | find / i \"explorer.exe\" > nul 2>&1 \n IF ERRORLEVEL 1(ECHO Explorer is not running GOTO LOOP) ELSE(ECHO Explorer is running GOTO CONTINUE) \n :CONTINUE \n";
        }
    }
}
