﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm
{
    public class EventAutoStartApplication
    {
        private ConvertIconType _convertIcon;
        private ExceptionController _error;
        public EventAutoStartApplication(ConvertIconType convertIcon , ExceptionController error)
        {
            _convertIcon = convertIcon;
            _error = error;
        }
        public void clickCheckBoxAutoStart(ref SqliteController sqlLiteControlle , bool isCheked)
        {
            if(isCheked)
            {
                IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
                string shortcutAddress = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\NewHH-SoftClientWPF.lnk";
          
                if (!File.Exists(shortcutAddress))
                {
                    System.Reflection.Assembly curAssembly = System.Reflection.Assembly.GetExecutingAssembly();

                    IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutAddress);
                    shortcut.Description = "hh-Soft облачный клиент";
                    shortcut.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    shortcut.TargetPath = curAssembly.Location;
                    string locationIcon = _convertIcon.getUriIcon(".applicationIcon").LocalPath;
                    shortcut.IconLocation = locationIcon;
                    shortcut.Save();
                   
                }
                else
                {
                    if(_error != null)
                    {
                        generatedError("Автозагрузка!", "Автоматическая загрузка уже создана!");
                    }
                    else
                    {
                        Console.WriteLine("EventAutoStartApplication->clickCheckBoxAutoStart: Автоматическая загрузка уже создана");
                    }
                    

                }
                  
            }
            else
            {
                string shortcutAddress = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\NewHH-SoftClientWPF.lnk";

                if (File.Exists(shortcutAddress))
                {
                    File.Delete(shortcutAddress);
                }
            }

           


        }

        public BaseException generatedError(string textError, string trhowError)
        {
            _error.sendError((int)CodeError.FILE_SYSTEM_EXCEPTION, textError, trhowError);
            return new BaseException((int)CodeError.FILE_SYSTEM_EXCEPTION, textError, trhowError);
        }
    }
}
