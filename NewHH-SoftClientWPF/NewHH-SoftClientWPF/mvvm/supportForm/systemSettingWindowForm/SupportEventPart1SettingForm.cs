﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using NewHH_SoftClientWPF.contoller.util.sync.autosave;
using NewHH_SoftClientWPF.contoller.util.sync.autosave.desibleAutoSave;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm
{
    public class SupportEventPart1SettingForm
    {
        private EventClick _eventClick;
        private SettingWindowViewModel _swvm;
        private SqliteController _sqlliteController;
        private bool isCommit = false;
        private EventCommit _eventCommit;
        private ViewFolderViewModel _vmvf;
        private MainWindowViewModel _vmm;
        private BlockingEventController _bce;
        private ConvertIconType _convertIcon;
        private ExceptionController _error;
        private EventScanFolderClick _esfc;
        private IAutoSaveFolder _autoSaveFolder;
        private IDesAutoSave _desAutoSave;
        private IFSWatcher _fsWatcher;
        private ISubScribleEvent _sefsw;
        public SupportEventPart1SettingForm(SystemSettingModelObj _ssmo)
        {
            _swvm = _ssmo._swvm;
            _sqlliteController = _ssmo._sqliteController;
            _eventClick = new EventClick(_swvm);
            _vmvf = _ssmo._viewModelViewFolder;
            _vmm = _ssmo._viewModelMain;
            _autoSaveFolder = new AutoSaveFolderController(_ssmo);
            _desAutoSave = new DesAutoSave(_ssmo);
            _sefsw = new SubsribeEventFsWatcher(_ssmo._sqliteController, _ssmo._soup , _ssmo._cfc , _ssmo._fsfc , _ssmo._cfc) ;
            _fsWatcher = new FSWatcher(_ssmo._cc , _sefsw);
            _eventCommit = new EventCommit(_ssmo._sqliteController, _swvm , _ssmo._convertIcon , _autoSaveFolder , _desAutoSave , _fsWatcher);
            _convertIcon = _ssmo._convertIcon;
            _bce = _ssmo._blockingEventController;
            _esfc = new EventScanFolderClick(_swvm , _ssmo);
            this._error = _ssmo._error;
            

        }

        public void ClickOkEvent(object sender, RoutedEventArgs e)
        {
            ManualResetEvent mre = _bce.getManualReset(staticVariable.Variable.runningAutoSyncBlockingId).getManualResetEvent();
            _eventCommit.commit(ref isCommit , ref _vmvf, ref _vmm, ref mre);
            _eventClick.clickOk();
        }

        public void ClickChangeDiskEvent(object sender, RoutedEventArgs e)
        {
            _eventClick.clickChangeDisk(ref isCommit);
        }

        public void ClickAddFolderScan(object sender, RoutedEventArgs e)
        {
            _esfc.AddFolderScan();
        }
        public void ClickDeleteFolderScan(ListBox listBox, RoutedEventArgs e)
        {
            ScanFolderModel model = (ScanFolderModel)listBox.SelectedItem;
            if(model != null) _esfc.DeleteFolderScan(model.fullPath);
        }


        public void CheckBoxAutoStart(object sender, RoutedEventArgs e)
        {
           
            EventAutoStartApplication eventAutoStart = new EventAutoStartApplication(_convertIcon , _error);
            eventAutoStart.clickCheckBoxAutoStart(ref _sqlliteController, _swvm.IsCheckBoxAutoStartChecked);
            isCommit = true;
        }

        public void CheckBoxScanFolder(object sender, RoutedEventArgs e)
        {
            isCommit = true;
        }
        public void ClickMountDiskEvent(object sender, RoutedEventArgs e)
        {
            _eventClick.clickMountDisk(ref isCommit , ref _sqlliteController);
        }

        public void ClickUnmountDiskEvent(object sender, RoutedEventArgs e)
        {
            _eventClick.clickUnmountDisk(ref isCommit , ref _sqlliteController);
        }

        public void ClickChangeTimeAutScan(object sender, RoutedEventArgs e)
        {
            isCommit = true;
            int timeMin = Convert.ToInt32(_swvm.TimeAutoScan);
            int millisecund = (int)TimeSpan.FromMinutes(timeMin).TotalMilliseconds;
            _sqlliteController.updateAutoStartMill(millisecund.ToString());
        }

        public void ClickCancelEvent(object sender, RoutedEventArgs e)
        {
            _eventClick.clickCancel();
        }


        public void ClickCommitEvent(object sender, RoutedEventArgs e)
        {
            ManualResetEvent mre = GetManualReset();
            _eventCommit.commit(ref isCommit , ref _vmvf, ref _vmm, ref mre);
        }

        private ManualResetEvent GetManualReset()
        {
            return _bce.getManualReset(staticVariable.Variable.runningAutoSyncBlockingId).getManualResetEvent();
        }
    }
}
