﻿using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.initizlizing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm
{
    public class InitializingViewForm
    {

        private InitializingViewFormModel initModel;

        public InitializingViewForm(InitializingViewFormModel initModel)
        {
            this.initModel = initModel;
        }

        public void ViewFolderWindow_load(object sender, EventArgs e)
        {
            // initModel._viewFolder.ContextMenuViewFolderCutFolder.Source = initModel._convertIcon.getIconType(".contextMenuItemCutFolder");
            // initModel._viewFolder.ContextMenuViewFolderCopyFolder.Source = initModel._convertIcon.getIconType(".contextMenuItemCopyFolder");
            // initModel._viewFolder.ContextMenuViewFolderPasteFolder.Source = initModel._convertIcon.getIconType(".contextMenuItemPasteFolder");
            // initModel._viewFolder.ContextMenuViewFolderDeleteFolder.Source = initModel. _convertIcon.getIconType(".contextMenuItemDeleteFolder");
            //  initModel._viewFolder.ContextMenuViewFolderRenameFolder.Source = initModel._convertIcon.getIconType(".contextMenuItemRenameFolder");
            //  initModel._viewFolder.ContextMenuViewFolderSaveOk.Source = initModel._convertIcon.getIconType(".contextMenuItemSaveOk");
            // initModel._viewFolder.ContextMenuViewFolderCreate.Source = initModel._convertIcon.getIconType(".contextMenuCreate");
            initializingContextMenu();
        }

        public void initializingContextMenu()
        {
            InitializingViewFolderContextMenu initCM = new InitializingViewFolderContextMenu(initModel._viewFolder);
            initCM.Initializing(initModel._viewFolderContextMenu , initModel._convertIcon);
        }
      
    }
}
