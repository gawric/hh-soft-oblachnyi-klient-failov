﻿using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext
{
    public class CopyLinkViewEvent
    {
        private ViewFolder _viewFolder;
        private UpdateNetworkJsonController _sendJsonNewtwork;

        public CopyLinkViewEvent(ViewFolder viewFolder , UpdateNetworkJsonController sendJsonNewtwork)
        {
            _viewFolder = viewFolder;
            _sendJsonNewtwork = sendJsonNewtwork;
        }


        public void copyLink()
        {
            if ((FolderNodes)_viewFolder.ListView.SelectedItem != null)
            {
                List<FolderNodes> listGenLink = _viewFolder.ListView.SelectedItems.OfType<FolderNodes>().ToList();
                Console.WriteLine("CopyLinkViewEvent->copyLink " + "Попытка получить copyLinkView");
                _sendJsonNewtwork.updateSendJsonREQUESTGENLINK(listGenLink[0].Row_id);
            }
            else
            {
                Console.WriteLine("CopyContextMenu_Click каталог или файл не выделен");
            }
        }
    }
}
