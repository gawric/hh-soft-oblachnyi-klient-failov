﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.viewFolderModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext
{
    public class PasteOrMoveEvent
    {
        private PasteOrMoveEventModel _moveModel;


        public PasteOrMoveEvent(PasteOrMoveEventModel moveModel)
        {
          this._moveModel =  moveModel;
        }

        public void paste(object sender, RoutedEventArgs e)
        {
            if (_moveModel._viewFolder.ListView.SelectedItem != null)
            {
                //выделенный нод из ViewFolder
                FolderNodes PasteNodes = (FolderNodes)_moveModel._viewFolder.ListView.SelectedItem;
                //ноды которые мы будем копировать
                List<FolderNodes> copyNodes = _moveModel._viewModelMain.getListCopyNodes();
             

                StartCopyOrMove(copyNodes, PasteNodes);

            }
            else
            {
                Console.Write("ViewFolder->PasteContextMenu_Clik -> Если не выделен FolderNodes мы берем его из нашего дерева " + _moveModel._viewModelMain.ActiveNodeID);

                long ActiveNodesTreeView = _moveModel._viewModelMain.ActiveNodeID[0];
                FileInfoModel serverModel = _moveModel._sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(ActiveNodesTreeView);
                List<FolderNodes> copyNodes2 = _moveModel._viewModelMain.getListCopyNodes();
                //пересоздаем из модели BD в модель TreeView или ViewFolder
                FolderNodes PasteNodes = new CreateModel(_moveModel._convert).createModelFolderNodesToViewFolder(serverModel);

                StartCopyOrMove(copyNodes2, PasteNodes);


            }

            _moveModel._viewModelMain.getListCopyNodes().Clear();
        }


        private void StartCopyOrMove(List<FolderNodes> copyNodes, FolderNodes PasteNodes)
        {
            string Event;

            if (_moveModel._copyStatus.isMove)
            {
                Event = "MOVE";
                //после опознавания event скидываем его обратно в false
                _moveModel._copyStatus.isMove = false;
            }
            else
            {
                Event = "PASTE";

            }



            //в await можем передать только локальную переменную
            List<FolderNodes> listCopy = new List<FolderNodes>();
            listCopy.AddRange(copyNodes);

            //запуск в новом потоке
            Task.Run(() =>
            {
                _moveModel._pasteController.PasteTask(Event, PasteNodes, listCopy);
            });

        }

        

    }
}
