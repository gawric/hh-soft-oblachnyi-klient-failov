﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext
{
    public class LostFocusEvent
    {
        private ViewFolderViewModel _viewModel;
        private MainWindowViewModel _viewModelMain;
        private ViewFolder _viewFolder;

        public LostFocusEvent(ViewFolderViewModel _viewModel , MainWindowViewModel _viewModelMain , ViewFolder _viewFolder)
        {
            this._viewModel = _viewModel;
            this._viewModelMain = _viewModelMain;
            this._viewFolder = _viewFolder;
        }

        public void lostFocus()
        {
            hideRenameFolderNodes();
        }

        //скрывает textBox для записывания данных
        public void hideRenameFolderNodes()
        {

            //проверяем из какой области запущенно переименование
            //ListView
            if (_viewModel._isRenameActiveToListView)
            {
                //здесь код обновление listview

                FolderNodes nod = (FolderNodes)_viewFolder.ListView.SelectedItem;
                nod.FolderNameVisibility = Visibility.Visible;
                nod.EditableTextBoxHeader = Visibility.Hidden;
                _viewModel._isRenameActiveToListView = false;

            }
            else
            {
                //или TreeView
                if (_viewModelMain._isRenameActiveToTreeView)
                {



                    _viewModelMain.RenameTreeView();
                    _viewModelMain._isRenameActiveToTreeView = false;
                }
                else
                {
                    //здесь код обновление listview
                    Console.WriteLine("ViewFolder->hideRenameFolderNodes->Rename TreeView Мы нечего не делаем");
                }

            }

            if (_viewFolder.ListView.SelectedItem != null)
            {
                FolderNodes nod = (FolderNodes)_viewFolder.ListView.SelectedItem;
                nod.FolderNameVisibility = Visibility.Visible;
                nod.EditableTextBoxHeader = Visibility.Collapsed;

                _viewModel._isRenameActiveToListView = false;
            }

        }
    }
}
