﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext
{
    public class CreateViewEvent
    {
        private MainWindowViewModel _viewModelMain;
        private SqliteController _sqlLiteController;
        private CreateFolderController _createFolderController;
        private ConvertIconType _convert;

        public CreateViewEvent(MainWindowViewModel _viewModelMain , SqliteController _sqlLiteController , ConvertIconType _convert , CreateFolderController createFolderController)
        {
            this._viewModelMain = _viewModelMain;
            this._sqlLiteController = _sqlLiteController;
            this._convert = _convert;
            this._createFolderController = createFolderController;
        }

        public void create()
        {
            FolderNodes sourceNodes = getActiveTreeViewNodes();
            List<FileInfoModel> listFolder = getActiveTreeViewNodesChildren();

            if (sourceNodes != null)
            {
                startCreateFolder(sourceNodes);
            }
        }

        private void startCreateFolder(FolderNodes sourceNodes)
        {
            sourceNodes.Location = checkLocationRoot(sourceNodes.Location);
            string nameName = _createFolderController.CreateNameTreeViewFolder(sourceNodes.Children);
            _createFolderController.TrainingCreateFolder(convertFnToFim(sourceNodes), nameName);
            FileInfoModel[] arr = _createFolderController.CreateModelArr(_sqlLiteController, convertFnToFim(sourceNodes), nameName);
        }

        private FileInfoModel convertFnToFim(FolderNodes fol)
        {
            FileInfoModel fim = new FileInfoModel();
            fim.row_id = fol.Row_id;
            fim.location = fol.Location;
            fim.type = fol.Type;
            fol.ParentID = fol.ParentID;
            fol.Icon = fol.Icon;

            return fim;
        }
        private string checkLocationRoot(string location)
        {
            if (location.Equals("disk") || location.Equals("Disk"))
            {
                location = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;
            }

            return location;
        }

        private List<FileInfoModel> getActiveTreeViewNodesChildren()
        {
            return _sqlLiteController.getSelect().GetSqlLiteParentIDFileInfoList(_viewModelMain.ActiveNodeID[0]);
        }

        private FolderNodes getActiveTreeViewNodes()
        {
            FileInfoModel fileInfo = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(_viewModelMain.ActiveNodeID[0]);
            return new CreateModel(_convert).convertFileInfoToFolderNodes(fileInfo);
        }
    }
}
