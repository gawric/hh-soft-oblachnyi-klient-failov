﻿using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other
{
    public class RebindingViewFolder
    {
        private ViewFolderViewModel _viewModel;
        private ViewFolder _viewFolder;


        public RebindingViewFolder(ViewFolderViewModel _viewModel , ViewFolder _viewFolder)
        {
            this._viewModel = _viewModel;
            this._viewFolder = _viewFolder;
        }

        
       public void rebindingListView(object sender, ArgsDataListView e)
       {
                TreeViewModel treeViewItems = new TreeViewModel();
                treeViewItems.Items = e.getObs;


                App.Current.Dispatcher.Invoke(new System.Action(() =>
                {
                    _viewModel.NodesView.Items.Clear();
                    _viewModel.NodesView.Items = null;
                    //Console.WriteLine("Rebinding list "+e.getObs.Count());
                    _viewModel.NodesView = treeViewItems;
                    _viewFolder.ListView.DataContext = _viewModel.NodesView;
                }));

            }
        }
}
