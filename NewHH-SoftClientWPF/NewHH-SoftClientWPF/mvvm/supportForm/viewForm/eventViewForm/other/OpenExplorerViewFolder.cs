﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other
{
    public class OpenExplorerViewFolder
    {
        private SqliteController _sqlliteController;
        private ExceptionController _error;

       

        public OpenExplorerViewFolder(SqliteController sqlliteController, ExceptionController error)
        {
            _sqlliteController = sqlliteController;
            _error = error;
        }
        public void Open(string locationFolderNodes, long parent)
        {
            string locationDownload = _sqlliteController.getSelect().getLocationDownload();
            string href = staticVariable.UtilMethod.convertLocationToHref(locationFolderNodes, locationDownload);

            if (isExistsFolder(href))
            {
                Process.Start("explorer.exe", href);
            }
            else
            {
                if(isExistsFiles(href))
                {
                    FileInfoModel parentNode = _sqlliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(parent);
                    string hrefParent = staticVariable.UtilMethod.convertLocationToHref(parentNode.location, locationDownload);
                    Process.Start("explorer.exe", hrefParent);
                }
                else
                {
                    _error.sendError((int)CodeError.FILE_SYSTEM_EXCEPTION, "Внимание!", "На компьютере такой папки или файла нет");
                }
                
            }


        }

        private bool isExistsFolder(string href)
        {
            bool check = false;
            if (Directory.Exists(href))
            {
                check = true;
            }
           
            return check;

        }

        private bool isExistsFiles(string href)
        {
            bool check = false;
            if (File.Exists(href))
            {
                check = true;
            }

            return check;

        }
    }
}
