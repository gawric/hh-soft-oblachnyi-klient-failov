﻿using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other
{
    public class ListCopyNodesEventView
    {
        private MainWindowViewModel _viewModelMain;
        private SqliteController _sqlLiteController;

        public ListCopyNodesEventView(MainWindowViewModel viewModelMain , SqliteController sqlLiteController)
        {
            this._viewModelMain = viewModelMain;
            this._sqlLiteController = sqlLiteController;
        }
        public void setEventListCopyNodes(List<FolderNodes> allCopyFolder)
        {
            //записываем  кэш
            _viewModelMain.setListCopyNodes(allCopyFolder);

            if (allCopyFolder[0] != null)
            {
                //эта папка откуда будет происходить копировани или вырезание для состовления нового пути у файлов
                string RootLocation = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(allCopyFolder[0].ParentID).location;
                _viewModelMain.CopyAndCutRootLocation = RootLocation;
            }
            else
            {
                Console.WriteLine("ViewFolder->setListCopyNodes:" + "Рутовая папка копирования !!!НЕ ОПРЕДЕЛЕНА!!! копировани не возможно");
            }


        }
    }
}
