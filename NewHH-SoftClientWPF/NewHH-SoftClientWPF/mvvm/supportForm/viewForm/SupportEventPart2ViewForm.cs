﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using NewHH_SoftClientWPF.mvvm.model.viewFolderModel;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.operationContext;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.eventViewForm.other;
using NewHH_SoftClientWPF.mvvm.supportForm.viewForm.operationContext;
using SimpleInjector;

using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewForm
{
    public class SupportEventPart2ViewForm
    {
        private MainWindowViewModel _viewModelMain;
        private SqliteController _sqlLiteController;
        private VisibleViewFormEvent _visibleEvent;
        private LostFocusEvent _lostFocusEvent;
        private UpdateFormMainController _updateMainForm;
        private ViewFolder _viewFolder;
        private ViewFolderViewModel _viewModel;
        private StackOperationUpDown _stackOperationUpDown;
        private ConvertIconType _convertIconType;
        private CreateFolderController _createFolderController;
        //private string oldFolderNodes = "";


        public SupportEventPart2ViewForm(Container cont)
        {
            _viewModelMain = cont.GetInstance<MainWindowViewModel>();
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _updateMainForm = cont.GetInstance<UpdateFormMainController>();
            _viewModel = cont.GetInstance<ViewFolderViewModel>();
            _stackOperationUpDown = cont.GetInstance<StackOperationUpDown>();
            _convertIconType = cont.GetInstance<ConvertIconType>();
            _createFolderController = cont.GetInstance<CreateFolderController>();

        }
        public void injectElemetns(ViewFolder viewFolder)
        {
            _viewFolder = viewFolder;
            _visibleEvent = new VisibleViewFormEvent( _viewFolder,  _updateMainForm);
            _lostFocusEvent = new LostFocusEvent(_viewModel, _viewModelMain, _viewFolder);
        }

        public void createFolderContextMenuEvent(object sender, RoutedEventArgs e)
        {
            CreateViewEvent createEvent = new CreateViewEvent( _viewModelMain,  _sqlLiteController, _convertIconType, _createFolderController);
            createEvent.create();
        }

        public void treeViewVisibleEvent(object sender, DependencyPropertyChangedEventArgs e , ref string oldFolderNodes)
        {
            TextBox renameTextbox = (TextBox)sender;
       
            _visibleEvent.visible(renameTextbox, e , ref oldFolderNodes);
        }

        public void hideRenameFolderNodes()
        {
            _lostFocusEvent.hideRenameFolderNodes();
        }
        public void hideRenameSetFolderName(string strTextBox , ref string oldFolderNodes)
        {
            _visibleEvent.hideRenameSetFolderName(strTextBox , ref oldFolderNodes);
        }

        public void treeViewRenameEvent(ViewFolderViewModel _viewModel, ViewFolder _viewFolder , ref string oldFolderNodes)
        {
            RenameViewEvent rename = new RenameViewEvent(_viewModel, _viewFolder);
            rename.rename(ref oldFolderNodes);
        }

        public void downloadViewEvent()
        {
            DownloadViewEvent downloadEvent = new DownloadViewEvent(_viewFolder , _stackOperationUpDown , _convertIconType);
            downloadEvent.download(true);
        }


       
        public void treeViewLostFocusEvent(object sender, RoutedEventArgs e)
        {
            
            _lostFocusEvent.lostFocus();
        }

        public void rebindingListViewEvent(object sender, ArgsDataListView e)
        {
            RebindingViewFolder rebindingEvent = new RebindingViewFolder( _viewModel,  _viewFolder);
            rebindingEvent.rebindingListView( sender,  e);
        }

       
    }
}
