﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer.eventViewTransferForm.transfer
{
    public class PauseUploadTransferEvent
    {
        private BlockingEventController _blockingController;
        private ViewTransfer _viewTransfer;
        private SupportViewTransfer _supportViewTransfer;



        public PauseUploadTransferEvent(BlockingEventController blockingController, ViewTransfer viewTransfer , SupportViewTransfer supportViewTransfer)
        {
            this._blockingController = blockingController;
            this._viewTransfer = viewTransfer;
            this._supportViewTransfer = supportViewTransfer;
        }


        public void pause(MenuItem menuItem)
        {
            
            IList list = _viewTransfer.ListView.SelectedItems;
            FolderNodesTransfer itemList = (FolderNodesTransfer)_viewTransfer.ListView.SelectedItem;

            if (itemList.TypeTransfer.Equals("upload"))
            {
                IContextMenuPause pause = new PauseUpload(_blockingController, _supportViewTransfer);
                bool[] block = { pause.isBlockingUpload(menuItem.Header.ToString()) };
                pause.startTrainingPause(list, block, itemList);
            }
            else
            {
                IContextMenuPause pause = new PauseDownload(_blockingController, _supportViewTransfer);
                bool[] block = { pause.isBlockingUpload(menuItem.Header.ToString()) };
                pause.startTrainingPause(list, block, itemList);
            }

        }
    }
}
