﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer
{
    public class InitializingViewTransfer
    {
        private ViewTransfer _viewTransfer;

        public InitializingViewTransfer(ViewTransfer viewTransfer)
        {
            this._viewTransfer = viewTransfer;
        }

        public  void InitializingContextMenu(ViewTransferContextMenu _viewContextMenu, ConvertIconType _convertIcon)
        {
            _viewContextMenu.HeaderPause = "Пауза";
            _viewContextMenu.HeaderStop = "Стоп";
            _viewContextMenu.HeaderClearAll = "Удалить все";
            _viewContextMenu.HeaderClearSingle = "Удалить выбранную";

            _viewContextMenu.HeaderPauseIcon = _convertIcon.getIconType(".contextMenuPause");
            _viewContextMenu.HeaderStopIcon = _convertIcon.getIconType(".contextMenuStop");
            _viewTransfer.ViewTransferContextMenuDemo.DataContext = _viewContextMenu;
        }

    }
}
