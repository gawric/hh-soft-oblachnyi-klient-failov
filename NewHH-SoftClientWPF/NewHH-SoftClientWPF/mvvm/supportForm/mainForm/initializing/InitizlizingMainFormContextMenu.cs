﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.initializing
{
    public class InitizlizingMainFormContextMenu
    {
        private MainWindow _mainWindow;

        public InitizlizingMainFormContextMenu(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
        }

        public void Initializing(MainWindowContextMenu _mainContextMenu, ConvertIconType _convertIcon)
        {

            _mainContextMenu.SaveOkText =    "Сохранить на компьютере";
            _mainContextMenu.SaveCloudText = "Оставить в облаке";
            _mainContextMenu.CopyLinkText = "Скопировать ссылку";
            _mainContextMenu.ContextMenuTreeViewCreate = _convertIcon.getIconType(".contextMenuCreate");
            _mainContextMenu.ContextMenuTreeViewSaveOk = _convertIcon.getIconType(".contextMenuItemSaveOk");
            _mainContextMenu.ContextMenuTreeViewSaveCloud = _convertIcon.getIconType(".cloud");
            _mainContextMenu.ContextMenuTreeViewCutFolder = _convertIcon.getIconType(".contextMenuItemCutFolder");
            _mainContextMenu.ContextMenuTreeViewCopyFolder = _convertIcon.getIconType(".contextMenuItemCopyFolder");
            _mainContextMenu.ContextMenuTreeViewPasteFolder = _convertIcon.getIconType(".contextMenuItemPasteFolder");
            _mainContextMenu.ContextMenuTreeViewRenameFolder = _convertIcon.getIconType(".contextMenuItemRenameFolder");
            _mainContextMenu.ContextMenuTreeViewDeleteFolder = _convertIcon.getIconType(".contextMenuItemDeleteFolder");
            _mainContextMenu.ContextMenuTreeViewCopyLink = _convertIcon.getIconType(".contextMenuItemCopyLink");
            _mainContextMenu.ContextMenuTreeViewGoFolder = _convertIcon.getIconType(".contextMenuItemGoFolder");

            _mainWindow.treeView1.ContextMenu.DataContext = _mainContextMenu;
        }
    }
}
