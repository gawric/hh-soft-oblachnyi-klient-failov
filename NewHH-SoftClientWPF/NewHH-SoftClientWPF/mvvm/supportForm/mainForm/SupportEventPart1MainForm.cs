﻿using Hardcodet.Wpf.TaskbarNotification;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.onlyCloud;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.changeIconViewModel;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model.taskBar;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.operationContext;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.transfer;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.windows;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NewHH_SoftClientWPF.mvvm.supportForm
{
    public class SupportEventPart1MainForm
    {
        private SqliteController _sqlLiteController;
      
        private UpdateFormMainController _updateMainForm;
        private MainWindowViewModel _viewModel;
       
        private MainWindow _mainWindow;
        private ViewFolderViewModel _viewModelViewFolder;
        private ViewFolder _viewFolder;
        private NavigationMainWindowsController _navigationMainController;
        private FirstSyncFilesController _firstSyncController;
        private CopyStatusController _copyStatus;
        private StackOperationUpDown _stackOperationUpDown;
        private CreateFolderController _createFolderController;
        private PasteWindowsController _pasteController;
        private ButtonUploadClick _uploadButton;
        private InitializingMainForm initializingMainForm;
        private ExceptionController _exceptionController;
        private ConvertIconType _convertIcon;
        private OpenContextMenuEvent _openContextMenu;
        private CreateModel _createModel;
        private UpdateNetworkJsonController _sendJsonNewtwork;




        public SupportEventPart1MainForm(Container cont)
        {
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _updateMainForm = cont.GetInstance<UpdateFormMainController>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _viewModelViewFolder = cont.GetInstance<ViewFolderViewModel>();
            _viewFolder = cont.GetInstance<ViewFolder>();
            _navigationMainController = cont.GetInstance<NavigationMainWindowsController>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            //статус копирования
            _copyStatus = cont.GetInstance<CopyStatusController>();
            _stackOperationUpDown = cont.GetInstance<StackOperationUpDown>();
            _createFolderController = cont.GetInstance<CreateFolderController>();
            _pasteController = cont.GetInstance<PasteWindowsController>();
            _uploadButton = cont.GetInstance<ButtonUploadClick>();
            _exceptionController = cont.GetInstance<ExceptionController>();
            _convertIcon = cont.GetInstance<ConvertIconType>();
            _openContextMenu = new OpenContextMenuEvent(cont.GetInstance<MainWindowContextMenu>() , _sqlLiteController , _convertIcon);
            _sendJsonNewtwork = cont.GetInstance<UpdateNetworkJsonController>();
            _createModel = new CreateModel(_convertIcon);



        }

        public void injectElemetns(InitializingMainForm initializingMainForm , MainWindow _mainWindow)
        {
            this.initializingMainForm = initializingMainForm;
            this._mainWindow = _mainWindow;
        }
        
        
        public void eventSearchTextBoxLeftDown(object sender, MouseButtonEventArgs e)
        {
            TextBox TextBoxSearch = (TextBox)sender;
            TextBoxSearch.Text = "";
        }

        public void eventSearchTextBoxLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox TextBoxSearch = (TextBox)sender;
            TextBoxSearch.Text = "Поиск..";
        }
        

        public void eventRemoveContextMenuClick(object sender, RoutedEventArgs e)
        {
            RemoveEvent removeEvent = new RemoveEvent( _mainWindow,  _firstSyncController);
            removeEvent.remove();
        }

        public void eventButtonInCloudClick(object sender, RoutedEventArgs e)
        {
            InCloudEvent inCloudEvent = new InCloudEvent(_uploadButton , _exceptionController);
            inCloudEvent.inCloud();
        }

        public void eventButtonInCloudFolderClick(object sender, RoutedEventArgs e)
        {
            InCloudEventFolder inCloudFolderEvent = new InCloudEventFolder(_uploadButton, _exceptionController);
            inCloudFolderEvent.inCloudFolder();
        }

        public void eventCreateFolderContextMenu(object sender, RoutedEventArgs e)
        {
            CreateMainEvent CreateMainEvent = new CreateMainEvent( _createFolderController,  _mainWindow,  _sqlLiteController , _exceptionController);
            CreateMainEvent.createFolder();
        }

        public void eventPasteContextMenu(object sender, RoutedEventArgs e)
        {
            PasteMainEvent pasteMainEvent = new PasteMainEvent( _mainWindow,  _pasteController,  _viewModel);
            pasteMainEvent.paste();
        }
        
        public void eventCopyContextMenu(object sender, RoutedEventArgs e)
        {
            CopyMainEvent copyMainEvent = new CopyMainEvent(_viewModel, this, _mainWindow);
            copyMainEvent.copy();
        }

       

        public void eventDownloadContextMenu(object sender, RoutedEventArgs e)
        {
            
            DownloadMainEvent downloadmainEvent = new DownloadMainEvent(_mainWindow, _stackOperationUpDown , _convertIcon);
            downloadmainEvent.download(false);
        }

        public void eventOnlyCloudContextMenu(object sender, RoutedEventArgs e)
        {
            OnlyCloudMainEvent onlyClodMainEvent = new OnlyCloudMainEvent(_createModel.createModel(_sqlLiteController,  _convertIcon, _viewModel.NodesView.Items, _viewModelViewFolder.NodesView.Items ,  _exceptionController));
            onlyClodMainEvent.cloud( sender,  e , _mainWindow);
        }


        public void eventCutContextMenu(object sender, RoutedEventArgs e)
        {
            CutMainEvent cutMainEvent = new CutMainEvent( _copyStatus,_viewModel,  _mainWindow,  this);
            cutMainEvent.cut();
        }

         public void eventCopyLinkContextMenu(object sender, RoutedEventArgs e)
        {
            CopyLinkMainEvent copyLinkMainEvent = new CopyLinkMainEvent(_mainWindow , _sendJsonNewtwork , _viewFolder);
            copyLinkMainEvent.copyLink();
        }

        public void eventOpenContextMenu(object sender, RoutedEventArgs e)
        {
            FolderNodes nodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
            if (nodes != null)
            {
                _openContextMenu.open(nodes.Row_id);
            }
           

        }
        
        public void setListCopyNodes(FolderNodes nodes)
        {
            //записываем  кэш
            _viewModel.addListCopyNodes(nodes);
            //эта папка откуда будет происходить копировани или вырезание для состовления нового пути у файлов
            string RootLocation = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(nodes.ParentID).location;
            _viewModel.CopyAndCutRootLocation = RootLocation;
        }
        

        //кого будем лениво обновлять TreeView или ListView
        public void UpdateLazyLodf(TreeViewItem tvi, string lazyEvent)
        {
            _mainWindow.FrameViewFolder.Navigate(_viewFolder);

            FolderNodes rootFolder = (FolderNodes)tvi.DataContext;
           
          
            long currentRowId = rootFolder.Row_id;

            if (rootFolder != null)
            {
                if (lazyEvent.Equals("LazyTreeView"))
                {
                    _navigationMainController.setNewOpenNode(false);
                    _updateMainForm.updateMainLazyTreeView(currentRowId.ToString(), rootFolder.Children, tvi);
                }
                else
                {

                    _updateMainForm.updateMainLazyListView(currentRowId.ToString(), rootFolder.Children);
                }


            }
        }

        public void taskBarSetIcon(object sender, ArgsDataSetIcon e)
        {
            if(e != null)
            {
                _mainWindow.taskBarIcon.Icon = e.getIcon;
               
            }
            
        }

        public void taskBarShowBallon(object sender, ArgsDataShowBallon e)
        {
            if (e != null)
            {
                _mainWindow.taskBarIcon.ShowBalloonTip(e.getTitle, e.getText, BalloonIcon.Info);
            }

        }


        public void eventExitButton(MainWindow main)
        {
            ExitWindowsEvent exitEvent = new ExitWindowsEvent(_viewModel);
            exitEvent.exitClickButton(main);
        }

        public void eventExitForm(MainWindow main)
        {
            ExitWindowsEvent exitEvent = new ExitWindowsEvent(_viewModel);
            exitEvent.exitClickForm(main);
        }

        public void eventOpenWindowsHelp(MainWindow main)
        {
            HelpWindowsEvent help = new HelpWindowsEvent();
            help.openWindowsHelp(main);
        }




    }

   

}
