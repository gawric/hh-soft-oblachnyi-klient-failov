﻿using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class Rebinding
    {
        private NavigationMainWindowsController _navigationMainController;

        public Rebinding(NavigationMainWindowsController navigationMainController)
        {
            this._navigationMainController = navigationMainController;
        }
        public void rebindingTreeView(object sender, ArgsDataTreeView e)
        {
            //подход реализован слега криво т.к DataContext отказывется нормально обновлять

            TreeViewItem tvi2 = e.getTvi;
            SortableObservableCollection<FolderNodes> obs = e.getObs;
            SortableObservableCollection<FolderNodes> nodes = e.getNodes;
            
            App.Current.Dispatcher.Invoke(new System.Action(() => nodes.AddRange(obs)));
            App.Current.Dispatcher.Invoke(new System.Action(() => _navigationMainController.setNewOpenNode(true)));
    
        }

       
    }
}
