﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class VisibleTreeViewEvent
    {
        private string oldFOlderName1;
        private MainWindow _mainWindow;
        private UpdateFormMainController _updateMainForm;

        public VisibleTreeViewEvent(ref string oldFOlderName1 , MainWindow _mainWindow , UpdateFormMainController _updateMainForm)
        {
            this.oldFOlderName1 = oldFOlderName1;
            this._mainWindow = _mainWindow;
            this._updateMainForm = _updateMainForm;
        }

        public void visible(TextBox renameTextbox, DependencyPropertyChangedEventArgs e)
        {
            //Visible - делаем видимым textBox
            if ((bool)e.NewValue == true)
            {
                renameTextbox.Focus();
                renameTextbox.SelectAll();
            }
            else
            {
                //Скрываем TextBox и переименовываем FolderNodes на переданный нам
                hideRenameSetFolderName(renameTextbox.Text);
            }
        }


        //Обновляет FolderName что-бы изменить именя в коллекции
        private void hideRenameSetFolderName(string strTextBox)
        {
            TreeViewItem tvi = _mainWindow.treeView1.Tag as TreeViewItem;

            if (tvi.DataContext != null)
            {
                FolderNodes nod = (FolderNodes)tvi.DataContext;

                SortableObservableCollection<FolderNodes> ChildrenNode = new SortableObservableCollection<FolderNodes>();
   

                if (oldFOlderName1.Equals(strTextBox) != true)
                {
                    nod.FolderName = strTextBox;
                    ChildrenNode.Insert(0, nod);

                    //Rename оповещает базы и webdav
                    _updateMainForm.updateMainRenameTreeView(oldFOlderName1, ChildrenNode);
                }
                else
                {
                    Console.WriteLine("*****MainWindow->hideRenameSetFolderName: Ошибка RENAME не будет сполнен> имя папки такое же на которое мы хотим его заменить*****");
                }

            }

        }
    }
}
