﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other
{
    public class OpenExplorer
    {
        private SqliteController _sqlliteController;
        private ExceptionController _error;
        public OpenExplorer(SqliteController sqlliteController , ExceptionController error)
        {
            _sqlliteController = sqlliteController;
            _error = error;
        }
        public void Open(string locationFolderNodes)
        {
            string locationDownload = _sqlliteController.getSelect().getLocationDownload();
            string href = staticVariable.UtilMethod.convertLocationToHref(locationFolderNodes , locationDownload);

            if(isExists(href))
            {
                Process.Start("explorer.exe", href);
            }
            else
            {
                _error.sendError((int)CodeError.FILE_SYSTEM_EXCEPTION, "Внимание!", "На компьютере такой папки нет");
            }
           
        }

        public void OpenToPathMinimized(string path)
        {
            if (isExists(path))
            {
               // System.Diagnostics.Process process = Process.Start("explorer.exe", path);
               // process.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;

                //string myDocspath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string windir = Environment.GetEnvironmentVariable("WINDIR");
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = windir + @"\explorer.exe";
                prc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
                prc.StartInfo.Arguments = path;
                prc.Start();

            }
        }

        private bool isExists(string href)
        {
            bool check = false;
            if(Directory.Exists(href))
            {
                check = true;
            }

            return check;

        }
    }
}
