﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other
{
    public class ExistSavePcEvent
    {
        private StackUpdateTreeViewIcon _treeViewSavePcIcon;
        private StackUpdateTreeViewIcon _listViewSavePcIcon;
        private SqliteController _sqlliteController;
        private ConvertIconType _convertIcon;

        //очень тяжкая операция ее делать можно только в фоновом режиме!!!!
        public ExistSavePcEvent(ConvertIconType convertIcon , SqliteController sqlliteController)
        {
            _convertIcon = convertIcon;
            _sqlliteController = sqlliteController;
            _treeViewSavePcIcon = new StackUpdateTreeViewIcon(_convertIcon);
            _listViewSavePcIcon = new StackUpdateTreeViewIcon(_convertIcon);
        }
        
        public void existStart(SortableObservableCollection<FolderNodes> rootTreeViewList , SortableObservableCollection<FolderNodes> rootListView)
        {
           // Task.Run(() =>
           // {
                List<long> listFilesRows = getAllSaveToPcRows();
                addRefreshPcSaveList(listFilesRows);

                startRefreshTreeView(rootTreeViewList, listFilesRows);
                startRefreshListView(rootListView, listFilesRows);
                
                string locationDownload = _sqlliteController.getSelect().getLocationDownload();
                startExist(listFilesRows , ref locationDownload , ref rootTreeViewList, ref rootListView);
                listFilesRows = null;
           // });
        }

        public void changePcSaveCloud(SortableObservableCollection<FolderNodes> rootTreeViewList, SortableObservableCollection<FolderNodes> rootListView)
        {
            List<long> listFilesRows = getAllSaveToPcRows();
            addCloudPcSaveList(listFilesRows);
            startAddCloudTree(_treeViewSavePcIcon,  rootTreeViewList,  listFilesRows);
            startAddCloudList(_treeViewSavePcIcon,  rootListView, listFilesRows);

            listFilesRows = null;

        }
        //перезаписывает новые id т.к всю базу пересканировали!
        public void rebuildPcSaveTable()
        {
            Dictionary<string, long> pcSaveListaDictionary = getAllSaveToPcLocation();
            Dictionary<string, long>  allListFilesLocation = getAllListFilesLocation();

            _sqlliteController.searchListFilesNewId(pcSaveListaDictionary, allListFilesLocation);
            pcSaveListaDictionary = null;
            allListFilesLocation = null;
        }



      
        private void startExist(List<long> listFilesRows , ref string locationDownload , ref SortableObservableCollection<FolderNodes> rootTreeViewList, ref SortableObservableCollection<FolderNodes> rootListView)
        {
            foreach (long listfilesId in listFilesRows)
            {
                SaveToPcModel sqlFiles = _sqlliteController.getSelect().getSqlLitePcSaveListFilesId(listfilesId);
                string href = convertLocationToHref(sqlFiles.location, locationDownload);

                checkStart( sqlFiles, ref href , ref rootTreeViewList, ref rootListView);
            }
        }

        private void checkStart(SaveToPcModel sqlFiles , ref string href ,ref  SortableObservableCollection<FolderNodes> rootTreeViewList, ref SortableObservableCollection<FolderNodes> rootListView)
        {
                if (staticVariable.Variable.isFolder(sqlFiles.type))
                {
                    if (System.IO.Directory.Exists(href))
                    {
                        checkFolder(sqlFiles , ref rootTreeViewList, ref rootListView, staticVariable.Variable.okIconId);
                    }
                    else
                    {
                        checkFolder(sqlFiles, ref rootTreeViewList, ref rootListView, staticVariable.Variable.normalIconId);
                        deleteSqlPcSave(sqlFiles.row_id_listfiles);
                    }

                }
                else
                {
                    if (System.IO.File.Exists(href))
                    {
                        checkFiles(sqlFiles , ref rootListView, ref rootTreeViewList, staticVariable.Variable.okIconId);
                    }
                    else
                    {
                        checkFiles(sqlFiles, ref rootListView, ref rootTreeViewList, staticVariable.Variable.normalIconId);
                        deleteSqlPcSave(sqlFiles.row_id_listfiles);
                    }
                }

        }
        
        private void deleteSqlPcSave(long listFilesRows)
        {
            _sqlliteController.getDelete().DeleteSavePcSinglRows(listFilesRows);
        }
        private void checkFolder(SaveToPcModel sqlFiles, ref SortableObservableCollection<FolderNodes> rootTreeViewList, ref SortableObservableCollection<FolderNodes> rootListView, int intIconId)
        {
            string newType = _treeViewSavePcIcon.changeIconReturn(sqlFiles.type, intIconId);
            _sqlliteController.updateListSaveToPc(sqlFiles.row_id_listfiles , newType , intIconId);

            update(sqlFiles,ref rootTreeViewList, ref rootListView, intIconId);
        }

        private void checkFiles(SaveToPcModel sqlFiles, ref SortableObservableCollection<FolderNodes> rootTreeViewList, ref SortableObservableCollection<FolderNodes> rootListView, int intIconId)
        {
            string newType = _treeViewSavePcIcon.changeIconReturn(sqlFiles.type, staticVariable.Variable.okIconId);
            _sqlliteController.updateListSaveToPc(sqlFiles.row_id_listfiles, newType, staticVariable.Variable.okIconId);

            update(sqlFiles, ref rootTreeViewList, ref rootListView, staticVariable.Variable.okIconId);
        }

      

        private void update(SaveToPcModel sqlFiles, ref SortableObservableCollection<FolderNodes> rootTreeViewList, ref SortableObservableCollection<FolderNodes> rootListView, int intIconId)
        {
            List<long> list = new List<long>();
            list.Add(sqlFiles.row_id_listfiles);

            _treeViewSavePcIcon.TraverseTreeList(rootTreeViewList, list, intIconId);
            _listViewSavePcIcon.TraverseChangeViewFolderList(rootListView, list, intIconId);
        }


        private string convertLocationToHref(string parentLocation, string location)
        {
            if(parentLocation != null)
            {
                string remoteWebDav = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;


                string hrefLocal = parentLocation.Replace(remoteWebDav, location + "\\");
                hrefLocal = hrefLocal.Replace("/", "\\");

                string hrefLocalDecode = staticVariable.Variable.DecodeUrlString(hrefLocal);


                return hrefLocalDecode;
            }
            else
            {
                Console.WriteLine("ExistSavePcEvent->convertLocationToHref: parentLocation не определен!!!"+parentLocation );
                return "";
            }
            
        }

        public void addRefreshPcSaveList(List<long> listFilesRows)
        {
           foreach(long listFilesId in listFilesRows)
           {
               string originalType =  _sqlliteController.getSelect().getSaveToPcType(listFilesId);
               string newType = _treeViewSavePcIcon.changeIconReturn(originalType, staticVariable.Variable.refreshIconId);

               _sqlliteController.updateListSaveToPc(listFilesId, newType, staticVariable.Variable.refreshIconId);
           }
        }

        private void addCloudPcSaveList(List<long> listFilesRows)
        {
            foreach (long listFilesId in listFilesRows)
            {
                string originalType = _sqlliteController.getSelect().getSaveToPcType(listFilesId);
                string newType = _treeViewSavePcIcon.changeIconReturn(originalType, staticVariable.Variable.normalIconId);

                _sqlliteController.updateListSaveToPc(listFilesId, newType, staticVariable.Variable.normalIconId);
            }
        }


        private List<long> getAllSaveToPcRows()
        {
           return  _sqlliteController.getSelect().getAllSaveToPcListFilesId();
        }

        private Dictionary<string, long>  getAllSaveToPcLocation()
        {
            return _sqlliteController.getSelect().getAllSaveToPcListFilesLocation();
        }

        private Dictionary<string, long> getAllListFilesLocation()
        {
            return _sqlliteController.getSelect().getAllHref(new Dictionary<long, string>());
        }


        

        private void startRefreshTreeView(SortableObservableCollection<FolderNodes> rootTreeViewList, List<long> listFilesRows)
        {
            
                startAddRefreshTree(_treeViewSavePcIcon, rootTreeViewList, listFilesRows);
            
        }


        private void startRefreshListView(SortableObservableCollection<FolderNodes> rootListView, List<long> listFilesRows)
        {
            
                startAddRefreshList(_listViewSavePcIcon, rootListView, listFilesRows);
            
        }



        private void startAddRefreshTree(StackUpdateTreeViewIcon _treeViewSavePcIcon, SortableObservableCollection<FolderNodes> rootTreeViewList, List<long> listFilesRows)
        {
            _treeViewSavePcIcon.TraverseTreeList(rootTreeViewList, listFilesRows, staticVariable.Variable.refreshIconId);
        }

        private void startAddRefreshList(StackUpdateTreeViewIcon _listViewSavePcIcon, SortableObservableCollection<FolderNodes> rootTreeViewList, List<long> listFilesRows)
        {
            _listViewSavePcIcon.TraverseTreeList(rootTreeViewList, listFilesRows, staticVariable.Variable.refreshIconId);
        }

        private void startAddCloudTree(StackUpdateTreeViewIcon _treeViewSavePcIcon, SortableObservableCollection<FolderNodes> rootTreeViewList, List<long> listFilesRows)
        {
            _treeViewSavePcIcon.TraverseTreeList(rootTreeViewList, listFilesRows, staticVariable.Variable.normalIconId);
        }

        private void startAddCloudList(StackUpdateTreeViewIcon _listViewSavePcIcon, SortableObservableCollection<FolderNodes> rootTreeViewList, List<long> listFilesRows)
        {
            _listViewSavePcIcon.TraverseTreeList(rootTreeViewList, listFilesRows, staticVariable.Variable.normalIconId);
        }

    }
}
