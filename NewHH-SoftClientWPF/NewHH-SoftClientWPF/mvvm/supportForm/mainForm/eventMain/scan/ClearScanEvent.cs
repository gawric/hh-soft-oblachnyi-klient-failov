﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.mvvm.model.changeIconViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class ClearScanEvent
    {
        private MainWindow _mainWindow;
        private CancellationController _cancellationController;
        private ConvertIconType _convertIcon;
        private FirstSyncFilesController _firstSyncController;


        public ClearScanEvent(MainWindow _mainWindow , CancellationController _cancellationController , ConvertIconType _convertIcon , FirstSyncFilesController _firstSyncController)
        {
            this._mainWindow = _mainWindow;
            this._cancellationController = _cancellationController;
            this._convertIcon = _convertIcon;
            this._firstSyncController = _firstSyncController;

        }

        public void start(MenuItem menuItem)
        {
            if (isStartClearScan(menuItem))
            {
                //отменяем операцию автосканирования если у нас пришла операция очистки и пересканирования
                _cancellationController.startCansel(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID);
                //с полной очистки
                _firstSyncController.ResponceServerScheduleDubbing();

            }
            else
            {
                _cancellationController.startCansel(staticVariable.Variable.autoSyncClearBasesCancellationTokenId);

            }
        }




        //Меняет картинку у найденного элемента
        public void changeIconMenuClearScan(object sender, ArgsDataChangeIconMenuViewModel e)
        {

            App.Current.Dispatcher.Invoke(new System.Action(() =>
            {
                Image image = (Image)_mainWindow.FindName(e.getNameSources);

                if (image != null)
                {
                    image.Source = _convertIcon.getIconType(e.getIconName);
                }

                MenuItem itemMenu = (MenuItem)_mainWindow.FindName(e.getMenuSourceItem);

                if (itemMenu != null)
                {
                    itemMenu.Header = e.getTextMenu;
                }


            }));
        }

        private bool isStartClearScan(MenuItem menuItem)
        {
            bool isStart = false;

            if (menuItem.Header.Equals(staticVariable.IconVariable.textMenuStartClearScan))
            {
                isStart = true;
            }

            return isStart;
        }
    }
}
