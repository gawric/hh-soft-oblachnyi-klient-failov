﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class InCloudEvent
    {
        private  ButtonUploadClick _uploadButton;
        private ExceptionController _exceptionController;

        public InCloudEvent(ButtonUploadClick _uploadButton , ExceptionController exceptionController)
        {
            this._uploadButton = _uploadButton;
            _exceptionController = exceptionController;
        }

        public void inCloud()
        {
            IOpenDialog dialog = _uploadButton.CreateDialog();

            if (dialog.FilePath != null)
            {
                try
                {
                    string[] selectHref = dialog.FilePath;

                    FileInfoModel pasteModel = _uploadButton.getPasteModel();
                    List<FileInfoModel> parentListPasteModel = _uploadButton.getChildrenPasteModel();
                    bool check = _uploadButton.isDublication(selectHref, parentListPasteModel);

                    _uploadButton.startTrainingUpload(check, selectHref, pasteModel);
                }
                catch(System.IO.DirectoryNotFoundException ex)
                {
                    Console.WriteLine("InCloudEvent->inCLoud: критическая ошибка загрузки файлов "+ex.ToString());
                    _exceptionController.sendError((int)CodeError.FILE_SYSTEM_EXCEPTION, "InCloudEvent->inCLoud: критическая ошибка загрузки файлов", ex.ToString());
                    
                }
                

            }
        }
    }
}
