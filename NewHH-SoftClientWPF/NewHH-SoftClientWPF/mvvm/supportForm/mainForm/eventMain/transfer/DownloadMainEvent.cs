﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class DownloadMainEvent
    {
        private StackOperationUpDown _stackOperationUpDown;
        private MainWindow _mainWindow;
        private ConvertIconType _convertIcon;

        public DownloadMainEvent(MainWindow _mainWindow , StackOperationUpDown _stackOperationUpDown , ConvertIconType _convertIcon)
        {
            this._mainWindow = _mainWindow;
            this._stackOperationUpDown = _stackOperationUpDown;
            this._convertIcon = _convertIcon;
        }

        public void download(bool isViewFolder)
        {
            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                
                List<FolderNodes> listDownload = new List<FolderNodes>();
                FolderNodes selectedNodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                if (UtilMethod.isRootId(selectedNodes.Row_id)) return;

                listDownload.Add(selectedNodes);
                _stackOperationUpDown.addStackDownload(listDownload , isViewFolder);
            }
        }

       
    }
}
