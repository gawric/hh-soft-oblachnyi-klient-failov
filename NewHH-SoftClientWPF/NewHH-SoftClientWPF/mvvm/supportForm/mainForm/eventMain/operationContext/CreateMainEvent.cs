﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.tehnicalException;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public  class CreateMainEvent
    {
      
        private MainWindow _mainWindow;
        private CreateFolderController _createFolderController;
        private SqliteController _sqlLiteController;
        private ExceptionController _exceptionController;

        public CreateMainEvent(CreateFolderController _createFolderController, MainWindow _mainWindow , SqliteController _sqlLiteController , ExceptionController exceptionController)
        {
            this._sqlLiteController = _sqlLiteController;
            this._mainWindow = _mainWindow;
            this._createFolderController = _createFolderController;
            _exceptionController = exceptionController;
        }

        public void createFolder()
        {
            if (_mainWindow.treeView1.Tag != null)
            {
                TreeViewItem tvi = _mainWindow.treeView1.Tag as TreeViewItem;

                if (tvi.DataContext != null)
                {
                    try
                    {
                        FolderNodes sourceNodes = (FolderNodes)tvi.DataContext;
                        startCreateFolder(sourceNodes);
                    }
                    catch (System.NullReferenceException z)
                    {
                        //Console.WriteLine("Ошибка MainWindow->CreateFolderContextMenu_Click: " + sc.ToString());
                        generatedError((int)CodeError.NULL_POINT_EXCEPTION, "CreateMainEvent->createFolder: Критическая ошибка NullReferenceException", z.ToString());
                    }


                }

            }
            else
            {
                //если папка вообще пустая мы тыкаем на диск
                if (_mainWindow.treeView1 != null)
                {
                    FolderNodes sourceNodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                    startCreateFolder(sourceNodes);
                }

            }
        }

        private void startCreateFolder(FolderNodes sourceNodes)
        {
            sourceNodes.Location = checkLocationRoot(sourceNodes.Location);
                //CreateNameTreeViewFolder
            string nameName = _createFolderController.CreateNameTreeViewFolder(sourceNodes.Children);
            _createFolderController.TrainingCreateFolder(convertFnToFim(sourceNodes), nameName);
            FileInfoModel[] arr = _createFolderController.CreateModelArr(_sqlLiteController, convertFnToFim(sourceNodes), nameName);
        }

        private FileInfoModel convertFnToFim(FolderNodes fol)
        {
            FileInfoModel fim = new FileInfoModel();
            fim.row_id = fol.Row_id;
            fim.location = fol.Location;
            fim.type = fol.Type;
            fol.ParentID = fol.ParentID;
            fol.Icon = fol.Icon;

            return fim;
        }

        private string checkLocationRoot(string location)
        {
            if (location.Equals("disk") || location.Equals("Disk"))
            {
                location = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;
            }

            return location;
        }

        private TechnicalException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new TechnicalException(codeError, textError, trhowError);
        }
    }
}
