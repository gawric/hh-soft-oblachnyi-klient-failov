﻿using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.operationContext
{
    public class CopyLinkMainEvent
    {

        private MainWindow _mainWindow;
        private UpdateNetworkJsonController _sendJsonNewtwork;
        private ViewFolder _viewFolder;

        public CopyLinkMainEvent(MainWindow mainWindow , UpdateNetworkJsonController sendJsonNewtwork , ViewFolder viewFolder)
        {
            _mainWindow = mainWindow;
            _sendJsonNewtwork = sendJsonNewtwork;
            _viewFolder = viewFolder;
        }


        public void copyLink()
        {
           

          Console.WriteLine(_viewFolder.ListView.SelectedItems.Count);
            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                FolderNodes nodes = getSelectNodes();

                if (UtilMethod.isRootId(nodes.Row_id)) return;
 
                _sendJsonNewtwork.updateSendJsonREQUESTGENLINK(nodes.Row_id);
            }
            else
            {
               // Console.WriteLine("CopyContextMenu_Click каталог или файл не выделен");
            }
        }

        private FolderNodes getSelectNodes()
        {
            if(_viewFolder.ListView != null)
            {
                int selectCountViewFolder = _viewFolder.ListView.SelectedItems.Count;

                if(selectCountViewFolder >= 1)
                {
                    return  (FolderNodes)_viewFolder.ListView.SelectedItem;
                }
                else
                {
                    return (FolderNodes)_mainWindow.treeView1.SelectedItem;
                }
            }
            else
            {
                return new FolderNodes();
            }
        }

    }
}
