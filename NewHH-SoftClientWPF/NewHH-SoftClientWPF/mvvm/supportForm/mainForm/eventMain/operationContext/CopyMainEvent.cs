﻿using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class CopyMainEvent
    {
        private MainWindowViewModel _viewModel;
        private SupportEventPart1MainForm _supportEventPart1MainForm;
        private MainWindow _mainWindow;

        public CopyMainEvent(MainWindowViewModel _viewModel , SupportEventPart1MainForm _supportEventPart1MainForm  , MainWindow _mainWindow)
        {
            this._viewModel = _viewModel;
            this._supportEventPart1MainForm = _supportEventPart1MainForm;
            this._mainWindow = _mainWindow;
        }

        public void copy()
        {
            _viewModel.getListCopyNodes().Clear();
            _viewModel.CopyAndCutRootLocation = "";

            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                FolderNodes nodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                if (UtilMethod.isRootId(nodes.Row_id)) return;
                _supportEventPart1MainForm.setListCopyNodes(nodes);
            }
            else
            {
                Console.WriteLine("CopyContextMenu_Click каталог или файл не выделен");
            }
        }
    }
}
