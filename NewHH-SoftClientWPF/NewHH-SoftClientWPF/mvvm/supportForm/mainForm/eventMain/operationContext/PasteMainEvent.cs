﻿using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class PasteMainEvent
    {
        private MainWindow _mainWindow;
        private PasteWindowsController _pasteController;
        private MainWindowViewModel _viewModel;

        public PasteMainEvent(MainWindow _mainWindow , PasteWindowsController _pasteController , MainWindowViewModel _viewModel)
        {
            this._mainWindow = _mainWindow;
            this._pasteController = _pasteController;
            this._viewModel = _viewModel;
        }

        public void paste()
        {

            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                FolderNodes PasteNodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                if (UtilMethod.isRootId(PasteNodes.Row_id)) return;
                string Event = _pasteController.insertEvent();

                Task.Run(() =>
                {
                    _pasteController.PasteTask(Event, PasteNodes, _viewModel.getListCopyNodes());
                });

            }
            else
            {
                Console.WriteLine("MainWindows-> PasteContextMenu_Click каталог или файл не выделен");
            }
        }

    }
}
