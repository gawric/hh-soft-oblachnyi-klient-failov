﻿using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class RenameMainEvent
    {
        private ViewFolderViewModel _viewModelViewFolder;
        private MainWindowViewModel _viewModel;
        private MainWindow _mainWindow;

        public RenameMainEvent(ref ViewFolderViewModel _viewModelViewFolder , ref MainWindowViewModel _viewModel , ref MainWindow _mainWindow)
        {
            this._viewModelViewFolder = _viewModelViewFolder;
            this._viewModel = _viewModel;
            this._mainWindow = _mainWindow;
        }


        //скрывает textBox для записывания данных
        public void hideRenameFolderNodes()
        {
            try
            {
                //проверяем из какой области запущенно переименование
                //ListView
                if (_viewModelViewFolder._isRenameActiveToListView)
                {
                    //здесь код обновление listview
                    Console.WriteLine("MainWindows->hideRenameFolderNodes->Запускаем Rename ListView");
                    _viewModelViewFolder.RenameListView();
                    _viewModelViewFolder._isRenameActiveToListView = false;
                }
                else
                {
                    //или TreeView
                    if (_viewModel._isRenameActiveToTreeView)
                    {
                        if (_mainWindow.treeView1.Tag != null)
                        {
                            TreeViewItem tvi = _mainWindow.treeView1.Tag as TreeViewItem;
                            FolderNodes nod = (FolderNodes)tvi.DataContext;
                            nod.FolderNameVisibility = Visibility.Visible;
                            nod.EditableTextBoxHeader = Visibility.Hidden;
                            _viewModel._isRenameActiveToTreeView = false;
                            //Console.WriteLine("MainWIndows->hideRenameFolderNodes->Запускаем Rename TreeView");
                        }
                    }
                    else
                    {
                        //здесь код обновление listview
                        //Console.WriteLine("MainWIndows->hideRenameFolderNodes->Нечего не делаем");
                    }

                }

            }
            catch (System.NullReferenceException a)
            {
                Console.WriteLine(a.ToString());
            }


        }

        //Открывает TextBox для ввода новых данных
        public void visibleRenameFolderNodes(ref string oldFOlderName1)
        {
            try
            {

                _viewModel._isRenameActiveToTreeView = true;
                TreeViewItem tvi = _mainWindow.treeView1.Tag as TreeViewItem;
                FolderNodes nod = (FolderNodes)tvi.DataContext;
                if (UtilMethod.isRootId(nod.Row_id)) return;
                nod.FolderNameVisibility = Visibility.Collapsed;
                nod.EditableTextBoxHeader = Visibility.Visible;

                if (staticVariable.Variable.isFolder(nod.Type))
                {
                    oldFOlderName1 = String.Copy(nod.FolderName) + "/";
                }
                else
                {
                    oldFOlderName1 = String.Copy(nod.FolderName);
                }


            }
            catch(System.NullReferenceException z)
            {
                Console.WriteLine("RenameMainEvent -> visibleRenameFolderNodes: " + z.ToString());
            }
           



        }

    }
}
