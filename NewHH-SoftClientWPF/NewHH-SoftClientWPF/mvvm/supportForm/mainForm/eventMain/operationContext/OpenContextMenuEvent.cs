﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.operationContext
{
    public class OpenContextMenuEvent
    {
        private MainWindowContextMenu _mainContextMenuModel;
        private SqliteController _sqlliteController;
        private ConvertIconType _convertIcon;

        public OpenContextMenuEvent(MainWindowContextMenu mainContextMenuModel , SqliteController sqlliteController , ConvertIconType convertIcon)
        {
            _mainContextMenuModel = mainContextMenuModel;
            _sqlliteController = sqlliteController;
            _convertIcon = convertIcon;
        }

        public void open(long row_id)
        {
            
                string typePc = getTypeSql(row_id);
                go(typePc);
            
          
            
        }

        private string getTypeSql(long row_id)
        {
            return _sqlliteController.getSelect().getSaveToPcType(row_id);
        }
        private void go(string typePc)
        {
           // changeContextItem(staticVariable.Variable.isTypeCloud(typePc));
        }

        private void changeContextItem(bool isTypeCloud)
        {
            if(isTypeCloud)
            {
                sendMenu("Оставить в облаке" , _convertIcon.getIconType(".cloud"));
            }
            
            
        }

        private void sendMenu(string nameItem , BitmapSource img)
        {
            _mainContextMenuModel.SaveOkText = nameItem;
            _mainContextMenuModel.ContextMenuTreeViewSaveOk = img;
        }
    }
}
