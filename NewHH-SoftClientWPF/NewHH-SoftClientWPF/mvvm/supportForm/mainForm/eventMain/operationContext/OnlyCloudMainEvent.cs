﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.convert;
using NewHH_SoftClientWPF.contoller.util.filesystem.delete;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.operationContext
{
    public class OnlyCloudMainEvent
    {

        private OnlyCloudController _onlyCloudController;
        private OnlyCloudMainModel _onlyCloudMainModel;
        private MainWindow _mainWindow;

        public OnlyCloudMainEvent(OnlyCloudMainModel onlyCloudMainModel)
        {
          _onlyCloudMainModel = onlyCloudMainModel;
          _onlyCloudController = new OnlyCloudController( onlyCloudMainModel);
 
        }

        public void cloud(object sender, RoutedEventArgs e , MainWindow _mainWindow)
        {
            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                FolderNodes foldernodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;

                if(!staticVariable.UtilMethod.isRootId(foldernodes.Row_id))
                {
                    cloud(foldernodes.Location);
                }

            }
        }

        public void cloud(string folderLocation)
        {
            Task.Run(() => _onlyCloudController.cloud(getList(folderLocation)));
            
        }

        public void cloud(List<string> listFolderlocation)
        {
            Task.Run(() => _onlyCloudController.cloud(listFolderlocation));
           
        }



        private List<string> getList(string folderLocation)
        {
            List<string> listFolderLocation = new List<string>();
            listFolderLocation.Add(folderLocation);
            return listFolderLocation;
        }


    }
}
