﻿using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class RemoveEvent
    {
        private MainWindow _mainWindow;
        private FirstSyncFilesController _firstSyncController;

        public RemoveEvent(MainWindow _mainWindow , FirstSyncFilesController _firstSyncController)
        {
            this._mainWindow = _mainWindow;
            this._firstSyncController = _firstSyncController;
        }

        public void remove()
        {
            if ((FolderNodes)_mainWindow.treeView1.SelectedItem != null)
            {
                FolderNodes deleteFoldersNodes = (FolderNodes)_mainWindow.treeView1.SelectedItem;
                if (UtilMethod.isRootId(deleteFoldersNodes.Row_id)) return;

                string Event = "DELETE";

                FolderNodes[] allDeleteFiles = new FolderNodes[1];
                allDeleteFiles[0] = deleteFoldersNodes;

                _firstSyncController.DeleteTask(deleteFoldersNodes, allDeleteFiles, Event);

            }
            else
            {
                Console.WriteLine("RemoveContextMenu_Click каталог или файл не выделен");
            }
        }
    }
}
