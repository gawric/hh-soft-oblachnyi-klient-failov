﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.windows
{
    public class WindowsStateEvent
    {
        private MainWindow _mainWin;
        private MainWindowViewModel _viewModel;

        public WindowsStateEvent(MainWindow mainWin , MainWindowViewModel viewModel)
        {
            this._mainWin = mainWin;
            this._viewModel = viewModel;
        }

        public void Window_StateChanged(object sender, EventArgs e)
        {
            switch (_mainWin.WindowState)
            {
                case WindowState.Maximized:
                   
                    break;
                case WindowState.Minimized:
                    Minimized();
                    break;
                case WindowState.Normal:
                    break;
            }
        }

        private void Minimized()
        {
            _mainWin.ShowInTaskbar = false;
            _viewModel.TaskBarIconVisability = Visibility.Visible;
        }

       
    }
}
