﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.tehnicalException;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.mainWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain
{
    public class SelectEvent
    {
        private SelectEventModel _model;
        private ExceptionController _exceptionController;

        public SelectEvent(SelectEventModel model , ExceptionController exceptionController)
        {
            _model = model;
            _exceptionController = exceptionController;
        }

        public void Select(ref BackNodesModel insert , RoutedEventArgs e , List<long> listSelectNodes , ref FolderNodes unSelectedFolderNodes)
        {
            try
            {

                _model._mainWindow.treeView1.Tag = e.OriginalSource;
                TreeViewItem tvi = _model._mainWindow.treeView1.Tag as TreeViewItem;

                //для rename если после rename нажали на открытие нодов
                _model._renameMainEvent.hideRenameFolderNodes();


                _model._supportEventPart1MainForm.UpdateLazyLodf(tvi, "LazyListView");

                FolderNodes fol = (FolderNodes)tvi.DataContext;

                _model._viewModel.ActiveNodeID[0] = fol.Row_id;
                
                if (!insert.isSelectBack & !insert.isSelectForward) _model._navigationMainController.SetSelect(ref listSelectNodes, fol, unSelectedFolderNodes);
               
                
                insert.isSelectBack = false;
                insert.isSelectForward = false;

            }
            catch(System.NullReferenceException z)
            {
               // Console.WriteLine("SupportEventPart2mainForm->SelectEvent: Select Ошибка " + a);
                generatedError((int)CodeError.NULL_POINT_EXCEPTION, "SupportEventPart2mainForm->SelectEvent: Критическая ошибка NullReferenceException", z.ToString());
            }
           
        }

        private TechnicalException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new TechnicalException(codeError, textError, trhowError);
        }



    }
}
