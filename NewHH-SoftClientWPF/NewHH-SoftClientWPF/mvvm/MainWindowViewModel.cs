﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.changeIconViewModel;
using NewHH_SoftClientWPF.mvvm.model.listViewModel;
using NewHH_SoftClientWPF.mvvm.model.taskBar;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static System.Net.Mime.MediaTypeNames;


namespace NewHH_SoftClientWPF.mvvm
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private MainWindowModel mainWindowModel;
        public event PropertyChangedEventHandler PropertyChanged;
        public TreeViewModel NodesView;
        public event EventHandler OnRefreshTreeView;
        public event EventHandler OnRenameTreeView;
        public event EventHandler<ArgsDataChangeIconMenuViewModel> OnChangeImageMenuClearScan;
        public event EventHandler<ArgsDataTreeView> OnRebindingTreeView;
        public event EventHandler<ArgsDataSetIcon> OnTaskBarSetIcon;
        public event EventHandler<ArgsDataShowBallon> OnTaskBarShowBallon;


        private List<long> listSelectNodes;
        private double pgSizeDisk;
        private Visibility taskBarIconVisability { get; set; }
        private Icon taskBarSetIcon { get; set; }
        //срабатывает когда нажимаем кнопку переименовать
        public bool _isRenameActiveToTreeView { get; set; }

        //для parentID для удаленных обновлений через ACtiveMq, что-бы знать какой актуальный сейчас  NOD
        public long[] ActiveNodeID { get; set; }

        //Содержит путь к папке откуда происходит копирование файлов
        //нужно для правильно преобразования нового пути
        public string CopyAndCutRootLocation { get; set; }

        //содержит данные запущено в данный момент приложение
        public bool isStartMainWindows { get; set; }
        private ICommand taskBarCommand { get; set; }

        private List<FolderNodes> listCopyNodes = new List<FolderNodes>();


        public void RebindingList(SortableObservableCollection<FolderNodes> obs , TreeViewItem tvi , SortableObservableCollection<FolderNodes> nodes)
        {
            ArgsDataTreeView a = new ArgsDataTreeView(obs , tvi , nodes);
            OnRebindingTreeView(this, a);
        }

        public void SetNewIconTaskBar(Icon _newIcon)
        {
            ArgsDataSetIcon a = new ArgsDataSetIcon(_newIcon);
            OnTaskBarSetIcon(this, a);
        }

        public void ShowBallonTaskBar(string title , string textName)
        {
            ArgsDataShowBallon a = new ArgsDataShowBallon(title , textName);
            OnTaskBarShowBallon(this, a);
        }

        public void OnChangeImage(string nameSources, string iconName , string menuSourceItem , string textMenu)
        {
            ArgsDataChangeIconMenuViewModel a = new ArgsDataChangeIconMenuViewModel(nameSources, iconName , menuSourceItem , textMenu);
            OnChangeImageMenuClearScan(this, a);
        }

      

        public List<FolderNodes>  getListCopyNodes()
        {
            if (listCopyNodes == null)
            {
                listCopyNodes = new List<FolderNodes>();
            }
            return listCopyNodes;
        }

        public void  addListCopyNodes(FolderNodes addNodes)
        {
            if (listCopyNodes == null)
            {
                listCopyNodes = new List<FolderNodes>();
            }
           
            listCopyNodes.Add(addNodes);
        }

        public void setListCopyNodes(List<FolderNodes>list)
        {
            if(listCopyNodes == null)
            {
                listCopyNodes = new List<FolderNodes>();
            }
            listCopyNodes = list;

        }


        public MainWindowViewModel()
        {
            mainWindowModel = new MainWindowModel();
            mainWindowModel.titlename = "Подключение...";
          // _NodesView = new ObservableCollection<FolderNodes>();
        }
        //Обновляет дерево TreeView
        public void RefreshTreeView()
        {
            App.Current.Dispatcher.Invoke(new System.Action(() => OnRefreshTreeView(this, new EventArgs())));
        }

        //Переименовать нод TreeView
        public void RenameTreeView()
        {
            App.Current.Dispatcher.Invoke(new System.Action(() => OnRenameTreeView(this, new EventArgs())));
        }

     



        public string Titlename
        {
            get { return mainWindowModel.titlename; }
            set
            {
                if (mainWindowModel.titlename != value)
                {
                    mainWindowModel.titlename = value;
                    OnPropertyChange("Titlename");

                }
            }
        }

     

        public ICommand TaskBarDoubleClick
        {
            get
            {
                return taskBarCommand;
            }
            set { 
                taskBarCommand = value;
                OnPropertyChange("TaskBarDoubleClick");
            }
        }

        public Visibility TaskBarIconVisability
        {
            get { return taskBarIconVisability; }
            set
            {
                if (taskBarIconVisability != value)
                {
                    taskBarIconVisability = value;
                    OnPropertyChange("TaskBarIconVisability");

                }
            }
        }

        public double pg
        {
            get { return mainWindowModel.pg; }
            set
            {

                mainWindowModel.pg = value;
                OnPropertyChange("pg");
            }
        }

        public double PgSizeDisk
        {
            get { return pgSizeDisk; }
            set
            {
                pgSizeDisk = value;
                OnPropertyChange("PgSizeDisk");
            }
        }


        public List<long> getListSelectNodes()
        {
            if (listSelectNodes == null) listSelectNodes = new List<long>();

            return listSelectNodes;
        }

        public void setListSelectNodes(long item)
        {
            if (listSelectNodes == null) listSelectNodes = new List<long>();

            listSelectNodes.Add(item);
        }

        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

