﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.mainWindow
{
    public class ChangeUserEventModel
    {
      
        public SqliteController _sqlliteController { get; set; }
        public MainWindowViewModel _viewModel { get; set; }
        public ViewFolderViewModel _viewModelViewFolder { get; set; }
        public BlockingEventController _blockingEventController { get; set; }
        public MqClientController _mqClientController { get; set; }
        public FirstSyncFilesController _firstSyncController { get; set; }
        public WebDavClientController _webDavClient { get; set; }
        public UpdateFormLoginController _updateForm { get; set; }
        public MqClientController _mqClient { get; set; }
        public LoginFormViewModel _loginFormViewModel { get; set; }
        public CancellationController _cancellationController { get; set; }
    }
}
