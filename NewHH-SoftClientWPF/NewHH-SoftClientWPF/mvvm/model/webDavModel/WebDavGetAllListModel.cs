﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class WebDavGetAllListModel
    {
        public UpdateNetworkJsonController updateNetworkJsonController { get; set; }
        public long newVersionBases { get; set; }
        public string username { get; set; }
        public SupportServerTopicModel supportTopicModel { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public WebDavClient _webdavclient { get; set; }
        public Progress<double> progress { get; set; }
        public MainWindowViewModel _viewModel { get; set; }
        public StatusAllOperationModel _allOperationModel { get; set; }
        public CancellationController _cancellationController { get; set; }
        public ExceptionController _exceptionController { get; set; }
    }   
}
