﻿using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm.model.renameJsonModel;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class RenameWebDavModel
    {

        //для отправки json файлов на сервер
        public UpdateNetworkJsonController _updateNetworkJsonController { get; set; }
        //для взаимодействия с sqllite
        public SqliteController _sqliteController { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        //куда происходит вставка
        public string pasteLocation { get; set; }

        //куда происходит вставка
        public string pasteName { get; set; }

        //старое имя
        public string oldFolderName { get; set; }
        //старое разположение
        public string oldLocation { get; set; }
        //клиент WebDav со всеми нужными методами
        public WebDavClient _webdavclient { get; set; }
        //WebDavClient - это подключенный webdav к серверу webdav
        public Client client { get; set; }

        public WebDavClientExist _webDavClientExist { get; set; }
        public StatusAllOperationModel allStatus { get; set; }
        //для преобразования Icon
        public FolderNodes rootNodesTreeView { get; set; }
        public SortableObservableCollection<FolderNodes> rootNodesListView { get; set; }
        public StackUpdateTreeViewIcon _stackUpdateTreeViewIcon {get;set;}
    }
}
