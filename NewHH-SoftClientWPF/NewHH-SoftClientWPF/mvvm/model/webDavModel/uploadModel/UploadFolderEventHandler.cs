﻿using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel
{
    public class UploadFolderEventHandler : EventArgs
    {
        public long version { get; set; }
        public FileInfoModel[] arr { get; set; }
        public UpdateNetworkJsonController networkJsonController { get; set; }
    }
}
