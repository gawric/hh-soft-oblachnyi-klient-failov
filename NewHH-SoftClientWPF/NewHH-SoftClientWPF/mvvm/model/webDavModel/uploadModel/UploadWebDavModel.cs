﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class UploadWebDavModel
    {
        
        public string username { get; set; }
        public string password { get; set; }
        public string[] allListHref { get; set; }
        public FileInfoModel pasteModel { get; set; }
       
        public Client client { get; set; }
        public string pasteHref { get; set; }
       
        //хранилизе файлов на передачу
        public FileInfoModel[] arr { get; set; }
        //хранилище последнего сгенерированного row_id 
        public long[] new_id_container { get; set; }
        public long version_bases { get; set; }
     
        public SotredSourceHrefToDouble checkParent { get; set; }
        public long sizeBytesFolder { get; set; }
   
        public Dictionary<string, long> allHrefFolderSqllite { get; set; }

        public Dictionary<string, long> dictUploadHrefRow_id { get; set; }
       
        public IDownload _statusDownload { get; set; }
        public EventHandler<UploadFolderEventHandler> updateEventHandler { get; set; }

        public InfoStackModel infoStack { get; set; }

        public  List<FileInfoModel> tempFileSubsribleList { get; set; }
    }
}
