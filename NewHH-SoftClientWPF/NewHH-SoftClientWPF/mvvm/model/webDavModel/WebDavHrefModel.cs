﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class WebDavHrefModel
    {
        public string href { get; set; }
        public string filename { get; set; }
        public string pasteHref { get; set; }

        public string lastOpenDate { get; set; }
        public string createDate { get; set; }
        public string changeDate { get; set; }

        public long size { get; set; }
    }
}
