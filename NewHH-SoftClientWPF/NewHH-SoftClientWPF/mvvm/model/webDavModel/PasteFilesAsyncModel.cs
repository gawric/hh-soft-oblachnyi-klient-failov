﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.webDavModel
{
    public class PasteFilesAsyncModel
    {
        public UpdateNetworkJsonController updateNetworkJsonController { get; set; }
        public long newVersionBases { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public SupportServerTopicModel supportTopicModel { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public string Event { get; set; }
        public string type { get; set; }
        public string copyFolderName { get; set; }
        public string copyLocation { get; set; }
        public FolderNodes pasteFolder { get; set; }
        public IProgress<CopyViewModel> progress { get; set; }
        public CopyViewModel status { get; set; }
        public bool replace { get; set; }
        public Dictionary<FolderNodes, bool> allCopyNodes;
        public FolderNodes currentCopyFolderNodes;
        public string copyOrCutRootLocation { get; set; }
        public Dictionary<FileInfoModel, bool> allPasteNodes { get; set; }
        public WebDavClientExist _webDavClientExist { get; set; }
        public ConvertIconType convertIcon { get; set; }
        public ExceptionController _exceptionController { get; set; }
    }
}
