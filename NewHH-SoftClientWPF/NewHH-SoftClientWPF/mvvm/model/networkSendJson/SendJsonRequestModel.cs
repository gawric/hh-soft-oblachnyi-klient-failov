﻿using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.newtworkSendJson
{
    public class SendJsonRequestModel
    {
        public ObserverNetwokSendJson onsj { get; set; }
        public MqClientController _mqclientController { get; set; }
        public SaveJsonToDiskContoller _saveJsonToDisk { get; set; }
        public ReadJsonToDiskController _readJsonToDisk { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public SupportServerTopicModel _supportTopicModel { get; set; }
        public UpdateFormMainController _updateMainForm { get; set; }
    }
}
