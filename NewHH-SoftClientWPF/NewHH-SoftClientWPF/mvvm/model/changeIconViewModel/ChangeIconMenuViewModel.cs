﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.changeIconViewModel
{
    public class ArgsDataChangeIconMenuViewModel : EventArgs
    {
        private string nameSources;
        private string iconName;
        private string menuSourceItem;
        private string textMenu;

        public ArgsDataChangeIconMenuViewModel(string nameSources , string iconName , string menuSourceItem, string textMenu)
        {
            this.nameSources = nameSources;
            this.iconName = iconName;
            this.menuSourceItem = menuSourceItem;
            this.textMenu = textMenu;
        }

        public string getNameSources
        {
            get { return nameSources; }
        }

        public string getIconName
        {
            get { return iconName; }
        }

        public string getMenuSourceItem
        {
            get { return menuSourceItem; }
        }

        public string getTextMenu
        {
            get { return textMenu; }
        }
    }
}
