﻿using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.renameJsonModel
{
    class RenameJsonModel
    {
        public RenameWebDavModel renameModel { get; set; }
        public FileInfoModel[] arr { get; set; }
        public long newVersionBases { get; set; }
        public string newLocation { get; set; }
        public long[][] list { get; set; }
    }
}
