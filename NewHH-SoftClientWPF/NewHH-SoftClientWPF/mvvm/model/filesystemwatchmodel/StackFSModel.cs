﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel
{
    public class StackFSModel
    {
        public string fullPath { get; set; }
        public string oldPath { get; set; }
        public string eventFS { get; set; }
    }
}
