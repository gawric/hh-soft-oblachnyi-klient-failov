﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.saveToPc
{
    public class SaveToPcModel
    {
        public long row_id_listfiles { get; set; }
        public int saveToPc { get; set; }
        public string type;
        public string location { get; set; }
    }
}
