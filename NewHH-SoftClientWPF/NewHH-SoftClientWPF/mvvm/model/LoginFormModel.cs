﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    class LoginFormModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string date { get; set; }
        public string textbuttonvalue { get; set; }
        public string texterrorvalue { get; set; }
        public double progressbarvalue { get; set; }
        
    }
}
