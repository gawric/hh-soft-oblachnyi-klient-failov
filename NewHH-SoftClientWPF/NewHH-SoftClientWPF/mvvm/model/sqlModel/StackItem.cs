﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.sqlModel
{
    public class StackItemSql<T>
    {
        private int operation;
        private T data1;
        private T data2;

        public StackItemSql(T data1 , T data2 , int operation)
        {
            this.data1 = data1;
            this.data2 = data2;
            this.operation = operation;
        }

        public StackItemSql(T data1, int operation)
        {
            this.data1 = data1;
            this.operation = operation;
        }

        public object GetData1()
        {
            return (object)data1;
        }
        public object GetData2()
        {
            return (object)data2;
        }

        public int GetOperation()
        {
            return operation;
        }
    }
}
