﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public class BackNodesModel
    {
        public int index { get; set; }
        public bool isSelectBack { get; set; }
        public bool isSelectForward { get; set; }
        public bool isFinish { get; set; }
    }
}
