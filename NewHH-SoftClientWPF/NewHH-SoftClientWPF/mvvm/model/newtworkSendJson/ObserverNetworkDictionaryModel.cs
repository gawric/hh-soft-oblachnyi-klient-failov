﻿using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.SupportDictionaryRequest;

namespace NewHH_SoftClientWPF.mvvm.model.newtworkSendJson
{
    public class ObserverNetworkDictionaryModel
    {
        public ObserverNetwokSendJson onsj;

        public Dictionary<string, OperationDelegate> _dictDelegatSend { get; set; }
        public Dictionary<string, OperationDelegateLazy> _dictDelegatLazy {get;set;}

        public Dictionary<string, string> _dictRequest { get; set; }
    }
}
