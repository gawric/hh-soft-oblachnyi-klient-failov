﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.autoScanModel
{
    public class AutoScanInfoModel
    {
        public int folder { get; set; }

       
        public int files { get; set; }

        public int allCount { get; set; }
        public string lastDataScan { get; set; }
      
        public string timeSpent { get; set; }

        public DateTime beginTimeScan { get; set; }
        public DateTime endTimeScan { get; set; }
    }
}
