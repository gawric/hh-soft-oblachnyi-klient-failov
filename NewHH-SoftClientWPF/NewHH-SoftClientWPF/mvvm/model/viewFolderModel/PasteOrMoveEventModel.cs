﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.viewFolderModel
{
    public class PasteOrMoveEventModel
    {
        public ViewFolder _viewFolder { get; set; }
        public MainWindowViewModel _viewModelMain { get; set; }
        public CopyStatusController _copyStatus { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public ConvertIconType _convert { get; set; }
        public PasteWindowsController _pasteController { get; set; }
    }
}
