﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public class FileInfoModel
    {
        public long row_id { get; set; }
        public string filename { get; set; }
        public string createDate { get; set; }
        public string changeDate { get; set; }
        public string lastOpenDate { get; set; }
        public string attribute { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public long sizebyte { get; set; }
        public long parent { get; set; }
        public long versionUpdateBases { get; set; }
        public long versionUpdateRows { get; set; }
        public long user_id { get; set; }
        public string changeRows { get; set; }
        public int saveTopc { get; set; }


    }
}
