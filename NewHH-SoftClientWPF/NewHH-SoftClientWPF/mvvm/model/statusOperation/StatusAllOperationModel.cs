﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.statusOperation
{
    public class StatusAllOperationModel
    {
        public bool isRename { get; set; }
        public bool isPaste { get; set; }
        public bool isDelete { get; set; }
        public bool isClearScan { get; set; }
        
        public bool[] isAllStatus()
        {
            bool[] all = new bool[4];

            all[0] = isRename;
            all[1] = isPaste;
            all[2] = isDelete;
            all[3] = isClearScan;

            return all;
        }
    
    }
}
