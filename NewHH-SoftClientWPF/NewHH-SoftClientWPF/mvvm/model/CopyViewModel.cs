﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public class CopyViewModel
    {
        public string title { get; set; }
        public string whence { get; set; }
        public string where { get; set; }
        public double pg { get; set; }
        public bool  isOpen { get; set; }
        public string StatusWorker { get; set; }
        public int Parties { get; set; }
    }
}
