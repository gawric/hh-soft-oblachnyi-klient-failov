﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.loginWindow
{
    public class LoginWindowModel
    {
        public LoginFormViewModel _viewModel { get; set; }
        public SqliteController sqliteControler { get; set; }
        public UpdateFormLoginController updateForm { get; set; }
        public WebDavClientController _webDavClient { get; set; }
        public MqClientController _mqClient { get; set; }
        public FirstSyncFilesController _firstSyncController { get; set; }
        public BlockingEventController _blockingEventController { get; set; }
        public LoginFormViewModel _loginFormViewModel { get; set; }
        public MainWindowViewModel _mainViewModel { get; set; }
    }
}
