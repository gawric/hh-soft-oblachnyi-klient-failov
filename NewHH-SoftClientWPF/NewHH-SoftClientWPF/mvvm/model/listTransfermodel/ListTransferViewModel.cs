﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.listtransfermodel
{
    public class ListTransferViewModel
    {
        public SortableObservableCollection<FolderNodesTransfer> Items { get; set; }

        public ListTransferViewModel()
        {
            Items = new SortableObservableCollection<FolderNodesTransfer>();
        }


    }
}
