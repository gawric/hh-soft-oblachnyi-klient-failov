﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.autoSync
{
    public class TraverseTreeModel
    {
        public WebDavClient client { get; set; }
        public long newVersionBases { get; set; }
        public UpdateNetworkJsonController networkJson { get; set; }
        public IProgress<double> progress { get; set; }
        public string[] usernameandPassword { get; set; }
        public SupportServerTopicModel supportTopicModel { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public Dictionary<long, long> sqlArray { get; set; }
        public Dictionary<string, long> sqlArrayHref { get; set; }
        public CancellationToken _cancellationToken { get; set; }
        public int[] countArray { get; set; }
      
  

    }
}
