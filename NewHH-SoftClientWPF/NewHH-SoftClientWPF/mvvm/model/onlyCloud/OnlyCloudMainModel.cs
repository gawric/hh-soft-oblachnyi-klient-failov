﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.onlyCloud
{
    public class OnlyCloudMainModel
    {
       public SqliteController sqlliteController { get; set; }
       public ConvertIconType convertIcon { get; set; }
       public SortableObservableCollection<FolderNodes> viewTreeList { get; set; }
       public SortableObservableCollection<FolderNodes> viewFolderList { get; set; }
       public ExceptionController _exceptionController { get; set; }
    }
}
