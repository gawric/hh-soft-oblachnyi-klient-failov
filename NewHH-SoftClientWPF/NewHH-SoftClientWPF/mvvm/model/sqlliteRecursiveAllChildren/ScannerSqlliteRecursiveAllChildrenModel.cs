﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren
{
    public class ScannerSqlliteRecursiveAllChildrenModel
    {
        public List<long[][]> allListFull { get; set; }
        public List<long[]> Alllist { get; set; }
        public long parent_id { get; set; }
        public SqliteController _sqlLiteController { get; set; }
        public IProgress<CopyViewModel> progress { get; set; }
        public CopyViewModel model { get; set; }
        public long[] countFiles { get; set; }
        public string progressLabel { get; set; }
        public ManualResetEvent _mre { get; set; }
        public CancellationToken _cancelToken{ get; set; }
        public string location { get; set; }
        public List<long[][]> allFiles { get; set; }
        public int[] p { get; set; }
        public Dictionary<long, List<long[]>> allParent { get; set; }
    }
}
