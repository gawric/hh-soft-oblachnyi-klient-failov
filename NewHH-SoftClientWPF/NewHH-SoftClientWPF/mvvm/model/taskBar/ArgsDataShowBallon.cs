﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.taskBar
{
   public  class ArgsDataShowBallon
    {
        private string _titleBallon;
        private string _textBallon;
        public ArgsDataShowBallon(string  titleBallon , string textBallon)
        {
            this._titleBallon = titleBallon;
            this._textBallon = textBallon;
        }


        public string getTitle
        {
            get { return _titleBallon; }
        }

        public string getText
        {
            get { return _textBallon; }
        }
    }
}
