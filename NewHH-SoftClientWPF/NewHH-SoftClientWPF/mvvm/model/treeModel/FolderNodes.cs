﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF
{
    public class FolderNodes : TreeViewItemBase 
    {
        
        public long Row_id { get; set; }
        public long ParentID { get; set; }
        public string Type { get; set; }
        public long VersionUpdateRows { get; set; }
        public string changeRows { get; set; }
        public string Location { get; set; }
        public string sizebyte { get; set; }
        public string createDate { get; set; }
        public string changeDate { get; set; }
        public string lastOpenDate { get; set; }
    

        
        public SortableObservableCollection<FolderNodes> Children { get; set; }

        public FolderNodes()
        {
            Children = new SortableObservableCollection<FolderNodes>();
           
        }

       
    }
}
