﻿using NewHH_SoftClientWPF.contoller.sorted;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.mvvm.model.treemodel
{
    public class ArgsDataTreeView : EventArgs
    {
        private SortableObservableCollection<FolderNodes> obs;
        private TreeViewItem tvi;
        private SortableObservableCollection<FolderNodes> nodes;


        public ArgsDataTreeView(SortableObservableCollection<FolderNodes> obs , TreeViewItem tvi , SortableObservableCollection<FolderNodes> nodes)
        {
            this.obs = obs;
            this.tvi = tvi;
            this.nodes = nodes;
        }

        public SortableObservableCollection<FolderNodes> getObs
        {
            get { return obs; }
        }

        public TreeViewItem getTvi
        {
            get { return tvi; }
        }

        public SortableObservableCollection<FolderNodes> getNodes
        {
            get { return nodes; }
        }
    }
}
