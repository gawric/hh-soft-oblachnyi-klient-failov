﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm.model.treemodel
{
        //Делает наследование для того что-бы извещать текущий nod Select или Expanded
        //т.к их нет xml HierarhiDataTemplate они  вынесены в отдельный стиль
        public class TreeViewItemBase : INotifyPropertyChanged
        {
            public TreeViewItemBase()
            {
                //isSelected = false;
                IsSelected = false;
                FolderNameVisibility = Visibility.Visible;
                EditableTextBoxHeader = Visibility.Collapsed;
                textBoxAllSelection = true;

            }
            private bool isSelected;
            private Visibility visibility_TextBlock;
            private Visibility visibility_editTable;
            private string folderName;
            private bool textBoxAllSelection;
            private BitmapSource icon;

            public bool IsSelected
            {
                get { return this.isSelected; }
                set
                {
                    if (value != this.isSelected)
                    {
                        this.isSelected = value;
                        NotifyPropertyChanged("IsSelected");
                    }
                }
            }

        public BitmapSource Icon
        {
            get { return this.icon; }
            set
            {
                if (value != this.icon)
                {
                    this.icon = value;
                    NotifyPropertyChanged("Icon");
                }
            }
        }


        public bool TextBoxAllSelection
        {
            get { return this.isSelected; }
            set
            {
                if (value != this.isSelected)
                {
                    this.isSelected = value;
                    NotifyPropertyChanged("TextBoxAllSelection");
                }
            }
        }

        private bool isExpanded;
            public bool IsExpanded
            {
                get { return this.isExpanded; }
                set
                {
                    if (value != this.isExpanded)
                    {
                        this.isExpanded = value;
                        NotifyPropertyChanged("IsExpanded");
                    }
                }
            }
        
        public Visibility EditableTextBoxHeader
        {
            get { return this.visibility_editTable; }
            set
            {
                if (value != this.visibility_editTable)
                {
                    this.visibility_editTable = value;
                    NotifyPropertyChanged("editableTextBoxHeader");
                }
            }
        }

       
        public string FolderName
        {
            get { return this.folderName; }
            set
            {
                if (value != this.folderName)
                {
                    this.folderName = value;
                    NotifyPropertyChanged("FolderName");
                }
            }
        }

        //скрывает текст нода
        public Visibility FolderNameVisibility
        {
            get { return this.visibility_TextBlock; }
            set
            {
                if (value != this.visibility_TextBlock)
                {
                    this.visibility_TextBlock = value;
                    NotifyPropertyChanged("FolderNameVisibility");
                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propName)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    
}
