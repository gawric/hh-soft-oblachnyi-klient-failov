﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.treemodel.insertTreeViewModel
{
    public class InsertTreeViewModel
    {
        public ConvertIconType convertIcon { get; set; }
        public SearchDublication searchDublication { get; set; }
    }
}
