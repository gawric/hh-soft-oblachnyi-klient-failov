﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.mvvm.model.treemodel
{
    public class FolderNodesTransfer: TransferItemBase
    {
        public long Row_id { get; set; }
        public long Row_id_listFiles_users { get; set; }
        public string FolderName { get; set; }
        public string Type { get; set; }
        public string TypeTransfer { get; set; }
    }
}
