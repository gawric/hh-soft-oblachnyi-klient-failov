﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.systemSettingModel
{
    public class SystemSettingModel
    {
        public string autoScan { get; set; }
        public int autoStartApplication { get; set; }
        public int startFolder { get; set; }
    }
}
