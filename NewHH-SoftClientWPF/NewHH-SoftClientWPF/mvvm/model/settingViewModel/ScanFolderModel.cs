﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.settingViewModel
{
    public class ScanFolderModel
    {
        public long row_id { get; set; }
        public string fullPath { get; set; }
        public string name { get; set; }
        public string location { get; set; }
    }
}
