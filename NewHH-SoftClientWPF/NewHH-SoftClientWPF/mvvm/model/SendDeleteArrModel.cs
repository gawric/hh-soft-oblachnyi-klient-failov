﻿using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model
{
    public class SendDeleteArrModel
    {
        public SqliteController sqliteController { get; set; }
        public List<string> locationList { get; set; }
    }
}
