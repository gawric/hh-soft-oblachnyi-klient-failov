﻿
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList
{
    class SyncCreateAllListModel
    {
        public MainWindowViewModel _viewModelMain { get; set; }
        public ViewFolderViewModel _viewModelViewFolder { get; set; }

        public UpdateDataController _updateDataController { get; set; }
        public UpdateFormMainController _updateMainForm { get; set; }
        public ServerTopicModel responceServer { get; set; }
        public ConvertIconType convertIcon { get; set; }
        public SqliteController sqlliteController { get; set; }
        public SortableObservableCollection<FolderNodes> rootTreeViewList { get; set; }
        public SortableObservableCollection<FolderNodes> rootListView { get; set; }
        public ObserverMainWindowFormUpdate _mainUpdateForm { get; set; }

    }
}
