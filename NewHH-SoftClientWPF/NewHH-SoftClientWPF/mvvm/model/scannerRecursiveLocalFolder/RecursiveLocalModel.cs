﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder
{
    public class RecursiveLocalModel
    {
        public long sizeBytesFolder{ get; set; }
        public int  sizeCountFiles{ get; set; }
        public int  sizeCountFolder { get; set; }
    }
}
