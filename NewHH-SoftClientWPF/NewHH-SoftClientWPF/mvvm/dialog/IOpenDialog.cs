﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.dialog
{
    public interface IOpenDialog
    {
        void CreateDialog();
        void CreateFolderDialog();
        void CreateFolderDialog(string path);
        string[] FilePath { get; set; }   // путь к выбранному файлу
        string[] FolderPath { get; set; }   // путь к выбранному папке
        string FileFolderPath { get; set; } // путь к выбранному папке
     
    }
}
