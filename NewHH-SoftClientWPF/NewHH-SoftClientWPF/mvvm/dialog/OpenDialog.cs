﻿using Microsoft.Win32;
using NewHH_SoftClientWPF.mvvm.dialog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm
{
    public class OpenDialog : IOpenDialog
    {
        private string loadingName = "Выбрать папку.";
        public string[] FilePath { get; set; }
        public string[] FolderPath { get; set; }
        public string FileFolderPath { get; set; }
        

        public void CreateDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.ValidateNames = false;
            openFileDialog.CheckFileExists = false;
            openFileDialog.CheckPathExists = true;
            openFileDialog.Multiselect = true;
           
            //Вставляет имя выбранного файла в самом начале
            openFileDialog.FileName = loadingName;

            if (openFileDialog.ShowDialog() == true)
            {
                Console.WriteLine("Click OpenFileDialog " + openFileDialog.FileNames);
                string[] selectedFiles = openFileDialog.FileNames;
                FilePath = CheckHref(selectedFiles);
                Debug.Print("");
            }

        }


        public void CreateFolderDialog()
        {
            Ookii.Dialogs.Wpf.VistaFolderBrowserDialog folderDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();

            if (folderDialog.ShowDialog() == true)
            {
                Console.WriteLine("Click OpenFolderDialog " + folderDialog.SelectedPath);
                string[] selectedFiles = { folderDialog.SelectedPath+"\\" };
                FolderPath = CheckHref(selectedFiles);
                Debug.Print("");
            }

        }

      
        public void CreateFolderDialog(string path)
        {
            using (var openFolderdialog = new System.Windows.Forms.FolderBrowserDialog())
            {

                openFolderdialog.SelectedPath = path;
                System.Windows.Forms.DialogResult result = openFolderdialog.ShowDialog();
                FileFolderPath = openFolderdialog.SelectedPath;
            }
        }

     

        private string[] CheckHref(string[] FilePath)
        {
            string[] checkedHref = new string[FilePath.Length];

            for(int d = 0; d < FilePath.Length; d++)
            {
                string originalHref = FilePath[d];
                string replace = originalHref.Replace(loadingName , "");
                string replaceM = replacePath(replace);

                checkedHref[d] = replaceM;
            }

            return checkedHref;
        }

        private string replacePath(string href)
        {
            return href.Replace("Выбрать папку", "");
        }
    }
}
