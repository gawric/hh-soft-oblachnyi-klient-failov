﻿using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.move
{
    public  class MoveFolderFileSystem
    {
        public event EventHandler<OnlyCloudDeleteFileSystem> eventMoveFileSystem;
        
        public bool move(string sourcesFilePath, string destinationFilePath)
        {
            bool check = false;

            if (isFolder(sourcesFilePath))
            {
                if (Directory.Exists(sourcesFilePath))
                {
                    Directory.Move(sourcesFilePath, destinationFilePath);
                    check = true;
                }
               
                //notifyOnlyCloud(listFilesRowId);
            }
            else
            {
                if (File.Exists(sourcesFilePath))
                {
                    File.Move(sourcesFilePath, destinationFilePath);
                    check = true;
                }
                
                //notifyOnlyCloud(listFilesRowId);
            }

            return check;
        }
        private bool isFolder(string sourcesFilePath)
        {
            if (File.Exists(sourcesFilePath))
            {
                return false;
            }
            else if (Directory.Exists(sourcesFilePath))
            {
                return true;
            }
            else
            {
                Console.WriteLine("MoveFolderFileSystem->isFolder: не смогли понять мы перемещаем Папку или Файл!!!!");
                return false;
                
            }
        }
      
        protected virtual void OnThresholdReached(OnlyCloudDeleteFileSystem e)
        {
            eventMoveFileSystem?.Invoke(this, e);
        }
    }
}
