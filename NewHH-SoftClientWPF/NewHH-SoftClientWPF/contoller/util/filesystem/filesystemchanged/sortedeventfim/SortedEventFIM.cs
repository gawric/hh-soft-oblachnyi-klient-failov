﻿using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged.sortedeventfim
{
    public class SortedEventFIM : ISortedEventFIM
    {
        private Dictionary<string, long>  allHref;
        private string rootWebDavLocation;
        private string rootLocalDirecory;
        public SortedEventFIM()
        {
            this.rootWebDavLocation = UtilMethod.getRootLocationAutoSaveFolder() + "/";
        }

        public void SetAllHref(Dictionary<string, long> allHref)
        {
            this.allHref = allHref;
        }
        public void SetRooTLocalDirecroty(ref string rootLocalDirectory)
        {
            this.rootLocalDirecory = rootLocalDirectory;
        }
        public List<System.IO.DirectoryInfo>  CalcEventFolderFIM(List<System.IO.DirectoryInfo> lFolder)
        {
            return CompareLocationFolder(lFolder);
     
        }

        public List<System.IO.FileInfo>  CalcEventFilesFIM(List<System.IO.FileInfo> lFiles)
        {
            return CompareLocationFiles(lFiles);
         
        }

        private List<System.IO.DirectoryInfo> CompareLocationFolder(List<System.IO.DirectoryInfo> lFolder)
        {
            List<System.IO.DirectoryInfo> tempList = new List<System.IO.DirectoryInfo>();

            foreach (DirectoryInfo di in lFolder)
            {
                HrefModel hModel = UtilMethod.GetFileInfoParent(di.FullName);

                string cLocation  = UtilMethod.ConvertHrefToLocation(hModel.href, rootLocalDirecory, rootWebDavLocation);
                if(!IsExistSql(cLocation))
                {
                    tempList.Add(di);
                }
            }

            return tempList;
        }

        private List<System.IO.FileInfo> CompareLocationFiles(List<System.IO.FileInfo> lFiles)
        {
            List<System.IO.FileInfo> tempList = new List<System.IO.FileInfo>();

            foreach (FileInfo di in lFiles)
            {
                HrefModel hModel = UtilMethod.GetFileInfoParent(di.FullName);

                string cLocation = UtilMethod.ConvertHrefToLocation(hModel.href, rootLocalDirecory, rootWebDavLocation);
                if (!IsExistSql(cLocation))
                {
                    tempList.Add(di);
                }
            }

            return tempList;

        }

        private bool IsExistSql(string cLocation)
        {
            if(allHref.ContainsKey(cLocation))
            {
                Debug.Print("Нашли в справочнике "+ cLocation);
                return true;
            }
            else
            {
                Debug.Print("Не нашли в справочнике "+ cLocation);
                return false;
            }
        }
    }
}
