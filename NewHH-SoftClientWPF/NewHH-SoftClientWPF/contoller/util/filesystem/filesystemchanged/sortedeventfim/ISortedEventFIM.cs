﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged.sortedeventfim
{
    public interface ISortedEventFIM
    {

        List<System.IO.DirectoryInfo> CalcEventFolderFIM(List<System.IO.DirectoryInfo> lFolder);
        List<System.IO.FileInfo> CalcEventFilesFIM(List<System.IO.FileInfo> lFiles);
        void SetRooTLocalDirecroty(ref string rootLocalDirectory);
        void SetAllHref(Dictionary<string, long> allHref);
    }
}
