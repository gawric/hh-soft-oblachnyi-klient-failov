﻿using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged.support
{
    public abstract class AbstractFsChanged: AbstractFsWSupport
    {
        public SqliteController sql;

        public AbstractFsChanged(SqliteController sql)
        {
            this.sql = sql;
        }

        protected string GetRootPath(string href, List<ScanFolderModel> listRootPath)
        {


            foreach (ScanFolderModel item in listRootPath)
            {

                if (href.IndexOf(item.fullPath) > -1)
                {
                    return item.fullPath;
                }
            }

            throw new InvalidOperationException("SubscribeData->GetRootPath: не смогли найти рут папку");
        }
        public List<long[][]> GetAllChildren(string location)
        {
            return new ScannerSqlliteRecursiveAllChildren().getScanAllChildren(createChildrenModel(new List<long[][]>(), location, sql));
        }
        public HrefModel GetHrefModel(string path)
        {
           return GetHModel(path , GetFolderScan());
        }
      
        private List<ScanFolderModel> GetFolderScan()
        {
            return sql.getSelect().getAllScanFolder();
        }

        private ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(List<long[][]> allListFull, string location, SqliteController sql)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();

            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = sql;
            model.progress = new Progress<CopyViewModel>();
            model.progressLabel = "search children";
            //все записи из базы
            model.allParent = sql.getSelect().getAllParentListRowId();
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();

            return model;
        }



    }
}
