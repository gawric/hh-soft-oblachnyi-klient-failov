﻿using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged.sortedeventfim;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged.support;
using NewHH_SoftClientWPF.contoller.util.scanner;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged
{
    public class FSChangedS : AbstractFsChanged, IFSChanged
    {
        private ISRIF srif;
        private ICreateFolder createFolder;
        private StackOperationUpDown soud;
        private bool isRun = false;
        private ISortedEventFIM sefim;
        public FSChangedS(SqliteController sql, ICreateFolder createFolder , StackOperationUpDown soud)
            : base(sql)
        {
            this.createFolder = createFolder;
            srif = new ScannerRecursiveInfoFiles();
            this.sql = sql;
            this.soud = soud;
            this.sefim = new SortedEventFIM();

        }

        public void Start(StackFSModel[] arr)
        {
            if(!isRun)
            {
                isRun = true;
                List<HrefModel> arrHref = FsToHref(arr);
                ScanFolder(arrHref);
                isRun = false;

                Debug.Print("");
            }
           
        }

        private List<HrefModel> FsToHref(StackFSModel[] arr)
        {
            List<HrefModel> listHref = new List<HrefModel>();
            foreach (StackFSModel item in arr) { listHref.Add(GetHrefModel(item.fullPath)); }

            return listHref;
        }

        private void ScanFolder(List<HrefModel> arrHref)
        {
            if (arrHref.Count == 0) return;
            srif.StartScan(arrHref);
            List<System.IO.FileInfo> lFilesTemp = srif.GetOnlyFiles();
            List<System.IO.DirectoryInfo> lFolderTemp = srif.GetOnlyFolder();

            if (lFolderTemp.Count != 0 | lFilesTemp.Count != 0)
            {

                string rootHrefParent = GetRootHrefCheck(arrHref, lFilesTemp);

                sefim.SetRooTLocalDirecroty(ref rootHrefParent);
                sefim.SetAllHref(GetAllHref());
                List<System.IO.FileInfo> lFiles = CalcEventFIMFiles(lFilesTemp);
                List<System.IO.DirectoryInfo> lFolder = CalcEventFIMFolder(lFolderTemp);

                Dictionary<string, FileInfoModel> cFolder = createFolder.CreateFolderListTest(lFolder, rootHrefParent, UtilMethod.getRootLocationAutoSaveFolder() + "/");
                List<Dictionary<string, List<FileInfo>>> dFiles = SortedPartiesFiles(lFiles);

                Debug.Print("Получили после сканирования файлов " + lFiles.Count);
                Debug.Print("Получили после сканирования папок " + lFolder.Count);

                UploadFiles(dFiles, cFolder, rootHrefParent);

            }

          
        }

        private string GetRootHrefCheck(List<HrefModel> arrHref, List<System.IO.FileInfo> lFilesTemp)
        {
            //если папок нет на создание
            //но нам нужно, как то получить корневую папку изменений.
            if (arrHref.Count == 0)
            {
                //UtilMethod.GetFileInfoParent(rootHref).parentHref;
                return GetRootHrefToEmptyList(lFilesTemp);
            }
            else
            {
                return  GetRootHref(new DirectoryInfo(arrHref[0].href));
            }
                
        }

        private string GetRootHrefToEmptyList(List<System.IO.FileInfo> lFilesTemp)
        {
            var min = lFilesTemp.Min(r => r.FullName.Length);
            return null;
        }
        private Dictionary<string, long> GetAllHref()
        {
            return sql.getSelect().getAllHref(new Dictionary<long, string>());
        }

        //Вычисляем CREATE/DELETE/RENAME
        private List<System.IO.FileInfo> CalcEventFIMFiles(List<System.IO.FileInfo> lFiles )
        {
            return sefim.CalcEventFilesFIM(lFiles);
        }

        private List<System.IO.DirectoryInfo> CalcEventFIMFolder( List<System.IO.DirectoryInfo> lFolder)
        {
            return sefim.CalcEventFolderFIM(lFolder);

        }

       

        private string GetRootHref(System.IO.DirectoryInfo lfolder)
        {
            string rootHref = GetRootPath(lfolder.FullName, sql.getSelect().getAllScanFolder());
            string rootHrefParent = UtilMethod.GetFileInfoParent(rootHref).parentHref;

            return rootHrefParent;
        }
        private void UploadFiles(List<Dictionary<string, List<FileInfo>>> lFiles , Dictionary<string, FileInfoModel> cFolder , string rootHrefParent)
        {
            IterFiles(lFiles, cFolder , rootHrefParent);
        }

        private void IterFiles(List<Dictionary<string, List<FileInfo>>> lFiles, Dictionary<string, FileInfoModel> cFolder , string rootHrefParent)
        {
           
            foreach(Dictionary<string, List<FileInfo>> dictItem in lFiles)
            {
                IterItem(dictItem , cFolder , rootHrefParent);
            }
         
        }

        private void IterItem(Dictionary<string, List<FileInfo>> dictItem , Dictionary<string, FileInfoModel> cFolder, string rootHrefParent)
        {
            foreach (KeyValuePair<string, List<FileInfo>> kvp in dictItem)
            {
                string keyItem = kvp.Key + "\\";

                if (cFolder.ContainsKey(keyItem))
                {
                    List<FileInfo> tempList = kvp.Value;
                    FileInfoModel fim = cFolder[keyItem];
                    ListUpload(tempList, fim);
                }
                else
                {
                    HrefModel hModel = UtilMethod.GetFileInfoParent(kvp.Key);
                    string cLocation = UtilMethod.ConvertHrefToLocation(hModel.href, rootHrefParent, UtilMethod.getRootLocationAutoSaveFolder()+"/");
                    FileInfoModel fim = sql.getSelect().getSearchNodesByLocation(cLocation);

                    if(fim != null)
                    {
                        List<FileInfo> tempList = kvp.Value;
                        Debug.Print(" FsChangesS->IterItem->Не критическая ошибка! Не смогли найти PasteFolder в темпе пробуем найти в базе:  " + kvp.Key + "\\");
                        ListUpload(tempList, fim);
                    }
                    else
                    {
                        Debug.Print(" FsChangesS->IterItem->Критическая ошибка Не смогли найти PasteFolder:  " + kvp.Key + "\\");
                    }
                   
                }
            }
        }

      
        private void ListUpload(List<FileInfo> tempList , FileInfoModel pasteFolder)
        {
            string[] allHref = ConvertFiToStr(tempList);
            soud.addStackUpload(allHref , pasteFolder);
        }

        private string[] ConvertFiToStr(List<FileInfo> tempList)
        {
            string[] temp = new string[tempList.Count];

            for(int i = 0; i < tempList.Count; i++)
            {
                temp[i] = tempList[i].FullName;
            }

            return temp;
        }
        private List<Dictionary<string, List<FileInfo>>> SortedPartiesFiles(List<System.IO.FileInfo> lFiles)
        {
            List<Dictionary<string, List<FileInfo>>> listResult = new List<Dictionary<string, List<FileInfo>>>();
            List<string> listTempParent = new List<string>();

            foreach (FileInfo item in lFiles)
            {
                if(!listTempParent.Any(x=>x.Equals(item.Directory.FullName)))
                {
                    List<FileInfo> tem = lFiles.Where(x => x.Directory.FullName.Equals(item.Directory.FullName)).ToList();
                    Dictionary<string, List<FileInfo>> dictTemp = AddDict(item, ref tem);
                    AddTemp(ref listResult, ref dictTemp, ref listTempParent, item);
                }


            }
            ClearTemp(listTempParent);

            return listResult;
        }

        private void AddTemp(ref List<Dictionary<string, List<FileInfo>>> listResult , ref Dictionary<string, List<FileInfo>> dictTemp , ref List<string> listTempParent , FileInfo item)
        {
            listResult.Add(dictTemp);
            listTempParent.Add(item.Directory.FullName);
        }

        private Dictionary<string, List<FileInfo>> AddDict(FileInfo item , ref List<FileInfo> tem)
        {
            Dictionary<string, List<FileInfo>> dictTemp = new Dictionary<string, List<FileInfo>>();
            dictTemp.Add(item.Directory.FullName, tem);

            return dictTemp;
        }
        private void ClearTemp(List<string> listTempParent)
        {
            listTempParent.Clear();
        }

       
    }
}
