﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support
{
    public abstract class AbstractFsWSupport
    {
        protected HrefModel GetHModel(string path , List<ScanFolderModel> listRootPath)
        {
            HrefModel hModel = UtilMethod.GetFileInfo(path);
            hModel.rootHref = GetRootPath(ref hModel , listRootPath);
            UtilMethod.GetFileInfoParent(hModel.pasteHref);
            return hModel;
        }

        protected HrefModel GetHModelNoRoot(string path)
        {
            return  UtilMethod.GetFileInfo(path);
        }

        protected string GetRootPath(ref HrefModel hModel , List<ScanFolderModel> listRootPath)
        {
            

            foreach (ScanFolderModel item in listRootPath)
            {

                if (hModel.href.IndexOf(item.fullPath) > -1)
                {
                    return GetParentRoot(item.fullPath);
                }
            }

            throw new InvalidOperationException("SubscribeData->GetRootPath: не смогли найти рут папку");
        }

        private string GetParentRoot(string path)
        {
            if (Directory.Exists(path))
            {
                DirectoryInfo di = new DirectoryInfo(path);
                return di.Parent.FullName;
            }

            throw new InvalidOperationException("SubscribeData->GetParentRoot: не смогли найти рут папку");
        }
    }
}
