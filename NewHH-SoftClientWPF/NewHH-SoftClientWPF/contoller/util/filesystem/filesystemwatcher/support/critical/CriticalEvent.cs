﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.critical
{
    public class CriticalEvent : AbstractFsWSupport , ICriticalEvent
    {
        private SqliteController sql;

        public CriticalEvent(SqliteController sql)
        {
            this.sql = sql;
        }

  
        public List<TimerFimModel> CriticalWorker(ref SortedSet<StackFSModel> listDelayed)
        {
            StackFSModel[] sArr = listDelayed.ToArray();
            List<TimerFimModel> lc = new List<TimerFimModel>();

            foreach(StackFSModel itemFs in sArr)
            {
                if(itemFs != null)
                {
                    HrefModel hCItem = UtilMethod.GetFileInfo(itemFs.fullPath);

                    if(staticVariable.Variable.isFolder(hCItem.type))
                    {
                        string parentHref = GetParentHref(hCItem);
                        string rootHref = GetRootPath(ref hCItem , sql.getSelect().getAllScanFolder());
                        FileInfoModel pfim = GetModelParent(ref parentHref, ref rootHref);

                        if (pfim != null)
                        {
                            lc.Add(ConvertTFM(parentHref, pfim));
                        }
                    }
                    else
                    {

                    }
                   
                    
                    //Debug.Print("");
                }
            }

            return lc;
        }


        private TimerFimModel ConvertTFM(string href, FileInfoModel item)
        {
            TimerFimModel tfm = new TimerFimModel();
            string[] pth = { href };
            tfm.listHref = pth;
            tfm.sourceFim = item;
            return tfm;
        }

        private string GetParentHref(HrefModel hModel)
        {
            if(!hModel.type.Equals("folder"))
            {
                FileInfo info = new FileInfo(hModel.href);
                return info.DirectoryName + "\\";
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(hModel.href);
                return di.Parent.FullName + "\\";
            }
        }

        private FileInfoModel GetModelParent(ref string parentHref, ref string rootHref)
        {
            FileInfoModel model = new FileInfoModel();
            HrefModel hPItem = UtilMethod.GetFileInfo(parentHref);
            model = setOther(hPItem, ref model);
            string location = UtilMethod.ConvertHrefToLocation(parentHref, rootHref, UtilMethod.getRootLocationAutoSaveFolder() + "/");

            if(sql.getSelect().getSearchNodesByLocation(location) == null)
            {
                model.location = location;
                model.row_id = staticVariable.Variable.generatedNewRowId(getLastRowId(sql), null);

                FileInfoModel parent = setParent(hPItem.pasteHref, ref model);

                if (parent == null) return null;
                model = setParent(hPItem.pasteHref, ref model);
                model.versionUpdateBases = getVersionBases(sql);
                model.user_id = getUserId(sql);

                return model;

            }

            return null;
        }

      

        private FileInfoModel setOther(HrefModel hModel, ref FileInfoModel model)
        {
            model.filename = hModel.filename;
            model.lastOpenDate = hModel.lastOpenDate;
            model.createDate = hModel.createDate;
            model.changeDate = hModel.changeDate;
            model.sizebyte = hModel.size;
            model.changeRows = "CREATE";
            model.attribute = ""; ;
            model.type = hModel.type;
            return model;
        }

        private FileInfoModel setParent(string pasteHref, ref FileInfoModel model)
        {
            HrefModel pModel = UtilMethod.GetFileInfoParent(pasteHref);
            pModel.rootHref = GetRootPath(ref pModel , sql.getSelect().getAllScanFolder());

            if (!pModel.rootHref.Equals(""))
            {
                string pasteLocation = UtilMethod.ConvertHrefToLocationFolder(pModel.href, pModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder());
                TempFilesModel parentFim = getFimSql(sql, pasteLocation);
                if (parentFim == null) return null;
                model.parent = parentFim.row_id;
            }


            return model;
        }

        private TempFilesModel getFimSql(SqliteController sql, string location)
        {

            if (sql != null) return sql.getSelect().getSearchByLocationToTempFilesModel(location);

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли location в нашей базе данных!!!");
        }

        private long getLastRowId(SqliteController sql)
        {
            long row_id = 0;
            if (sql != null) row_id = sql.getSelect().getLastRow_idFileInfoModel();

            return row_id;
        }

        private long getVersionBases(SqliteController sql)
        {

            if (sql != null) return sql.getSelect().getSqlLiteVersionDataBasesClient();

            return 0;
        }

        private long getUserId(SqliteController sql)
        {
            if (sql != null) return sql.getSelect().existUserIdToListFiles();
            return 0;
        }



    }
}
