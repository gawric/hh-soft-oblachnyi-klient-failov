﻿using NewHH_SoftClientWPF.contoller.sqlite;

using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.severmodel;

using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;


namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe
{
    public class SubscribeData : AbstractFsWSupport , ISubscribeData
    {
        private SqliteController sql;

        public SubscribeData(SqliteController sql)
        {
            this.sql = sql;
        }
        private HrefModel ConvertPathToFim(string path)
        {
            try
            {
                return GetHModel(path, GetFolderScan());
                
            }
            catch(InvalidOperationException s)
            {
                Debug.Print(s.ToString());
                return null;
            }
            
        }

     


        public FileInfoModel GetFimToEventChange(string path)
        {
            HrefModel hModel = GetHModel(path , GetFolderScan());
            return GetFimDatabases(ref hModel);
        }

       

        public FileInfoModel GetFimToEventCreate(string path)
        {
             HrefModel hModel = ConvertPathToFim(path);
             if (hModel.filename.Equals("")) return ElseEmptyHref(hModel);
             return GetModelFim(ref hModel);
        }

        public FileInfoModel GetFimToEventDelete(string path)
        {
            try
            {
                HrefModel hModel = UtilMethod.GetFileInfo(path);
                hModel.rootHref = GetRootPath(ref hModel , GetFolderScan());

                return ElseEmptyHref(hModel);
            }
            catch (InvalidOperationException s)
            {
                Debug.Print(s.ToString());
                return null;
            }
           
        }
      
        private List<ScanFolderModel> GetFolderScan()
        {
            return sql.getSelect().getAllScanFolder();
        }


        private FileInfoModel ElseEmptyHref(HrefModel href)
        {
            string location  = UtilMethod.ConvertHrefToLocation(new FileInfo(href.href).FullName, href.rootHref, UtilMethod.getRootLocationAutoSaveFolder()+"/");
            string webdavlocation = ChangeLocationToFolder(location, href.type);
            FileInfoModel fim =   getFimSqlAllField(sql, webdavlocation);
            return GetFimElseNull(ref fim, location);
        }

        private FileInfoModel GetFimElseNull(ref FileInfoModel fim , string location)
        {
            if(fim == null)
            {
                return getFimSqlAllField(sql, ChangeLocationToFolder(location, "folder"));
            }
           
                return fim;
            
        }


        private string ChangeLocationToFolder(string location , string type)
        {
            if(staticVariable.Variable.isFolder(type))
            {
                return location + "/";
            }

            return location;
        }
        private FileInfoModel setParent(string pasteHref , ref FileInfoModel model)
        {
            HrefModel pModel = UtilMethod.GetFileInfoParent(pasteHref);
            pModel.rootHref = GetRootPath(ref pModel , sql.getSelect().getAllScanFolder());

            if(!pModel.rootHref.Equals(""))
            {
                string pasteLocation = UtilMethod.ConvertHrefToLocationFolder(pModel.href, pModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder());
                TempFilesModel parentFim = getFimSql(sql, pasteLocation);
                if (parentFim == null)  return null;
                model.parent = parentFim.row_id;
            }
           

            return model;
        }
      
        private FileInfoModel setOther(HrefModel hModel, ref FileInfoModel model)
       {
            model.filename = hModel.filename;
            model.lastOpenDate = hModel.lastOpenDate;
            model.createDate = hModel.createDate;
            model.changeDate = hModel.changeDate;
            model.sizebyte = hModel.size;
            model.changeRows = "CREATE";
            model.attribute = ""; ;
            model.type = hModel.type;
            return model;
        }
     
        private long getLastRowId(SqliteController sql)
        {
            long row_id = 0;
            if (sql != null) row_id = sql.getSelect().getLastRow_idFileInfoModel();

            return row_id;
        }

        private long getVersionBases(SqliteController sql)
        {
           
            if (sql != null) return sql.getSelect().getSqlLiteVersionDataBasesClient();

            return 0;
        }

        private long getUserId(SqliteController sql)
        {
          if (sql != null) return sql.getSelect().existUserIdToListFiles();
          return 0;
        }

        private TempFilesModel getFimSql(SqliteController sql , string location)
        {
           
            if (sql != null)return sql.getSelect().getSearchByLocationToTempFilesModel(location);

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли location в нашей базе данных!!!");
        }

        private FileInfoModel getFimSqlAllField(SqliteController sql, string location)
        {

            if (sql != null) return sql.getSelect().getSearchNodesByLocation(location);

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли location в нашей базе данных!!!");
        }

        private FileInfoModel GetModelFim(ref HrefModel hModel)
        {
            FileInfoModel model = new FileInfoModel();
            model = setOther(hModel, ref model);
            string lcoation = UtilMethod.ConvertHrefToLocation(hModel.href, hModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder() + "/");
            model.location = ChangeLocationElseFolder(lcoation, hModel.type);
            model.row_id = staticVariable.Variable.generatedNewRowId(getLastRowId(sql), null);

            FileInfoModel parent = setParent(hModel.pasteHref, ref model);

            if (parent == null) return null;
            model = setParent(hModel.pasteHref, ref model);
            model.versionUpdateBases = getVersionBases(sql);
            model.user_id = getUserId(sql);

            return model;
        }

        private string ChangeLocationElseFolder(string location , string type)
        {
  
            if (staticVariable.Variable.isFolder(type))
            {
                //return location + "/";
                return location;
            }

            return location;
        }
        private FileInfoModel GetFimDatabases(ref HrefModel hModel)
        {
            string locationFiles = UtilMethod.ConvertHrefToLocation(hModel.href, hModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder() + "/");
            return getFimSqlAllField(sql, locationFiles);
        }

        public StackFSModel[] SortTsw(StackFSModel[] arr)
        {
            return null;
        }
    }
}
