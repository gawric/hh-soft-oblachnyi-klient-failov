﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe
{
    public interface ISubscribeData
    {
        FileInfoModel GetFimToEventChange(string path);
        FileInfoModel GetFimToEventCreate(string path);
        FileInfoModel GetFimToEventDelete(string path);

        StackFSModel[] SortTsw(StackFSModel[] arr);


    

    }
}
