﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe
{
    public class SubscribeData : ISubscribeData
    {
        private SqliteController sql;

        public SubscribeData(SqliteController sql)
        {
            this.sql = sql;
        }
        private FileInfoModel ConvertPathToFim(string path)
        {

            HrefModel hModel = GetHModel( path);
            if (hModel.filename.Equals("")) return ElseEmptyHref(hModel);

            return GetModelFim(ref hModel);
        }

        public FileInfoModel GetFimToEventChange(string path)
        {
            HrefModel hModel = GetHModel(path);
            return GetFimDatabases(ref hModel);
        }

        private HrefModel GetHModel(string path)
        {
            HrefModel hModel = GetFileInfo(path);
            hModel.rootHref = GetRootPath(ref hModel);
            return hModel;
        }

        public FileInfoModel GetFimToEventCreate(string path)
        {
            return ConvertPathToFim(path);
        }

        public FileInfoModel GetFimToEventDelete(string path)
        {
            HrefModel hModel = GetFileInfo(path);
            hModel.rootHref = GetRootPath(ref hModel);

            return ElseEmptyHref(hModel);
        }
        private string GetRootPath(ref HrefModel hModel)
        {
            List<ScanFolderModel> listRootPath = sql.getSelect().getAllScanFolder();
            foreach(ScanFolderModel item in listRootPath)
            {
                if(hModel.href.IndexOf(item.fullPath) > -1)
                {
                    return GetParentRoot(item.fullPath);
                }
            }

            return "";
        }

        private string GetParentRoot(string path)
        {
            if(Directory.Exists(path))
            {
                DirectoryInfo di = new DirectoryInfo(path);
                return di.Parent.FullName;
            }

            throw new InvalidOperationException("SubscribeData->GetparentRoot: не смогли найти рут папку");
        }
        private FileInfoModel ElseEmptyHref(HrefModel href)
        {
            FileInfo fi = new FileInfo(href.href);
            string webdavlocation  = ConvertHrefToLocation(fi.FullName, href.rootHref, UtilMethod.getRootLocationAutoSaveFolder()+"/");
            return  getFimSqlAllField(sql, webdavlocation);
        }
        private FileInfoModel setParent(string pasteHref , ref FileInfoModel model)
        {
            HrefModel pModel = GetFileInfoParent(pasteHref);
            pModel.rootHref = GetRootPath(ref pModel);
            string pasteLocation = ConvertHrefToLocationFolder(pModel.href, pModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder());
            model.parent = getFimSql(sql, pasteLocation).row_id;

            return model;
        }
      
        private FileInfoModel setOther(HrefModel hModel, ref FileInfoModel model)
       {
            model.filename = hModel.filename;
            model.lastOpenDate = hModel.lastOpenDate;
            model.createDate = hModel.createDate;
            model.changeDate = hModel.changeDate;
            model.sizebyte = hModel.size;
            model.changeRows = "CREATE";
            model.attribute = ""; ;
            model.type = hModel.type;
            return model;
        }
        private string ConvertHrefToLocation(string clientLocalDirectory , string clientRootLocalDirectory , string pasteRootWebDavDirectory)
        {
            //полный путь к папке изменяем все на unix систему
            string clearLocalHref = clientLocalDirectory.Replace("\\", "/");

            //папка откуда все копируется
            string clearRootHref = clientRootLocalDirectory.Replace("\\", "/");
            //находим последнее вхождение обычно это имя файла
            int indexSl = clearLocalHref.LastIndexOf("/") + 1;

            //находим главную папку где нажали кнопку копировать и меняем на рутовый путь сервера
            string newhref = clearLocalHref.Replace(clearRootHref, pasteRootWebDavDirectory);


            return staticVariable.Variable.EscapeUrlString(newhref);
        }

        private string ConvertHrefToLocationFolder(string clientLocalDirectory, string clientRootLocalDirectory, string pasteRootWebDavDirectory)
        {
            //полный путь к папке изменяем все на unix систему
            string clearLocalHref = clientLocalDirectory.Replace("\\", "/") + "/";

            //папка откуда все копируется
            string clearRootHref = clientRootLocalDirectory.Replace("\\", "/");
            //находим последнее вхождение обычно это имя файла
            int indexSl = clearLocalHref.LastIndexOf("/") + 1;


            //находим главную папку где нажали кнопку копировать и меняем на рутовый путь сервера
            string newhref = clearLocalHref.Replace(clearRootHref, pasteRootWebDavDirectory+"/");


            return staticVariable.Variable.EscapeUrlString(newhref);
        }


        public HrefModel GetFileInfo(string path)
        {
            if (File.Exists(path))
            {
                FileInfo fi1 = new FileInfo(path);
                HrefModel fileModel = new HrefModel();

                fileModel.changeDate = fi1.LastWriteTimeUtc.ToString();
                fileModel.lastOpenDate = fi1.LastAccessTime.ToString();
                fileModel.filename = fi1.Name;
                fileModel.href = fi1.FullName;
                fileModel.createDate = fi1.CreationTime.ToString();
                fileModel.parentHref = fi1.Directory.FullName;
                fileModel.pasteHref = fi1.Directory.FullName;
                fileModel.type = fi1.Extension;
                fileModel.size = fi1.Length;

                return fileModel;
            }
            else
            {
                if(Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    HrefModel folderModel = new HrefModel();

                    folderModel.changeDate = di.LastWriteTime.ToString();
                    folderModel.lastOpenDate = di.LastAccessTime.ToString();
                    folderModel.filename = di.Name;
                    folderModel.href = di.FullName;
                    folderModel.createDate = di.CreationTime.ToString();
                    folderModel.parentHref = di.Parent.FullName;
                    folderModel.pasteHref = di.Parent.FullName;
                    folderModel.type = "folder";

                 return folderModel;
                }
               
            }

            return GetEmptyModel(path);
        }

        private HrefModel GetEmptyModel(string href)
        {
            HrefModel hrefModel = new HrefModel();
            hrefModel.href = href;
            hrefModel.filename = "";

            return hrefModel;
        }
        public HrefModel GetFileInfoParent(string path)
        {

            if (File.Exists(path))
            {
                FileInfo fi1 = new FileInfo(path);
                HrefModel fileModel = new HrefModel();

                fileModel.changeDate = fi1.LastWriteTimeUtc.ToString();
                fileModel.lastOpenDate = fi1.LastAccessTime.ToString();
                fileModel.filename = fi1.Name;
                fileModel.href = fi1.FullName;
                fileModel.createDate = fi1.CreationTime.ToString();
                fileModel.parentHref = fi1.Directory.FullName;
                fileModel.pasteHref = fi1.Directory.FullName;
                fileModel.type = fi1.Extension;
                fileModel.size = fi1.Length;

                return fileModel;
            }
            else
            {
                if (Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    HrefModel folderModel = new HrefModel();

                    folderModel.changeDate = di.LastWriteTime.ToString();
                    folderModel.lastOpenDate = di.LastAccessTime.ToString();
                    folderModel.filename = di.Name;
                    folderModel.href = di.FullName;
                    folderModel.createDate = di.CreationTime.ToString();
                    folderModel.parentHref = di.Parent.FullName;
                    folderModel.pasteHref = di.Parent.FullName;
                    folderModel.type = "folder";

                    return folderModel;
                }

            }

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли на копьютере папку с намими файлами!!!");

        }

        private long getLastRowId(SqliteController sql)
        {
            long row_id = 0;
            if (sql != null) row_id = sql.getSelect().getLastRow_idFileInfoModel();

            return row_id;
        }

        private long getVersionBases(SqliteController sql)
        {
           
            if (sql != null) return sql.getSelect().getSqlLiteVersionDataBasesClient();

            return 0;
        }

        private long getUserId(SqliteController sql)
        {
          if (sql != null) return sql.getSelect().existUserIdToListFiles();
          return 0;
        }

        private TempFilesModel getFimSql(SqliteController sql , string location)
        {
           
            if (sql != null)return sql.getSelect().getSearchByLocationToTempFilesModel(location);

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли location в нашей базе данных!!!");
        }

        private FileInfoModel getFimSqlAllField(SqliteController sql, string location)
        {

            if (sql != null) return sql.getSelect().getSearchNodesByLocation(location);

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли location в нашей базе данных!!!");
        }

        private FileInfoModel GetModelFim(ref HrefModel hModel)
        {
            FileInfoModel model = new FileInfoModel();
            model = setOther(hModel, ref model);
            model.location = ConvertHrefToLocation(hModel.href, hModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder() + "/");
            model.row_id = staticVariable.Variable.generatedNewRowId(getLastRowId(sql), null);

            model = setParent(hModel.pasteHref, ref model);
            model.versionUpdateBases = getVersionBases(sql);
            model.user_id = getUserId(sql);

            return model;
        }

        private FileInfoModel GetFimDatabases(ref HrefModel hModel)
        {
            string locationFiles = ConvertHrefToLocation(hModel.href, hModel.rootHref, UtilMethod.getRootLocationAutoSaveFolder() + "/");
            return getFimSqlAllField(sql, locationFiles);
        }

    }
}
