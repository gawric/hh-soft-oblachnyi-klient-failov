﻿using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.comparable;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack.deffered;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.timernet;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe
{
    public class TimerSendData : SendDataWatcher, ITimeSendData
    {
        private System.Timers.Timer aTimer;
        private SortedSet<TimerFimModel> sList;
        private IDelayedLaunch dl;
        private SqliteController sql;
        private bool isWorking = false;
        private int countAllSend = 0;
        public TimerSendData(CreateFolderController cfc, StackOperationUpDown soud, IDelayedLaunch dl, SqliteController sql , FirstSyncFilesController fsfc)
        : base(cfc, soud , fsfc)
        {
            SetTimer();
            sList = new SortedSet<TimerFimModel>(new FIMComparableId());
            this.dl = dl;
            this.sql = sql;
            StartTimer();
        }


        public void SendData()
        {
            
            try
            {
                if (isWorking) return;
                var sArr = UnionSet(sList, dl.VerifiedList());

                if (sArr.Length > 0)
                {

                    SetWorking(true);

                    Debug.Print("TimerSendData->SendData: Id номера потока " + Thread.CurrentThread.ManagedThreadId);
                    Start(sArr).Wait();
                    countAllSend = countAllSend + GetCount(sArr);
                    Debug.Print("TimerSendData->SendData: кол-во данных отправленых на сервера: " + countAllSend);
                    ClearData(ref sArr, ref sList);

                    SetWorking(false);
                }

            }
            catch(System.AggregateException s)
            {
                Debug.Print("TimerSendData->SendData: критическая ошибка " + s.ToString());
            }
            

           
            
        }

     
        private int GetCount(TimerFimModel[] arr)
        {
            int fs2 = 0;
            foreach(TimerFimModel tfm in arr)
            {
                if(tfm != null)
                {
 
                    staticVariable.UtilMethod.WriteLog("D:\\XiaoMi\\log\\send.txt", "" + tfm.sourceFim.location + "   "+ fs2++);
                }
            }

            return fs2;
        }

        private void SetWorking(bool isWorking)
        {
            this.isWorking = isWorking;
        }

        private TimerFimModel[] UnionSet(SortedSet<TimerFimModel>  sList , TimerFimModel[] verifiedList)
        {
            try
            {
                TimerFimModel[] sArrNow = ConvertSetToArr(sList);
                //TimerFimModel[] sArr2Delayed = ConvertSetToArr(VerifiedList);
                TimerFimModel[] finalArr = sArrNow.Union(verifiedList).ToArray();
                //Array.Sort(finalArr, new TFMComparable());
                //Array.Reverse(finalArr);
                return finalArr;
            }
            catch(System.ArgumentException s)
            {
                TimerFimModel[] sArrNow = ConvertSetToArr(sList);
                return  sArrNow.Union(verifiedList).ToArray();
            }
          
        }
        private async Task Start(TimerFimModel[] sArr)
        {
            TimerFimModel[] sUArr = UpdatePasteFim(ref sArr);
            await CreateFolder(sUArr);
            UploadFiles(sUArr);
            Delete(sUArr);
        }

        private TimerFimModel[] UpdatePasteFim(ref TimerFimModel[] sArr)
        {
            foreach (TimerFimModel tfm in sArr)
            {
                FileInfoModel fim = GetFimPaste(sql, tfm.sourceFim.parent);
                tfm.pasteFim = fim;
            }

            return sArr;
           
        }
        private FileInfoModel GetFimPaste(SqliteController sql , long rowid)
        {
            return sql.getSelect().getSearchNodesByRow_IdFileInfoModel(rowid);
        }
       
        private TimerFimModel[] ConvertSetToArr(SortedSet<TimerFimModel> sList)
        {
            if(sList.Count > 0) return sList.ToArray();

            return new TimerFimModel[0] ;
        }

        public void InsertItemArr(TimerFimModel tFm)
        {
            sList.Add(tFm);
        }

        private  void SetTimer()
        {
            try
            {
                aTimer = new System.Timers.Timer(10000);
                aTimer.Elapsed += (sender, e) => SendData();
            }
            catch(System.InvalidOperationException s)
            {
                Debug.Print("Ошибка номер 2 " + s.ToString()); ;
            }
  
            
        }
        private void StartTimer()
        {
            aTimer.Start();
        }

        private void ClearData(ref TimerFimModel[] sArr, ref SortedSet<TimerFimModel> sList)
        {
            sList.Clear();
            sArr = null;
           // dl.ClearVerifiedList();
            Debug.Print("TimerSendData->SendData:  массив был очищен: " + sList.Count);
        }


    }
}
