﻿using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemchanged;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.comparable;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack.deffered
{
    public class DelayedLaunch : StackEvent, IDelayedLaunch
    {
        private Dictionary<int, StackFSModel> listDelayed;
        private SortedSet<TimerFimModel> verifiedList;
        private List<StackFSModel> removeList;
        private bool isClearArr = false;
        private volatile bool isRunThread;
        private System.Timers.Timer delayTimer;
        private SqliteController sql;
        private FSChangedS s;


        public DelayedLaunch(ISubscribeData subData , SqliteController sql , ICreateFolder createFolder , StackOperationUpDown soud)
            : base(subData)
        {
            this.sql = sql;
            this.listDelayed = new Dictionary<int, StackFSModel>(); 
            this.verifiedList = new SortedSet<TimerFimModel>(new FIMComparableId());
            this.removeList = new List<StackFSModel>();

            SetTimer();
            StartTimer();
            s = new FSChangedS(sql , createFolder , soud);
            staticVariable.UtilMethod.dict = new Dictionary<string, mvvm.model.autoSaveFolder.HrefModel>();
        }

        public void AddDelay(StackFSModel item)
        {
            SafeThreadAddDelay(item);
        }

        public TimerFimModel[] VerifiedList()
        {
            TimerFimModel[] arr = verifiedList.ToArray();
            ClearVerifiedList();
            return arr;
        }
        private void SafeThreadAddDelay(StackFSModel item)
        {
            AddSave(item);
        }

        private void AddSave(StackFSModel item)
        {
            if (item != null)
            {
                int charArr = item.fullPath.GetHashCode();

                if (!listDelayed.ContainsKey(charArr))
                {
                    listDelayed.Add(charArr, item);
                }
                
            }     
        }

   

        private void SetTimer()
        {
            delayTimer = new System.Timers.Timer(10000);
            delayTimer.Elapsed += (sender, e) => DelayedStart(listDelayed);
        }

        private void StartTimer()
        {
            delayTimer.Start();
        }


        private void DelayedStart(Dictionary<int, StackFSModel> listDelayed)
        {
            Debug.Print("DelayedLaunch->DelayedStart: Размер массива на удержании " + listDelayed.Count() + "флаг fsrun " + UtilMethod.fsrun + "и ");
            if (isRunThread) return;
           
            try
            {
                isRunThread = true;

                if (!isClearArr)
                {
                    if (!UtilMethod.fsrun)
                    {
                        try
                        {
                            StackFSModel[] arr = listDelayed.Select(z => z.Value).ToArray();
                            Debug.Print("До сортировки размер массива " + arr.Length);
                            s.Start(arr);
                            listDelayed.Clear();
                        }
                        catch(System.ArgumentOutOfRangeException f)
                        {
                            Debug.Print("DelayedLaunch->DelayedStart: Критическая ошибка " + f.ToString());
                        }
                      
                    }

                    isRunThread = false;
                }

                UtilMethod.fsrun = false;
            }
            catch(System.InvalidOperationException s)
            {
                Debug.Print("Ошибка " + s.ToString());
                isRunThread = false;
            }
            catch(System.ArgumentException s)
            {
                Debug.Print("Ошибка " + s.ToString());
                isRunThread = false;
            }

            Debug.Print("DelayedLaunch->DelayedStart: Изменяем флаг на false");
            
            
        }


        private void RemoveItem(ref Dictionary<int, StackFSModel> listDelayed)
        {
            if (removeList.Count > 0)
            {
                SetClear(true);

                Remove(ref  listDelayed);
                removeList.Clear();

                SetClear(false);
            }

           
        }

        
        private void Remove(ref Dictionary<int, StackFSModel> listDelayed)
        {
            try
            {
                
                int delcount = 0;
                foreach (StackFSModel item in removeList)
                {
                    int hashcode = item.fullPath.GetHashCode();

                    if (listDelayed.ContainsKey(hashcode))
                    {
                        listDelayed.Remove(hashcode);
                        staticVariable.UtilMethod.WriteLog("D:\\XiaoMi\\log\\remove.txt", item.fullPath + "  " + delcount++);
                    }
                }


            }
            catch(InvalidOperationException ex)
            {
                Debug.Print("DelayedLaunch->Remove критическая ошибка " + ex.ToString()); ;
            }
            

           
        }

        private void SetClear(bool isclear)
        {
            this.isClearArr = isclear;
        }
        private void AddRemoveList(StackFSModel item)
        {
                removeList.Add(item);
        }

     
        private void AddFimList(string href , FileInfoModel item)
        {
            verifiedList.Add(ConvertTFM( href,  item));
            
        }

        private TimerFimModel ConvertTFM(string href, FileInfoModel item)
        {
            TimerFimModel tfm = new TimerFimModel();
            string[] pth = { href };
            tfm.listHref = pth;
            tfm.sourceFim = item;
            return tfm;
        }

        public void ClearVerifiedList()
        {
            verifiedList.Clear();
        }
    }
}
