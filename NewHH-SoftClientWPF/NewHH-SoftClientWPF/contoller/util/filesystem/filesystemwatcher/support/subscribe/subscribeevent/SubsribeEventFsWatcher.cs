﻿using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack.deffered;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher
{
    public class SubsribeEventFsWatcher: ISubScribleEvent
    {

        public event EventHandler<SubscribeDataModel> eventAddData;
        private IStackFSEvent stackEvent;


        public SubsribeEventFsWatcher(SqliteController sql , StackOperationUpDown soud , CreateFolderController cfc , FirstSyncFilesController fsfc , ICreateFolder createFolder)
        {
            ISubscribeData subData = new SubscribeData(sql);
            stackEvent = new StackFSEvent(subData , sql , createFolder , soud);
            stackEvent.StartStack();
        }
        public  void OnChanged(object sender, FileSystemEventArgs fse)
        {
            Debug.Print("Произошли изменения в папке " + fse.FullPath);
            stackEvent.AddEventData(CreateModel(fse.FullPath, "UPDATE", ""));
            UtilMethod.fsrun = true;
        }

 

        public void OnCreated(object sender, FileSystemEventArgs fse)
        {
            Debug.Print("Произошли изменения в папке " + fse.FullPath);
            stackEvent.AddEventData(CreateModel(fse.FullPath, "UPDATE", ""));
            UtilMethod.fsrun = true;
        }

        public void OnDeleted(object sender, FileSystemEventArgs fse)
        {
            //Console.WriteLine($"Deleted Event: {fse.FullName}");
            //stackEvent.AddEventData(CreateModel(fse.FullPath, "DELETE", ""));
        }
           

        public void OnRenamed(object sender, RenamedEventArgs fse)
        {
           // Console.WriteLine($"Renamed:");
           // Console.WriteLine($"    Old: {rfse.PreviousFullName}");
           // Console.WriteLine($"    Rename Event: {rfse.FullName}");

            //stackEvent.AddEventData(CreateModel(fse.FullPath, "RENAME", fse.OldFullPath));

        }

        public void OnError(object sender, ErrorEventArgs e)
        {
            PrintException(e.GetException());
        }
            

        public  void PrintException(Exception ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine();
                PrintException(ex.InnerException);
            }
        }




        public void AddEvent(SortedSet<FileInfoModel> sArr)
        {
            SubscribeDataModel mArgs = new SubscribeDataModel();
            OnThresholdReached(mArgs);
            
        }

        private StackFSModel CreateModel(string fullPath , string eventFs , string oldFullpath)
        {
            StackFSModel model = new StackFSModel();
            model.fullPath = fullPath;
            model.oldPath = oldFullpath;
            model.eventFS = eventFs;

            return model;
        }
        protected virtual void OnThresholdReached(SubscribeDataModel e)
        {
            eventAddData?.Invoke(this, e);
        }

       
    }
}
