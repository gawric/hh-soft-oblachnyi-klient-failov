﻿using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.comparable
{
    public class TFMComparable : IComparer<TimerFimModel>
    {
        public int Compare(TimerFimModel x, TimerFimModel y)
        {
            if (staticVariable.Variable.isFolder(x.sourceFim.type) & staticVariable.Variable.isFolder(y.sourceFim.type)) return 0;

            if(staticVariable.Variable.isFolder(x.sourceFim.type))
            {
                if(staticVariable.Variable.isFolder(x.sourceFim.type) & staticVariable.Variable.isFolder(y.sourceFim.type))
                {
                    return 0;
                }
                return 1;
            }
            else
            {
                return -1;
            }

            
        }
    }
}
