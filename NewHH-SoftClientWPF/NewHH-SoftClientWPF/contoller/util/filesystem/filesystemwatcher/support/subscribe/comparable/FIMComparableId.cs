﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.comparable
{
    public class FIMComparableId : IComparer<TimerFimModel>
    {
        public int Compare(TimerFimModel x, TimerFimModel y)
        {
            var x_byte = x.sourceFim.location.ToCharArray();
            var y_byte = y.sourceFim.location.ToCharArray();

            if (x.sourceFim.row_id == y.sourceFim.row_id || x_byte.SequenceEqual(y_byte))
            {
                //var x_byte_change = x.sourceFim.changeRows.ToCharArray();
                //var y_byte_change = y.sourceFim.changeRows.ToCharArray();

                //if (x_byte_change.SequenceEqual(y_byte_change))
                //{
                    return 0;
               // }
            }

            if (x.sourceFim.row_id > y.sourceFim.row_id) return 1;
            if (x.sourceFim.row_id < y.sourceFim.row_id) return -1;

            return 0;
        }
    }
}
