﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher
{
    public class SubsribeEventFsWatcher: ISubScribleEvent
    {

        public event EventHandler<SubscribeDataModel> eventAddData;
        private TimerSendData timer;
        private ISubscribeData subData;
        public SubsribeEventFsWatcher(SqliteController sql)
        {
            timer = new TimerSendData(this);
            subData = new SubscribeData(sql);
        }
        public  void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
            {
                return;
            }
            FileInfoModel fim = subData.GetFimToEventChange(e.FullPath);
            fim.changeRows = "UPDATE";
            timer.InsertItemArr(fim);

            Console.WriteLine($"Changed: {e.FullPath}");
        }

        public  void OnCreated(object sender, FileSystemEventArgs e)
        {
            string value = $"Created: {e.FullPath}";
            Console.WriteLine(value);
            FileInfoModel fim = subData.GetFimToEventCreate(e.FullPath);
            fim.changeRows = "CREATE";
            timer.InsertItemArr(fim);
        }

        public  void OnDeleted(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"Deleted: {e.FullPath}");
            FileInfoModel fim = subData.GetFimToEventDelete(e.FullPath);
            fim.changeRows = "DELETE";
            timer.InsertItemArr(fim);
        }
           

        public  void OnRenamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine($"Renamed:");
            Console.WriteLine($"    Old: {e.OldFullPath}");
            Console.WriteLine($"    New: {e.FullPath}");

            FileInfoModel oldFim = subData.GetFimToEventDelete(e.OldFullPath);
            oldFim.changeRows = "DELETE";
            timer.InsertItemArr(oldFim);

            FileInfoModel newFim = subData.GetFimToEventCreate(e.FullPath);
            newFim.changeRows = "CREATE";
            timer.InsertItemArr(newFim);
        }

        public  void OnError(object sender, ErrorEventArgs e)
        {
            PrintException(e.GetException());
        }
            

        public  void PrintException(Exception ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine();
                PrintException(ex.InnerException);
            }
        }




        public void AddEvent(SortedSet<FileInfoModel> sArr)
        {
            SubscribeDataModel mArgs = new SubscribeDataModel();
            OnThresholdReached(mArgs);
            
        }

        protected virtual void OnThresholdReached(SubscribeDataModel e)
        {
            eventAddData?.Invoke(this, e);
        }
    }
}
