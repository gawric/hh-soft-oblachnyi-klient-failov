﻿using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.subscribe.stack
{
    interface IStackFSEvent 
    {        
       void AddEventData(StackFSModel model);
       void StartStack();
    }
}
