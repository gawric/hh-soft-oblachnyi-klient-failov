﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.sortList
{
    public class SortTreeHref : AbstractFsWSupport, ISortTreeHref
    {
        private SqliteController sql;
        private Dictionary<string, List<StackFSModel>> dictHref;
        public SortTreeHref(SqliteController sql)
        {
            this.sql = sql;
            dictHref = new Dictionary<string, List<StackFSModel>>();
        }
        public StackFSModel[] Sort(StackFSModel[] arr)
        {
            if(IsFolderScan(arr))
            {
                return GetOnlyFolder(arr);
            }
            else
            {
                Debug.Print(" SortTreeHref-> Папок в массиве DelayLaunche не найденно возвращаем только файлы ");
                return GetOnlyFiles(arr);
            }
        }

        private StackFSModel[] GetOnlyFolder(StackFSModel[] arr)
        {
            List<StackFSModel> tempList = new List<StackFSModel>();

            foreach (StackFSModel itemFs in arr)
            {
                HrefModel hrefModel = GetHref(itemFs);

                if (staticVariable.Variable.isFolder(hrefModel.type))
                {
                    tempList.Add(itemFs);
                }
            }

            return tempList.ToArray();
        }

        private StackFSModel[] GetOnlyFiles(StackFSModel[] arr)
        {
            List<StackFSModel> tempList = new List<StackFSModel>();

            foreach (StackFSModel itemFs in arr)
            {
                HrefModel hrefModel = GetHref(itemFs);

                if (!staticVariable.Variable.isFolder(hrefModel.type))
                {
                    tempList.Add(itemFs);
                }
            }

            return tempList.ToArray();
        }
        private bool IsFolderScan(StackFSModel[] arr)
        {
            foreach(StackFSModel item in arr)
            {
                HrefModel hrefModel = GetHref(item);

                if(staticVariable.Variable.isFolder(hrefModel.type))
                {
                    return true;
                }
            }

            return false;
        }
       
        private HrefModel GetHref(StackFSModel itemFs)
        {
            if(staticVariable.UtilMethod.dict.ContainsKey(itemFs.fullPath))
            {
                return staticVariable.UtilMethod.dict[itemFs.fullPath];
            }
            else
            {
                HrefModel hModel = GetHModelNoRoot(itemFs.fullPath);
                staticVariable.UtilMethod.dict.Add(itemFs.fullPath, hModel);
                return GetHModelNoRoot(itemFs.fullPath);
            }
           
        }
      


      

       

  
       
    }
}
