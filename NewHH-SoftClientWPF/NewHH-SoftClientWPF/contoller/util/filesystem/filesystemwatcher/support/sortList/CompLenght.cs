﻿using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.filesystemwatchmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support.sortList
{
    public class CompLenght : IComparer<StackFSModel>
    {
        public int Compare(StackFSModel x, StackFSModel y)
        {
            if (x.fullPath.Length > y.fullPath.Length) return 1;

            if (x.fullPath.Length < y.fullPath.Length) return -1;

            if (x.fullPath.Length == y.fullPath.Length) return 0;

            return -1;
        }
    }
}
