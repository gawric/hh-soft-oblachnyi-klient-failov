﻿
using NewHH_SoftClientWPF.contoller.cancellation;

using NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher.support;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.filesystemwatcher
{
    public class FSWatcher: IFSWatcher
    {
        private ISubScribleEvent sefsw;
        private CanselWatch cansel;
        private bool isRun = false;
        public FSWatcher(CancellationController cc , ISubScribleEvent sefsw)
        {
            this.sefsw = sefsw;
            cansel = new CanselWatch(cc);
            cansel.CreateCanselToken();
        }

        public void StartCanselWS()
        {
            cansel.StartCansel();
        }

        public bool IsRun()
        {
            return isRun;
        }
        public void StartRunningWatch(string watchfs)
        {
            Task.Run(() =>
            {
                try
                {
                     var watcher = new FileSystemWatcher(watchfs);

                    watcher.NotifyFilter = NotifyFilters.Attributes
                     | NotifyFilters.CreationTime
                     | NotifyFilters.DirectoryName
                     | NotifyFilters.FileName
                     | NotifyFilters.LastAccess
                     | NotifyFilters.LastWrite
                     | NotifyFilters.Security
                     | NotifyFilters.Size;

                    watcher.Changed += sefsw.OnChanged;
                    watcher.Created += sefsw.OnCreated;
                    watcher.Deleted += sefsw.OnDeleted;
                    watcher.Renamed += sefsw.OnRenamed;
                    watcher.Error += sefsw.OnError;

                    watcher.InternalBufferSize = 94192;
                    watcher.Filter = "*.*";
                    watcher.IncludeSubdirectories = true;
                    watcher.EnableRaisingEvents = true;


 
           

                      while (true)
                      {
                        isRun = true;
                        Thread.Sleep(500);

                        if (cansel.IsCansel())
                        {
                            isRun = false;
                            Debug.Print("FSWatcher->StartRunningWatch: Предупреждение *.* Отключаем слежение за папками Путь: " + watchfs);
                            break;
                        }
                      }


                    watcher.Dispose();

                    Debug.Print("FSWatcher->StartRunningWatch: Остановили бесконечный цикл " + watchfs);

                }
                catch(System.InvalidOperationException s)
                {
                    Debug.Print("ошибка 23" + s.ToString());
                }
               
            });


            
        }


  


    }
}
