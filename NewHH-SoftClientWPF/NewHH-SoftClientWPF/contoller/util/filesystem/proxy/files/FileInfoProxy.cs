﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.proxy
{
    public class FileInfoProxy: IFileInfoProxy
    {
        FileInfo fi;
        public FileInfoProxy(string path)
        {
            fi = new FileInfo(path);
        }

        public DateTime CreationTime()
        {
            return fi.CreationTime;
        }

        public DateTime CreationTimeUtc()
        {
            return fi.CreationTimeUtc;
        }

        public bool Exists()
        {
            return fi.Exists;
        }

        public string FullName()
        {
            return fi.FullName;
        }

        public DateTime LastAccessTime()
        {
            return fi.LastAccessTime;
        }

        public DateTime LastAccessTimeUtc()
        {
            return fi.LastAccessTimeUtc;
        }

        public DateTime LastWriteTime()
        {
            return fi.LastAccessTime;
        }

        public DateTime LastWriteTimeUtc()
        {
            return fi.LastWriteTimeUtc;
        }

        public long Length()
        {
            return fi.Length;
        }

        public string Name()
        {
            return fi.Name;
        }
    }
}
