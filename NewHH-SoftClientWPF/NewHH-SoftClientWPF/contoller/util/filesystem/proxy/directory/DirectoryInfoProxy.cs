﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.filesystem.proxy
{
    public class DirectoryInfoProxy: IDirectoryInfoProxy
    {
        DirectoryInfo di;
        public DirectoryInfoProxy(string path)
        {
            di = new DirectoryInfo(path);
        }

        public DateTime CreationTime()
        {
            return di.CreationTime;
        }

        public DateTime CreationTimeUtc()
        {
            return di.CreationTimeUtc;
        }

        public bool Exists()
        {
            return di.Exists;
        }

        public string FullName()
        {
            return di.FullName;
        }

        public DirectoryInfo[] GetDirectories()
        {
            return di.GetDirectories();
        }

        public IFileInfoProxy[] GetFiles()
        {
            return FileInfoConvert(di.GetFiles());
        }

        public DateTime LastAccessTime()
        {
            return di.LastAccessTime;
        }

        public DateTime LastAccessTimeUtc()
        {
            return di.LastAccessTimeUtc;
        }

        public DateTime LastWriteTime()
        {
            return di.LastAccessTime;
        }

        public DateTime LastWriteTimeUtc()
        {
            return di.LastWriteTimeUtc;
        }

        public string Name()
        {
            return di.Name;
        }

        private IFileInfoProxy[] FileInfoConvert(FileInfo[] arr)
        {
            IFileInfoProxy[] fipArr;

            if(arr != null)
            {
                fipArr = new FileInfoProxy[arr.Length];
                
                for(int i = 0; i < arr.Length; i++)
                {
                    IFileInfoProxy fip = new FileInfoProxy(arr[i].FullName);
                    fipArr[i] = fip;
                }
            }
            else
            {
                fipArr = new FileInfoProxy[1];
            }

            return fipArr;
        }
    }
}
