﻿using NewHH_SoftClientWPF.mvvm.model.infoSystemModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.infosystem
{
    public class InfoSystem
    {
        private InfoSystemModel infoModel;
        
        public InfoSystem()
        {
            infoModel = new InfoSystemModel();
            ShowSystemInfo();
            ShowNetworkInterfaces();
        }

        public InfoSystemModel getInfoSystem()
        {
            return infoModel;
        }
        private  void ShowSystemInfo()
        {
            infoModel.osVerison = Environment.OSVersion.ToString();
            infoModel.osUserName = Environment.UserName;
        }

        private  void ShowNetworkInterfaces()
        {
 
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();


            if (nics == null || nics.Length < 1)
            {
                Console.WriteLine("  No network interfaces found.");
                return;
            }
      
       
            try
            {
                for (int i = 0; i < nics.Length; i++)
                {
                    OperationalStatus isUp = nics[i].OperationalStatus;

                    if (isUp.HasFlag(OperationalStatus.Up))
                    {
                        IPInterfaceProperties properties = nics[i].GetIPProperties(); //  .GetIPInterfaceProperties();
                        NetworkInterfaceType type = nics[i].NetworkInterfaceType;

                        if (!type.HasFlag(NetworkInterfaceType.Loopback))
                        {
                         
                            PhysicalAddress address = nics[i].GetPhysicalAddress();
                            IPAddress ipAdress = getIPAddress();


                            infoModel.macNetwork = address.ToString();
                            infoModel.ipLocal = ipAdress.MapToIPv4().ToString();
                           
                        }

                        
                    }
                }
            }
            catch(System.ArgumentException z)
            {
                Console.WriteLine("infoSystem:->ShowNetworkInterfaces: не критическая ошибка "+ z);
            }
           
          
        }

        private IPAddress getIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }

        
    }
}
