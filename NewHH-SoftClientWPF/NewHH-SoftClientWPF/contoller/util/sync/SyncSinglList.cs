﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.contoller.util.sync.autosave;
using NewHH_SoftClientWPF.contoller.util.sync.autosave.support;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack.updateIconSync;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.sync
{
    class SyncSinglList
    {
        public SyncSinglListModel syncCreateAllList;
        private UpdateNetworkJsonController _updateSendController;
        private SupportWebDavRenameFiles _supportWebDavRenameFiles;
        private IAutoSaveFolder _autoSaveFolder;
        private Dictionary<long, long> allPcSave = null;

        public SyncSinglList(SyncSinglListModel syncCreateAllList , UpdateNetworkJsonController _updateSendController)
        {
            syncCreateAllList._stackUpdateTreeViewIcon = new StackUpdateTreeViewIcon(syncCreateAllList._convertIcon);
            this.syncCreateAllList = syncCreateAllList;
            this._updateSendController = _updateSendController;
            _supportWebDavRenameFiles = new SupportWebDavRenameFiles();
            _autoSaveFolder = new IconAutoSaveFolder(syncCreateAllList._sqlliteController , syncCreateAllList);
        }


        public void SyncSinglTraining()
        {
            if (syncCreateAllList.responceServer.textmessage.Length >= 5)
            {
                string statusDeleteParties = syncCreateAllList.responceServer.textmessage[4];

                if (statusDeleteParties.Equals("DELETEBEGINPARTIES"))
                {
                    Console.WriteLine("ObserverNetworkUpdate-> Пришел запрос от сервера DELETEBEGINPARTIES");
                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                    syncCreateAllList._viewModelMain.pg = 10;
                    syncCreateAllList._viewModelMain.SetNewIconTaskBar(syncCreateAllList._convertIcon.streamToIcon(".notifyIconBarRefresh"));
                }
                else if (statusDeleteParties.Equals("DELETEPARTIES"))
                {
                    Console.WriteLine("ObserverNetworkUpdate-> Пришел запрос от сервера DELETEPARTIES");
                    //syncCreateAllList._controlMq.DeleteMqQueue("DELETEPARTIES");
                    deletePartiesStart(syncCreateAllList.responceServer);
                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");

                }
                else if (statusDeleteParties.Equals("DELETEENDPARTIES"))
                {
                   // syncCreateAllList._controlMq.DeleteMqQueue("DELETEENDPARTIES");
                    deletePartiesStart(syncCreateAllList.responceServer);
                    Console.WriteLine("ObserverNetworkUpdate-> Пришел запрос от сервера DELETEENDPARTIES");
                    //syncCreateAllList._viewModelMain.pg = 100;
                    // syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизирован");

                    _updateSendController.updateSendJsonCHECKWORKINGSERVER();
                    rebuildingSizeSql();
                }
                //PASTE
                else if (statusDeleteParties.Equals("PASTEBEGINPARTIES"))
                {
                   // syncCreateAllList._controlMq.DeleteMqQueue("PASTEBEGINPARTIES");
                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                    syncCreateAllList._viewModelMain.pg = 10;
                    Console.WriteLine("ObserverNetworkUpdate-> Пришел запрос от сервера PASTEBEGINPARTIES");
                    syncCreateAllList._viewModelMain.SetNewIconTaskBar(syncCreateAllList._convertIcon.streamToIcon(".notifyIconBarRefresh"));
                }

                else if (statusDeleteParties.Equals("PASTEPARTIES"))
                {
                    //syncCreateAllList._controlMq.DeleteMqQueue("PASTEPARTIES");
                    pastePartiesStart(syncCreateAllList.responceServer);
                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                }

                else if (statusDeleteParties.Equals("PASTEENDPARTIES"))
                {
                    //syncCreateAllList._controlMq.DeleteMqQueue("PASTEENDPARTIES");

                    pastePartiesStart(syncCreateAllList.responceServer);


                    if (syncCreateAllList._copyStatus.isMoveNetwork == true)
                    {
                       // syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                    }
                    else
                    {
                        syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизирован");
                    }

                    //Говорим, что можем продолжать копирование в ViewFolder или MainFolder
                    syncCreateAllList._copyStatus.isCopyStatus = false;
                    //syncCreateAllList._viewModelMain.pg = 100;

                    _updateSendController.updateSendJsonCHECKWORKINGSERVER();
                    rebuildingSizeSql();

                }
                else if (statusDeleteParties.Equals("SINGLBEGINUPDATEPARTIES"))
                {

                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                   // syncCreateAllList._controlMq.DeleteMqQueue("SINGLBEGINUPDATEPARTIES");
                    syncCreateAllList._viewModelMain.pg = 10;
                    syncCreateAllList._viewModelMain.SetNewIconTaskBar(syncCreateAllList._convertIcon.streamToIcon(".notifyIconBarRefresh"));
                    Console.WriteLine("SincSinglList-> SyncSinglTraining*****<----Пришел запрос от сервера SINGLBEGINUPDATEPARTIES");

                }
                else if (statusDeleteParties.Equals("SINGLUPDATEPARTIES"))
                {

                    syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизация");
                    //syncCreateAllList._controlMq.DeleteMqQueue("SINGLUPDATEPARTIES");
                    syncCreateAllList._viewModelMain.pg = 10;
                    SinglParties(syncCreateAllList.responceServer);
                    Console.WriteLine("SincSinglList-> SyncSinglTraining*****<----Пришел запрос от сервера SINGLUPDATEPARTIES");

                }
                else if (statusDeleteParties.Equals("SINGLENDUPDATEPARTIES"))
                {

                    SinglENDParties(syncCreateAllList.responceServer);
                   // syncCreateAllList._controlMq.DeleteMqQueue("SINGLENDUPDATEPARTIES");
                    //syncCreateAllList._viewModelMain.pg = 100;
                    Console.WriteLine("SincSinglList-> SyncSinglTraining*****<----Пришел запрос от сервера SINGLENDUPDATEPARTIES");
                    //syncCreateAllList._updateMainForm.updateTitleMainWindow("Синхронизирован");

                    _updateSendController.updateSendJsonCHECKWORKINGSERVER();
                    rebuildingSizeSql();
                }

            }


        }


        //Обновляет базу не стирая все старые данные
        private void SinglENDParties(ServerTopicModel responceServer)
        {
            syncCreateAllList._viewModelMain.pg = 45;

            if(responceServer.listFileInfo != null)
            {
                List<FileInfoModel> listFiles = responceServer.listFileInfo.ToList();

                syncCreateAllList._viewModelMain.pg = 55;

                //sqllite обновляем не удаляя базу целиком только отедльные узлы
                syncCreateAllList._updateDataController.UpdateSinglSqliteData(listFiles, syncCreateAllList._viewModelMain);

                syncCreateAllList._viewModelMain.pg = 65;


                syncCreateAllList._sqlliteController.checkActiveTransaction();
                //обновляем дерево (не полностью) только открытые ноды в дереве
                //или полностью если дерево не имеет нодов
                syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
                syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);
                syncCreateAllList._viewModelMain.pg = 83;

                addOkEndIcon(listFiles, syncCreateAllList);
                addOkSaveFolderUpdateIcon(listFiles, syncCreateAllList);
                //проверял изменятся ли иконки на нужные мне
                // addRefreshUpdateIcon(listFiles, syncCreateAllList);

                syncCreateAllList._viewModelMain.pg = 93;
                listFiles = null;
                rebuildingSizeSql();
            }
          
            
        }

        private void rebuildingSizeSql()
        {
            Task.Run(() =>
            {
                InfoSqlSize infoSql = new InfoSqlSize(syncCreateAllList._sqlliteController);
                App.Current.Dispatcher.Invoke(new System.Action(() => syncCreateAllList._viewModelMain.PgSizeDisk = infoSql.getSize()));
            });
        }
        //Только те которые мы сделали сохронить на компьютере
        private void addOkUpdateIcon(List<FileInfoModel> listFiles , SyncSinglListModel syncCreateAllList)
        {
            allPcSave = getPcSaveAllRowId(syncCreateAllList._sqlliteController, allPcSave);
            listFiles = getSortedOkList(ref allPcSave, ref listFiles);

            foreach (FileInfoModel fim in listFiles)
            {
                if(fim != null) _supportWebDavRenameFiles.addOkFolder(ref syncCreateAllList, fim);
            }
            
        }

        private void addOkSaveFolderUpdateIcon(List<FileInfoModel> listFiles, SyncSinglListModel syncCreateAllList)
        {
            _autoSaveFolder.IconOkAutoSaveFolder(listFiles);
        }
        //вообще все остальные хранящиеся на сервере
        private void addRefreshUpdateIcon(List<FileInfoModel> listFiles, SyncSinglListModel syncCreateAllList)
        {
            IUpdateALLIcon update = new UpdateALLiconSync();
            foreach (FileInfoModel fim in listFiles)
            {
                if (fim != null) update.UpdateAllRefresh(ref syncCreateAllList, fim);
            }

        }

        private void addOkEndIcon(List<FileInfoModel> listFiles, SyncSinglListModel syncCreateAllList)
        {
            allPcSave = getPcSaveAllRowId(syncCreateAllList._sqlliteController, allPcSave);
            listFiles = getSortedOkList(ref allPcSave, ref listFiles);

            foreach (FileInfoModel fim in listFiles)
            {
                if (fim != null) _supportWebDavRenameFiles.addOkFolder(ref syncCreateAllList, fim);
            }
            allPcSave = null;
        }

        private Dictionary<long, long> getPcSaveAllRowId(SqliteController sqliteController, Dictionary<long, long> allPcSave)
        {
            return _supportWebDavRenameFiles.getAllPcSaveRow_id(sqliteController,  allPcSave);
        }

        private List<FileInfoModel> getSortedOkList(ref Dictionary<long , long> allPcSave , ref List<FileInfoModel> fimList)
        {
            return _supportWebDavRenameFiles.getSortedOkList(ref  allPcSave, ref fimList);
        }

        //Обновляет базу не стирая все старые данные
        private void SinglParties(ServerTopicModel responceServer)
        {
            syncCreateAllList._viewModelMain.pg = 25;
            List<FileInfoModel> listFiles = responceServer.listFileInfo.ToList();

            //sqllite обновляем не удаляя базу целиком только отедльные узлы
            syncCreateAllList._updateDataController.UpdateSinglSqliteData(listFiles, syncCreateAllList._viewModelMain);

            //обновляем дерево (не полностью) только открытые ноды в дереве
            //или полностью если дерево не имеет нодов
            syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
            syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);
            syncCreateAllList._viewModelMain.pg = 33;


            addOkUpdateIcon(listFiles, syncCreateAllList);
            addOkSaveFolderUpdateIcon(listFiles, syncCreateAllList);

            syncCreateAllList._viewModelMain.pg = 55;
            listFiles = null;
        }

        private void pastePartiesStart(ServerTopicModel responceServer)
        {
            List<FileInfoModel> listFiles = responceServer.listFileInfo.ToList();
            syncCreateAllList._viewModelMain.pg = 15;
            if (listFiles != null)
            {
                syncCreateAllList._viewModelMain.pg = 16;
                syncCreateAllList._viewModelMain.pg = 17;
                syncCreateAllList._viewModelMain.pg = 18;
                Console.WriteLine("ObserverNetworkUpdate->SYNSINGLLIST -> PASTEPARTIES Save sqllite");
                syncCreateAllList._viewModelMain.pg = 19;
                syncCreateAllList._updateDataController.insertSqlliteFileInfo(listFiles, syncCreateAllList._viewModelMain);

            }

            //обновляем дерево (не полностью) только открытые ноды в дереве
            //или полностью если дерево не имеет нодов
            syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
            syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);

            listFiles = null;
            syncCreateAllList._viewModelMain.pg = 13;
        }

        private void deletePartiesStart(ServerTopicModel responceServer)
        {
            syncCreateAllList._viewModelMain.pg = 25;
            List<FileInfoModel> listFiles = responceServer.listFileInfo.ToList();

            //sqllite обновляем
            syncCreateAllList._updateDataController.UpdateSingl(listFiles, syncCreateAllList._viewModelMain);

            //обновляем дерево (не полностью) только открытые ноды в дереве
            //или полностью если дерево не имеет нодов
            syncCreateAllList._mainUpdateForm.UpdateMainTreeView(listFiles);
            syncCreateAllList._mainUpdateForm.UpdateMainListView(listFiles);

            syncCreateAllList._viewModelMain.pg = 33;
            listFiles = null;
        }



    }
}
