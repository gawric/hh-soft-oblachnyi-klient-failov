﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.sync.stack
{
    public class CreateItem
    {

        int f = 0;

        public FileInfoModel getFileInfoModelToWebDav(TraverseTreeModel treeObjModel , WokrkerStackModel workerModel  , FileInfoModel[] containerAllFiles , FileInfoModelSupport modelSupport)
        {


            bool checkExistDataBases = false;
            FileInfoModel modelFileInfo = null;

            try
            {
                
                //если это папка будет не определенно кол-во байтов
                if (workerModel.modelWebDav.ContentLength == null)
                {
                    workerModel.modelWebDav.ContentLength = 0;

                }

          
                if (workerModel.sqlArrayHref.ContainsKey(workerModel.modelWebDav.Href))
                {
                    

                    long row_id = workerModel.sqlArrayHref[workerModel.modelWebDav.Href];

                    //что у нас есть такой элемент во временном масиве
                    if (workerModel.sqlArray.ContainsKey(row_id))
                    {
                        deleteModel(workerModel, row_id , workerModel.modelWebDav.Href);

                        //true не будет добавлять в массив для передачи другим клиентам и серверу т.к он там уже есть
                        checkExistDataBases = true;

                        f = f + 1 / staticVariable.Variable.CountFilesTrasferServer;
                        updateProgress(treeObjModel, f);

                       
                    }

                }
                else
                {


                    //нужно будет оптимизировать т.к 3 раза проходит по массиву из 1000 элементов
                    //поиск его родителя в базе
                    // FileInfoModel modelFileInfoParet = treeObjModel._sqlLiteController.getSearchNodesByLocation(workerModel.parentHref);
                    long modelFileInfoParetrow_id = -1;

                    if(treeObjModel.sqlArrayHref.ContainsKey(workerModel.parentHref))
                        modelFileInfoParetrow_id = treeObjModel.sqlArrayHref[workerModel.parentHref];



                    if (modelFileInfoParetrow_id != -1)
                    {
                        modelFileInfo = createModelInfoParent(treeObjModel, workerModel, containerAllFiles, modelSupport, modelFileInfoParetrow_id);
                    }
                    else
                    {
                      

                        string strCheck = getRootHref();
                        int[] check = { 0 };

                        checkRootHref(workerModel, check, strCheck);

                        modelFileInfo = createRootHrefModel(check, treeObjModel, workerModel, modelFileInfo, containerAllFiles, modelSupport);
                        
                    }

                    if (modelFileInfo != null)
                    {
                        addNewLocation(workerModel, modelFileInfo);
                    }
                    else
                    {
                        Console.WriteLine("!!!!! FileInfoModelSupport->getFileInfoModelToWebDav: Критическая ошибка  !!!!! Описание: Не создан modelFileInfo для записи его в бд. Href:  "+workerModel.modelWebDav.Href+ "parentHref: "+workerModel.parentHref);

                    }

                }

            }
            catch (System.IndexOutOfRangeException sr)
            {
                Console.WriteLine(sr);
            }
            catch (System.NullReferenceException sr)
            {
                Console.WriteLine(sr);
            }


           
            //если такой элемент уже есть в нашей базе данных
            //мы его не отправляем на сервак, а делаем null что-бы он его пропустил
            if(checkExistDataBases)
            {
                return null;
            }
            else
            {
                return modelFileInfo;
            }


        }


        private FileInfoModel  createRootHrefModel(int[] check , TraverseTreeModel treeObjModel,  WokrkerStackModel workerModel, FileInfoModel modelFileInfo , FileInfoModel[] containerAllFiles, FileInfoModelSupport modelSupport)
        {
            //проверка если мы удалим из location часть пути а имеено домен и после него не останется записей
            //значит папка содержится в рутовой папке 
            if (check[0] != 0)
            {
                if (workerModel.parentArray.ContainsKey(workerModel.parentHref))
                {
                    return createModel(treeObjModel, workerModel, containerAllFiles, modelSupport);
                }
                else
                {
                    return null;
                }
                    
            }
            else
            {
                return createModelNoRootHref(treeObjModel, workerModel, containerAllFiles, modelSupport);
            }
        }



        private void addNewLocation(WokrkerStackModel workerModel , FileInfoModel modelFileInfo)
        {
            //записываем только новые узлы полученные в ходе сканирования
            workerModel.parentArray.Add(modelFileInfo.location, modelFileInfo.row_id);
        }



        private FileInfoModel createModelNoRootHref(TraverseTreeModel treeObjModel, WokrkerStackModel workerModel, FileInfoModel[] containerAllFiles, FileInfoModelSupport modelSupport)
        {
            long id_databases = treeObjModel._sqlLiteController.getSelect().getLastRow_idFileInfoModel() + 1;

            //проверяет что в массиве нет новых нодов с более высоким или таким же id
            long new_row_id = staticVariable.Variable.generatedNewRowId(id_databases, containerAllFiles);
            treeObjModel._sqlLiteController.getInsert().insertListFilesTempIndex(new_row_id);
            return  modelSupport.createFileInfoModel(new_row_id, workerModel.modelWebDav.DisplayName, workerModel.modelWebDav.CreationDate.ToString(), workerModel.modelWebDav.LastModified.ToString(), workerModel.modelWebDav.LastModified.ToString(), workerModel.modelWebDav.ContentType, workerModel.modelWebDav.Href, -1, workerModel.modelWebDav.ContentType, (long)workerModel.modelWebDav.ContentLength, treeObjModel.newVersionBases, 0, 0, "CREATE", 0);
        
        }






        private FileInfoModel createModelInfoParent(TraverseTreeModel treeObjModel, WokrkerStackModel workerModel, FileInfoModel[] containerAllFiles, FileInfoModelSupport modelSupport, long modelFileInfoParetId)
        {
            long id_databases = treeObjModel._sqlLiteController.getSelect().getLastRow_idFileInfoModel() + 1;
            //проверяет что в базе нет новы нодов с более высоким или таким же id
            long new_row_id = staticVariable.Variable.generatedNewRowId(id_databases, containerAllFiles);
            treeObjModel._sqlLiteController.getInsert().insertListFilesTempIndex(new_row_id);

            return modelSupport.createFileInfoModel(new_row_id, workerModel.modelWebDav.DisplayName, workerModel.modelWebDav.CreationDate.ToString(), workerModel.modelWebDav.LastModified.ToString(), workerModel.modelWebDav.LastModified.ToString(), workerModel.modelWebDav.ContentType, workerModel.modelWebDav.Href, modelFileInfoParetId, workerModel.modelWebDav.ContentType, (long)workerModel.modelWebDav.ContentLength, treeObjModel.newVersionBases, 0, 0, "CREATE", 0);
        }







        private FileInfoModel  createModel(TraverseTreeModel treeObjModel, WokrkerStackModel workerModel , FileInfoModel[] containerAllFiles, FileInfoModelSupport modelSupport)
        {
            long id_databases = getLRowId(treeObjModel._sqlLiteController);

            //проверяет что в массиве нет новы нодов с более высоким или таким же id
            long new_row_id = staticVariable.Variable.generatedNewRowId(id_databases, containerAllFiles);
            insetListTemp(treeObjModel._sqlLiteController,  new_row_id);

            long parentRow_Id = getParentHref(workerModel.parentHref, workerModel.parentArray);
           // long parentRow_Id = workerModel.parentArray[workerModel.parentHref];

            return  modelSupport.createFileInfoModel(new_row_id, workerModel.modelWebDav.DisplayName, workerModel.modelWebDav.CreationDate.ToString(), workerModel.modelWebDav.LastModified.ToString(), workerModel.modelWebDav.LastModified.ToString(), workerModel.modelWebDav.ContentType, workerModel.modelWebDav.Href, parentRow_Id, workerModel.modelWebDav.ContentType, (long)workerModel.modelWebDav.ContentLength, treeObjModel.newVersionBases, 0, 0, "CREATE", 0);
        }

        private void insetListTemp(SqliteController sql , long new_row_id)
        {
            if(sql != null) sql.getInsert().insertListFilesTempIndex(new_row_id);
        }


        private long getParentHref(string parentHref, Dictionary<string, long> dict)
        {
            if(dict.ContainsKey(parentHref))
            {
                return dict[parentHref];
            }
            else
            {
                Console.WriteLine("CreateItem->getFileInfoModelToWebDav: Критическая ошибка не найден ParrentArray");
                return 0;
            }
        }
        private long getLRowId(SqliteController sql)
        {
            long rowId = 0;
            if (sql != null) rowId = sql.getSelect().getLastRow_idFileInfoModel() + 1;

            return rowId;
        }





        private void checkRootHref(WokrkerStackModel workerModel , int[] check , string strCheck)
        {
            //если количество символов в строке домена
            //равна количеству символов в parentHref означает, что родитель рутовая папка
            if (workerModel.parentHref.Length == strCheck.Length)
            {
                check[0] = 0;
            }
            else
            {
                check[0] = workerModel.parentHref.Length;
            }

        }
        private void updateProgress(TraverseTreeModel treeObjModel , double procent)
        {
            
            treeObjModel.progress.Report(f);
        }




        private void deleteModel(WokrkerStackModel workerModel , long row_id , string hrefLocation)
        {
            //sqlArray содержит все записи в нашей базе  
            //мы удаляем row_id из этого массива что-бы в будущем определить
            //какие записи нужно удалить т.е они уже по факту не содержатся webdav
            //Оказался ошибочный вывод нельзя килять узел т.к у него может быть цела куча ДЕТЕЙ!
             workerModel.sqlArray.Remove(row_id);
            //workerModel.sqlArrayHref.Remove(hrefLocation);
        }


        private string getRootHref()
        {
            //получается https://localhost:8080/test/
            return staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;
        }



    }
}
