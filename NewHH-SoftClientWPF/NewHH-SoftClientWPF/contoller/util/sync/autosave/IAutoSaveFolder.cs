﻿using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave
{
    public interface IAutoSaveFolder
    {
        void StartUploadFolder(List<ScanFolderModel> listPath, List<FileInfoModel> parentListPasteModel);
        void IconOkAutoSaveFolder(List<FileInfoModel> listFim);
    }
}
