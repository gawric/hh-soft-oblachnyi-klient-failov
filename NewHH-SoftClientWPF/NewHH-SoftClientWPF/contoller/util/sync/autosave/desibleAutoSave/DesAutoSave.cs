﻿using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave.desibleAutoSave
{
    public class DesAutoSave: IDesAutoSave
    {
        private SqliteController sql;
        private WebDavClientExist wdce;
        private SupportCFData scfData;
        private FirstSyncFilesController fsfc;
        public DesAutoSave(SystemSettingModelObj _ssmo)
        {
            this.sql = _ssmo._sqliteController;
            this.scfData = new SupportCFData(sql);
            this.wdce = _ssmo._wdce;
            this.fsfc = _ssmo._fsfc;
        }

        public async void StartDisabledAutoSave()
        {
            string myPcRootFolder = Variable.EscapeUrlString(GetRootFolderNodes().location + UtilMethod.GetMachineName() + "/");

            if (await IsCreateServerFolder(myPcRootFolder))
            {
                DeleteFolder(myPcRootFolder, scfData);
                ClearTablesAutoSave(sql);
            }
           
        }

        private void ClearTablesAutoSave(SqliteController sql)
        {
            sql.getDelete().ClearListScanFolder();
        }

        public Task<bool> IsCreateServerFolder(string rootLocation)
        {
            return wdce.isExist(rootLocation);
        }

        private void DeleteFolder(string myPcRootFolder, SupportCFData scfData)
        {
            FileInfoModel pasteModel = scfData.getRootAutoSaveModel(myPcRootFolder);
            List<FileInfoModel> childrenList = scfData.getChildrenAutoSaveModel(pasteModel.row_id);
            string Event = "DELETE";
            Delete(ref childrenList, ref Event);
        }

        private void Delete(ref List<FileInfoModel> childrenList, ref string Event)
        {
            foreach (FileInfoModel fim in childrenList)
            {
                FolderNodes[] folderArr = { ConvertFimtoFolderNodes(fim) };
                fsfc.DeleteTaskNoFormNoNewThread(ConvertFimtoFolderNodes(fim), folderArr, Event);
            }
        }

        private FileInfoModel GetRootFolderNodes()
        {
            return UtilMethod.getRootFolderNodes();
        }

        private FolderNodes ConvertFimtoFolderNodes(FileInfoModel pasteModel)
        {
            FolderNodes fol = new FolderNodes();
            fol.Row_id = pasteModel.row_id;
            fol.FolderName = pasteModel.filename;
            fol.Location = pasteModel.location;
            fol.ParentID = pasteModel.parent;
            fol.Type = pasteModel.type;

            return fol;
        }

     
    }
}
