﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave.support
{
    public class IconAutoSaveFolder: IAutoSaveFolder
    {
        private SqliteController sql;
        private SyncSinglListModel model;
        public IconAutoSaveFolder(SqliteController sql , SyncSinglListModel model)
        {
            this.sql = sql;
            this.model = model;
        }

        public void IconOkAutoSaveFolder(List<FileInfoModel> listFim)
        {

            Dictionary<long, long>  allAutoSaveItem = sql.getSelect().getAllAutoSaveListFilesRowId();
            List<FileInfoModel>  listFimSorted = getSortedOkList(allAutoSaveItem, ref listFim);


            listFimSorted.ForEach(x => RunOkFolder(x , model));
            listFimSorted.Clear();
            allAutoSaveItem.Clear();
        }
        private void RunOkFolder(FileInfoModel fim , SyncSinglListModel model)
        {
            if (fim != null) addOkFolder(ref model, fim);
        }

        private void addOkFolder(ref SyncSinglListModel model, FileInfoModel parentModel)
        {
            
                string type = model._sqlliteController.getSelect().getTypeAutoSaveFolder(parentModel.row_id);
                string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.okIconId);

                model._sqlliteController.updateTypeAndLocationAutoSaveFolder(parentModel.row_id, newtype, parentModel.location, staticVariable.Variable.okIconId);

                model._stackUpdateTreeViewIcon.TraverseSingl(model._viewModelMain.NodesView.Items[0].Children, parentModel.row_id, staticVariable.Variable.okIconId);
                model._stackUpdateTreeViewIcon.TraverseViewSingl(model.rootNodesListView, parentModel.row_id, staticVariable.Variable.okIconId);

                //  Console.WriteLine("Закончили обработку файла  " + parentModel.filename);

            
        }
        public List<FileInfoModel> getSortedOkList(Dictionary<long, long> allPcSave, ref List<FileInfoModel> fimList)
        {
            List<FileInfoModel> sortedList =  fimList
                .FindAll(x =>x != null)
                .FindAll(x=>allPcSave.ContainsKey(x.row_id))
                .ToList();
     
            return sortedList;
        }

        public void StartUploadFolder(List<ScanFolderModel> listPath, List<FileInfoModel> parentListPasteModel)
        {
            throw new NotImplementedException();
        }
    }
}
