﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.operationContextMenu.upload;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave.support
{
    public class InitAutoSaveFolder
    {
    
        private WebDavClientExist _wdce;
        private CreateFolderController _cfc;
        private ExceptionController _excepc;
        private FirstSyncFilesController _fsfc;

        public InitAutoSaveFolder(WebDavClientExist wdce, CreateFolderController cfc , ExceptionController excepc , FirstSyncFilesController fsfc)
        {
            this._wdce = wdce;
            this._cfc = cfc;
            this._excepc = excepc;
            this._fsfc = fsfc;
        }
        //Создаем папки из списка выбранных на сервере
        //и проверяем что рутовая папка создана для хранения всех остальных
        public async Task InitUpload(SupportCFData scfData)
        {
            string myPcRootFolder = Variable.EscapeUrlString(GetRootFolderNodes().location + UtilMethod.GetMachineName() + "/");


            if (await IsCreateServerFolder(myPcRootFolder))
            {
                DeleteFolder(myPcRootFolder , scfData);
                ClearDataBases(scfData);
                WaitClearDatabases(scfData);

            }
            else
            {
                CreateFolderAutoSave(GetRootFolderNodes(), UtilMethod.GetMachineName());
            }
        }


        private void ClearDataBases(SupportCFData scfData)
        {
            scfData.clearListScanFolder();
        }
        
        private FolderNodes ConvertFimtoFolderNodes(FileInfoModel pasteModel)
        {
            FolderNodes fol = new FolderNodes();
            fol.Row_id = pasteModel.row_id;
            fol.FolderName = pasteModel.filename;
            fol.Location = pasteModel.location;
            fol.ParentID = pasteModel.parent;
            fol.Type = pasteModel.type;

            return fol;
        }


        private void DeleteFolder(string myPcRootFolder , SupportCFData scfData)
        {
            FileInfoModel pasteModel = scfData.getRootAutoSaveModel(myPcRootFolder);

            if(pasteModel != null)
            {
                List<FileInfoModel> childrenList = scfData.getChildrenAutoSaveModel(pasteModel.row_id);
                string Event = "DELETE";
                Delete(ref childrenList, ref Event);
                WaitDeleteJson(childrenList, scfData);
            }
            else
            {
                Debug.Print("InitAutoSaveFolder->DeleteFolder: критическая ошибка не найден pasteModel");
            }
            

        }
        //true нашли 
        //false не нашли
        private bool WaitDeleteJson(List<FileInfoModel> childrenList , SupportCFData scfData)
        {
            bool isDel = true;
            int stop = 0;

            while(true)
            {
                Debug.Print("InitAutoSaveFolder->WaitDeleteJson: Ожидание удаление старых данных");

                if(childrenList.Count > 0)
                {
                    FileInfoModel fim = childrenList[0];
                    isDel = scfData.isExistsRowId(fim.row_id);
                }
                else
                {
                    //если не нашли детей у pasteModel
                    //т.е в нашей базу записей нет
                    break;
                }

                Thread.Sleep(1000);

                if (!isDel) break;
                if (stop == 30) break;
                stop++;
            }

            return isDel;
        }
        private void WaitClearDatabases(SupportCFData scfData)
        {

            int stop = 0;

            while (true)
            {
                Debug.Print("InitAutoSaveFolder->WaitClearDatabases: Ожидание...... очистки базы данных");
                if (scfData.isExistsClearListScanFolder())
                {
                    Debug.Print("InitAutoSaveFolder->WaitClearDataBases: База очищена прекращаем цикл");
                    break;
                }
               
                
                Thread.Sleep(1000);

                if (stop == 30) break;
                stop++;
            }

        }
        private void Delete(ref List<FileInfoModel> childrenList , ref string Event)
        {
            foreach (FileInfoModel fim in childrenList)
            {
                FolderNodes[] folderArr = { ConvertFimtoFolderNodes(fim) };
                _fsfc.DeleteTaskNoFormNoNewThread(ConvertFimtoFolderNodes(fim), folderArr, Event);
            }
        }
        private async void CreateFolderAutoSave(FileInfoModel sourceNodes, string myPcName)
        {
            
            await _cfc.TrainingCreateFolder(sourceNodes, myPcName);
        }

       
        public Task<bool> IsCreateServerFolder(string rootLocation)
        {
            return _wdce.isExist(rootLocation);
        }
        private FileInfoModel GetRootFolderNodes()
        {
            return UtilMethod.getRootFolderNodes();
        }

       

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _excepc.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }
    }
}
