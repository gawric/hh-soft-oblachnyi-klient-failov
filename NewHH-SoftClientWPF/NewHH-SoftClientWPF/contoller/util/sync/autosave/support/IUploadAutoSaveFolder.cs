﻿using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.sync.autosave.support
{
    public interface IUploadAutoSaveFolder
    {
        void UploadFolder(List<ScanFolderModel> listPath);
    }
}
