﻿using NewHH_SoftClientWPF.contoller.statusWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList
{
    class SyncSearch
    {
        private SyncSearchModel searchModel;

        public SyncSearch(SyncSearchModel searchModel)
        {
            this.searchModel = searchModel;
        }

        public void SearchBeginFiles()
        {
            //searchModel._controlMq.DeleteMqQueue("SEARCHBEGINFILES");
            searchModel._updateMainForm.updateTitleMainWindow("Синхронизация");
            searchModel._viewModelMain.pg = 10;
            searchModel._viewModelMain.pg = 20;
            searchModel._viewModelMain.pg = 30;
        }

        public void SearchPartiesFiles()
        {
           // searchModel._controlMq.DeleteMqQueue("SEARCHPARTIESFILES");
            searchModel._updateMainForm.updateTitleMainWindow("Синхронизация");
            searchModel._viewModelMain.pg = 40;
            FakeUpdaterProgressBar fake = new FakeUpdaterProgressBar();
            //обновляет pg как будто мы что-то делаем
            //на самом деле работает другой клиент а здесь мы только
            //отражаем ход работы
            fake.UpdateMainWindowsPg(searchModel._viewModelMain);
            searchModel._viewModelMain.pg = 10;
        }

        public void SearchEndFiles()
        {
            //searchModel._controlMq.DeleteMqQueue("SEARCHENDFILES");
            //searchModel._viewModelMain.pg = 100;
            //searchModel._updateMainForm.updateTitleMainWindow("Синхронизирован");
        }
    }
}
