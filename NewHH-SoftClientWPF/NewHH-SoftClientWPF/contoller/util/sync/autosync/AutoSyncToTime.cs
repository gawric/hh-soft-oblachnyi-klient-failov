﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.scanner.autoscan;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.sync
{
    public class AutoSyncToTime
    {
    
        private AutoSyncToTimeObj _autoSynObj;
        //true в работе
        //false свободно
        private bool[] isRunningAllUpdateNoDelete = { false };
        private bool  isRunningAutoSyncService = false;
        private bool stopService = false;
    
 

        public AutoSyncToTime(AutoSyncToTimeObj autoSyncObj)
        {
            _autoSynObj = autoSyncObj;
          


        }

        public void Start()
        {
            UpdateStatusPublsher();
            Task.Run(() => 
            {
                RunStart();
            });
            
        }
      
        private void RunStart()
        {
            Console.WriteLine("AutoSyncToTime-> RunStart: Попытка запроса в AutoSyncToTime");
            
            WaitAnswer();

            while (true)
            {
                //false значит время ожидания для сканера вышло и можно запускать сканирование
                //true значит что мы изменили время ожидания запуска и решили его изменить тогда возвращаем true
                Console.WriteLine("AutoSyncToTime->RunStart: осталось времени до начала автосканирования " + staticVariable.Variable.startMillisecondAutoSync);
                if (!WaitScanner(staticVariable.Variable.startMillisecondAutoSync)) startTime();
                
                isRunningAutoSyncService = true;

                if (StopService())
                    break;
                
            }
        }


        private void startTime()
        {
            //если Running уже запущен пропускаем отправку пересоздания папок
            if (!isRunningAllUpdateNoDelete[0])
            {
                UpdateStatusPublsher();
                WaitAnswer();

                //если ответ от сервера сказал, что он занят другим делом
                if (!staticVariable.Variable.activemqIsWorking)
                {
                    //если мы не публикатор
                    if (staticVariable.Variable.activemqIsPublisher)
                    {
                        Console.WriteLine("AutoSyncToTIme: кол-во сохраненных файлов на диске для чтения в базу   " + _autoSynObj.saveJsonToDiskContoller.getListGetLazyTempHref());
                        //если мы сейчас не читаем с диска(здесь хранится список с файлами записанные на диск)
                        if (_autoSynObj.saveJsonToDiskContoller.getListGetLazyTempHref().Count <= 0)
                        {
                            if (!_autoSynObj.statusTransactionSave.isTransaction)
                            {
                                //проверяем что операция на данном клиенте копирования удаления и перемещения не запущены
                                if (!checkAllOperationStatus())
                                {
                                    RunningService();
                                }

                            }



                        }

                    }

                }

            }
        }

        private bool checkAllOperationStatus()
        {
            bool check = false;
            bool[] arr = _autoSynObj._allStatus.isAllStatus();

            for (int g = 0;  g < arr.Length; g++)
            {
                if (arr[g] == true)
                {
                    Console.WriteLine("AutoSyncToTime->checkAllOperationStatus: Автоматическое сканирование не будет запущенно т.к мы нашли работающий процесс");
                    check = true;
                    break;
                }
                    
            }

            return check;
        }

        private void UpdateStatusPublsher()
        {
            _autoSynObj._updateNetworkJsonController.updateSendJsonSTATUSPUBLISHER();
        }

        private bool WaitScanner(int milliseconds)
        {
            _autoSynObj._blockingEvent.createManualResetEvent(staticVariable.Variable.runningAutoSyncBlockingId);
            return _autoSynObj._blockingEvent.getManualReset(staticVariable.Variable.runningAutoSyncBlockingId).getManualResetEvent().WaitOne(milliseconds);
        }

        private void WaitAnswer()
        {
            _autoSynObj._blockingEvent.createManualResetEvent(staticVariable.Variable.autoSyncToTimeBlockingId);
            _autoSynObj._blockingEvent.getManualReset(staticVariable.Variable.autoSyncToTimeBlockingId).WaitOne();
        }

        public void StopRunningShedule()
        {
            //отмена операции сканирования
            _autoSynObj._cancellationController.startCansel("ResponceServerScheduleNoDeleteBases");
        }

        public void StopRunningAutoSync()
        {
            stopService = true;
        }

        public bool isRunningAutoSyncToTimeService()
        {
            return isRunningAutoSyncService;
        }

        public bool isRunningShedule()
        {
            return isRunningAllUpdateNoDelete[0];
        }

        //перечитать всю базу данных не удаляет старую
        public void startAutoScanNoDeleteBases(bool[] isRunningAllUpdateNoDelete)
        {
            createCancellation(_autoSynObj);
            _autoSynObj.usernameAndPassword = getUsername(_autoSynObj);
            _autoSynObj.version = getVersionBases(_autoSynObj);

            _autoSynObj._scanner = new ScannerWebDavAndSendToServer(_autoSynObj._exceptionController);
            _autoSynObj._scanner.TrainingStartScan(_autoSynObj , isRunningAllUpdateNoDelete);
        }


      

        private void RunningService()
        {
            if (!isRunningAllUpdateNoDelete[0])
            {
                Console.WriteLine("AutoSyncToTime->Start->RunStart->RunningService: Автоматический запуск синхронизации webdav и acteveMq");
                startAutoScanNoDeleteBases(isRunningAllUpdateNoDelete);
            }

        }
        private bool StopService()
        {
            //выход
            if (stopService)
            {
                stopService = false;
                isRunningAutoSyncService = false;
                return true;
            }
            {
                return false;
            }
        }


        private string[] getUsername(AutoSyncToTimeObj _autoSynObj)
        {
            return _autoSynObj._sqlLiteController.getSelect().GetUserSqlite();
        }
        private long getVersionBases(AutoSyncToTimeObj _autoSynObj)
        {
            return _autoSynObj._sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient();
        }

        private void createCancellation(AutoSyncToTimeObj _autoSynObj)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            _autoSynObj._cancellationController.addWarehouse(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID, cts);
        }


    }
}
