﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sync
{
    public class AutoSyncController
    {
        private AutoSyncToTime _autoSync;
        private AutoSyncToTimeObj _autoSynObj;
        private BlockingEventController _bec;


        public AutoSyncController(Container cont)
        {
            _autoSynObj = new AutoSyncToTimeObj();
            

            _autoSynObj._cancellationController =  cont.GetInstance<CancellationController>();
            _autoSynObj._updateNetworkJsonController  = cont.GetInstance<UpdateNetworkJsonController>();
            _autoSynObj._sqlLiteController = cont.GetInstance<SqliteController>();
            _autoSynObj._webDavClient = cont.GetInstance<WebDavClientController>();
            _autoSynObj._supportTopicModel = new SupportServerTopicModel();
            _autoSynObj._blockingEvent = cont.GetInstance<BlockingEventController>();
            _autoSynObj._viewModel = cont.GetInstance<MainWindowViewModel>();
            _autoSynObj.saveJsonToDiskContoller = cont.GetInstance<SaveJsonToDiskContoller>();
            _autoSynObj.statusTransactionSave =  cont.GetInstance<StatusSqlliteTransaction>();
            _autoSynObj._allStatus = cont.GetInstance<StatusAllOperationModel>();
            _autoSynObj._exceptionController = cont.GetInstance<ExceptionController>();
            _bec = cont.GetInstance<BlockingEventController>();
            _autoSync = new AutoSyncToTime(_autoSynObj);


        }
        
        public void StartAutoSyncToTime()
        {
            _autoSync.Start();
        }

        public void AbortAutoSyncToTime()
        {
            _autoSync.StopRunningAutoSync();
        }

        public bool isRunningAutoSync()
        {
            return true;
        }

    }
}
