﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;

using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.delete;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.paste;
using NewHH_SoftClientWPF.contoller.progress;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.controlmodel;
using NewHH_SoftClientWPF.mvvm.model.renameJsonModel;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using Newtonsoft.Json;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.sync
{
    public class FirstSyncFilesController
    {
        
        private WebDavClientController _webDavClient;
        //Главное подключение к серверу
        //private MqClientController _mqclientController;
        //работа с кэшем в базе данных
        private SqliteController _sqlLiteController;
        private SupportServerTopicModel _supportTopicModel;
        private UpdateNetworkJsonController _updateNetworkJsonController;
        //приходиться передавать ActiveMq через аргумент т.к simpleInjection не дает этого сделать
        private ObserverNetwokSendJson _observer;
        //Статус открыто окно копирования или нет
        private CopyViewModel _copyViewModel;
        //само окно
        private CopyMainWindow _copyWindows;
        //его модель 
        private CopyMainWindowViewModel _copyMainWIndowsModel;
        private PasteFilesController _pasteFilesController;
        private PasteFolderController _pasteFolderController;
        private DeleteController _deleteController;

        private UpdateFormCopyWindowsController _copyForm;
        private CancellationController _cancellationController;
        private WebDavClientExist _webDavExist;
        private StatusAllOperationModel _allStatus;
        private  WebDavClientExist _webDavClientExist;
        private ConvertIconType _convertIconType;
        private ViewFolderViewModel _viewModelViewFolder;
        private MainWindowViewModel _viewModel;

        public FirstSyncFilesController(Container cont)
        {
            
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _webDavClient = cont.GetInstance<WebDavClientController>();
            _supportTopicModel = new SupportServerTopicModel();
            _updateNetworkJsonController = cont.GetInstance<UpdateNetworkJsonController>();
            _observer = cont.GetInstance<ObserverNetwokSendJson>();
            _copyViewModel = cont.GetInstance<CopyViewModel>();

            _copyMainWIndowsModel = cont.GetInstance<CopyMainWindowViewModel>();
            _pasteFilesController = cont.GetInstance<PasteFilesController>();
            _pasteFolderController = cont.GetInstance<PasteFolderController>();
            _deleteController = cont.GetInstance<DeleteController>();

            _copyForm = cont.GetInstance<UpdateFormCopyWindowsController>();
            _cancellationController = cont.GetInstance<CancellationController>();
            _webDavExist = cont.GetInstance<WebDavClientExist>();
            _allStatus = cont.GetInstance<StatusAllOperationModel>();
            _webDavClientExist = cont.GetInstance<WebDavClientExist>();
            _convertIconType = cont.GetInstance<ConvertIconType>();
            _viewModelViewFolder = cont.GetInstance<ViewFolderViewModel>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();

        }


        public void Start(MqClientController _mqclientController, ActiveMqSubscribleModel activeMqSubscrible)
        {

           Task.Run(() => StartGetList(_mqclientController , activeMqSubscrible));
        }

        int count2 = 0;
        private void StartGetList(MqClientController _mqclientController, ActiveMqSubscribleModel activeMqSubscrible)
        {

            while (true)
            {
                Thread.Sleep(1000);

                if(activeMqSubscrible.isSubscrible)
                {

                    // 0 - username
                    // 1 - password
                    string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();
                    long versionDataBases = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient();
                    long versionRowsBases = _sqlLiteController.getSelect().getSqlLiteVersionRowsClient();
                    serverGetList(_mqclientController, usernameAndPassword, versionDataBases , versionRowsBases);

                    break;

                }
                else
                {
                    Console.WriteLine("FristSyncFilesController->StartGetList: Сканирование запуска SubScrible попытка " + count2);
                }

                if (count2 == 100)
                {
                    Console.WriteLine("FristSyncFilesController->StartGetList: Критическая ошибка не произошла подписка на события в ActiveMQ!!!");
                    break;
                }


                count2++;

                
            }
            
        }



        private void serverGetList(MqClientController _mqclientController , string[] usernameAndPassword , long versionDatabases , long versionRows)
        {
            //GETLIST - получить актуальную базу данных с сервера
            ServerTopicModel ServerModel = _supportTopicModel.CreateModelTopic("GETLIST", usernameAndPassword, versionDatabases , versionRows);
            string json = JsonConvert.SerializeObject(ServerModel);
            Console.WriteLine("FristSyncFilesController->StartGetList: отправка фалага GETLIST");
            _mqclientController.SendMqJsonServer(json);
           
        }


       
        //перечитать всю базу данных  и отправить на сервер (если сервер ответил что его база пустая)
        public void ResponceServerEmptyBases()
        {
            string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();
            //текущая версия +1
            long version = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient() + 1;
            _webDavClient.getListWebDavAsync(_updateNetworkJsonController, version, usernameAndPassword[0], _supportTopicModel,  _sqlLiteController);
        }

        //перечитать всю базу данных и отправить на сервер (если счетчик сказал, что пора обновить всю базу)
        public void ResponceServerScheduleDubbing()
        {

            pcSaveChangeCloud();
            //во временную таблицу
            copyPcSaveInTemp(_sqlLiteController);

            string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();
            //текущая версия +1
            long version = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient() + 1;

            _webDavClient.getListWebDavAsync(_updateNetworkJsonController, version, usernameAndPassword[0], _supportTopicModel , _sqlLiteController);
        }

        private void pcSaveChangeCloud()
        {
            Task.Run(() => 
            {
                ExistSavePcEvent pcEvent = new ExistSavePcEvent(_convertIconType, _sqlLiteController);
                pcEvent.changePcSaveCloud(_viewModel.NodesView.Items, _viewModelViewFolder.NodesView.Items);
            });
           
        }

        private void copyPcSaveInTemp(SqliteController sqliteController)
        {
            sqliteController.getInsert().CopySavePcInTemp();
        }

        public void renameServer(SortableObservableCollection<FolderNodes> TreeViewNode , SqliteController  _sqlliteController ,string oldFolderName)
        {
            //0 с новым именем
            FolderNodes original = TreeViewNode[0];
            string oldLocation = String.Copy(original.Location);
            List<FileInfoModel> renameList = _sqlliteController.UpdateRenameName(original.Row_id, original.FolderName);
            
            //логин пароль от webdav
            string[] usernameAndPassword = _sqlliteController.getSelect().GetUserSqlite();
            string username = usernameAndPassword[0];
            string password = usernameAndPassword[1];
            //область куда будем отправлять данные т.е папка где содержится файл для переименования
            FileInfoModel pasteLocation = _sqlliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(original.ParentID);

            //подготовка данных для работы внутри метода RenameWebDavClient
            RenameWebDavModel renameModel = new RenameWebDavModel();
           
            renameModel._updateNetworkJsonController = _updateNetworkJsonController;
            renameModel._sqliteController = _sqlliteController;
            renameModel.username = username;
            renameModel.password = password;
            renameModel.pasteLocation = pasteLocation.location;
            renameModel.pasteName = original.FolderName;
            renameModel.oldFolderName = oldFolderName;
            renameModel.oldLocation = oldLocation;
            renameModel._webDavClientExist = _webDavExist;
            renameModel.allStatus = _allStatus;
            renameModel._stackUpdateTreeViewIcon = new StackUpdateTreeViewIcon(_convertIconType);
            renameModel.rootNodesListView = _viewModelViewFolder.NodesView.Items;
            renameModel.rootNodesTreeView = _viewModel.NodesView.Items[0];


            _webDavClient.RenameWebDavClient(renameModel,  renameList);
        }

        //перечитать всю базу данных не удаляет старую пока откл т.к перенесли в режим авто сканирование AutoSync
        public void ResponceServerScheduleNoDeleteBases(bool[] isRunningAllUpdateNoDelete)
        {
            //CancellationTokenSource cts = new CancellationTokenSource();
           // _cancellationController.addWarehouse(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID, cts);


           // string[] usernameAndPassword = _sqlLiteController.GetUserSqlite();
            //текущая версия +1
           // long version = _sqlLiteController.getSqlLiteVersionDataBasesClient() + 1;

           // _webDavClient.getListWebDavAsyncNoDeleteBases(_updateNetworkJsonController, version, usernameAndPassword[0], _supportTopicModel, _sqlLiteController , isRunningAllUpdateNoDelete , _cancellationController);
        }

        public async Task startAsyncDelete(FolderNodes[] allDeletenodes)
        {
            bool firstElements = false;
            FolderNodes firstdeleteFoldersNodes = null;

            for (int y = 0; y < allDeletenodes.Length; y++)
            {
                if (firstElements == false)
                {
                    if (allDeletenodes[y] != null)
                    {
                        firstElements = true;
                        firstdeleteFoldersNodes = allDeletenodes[y];
                        break;
                    }

                }

            }

            string Event = "DELETE";
            DeleteTask(firstdeleteFoldersNodes, allDeletenodes, Event);

        }


        public void DeleteTask(FolderNodes deleteFoldersNodes , FolderNodes[] allDeleteFiles, string Event)
        {
            string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();
            DeleteFilesAsyncModel DeleteModel = new DeleteFilesAsyncModel();

            DeleteModel._sqlLiteController = _sqlLiteController;
            DeleteModel.newVersionBases = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient() + 1;
            DeleteModel.updateNetworkJsonController = _updateNetworkJsonController;
            DeleteModel.username = usernameAndPassword[0];
            DeleteModel.password = usernameAndPassword[1];
            DeleteModel.supportTopicModel = _supportTopicModel;
            DeleteModel.allDeleteFiles = allDeleteFiles;
            DeleteModel.location = deleteFoldersNodes.Location;
            DeleteModel.type = deleteFoldersNodes.Type;
            DeleteModel._webdavClientExist = _webDavClientExist;
            DeleteModel.status = new CopyViewModel();
            DeleteModel.deleteRootFolder = false;



            openWormWindowsOpenTaskDelete(deleteFoldersNodes.FolderName);
            DeleteModel.progress = new ProgressToDelete(_copyMainWIndowsModel, _copyForm, _copyWindows, _copyViewModel).updateProgressToDelete();
            
         
            DeleteModel.progressLabel = "Поиск файлов";
            bool isMove = false;

            //null - ждет IProgress для реализации выполнения удаления
            //null - последний это CopyViewModel status
            //null - передаем статус удаления(для уведомления IProgress когда закончится удаление)
            //true - не удалять / false - удалять
            _deleteController.StartDelete(DeleteModel, Event, isMove);
        }

        public void DeleteTaskNoFormNoNewThread(FolderNodes deleteFoldersNodes, FolderNodes[] allDeleteFiles, string Event)
        {
            string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();
            DeleteFilesAsyncModel DeleteModel = new DeleteFilesAsyncModel();

            DeleteModel._sqlLiteController = _sqlLiteController;
            DeleteModel.newVersionBases = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient() + 1;
            DeleteModel.updateNetworkJsonController = _updateNetworkJsonController;
            DeleteModel.username = usernameAndPassword[0];
            DeleteModel.password = usernameAndPassword[1];
            DeleteModel.supportTopicModel = _supportTopicModel;
            DeleteModel.allDeleteFiles = allDeleteFiles;
            DeleteModel.location = deleteFoldersNodes.Location;
            DeleteModel.type = deleteFoldersNodes.Type;
            DeleteModel._webdavClientExist = _webDavClientExist;
            DeleteModel.status = new CopyViewModel();
            DeleteModel.deleteRootFolder = false;



           // openWormWindowsOpenTaskDelete(deleteFoldersNodes.FolderName);
            DeleteModel.progress = new ProgressToDelete(_copyMainWIndowsModel, _copyForm, _copyWindows, _copyViewModel).updateProgressToDelete();


            DeleteModel.progressLabel = "Поиск файлов";
            bool isMove = false;

            //null - ждет IProgress для реализации выполнения удаления
            //null - последний это CopyViewModel status
            //null - передаем статус удаления(для уведомления IProgress когда закончится удаление)
            //true - не удалять / false - удалять
            _deleteController.StartDeleteNoNewThread(DeleteModel, Event, isMove);
        }


        public async Task PasteServer(PasteServerModel pasteServerModel , string Event)
        {
            FolderNodes folderNodes = pasteServerModel.folderNodes;
            FolderNodes pasteNodes = pasteServerModel.pasteNodes;
            bool replace = pasteServerModel.replace;
            Dictionary<FolderNodes, bool> allCopyNodes = pasteServerModel.allCopyNodes;


            string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();
            PasteFilesAsyncModel PasteModel = new PasteFilesAsyncModel();

            PasteModel.convertIcon = _convertIconType;
            //текущая версия +1
            PasteModel.newVersionBases = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient() + 1;
            PasteModel.username = usernameAndPassword[0];
            PasteModel.password = usernameAndPassword[1];
            PasteModel.supportTopicModel = _supportTopicModel;
            PasteModel._sqlLiteController = _sqlLiteController;
            PasteModel.type = folderNodes.Type;
            PasteModel.copyFolderName = folderNodes.FolderName;
            //обрабатываем пример когда перемещаем файл
            PasteModel.copyLocation = ConverteCopyLocationFilesTo( folderNodes,  _sqlLiteController);
            PasteModel.pasteFolder = pasteNodes;
            PasteModel.status = new CopyViewModel();
            PasteModel.replace = replace;
            PasteModel.updateNetworkJsonController = _updateNetworkJsonController;
            PasteModel.allCopyNodes = allCopyNodes;
            PasteModel.copyOrCutRootLocation = pasteServerModel.copyOrCutRootLocation;
            PasteModel._webDavClientExist = _webDavClientExist;
            PasteModel._exceptionController = pasteServerModel._exceptionController;

            try
            {
                PasteModel.allPasteNodes = pasteServerModel.allPasteNodes;

                openWormWindowsOpenTaskPaste(folderNodes, pasteNodes);
                PasteModel.progress = new ProgressToCopy(_copyMainWIndowsModel, _copyForm, _copyWindows, _copyViewModel).updateProgressToCopy();

              
                    if(Event.Equals("PASTE"))
                    {
                        _pasteFolderController.StartFolderPaste(PasteModel, Event);
                    }
                    else
                    {
                        _pasteFolderController.StartFolderMove(PasteModel, Event);
                    }
                   
              
            }
            catch (System.Data.SQLite.SQLiteException g)
            {
                Console.WriteLine("FirstSyncFilesController->PasteServer: Ошибка Проблема копирования файлов ");
                Console.WriteLine(g.ToString());
                closeWindowsCopy(_copyWindows, _copyViewModel);
            }
            catch (AggregateException e)
            {
                Console.WriteLine("FirstSyncFilesController->PasteServer: Ошибка Проблема копирования файлов ");
                Console.WriteLine(e.ToString());
                closeWindowsCopy(_copyWindows, _copyViewModel);
            }
            catch (WebDAVClient.Helpers.WebDAVException s)
            {
                Console.WriteLine("FirstSyncFilesController->PasteServer: Ошибка Проблема копирования файлов ");
                Console.WriteLine(s.ToString());
                closeWindowsCopy(_copyWindows, _copyViewModel);
            }
            catch (System.InvalidOperationException d)
            {
                Console.WriteLine("FirstSyncFilesController->PasteServer: Ошибка Проблема копирования файлов ");
                Console.WriteLine(d.ToString());
                closeWindowsCopy(_copyWindows, _copyViewModel);
            }
            catch (System.NullReferenceException d)
            {
                Console.WriteLine("FirstSyncFilesController->PasteServer: Ошибка Проблема копирования файлов ");
                Console.WriteLine(d.ToString());
                closeWindowsCopy(_copyWindows, _copyViewModel);
            }


        }

        private string ConverteCopyLocationFilesTo(FolderNodes folderNodes , SqliteController _sqlLiteController)
        {
            string copyLocation = "";
            if(staticVariable.Variable.isFolder(folderNodes.Type) != true)
            {
               // long parentId = folderNodes.ParentID;
                copyLocation = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(folderNodes.ParentID).location;
            }
            else
            {
                return folderNodes.Location;
            }

            return copyLocation;
        }


        //происходит когда мы удаляем/перемещаем/создаем - в дереве какой-то event
        public async Task RensponceServerSinglEvent(FolderNodes folderNodes , string Event , FolderNodes pasteNodes , bool replace)
        {
            string[] usernameAndPassword = _sqlLiteController.getSelect().GetUserSqlite();


         
            //нужно реализовать копирование файлов с заменой
            if (Event.Equals("PASTE"))
            {
                


            }
            else if(Event.Equals("MOVE"))
            {

                PasteFilesAsyncModel PasteModel = new PasteFilesAsyncModel();
                //текущая версия +1
                PasteModel.newVersionBases = _sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient() + 1;
                PasteModel.username = usernameAndPassword[0];
                PasteModel.password = usernameAndPassword[1];
                PasteModel.supportTopicModel = _supportTopicModel;
                PasteModel._sqlLiteController = _sqlLiteController;
                PasteModel.type = folderNodes.Type;
                PasteModel.copyFolderName = folderNodes.FolderName;
                PasteModel.copyLocation = folderNodes.Location;
                PasteModel.pasteFolder = pasteNodes;
                PasteModel.status = new CopyViewModel();
                PasteModel.replace = replace;
                PasteModel.updateNetworkJsonController = _updateNetworkJsonController;
                PasteModel._webDavClientExist = _webDavClientExist;
                PasteModel.convertIcon = _convertIconType;

                openWormWindowsOpenTaskMove(folderNodes, pasteNodes);

                try
                {
                  

                  
                    PasteModel.progress = new ProgressToMove(_copyMainWIndowsModel, _copyForm, _copyWindows, _copyViewModel).updateProgressToMove();


                    if (staticVariable.Variable.isFolder(folderNodes.Type))
                    {
                        CopyViewModel status = new CopyViewModel();
                        //сам перенос
                        _pasteFolderController.StartFolderMove(PasteModel, Event);

                    }
                    else
                    {
                        CopyViewModel status = new CopyViewModel();

                        //cам перенос
                        _pasteFilesController.StartFilesMove(PasteModel , Event);

                    }
                }
                catch (System.Data.SQLite.SQLiteException g)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(g.ToString());
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }
                catch (AggregateException e)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(e.ToString());
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }
                catch (WebDAVClient.Helpers.WebDAVException s)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(s.ToString());
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }
                catch (System.InvalidOperationException d)
                {
                    Console.WriteLine("WebDavClientController->getPasteFolderWebDavAsync: Ошибка Проблема копирования файлов ");
                    Console.WriteLine(d.ToString());
                    closeWindowsCopy(_copyWindows, _copyViewModel);
                }

            }
            

            
        }

        private void openWormWindowsOpenTaskMove(FolderNodes folderNodes,  FolderNodes pasteNodes)
        {
            openWindowsCopy(_copyWindows, _copyViewModel);
            _copyMainWIndowsModel.title = "Процесс переноса";
            _copyMainWIndowsModel.status = "Подготовка";
            _copyMainWIndowsModel.whence = pasteNodes.FolderName;
            _copyMainWIndowsModel.where = folderNodes.FolderName;
        }

        private void openWormWindowsOpenTaskPaste(FolderNodes folderNodes, FolderNodes pasteNodes)
        {
            openWindowsCopy(_copyWindows, _copyViewModel);
            if(_copyMainWIndowsModel != null)
            {
                _copyMainWIndowsModel.title = "Процесс копирования";
                _copyMainWIndowsModel.status = "Подготовка";
                _copyMainWIndowsModel.whence = pasteNodes.FolderName;
                _copyMainWIndowsModel.where = folderNodes.FolderName;
            }
           
        }

        private void openWormWindowsOpenTaskDelete(string deleteFolderNodes)
        {
            openWindowsCopy(_copyWindows, _copyViewModel);
            _copyMainWIndowsModel.title = "Процесс удаления";
            _copyMainWIndowsModel.status = "Подготовка";
            _copyMainWIndowsModel.whence = "Сервер";
            _copyMainWIndowsModel.where = deleteFolderNodes;
        }

       


        
        //корявость Simple Injector не дает инсертить зависимость напрямую в _observer
        public void startFirstSync(MqClientController _mqclientController, UpdateFormMainController updateFormMainController, ActiveMqSubscribleModel activeMqSubscrible)
        {
            _observer.injectActiveMq(_mqclientController);
            _observer.injectUpdateFormMainController(updateFormMainController);
            Start(_mqclientController , activeMqSubscrible);
          
            Console.WriteLine("FirstSyncFilesController -> startFirstSync: Запуск первичной синхронизации");
        }

        private void openWindowsCopy(CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {
            if(_copyForm != null)
            {
                _copyForm.updateOpenWindowsCopy();
            }
           
        }

        private void closeWindowsCopy(CopyMainWindow _copyWindows, CopyViewModel _copyViewModel)
        {
            if(_copyWindows != null)
            {
                _copyForm.updateCloseWindowsCopy();
            }
          
        }





    }

}
