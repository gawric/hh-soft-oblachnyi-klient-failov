﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sorted
{
    public class SortedArrayToRevers
    {

        public FileInfoModel[] sortedArrayFileInfoToRevers(FileInfoModel[] arr)
        {
            if(arr != null)Array.Reverse(arr);
            
            return arr;
        }
    }
}
