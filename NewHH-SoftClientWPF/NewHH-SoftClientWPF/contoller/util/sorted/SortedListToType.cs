﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sorted
{
    class SortedListToType
    {

        public List<FileInfoModel> sortedType(List<FileInfoModel> original)
        {
            List<FileInfoModel> sortedList = new List<FileInfoModel>();


            //добавляем только folder
            addOnlyFolder(ref original, ref sortedList);
            //только файлы
            addOnlyFiles(ref original, ref sortedList);

            original = null;

            return sortedList;
        }
        private void addOnlyFiles(ref List<FileInfoModel> original, ref List<FileInfoModel> sortedList)
        {
            for (int d = 0; d < original.Count; d++)
            {
                if (original[d] != null)
                {
                    if (staticVariable.Variable.isFolder(original[d].type) != true)
                    {
                        sortedList.Add(original[d]);
                        // Console.WriteLine("Получение данных из базы тип files>> " + original[d].row_id);
                    }

                }

            }
        }
        private void addOnlyFolder(ref List<FileInfoModel> original , ref List<FileInfoModel> sortedList)
        {
            for (int j = 0; j < original.Count; j++)
            {
                if (original[j] != null)
                {
                    if (staticVariable.Variable.isFolder(original[j].type))
                    {
                        sortedList.Add(original[j]);
                        // Console.WriteLine("Получение данных из базы тип folder*** "+original[j].row_id);
                    }
                }

            }
        }
    }
}
