﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner.reloadAllTreeView.support
{
    public class InsertRoot
    {
        private SqliteController sqlcont;

        public InsertRoot(SqliteController sqlcont)
        {
            this.sqlcont = sqlcont;
        }
        public void insertRootDirectory(ref FolderNodes nodes, ref FileInfoModel s, ref CreateModel createModel)
        {
            List<FileInfoModel> children = sqlcont.getSelect().GetSqlLiteParentIDFileInfoList(s.row_id);
            nodes = convert(ref nodes, ref s);

            //проверка есть ли дети
            if (children.Count > 0)
            {
                if (nodes.Children.Count > 0)
                {
                    run(ref nodes, ref children, ref createModel);
                }
                else
                {
                    runDisk(ref nodes, ref children, ref createModel);
                }


            }
        }

        private void runDisk(ref FolderNodes nodes, ref List<FileInfoModel> children, ref CreateModel createModel)
        {
            //если диск пустой у него нет детей
            //и мы его пересканируем т.е возможно дети у него появились
            //запускаем цикл присваивания
            if (nodes.FolderName.Equals("Disk"))
            {
                List<FileInfoModel> mysqlmodel = children;

                if (staticVariable.Variable.isFolder(nodes.Type) == true)
                {
                    new MassUpdate(sqlcont).MassivUpateReleases(children, nodes, createModel);
                }
            }
        }
        private void run(ref FolderNodes nodes , ref List<FileInfoModel> children , ref CreateModel createModel)
        {
            //есть ли открытые дети то не будет в имени загрузка
            if (nodes.Children[0].FolderName.Equals("Загрузка") != true)
            {
                List<FileInfoModel> mysqlmodel = children;

                if (staticVariable.Variable.isFolder(nodes.Type) == true)
                {
                    new MassUpdate(sqlcont).MassivUpateReleases(children, nodes, createModel);
                }
            }
        }

        private FolderNodes convert(ref FolderNodes nodes , ref FileInfoModel s)
        {
            nodes.Row_id = s.row_id;
            nodes.ParentID = s.parent;
            nodes.FolderName = s.filename;
            nodes.Location = s.location;
            nodes.VersionUpdateRows = s.versionUpdateRows;
            nodes.Type = s.type;
            nodes.changeDate = s.changeDate;
            nodes.changeRows = s.changeRows;
            nodes.sizebyte = s.sizebyte.ToString();
            nodes.lastOpenDate = s.lastOpenDate;


            return nodes;
        }

    }
}
