﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner.reloadAllTreeView.support
{
    public class MassUpdate
    {

        private SqliteController sqlcont;

        public MassUpdate(SqliteController sqlcont)
        {
            this.sqlcont = sqlcont;
        }

        public void MassivUpateReleases(List<FileInfoModel> children, FolderNodes folderNodesTree, CreateModel createModel)
        {


            for (int a = 0; a < folderNodesTree.Children.Count; a++)
            {
                string locationTree = folderNodesTree.Children[a].Location;
                FileInfoModel mysqlNodes = searchInTreeMysql(children, locationTree);

                if (mysqlNodes != null)
                {

                    FolderNodes oldTreeNodes = folderNodesTree.Children[a];
                    runUpdate(ref oldTreeNodes, ref mysqlNodes);
                }
                else
                {
                    deleteIndex(ref folderNodesTree, ref a);
                }

            }

            checkNodes(ref children, ref folderNodesTree, ref createModel);

        }

        private void runUpdate(ref FolderNodes oldTreeNodes , ref FileInfoModel mysqlNodes)
        {
            if (oldTreeNodes.Type != null)
            {
                if (staticVariable.Variable.isFolder(oldTreeNodes.Type)) updateOldNodes(oldTreeNodes, mysqlNodes);
            }
            else
            {
                update(ref oldTreeNodes, ref mysqlNodes);
            }
        }

        private void update(ref FolderNodes oldTreeNodes, ref FileInfoModel mysqlNodes)
        {
            oldTreeNodes.Type = mysqlNodes.type;

            if (oldTreeNodes.Type != null)
            {
                if (staticVariable.Variable.isFolder(oldTreeNodes.Type))
                {

                    updateOldNodes(oldTreeNodes, mysqlNodes);
                }
            }
        }

        private void checkNodes(ref List<FileInfoModel> children, ref FolderNodes folderNodesTree, ref CreateModel createModel)
        {
            for (int k = 0; k < children.Count; k++)
            {
                //проходим по всем детям оригинала
                //nodes[f].children
                //и сравниваем их с данными из базы
                if (staticVariable.Variable.isFolder(children[k].type))
                {
                    FolderNodes check = searchInMysqlTree(folderNodesTree.Children, children[k].location);
                    bool children2 = sqlcont.getSelect().existSqlLiteParentNodes(children[k].row_id);

                    if (check == null)
                    {
                        FolderNodes newNodes = createModel.convertFileInfoRecursiveToFolderNodes(children[k]);

                        insertLoadingNodes(ref children2 , ref newNodes);
                        treeNodeAdd(folderNodesTree , newNodes);
                    }



                }

            }
        }


        private void treeNodeAdd(FolderNodes treeNode, FolderNodes newNode)
        {
            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {
                    treeNode.Children.Add(newNode);
                });
            }

        }

        private void insertLoadingNodes(ref bool children2, ref FolderNodes newNodes)
        {
            //если у ребенка есть тоже дети вставляем туда заглушку
            if (children2 == true)
            {
                FolderNodes loadNodes = new FolderNodes();
                loadNodes.FolderName = "Загрузка";
                loadNodes.Type = "txt";
                loadNodes.Location = "Загрузка";

                newNodes.Children = new SortableObservableCollection<FolderNodes>();
                newNodes.Children.Add(loadNodes);

            }
        }

        private FolderNodes searchInMysqlTree(ObservableCollection<FolderNodes> treeChildren, string mysqllocation)
        {
            FolderNodes check = null;

            for (int g = 0; g < treeChildren.Count; g++)
            {
                FolderNodes childrenTree = treeChildren[g];

                if (childrenTree.Location.Equals(mysqllocation) == true)
                {
                    check = childrenTree;
                }

            }

            return check;
        }


        private void deleteIndex(ref FolderNodes folderNodesTree, ref int a)
        {
            //удаление элемента из дерева
            if (folderNodesTree.Children[a].FolderName.Equals("Загрузка") != true)
            {
                DeleteNodesIndex(folderNodesTree.Children, a);
            }
        }

        private void DeleteNodesIndex(ObservableCollection<FolderNodes> treeNode, int index)
        {
            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                {

                    treeNode.RemoveAt(index);

                });
            }


        }


        private void updateOldNodes(FolderNodes oldTreeNodes, FileInfoModel mysqlNodes)
        {
            oldTreeNodes.changeRows = mysqlNodes.changeRows;
            oldTreeNodes.FolderName = mysqlNodes.filename;

            oldTreeNodes.ParentID = mysqlNodes.parent;
            oldTreeNodes.Type = mysqlNodes.type;
            oldTreeNodes.VersionUpdateRows = mysqlNodes.versionUpdateRows;
            oldTreeNodes.Row_id = mysqlNodes.row_id;
            oldTreeNodes.Location = mysqlNodes.location;

            oldTreeNodes.changeDate = mysqlNodes.changeDate;
            oldTreeNodes.createDate = mysqlNodes.createDate;
            oldTreeNodes.lastOpenDate = mysqlNodes.lastOpenDate;
            oldTreeNodes.sizebyte = mysqlNodes.sizebyte.ToString();
        }



        private FileInfoModel searchInTreeMysql(List<FileInfoModel> MySqlNodes, string treelocation)
        {
            FileInfoModel check = null;

            for (int g = 0; g < MySqlNodes.Count; g++)
            {
                FileInfoModel treechildren = MySqlNodes[g];

                if (staticVariable.Variable.isFolder(treechildren.type))
                {
                    if (treechildren.location.Equals(treelocation) == true)
                    {
                        //check = treechildren;
                        check = new FileInfoModel();

                        check.row_id = treechildren.row_id;
                        check.parent = treechildren.parent;
                        check.saveTopc = treechildren.saveTopc;
                        check.sizebyte = treechildren.sizebyte;
                        check.user_id = treechildren.user_id;
                        check.versionUpdateRows = treechildren.versionUpdateRows;
                        check.location = treechildren.location;
                        check.changeRows = treechildren.changeRows;
                        check.filename = treechildren.filename;

                        MySqlNodes.Remove(treechildren);
                    }
                }


            }

            return check;
        }


    }
}
