﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.autoSync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.scanner.autoscan
{
    public class ScannerWebDavAndSendToServer
    {
        private ExceptionController _exceptionController;

        public ScannerWebDavAndSendToServer(ExceptionController exceptionController)
        {
            _exceptionController = exceptionController;
        }


        //пересканируем файлы webdav без очистки
        public void TrainingStartScan(AutoSyncToTimeObj _autoSynObj , bool[] isRunningAllUpdateNoDelete)
        {


            IProgress<double> progress = createProgress(_autoSynObj);

            Task.Run(async () =>
            {
               
                updateIsRunning(_autoSynObj , isRunningAllUpdateNoDelete, true);

                await Start(_autoSynObj , progress);

                updateIsRunning(_autoSynObj , isRunningAllUpdateNoDelete, false);

                destroyCancellation(_autoSynObj);
            });




        }

      

        private Progress<double> createProgress(AutoSyncToTimeObj _autoSynObj)
        {
            Progress<double> progress = new Progress<double>();
            //Срабатывает когда WebDavClient наполняется 2500 файлов
            progress.ProgressChanged += (s, e) =>
            {
                _autoSynObj._viewModel.pg = e;
            };

            return progress;
        }

        private async Task Start(AutoSyncToTimeObj _autoSynObj , IProgress<double> progress)
        {
            try
            {
                WebDavGetAllList getScanRootFolder = new WebDavGetAllList();
                await getScanRootFolder.NoClearBasesRescanWebDavAllListStart(_autoSynObj, progress);
            }
            catch(System.NullReferenceException w)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "ScannerWebDavAndSendTOserver-> Start NullReferenceException", w.ToString());
            
            }
            catch (System.ArgumentException w)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "ScannerWebDavAndSendTOserver-> Start ArgumentException", w.ToString());
            }
            
        }
        private void destroyCancellation(AutoSyncToTimeObj _autoSynObj)
        {
            //уничтожение
            _autoSynObj._cancellationController.removeObjectWarehouse(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID);
        }
        private void updateIsRunning(AutoSyncToTimeObj _autoSynObj, bool[] isRunningAllUpdateNoDelete, bool isRunning)
        {
            isRunningAllUpdateNoDelete[0] = isRunning;

            if(isRunning)
            {
                _autoSynObj._viewModel.OnChangeImage(staticVariable.IconVariable.iconMenuStartAutoScan,
                                    staticVariable.IconVariable.iconTypeStop,
                                    staticVariable.IconVariable.itemMenuStartAutoScan,
                                    staticVariable.IconVariable.textMenuStopAutoScan);
            }
            else
            {
                _autoSynObj._viewModel.OnChangeImage(staticVariable.IconVariable.iconMenuStartAutoScan,
                                    staticVariable.IconVariable.iconTypeStart,
                                    staticVariable.IconVariable.itemMenuStartAutoScan,
                                    staticVariable.IconVariable.textMenuStartAutoScan);
            }
        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }

    }
}
