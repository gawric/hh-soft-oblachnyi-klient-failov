﻿using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner
{
    public interface ISRIF
    {
        List<System.IO.FileInfo> GetOnlyFiles();
        void StartScan(List<HrefModel> arrHref);
        List<System.IO.DirectoryInfo> GetOnlyFolder();
    }
}
