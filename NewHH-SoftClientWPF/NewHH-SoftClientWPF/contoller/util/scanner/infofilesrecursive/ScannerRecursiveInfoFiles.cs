﻿using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner
{
    public class ScannerRecursiveInfoFiles: ISRIF
    {
        private Dictionary<int, System.IO.DirectoryInfo> tempDictFolder;
        private Dictionary<int, System.IO.FileInfo> tempDictFiles;
        private Dictionary<int, System.IO.DirectoryInfo> tempFolder;
        private Dictionary<int , System.IO.FileInfo> tempFiles;
        List<int> delList;

        public ScannerRecursiveInfoFiles()
        {
            tempDictFiles = new Dictionary<int, System.IO.FileInfo>();
            tempDictFolder = new Dictionary<int, System.IO.DirectoryInfo>();
            tempFolder = new Dictionary<int, System.IO.DirectoryInfo>();
            tempFiles = new Dictionary<int, System.IO.FileInfo>();
        }
        public List<System.IO.FileInfo> GetOnlyFiles()
        {
            return tempFiles.Values.ToList<System.IO.FileInfo>();
        }

        public List<System.IO.DirectoryInfo> GetOnlyFolder()
        {
            SearchDublication();
            DeleteDublication();
            return tempFolder.Values.ToList<System.IO.DirectoryInfo>();
        }
        private void SearchDublication()
        {
            delList  = new List<int>();

            foreach (KeyValuePair<int, DirectoryInfo> kvp in tempFolder)
            {
                ForeSearch(kvp.Value, ref delList);
            }
        }

        private void DeleteDublication()
        {
            foreach(int itemDel in delList)
            {
                tempFolder.Remove(itemDel);
            }
        }

        private void ForeSearch(DirectoryInfo di , ref List<int> delList)
        {
            int dublcount = 0;
            foreach (KeyValuePair<int, DirectoryInfo> kvp in tempFolder)
            {
                if(kvp.Value.FullName.Equals(di.FullName))
                {
                    dublcount++;
                    if (dublcount > 1) delList.Add(kvp.Key);
         
                }
            }
            
        }
        public void StartScan(List<HrefModel> arrHref)
        {
            tempFolder.Clear();
            tempFiles.Clear();
            bool IsFirst = false;

            foreach (HrefModel item in arrHref)
            {
                WalkDirectoryTree(new System.IO.DirectoryInfo(item.href) , ref IsFirst);
            }
           // PrintScan();
            Debug.Print("ScannerRecursiveInfoFiles->WalkDirectoryTree: Завершили полное сканирование");
        }
        private void PrintScan()
        {
            Debug.Print("ScannerRecursiveInfoFiles->WalkDirectoryTree: Прошли по каталогу получили данные");
            foreach (KeyValuePair<int, System.IO.DirectoryInfo> kvp in tempFolder)
            {
                Console.WriteLine("Key = {0}, Value = {1}",
                    kvp.Key, kvp.Value.FullName);
            }

            foreach (KeyValuePair<int, System.IO.FileInfo> kvp in tempFiles)
            {
                Console.WriteLine("Key = {0}, Value = {1}",
                    kvp.Key, kvp.Value.FullName);
            }
        }
        public void WalkDirectoryTree(System.IO.DirectoryInfo root , ref bool isFirst)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;

            try
            {
                    files = root.GetFiles("*.*");
                    if (!isFirst) { AddSinglItem(root); isFirst = true; }

                    AddFiles(files);
                    subDirs = root.GetDirectories();
                    AddListItem(ref subDirs , ref isFirst);
                
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("ScannerRecursiveLocalFolder-> WalkDirectoryTree  " + e);
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                Console.WriteLine("ScannerRecursiveLocalFolder-> WalkDirectoryTree  " + e);
            }
        }

        private void AddFolder(System.IO.DirectoryInfo di, int hashcode)
        {
            if(!tempFolder.ContainsKey(hashcode))
            {
                tempFolder.Add(hashcode, di);

            }
        }
        private void AddFiles(System.IO.FileInfo[] arr)
        {
            foreach(System.IO.FileInfo fi in arr)
            {
                int hashcode = fi.FullName.GetHashCode();
                if(!tempDictFiles.ContainsKey(hashcode))
                {
                    tempDictFiles.Add(hashcode, fi);
                    if (!tempFiles.ContainsKey(hashcode)) tempFiles.Add(hashcode, fi);

                }
            }
        }
        private void AddListItem(ref System.IO.DirectoryInfo[] subDirs , ref bool isFirst)
        {
            foreach (System.IO.DirectoryInfo dirInfo in subDirs)
            {
                AddListItemRecur(dirInfo , ref  isFirst);
            }
        }

        private void AddSinglItem(System.IO.DirectoryInfo dirInfo)
        {
            string fullName = dirInfo.FullName;
            int hashcode = fullName.GetHashCode();

            if (!IsContains(hashcode))
            {
                AddFolder(dirInfo, hashcode);
                AddTempDict(dirInfo, hashcode);
            }
            else
            {
                //Debug.Print("ScannerRecursiveInfoFiles->WalkDirectoryTree: Не сканируем данный папка уже была просканирована!!!!  " + fullName);
            }
        }
        private void AddListItemRecur(System.IO.DirectoryInfo dirInfo, ref bool isFirst)
        {
            string fullName = dirInfo.FullName + "\\";
            int hashcode = fullName.GetHashCode();


            if (!IsContains(hashcode))
            {
                AddFolder(dirInfo, hashcode);
                AddTempDict(dirInfo, hashcode);
                WalkDirectoryTree(dirInfo, ref isFirst);
            }
            else
            {
                //Debug.Print("ScannerRecursiveInfoFiles->WalkDirectoryTree: Не сканируем данный папка уже была просканирована!!!!  " + fullName);
            }
        }

        private bool IsContains(int hashcode)
        {
            return tempDictFolder.ContainsKey(hashcode);
        }
        private void AddTempDict(System.IO.DirectoryInfo di , int hashcode)
        {
            if(!tempDictFolder.ContainsKey(hashcode))
            {
                tempDictFolder.Add(hashcode, di);
            }
        }
 
    }
}
