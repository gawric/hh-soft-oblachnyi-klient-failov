﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel.insertTreeViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner.insertRecursive.support
{
    public class InsertNodesRecursive : IInsertRecursive
    {
        private SqliteController sqlcont;
        private List<FileInfoModel> listFiles;

        public InsertNodesRecursive(SqliteController sqlcont, List<FileInfoModel> listFiles)
        {
            this.listFiles = listFiles;
            this.sqlcont = sqlcont;
        }

        public void insertNodesListView(ObservableCollection<FolderNodes> ListView, long[] parentListViewID, ConvertIconType convertIcon)
        {
            throw new NotImplementedException();
        }

        public void insertNodesRecursive(FolderNodes treeNode, ObservableCollection<FolderNodes> list, int index, InsertTreeViewModel insertObjModel)
        {

            // Console.WriteLine("Имя выполняемого потока   " +treeNode.FolderName + " " +Thread.CurrentThread.Name);

            for (int s = 0; s < listFiles.Count; s++)
            {
                if (treeNode != null)
                {
                    if (listFiles[s] != null)
                    {

                        if (treeNode.Row_id == listFiles[s].parent)
                        {
                            if (listFiles[s].changeRows.Equals("CREATE"))
                            {
                                if (staticVariable.Variable.isFolder(listFiles[s].type))
                                {


                                    //создание нода
                                    FolderNodes newNodes = new FolderNodes();
                                    newNodes.changeRows = listFiles[s].changeRows;
                                    newNodes.FolderName = listFiles[s].filename;

                                    App.Current.Dispatcher.Invoke(delegate
                                    {
                                        newNodes.Icon = insertObjModel.convertIcon.getIconType(listFiles[s].type);
                                    });


                                    newNodes.ParentID = listFiles[s].parent;
                                    newNodes.Type = listFiles[s].type;
                                    newNodes.VersionUpdateRows = listFiles[s].versionUpdateRows;
                                    newNodes.Row_id = listFiles[s].row_id;
                                    newNodes.Location = listFiles[s].location;
                                    newNodes.changeDate = listFiles[s].changeDate;
                                    newNodes.createDate = listFiles[s].createDate;
                                    newNodes.lastOpenDate = listFiles[s].lastOpenDate;
                                    newNodes.sizebyte = listFiles[s].sizebyte.ToString();





                                    if (treeNode.Children == null)
                                    {

                                        newNodes = CheckChildrenNodes(newNodes.Row_id, newNodes);
                                        treeNode.Children = new SortableObservableCollection<FolderNodes>();
                                        TreeNodeAdd(treeNode, newNodes);
                                    }
                                    else
                                    {
                                        newNodes = CheckChildrenNodes(newNodes.Row_id, newNodes);
                                        TreeNodeAdd(treeNode, newNodes);
                                    }

                                    //Проверка на дублирование нодов
                                    insertObjModel.searchDublication.clearDublication(list);

                                }


                            }


                        }

                    }

                }

            }

            if (treeNode.FolderName.Equals("Загрузка") != true)
            {
                for (int f = 0; f < treeNode.Children.Count; f++)
                {
                    insertNodesRecursive(treeNode.Children[f], treeNode.Children, f, insertObjModel);
                }
            }





        }

        //если у переносимого обьекта есть дети, добавляем в него массив
        private FolderNodes CheckChildrenNodes(long parent_id, FolderNodes nodes)
        {
            bool ServerModel = sqlcont.getSelect().existSqlLiteParentNodes(parent_id);

            if (ServerModel == true)
            {
                FolderNodes loadNodes = new FolderNodes();
                loadNodes.FolderName = "Загрузка";
                loadNodes.Type = "txt";

                nodes.Children = new SortableObservableCollection<FolderNodes>();
                nodes.Children.Add(loadNodes);

            }


            return nodes;
        }

        private void TreeNodeAdd(FolderNodes treeNode, FolderNodes newNode)
        {

            App.Current.Dispatcher.Invoke(delegate
            {
                treeNode.Children.Add(newNode);
            });


        }

    }
}
