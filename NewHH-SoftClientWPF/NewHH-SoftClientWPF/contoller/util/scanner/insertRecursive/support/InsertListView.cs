﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel.insertTreeViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.scanner.insertRecursive.support
{
    public  class InsertListView : IInsertRecursive
    {
   
        private List<FileInfoModel> listFiles;

        public InsertListView(List<FileInfoModel> listFiles)
        {
            this.listFiles = listFiles;
        }
        public void insertNodesListView(ObservableCollection<FolderNodes> ListView, long[] parentListViewID, ConvertIconType convertIcon)
        {
            for (int s = 0; s < listFiles.Count; s++)
            {
                //если list не пустой
                if (listFiles[s] != null)
                {
                    //если list принадлежит данной папке
                    if (parentListViewID[0] == listFiles[s].parent)
                    {
                        //если имеет статус на создание
                        if (listFiles[s].changeRows.Equals("CREATE"))
                        {
                            //если в listview нет такого файла (ищем по location)
                            if (checkDuplicate(ListView, listFiles[s].location, convertIcon))
                            {
                                //создание нода закоментировал нужно проверить
                            }
                            else
                            {
                                //создание нода
                                FolderNodes newNodes = new FolderNodes();
                                newNodes.changeRows = listFiles[s].changeRows;
                                newNodes.FolderName = listFiles[s].filename;
                                App.Current.Dispatcher.Invoke(delegate // <--- HERE
                                {
                                    newNodes.Icon = convertIcon.getIconType(listFiles[s].type);
                                });

                                newNodes.ParentID = listFiles[s].parent;
                                newNodes.Type = listFiles[s].type;
                                newNodes.VersionUpdateRows = listFiles[s].versionUpdateRows;
                                newNodes.Row_id = listFiles[s].row_id;
                                newNodes.Location = listFiles[s].location;
                                newNodes.changeDate = listFiles[s].changeDate;
                                newNodes.createDate = listFiles[s].createDate;
                                newNodes.lastOpenDate = listFiles[s].lastOpenDate;
                                newNodes.sizebyte = listFiles[s].sizebyte.ToString();
                                insertListViewNodes(newNodes, ListView);

                            }

                        }


                    }
                }

            }
        }

        public void insertNodesRecursive(FolderNodes treeNode, ObservableCollection<FolderNodes> list, int index, InsertTreeViewModel insertObjModel)
        {
            throw new NotImplementedException();
        }

        //находит есть ли с таким именем в listView файлы
        private bool checkDuplicate(ObservableCollection<FolderNodes> ListView, string location, ConvertIconType convertIcon)
        {
            bool check = false;

            foreach (FolderNodes item in ListView)
            {
                if (item.Location.Equals(location))
                {
                    check = true;
                }
            }

            return check;
        }

        //folderclient - новый массив
        //RootNodes - текущий массив со всеми нодами
        //Добавляет Node в конец дерева
        private void insertListViewNodes(FolderNodes folderclient, ObservableCollection<FolderNodes> ListViewNode)
        {
            //проверка из какого потока запускается данный скрипт
            if (System.Windows.Application.Current != null)
            {
                App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                {
                    try
                    {

                        ListViewNode.Add(folderclient);
                    }
                    catch (System.InvalidOperationException ss)
                    {
                        Console.WriteLine(ss);
                    }

                });
            }
            else
            {

                ListViewNode.Add(folderclient);
            }
        }

    }
}
