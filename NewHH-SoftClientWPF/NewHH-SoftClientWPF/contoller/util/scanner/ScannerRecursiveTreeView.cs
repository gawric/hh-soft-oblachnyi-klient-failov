﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    class ScannerRecursiveTreeView
    {
        private SqliteController sqlcont;
        private ObservableCollection<FolderNodes> nodes;
        private ObservableCollection<FolderNodes> ListСarryOverNodes;

        public ScannerRecursiveTreeView(SqliteController sqlitecont, ObservableCollection<FolderNodes> nodes)
        {
            sqlcont = sqlitecont;
            this.nodes = nodes;
            ListСarryOverNodes = new ObservableCollection<FolderNodes>();
        }

        public void UpdateNodesRecursive(FolderNodes treeNode , ObservableCollection<FolderNodes> list, int[] index , string operation , ConvertIconType convertIcon)
        {

            // System.Diagnostics.Debug.WriteLine("Рекурсивный цикл: update " + treeNode.FolderName);

           // Console.WriteLine("ScannerRecursiveTreeView -> UpdateNodesRecursive -> Обновление данных в дереве т.к пришло сообщение от сервера!");

            if(operation.Equals("CheckNodes"))
            {
                //проверяет текущие данные из базы 
                //и текущие данные из дерева
                //сравнивает их на изменения (Удаление/Переименование/Перенос)
                CheckUpdateDataView(treeNode, list, index , convertIcon);

            }
            else
            {
                //проверяет массив ListСarryOverNodes - является хранилищем для узлов которые были перенесены
                //проходит по всем открытым нодам и переносит его если находит нужный нод открытым
                CarrayOverDataView(treeNode);
            }

            int[] index2 = { 0 };

            for (index2[0] = 0; index2[0] < treeNode.Children.Count; index2[0]++)
            {
                if(treeNode.Children[index2[0]].FolderName.Equals("Загрузка") != true)
                {
                    UpdateNodesRecursive(treeNode.Children[index2[0]], treeNode.Children, index2, operation , convertIcon);
                }
            }
           


        }

        // Call the procedure using the TreeView.  
        public void CallRecursive(ObservableCollection<FolderNodes> nodes , ConvertIconType convertIcon)
        {
            int[] index = { 0 };
            //nodes - коллекция для перебора
            //nodes[f] - текущий элемент
            //f - индекс перебора на случай удаления элмента
            //operation - разные операции над нодами
            //CheckNodes - проверка на переименование-изменения в полях и удаление
            //СarryOver - перенос node из folder в другую folder
            for (index[0] = 0; index[0] < nodes.Count; index[0]++)
            {
                UpdateNodesRecursive(nodes[index[0]], nodes, index, "CheckNodes" , convertIcon);
            }

            //еще один проход на перенос folderNode
            if(ListСarryOverNodes.Count > 0)
            {
                for (int f = 0; f < nodes.Count; f++)
                {
                    UpdateNodesRecursive(nodes[f], nodes, index, "СarryOver" , convertIcon);
                }

            }
           
        }
        //Перенос Папки из одного места в другое
        private void CarrayOverDataView(FolderNodes treeNode)
        {
            for(int v = 0; v < ListСarryOverNodes.Count; v++)
            {
                if(ListСarryOverNodes[v].ParentID.Equals(treeNode.Row_id))
                {
                    if(treeNode.Children == null)
                    {

                        ListСarryOverNodes[v] = CheckChildrenNodes(treeNode.Row_id, ListСarryOverNodes[v]);
                        treeNode.Children = new SortableObservableCollection<FolderNodes>();
                        TreeNodeAdd(treeNode, ListСarryOverNodes[v]);
                    }
                    else
                    {
                        ListСarryOverNodes[v] = CheckChildrenNodes(treeNode.Row_id, ListСarryOverNodes[v]);
                        TreeNodeAdd(treeNode, ListСarryOverNodes[v]);
                    }
                }
            }

        }

        private void TreeNodeAdd(FolderNodes treeNode , FolderNodes newNode)
        {
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                treeNode.Children.Add(newNode);
            });
        }
        //если у переносимого обьекта есть дети, добавляем в него массив
        private FolderNodes CheckChildrenNodes(long parent_id , FolderNodes nodes)
        {
            bool ServerModel = sqlcont.getSelect().existSqlLiteParentNodes(parent_id);

            if (ServerModel == true)
            {
                FolderNodes loadNodes = new FolderNodes();
                loadNodes.FolderName = "Загрузка";
                loadNodes.Type = "txt";
                loadNodes.Location = "Загрузка";
                
                nodes.Children = new SortableObservableCollection<FolderNodes>();
                nodes.Children.Add(loadNodes);
               
            }
           

            return nodes;
        }

        private void CheckUpdateDataView(FolderNodes treeNode, ObservableCollection<FolderNodes> list, int[] index , ConvertIconType convertIcon)
        {
            long row_id = treeNode.Row_id;
            long versionUpdateRows = treeNode.VersionUpdateRows;


            //sqllite
            FileInfoModel ServerModel = sqlcont.getSelect().getSearchNodesUpdateVersionRows(row_id, versionUpdateRows);



            if (ServerModel != null)
            {

                if (ServerModel.versionUpdateRows >= treeNode.VersionUpdateRows)
                {
                    //если файл или папка была прикреплена к другому обьекту(папке)
                    //мы ее удаляем из данного массива, что-бы он был перекреплен к другой папке
                    if (ServerModel.parent == treeNode.ParentID)
                    {

                        App.Current.Dispatcher.Invoke(delegate 
                        {
                            treeNode.Row_id = ServerModel.row_id;
                            treeNode.FolderName = ServerModel.filename;
                            treeNode.ParentID = ServerModel.parent;
                            treeNode.VersionUpdateRows = ServerModel.versionUpdateRows;
                            treeNode.Type = ServerModel.type;
                            treeNode.changeDate = ServerModel.changeDate;
                            treeNode.changeRows = ServerModel.changeRows;
                            treeNode.createDate = ServerModel.createDate;
                            treeNode.Location = ServerModel.location;
                        });
                    }
                    else
                    {
                        //создание нода
                        FolderNodes newNodes = new FolderNodes();
                        newNodes.changeRows = ServerModel.changeRows;
                        newNodes.FolderName = ServerModel.filename;
                        App.Current.Dispatcher.Invoke(delegate // <--- HERE
                        {
                            newNodes.Icon = convertIcon.getIconType(ServerModel.type);
                        });

                        
                        newNodes.ParentID = ServerModel.parent;
                        newNodes.Type = ServerModel.type;
                        newNodes.VersionUpdateRows = ServerModel.versionUpdateRows;
                        newNodes.Row_id = ServerModel.row_id;

                        newNodes.Location = ServerModel.location;
                        newNodes.changeDate = ServerModel.changeDate;
                        newNodes.createDate = ServerModel.createDate;
                        newNodes.lastOpenDate = ServerModel.lastOpenDate;
                        newNodes.sizebyte = ServerModel.sizebyte.ToString();




                        ListСarryOverNodes.Add(newNodes);
                        UpdateDeleteInvoke(list, index[0]);
                    }

                }

            }
            else
            {
                //удаляем т.к его в базе вообще не нашли
                //значит файл был удален
                UpdateDeleteInvoke(list, index[0]);
                index[0]--;
                //Console.WriteLine();
            }

        }

        
        private void UpdateDeleteInvoke(ObservableCollection<FolderNodes> list, int index)
        {
            try
            {
                if (list.Count >= 1)
                {
                    //проверка из какого потока запускается данный скрипт
                    if (System.Windows.Application.Current != null)
                    {
                        App.Current.Dispatcher.Invoke((Action)delegate // <--- HERE
                        {
                            list.RemoveAt(index);
                        });
                    }
                    else
                    {
                        list.RemoveAt(index);
                    }

                }

                else
                {
                    Console.WriteLine("ScannerRecursiveTreeView: Подозрительное обновление базы внимательно проверить! Размер дерева  "+ list.Count);
                }
            }
            catch(System.ArgumentOutOfRangeException a)
            {
                Console.WriteLine("ScannerRecursiveTreeView: Ошибка обновления "+a);
            }
           
           
        }

        
    }
}

   
