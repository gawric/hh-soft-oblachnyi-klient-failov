﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    public class ScannerSqlliteRecursiveAllChildren
    {

        static object lockerAllChildren = new object();
        //получает всех детей данной папки + сама папка
        public List<long[][]> getScanAllChildren(ScannerSqlliteRecursiveAllChildrenModel recursiveChildrenModel)
        {
            try
            {
                lock (lockerAllChildren)
                {
                    List<long[]> list = getAllRoot(recursiveChildrenModel);

                    recursiveChildrenModel.allFiles  = addListToAllList(recursiveChildrenModel, ref list);

                    return recursiveChildrenModel.allFiles;
                }


            }
            catch (Exception s1)
            {
                Console.WriteLine("ScannerSqlliteRecursiveAllChildren: getScanAllChildren: Сканирования Файлов из базы данных");
                Console.WriteLine(s1.ToString());
                return null;
            }

        }

        private List<long[][]> addListToAllList(ScannerSqlliteRecursiveAllChildrenModel recursiveChildrenModel , ref List<long[]> list)
        {
            List<long[][]> allListFull = recursiveChildrenModel.allListFull;

            if (list != null)
                allListFull.Add(list.ToArray());

            return allListFull;
        }


        //рекурсивный проход - по рутовым папкам
        private List<long[]> getAllRoot(ScannerSqlliteRecursiveAllChildrenModel recursiveChildrenModel)
        {
            //0 - id самого нода
            //1 - папка или файл
            List<long[]> allList = new List<long[]>();

            //получаем текущий узел из базы
            TempFilesModel rootLocation = getLocationTempFiles(recursiveChildrenModel._sqlLiteController, recursiveChildrenModel.location);

            if (rootLocation != null)
            {
                //его id номер
                long root_id = rootLocation.row_id;         
                long[] countFiles = { 0 };

                recursiveChildrenModel.countFiles = countFiles;
                recursiveChildrenModel.model = createProgressModel();
                recursiveChildrenModel.parent_id = root_id;
                recursiveChildrenModel.Alllist = allList;

                insertRootTemp(allList, rootLocation);
              

                return getAllScan(ref recursiveChildrenModel);

            }
            else
            {
                //срабатывает когда мы не знаем его id
                return allList;
            }

        }

        private TempFilesModel getLocationTempFiles(SqliteController sqllite , string location)
        {
            TempFilesModel model = new TempFilesModel();

            if(sqllite != null) model = sqllite.getSelect().getSearchByLocationToTempFilesModel(location);

            return model;
        }

        private CopyViewModel createProgressModel()
        {
            return  new CopyViewModel();
        }


        public List<long[]> getAllScan(ref ScannerSqlliteRecursiveAllChildrenModel recursiveChildrenModel)
        {
            try
            {
                SqliteController _sqlLiteController = recursiveChildrenModel._sqlLiteController;
                long parent_id = recursiveChildrenModel.parent_id;
                List<long[]> list = null;
                Dictionary<long, List<long[]>>  allParent = recursiveChildrenModel.allParent;

                if(allParent.ContainsKey(parent_id))
                {
                    list = allParent[parent_id];
                }
                else
                {
                    Console.WriteLine("ScannserSqlliteRecursiceAllChildren->getAllScan: Не критическая ошибка у данной папки нет детей!!! "+ parent_id);
                    list = new List<long[]>(0);
                }

                List<long[]> Alllist = recursiveChildrenModel.Alllist;

              
                
                for (int j = 0; j < list.Count; j++)
                {
                    try
                    {
                        recursiveChildrenModel._mre.WaitOne();
                        if(recursiveChildrenModel._cancelToken.IsCancellationRequested)
                        {
                            Alllist = null;
                            allParent = null;
                            break;
                        }


                        updatePb(ref recursiveChildrenModel);

                        long[] modelServer = list[j];
                        long type = modelServer[1];

                        Alllist.Add(modelServer);
                        recursiveChildrenModel.parent_id = modelServer[0];

                        if (type == 1)
                        {
                            getAllScan(ref recursiveChildrenModel);
                        }
                        

                    }
                    catch (System.InvalidOperationException s1)
                    {
                        Console.WriteLine("ScannerSqlliteRecursiveAllChildren-> getAllSacn: Сканнер Файлов для удаления потерпел не удачу");
                        Console.WriteLine(s1.ToString());
                        return Alllist;
                    }
                    catch (System.NullReferenceException s2)
                    {
                        Console.WriteLine("ScannerSqlliteRecursiveAllChildren-> getAllSacn: Сканнер Файлов для удаления потерпел не удачу");
                        Console.WriteLine(s2.ToString());
                        return Alllist;
                    }



                }

                return Alllist;
            }
            catch (System.InvalidOperationException s1)
            {
                Console.WriteLine("ScannerSqlliteRecursiveAllChildren-> getAllSacn: Сканнер Файлов для удаления потерпел не удачу");
                Console.WriteLine(s1.ToString());
                return new List<long[]>();
            }
            catch (System.NullReferenceException s2)
            {
                Console.WriteLine("ScannerSqlliteRecursiveAllChildren-> getAllSacn: Сканнер Файлов для удаления потерпел не удачу");
                Console.WriteLine(s2.ToString());
                return new List<long[]>();
            }

        }

        private void updatePb(ref ScannerSqlliteRecursiveAllChildrenModel recursiveChildrenModel)
        {
            recursiveChildrenModel.model.StatusWorker = recursiveChildrenModel.progressLabel + "  " + recursiveChildrenModel.countFiles[0]++;
            recursiveChildrenModel.progress.Report(recursiveChildrenModel.model);
        }


        //Добавляет рутовую папку или файл во временное хранилище вместе с остальными
        //т.к раньше добавлялись только дети
        private void insertRootTemp(List<long[]> list, TempFilesModel rootFiles)
        {

            long typeRoot;

            if (staticVariable.Variable.isFolder(rootFiles.type) == true)
            {
                typeRoot = 1;
            }
            else
            {
                typeRoot = 0;
            }

            long[] rootLong = { rootFiles.row_id, typeRoot };
            list.Add(rootLong);

        }
    }
}
