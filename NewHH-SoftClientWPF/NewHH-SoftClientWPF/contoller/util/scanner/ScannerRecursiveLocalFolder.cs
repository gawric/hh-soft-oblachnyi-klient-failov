﻿using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.scanner
{
    public class ScannerRecursiveLocalFolder
    {
        public  void WalkDirectoryTree(System.IO.DirectoryInfo root , RecursiveLocalModel model)
        {
                System.IO.FileInfo[] files = null;
                System.IO.DirectoryInfo[] subDirs = null;

            try
            {
                files = root.GetFiles("*.*");
                if (files != null)
                {

                    injectSizeBytes(ref files, ref model);

                    subDirs = root.GetDirectories();
                    start(ref subDirs, ref model);
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine("ScannerRecursiveLocalFolder-> WalkDirectoryTree  " + e);
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                Console.WriteLine("ScannerRecursiveLocalFolder-> WalkDirectoryTree  " + e);
            }
        }

        private void start(ref System.IO.DirectoryInfo[] subDirs , ref RecursiveLocalModel model)
        {
            foreach (System.IO.DirectoryInfo dirInfo in subDirs)
            {
                model.sizeCountFolder = model.sizeCountFolder + 1;
                WalkDirectoryTree(dirInfo, model);
            }
        }
        private void injectSizeBytes(ref System.IO.FileInfo[] files , ref RecursiveLocalModel model)
        {
            foreach (System.IO.FileInfo fi in files)
            {
                //Console.WriteLine(fi.FullName);

                model.sizeBytesFolder = model.sizeBytesFolder + fi.Length;
                model.sizeCountFiles = model.sizeCountFiles + 1;

            }
        }
    }
}
