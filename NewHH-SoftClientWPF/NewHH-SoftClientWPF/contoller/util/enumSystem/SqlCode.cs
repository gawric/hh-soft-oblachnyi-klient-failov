﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.util.enumSystem
{
    public enum SqlCode : int
    {
        //Insert код
        InsertUsernameAndPassword = 0,
        InsertAutoScanInfo = 1,
        InsertSystemSetting = 2,
        InsertAutoScanTimemill = 3,
        InsertListSavePc = 4,
        InsertLocationDownload = 5,
        InsertScanFolder = 6,
        InsertListFilesTempIndex = 8,
        InsertFileListSinglRows = 9,
        InsertFileInfoTransaction = 10,
        InsertFileTempTransaction = 11,
        InsertAutoSaveFileTransaction = 12,
        InsertFileTransferTransaction = 13,
        InsertListScanFolder = 14,

        //Delete Code
        RemoveTableslistFilesTemp = 15,
        DeletelistSaveToPc = 16,
        DeleteListFilesTransaction = 17,
        DeleteAllRows = 18,
        ClearLocation = 19,
        ClearListScanFolder = 20,
        ClearScanFolder = 32,
        ClearTableslistFilesTemp = 22,
        DeletePcSaveRowsLocation = 23,
        DeleteSavePcSinglRows = 24,
        RemoveListFilesTransferSingl = 25,
        RemoveSinglItemScanFolder = 26,
        RemoveTablelistFileTempChangeRows = 27,
        ClearTableslistFiles = 28,
        ClearTablesPcTemp = 29,
        ClearTableslistTransfer = 30,
        ClearTablesUsers = 31,
        CopyTempInSavePc = 33,
        CopySavePcToTempSavePc = 34

    }
}
