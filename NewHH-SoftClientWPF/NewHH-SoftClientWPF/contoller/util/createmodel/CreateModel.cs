﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.onlyCloud;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.viewFolderModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.model
{
    public class CreateModel
    {
       private ConvertIconType convertIcon;

        public CreateModel(ConvertIconType convertIcon)
        {
            this.convertIcon = convertIcon;
        }

        public PasteOrMoveEventModel createPateOrMoveEventModel(CopyStatusController _copyStatus, PasteWindowsController _pasteController, SqliteController _sqlLiteController, ViewFolder _viewFolder, MainWindowViewModel _viewModelMain)
        {
            PasteOrMoveEventModel moveModel = new PasteOrMoveEventModel();
            moveModel._convert = convertIcon;
            moveModel._copyStatus = _copyStatus;
            moveModel._pasteController = _pasteController;
            moveModel._sqlLiteController = _sqlLiteController;
            moveModel._viewFolder = _viewFolder;
            moveModel._viewModelMain = _viewModelMain;

            return moveModel;
        }

        public FolderNodes createModelFolderNodesToViewFolder(FileInfoModel serverModel)
        {
            
                //Данный Nodes выбран в treeview т.к в ViewFolder выбор не произвелся
                FolderNodes PasteNodes = new FolderNodes();
                PasteNodes.Row_id = serverModel.row_id;
                PasteNodes.sizebyte = serverModel.sizebyte.ToString();
                PasteNodes.Type = serverModel.type;
                PasteNodes.ParentID = serverModel.parent;
                PasteNodes.Location = serverModel.location;
                PasteNodes.FolderName = serverModel.filename;

                return PasteNodes;
            
        }

        public ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(ref List<long[][]> allListFull, ref string location, DownloadWebDavModel downloadModel)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();

            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = downloadModel._sqlliteController;
            model.progress = new Progress<CopyViewModel>();
            model.progressLabel = "search children";
            //все записи из базы
            model.allParent = downloadModel._sqlliteController.getSelect().getAllParentListRowId();
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();

            return model;
        }

        public OnlyCloudMainModel createModel(SqliteController sqlliteController, ConvertIconType convertIcon, SortableObservableCollection<FolderNodes> viewTreeList, SortableObservableCollection<FolderNodes> viewFolderList , ExceptionController _exceptionController )
        {
            OnlyCloudMainModel model = new OnlyCloudMainModel();
            model.convertIcon = convertIcon;
            model.sqlliteController = sqlliteController;
            model.viewFolderList = viewFolderList;
            model.viewTreeList = viewTreeList;
            model._exceptionController = _exceptionController;
            return model;
        }


        public FolderNodesTransfer createViewTransferModel(FileInfoModel fileInfoModel, string FolderName, string uploadStatus, string iconTransfer , string TypeTransfer)
        {
            FolderNodesTransfer model = new FolderNodesTransfer();
            //model.row_id задается dj 
            model.Row_id_listFiles_users = fileInfoModel.row_id;
            model.Sizebyte = staticVariable.UtilMethod.FormatBytes(fileInfoModel.sizebyte);
            model.Type = fileInfoModel.type;
            model.uploadStatus = uploadStatus;
            model.Progress = 0;
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                model.Icon = convertIcon.getIconType(fileInfoModel.type);
            });
           
            model.FolderName = FolderName;
            model.IconTransfer = convertIcon.getIconTransfer(iconTransfer);
            model.TypeTransfer = TypeTransfer;
            return model;
        }


        public FolderNodesTransfer createTransferModel(long row_id, string folderName, long sizeByteFolder , string typeTransfer , string type)
        {
            FolderNodesTransfer model = new FolderNodesTransfer();
            model.Row_id = row_id;
            model.Sizebyte = staticVariable.UtilMethod.FormatBytes(0);
            model.Type = type;
            model.uploadStatus = "Запуск";
            model.Progress = 0;
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                model.Icon = convertIcon.getIconType(type);
            });
            
            model.FolderName = folderName;
            model.TypeTransfer = typeTransfer;
            model.IconTransfer = convertIcon.getIconTransfer(typeTransfer);
            model.Sizebyte = staticVariable.UtilMethod.FormatBytes(sizeByteFolder);



            return model;
        }

        public FileInfoTransfer convertNodesTransferToFileInfoTransfer(FolderNodesTransfer fnt)
        {
            FileInfoTransfer sqllite_model = new FileInfoTransfer();
            sqllite_model.row_id = (int)fnt.Row_id;
            sqllite_model.row_id_listFiles_users = (int)fnt.Row_id_listFiles_users;
            sqllite_model.filename = fnt.FolderName;
            sqllite_model.sizebyte = fnt.Sizebyte;
            sqllite_model.type = fnt.Type;
            sqllite_model.progress = fnt.progress;
            sqllite_model.typeTransfer = fnt.TypeTransfer;
            return sqllite_model;
        }

        public MoveFilesAsyncModel convertRenameModelToMoveFilesAsyncModel(bool isFolder, string copyFolderName, string location, PasteFilesAsyncModel pasteModel)
        {
            MoveFilesAsyncModel moveModel = new MoveFilesAsyncModel();

            moveModel.isFolder = isFolder;
            moveModel.copyFolderName = copyFolderName;
            moveModel.location = location;
            moveModel.pasteLocation = pasteModel.pasteFolder.Location;
            moveModel.username = pasteModel.username;
            moveModel.password = pasteModel.password;
            moveModel.oldFolderName = pasteModel.copyFolderName;

            return moveModel;
        }

        public FileInfoModel createFileInfoModel(long Row_id, string Filename, string CreateDate, string ChangeDate, string lastOpenDate, string Attribute, string Location, long Parent, string Type, long sizebytes, long versionupdatebases, long versionupdaterows, long User_id, string ChangeRows, int saveTopc)
        {
            FileInfoModel model = new FileInfoModel();
            try
            {
                model.row_id = Row_id;
                model.filename = Filename;
                model.createDate = CreateDate;
                model.changeDate = ChangeDate;
                model.lastOpenDate = lastOpenDate;
                model.attribute = Attribute;
                model.location = Location;
                model.parent = Parent;
                model.type = Type;
                model.sizebyte = sizebytes;
                model.versionUpdateBases = versionupdatebases;
                model.versionUpdateRows = versionupdaterows;
                model.user_id = staticVariable.Variable.getUser_id();
                model.changeRows = ChangeRows;
                model.saveTopc = saveTopc;
            }
            catch (System.InvalidOperationException j)
            {
                Console.WriteLine(j);
            }

            return model;
        }

        public FolderNodes convertFileInfoToFolderNodes(FileInfoModel fileInfoModel)
        {
            FolderNodes folderclient = new FolderNodes();
            folderclient.Row_id = fileInfoModel.row_id;
            folderclient.FolderName = fileInfoModel.filename;
            folderclient.ParentID = fileInfoModel.parent;
            folderclient.Type = fileInfoModel.type;
            folderclient.VersionUpdateRows = fileInfoModel.versionUpdateRows;

            App.Current.Dispatcher.Invoke(delegate
            {
                folderclient.Icon = convertIcon.getIconType(fileInfoModel.type);
            });


            
            folderclient.changeRows = fileInfoModel.changeRows;
            folderclient.Location = fileInfoModel.location;
            folderclient.changeDate = fileInfoModel.changeDate;
            folderclient.createDate = fileInfoModel.createDate;
            folderclient.sizebyte = staticVariable.UtilMethod.FormatBytes(fileInfoModel.sizebyte);
            folderclient.lastOpenDate = fileInfoModel.lastOpenDate;

            return folderclient;
        }

        public FolderNodes CreateFolderNodesModel(string FolderName, long ParentID, string Type, long VersionUpdateRows, long Row_id, string changeRows, string location, string createDate, string changeDate, long sizebytes, string lastopendate, ConvertIconType convertIcon)
        {



            FolderNodes folderclient = new FolderNodes();
            folderclient.Row_id = Row_id;
            folderclient.FolderName = FolderName;
            folderclient.ParentID = ParentID;
            folderclient.Type = Type;
            folderclient.VersionUpdateRows = VersionUpdateRows;
            folderclient.changeRows = changeRows;
            folderclient.Location = location;
            folderclient.changeDate = changeDate;
            folderclient.createDate = createDate;
            setIcon(folderclient, Type);
            setSizeBytes(folderclient , sizebytes);
            folderclient.lastOpenDate = lastopendate;

            return folderclient;
        }
        private void setIcon(FolderNodes folderclient , string type)
        {
            App.Current.Dispatcher.Invoke(delegate
            {

                folderclient.Icon = convertIcon.getIconType(type);
            });
        }

        private void setSizeBytes(FolderNodes folderclient , long sizebytes)
        {
            if (staticVariable.Variable.isFolder(folderclient.Type))
            {
                folderclient.sizebyte = "";
            }
            else
            {
                //конвертер байты в кб или мб (для правильного чтения)
                folderclient.sizebyte = staticVariable.UtilMethod.FormatBytes(sizebytes);
            }
        }

        public FolderNodes convertFileInfoRecursiveToFolderNodes(FileInfoModel children)
        {
            //создание нода
            FolderNodes newNodes = new FolderNodes();
            newNodes.changeRows = children.changeRows;
            newNodes.FolderName = children.filename;
            App.Current.Dispatcher.Invoke(delegate // <--- HERE
            {
                newNodes.Icon = convertIcon.getIconType(children.type);
            });
          
            newNodes.ParentID = children.parent;
            newNodes.Type = children.type;
            newNodes.VersionUpdateRows = children.versionUpdateRows;
            newNodes.Row_id = children.row_id;
            newNodes.Location = children.location;
            newNodes.changeDate = children.changeDate;
            newNodes.createDate = children.createDate;
            newNodes.lastOpenDate = children.lastOpenDate;
            newNodes.sizebyte = children.sizebyte.ToString();

            return newNodes;
        }

       

    }
}
