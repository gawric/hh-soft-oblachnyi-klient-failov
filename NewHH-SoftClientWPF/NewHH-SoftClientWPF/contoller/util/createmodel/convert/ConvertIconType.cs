﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using static NewHH_SoftClientWPF.staticVariable.Variable;
using static NewHH_SoftClientWPF.staticVariable.UtilMethod.BitmapConversion;
using static NewHH_SoftClientWPF.staticVariable.UtilMethod;
using System.Drawing;
using System.IO;
using NewHH_SoftClientWPF.staticVariable;

namespace NewHH_SoftClientWPF.contoller.createmodel.convert
{
    public class ConvertIconType
    {
        private Dictionary<string, BitmapImage> typeIcon = new Dictionary<string, BitmapImage>();
        private Dictionary<string, Uri> uriIcon = new Dictionary<string, Uri>();


        public Icon streamToIcon(string typeName)
        {
            try
            {
                Uri s = getUriIcon(typeName);
                Icon icon;
                string f = s.AbsolutePath;
                string v = staticVariable.Variable.DecodeUrlString(f);
                using (Stream fs = new FileStream(v, FileMode.Open, FileAccess.Read))
                {
                    icon = new Icon(fs);
                }

                return icon;
            }
            catch(Exception a)
            {
                Console.WriteLine("ConvertIconType->streamToIcon: критическая ошиюка" + a.ToString());
                return new Icon("error");
            }
           
        }

        //получение иконки для MainVolder и ViewFolder
        public  BitmapSource getIconType(string type)
        {

            BitmapSource source = null;
            source = checkThredRun(source, type);



            return source;
        }

        //получение иконки для ViewTransfer
        public  BitmapSource getIconTransfer(string type)
        {

            BitmapSource source = null;
            source = checkThredRun(source, type);

            return source;
        }

        private  BitmapSource checkThredRun(BitmapSource source, string type)
        {
            try
            {
              source = getIcon(type);

                return source;
            }
            catch (System.StackOverflowException h)
            {
                Console.WriteLine("checkThredRun  " + h);
                return getIcon("folder");
            }

        }

       
        public Uri getUriIcon(string type)
        {
            if(uriIcon.Count == 0)
            {
                string dirRootCatalog = staticVariable.Variable.dirRootCatalog;
                if (dirRootCatalog == null)
                {
                    staticVariable.Variable.getBasesDir();
                     dirRootCatalog = staticVariable.Variable.dirRootCatalog;
                }
                
  

                addUriToDictionary("folder", dirRootCatalog + "\\icon\\clound_icon\\78_border_cloud.png");
                addUriToDictionary("txt", dirRootCatalog + "\\icon\\clound_icon\\33_cloud.png");
                addUriToDictionary(".mp3", dirRootCatalog + "\\icon\\clound_icon\\45_cloud.png");
                addUriToDictionary(".aiff", dirRootCatalog + "\\icon\\clound_icon\\45_cloud.png");
                addUriToDictionary(".aac", dirRootCatalog + "\\icon\\clound_icon\\45_cloud.png");
                addUriToDictionary(".ogg", dirRootCatalog + "\\icon\\clound_icon\\45_cloud.png");
                addUriToDictionary(".wma", dirRootCatalog + "\\icon\\clound_icon\\45_cloud.png");



                addUriToDictionary(".doc", dirRootCatalog + "\\icon\\clound_icon\\doc2_cloud.png");
                addUriToDictionary(".docx", dirRootCatalog + "\\icon\\clound_icon\\doc2_cloud.png");
                addUriToDictionary(".ppt", dirRootCatalog + "\\icon\\clound_icon\\doc2_cloud.png");
                addUriToDictionary(".xls", dirRootCatalog + "\\icon\\clound_icon\\exel_cloud.png");
                addUriToDictionary(".xlsx", dirRootCatalog + "\\icon\\clound_icon\\exel_cloud.png");


                addUriToDictionary(".pdf", dirRootCatalog + "\\icon\\clound_icon\\pdf_cloud.png");
                addUriToDictionary(".exe", dirRootCatalog + "\\icon\\clound_icon\\exe_cloud.png");



                addUriToDictionary(".jpeg", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".jpg", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".png", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".svg", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".tiff", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".gif", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".bmp", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".ico", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".psd", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");
                addUriToDictionary(".pcx", dirRootCatalog + "\\icon\\clound_icon\\image2_cloud.png");


                addUriToDictionary("upload", dirRootCatalog + "\\icon\\uploadTransfer.png");
                addUriToDictionary("download", dirRootCatalog + "\\icon\\downloadTransfer.png");

                addUriToDictionary(".rar", dirRootCatalog + "\\icon\\clound_icon\\winrar_cloud.png");
                addUriToDictionary(".zip", dirRootCatalog + "\\icon\\clound_icon\\winrar_cloud.png");
                addUriToDictionary(".7z", dirRootCatalog + "\\icon\\clound_icon\\winrar_cloud.png");
                addUriToDictionary(".tar", dirRootCatalog + "\\icon\\clound_icon\\winrar_cloud.png");
                addUriToDictionary(".tz", dirRootCatalog + "\\icon\\clound_icon\\winrar_cloud.png");
                addUriToDictionary(".gz", dirRootCatalog + "\\icon\\clound_icon\\winrar_cloud.png");
     
                addUriToDictionary(".properties", dirRootCatalog + "\\icon\\clound_icon\\properties_cloud.png");

                //refresh
                addUriToDictionary("txt_refresh", dirRootCatalog + "\\icon\\refresh_icon\\33_refresh.png");
                addUriToDictionary(".mp3_refresh", dirRootCatalog + "\\icon\\refresh_icon\\45-refresh.png");
                addUriToDictionary(".aiff_refres", dirRootCatalog + "\\icon\\refresh_icon\\45-refresh.png");
                addUriToDictionary(".aac_refres", dirRootCatalog + "\\icon\\refresh_icon\\45-refresh.png");
                addUriToDictionary(".ogg_refres", dirRootCatalog + "\\icon\\refresh_icon\\45-refresh.png");
                addUriToDictionary(".wma_refres", dirRootCatalog + "\\icon\\refresh_icon\\45-refresh.png");

                addUriToDictionary("folder_refresh", dirRootCatalog + "\\icon\\refresh_icon\\78_border_refresh.png");

                addUriToDictionary("doc_refresh", dirRootCatalog + "\\icon\\refresh_icon\\doc2_refresh.png");
                addUriToDictionary(".docx_refresh", dirRootCatalog + "\\icon\\refresh_icon\\doc2_refresh.png");
                addUriToDictionary(".ppt_refresh", dirRootCatalog + "\\icon\\refresh_icon\\doc2_refresh.png");

                addUriToDictionary(".exe_refresh", dirRootCatalog + "\\icon\\refresh_icon\\exe_refresh.png");
                addUriToDictionary(".xls_refresh", dirRootCatalog + "\\icon\\refresh_icon\\exel_refresh.png");
                addUriToDictionary(".xlsx_refresh", dirRootCatalog + "\\icon\\refresh_icon\\exel_refresh.png");
                addUriToDictionary(".pdf_refresh", dirRootCatalog + "\\icon\\refresh_icon\\pdf_refresh.png");
                addUriToDictionary(".properties_refresh", dirRootCatalog + "\\icon\\refresh_icon\\properties_refresh.png");

                addUriToDictionary(".rar_refresh", dirRootCatalog + "\\icon\\refresh_icon\\winrar.refresh.png");
                addUriToDictionary(".zip_refresh", dirRootCatalog + "\\icon\\refresh_icon\\winrar.refresh.png");
                addUriToDictionary(".7z_refresh", dirRootCatalog + "\\icon\\refresh_icon\\winrar.refresh.png");
                addUriToDictionary(".tar_refresh", dirRootCatalog + "\\icon\\refresh_icon\\winrar.refresh.png");
                addUriToDictionary(".tz_refresh", dirRootCatalog + "\\icon\\refresh_icon\\winrar.refresh.png");
                addUriToDictionary(".gz_refresh", dirRootCatalog + "\\icon\\refresh_icon\\winrar.refresh.png");
                addUriToDictionary(".xml_refresh", dirRootCatalog + "\\icon\\refresh_icon\\xml_refresh.png");


                addUriToDictionary(".jpeg_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".jpg_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".png_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".svg_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".tiff_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".gif_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".bmp_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".ico_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".psd_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");
                addUriToDictionary(".pcx_refresh", dirRootCatalog + "\\icon\\refresh_icon\\image2_refresh.png");




                //ok
                addUriToDictionary("txt_ok", dirRootCatalog + "\\icon\\ok_icon\\33_ok.png");
                addUriToDictionary(".mp3_ok", dirRootCatalog + "\\icon\\ok_icon\\45-ok.png");
                addUriToDictionary(".aiff_ok", dirRootCatalog + "\\icon\\ok_icon\\45-ok.png");
                addUriToDictionary(".aac_ok", dirRootCatalog + "\\icon\\ok_icon\\45-ok.png");
                addUriToDictionary(".ogg_ok", dirRootCatalog + "\\icon\\ok_icon\\45-ok.png");
                addUriToDictionary(".wma_ok", dirRootCatalog + "\\icon\\ok_icon\\45-ok.png");

                addUriToDictionary("folder_ok", dirRootCatalog + "\\icon\\ok_icon\\78_border_ok.png");

                addUriToDictionary("doc_ok", dirRootCatalog + "\\icon\\ok_icon\\doc2_ok.png");
                addUriToDictionary(".docx_ok", dirRootCatalog + "\\icon\\ok_icon\\doc2_ok.png");
                addUriToDictionary(".ppt_ok", dirRootCatalog + "\\icon\\ok_icon\\doc2_ok.png");

                addUriToDictionary(".exe_ok", dirRootCatalog + "\\icon\\ok_icon\\exe_ok.png");
                addUriToDictionary(".xls_ok", dirRootCatalog + "\\icon\\ok_icon\\exel_ok.png");
                addUriToDictionary(".xlsx_ok", dirRootCatalog + "\\icon\\ok_icon\\exel_ok.png");
                addUriToDictionary(".pdf_ok", dirRootCatalog + "\\icon\\ok_icon\\pdf_ok.png");
                addUriToDictionary(".properties_ok", dirRootCatalog + "\\icon\\ok_icon\\properties_ok.png");

                addUriToDictionary(".rar_ok", dirRootCatalog + "\\icon\\ok_icon\\winrar_ok.png");
                addUriToDictionary(".zip_ok", dirRootCatalog + "\\icon\\ok_icon\\winrar_ok.png");
                addUriToDictionary(".7z_ok", dirRootCatalog + "\\icon\\ok_icon\\winrar_ok.png");
                addUriToDictionary(".tar_ok", dirRootCatalog + "\\icon\\ok_icon\\winrar_ok.png");
                addUriToDictionary(".tz_ok", dirRootCatalog + "\\icon\\ok_icon\\winrar_ok.png");
                addUriToDictionary(".gz_ok", dirRootCatalog + "\\icon\\ok_icon\\winrar_ok.png");
                addUriToDictionary(".xml_ok", dirRootCatalog + "\\icon\\ok_icon\\xml_ok.png");


                addUriToDictionary(".jpeg_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".jpg_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".png_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".svg_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".tiff_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".gif_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".bmp_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".ico_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".psd_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");
                addUriToDictionary(".pcx_ok", dirRootCatalog + "\\icon\\ok_icon\\image2_ok.png");



                //cloud
                addUriToDictionary(".xml", dirRootCatalog + "\\icon\\clound_icon\\xml_cloud.png");
                addUriToDictionary(".buttonCloud", dirRootCatalog + "\\icon\\staticIcon\\buttonInCloud.png");
                addUriToDictionary(".buttonSync", dirRootCatalog + "\\icon\\staticIcon\\syncing.png");
                addUriToDictionary(".buttonLink", dirRootCatalog + "\\icon\\staticIcon\\buttonLink.png");

                addUriToDictionary(".buttonNavigationBack", dirRootCatalog + "\\icon\\staticIcon\\_backButton.png");
                addUriToDictionary(".buttonNavigationForward", dirRootCatalog + "\\icon\\staticIcon\\_forwardButton.png");

                addUriToDictionary(".menuItemChangeUser", dirRootCatalog + "\\icon\\staticIcon\\change_user.png");
                addUriToDictionary(".menuItemExitProgramm", dirRootCatalog + "\\icon\\staticIcon\\exit.png");
                addUriToDictionary(".menuItemSystemSetting", dirRootCatalog + "\\icon\\staticIcon\\settingIcon.png");
                addUriToDictionary(".menuItemAllScan", dirRootCatalog + "\\icon\\staticIcon\\folderScan.png");

                addUriToDictionary(".contextMenuItemCutFolder", dirRootCatalog + "\\icon\\staticIcon\\cut_folder.png");
                addUriToDictionary(".contextMenuItemCopyFolder", dirRootCatalog + "\\icon\\staticIcon\\copy_folder.png");
                addUriToDictionary(".contextMenuItemPasteFolder", dirRootCatalog + "\\icon\\staticIcon\\paste_folder.png");
                addUriToDictionary(".contextMenuItemDeleteFolder", dirRootCatalog + "\\icon\\staticIcon\\delete_folder.png");
                addUriToDictionary(".contextMenuItemRenameFolder", dirRootCatalog + "\\icon\\staticIcon\\rename_folder.png");
                addUriToDictionary(".contextMenuItemSaveOk", dirRootCatalog + "\\icon\\staticIcon\\ok.png");
                addUriToDictionary(".contextMenuStart", dirRootCatalog + "\\icon\\staticIcon\\start.png");
                addUriToDictionary(".contextMenuCreate", dirRootCatalog + "\\icon\\staticIcon\\createFolder.png");
                addUriToDictionary(".contextMenuItemGoFolder", dirRootCatalog + "\\icon\\staticIcon\\open_folder.png");

                addUriToDictionary(".contextMenuPause", dirRootCatalog + "\\icon\\staticIcon\\pause.png");
                addUriToDictionary(".contextMenuStop", dirRootCatalog + "\\icon\\staticIcon\\stop.png");
                addUriToDictionary(".attention", dirRootCatalog + "\\icon\\staticIcon\\attemtion.png");
                addUriToDictionary(".cloud", dirRootCatalog + "\\icon\\staticIcon\\cloud.png");
                addUriToDictionary(".contextMenuItemCopyLink", dirRootCatalog + "\\icon\\staticIcon\\url.png");


                addUriToDictionary(".notifyIconBarOk", dirRootCatalog + "\\icon\\staticIcon\\logo4.ico");
                addUriToDictionary(".notifyIconBarRefresh", dirRootCatalog + "\\icon\\staticIcon\\refreshNotify.ico");
                addUriToDictionary(".applicationIcon", dirRootCatalog + "\\icon\\staticIcon\\logo4.ico");
            }

            if (uriIcon.ContainsKey(type))
            {
                return uriIcon[type];
            }
            else
            {
                return getUnknownIcon(type);
            }

        
        }

     
        private Uri getUnknownIcon(string type)
        {
            int search = type.LastIndexOf("_") + 1;

            if(search > 0)
            {
                string type2 = type.Remove(0 , search);
                
                if(type2.Equals("ok"))
                {
                    return uriIcon["txt_ok"];
                }
                else
                {
                    return uriIcon["txt_refresh"];
                }
            }
            else
            {
                return uriIcon["txt"];
            }
        }


        private BitmapImage getUnknownBitmap(string type , Dictionary<string, BitmapImage> arrayIcon)
        {
            int search = type.LastIndexOf("_") + 1;

            if (search > 0)
            {
                string type2 = type.Remove(0, search);

                if (type2.Equals("ok"))
                {
                    return arrayIcon["txt_ok"];
                }
                else
                {
                    return arrayIcon["txt_refresh"];
                }
            }
            else
            {
                return arrayIcon["txt"];
            }
        }

        private BitmapImage getIcon(string type)
        {
            Dictionary<string, BitmapImage> arrayIcon = getArrayTypeIcon();

            if (arrayIcon.ContainsKey(type))
            {

                return arrayIcon[type];
            }
            else
            {
                return getUnknownBitmap(type, arrayIcon);
            }
        }


        public  Dictionary<string, BitmapImage> getArrayTypeIcon()
        {
            if (typeIcon.Count == 0)
            {


                // typeIcon.Add("folder", BitmapConversion.BitmapToBitmapSource(ResourceIcon._78_border));
                // typeIcon.Add("txt", BitmapConversion.BitmapToBitmapSource(ResourceIcon._33));
                //typeIcon.Add("folderTreeView", BitmapConversion.BitmapToBitmapSource(ResourceIcon._78_border));
                // typeIcon.Add("mp3", BitmapConversion.BitmapToBitmapSource(ResourceIcon._45));
                // typeIcon.Add("doc", BitmapConversion.BitmapToBitmapSource(ResourceIcon._31));
                // typeIcon.Add("jpg", BitmapConversion.BitmapToBitmapSource(ResourceIcon._39));
                //  typeIcon.Add("upload", BitmapConversion.BitmapToBitmapSource(ResourceIcon._uploadTransfer));
                //  typeIcon.Add("download", BitmapConversion.BitmapToBitmapSource(ResourceIcon._downloadTransfer));

                addIconToDictionary("folder", BitmapToBitmapSource(getUriIcon("folder")));
                addIconToDictionary("txt", BitmapToBitmapSource(getUriIcon("txt")));


                 addIconToDictionary(".mp3", BitmapToBitmapSource(getUriIcon(".mp3")));
                 addIconToDictionary(".aiff", BitmapToBitmapSource(getUriIcon(".aiff")));
                addIconToDictionary(".aac", BitmapToBitmapSource(getUriIcon(".aac")));
                addIconToDictionary(".ogg", BitmapToBitmapSource(getUriIcon(".ogg")));
                addIconToDictionary(".wma", BitmapToBitmapSource(getUriIcon(".wma")));

                addIconToDictionary(".doc", BitmapToBitmapSource(getUriIcon(".doc")));
                addIconToDictionary(".docx", BitmapToBitmapSource(getUriIcon(".docx")));
                addIconToDictionary(".ppt", BitmapToBitmapSource(getUriIcon(".ppt")));
                addIconToDictionary(".xls", BitmapToBitmapSource(getUriIcon(".xls")));
                addIconToDictionary(".xlsx", BitmapToBitmapSource(getUriIcon(".xlsx")));

                addIconToDictionary(".pdf", BitmapToBitmapSource(getUriIcon(".pdf")));
                addIconToDictionary(".exe", BitmapToBitmapSource(getUriIcon(".exe")));

                addIconToDictionary(".jpeg", BitmapToBitmapSource(getUriIcon(".jpeg")));
                addIconToDictionary(".jpg", BitmapToBitmapSource(getUriIcon(".jpg")));
                addIconToDictionary(".png", BitmapToBitmapSource(getUriIcon(".png")));
                addIconToDictionary(".svg", BitmapToBitmapSource(getUriIcon(".svg")));
                addIconToDictionary(".tiff", BitmapToBitmapSource(getUriIcon(".tiff")));
                addIconToDictionary(".gif", BitmapToBitmapSource(getUriIcon(".gif")));
                addIconToDictionary(".bmp", BitmapToBitmapSource(getUriIcon(".bmp")));
                addIconToDictionary(".ico", BitmapToBitmapSource(getUriIcon(".ico")));
                addIconToDictionary(".psd", BitmapToBitmapSource(getUriIcon(".psd")));
                addIconToDictionary(".pcx", BitmapToBitmapSource(getUriIcon(".pcx")));

                addIconToDictionary("upload", BitmapToBitmapSource(getUriIcon("upload")));
                addIconToDictionary("download", BitmapToBitmapSource(getUriIcon("download")));
                addIconToDictionary(".rar", BitmapToBitmapSource(getUriIcon(".rar")));
                addIconToDictionary(".zip", BitmapToBitmapSource(getUriIcon(".zip")));
                addIconToDictionary(".7z", BitmapToBitmapSource(getUriIcon(".7z")));
                addIconToDictionary(".tar", BitmapToBitmapSource(getUriIcon(".tar")));
                addIconToDictionary(".tz", BitmapToBitmapSource(getUriIcon(".tz")));
                addIconToDictionary(".gz", BitmapToBitmapSource(getUriIcon(".gz")));


                //refresh
               
                addIconToDictionary("folder_refresh", BitmapToBitmapSource(getUriIcon("folder_refresh")));

                addIconToDictionary("txt_refresh", BitmapToBitmapSource(getUriIcon(".txt_refresh")));
                addIconToDictionary(".mp3_refresh", BitmapToBitmapSource(getUriIcon(".mp3_refresh")));
                addIconToDictionary(".aiff_refres", BitmapToBitmapSource(getUriIcon(".aiff_refres")));
                addIconToDictionary(".aac_refres", BitmapToBitmapSource(getUriIcon(".aac_refres")));
                addIconToDictionary(".ogg_refres", BitmapToBitmapSource(getUriIcon(".ogg_refres")));
                addIconToDictionary(".wma_refres", BitmapToBitmapSource(getUriIcon(".wma_refres")));


                addIconToDictionary("doc_refresh", BitmapToBitmapSource(getUriIcon(".doc_refresh")));
                addIconToDictionary(".docx_refresh", BitmapToBitmapSource(getUriIcon(".docx_refresh")));
                addIconToDictionary(".ppt_refresh", BitmapToBitmapSource(getUriIcon(".ppt_refresh")));

                addIconToDictionary(".exe_refresh", BitmapToBitmapSource(getUriIcon(".exe_refresh")));
                addIconToDictionary(".xls_refresh", BitmapToBitmapSource(getUriIcon(".xls_refresh")));
                addIconToDictionary(".xlsx_refresh", BitmapToBitmapSource(getUriIcon(".xlsx_refresh")));
                addIconToDictionary(".pdf_refresh", BitmapToBitmapSource(getUriIcon(".pdf_refresh")));
                addIconToDictionary(".properties_refresh", BitmapToBitmapSource(getUriIcon(".properties_refresh")));

                addIconToDictionary(".rar_refresh", BitmapToBitmapSource(getUriIcon(".rar_refresh")));
                addIconToDictionary(".zip_refresh", BitmapToBitmapSource(getUriIcon(".zip_refresh")));
                addIconToDictionary(".7z_refresh", BitmapToBitmapSource(getUriIcon(".7z_refresh")));
                addIconToDictionary(".tar_refresh", BitmapToBitmapSource(getUriIcon(".tar_refresh")));
                addIconToDictionary(".tz_refresh", BitmapToBitmapSource(getUriIcon(".tz_refresh")));
                addIconToDictionary(".gz_refresh", BitmapToBitmapSource(getUriIcon(".gz_refresh")));
                addIconToDictionary(".xml_refresh", BitmapToBitmapSource(getUriIcon(".xml_refresh")));


                addIconToDictionary(".jpeg_refresh", BitmapToBitmapSource(getUriIcon(".jpeg_refresh")));
                addIconToDictionary(".jpg_refresh", BitmapToBitmapSource(getUriIcon(".jpg_refresh")));
                addIconToDictionary(".png_refresh", BitmapToBitmapSource(getUriIcon(".png_refresh")));
                addIconToDictionary(".svg_refresh", BitmapToBitmapSource(getUriIcon(".svg_refresh")));
                addIconToDictionary(".tiff_refresh", BitmapToBitmapSource(getUriIcon(".tiff_refresh")));
                addIconToDictionary(".gif_refresh", BitmapToBitmapSource(getUriIcon(".gif_refresh")));
                addIconToDictionary(".bmp_refresh", BitmapToBitmapSource(getUriIcon(".bmp_refresh")));
                addIconToDictionary(".ico_refresh", BitmapToBitmapSource(getUriIcon(".ico_refresh")));
                addIconToDictionary(".psd_refresh", BitmapToBitmapSource(getUriIcon(".psd_refresh")));
                addIconToDictionary(".pcx_refresh", BitmapToBitmapSource(getUriIcon(".pcx_refresh")));



                //ok
                addIconToDictionary("folder_ok", BitmapToBitmapSource(getUriIcon("folder_ok")));
                addIconToDictionary("txt_ok", BitmapToBitmapSource(getUriIcon("txt_ok")));
                addIconToDictionary(".mp3_ok", BitmapToBitmapSource(getUriIcon(".mp3_ok")));
                addIconToDictionary(".aiff_ok", BitmapToBitmapSource(getUriIcon(".aiff_ok")));
                addIconToDictionary(".aac_ok", BitmapToBitmapSource(getUriIcon(".aac_ok")));
                addIconToDictionary(".ogg_ok", BitmapToBitmapSource(getUriIcon(".ogg_ok")));
                addIconToDictionary(".wma_ok", BitmapToBitmapSource(getUriIcon(".wma_ok")));




                addIconToDictionary("doc_ok", BitmapToBitmapSource(getUriIcon("doc_ok")));
                addIconToDictionary(".docx_ok", BitmapToBitmapSource(getUriIcon("docx_ok")));
                addIconToDictionary(".ppt_ok", BitmapToBitmapSource(getUriIcon("ppt_ok")));

                addIconToDictionary(".exe_ok", BitmapToBitmapSource(getUriIcon(".exe_ok")));
                addIconToDictionary(".xls_ok", BitmapToBitmapSource(getUriIcon(".xls_ok")));
                addIconToDictionary(".xlsx_ok", BitmapToBitmapSource(getUriIcon(".xlsx_ok")));
                addIconToDictionary(".pdf_ok", BitmapToBitmapSource(getUriIcon(".pdf_ok")));
                addIconToDictionary(".properties_ok", BitmapToBitmapSource(getUriIcon(".properties_ok")));

                addIconToDictionary(".rar_ok", BitmapToBitmapSource(getUriIcon(".rar_ok")));
                addIconToDictionary(".zip_ok", BitmapToBitmapSource(getUriIcon(".zip_ok")));
                addIconToDictionary(".7z_ok", BitmapToBitmapSource(getUriIcon(".7z_ok")));
                addIconToDictionary(".tar_ok", BitmapToBitmapSource(getUriIcon(".tar_ok")));
                addIconToDictionary(".tz_ok", BitmapToBitmapSource(getUriIcon(".tz_ok")));
                addIconToDictionary(".gz_ok", BitmapToBitmapSource(getUriIcon(".gz_ok")));
                addIconToDictionary(".xml_ok", BitmapToBitmapSource(getUriIcon(".xml_ok")));


                addIconToDictionary(".jpeg_ok", BitmapToBitmapSource(getUriIcon(".jpeg_ok")));
                addIconToDictionary(".jpg_ok", BitmapToBitmapSource(getUriIcon(".jpg_ok")));
                addIconToDictionary(".png_ok", BitmapToBitmapSource(getUriIcon(".png_ok")));
                addIconToDictionary(".svg_ok", BitmapToBitmapSource(getUriIcon(".svg_ok")));
                addIconToDictionary(".tiff_ok", BitmapToBitmapSource(getUriIcon(".tiff_ok")));
                addIconToDictionary(".gif_ok", BitmapToBitmapSource(getUriIcon(".gif_ok")));
                addIconToDictionary(".bmp_ok", BitmapToBitmapSource(getUriIcon(".bmp_ok")));
                addIconToDictionary(".ico_ok", BitmapToBitmapSource(getUriIcon(".ico_ok")));
                addIconToDictionary(".psd_ok", BitmapToBitmapSource(getUriIcon(".psd_ok")));
                addIconToDictionary(".pcx_ok", BitmapToBitmapSource(getUriIcon(".pcx_ok")));




                addIconToDictionary(".properties", BitmapToBitmapSource(getUriIcon(".properties")));

                addIconToDictionary(".xml", BitmapToBitmapSource(getUriIcon(".xml")));
                addIconToDictionary(".buttonCloud", BitmapToBitmapSource(getUriIcon(".buttonCloud")));
                addIconToDictionary(".buttonSync", BitmapToBitmapSource(getUriIcon(".buttonSync")));
                addIconToDictionary(".buttonLink", BitmapToBitmapSource(getUriIcon(".buttonLink")));

                addIconToDictionary(".buttonNavigationBack", BitmapToBitmapSource(getUriIcon(".buttonNavigationBack")));
                addIconToDictionary(".buttonNavigationForward", BitmapToBitmapSource(getUriIcon(".buttonNavigationForward")));


                addIconToDictionary(".menuItemChangeUser", BitmapToBitmapSource(getUriIcon(".menuItemChangeUser")));
                addIconToDictionary(".menuItemExitProgramm", BitmapToBitmapSource(getUriIcon(".menuItemExitProgramm")));
                addIconToDictionary(".menuItemSystemSetting", BitmapToBitmapSource(getUriIcon(".menuItemSystemSetting")));
                addIconToDictionary(".menuItemAllScan", BitmapToBitmapSource(getUriIcon(".menuItemAllScan")));

                addIconToDictionary(".contextMenuItemCutFolder", BitmapToBitmapSource(getUriIcon(".contextMenuItemCutFolder")));
                addIconToDictionary(".contextMenuItemCopyFolder", BitmapToBitmapSource(getUriIcon(".contextMenuItemCopyFolder")));
                addIconToDictionary(".contextMenuItemPasteFolder", BitmapToBitmapSource(getUriIcon(".contextMenuItemPasteFolder")));
                addIconToDictionary(".contextMenuItemDeleteFolder", BitmapToBitmapSource(getUriIcon(".contextMenuItemDeleteFolder")));
                addIconToDictionary(".contextMenuItemRenameFolder", BitmapToBitmapSource(getUriIcon(".contextMenuItemRenameFolder")));
                addIconToDictionary(".contextMenuItemSaveOk", BitmapToBitmapSource(getUriIcon(".contextMenuItemSaveOk")));
                addIconToDictionary(".contextMenuStart", BitmapToBitmapSource(getUriIcon(".contextMenuStart")));
                addIconToDictionary(".contextMenuCreate", BitmapToBitmapSource(getUriIcon(".contextMenuCreate")));
                addIconToDictionary(".contextMenuItemCopyLink", BitmapToBitmapSource(getUriIcon(".contextMenuItemCopyLink")));
                addIconToDictionary(".contextMenuItemGoFolder", BitmapToBitmapSource(getUriIcon(".contextMenuItemGoFolder")));

                addIconToDictionary(".contextMenuPause", BitmapToBitmapSource(getUriIcon(".contextMenuPause")));
                addIconToDictionary(".contextMenuStop", BitmapToBitmapSource(getUriIcon(".contextMenuStop")));
                addIconToDictionary(".attention", BitmapToBitmapSource(getUriIcon(".attention")));
              
                addIconToDictionary(".cloud", BitmapToBitmapSource(getUriIcon(".cloud")));

            }

            return typeIcon;
        }
        //получает BitImage
        private void addIconToDictionary(string nameIcon , BitmapImage bitmapIcon)
        {
            if (typeIcon.ContainsKey(nameIcon) != true)
            {
                if(bitmapIcon != null)
                {
                    typeIcon.Add(nameIcon, bitmapIcon);
                }
                else
                {
                    Console.WriteLine("ConvertIconType->addIconToDictionary: ошибка добавления иконки "+ nameIcon);
                }
                
            }
        }
        //Заполняем словарь ссылок
        private void addUriToDictionary(string nameIcon, string path)
        {
            try
            {

                if (uriIcon.ContainsKey(nameIcon) != true)
                {

                    if (uriIcon != null)
                    {

                        uriIcon.Add(nameIcon, new Uri(path));
                    }
                    else
                    {
                        Console.WriteLine("ConvertIconType->addIconToDictionary: ошибка добавления иконки " + nameIcon);
                    }

                }

            }
            catch(System.UriFormatException a)
            {
                Console.WriteLine(a.ToString());
            }
           
        }
    }
}
