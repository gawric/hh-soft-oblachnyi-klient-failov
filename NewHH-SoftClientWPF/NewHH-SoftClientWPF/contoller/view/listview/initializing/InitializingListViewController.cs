﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.listview.initializing
{
    public class InitializingListViewController
    {

        private IUpdateListView _updateListView;

        public InitializingListViewController(SqliteController _sqlliteController , ConvertIconType convertIcon)
        {
            this._updateListView = new UpdateListViewNodes(_sqlliteController , convertIcon);
        }

        public void initializingListView(SqliteController _sqlliteController, ViewFolderViewModel _listViewFiles, MainWindowViewModel _viewModel , ConvertIconType convertIcon)
        {
            //все данные из бд для root нодов поиск по -1
            List<FileInfoModel> listClient = _sqlliteController.getSelect().GetSqlLiteRootFileInfoList();



            //коллекция для заполнения TreeView
            SortableObservableCollection<FolderNodes> NodesView = _viewModel.NodesView.Items;
            SortableObservableCollection<FolderNodes> ListViewFolder = _listViewFiles.NodesView.Items;

            //Обновляем TreeViewNodes -> items
          
            _updateListView.Start(listClient, NodesView, ListViewFolder, _viewModel.ActiveNodeID, convertIcon);

            listClient = null;
        }
    }
}
