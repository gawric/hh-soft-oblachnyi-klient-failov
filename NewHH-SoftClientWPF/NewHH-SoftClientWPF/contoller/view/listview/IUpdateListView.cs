﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.renameJsonModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.listview
{
    interface IUpdateListView
    {
        SortableObservableCollection<FolderNodes> UpdateNodesLazyListView(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> ListViewNode, long ParentID, MainWindowViewModel _viewModel, ConvertIconType convertIcon);
        void UpdateEmptyListViewNodes(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> ListViewNode, ConvertIconType convertIcon);
        void Start(List<FileInfoModel> listFiles, SortableObservableCollection<FolderNodes> TreeViewNode, SortableObservableCollection<FolderNodes> ListViewNode, long[] ActiveNodeID, ConvertIconType convertIcon);
        void UpdateRecursiveReloadAllListView(SortableObservableCollection<FolderNodes> ListViewNode, SortableObservableCollection<FolderNodes> TreeViewNode, long[] ActiveID, ConvertIconType convertIcon);
    }
}
