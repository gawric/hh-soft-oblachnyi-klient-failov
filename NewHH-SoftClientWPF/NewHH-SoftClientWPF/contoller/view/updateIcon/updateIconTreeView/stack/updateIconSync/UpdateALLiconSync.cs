﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack.updateIconSync
{
    public class UpdateALLiconSync : IUpdateALLIcon
    {
        public void UpdateAllRefresh(ref SyncSinglListModel model , FileInfoModel parentModel)
        {
            string type = GetType(ref model, parentModel);
            string newtype = GetNewTypeRefresh(ref  model,  type);
            UpdateTypeModel(ref model, parentModel, newtype);

            model._stackUpdateTreeViewIcon.TraverseSingl(model._viewModelMain.NodesView.Items[0].Children, parentModel.row_id, staticVariable.Variable.refreshIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model.rootNodesListView, parentModel.row_id, staticVariable.Variable.refreshIconId);
        }

        public void UpdateAllNormal(ref SyncSinglListModel model, FileInfoModel parentModel)
        {
            string type = GetType(ref model, parentModel);
            string newtype = GetNewTypeNormal(ref model, type);
            UpdateTypeModel(ref model, parentModel, newtype);

            model._stackUpdateTreeViewIcon.TraverseSingl(model._viewModelMain.NodesView.Items[0].Children, parentModel.row_id, staticVariable.Variable.normalIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model.rootNodesListView, parentModel.row_id, staticVariable.Variable.normalIconId);
        }

        private void UpdateTypeModel(ref SyncSinglListModel model, FileInfoModel parentModel , string newtype)
        {
            model._sqlliteController.updateTypeListFiles(parentModel.row_id, newtype);
        }
        private string GetNewTypeRefresh(ref SyncSinglListModel model , string type)
        {
            return model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.refreshIconId);
        }

        private string GetNewTypeNormal(ref SyncSinglListModel model, string type)
        {
            return model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.normalIconId);
        }
        private string GetType(ref SyncSinglListModel model , FileInfoModel parentModel)
        {
            return  model._sqlliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(parentModel.row_id).type;
        }
    }
}
