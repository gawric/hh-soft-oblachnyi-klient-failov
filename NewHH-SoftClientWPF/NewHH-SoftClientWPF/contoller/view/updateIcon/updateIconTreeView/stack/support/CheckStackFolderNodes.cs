﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack.support
{
    public  class CheckStackFolderNodes
    {
        private ConvertIconType _convertIcon;

        public CheckStackFolderNodes(ConvertIconType convertIcon)
        {
            _convertIcon = convertIcon;
        }
        public bool isChildren(FolderNodes folderNodes)
        {
            if (folderNodes.Children.Count > 1) return true;
            else
            {
                //&& вычисляет только 1 проверку если false завершается
                //& проверяет все значения
                if (folderNodes.Children.Count != 0 && folderNodes.Children[0] != null & folderNodes.Children[0].FolderName.Equals("Загрузка")) return false; else { return true; }
            }
        }
        public bool isNameLoading(string folderName)
        {
            bool isLoading = false;
            if (folderName.Equals("Загрузка")) isLoading = true;

            return isLoading;
        }
        public void changeIcon(FolderNodes currentNodes, int newIconId)
        {
           
            string iconName = createIconName(currentNodes.Type);
            string endConType = addIconType(iconName, newIconId);

           
            App.Current.Dispatcher.Invoke(delegate
            {
                //Console.WriteLine("НОВАЯ ИКОНКА " + currentNodes.FolderName + " Иконка " + endConType);
                currentNodes.Icon = _convertIcon.getIconType(endConType);
            });
        }

        public string changeIconReturn(string originalType, int newIconId)
        {

            string iconName = createIconName(originalType);
            string endConType = addIconType(iconName, newIconId);

            return endConType;
        }

        private string createIconName(string type)
        {
            int indexB = 0;
            int indexEnd = type.LastIndexOf("_");

            if (indexEnd == -1) return type;
            else
            {
                return type.Substring(indexB, indexEnd);
            }
        }

        private string addIconType(string iconName, int newIconId)
        {
            return iconName + getStringTypeId(newIconId);
        }

        private string getStringTypeId(int newIconId)
        {
            if (newIconId == staticVariable.Variable.normalIconId) return "";
            else if (newIconId == staticVariable.Variable.refreshIconId) return "_refresh";
            else if (newIconId == staticVariable.Variable.okIconId) return "_ok";
            else return "";
        }

        public bool isSearch(FolderNodes currentNodes, List<long> listSql)
        {
            bool check = false;
            foreach (long listFilesRow_id in listSql)
            {
                if (listFilesRow_id == currentNodes.Row_id) { check = true; break; }
            }

            return check;
        }
    }
}
