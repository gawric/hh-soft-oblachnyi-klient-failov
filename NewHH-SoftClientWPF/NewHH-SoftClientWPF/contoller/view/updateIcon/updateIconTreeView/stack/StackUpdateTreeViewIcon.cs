﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack.support;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack
{
    public class StackUpdateTreeViewIcon
    {
        private Stack<FolderNodes> dirsList;
        private Stack<FolderNodes> dirsTree;
        private ConvertIconType _convertIcon;
        private CheckStackFolderNodes _checkStackFolderNodes;

        public StackUpdateTreeViewIcon(ConvertIconType convertIcon)
        {
            _convertIcon = convertIcon;
            _checkStackFolderNodes = new CheckStackFolderNodes(convertIcon);
            dirsList = new Stack<FolderNodes>();
            dirsTree = new Stack<FolderNodes>();
        }

      
        public void TraverseTree(FolderNodes treeViewList, long[][] list, int newIconId)
        {

            addRootFolder(treeViewList, dirsTree);

            while (dirsTree.Count > 0)
            {
                FolderNodes currentNodes = dirsTree.Pop();
                
                if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
                if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes , dirsTree);

                if (isFoundTreeListNodes(list, currentNodes.Row_id))
                {
                    _checkStackFolderNodes.changeIcon(currentNodes, newIconId);
                }
                
            }

        }

    
        public void TraverseViewList(SortableObservableCollection<FolderNodes> _viewFolderList, int newIconId , long[][] list)
        {

            addRootFolder(_viewFolderList, dirsList);

            while (dirsList.Count > 0)
            {
                FolderNodes currentNodes = dirsList.Pop();

                //if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
                //if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes, dirsList);

                //if(currentNodes.Row_id.Equals(downloadNodes.Row_id))
                // {
                //    _checkStackFolderNodes.changeIcon(currentNodes, newIconId);
                // }

                if (isFoundTreeListNodes(list, currentNodes.Row_id))
                {
                    _checkStackFolderNodes.changeIcon(currentNodes, newIconId);
                }
            }

        }
    

      
        private bool isFoundTreeListNodes(long[][] list, long row_id)
        {
            bool check = false;
            foreach(long[] item in list)
            {
                long row_id_source = item[0];
                if (row_id_source.Equals(row_id)) { check = true; break; }
            }
           
            return check;
        }

        public void TraverseTreeList(SortableObservableCollection<FolderNodes>treeViewList, List<long> listSql, int newIconId)
        {
            change1(treeViewList, listSql, newIconId , 0 , dirsTree);
        }

        public void TraverseChangeViewFolderList(SortableObservableCollection<FolderNodes> viewFolder, List<long> listSql, int newIconId)
        {

            change(viewFolder, listSql, newIconId , 1 , dirsList);

        }

        private void change(SortableObservableCollection<FolderNodes> List, List<long> listSql, int newIconId , int run , Stack<FolderNodes> dirs)
        {
            addRootFolder(List, dirs);
           
            while (dirs.Count > 0)
            {

                FolderNodes currentNodes = dirs.Pop();

                //if (run == 1) Console.WriteLine("Запускаем изменение " + currentNodes.FolderName);
                if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
                if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes, dirs);
                if (_checkStackFolderNodes.isSearch(currentNodes, listSql))
                {
                    _checkStackFolderNodes.changeIcon(currentNodes, newIconId);
                  // Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Размер детей " + dirs.Count);
                    //Console.WriteLine("Получаем на изменение " + currentNodes.FolderName +" row_id "+ currentNodes.Row_id + " c типом " + currentNodes.Type + "newIconId " + newIconId);
                }

               

            }

            //Console.WriteLine("После завершения " + dirs.Count);
        }


        private void change1(SortableObservableCollection<FolderNodes> List, List<long> listSql, int newIconId, int run, Stack<FolderNodes> dirs)
        {
            addRootFolder(List, dirs);

            while (dirs.Count > 0)
            {

                FolderNodes currentNodes = dirs.Pop();

                //if (run == 1) Console.WriteLine("Запускаем изменение " + currentNodes.FolderName);
                if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
                if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes, dirs);
                if (_checkStackFolderNodes.isSearch(currentNodes, listSql))
                {
                    _checkStackFolderNodes.changeIcon(currentNodes, newIconId);
                   // Console.WriteLine("Получаем на изменение " + currentNodes.FolderName + " row_id " + currentNodes.Row_id + " c типом " + currentNodes.Type + "Запустил " + run);
                }



            }
        }

        public void TraverseSingl(SortableObservableCollection<FolderNodes> treeViewList, long row_id , int newIconId)
        {

            addRootFolder(treeViewList, dirsTree);

            while (dirsTree.Count > 0)
            {
                FolderNodes currentNodes = dirsTree.Pop();
                if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
                if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes, dirsTree);
                if (currentNodes.Row_id == row_id) _checkStackFolderNodes.changeIcon(currentNodes, newIconId);

            }

        }

    
        public void TraverseViewSingl(SortableObservableCollection<FolderNodes> _viewFolderList, long row_id, int newIconId)
        {

            addRootFolder(_viewFolderList, dirsList);

            while (dirsList.Count > 0)
            {
                
                FolderNodes currentNodes = dirsList.Pop();

               // Console.WriteLine("StackUpdateTreeViewIcon->TraverseViewSingl: изменяем статус OK " + currentNodes.FolderName+ " : "+ currentNodes.Row_id + " ==  " + row_id);



               // Console.WriteLine("1");
                if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
               // Console.WriteLine("2");
                if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes, dirsList);
              //  Console.WriteLine("3");
                if (currentNodes.Row_id == row_id)
                {
               //     Console.WriteLine("4");
                    _checkStackFolderNodes.changeIcon(currentNodes, newIconId);
                    //Console.WriteLine("StackUpdateTreeViewIcon->TraverseViewSingl: изменяем статус OK " + currentNodes.FolderName + "  " + currentNodes.Row_id);
                   // Console.WriteLine("StackUpdateTreeViewIcon->TraverseViewSingl: искомый ID " + row_id);
                    //Console.WriteLine("StackUpdateTreeViewIcon->TraverseViewSingl: новый статус иконки newIconId " + newIconId);
                }
              //  Console.WriteLine("5");

            }

        }


        public void TraverseTreeSingl(SortableObservableCollection<FolderNodes> _viewTreeList, long row_id, int newIconId)
        {

            addRootFolder(_viewTreeList, dirsTree);

            while (dirsTree.Count > 0)
            {
                FolderNodes currentNodes = dirsTree.Pop();
                if (_checkStackFolderNodes.isNameLoading(currentNodes.FolderName)) return;
                if (_checkStackFolderNodes.isChildren(currentNodes)) addChildren(currentNodes, dirsTree);
                if (currentNodes.Row_id == row_id) _checkStackFolderNodes.changeIcon(currentNodes, newIconId);

            }

        }

        public string changeIconReturn(string originalType, int newIconId)
        {
            return _checkStackFolderNodes.changeIconReturn(originalType, newIconId);
        }

        private void addRootFolder(FolderNodes treeViewList, Stack<FolderNodes> dirs)
        {
            dirs.Push(treeViewList);
        }

        private void addRootFolder(SortableObservableCollection<FolderNodes> treeViewList, Stack<FolderNodes> dirs)
        {
            foreach (FolderNodes item in treeViewList) { dirs.Push(item); }
        }

        private Stack<FolderNodes>  addChildren(FolderNodes treeViewList , Stack<FolderNodes> dirs)
        {
            foreach (FolderNodes item in treeViewList.Children)
            {
                dirs.Push(item);
            }

            return dirs;
        }

       

        
    }
}
