﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create
{
    public interface ICreateFolder
    {
        Task TrainingCreateFolder(FileInfoModel sourceNodes, string folderName);
        string CreateNameTreeViewFolder(ObservableCollection<FolderNodes> listTree);
        string CreateNameListViewFolder(List<FileInfoModel> listTree);

        Dictionary<string, FileInfoModel> CreateFolderListTest(List<System.IO.DirectoryInfo> lFolder, string rootHref , string rootLocalDirectory);

        FileInfoModel[] CreateModelArr(SqliteController _sqlLiteController, FileInfoModel sourceNodes, string folderName);


        event EventHandler<CFModel> eventAddData;

    }
}
