﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support.createlist;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using NewHH_SoftClientWPF.staticVariable;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.create
{
    public class CreateFolderController : AbstractCreateItem , ICreateFolder
    {
        private WebDavClientController webDavController;
      
        public event EventHandler<CFModel> eventAddData;
        private bool isSubscrible = false;
        private bool isSubscribleList = false;
        private SupportSubscribleEvent ss;
        public bool isSubscribleData = false;
        private ISupportCFCConnect scfc;

        public CreateFolderController(Container cont)
        {
            if(cont != null)
            {
                webDavController = cont.GetInstance<WebDavClientController>();
                sql = cont.GetInstance<SqliteController>();
                unjc = cont.GetInstance<UpdateNetworkJsonController>();
                createModel = new CreateModel(cont.GetInstance<ConvertIconType>());
                ss = new SupportSubscribleEvent(this);
                createFolder = new CreateFolder();
                scfc = new SupportCFConnect(createModel, sql);
                allFolderDictionary = new Dictionary<string, long>();
            }
            
        }
        public async Task TrainingCreateFolder(FileInfoModel sourceNodes , string folderName)
        {
            string[] usernameAndPassword = scfc.GetUsername(sql);
            Client client = scfc.CreateConnect(usernameAndPassword[0], usernameAndPassword[1],webDavController);
            Item folder = await GetFolder(client , sourceNodes);
            SubscribeCreateFolder();
            createFolder.Create(ref folder , ref client , ref sourceNodes , ref folderName );
            FileInfoModel[] arr = updateNetworkClient(sql, sourceNodes, folderName);
        }

        
        private void SubscribeCreateFolder()
        {
            try
            {
                if(!isSubscrible)
                {
                    isSubscrible = true;
                    this.eventBegin += ss.AddItemBegin;
                    this.eventEnd += ss.AddItemEnd;
                }
               
            }
            catch(System.ArgumentException a)
            {
                Debug.Print("CreateFolderController->SubscribeCreateFolder: "+ a.ToString());
            }
         
        }

        private void SubscribeCreateListFolder(ICreateFolderList c)
        {
            try
            {
                if (!c.IsEvent())
                {
                    SupportSubscribleEvent ss1 = new SupportSubscribleEvent(this);
                    isSubscribleList = true;
                    c.eventBegin += ss1.AddItemBegin;
                    c.eventUpdateData += ss1.AddItemUpdate;
                    c.eventEnd += ss1.AddItemEnd;
                    c.SetEvent(true);
                }
                else
                {
                    Debug.Print("CreateFolderController->SubscribeCreateListFolder: не критическая ошибки. Мы не подписались на обновления !!!");
                }

            }
            catch (System.ArgumentException a)
            {
                Debug.Print("CreateFolderController->SubscribeCreateFolder: " + a.ToString());
            }

        }



        public virtual void OnThresholdData(CFModel e)
        {
            eventAddData?.Invoke(this, e);
        }
    
       private async Task<Item> GetFolder(Client client , FileInfoModel sourceNodes)
       {
            if (client == null) return null;  
            return await client.GetFolder(sourceNodes.location);
       }
       
       public string CreateNameTreeViewFolder(ObservableCollection<FolderNodes> listTree)
       {
            return createFolder.CreateNameTreeViewFolder(listTree);
       }

        public string CreateNameListViewFolder(List<FileInfoModel> listTree)
        {
            return createFolder.CreateNameListViewFolder(listTree);
        }


        private FileInfoModel[] updateNetworkClient(SqliteController _sqlLiteController, FileInfoModel sourceNodes, string folderName)
        {
            long versionBases = scfc.GetVersionBases();

            if(unjc != null)
            {
                AddBegin(versionBases, null);

                FileInfoModel[] arr = scfc.CreateModelArr(_sqlLiteController, sourceNodes, folderName);
                AddEnd(versionBases, arr);
                return arr;
            }


            return null;
        }

       

        public FileInfoModel[] CreateModelArr(SqliteController sql, FileInfoModel sourceNodes, string folderName)
        {
            return scfc.CreateModelArr(sql, sourceNodes, folderName);
        }

     
        public Dictionary<string, FileInfoModel> CreateFolderListTest(List<System.IO.DirectoryInfo> lFolder , string rootLocalHref, string rootWebDavLocation)
        {
            ICreateFolderList listCF = CreateObject();
            return StartFolderList(lFolder, listCF, rootLocalHref, rootWebDavLocation);
        }

        private ICreateFolderList CreateObject()
        {
            return CreateFolderList
                .CreateBuilder()
                .SetSql(sql)
                .SetTempDict(allFolderDictionary)
                .SetUpdateJsonController(unjc)
                .SetClientWdClient(GetConnectWebDav())
                .SetCreateFolder(createFolder)
                .Build();

        }

        private Dictionary<string, FileInfoModel> StartFolderList(List<DirectoryInfo> lFolder , ICreateFolderList listCF, string rootLocalHref, string rootWebDavLocation)
        {
            SubscribeCreateListFolder(listCF);
            return listCF.Create(lFolder, rootLocalHref, rootWebDavLocation);
            //Debug.Print("");
        }

        private Client GetConnectWebDav()
        {
            string[] usernameAndPassword = scfc.GetUsername(sql);
            return scfc.CreateConnect(usernameAndPassword[0], usernameAndPassword[1], webDavController);
        }
    }
}
