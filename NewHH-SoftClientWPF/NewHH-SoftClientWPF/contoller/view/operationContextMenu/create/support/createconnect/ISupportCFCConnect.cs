﻿using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support
{
    public interface ISupportCFCConnect
    {
        long GetVersionBases();
        Client CreateConnect(string login, string password, WebDavClientController _webDavController);

        string[] GetUsername(SqliteController sql);
        FileInfoModel[] CreateArr(long new_row_id, string filename, string newlocation, long parent_row_id);

        FileInfoModel[] CreateModelArr(SqliteController _sqlLiteController, FileInfoModel sourceNodes, string folderName);
    }
}
