﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support
{
    public class SupportCFConnect : ISupportCFCConnect
    {
        public CreateModel createModel;
        public SqliteController sql;
        public SupportCFConnect(CreateModel createModel , SqliteController sql)
        {
            this.createModel = createModel;
            this.sql = sql;
        }
        public FileInfoModel[] CreateModelArr(SqliteController _sqlLiteController, FileInfoModel sourceNodes, string folderName)
        {
            long new_row_id = _sqlLiteController.getSelect().getLastRow_idFileInfoModel() + 1;

            new_row_id = staticVariable.Variable.generatedNewRowId(new_row_id, null);
            _sqlLiteController.getInsert().insertListFilesTempIndex(new_row_id);
            //приводим в url вид
            string convertNamefolderName = staticVariable.Variable.EscapeUrlString(folderName);
            string new_location = sourceNodes.location + convertNamefolderName + "/";

            FileInfoModel[] arr = CreateArr(new_row_id, folderName, new_location, sourceNodes.row_id);

            return arr;
        }
        public FileInfoModel[] CreateArr(long new_row_id, string filename, string newlocation, long parent_row_id)
        {
            long versionBases = GetVersionBases();
            string date1 = new DateTime().ToString();

            FileInfoModel fileInfo = createModel.createFileInfoModel(new_row_id, filename, date1, date1, date1, "", newlocation, parent_row_id, "folder", 0, versionBases, 0, 0, "CREATE", 0);
            FileInfoModel[] arr = new FileInfoModel[1];
            arr[0] = fileInfo;

            return arr;
        }

        public string[] GetUsername(SqliteController _sqlLiteController)
        {
            if (_sqlLiteController == null) return new string[2];

            return _sqlLiteController.getSelect().GetUserSqlite();

        }

        public Client CreateConnect(string login, string password, WebDavClientController _webDavController)
        {
            if (_webDavController == null) return null;

            return _webDavController.CreateClient(login, password);

        }

        public long GetVersionBases()
        {
            if (sql != null) return sql.getSelect().getSqlLiteVersionDataBasesClient();

            return 0;

        }

       
    }
}
