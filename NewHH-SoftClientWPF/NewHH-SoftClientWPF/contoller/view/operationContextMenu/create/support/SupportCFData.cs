﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support
{
    public class SupportCFData
    {
        private SqliteController sql;

        public SupportCFData(SqliteController sql)
        {
            this.sql = sql;
        }
        public FileInfoModel getRootAutoSaveModel(string rootAutSaveLocation)
        {
            if (sql != null) return sql.getSelect().getSearchNodesByLocation(rootAutSaveLocation);
            return null;
        }

        public List<FileInfoModel> getChildrenAutoSaveModel(long row_id)
        {
            return sql.getSelect().GetSqlLiteParentIDFileInfoList(row_id);
        }

        public void clearListScanFolder()
        {
            sql.getDelete().ClearListScanFolder();
        }

        public void insertCreateFolderData(FileInfoModel[] arr)
        {
            sql.getInsert().InsertAutoSaveFileTransaction(arr, 0);
        }
        public bool isExistsRowId(long row_id)
        {
            return sql.getSelect().isExistsRowId(row_id);
        }

        public bool isExistsClearListScanFolder()
        {
            return sql.getSelect().isExistsClearListScanFolder();
        }

        public void updateScanFolderLocation(string location, long row_id)
        {
            sql.updateScanFolderLocation(location, row_id);
        }

       


    }
}
