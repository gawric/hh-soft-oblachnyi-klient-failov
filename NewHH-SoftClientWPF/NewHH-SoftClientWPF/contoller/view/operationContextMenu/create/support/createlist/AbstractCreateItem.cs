﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support.createlist
{
    public abstract class AbstractCreateItem
    {
        public Dictionary<string, long> allFolderDictionary;
        public SqliteController sql;
        public UpdateNetworkJsonController unjc;
        public event EventHandler<CFModel> eventBegin;
        public event EventHandler<CFModel> eventEnd;
        public event EventHandler<CFModel> eventUpdateData;
        public CreateModel  createModel;
        public CreateFolder createFolder;
        public bool isEvent = false;
        public FileInfoModel SetOther(HrefModel hModel, ref FileInfoModel model)
        {
            model.filename = hModel.filename;
            model.lastOpenDate = hModel.lastOpenDate;
            model.createDate = hModel.createDate;
            model.changeDate = hModel.changeDate;
            model.sizebyte = hModel.size;
            model.changeRows = "CREATE";
            model.attribute = ""; ;
            model.type = hModel.type;

            return model;
        }

        public long getLastRowId()
        {
            long row_id = 0;
            if (sql != null) row_id = sql.getSelect().getLastRow_idFileInfoModel();

            return row_id;
        }

        public long getVersionBases()
        {

            if (sql != null) return sql.getSelect().getSqlLiteVersionDataBasesClient();

            return 0;
        }

        public long getUserId()
        {
            if (sql != null) return sql.getSelect().existUserIdToListFiles();
            return 0;
        }

        protected virtual void OnThresholdBegin(CFModel e)
        {
            eventBegin?.Invoke(this, e);
        }

        protected virtual void OnThresholdEnd(CFModel e)
        {
            eventEnd?.Invoke(this, e);
        }

        protected virtual void OnThresholdUpdate(CFModel e)
        {
            eventUpdateData?.Invoke(this, e);
        }

        public void AddBegin(long version, FileInfoModel[] arr)
        {
            CFModel args = new CFModel();
            args.version = version;
            args.arr = arr;
            args.unjc = unjc;
            OnThresholdBegin(args);
        }

        public void AddEnd(long version, FileInfoModel[] arr)
        {
            CFModel args = new CFModel();
            args.version = version;
            args.arr = arr;
            args.unjc = unjc;
            OnThresholdEnd(args);
        }

        public void AddUpdate(long version, FileInfoModel[] arr)
        {
            CFModel args = new CFModel();
            args.version = version;
            args.arr = arr;
            args.unjc = unjc;
            OnThresholdUpdate(args);
        }

    }
}
