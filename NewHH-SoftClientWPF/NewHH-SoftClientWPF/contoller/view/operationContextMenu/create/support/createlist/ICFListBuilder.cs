﻿using NewHH_SoftClientWPF.contoller.sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support.createlist
{
    public interface ICFListBuilder
    {
        void SetSql(SqliteController sql);
    }
}
