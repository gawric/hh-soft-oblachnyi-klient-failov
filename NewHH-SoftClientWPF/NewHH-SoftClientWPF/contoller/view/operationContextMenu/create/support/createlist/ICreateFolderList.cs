﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support.createlist
{
    public interface ICreateFolderList
    {
        Dictionary<string, FileInfoModel> Create(List<System.IO.DirectoryInfo> lFolder, string rootLocalHref, string rootWebDavLocation);

         event EventHandler<CFModel> eventBegin;
         event EventHandler<CFModel> eventEnd;
         event EventHandler<CFModel> eventUpdateData;
         bool IsEvent();
         void SetEvent(bool status);
    }
}
