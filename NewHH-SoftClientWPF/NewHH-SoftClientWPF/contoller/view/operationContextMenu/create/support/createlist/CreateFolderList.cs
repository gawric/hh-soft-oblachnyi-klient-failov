﻿using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support.createlist;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using NewHH_SoftClientWPF.staticVariable;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support
{
    public class CreateFolderList: AbstractCreateItem , ICreateFolderList
    {
        
        private List<FileInfoModel> lFim;
        private Client client;
        //private CreateFolder createFolder;
        private string currentParent = "";
        private Dictionary<string, FileInfoModel> dicAllItem;
        public Dictionary<string, FileInfoModel> Create(List<System.IO.DirectoryInfo> lFolder , string rootLocalHref, string rootWebDavLocation)
        {
            CreateTempObj(ref lFim, ref dicAllItem);
            AddBegin(getVersionBases(), null);

            foreach (DirectoryInfo di in lFolder)
            {
                HrefModel hModel = UtilMethod.GetFileInfoParent(di.FullName);
                FileInfoModel fim = GetModelFim(ref hModel, rootLocalHref, rootWebDavLocation);

                if(fim == null) throw new NullReferenceException();

                CreateFolder(currentParent, fim.filename).Wait();
                AddItem(fim, lFim , hModel);
                SendSize1000(lFim);
            }

            AddEnd(getVersionBases(), lFim.ToArray());

            return dicAllItem;
        }

        private void CreateTempObj(ref List<FileInfoModel>  lFim , ref Dictionary<string, FileInfoModel> dicAllItem)
        {
            lFim = new List<FileInfoModel>();
            dicAllItem = new Dictionary<string, FileInfoModel>();
        }

        
        private void AddItem(FileInfoModel fim , List<FileInfoModel> lFim , HrefModel hModel)
        {
            lFim.Add(fim);
            dicAllItem.Add(hModel.href , fim);
        }

     
        private async Task CreateFolder(string  locationParent, string createfn)
        {
            Item folder = await GetFolderStr(client, locationParent);
            createFolder.CreateFolderStr(ref folder, ref client, locationParent , ref createfn);
        }

       
        private async Task<Item> GetFolderStr(Client client, string updaloadLocation)
        {
            if (client == null) return null;
            return await client.GetFolder(updaloadLocation);
        }

        private FileInfoModel GetModelFim(ref HrefModel hModel , string rootLocalHref, string rootWebDavLocation)
        {
            FileInfoModel model = new FileInfoModel();
            model = SetOther(hModel, ref model);
            SetRootLocal(ref hModel, rootLocalHref);
            model.location = UtilMethod.ConvertHrefToLocation(hModel.href, hModel.rootHref, rootWebDavLocation);
            model.row_id = staticVariable.Variable.generatedNewRowId(getLastRowId(), null);

            string pasteHref = GetPasteHref(hModel, rootLocalHref);

            FileInfoModel parent = SetParent(pasteHref, ref model , rootLocalHref , rootWebDavLocation);

            if (parent == null) return null;
            model.versionUpdateBases = getVersionBases();
            model.user_id = getUserId();

            return model;
        }

        private string GetPasteHref(HrefModel hModel , string rootLocalHref)
        {
            if (hModel.href.Equals(rootLocalHref))
            {
                return hModel.href;
            }

            return hModel.pasteHref;
        }

        private void SetRootLocal(ref HrefModel hModel , string rootLocalDirectory)
        {
            hModel.rootHref = rootLocalDirectory;
        }

      

        private FileInfoModel SetParent(string pasteHref, ref FileInfoModel model , string rootPath , string rootWebDavLocation)
        {
            HrefModel pModel = UtilMethod.GetFileInfoParent(pasteHref);
            pModel.rootHref = rootPath;

            if (!pModel.rootHref.Equals(""))
            {
                currentParent = UtilMethod.ConvertHrefToLocation(pModel.href, pModel.rootHref, rootWebDavLocation);
                
                Debug.Print("CreateFolderList->SetParent новые parentLocation " + currentParent);
                model.parent = searchParentFileInfo(currentParent, model);
            }


             return model;
        }

       

      
        private void SendSize1000(List<FileInfoModel> lFim)
        {
            if (lFim.Count > staticVariable.Variable.CountFilesTrasferServer)
            {
                AddUpdate(getVersionBases(), lFim.ToArray());
                ClearList(lFim);
            }
        }

        private void ClearList(List<FileInfoModel> lFim)
        {
            lFim.Clear();
        }

        //allFolderDictionary - хранятся все папки найденные в рутовой папке
        //что-бы файлы могли найти своих родителей
        private long searchParentFileInfo(string parentLocation, FileInfoModel model)
        {


            if (sql != null)
            {
                FileInfoModel parentSql = sql.getSelect().getSearchNodesByLocation(parentLocation);

                if (parentSql != null)
                {
                    if (allFolderDictionary.ContainsKey(parentLocation) != true)
                    {
                        Debug.Print("CreateFolderList->searchParentFileInfo: Доавлено parentLocation " + parentLocation);
                        allFolderDictionary.Add(parentLocation, parentSql.row_id);
                    }

                    if (allFolderDictionary.ContainsKey(model.location) != true)
                    {
                        Debug.Print("CreateFolderList->searchParentFileInfo: Доавлено model.location " + model.location);
                        allFolderDictionary.Add(model.location, model.row_id);
                    }


                    return parentSql.row_id;
                }
                else
                {

                    if (allFolderDictionary.ContainsKey(model.location) != true)
                    {
                        allFolderDictionary.Add(model.location, model.row_id);
                    }



                    return allFolderDictionary[parentLocation];

                }
            }
            else
            {
                return 0;
            }

        }

        

        public void SetSql(SqliteController sql)
        {
            this.sql = sql;
        }

        public void SetTempDict(Dictionary<string, long> allFolderDictionary)
        {
            this.allFolderDictionary = allFolderDictionary;
        }

        public void SetUpdateJsonController(UpdateNetworkJsonController unjc)
        {
            this.unjc = unjc;
        }

        public void SetClientWd(Client client)
        {
            this.client = client;
        }

        public void SetCreateFolder(CreateFolder createFolder)
        {
            this.createFolder = createFolder;
        }

        

        public static CreateFolderBuilder CreateBuilder()
        {
            return new CreateFolderBuilder();
        }

        public bool IsEvent()
        {
            return isEvent;
        }

        public void SetEvent(bool status)
        {
            isEvent = status;
        }
    }
}
