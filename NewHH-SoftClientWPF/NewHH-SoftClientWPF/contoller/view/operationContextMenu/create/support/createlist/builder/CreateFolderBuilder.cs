﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support.createlist
{
    public class CreateFolderBuilder
    {
        private CreateFolderList cfl;
        private SqliteController sql;
        public CreateFolderBuilder()
        {
            cfl = new CreateFolderList();
        }

        public CreateFolderBuilder SetSql(SqliteController sql)
        {
            cfl.SetSql(sql);
            return this;
        }

        public CreateFolderBuilder SetTempDict(Dictionary<string, long> allFolderDictionary)
        {
            cfl.SetTempDict(allFolderDictionary);
            return this;
        }

        public CreateFolderBuilder SetUpdateJsonController(UpdateNetworkJsonController unjc)
        {
            cfl.SetUpdateJsonController(unjc);
            return this;
        }

        public CreateFolderBuilder SetClientWdClient(Client clientwd)
        {
            cfl.SetClientWd(clientwd);
            return this;
        }

        public CreateFolderBuilder SetCreateFolder(CreateFolder createFolder)
        {
            cfl.SetCreateFolder(createFolder);
            return this;
        }

        public ICreateFolderList Build()
        {
            return cfl;
        }
    }
}
