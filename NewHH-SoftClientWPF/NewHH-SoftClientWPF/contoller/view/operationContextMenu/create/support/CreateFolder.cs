﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support
{
    public class CreateFolder
    {
       
        public void Create(ref Item folder, ref Client client, ref FileInfoModel parentNodes, ref string createFoldername)
        {
            try
            {
                if (folder != null)
                {
                    client.CreateDir(parentNodes.location, createFoldername).Wait();
                }
                else
                {
                    Console.WriteLine("CreateFolderController->TrainingCreateFolder: Ошибка создания папки!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CreateFolderController->TrainingCreateFolder: Ошибка WebDav не смог создать директорию    \n" + ex + " Директория: " + parentNodes.location + "Имя директории: " + createFoldername);
            }
        }

        public void CreateFolderStr(ref Item folder, ref Client client, string locationParent, ref string createFoldername)
        {
            try
            {
                if (folder != null)
                {
                    client.CreateDir(locationParent, createFoldername).Wait();
                }
                else
                {
                    Console.WriteLine("CreateFolderController->TrainingCreateFolder: Ошибка создания папки!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CreateFolderController->TrainingCreateFolder: Ошибка WebDav не смог создать директорию    \n" + ex + " Директория: " + locationParent + "Имя директории: " + createFoldername);
            }
        }


        public string CreateNameTreeViewFolder(ObservableCollection<FolderNodes> listTree)
        {
            string nameFolder = "Новая папка";

            foreach (FolderNodes item in listTree)
            {
                if (item.FolderName.Equals("Новая папка"))
                {
                    nameFolder = "Новая папка 999";
                }

            }

            return nameFolder;

        }

        public string CreateNameListViewFolder(List<FileInfoModel> listTree)
        {
            string nameFolder = "Новая папка";

            foreach (FileInfoModel item in listTree)
            {
                if (item.filename.Equals("Новая папка"))
                {
                    nameFolder = "Новая папка 999";
                }

            }

            return nameFolder;

        }

    }
}
