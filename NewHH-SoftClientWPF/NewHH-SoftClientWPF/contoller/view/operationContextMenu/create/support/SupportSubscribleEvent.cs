﻿using NewHH_SoftClientWPF.contoller.operationContextMenu.create;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.createFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.operationContextMenu.create.support
{
    public class SupportSubscribleEvent
    {
        private CreateFolderController cfc;

        public SupportSubscribleEvent(CreateFolderController cfc)
        {
            this.cfc = cfc;
        }

        public void AddItemBegin(object sender, CFModel data)
        {
            AddData(data.arr);
            data.unjc.updateSendJsonSINGLBEGINUPDATEPARTIES(null, data.version);
        }

        public void AddItemEnd(object sender, CFModel data)
        {
            AddData(data.arr);
            data.unjc.updateSendJsonSINGLENDUPDATEPARTIES(data.arr, data.version);
        }

        public void AddItemUpdate(object sender, CFModel data)
        {
            AddData(data.arr);
            data.unjc.updateSendJsonSINGLUPDATEPARTIES(data.arr, data.version);
        }

        public void AddData(FileInfoModel[] arr)
        {
            CFModel args = new CFModel();
            args.arr = arr;
            cfc.OnThresholdData(args);
        }


  
    }
}
