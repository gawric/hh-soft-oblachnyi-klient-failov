﻿using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer
{
    public interface IContextMenuPause
    {
        void startTrainingPause(IList list, bool[] block, FolderNodesTransfer itemList);
        bool isBlockingUpload(string text);

    }
}
