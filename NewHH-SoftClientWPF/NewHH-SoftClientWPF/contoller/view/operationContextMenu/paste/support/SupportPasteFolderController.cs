﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.paste
{
    class SupportPasteFolderController
    {
        SupportPasteControllerAllFunction otherFunction;

        public SupportPasteFolderController()
        {
            otherFunction = new SupportPasteControllerAllFunction();
        }
        //new_id генерируем по новой т.к мы отправляем несколько файлов 
        //мы не можем сразу получить актуальный id поэтому мы ее сохроняем в переменную и вытаскиваем при след проходе
        //или если контайнер равен 0 мы получаем актуальный из базы данных
        private long GeneRateNewId(PasteFilesAsyncModel PatsteModel , long[] new_id_container)
        {
            if(new_id_container[0] != 0)
            {
                return new_id_container[0] + 1;
            }
            else
            {
                new_id_container[0]  = PatsteModel._sqlLiteController.getSelect().getLastRow_idFileInfoModel() + 1 ;
                return new_id_container[0];
            }
        }
        
        public void getDataFolder(PasteFilesAsyncModel PatsteModel , long[][] allChildrenList , long[] new_id_container)
        {

            try
            {


                //создание копии с новыми id
                //получаем список всех детей arr[0] - arr2[0] - oldid
                //                                    arr2[1] - isFolder
                //                                    arr2[2] - newId
                //                                    arr2[3] - oldParent
                for (int k = 0; k < allChildrenList.Length; k++)
                {
                    long oldId = allChildrenList[k][0];
                    FileInfoModel oldFileInfo = PatsteModel._sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(oldId);

                    if (k == 0)
                    {
                        //присваиваем новый row_id
                        new_id_container[0] = GeneRateNewId(PatsteModel, new_id_container);
                        //long oldId = allChildrenList[k][0];
                        long[] row_new_id = otherFunction.addPasteFirstNewId(new_id_container[0], allChildrenList[k]);

                        allChildrenList[k] = row_new_id;
                        //0 - текущий выбранный элемент ему присваиваем parent(Вставляемого обьекта)
                        allChildrenList[k][3] = PatsteModel.pasteFolder.Row_id;
                    }
                    else
                    {
                        //прошлая модель и текущая модель
                        //берем из нее row_id и начинаем увеличивать каждую итерацию
                        long[] lastModelServer = allChildrenList[k - 1];
                        long[] currentModelServer = allChildrenList[k];

                        //записываем в контайнер т.к возможно будет несколько папок и в базу сразу мы их не записываем
                        new_id_container[0] = lastModelServer[2] + 1;

                        allChildrenList[k] = otherFunction.addPasteNewId(currentModelServer, lastModelServer);


                        //parent_id из базы присваиваем long массиву
                        allChildrenList[k][3] = oldFileInfo.parent;
                    }

                }

                PatsteModel.status.StatusWorker = "Создание новых файлов";
                PatsteModel.progress.Report(PatsteModel.status);
                //теперь присваиваем им новые parent т.к все id номера поменялись
                //создание копии с новыми id
                //получаем список всех детей arr[0] - arr2[0] - oldid
                //                                    arr2[1] - isFolder
                //                                    arr2[2] - newId
                //                                    arr2[3] - newParent
                for (int k = 0; k < allChildrenList.Length; k++)
                {
                    // 0 -элемет самый первый мы его не трогаем только меняем ему id в прошлой итерации и уже присвоили нормальный парент id
                    if (k == 0)
                    {
                        //0 - это текущий выбранный элемент т.е copy
                    }
                    else
                    {

                        // allChildrenList[k] = addPasteNewId(allChildrenList[k], allChildrenList[k - 1]);
                        long oldParent = allChildrenList[k][3];
                        long[] serchChild = otherFunction.serachOldId(allChildrenList, oldParent);
                        allChildrenList[k][3] = serchChild[2];
                    }

                }


                //все дети папки - куда будет вставлен обьект 
                List<FileInfoModel> listAllPasteNodes = PatsteModel._sqlLiteController.getSelect().GetSqlLiteParentIDFileInfoList(PatsteModel.pasteFolder.Row_id);

                //Ищем FIleInfoMode в нашей базе данных (откуда копируем) только ее
                FileInfoModel copyFileInfo = PatsteModel._sqlLiteController.getSelect().getSearchNodesByLocation(PatsteModel.copyLocation);

                for (int h = 0; h < allChildrenList.Length; h++)
                {
                    long oldId = allChildrenList[h][0];
                    long newid = allChildrenList[h][2];

                    //ищем ее в листе всех файлов
                    if (copyFileInfo.row_id == oldId)
                    {
                        //ищем что папка (откуда копируем) найденна в папке куда копируем т.е мы ее будем заменять(не нужно вставлять заново)
                        if (checkDublicationName(copyFileInfo.filename, listAllPasteNodes, allChildrenList, newid))
                        {


                            //делаем из нее null что-бы скрипт пропустил ее
                            //не добавлял
                            //очень коварное решение плохо работает если мы заменяем файл
                            //т.к удаление очищает эти папки и здесь же мы второй раз их не вставляем 
                            //т.е удаление происзодит 2 раза в сканировании и удалении
                            //allChildrenList[h] = null;
                        }
                    }


                }


            }
            catch (System.Data.SQLite.SQLiteException g)
            {
                throw;
            }
            catch (AggregateException e)
            {
                throw;
            }


        }

       

        //проверка на дубль в вставляемом каталоге
        private bool checkDublicationName(string filename, List<FileInfoModel> listAllPasteNodes, long[][] allChildrenList, long newId)
        {

            bool check = false;
            foreach (FileInfoModel item in listAllPasteNodes)
            {
                //имя папки или файла откуда копируем  - string filename
                //список всех файлов и папок(куда вставляем) - List<> listAllPasteNodes
                //ищем по имени
                if (item.filename.Equals(filename))
                {
                    check = true;
                    //если мы нашли совпадение
                    //мы проходим по всем нашим id номера и ищем есть ли у кого в родителях такой id
                    //и заменяем его на row_id той папки куда мы ее копируем, что-бы перепривязать - long[][] allChildrenList
                    for (int w = 0; w < allChildrenList.Length; w++)
                    {
                        long oldParent = allChildrenList[w][3];

                        if (oldParent == newId)
                        {
                            allChildrenList[w][3] = item.row_id;
                        }
                    }
                }
            }

            return check;
        }

    }
}
