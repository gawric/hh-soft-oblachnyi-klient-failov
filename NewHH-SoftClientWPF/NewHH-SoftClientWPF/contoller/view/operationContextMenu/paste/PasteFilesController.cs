﻿using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.delete;
using NewHH_SoftClientWPF.contoller.operationContextMenu.paste.support;
using NewHH_SoftClientWPF.contoller.paste;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.paste
{
    public class PasteFilesController
    {

        private StatusSqlliteTransaction _statusTransaction;
        private WebDavClientController _webDavClientController;
        private SupportPasteControllerAllFunction _supportAllFunction;
        private DeleteController _deleteController;
        private StatusAllOperationModel _allStatus;

        public PasteFilesController(Container cont)
        {
            _statusTransaction = cont.GetInstance<StatusSqlliteTransaction>();
            _webDavClientController = cont.GetInstance<WebDavClientController>();
            _supportAllFunction = new SupportPasteControllerAllFunction();
            _deleteController = cont.GetInstance<DeleteController>();
            _allStatus = cont.GetInstance<StatusAllOperationModel>();
        }


        public async void StartFilesPaste(PasteFilesAsyncModel PasteModel, string Event)
        {
            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            bool isMove = false;
            status = _supportAllFunction.CreateIProgressPasteFiles(status, PasteModel, progress);
            long[] new_id_container = { 0 };

            await checkFolder( PasteModel ,  Event , isMove , new_id_container);

            _allStatus.isPaste = false;

        }

        private async Task checkFolder(PasteFilesAsyncModel PasteModel, string Event , bool isMove , long[] new_id_container)
        {

            if (await isExistFiles(PasteModel))
            {
                long[][][] allEndChildren = CreateChildrebFilesListCopy(PasteModel, Event, new_id_container);
                _webDavClientController.StartCopy(PasteModel, allEndChildren, isMove);
            }
            else
            {
                sendToDeleteArr(await getListLocationDelete(PasteModel), PasteModel);
                updatePStatus(PasteModel);

                Console.WriteLine("PasteFilesController->StartFilesMove: Копирование завершилось аварийно не найден файл на диске");
            }

        }

        private void updatePStatus(PasteFilesAsyncModel PasteModel)
        {
            _supportAllFunction.updateRunningProgressStatus(PasteModel, "Ошибка вставки файлов");
        }
        private async Task<List<string>> getListLocationDelete(PasteFilesAsyncModel PasteModel)
        {
            return  await _webDavClientController.getListNoExist(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);
        }
        private async Task<bool> isExistFiles(PasteFilesAsyncModel PasteModel)
        {
            return  await _webDavClientController.isExistLocation(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);
        }

        public async void StartFilesMove(PasteFilesAsyncModel PasteModel, string Event)
        {
            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            bool isMove = true;
            status = _supportAllFunction.CreateIProgressMoveFiles(status, PasteModel, progress);
            long[] new_id_container = { 0 };


            bool check = await _webDavClientController.isExistLocation(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);


            if(check)
            {
                long[][][] allEndChildren = CreateChildrebFilesListCopy(PasteModel, Event, new_id_container);


                //Подготовили все данные для копирования, отправляем на копирование
                _webDavClientController.StartCopy(PasteModel, allEndChildren, isMove);

                _supportAllFunction.updateRunningProgressStatus(PasteModel, "Очистка старых файлов");

                _supportAllFunction.WaitingTransaction(_statusTransaction);

                ClearAllOtherTempFiles(PasteModel, isMove);
            }
            else
            {
                List<string> listLocationDelete = await _webDavClientController.getListNoExist(PasteModel, PasteModel.allCopyNodes, PasteModel.allPasteNodes);
                sendToDeleteArr(listLocationDelete, PasteModel);

                Console.WriteLine("PasteFilesController->StartFilesMove: Копирование завершилось аварийно не найден файл на диске");
                _supportAllFunction.updateRunningProgressStatus(PasteModel, "Ошибка вставки файлов");
            }

          

            _allStatus.isPaste = false;
        }


        private void sendToDeleteArr(List<string> locationList, PasteFilesAsyncModel PasteModel)
        {
            SendDeleteArrModel deleteModel = new SendDeleteArrModel();
            deleteModel.sqliteController = PasteModel._sqlLiteController;
            deleteModel.locationList = locationList;

            PasteModel.updateNetworkJsonController.sendToDeleteArr(locationList, deleteModel);
        }


        private void CreateTempFilesChildren(List<long[][]> allFiles, PasteFilesAsyncModel PasteModel, string copyNodesLocation, string Event)
        {
            string progressLabel = "Поиск файлов";

            //получаем список всех детей arr[0] - arr2[0] - id
            //                                    arr2[1] - isFolder
            allFiles = PasteModel._sqlLiteController.getAllChildren(allFiles, copyNodesLocation, PasteModel._sqlLiteController, PasteModel.progress, progressLabel);


        }

        private long[][][] CreateChildrebFilesListCopy(PasteFilesAsyncModel PasteModel, string Event, long[] new_id_container)
        {
            List<long[][]> allChildrenList = new List<long[][]>();

            foreach (KeyValuePair<FolderNodes, bool> kvp in PasteModel.allCopyNodes)
            {

                FolderNodes copyNodes = kvp.Key;
                bool replace = kvp.Value;

                CreateTempFilesChildren(allChildrenList, PasteModel, copyNodes.Location, Event);
            }

            //3 уровень всегда равено 0
            long[][][] allFiles = allChildrenList.ToArray();

            long[][] allEnd = new long[allFiles.Length][];

            for (int z = 0; z < allFiles.Length; z++)
            {
                allFiles[z] = GeneratedNewIdFilesChildren(allFiles[z], PasteModel , new_id_container);
            }


            return allFiles;
        }

        private long[][] GeneratedNewIdFilesChildren(long[][] allFiles, PasteFilesAsyncModel PasteModel, long[] new_id_container)
        {
            SupportPasteFolderController supportPaste = new SupportPasteFolderController();
            //подготовка всех данных для отправки на копирования "Создание новых id номеров и создание новых нодов для вставки" ->(изменяем allChildrenList)
            supportPaste.getDataFolder(PasteModel, allFiles, new_id_container);

            return allFiles;
        }




        private void ClearAllOtherTempFiles(PasteFilesAsyncModel PasteModel , bool isMove)
        {
            string EventDelete = "DELETE";
            string deleteStatus = "Выполнено!";

            string progressLabel = "Поиск временных файлов";

            _supportAllFunction.WaitingTransaction( _statusTransaction);
            try
            {
                _deleteController.deleteSql(PasteModel, deleteStatus, true, progressLabel, isMove, EventDelete);
            }
            catch(System.NullReferenceException sf)
            {
                Console.WriteLine(sf);
            }
            

        }


    }
}
