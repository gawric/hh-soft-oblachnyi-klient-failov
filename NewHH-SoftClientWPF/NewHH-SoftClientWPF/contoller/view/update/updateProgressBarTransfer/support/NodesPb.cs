﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support
{
    public class NodesPb
    {
        private ObservableCollection<FolderNodesTransfer> nodesTrasfer;
        private long row_id_transfer;
        private ConvertIconType convertIcon;

        //содержит дополнительные методы для UploadWebDavPb и DownloadWebDavPb
        public NodesPb(ObservableCollection<FolderNodesTransfer> nodesTrasfer , long row_id_transfer, ConvertIconType convertIcon)
        {
           this.nodesTrasfer = nodesTrasfer;
           this.row_id_transfer = row_id_transfer;
           this.convertIcon = convertIcon;
        }

        public FolderNodesTransfer getNodes(long row_id_transfer)
        {
            FolderNodesTransfer nodes = null;
            try
            {
                if (nodesTrasfer != null)
                {
                    for (int s = 0; s < nodesTrasfer.Count; s++)
                    {
                        FolderNodesTransfer searchNodes = nodesTrasfer[s];

                        if (searchNodes.Row_id == row_id_transfer)
                        {
                            nodes = searchNodes;
                        }
                    }
                }
            }
            catch(System.ArgumentOutOfRangeException s)
            {
                Debug.Print("NodesPb->GetNodes: Не критическая ошибка " + s.ToString());
                return nodes;
            }
           


            return nodes;
        }

        public void updateListTransferSqlLite(FolderNodesTransfer nodes, SqliteController sqliteController, string uploadStatus)
        {
            bool check = sqliteController.getSelect().isExistListTransferToRow_id(nodes.Row_id);

            if (check)
            {
                sqliteController.updateListFilesProgress(row_id_transfer, nodes.sizebyte ,  nodes.progress, uploadStatus);
            }
            else
            {
                insertListTransferSqllite(nodes, sqliteController , convertIcon);
            }
        }

        private void insertListTransferSqllite(FolderNodesTransfer nodes, SqliteController sqliteController, ConvertIconType convertIcon)
        {
            FileInfoTransfer[] arr = new FileInfoTransfer[1];
            CreateModel createModel = new CreateModel(convertIcon);
            arr[0] = createModel.convertNodesTransferToFileInfoTransfer(nodes);

            sqliteController.getInsert().InsertFileTransferTransaction(arr);
        }


        public double GetPercent(long b, long a)
        {
            if (b == 0) return 0;

            return (double)(a / (b / 100M));
        }

    }
}
