﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBar.download.supportDownload
{
    public class UpdatePbUploadFolder
    {
        private long row_id_transfer;
        private ScanUploadWebDavModel _scanUploadModel;
        private FolderNodesTransfer nodes;
        private NodesPb nodesPb;
        private UpdatePb updatePb;

        public UpdatePbUploadFolder(long row_id_transfer, ScanUploadWebDavModel _scanUploadModel, FolderNodesTransfer nodes, NodesPb nodesPb)
        {
            this.row_id_transfer = row_id_transfer;
            this._scanUploadModel = _scanUploadModel;
            this.nodes = nodes;
            this.nodesPb = nodesPb;

            updatePb = new UpdatePb(nodesPb, _scanUploadModel._sqlLiteController , nodes , _scanUploadModel.convertIcon);
        }

        public void update(bool check , FolderNodesTransfer nodes)
        {
            this.nodes = nodes;

            if(nodes != null)
            {
                long sizeFilesCount = getCount(nodes);
                int[] currentFilesCount = _scanUploadModel.currentCountFilesToFolder;
                long sizeBytesFolder = _scanUploadModel.allSizeBytesFolder;
                
                updatePb.update(check, sizeFilesCount, currentFilesCount, sizeBytesFolder , 0);
            }
            else
            {
                Console.WriteLine("UpdatePbUploadFolder->update   FolderNodesTransfer nodes не определен. Нужен для определения данных ProgressBar");
            }
           
        }

        public void updateStop()
        {
            updatePb.updateStopRelase();
        }

        private long getCount(FolderNodesTransfer nodes)
        {
            long sizeFilesCount = 0;

           if(_scanUploadModel.allCountFilesToFolder > 0 )
            {
                return sizeFilesCount = _scanUploadModel.allCountFilesToFolder;
            }
            else
            {
                return sizeFilesCount = _scanUploadModel.allCountFilesToFiles;
            }
                
           
                
            

        }
    }
}


