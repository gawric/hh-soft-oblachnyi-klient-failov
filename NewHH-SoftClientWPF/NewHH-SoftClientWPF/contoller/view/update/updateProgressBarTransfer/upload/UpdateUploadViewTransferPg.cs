﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar.download.supportDownload;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.upload.supportUpload;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using WebDAVClient.Progress;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBar
{
    //Обрабатывает ViewTransfer когда происходит заливка файлов обновляет pg
    class UpdateUploadViewTransferPg
    {
        private IDownload statusDownload;
        private ObservableCollection<FolderNodesTransfer> nodesTrasfer;
        private FolderNodesTransfer nodes;
        private long row_id_listFiles;
        private UploadWebDavModel uploadModelWebDave;
        private UploadWebDavObjectModel uploadModelWebObj;
        private ScanUploadWebDavModel uploadModel;
        private long row_id_transfer;
        private UpdatePbUploadFolder checkDownloadpg;
        private NodesPb nodesPb;
        private UpdatePbUploadFiles updatePbUploadFiles;

        public UpdateUploadViewTransferPg(IDownload statusDownload , UploadWebDavObjectModel uploadWabDavModelObj, UploadWebDavModel uploadModelWebDave, long row_id_transfer, long row_id_listFiles)
        {
            this.statusDownload = statusDownload;
            this.nodesTrasfer = uploadWabDavModelObj._transferViewNodes;
            this.row_id_transfer = row_id_transfer;
            this.row_id_listFiles = row_id_listFiles;
            this.uploadModelWebDave = uploadModelWebDave;
            this.uploadModelWebObj = uploadWabDavModelObj;
            //Общий класс для получения нодов для обновления статуса
            nodesPb = new NodesPb(nodesTrasfer, row_id_transfer , uploadWabDavModelObj.convertIcon);
            //Логика обновления файлов, срабатывает когда что-то происходит с файлом(проценты копирования/завершения копирования)
            updatePbUploadFiles = new  UpdatePbUploadFiles(nodesPb, nodes, row_id_transfer, uploadModelWebDave , uploadWabDavModelObj);


        }

        public UpdateUploadViewTransferPg(ScanUploadWebDavModel uploadModel , FolderNodesTransfer modelSourceFolder , UploadWebDavModel uploadModelWebDave , UploadWebDavObjectModel uploadModelWebDavObj)
        {
            this.uploadModel = uploadModel;
            row_id_transfer = modelSourceFolder.Row_id;
            row_id_listFiles = uploadModel.pasteModel.row_id;
            nodesTrasfer = uploadModel._transferViewNodes;
            this.uploadModelWebDave =  uploadModelWebDave;

            //Общий класс для получения нодов для обновления статуса
            nodesPb = new NodesPb(nodesTrasfer, row_id_transfer , uploadModelWebDavObj.convertIcon);
          
     
        }
        //Подписка на Обновление файлов
        public void updateFilesPgUpload()
        {
            if(nodes == null)
            {
                nodes = new FolderNodesTransfer();
            }

            if(nodes != null)
            {
                nodes = nodesPb.getNodes(row_id_transfer);

                statusDownload.Completed += new EventHandler<DownloadStateEventArgs>(updatePbUploadFiles.c_StateComplete);
                statusDownload.StateChanged += new EventHandler<DownloadStateEventArgs>(updatePbUploadFiles.c_StateChanged);
                statusDownload.StateError += new EventHandler<DownloadStateEventArgs>(updatePbUploadFiles.c_StateError);
            }
          
        }



        public void updateFolderPgUpload()
        {
            //Обязательно нужно создавать обьект здесь. Мы можем найти созданную TreeViewNodes
            this.nodes = nodesPb.getNodes(row_id_transfer);
            checkDownloadpg = new UpdatePbUploadFolder(row_id_transfer, uploadModel, this.nodes, nodesPb);
            uploadModel.folderTreeViewPgEventHandle += new EventHandler<UpdateViewTransferFolderPgArgs>(updateFolderPgUpload);
        }


        //Логика обновление копирования папки
        public void updateFolderPgUpload(object sender, UpdateViewTransferFolderPgArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {
                if(e.isStop)
                {
                   checkDownloadpg.updateStop();
                }
                else
                {
                    if (e.errorStatus != true)
                    {
                        bool check = e.training;
                        checkDownloadpg.update(check, nodes);
                    }
                    else
                    {
                        long sizeFilesCount = uploadModel.allCountFilesToFolder;
                        int[] currentFilesCount = uploadModel.currentCountFilesToFolder;

                        errorTraining(sizeFilesCount, currentFilesCount);
                    }
                }
                
               

            }

        }

        

        private void errorTraining(long sizeFilesCount, int[] currentFilesCount)
        {
            double percent = nodesPb.GetPercent(sizeFilesCount, currentFilesCount[0]);
            try
            {
                if (nodes != null)
                {

                    nodes.UploadStatus = "Ошибка";
                    nodes.Progress = percent;
                    nodesPb.updateListTransferSqlLite(nodes, uploadModelWebObj._sqlLiteController, nodes.UploadStatus);

                }


            }
            catch(System.NullReferenceException s)
            {
                Debug.Print("UpdateUploadViewTransferPg->errorTraining: Не критическая ошибка " + s.ToString());
            }
            

        }

       
     

      

       

       
    }
}
