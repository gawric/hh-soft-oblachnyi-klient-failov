﻿using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.download.supportDownload
{
    public class UpdatePbDownloadFiles
    {

        private NodesPb nodesPb;
        private FolderNodesTransfer nodes;
        private long row_id_transfer;
        private DownloadWebDavModel downloadModelWebDav;

        public UpdatePbDownloadFiles(NodesPb nodesPb, FolderNodesTransfer nodes, long row_id_transfer, DownloadWebDavModel downloadModelWebDav)
        {
            this.nodesPb = nodesPb;
            this.nodes = nodes;
            this.row_id_transfer = row_id_transfer;
            this.downloadModelWebDav = downloadModelWebDav;
        }

        public void c_StateComplete(object sender, DownloadStateEventArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {
               
                if (e.isStop)
                {
                    updateStopRelase();
                }
                else
                {
                    nodes.Progress = 100;
                    nodes.UploadStatus = "Выполнено";
                    nodesPb.updateListTransferSqlLite(nodes, downloadModelWebDav._sqlliteController, nodes.UploadStatus);
                }

             
            }

        }

        public void c_StateError(object sender, DownloadStateEventArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {
               
                if (e.isStop)
                {
                    updateStopRelase();
                }
                else
                {
                    nodes.Progress = 0;
                    nodes.UploadStatus = "Ошибка";
                    nodesPb.updateListTransferSqlLite(nodes, downloadModelWebDav._sqlliteController, nodes.UploadStatus);
                }

               
            }

        }

        

        public void c_StateChanged(object sender, DownloadStateEventArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {

                try
                {
                   
                    if (e.isStop)
                    {
                        updateStopRelase();
                    }
                    else
                    {
                        if (nodes.progress != e.percent)
                        {
                            nodes.UploadStatus = "Загрузка";
                            nodes.Progress = e.percent;
                            nodesPb.updateListTransferSqlLite(nodes, downloadModelWebDav._sqlliteController, nodes.UploadStatus);

                        }
                    }
                  

                }
                catch (Exception a)
                {
                    Console.WriteLine("UpdateViewTransferPg ->c_StateChanged  " + a);
                }

            }

        }

        public void updateStopRelase()
        {

            nodes.UploadStatus = "Остановлен";
            nodes.Progress = 0;
            nodes.Sizebyte = staticVariable.UtilMethod.FormatBytes(0);
        }
    }
}
