﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar.download.supportDownload;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.download.supportDownload;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Progress;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.update.updateProgressBar.download
{
    public class UpdateDownloadViewTransferPg
    {
        private IDownload statusDownload;
        private ObservableCollection<FolderNodesTransfer> nodesTrasfer;
        private long row_id_transfer;
        private FolderNodesTransfer nodes;
        private long row_id_listFiles;
        private DownloadWebDavModel downloadModelWebDav;
        private UpdatePbDownloadFolder checkDownload;
        private NodesPb nodesPb;
        private UpdatePbDownloadFiles updatePbFiles;
        //для папок
        public UpdateDownloadViewTransferPg(DownloadWebDavModel downloadModelWebDav, long row_id_transfer, long row_id_listFiles)
        {
           
            this.nodesTrasfer = downloadModelWebDav._transferViewNodes;
            this.row_id_transfer = row_id_transfer;
            this.row_id_listFiles = row_id_listFiles;
            this.downloadModelWebDav = downloadModelWebDav;

            nodesPb = new NodesPb(nodesTrasfer,  row_id_transfer , downloadModelWebDav.convertIcon);
            nodes = nodesPb.getNodes(row_id_transfer);

            checkDownload = new UpdatePbDownloadFolder(row_id_transfer, downloadModelWebDav, nodes,  nodesPb);
        }
        //для файлов
        public UpdateDownloadViewTransferPg(IDownload statusDownload , DownloadWebDavModel downloadModelWebDav , long row_id_transfer)
        {
            this.statusDownload = statusDownload;
            this.nodesTrasfer = downloadModelWebDav._transferViewNodes;
            nodesPb = new NodesPb(nodesTrasfer, row_id_transfer , downloadModelWebDav.convertIcon);
            nodes = nodesPb.getNodes(row_id_transfer);
            updatePbFiles = new UpdatePbDownloadFiles(nodesPb, nodes,  row_id_transfer,  downloadModelWebDav);
        }

        public void updateFolderPgDownload()
        {
            downloadModelWebDav.folderTreeViewPgEventHandle += new EventHandler<UpdateViewTransferFolderPgArgs>(updateFolderPgDownload);
        }

        public void updateFilesPgUpload()
        {
            if (nodes == null)
            {
                nodes = new FolderNodesTransfer();
            }

            if (nodes != null)
            {
                nodes = nodesPb.getNodes(row_id_transfer);

                statusDownload.Completed += new EventHandler<DownloadStateEventArgs>(updatePbFiles.c_StateComplete);
                statusDownload.StateChanged += new EventHandler<DownloadStateEventArgs>(updatePbFiles.c_StateChanged);
                statusDownload.StateError += new EventHandler<DownloadStateEventArgs>(updatePbFiles.c_StateError);
            }

        }

   


        public void updateFolderPgDownload(object sender, UpdateViewTransferFolderPgArgs e)
        {
            nodes = nodesPb.getNodes(row_id_transfer);

            if (nodes != null)
            {
                //Console.WriteLine("Статус "+e.isStop);
                if (e.isStop)
                {
                    checkDownload.updateStop();
                }
                else
                {
                    if (e.errorStatus != true)
                    {
                        bool check = e.training;
                        checkDownload.update(check);
                    }
                    else
                    {
                        long sizeFilesCount = downloadModelWebDav.modelSizebyteAndCount.sizeCountFolder;
                        int[] currentFilesCount = downloadModelWebDav.currentCountFiles;

                        errorTraining(sizeFilesCount, currentFilesCount);
                    }
                }
                    
                    

               


            }
            
        }

        

        private void errorTraining(long sizeFilesCount, int[] currentFilesCount)
        {
            double percent = nodesPb.GetPercent(sizeFilesCount, currentFilesCount[0]);

            if (nodes != null)
            {

                nodes.UploadStatus = "Ошибка";
                nodes.Progress = percent;
                SqliteController sqliteController = downloadModelWebDav._sqlliteController;
                checkDownload.updateListTransferSqlLite(nodes, sqliteController, nodes.UploadStatus);

            }

        }
      
    }
}
