﻿using NewHH_SoftClientWPF.contoller.update.support.observable.subscription;
using NewHH_SoftClientWPF.contoller.update.support.observable.viewfoldertransfer;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar.download;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.contoller.update
{
    public class UpdateViewTransferController
    {
        private ObserverViewTransferUpdate observer;
        private CreateSubscriptionViewTransfer updateHandler;
       

        public UpdateViewTransferController(Container cont)
        {
            observer = cont.GetInstance<ObserverViewTransferUpdate>();
            updateHandler = new CreateSubscriptionViewTransfer();

         
            //подписка на add
            observer.ObserverUpdateHandlerTreeView(updateHandler);
            //подписка на delete
            observer.ObserverDeleteHandlerTreeView(updateHandler);
        }

        public void createProgressBar(FolderNodesTransfer nodesTrasfer, string nameNodes)
        {
            updateHandler.updateViewTrasfer(nodesTrasfer,  nameNodes);
        }

        public void deleteTempProgressBar(long row_id, string location)
        {
            updateHandler.deleteViewTrasfer(row_id, location);
        }

        //Upload ProgressBar
        public void UpdateFilesProgressBar(IDownload statusDownload, UploadWebDavObjectModel uploadWebDavObjectModel , UploadWebDavModel uploadModel, long row_id_transfer , long row_id_listFiles)
        {
            UpdateUploadViewTransferPg updatePg = new UpdateUploadViewTransferPg(statusDownload, uploadWebDavObjectModel, uploadModel, row_id_transfer , row_id_listFiles);
            updatePg.updateFilesPgUpload();
        }


        public void UpdateFolderProgressBar(ScanUploadWebDavModel  uploadModel , UploadWebDavObjectModel uploadWebDavObjectModel ,FolderNodesTransfer modelSource , UploadWebDavModel uploadModelWebDave)
        {
            UpdateUploadViewTransferPg updatePg = new UpdateUploadViewTransferPg(uploadModel , modelSource , uploadModelWebDave , uploadWebDavObjectModel);
            updatePg.updateFolderPgUpload();
        }

        

        //Download ProgressBar


        public void UpdateDownloadFolderProgressBar(DownloadWebDavModel downloadModelWebDav, long row_id_transfer, long row_id_listFiles)
        {
            UpdateDownloadViewTransferPg updatePg = new UpdateDownloadViewTransferPg(downloadModelWebDav, row_id_transfer, row_id_listFiles);
            updatePg.updateFolderPgDownload();
        }

        public void UpdateDownloadFilesProgressBar(IDownload statusDownload , DownloadWebDavModel downloadModelWebDav,  long row_id_transfer)
        {
            UpdateDownloadViewTransferPg updatePg = new UpdateDownloadViewTransferPg(statusDownload,  downloadModelWebDav,  row_id_transfer);
            updatePg.updateFilesPgUpload();
        }



    }
}
