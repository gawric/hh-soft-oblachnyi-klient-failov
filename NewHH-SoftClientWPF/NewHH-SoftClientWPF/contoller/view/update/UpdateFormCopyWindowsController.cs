﻿using NewHH_SoftClientWPF.contoller.update.support.observable;
using SimpleInjector;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update
{
    public class UpdateFormCopyWindowsController
    {
        //промежуточная подписка
        private CreateSubscriptionLoginForm updateHandler;

        //здесь происходит вся магия обновления
        private ObserverCopyWindowUpdate observerCopy;

        public UpdateFormCopyWindowsController(Container cont)
        {
            updateHandler = cont.GetInstance<CreateSubscriptionLoginForm>();
            observerCopy = cont.GetInstance<ObserverCopyWindowUpdate>();

            //Регистрация кто будет слушать сообщения
            observerCopy.ObserverCopyUpdateHandler(updateHandler);
        }

        //вызываем из нужно участка кода для обновления View 
        //[0] - nameViewElements
        //[1] - data Elements
        public void sendUpdate(string nameForm, string[] textMessage)
        {
            updateHandler.updateForm(nameForm, textMessage);
        }

        //Открывает форму(обычно она спрятана)
        public void updateOpenWindowsCopy()
        {
            string nameForm = "CopyForm";
            string nameElemetns = "OpenWindows";
            string data = "open";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }

        //Прячет форму
        public void updateCloseWindowsCopy()
        {
            string nameForm = "CopyForm";
            string nameElemetns = "CloseWindows";
            string data = "close";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }
    }
}
