﻿using NewHH_SoftClientWPF.contoller.update.support.observable;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update
{
   public class UpdateFormLoginController
    {
        //промежуточная подписка
        private CreateSubscriptionLoginForm updateHandler;

        //здесь происходит вся магия обновления
        private ObserverLoginUpdate observer;

        public UpdateFormLoginController(Container cont)
        {
            updateHandler = cont.GetInstance<CreateSubscriptionLoginForm>();
            observer = cont.GetInstance<ObserverLoginUpdate>();

            //Регистрация кто будет слушать сообщения
            observer.ObserverLoginUpdateHandler(updateHandler);
        }

        //вызываем из нужно участка кода для обновления View 
        //[0] - nameViewElements
        //[1] - data Elements
        public void sendUpdate(string nameForm, string[] textMessage)
        {
            updateHandler.updateForm(nameForm, textMessage);
        }

        //обновляет форму ошибку на форме
        public void updateCreatLoginForm(string data)
        {
            string nameForm = "LoginForm";
            string nameElemetns = "CrateAction";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }

        //обновляет форму ошибку на форме
        public void updateErrorLoginForm(string data)
        {
            string nameForm = "LoginForm";
            string nameElemetns = "TextErrorValue";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }

        //обновляет форму текст на кнопке
        public void updateButtonLoginForm(string data)
        {
            string nameForm = "LoginForm";
            string nameElemetns = "TextButtonValue";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }

        //обновляет форму текст на кнопке
        public void updatePgLoginForm(string data)
        {
            string nameForm = "LoginForm";
            string nameElemetns = "ProgressBarValue";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }

        //обновляет форму текст на кнопке
        public void updateCloseLoginForm()
        {
            string nameForm = "LoginForm";
            string nameElemetns = "CloseAction";
            string data = "#";
            string[] textMessage = { nameElemetns, data };

            sendUpdate(nameForm, textMessage);
        }


    }
}
