﻿using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.listview;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.treeview;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.renameJsonModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.support
{
   public  class ObserverMainWindowFormUpdate
    {
        //обновление это модели приводит к изменениям во view
        private MainWindowViewModel _viewModel;
        private ViewFolderViewModel _viewListViewModel;
        private UpdateDataController _updateDataController;
        private SqliteController _sqlliteController;
        private IUpdateTreeView _updateTreeView;
        private IUpdateListView _updateListView;
        private ConvertIconType _convertIcon;


        public ObserverMainWindowFormUpdate(Container cont)
        {
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _updateDataController = cont.GetInstance<UpdateDataController>();
            _sqlliteController = cont.GetInstance<SqliteController>();
            _viewListViewModel = cont.GetInstance<ViewFolderViewModel>();
            _convertIcon = cont.GetInstance<ConvertIconType>();

            _updateTreeView = new UpdateTreeViewNodes(_sqlliteController , _convertIcon);
            _updateListView = new UpdateListViewNodes(_sqlliteController , _convertIcon);
        }

        public void updateView(string nameElemetns, string data)
        {
            if (nameElemetns.Equals("Titlename"))
            {
                _viewModel.Titlename = data;
            } 
           
          
        }

        //Данные получены от сервера listFiles
        //Обновляет данные в treeView
        public async void UpdateMainTreeView(List<FileInfoModel> listFiles)
        {
           
            _updateTreeView.Start(listFiles, _viewModel.NodesView.Items, _viewListViewModel.NodesView.Items, _viewModel.ActiveNodeID , _convertIcon);
        }

        public void UpdateMainListView(List<FileInfoModel> listFiles)
        {
            
            _updateListView.Start(listFiles, _viewModel.NodesView.Items, _viewListViewModel.NodesView.Items, _viewModel.ActiveNodeID, _convertIcon);
        }

        public async void UpdateMainReloadALLTreeView()
        {
           
            _updateTreeView.UpdateRecursiveReloadAllTreeView(_viewListViewModel.NodesView.Items , _viewModel.NodesView.Items , _viewModel.ActiveNodeID , _convertIcon);
            
        }

        public async void UpdateMainReloadALLListView()
        {
           
            _updateListView.UpdateRecursiveReloadAllListView(_viewListViewModel.NodesView.Items, _viewModel.NodesView.Items, _viewModel.ActiveNodeID, _convertIcon);

        }





    }
}
