﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network;
using NewHH_SoftClientWPF.contoller.network.json;

using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.statusWorker;
using NewHH_SoftClientWPF.contoller.sync;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.contoller.view.update.support.observable.getJson.support;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.networkGetJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using Newtonsoft.Json;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
     class ObserverNetwokUpdate
    {
        private WebDavClientController _webDavClient;
        private UpdateFormMainController _updateMainForm;
        private ObserverMainWindowFormUpdate _mainUpdateForm;
        private UpdateDataController _updateDataController;
        private FirstSyncFilesController _firstSyncController;
        private SqliteController _sqlliteController;
        private CopyStatusController _copyStatus;

        private MainWindowViewModel _viewModelMain;
        private AutoSyncController _autoSync;
        public BlockingEventController _blockingEvent;
        private UpdateNetworkJsonController _updateSendController;
        private CancellationController _cancellationController;
        private SaveJsonToDiskContoller _saveJsonToDisk;
        private ReadJsonToDiskController _readJsonToDisk;
        private ViewFolderViewModel _viewModelViewFolder;
        private ConvertIconType _convertIcon;
        private AutoScanInfoModel _autoScanInfoModel;
        private SupportDictionaryResponce _dictResponce;

        public ObserverNetwokUpdate(Container cont)
        {
            _copyStatus = cont.GetInstance<CopyStatusController>();
            _mainUpdateForm = cont.GetInstance<ObserverMainWindowFormUpdate>();
            _updateDataController = cont.GetInstance<UpdateDataController>();
            _firstSyncController = cont.GetInstance<FirstSyncFilesController>();
            _sqlliteController = cont.GetInstance<SqliteController>();

            _updateMainForm = cont.GetInstance<UpdateFormMainController>();
            _viewModelMain = cont.GetInstance<MainWindowViewModel>();
            _autoSync = cont.GetInstance<AutoSyncController>();
            _blockingEvent = cont.GetInstance<BlockingEventController>();
            _updateSendController = cont.GetInstance<UpdateNetworkJsonController>();
            _cancellationController = cont.GetInstance<CancellationController>();
            _saveJsonToDisk = cont.GetInstance<SaveJsonToDiskContoller>();
            _readJsonToDisk = cont.GetInstance<ReadJsonToDiskController>();
            _viewModelViewFolder = cont.GetInstance<ViewFolderViewModel>();
            _convertIcon = cont.GetInstance<ConvertIconType>();
            _webDavClient = cont.GetInstance<WebDavClientController>();
            _autoScanInfoModel = new AutoScanInfoModel();

            GetJsonRequestModel model = createModel(_mainUpdateForm, _updateDataController, _viewModelMain, _updateMainForm,
                _sqlliteController, _copyStatus, _viewModelViewFolder, _convertIcon,
                _updateSendController, _autoScanInfoModel ,  _saveJsonToDisk,  _readJsonToDisk, 
                _autoSync , _firstSyncController , _blockingEvent , _webDavClient , _cancellationController);

            _dictResponce = new SupportDictionaryResponce(ref model);

        }

        
       
        public void ObserverMainUpdateHandler(CreateSubscriptionTreeView subject)
        {

            subject.OnSaved += updateNetworkHandler;
        }


        
        private void updateNetworkHandler(object sender, EventArgs e)
        {

            CustomUpdateEventTreeViewArgs data = (CustomUpdateEventTreeViewArgs)e;


            //обновляет данные пришедшие из сетевого соединения
            if (data.getNameForm.Equals("MqClientGetBases"))
            {
                string viewElemetns = data.getTextMessage[0];
                string viewNewData = data.getTextMessage[1];

                //свежие обновления
                ServerTopicModel responceServer = JsonConvert.DeserializeObject<ServerTopicModel>(viewNewData);

                long user_id_server = Convert.ToInt64(responceServer.textmessage[3]);
                staticVariable.Variable.setUser_id(user_id_server);
                string statusServer = responceServer.textmessage[1];


                if(_dictResponce.getDictionary().ContainsKey(statusServer))
                {
                    _dictResponce.getDictionary()[statusServer](statusServer, responceServer, data);
                }
                else
                {
                    Console.WriteLine("ObserverNetworkUpdate->updateNetworkHandler: Критическая ошибка не найден запрос!!! " + statusServer);
                }
               

            }

        }

       private GetJsonRequestModel createModel(ObserverMainWindowFormUpdate mainUpdateForm , UpdateDataController updateDataController , MainWindowViewModel viewModelMain , UpdateFormMainController updateMainForm , SqliteController sqlliteController , CopyStatusController copyStatus , ViewFolderViewModel viewModelViewFolder , ConvertIconType convertIcon , UpdateNetworkJsonController updateSendController , AutoScanInfoModel autoScanInfoModel , SaveJsonToDiskContoller saveJsonToDisk , ReadJsonToDiskController readJsonToDisk , AutoSyncController autoSync , FirstSyncFilesController firstSyncController , BlockingEventController blockingEvent , WebDavClientController webDavClient , CancellationController cancellationController)
       {
            GetJsonRequestModel gjrm = new GetJsonRequestModel();
            gjrm.mainUpdateForm = mainUpdateForm;
            gjrm.updateDataController = updateDataController;
            gjrm.viewModelMain = viewModelMain;
            gjrm.updateMainForm = updateMainForm;
            gjrm.sqlliteController = sqlliteController;
            gjrm.copyStatus = copyStatus;
            gjrm.viewModelViewFolder = viewModelViewFolder;
            gjrm.convertIcon = convertIcon;
            gjrm.updateSendController = updateSendController;
            gjrm.autoScanInfoModel = autoScanInfoModel;
            gjrm.saveJsonToDisk = saveJsonToDisk;
            gjrm.readJsonToDisk = readJsonToDisk;
            gjrm.autoSync = autoSync;
            gjrm.firstSyncController = firstSyncController;
            gjrm.blockingEvent = blockingEvent;
            gjrm.webDavClient = webDavClient;
            gjrm.cancellationController = cancellationController;
         return gjrm;
       }

     
       


        
     
    }

    }

