﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.network;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.sync;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.networkGetJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.SyncCreateAllList;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.getJson.support
{
    public class GetReponce
    {
        private GetJsonRequestModel gjrm;
        public GetReponce(ref GetJsonRequestModel gjrm)
        {
            this.gjrm = gjrm;
        }

        public string SyncCreateAllList(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            SyncCreateAllListModel CreateModel = new SyncCreateAllListModel();
            CreateModel.responceServer = responceServer;

            CreateModel._mainUpdateForm = gjrm.mainUpdateForm;
            CreateModel._updateDataController = gjrm.updateDataController;
            CreateModel._viewModelMain = gjrm.viewModelMain;
            CreateModel._updateMainForm = gjrm.updateMainForm;
            CreateModel.sqlliteController = gjrm.sqlliteController;
            CreateModel._viewModelViewFolder = gjrm.viewModelViewFolder;
            CreateModel.convertIcon = gjrm.convertIcon;
            AutoScanInfoModel autModel = gjrm.autoScanInfoModel;

            SyncCreateAllList createAllList = new SyncCreateAllList(CreateModel, gjrm.updateSendController, ref autModel);
            writeWebDavNameSpace(responceServer);
            createAllList.SyncTraining();


            return responce;
        }

        public string SynSinglList(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {
            SyncSinglListModel CreateModel = new SyncSinglListModel();
            CreateModel.responceServer = responceServer;

            CreateModel._mainUpdateForm = gjrm.mainUpdateForm;
            CreateModel._updateDataController = gjrm.updateDataController;
            CreateModel._viewModelMain = gjrm.viewModelMain;
            CreateModel._updateMainForm = gjrm.updateMainForm;
            CreateModel._copyStatus = gjrm.copyStatus;
            CreateModel._sqlliteController = gjrm.sqlliteController;
            CreateModel._convertIcon = gjrm.convertIcon;
            CreateModel.rootNodesListView = gjrm.viewModelViewFolder.NodesView.Items;

            SyncSinglList createSinglList = new SyncSinglList(CreateModel, gjrm.updateSendController);
            writeWebDavNameSpace(responceServer);
            createSinglList.SyncSinglTraining();

            return responce;
        }

        int testCount = 0;
        public string LazySinSinglList(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            SyncSinglListModel createModel = new SyncSinglListModel();
            createModel._convertIcon = gjrm.convertIcon;
            createModel.responceServer = responceServer;

            FileInfoModel[] copyArrFileInfo = null;

            if (responceServer.listFileInfo != null)
            {

                copyArrFileInfo = copyArr(responceServer);

                testCount = testCount + 1;
                Console.WriteLine("ObserverNetwokUpdate->LAZYSYNSINGLLIST Ответ от сервера: Запрос на запись номер " + testCount);
            }




            createModel._mainUpdateForm = gjrm.mainUpdateForm;
            createModel._updateDataController = gjrm.updateDataController;
            createModel._viewModelMain = gjrm.viewModelMain;
            createModel._updateMainForm = gjrm.updateMainForm;
            createModel._copyStatus = gjrm.copyStatus;
            createModel._sqlliteController = gjrm.sqlliteController;

            AutoScanInfoModel autModel = gjrm.autoScanInfoModel;
            SyncLazySingList createSinglList = new SyncLazySingList(createModel, gjrm.updateSendController, ref autModel);
            createSinglList.SyncLazySinglTraining(gjrm.saveJsonToDisk, gjrm.readJsonToDisk, copyArrFileInfo);


            return responce;
        }

        public string Actual(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            gjrm.viewModelMain.pg = 80;
            //тестовая автосинхронизация файлов каждый n миллисекунд
            Console.WriteLine("Пришел ответ, что база данных актуальна! и производим за пуск StartAutoSyncToTime!");

            gjrm.autoSync.StartAutoSyncToTime();

            gjrm.viewModelMain.pg = 90;
            if (responceServer.webdavnamespace != null) staticVariable.Variable.webdavnamespace = responceServer.webdavnamespace;
            writeWebDavNameSpace(responceServer);
            existSavePc();
            gjrm.updateMainForm.updateTitleMainWindow("Синхронизирован");
            gjrm.viewModelMain.SetNewIconTaskBar(gjrm.convertIcon.streamToIcon(".notifyIconBarOk"));
            gjrm.viewModelMain.pg = 100;


            return responce;
        }

        public string Empty(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            writeWebDavNameSpace(responceServer);

            gjrm.updateMainForm.updateTitleMainWindow("Повторная синхронизация");
            gjrm.viewModelMain.SetNewIconTaskBar(gjrm.convertIcon.streamToIcon(".notifyIconBarRefresh"));
            gjrm.viewModelMain.pg = 10;
            gjrm.firstSyncController.ResponceServerEmptyBases();


            return responce;
        }
        public string SearchBeginFiles(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            SyncSearchModel syncSearchModel = new SyncSearchModel();
            syncSearchModel._updateMainForm = gjrm.updateMainForm;
            syncSearchModel._viewModelMain = gjrm.viewModelMain;
            SyncSearch syncSearch = new SyncSearch(syncSearchModel);

            syncSearch.SearchBeginFiles();


            return responce;
        }

        public string SearchPartiesFiles(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            SyncSearchModel syncSearchModel = new SyncSearchModel();

            syncSearchModel._updateMainForm = gjrm.updateMainForm;
            syncSearchModel._viewModelMain = gjrm.viewModelMain;

            SyncSearch syncSearch = new SyncSearch(syncSearchModel);
            syncSearch.SearchPartiesFiles();


            return responce;
        }

        public string SearchEndFiles(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {

            SyncSearchModel syncSearchModel = new SyncSearchModel();

            syncSearchModel._updateMainForm = gjrm.updateMainForm;
            syncSearchModel._viewModelMain = gjrm.viewModelMain;

            SyncSearch syncSearch = new SyncSearch(syncSearchModel);
            syncSearch.SearchEndFiles();

            gjrm.updateSendController.updateSendJsonCHECKWORKINGSERVER();


            return responce;
        }

    
        public string AnswerGenLink(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {
            UpdateNetworkJsonController updateSendController = gjrm.updateSendController;
            AnswerGenLink answerGenLink = new AnswerGenLink(ref responceServer, ref updateSendController);
            answerGenLink.ANSWER_GEN_LINK();

            return responce;
        }


        public string StatusPublisher(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {
            BlockingEventController blockingEvent = gjrm.blockingEvent;
            StatusPublisher status = new StatusPublisher(ref responceServer, ref blockingEvent);
            status.STATUS_PUBLISHSER_RESPONCE();

            return responce;
        }

   

        public string CheckWorkingServer(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {
            MainWindowViewModel viewModelMain = gjrm.viewModelMain;
            ConvertIconType _convertIcon = gjrm.convertIcon;
            UpdateFormMainController _updateMainForm = gjrm.updateMainForm;

            CheckWorkingServer checkworking = new CheckWorkingServer(ref viewModelMain, ref responceServer);
            checkworking.CHECK_WORKING_SERVER(ref _updateMainForm, ref _convertIcon);

            return responce;
        }

   

        public string StopPublisherNoDeleteBases(string responce, ServerTopicModel responceServer, CustomUpdateEventTreeViewArgs data)
        {
            gjrm.cancellationController.startCansel(staticVariable.Variable.reponceNoDeleteBasesCanselationTokenID);


            return responce;
        }

        private void writeWebDavNameSpace(ServerTopicModel responceServer)
        {
            if (responceServer.webdavnamespace != null)
            {
                staticVariable.Variable.webdavrootnamespace = responceServer.webdavnamespace;
                //Console.WriteLine("WebDavNameSpace: перезаписан на "+ staticVariable.Variable.webdavnamespace);
                setWEbSpace();
                string[] user = gjrm.sqlliteController.getSelect().GetUserSqlite();
                gjrm.webDavClient.createNewConnect(user[0], user[1]);
            }
            else
            {
                Console.WriteLine("WebDavNameSpace: Не критическая ошибка  Actual не вернул webdavnamespace!!! " + "Текущая версия " + staticVariable.Variable.webdavnamespace);
            }

        }

     

        private void setWEbSpace()
        {
            string[] user = gjrm.sqlliteController.getSelect().GetUserSqlite();
            staticVariable.Variable.setWebDavNameSpace(user[0]);
        }

        //проверка какие файлы сох. на компьютере
        public void existSavePc()
        {
            ExistSavePcEvent existSavePc = new ExistSavePcEvent(gjrm.convertIcon, gjrm.sqlliteController);
            existSavePc.existStart(gjrm.viewModelMain.NodesView.Items, gjrm.viewModelViewFolder.NodesView.Items);
        }

        private FileInfoModel[] copyArr(ServerTopicModel responceServer)
        {
            int size = responceServer.listFileInfo.Length;
            FileInfoModel[] arrFileInfo = new FileInfoModel[size];
            Array.Copy(responceServer.listFileInfo, arrFileInfo, size);
            responceServer.listFileInfo = null;

            return arrFileInfo;
        }

    }
}
