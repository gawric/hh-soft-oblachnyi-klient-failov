﻿using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support;
using NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support.request;
using NewHH_SoftClientWPF.mvvm.model.newtworkSendJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson
{
    public class SupportDictionaryRequest
    {
        private ObserverNetworkDictionaryModel dictModel;
        public delegate string OperationDelegate(string request, string parties, CustomSendJsonEventArgs data);
        public delegate string OperationDelegateLazy(string request, CustomSendJsonEventArgs data);
        private SendJsonRequestModel _sjrm;
        private IRequest _sjr;
        private IRequest _lazysjr;
        private IOtherRequest _otherAlertRequest;
        private IOtherRequest _otheGenLink;
        private IOtherRequest _otherPublisher;
        public SupportDictionaryRequest(SendJsonRequestModel sjrm)
        {
            _sjrm = sjrm;
            dictModel = new ObserverNetworkDictionaryModel();
            _lazysjr = new LazySendJsonRequest(ref sjrm);
            _sjr = new SendJsonRequest(ref sjrm);
            _otherAlertRequest = new AlertRequest(ref sjrm);
            _otheGenLink = new GenLinkRequest(ref sjrm);
            _otherPublisher = new PublisherRequest(ref sjrm);

            dictModel.onsj = _sjrm.onsj;

            initDictDelegat();
            initDictRequest();
            initDictLazy();

           
        }

        public ObserverNetworkDictionaryModel getModelDict()
        {
            return dictModel;
        }

        private void initDictLazy()
        {
            dictModel._dictDelegatLazy = new Dictionary<string, OperationDelegateLazy>();

            dictModel._dictDelegatLazy.Add("LAZYSINGLUPDATEPARTIES", _lazysjr.LazyUpdateParties);
            dictModel._dictDelegatLazy.Add("LAZYSINGLUPDATEPARTIESTOP", _lazysjr.LazyStopParties);
            dictModel._dictDelegatLazy.Add("LAZYSINGLENDUPDATEPARTIES", _lazysjr.LazyEndUpdateParties);
        }
        private void initDictRequest()
        {
            dictModel._dictRequest = new Dictionary<string, string>();

            dictModel._dictRequest.Add("ENDPARTIES", "SYNCREATEALLLIST");
            dictModel._dictRequest.Add("PARTIES", "SYNCREATEALLLIST");
            dictModel._dictRequest.Add("BEGINPARTIES", "SYNCREATEALLLIST");

            dictModel._dictRequest.Add("DELETEBEGINPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("DELETEPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("DELETEENDPARTIES", "SYNSINGLLIST");

            dictModel._dictRequest.Add("SINGLBEGINUPDATEPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("SINGLUPDATEPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("SINGLENDUPDATEPARTIES", "SYNSINGLLIST");

            dictModel._dictRequest.Add("PASTEBEGINPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("PASTEPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("PASTEENDPARTIES", "SYNSINGLLIST");

            dictModel._dictRequest.Add("RENAMESINGLBEGINUPDATEPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("RENAMESINGLUPDATEPARTIES", "SYNSINGLLIST");
            dictModel._dictRequest.Add("RENAMESINGLENDUPDATEPARTIES", "SYNSINGLLIST");

            dictModel._dictRequest.Add("SEARCHBEGINFILES", "SEARCH");
            dictModel._dictRequest.Add("SEARCHENDFILES", "SEARCH");
            dictModel._dictRequest.Add("SEARCHPARTIESFILES", "SEARCH");

            dictModel._dictRequest.Add("CHECKWORKINGSERVER", "CHECKWORKINGSERVER");
            dictModel._dictRequest.Add("STATUSPUBLISHER", "STATUSPUBLISHER");

            dictModel._dictRequest.Add("REQUESTGENLINK", "REQUESTGENLINK");
            dictModel._dictRequest.Add("ALERTERROR", "ALERTERROR");

            dictModel._dictRequest.Add("LAZYSINGLBEGINUPDATEPARTIES", "LAZYSYNSINGLLIST");
            dictModel._dictRequest.Add("LAZYSINGLUPDATEPARTIES", "LAZYSYNSINGLLIST");
            dictModel._dictRequest.Add("LAZYSINGLUPDATEPARTIESTOP", "LAZYSYNSINGLLIST");
            dictModel._dictRequest.Add("LAZYSINGLENDUPDATEPARTIES", "LAZYSYNSINGLLIST");
        }
        private void initDictDelegat()
        {
            dictModel._dictDelegatSend = new Dictionary<string, OperationDelegate>();

            dictModel._dictDelegatSend.Add("ENDPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("PARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("BEGINPARTIES", _sjr.beginParties);

            dictModel._dictDelegatSend.Add("DELETEBEGINPARTIES", _sjr.deleteBeginParties);
            dictModel._dictDelegatSend.Add("DELETEPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("DELETEENDPARTIES", _sjr.send);

            dictModel._dictDelegatSend.Add("PASTEBEGINPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("PASTEPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("PASTEENDPARTIES", _sjr.send);

            dictModel._dictDelegatSend.Add("SINGLBEGINUPDATEPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("SINGLUPDATEPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("SINGLENDUPDATEPARTIES", _sjr.send);

            dictModel._dictDelegatSend.Add("RENAMESINGLBEGINUPDATEPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("RENAMESINGLUPDATEPARTIES", _sjr.send);
            dictModel._dictDelegatSend.Add("RENAMESINGLENDUPDATEPARTIES", _sjr.send);

            dictModel._dictDelegatSend.Add("SEARCHBEGINFILES", _sjr.send);
            dictModel._dictDelegatSend.Add("SEARCHENDFILES", _sjr.send);
            dictModel._dictDelegatSend.Add("SEARCHPARTIESFILES", _sjr.send);

            dictModel._dictDelegatSend.Add("STATUSPUBLISHER", _otherPublisher.sendPUBLISHER);
            dictModel._dictDelegatSend.Add("CHECKWORKINGSERVER", _otherPublisher.sendPUBLISHER);
            dictModel._dictDelegatSend.Add("REQUESTGENLINK", _otheGenLink.requestGenLink);
            dictModel._dictDelegatSend.Add("ALERTERROR", _otherAlertRequest.alertError);

            dictModel._dictDelegatSend.Add("LAZYSINGLBEGINUPDATEPARTIES", _lazysjr.sendLazy);



        }



    }
}
