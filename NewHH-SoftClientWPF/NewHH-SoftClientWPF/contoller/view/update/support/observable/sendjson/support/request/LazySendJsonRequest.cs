﻿using NewHH_SoftClientWPF.contoller.network.json;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support.request;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.newtworkSendJson;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support
{
    public class LazySendJsonRequest : IRequest
    {
       
        private SendJsonRequestModel _sjrm;
        public LazySendJsonRequest(ref SendJsonRequestModel sjrm)
        {
            this._sjrm = sjrm;

        }
 

        public string LazyUpdateParties(string request, CustomSendJsonEventArgs data)
        {
            saveToDisk(data.getFileInfoArr);
            data.setFileInfoArr(null);
            //sendLazy(request, parties, data);

            return request;
        }

        private void saveToDisk(FileInfoModel[] fileInfoModelArray)
        {
            //инициализирован
            if (fileInfoModelArray != null)
            {
                //не заполнен только null
                if (checkEmptyArr(fileInfoModelArray))
                    _sjrm._saveJsonToDisk.saveLazyJsonArray(fileInfoModelArray);
            }

        }

        private bool checkEmptyArr(FileInfoModel[] fileInfoModelArray)
        {
            bool check = false;
            for (int h = 0; h < fileInfoModelArray.Length; h++)
            {
                if (fileInfoModelArray[h] != null)
                    check = true;
            }
            return check;
        }

        public string LazyStopParties(string request, CustomSendJsonEventArgs data)
        {
            Console.WriteLine("LAZYSINGLUPDATEPARTIESTOP Пришел запрос на остановку отправки файлов");
            List<string> tempHref = getListSaveDiskFiles();
            clearDataLazy(tempHref, request, data);

            return request;
        }


        private List<string> getListSaveDiskFiles()
        {
            return _sjrm._saveJsonToDisk.getListSendLazyTempHref();
        }

        private string clearDataLazy(List<string> tempHref, string request, CustomSendJsonEventArgs data)
        {
            tempHref.Clear();
            data.setFileInfoArr(null);

            setListSavDiskFiles(tempHref);

            return request;
        }

        public string LazyEndUpdateParties(string request, CustomSendJsonEventArgs data)
        {

            saveToDisk(data.getFileInfoArr);

            List<string> tempHref = getListSaveDiskFiles();
            readToDiskAndSend(tempHref, data);

            clearDataLazy(tempHref, "LAZYSINGLENDUPDATEPARTIES", data);

            sendLazy(request, "LAZYSINGLENDUPDATEPARTIES", data);

            return request;
        }


        void readToDiskAndSend(List<string> tempHref, CustomSendJsonEventArgs data)
        {
            try
            {
                if (tempHref != null)
                {
                    CustomSendJsonEventArgs copyData = (CustomSendJsonEventArgs)data.Clone();

                    for (int f = 0; f < tempHref.Count; f++)
                    {
                        copyData.setFileInfoArr(null);
                        FileInfoModel[] fileInfoArray = _sjrm._readJsonToDisk.readLazyJsonArray(tempHref[f]);
                        copyData.setFileInfoArr(fileInfoArray);
                        sendLazyUpdate(copyData);

                        fileInfoArray = null;
                        _sjrm._readJsonToDisk.deleteTempFiles(tempHref[f]);

                        Thread.Sleep(1000);
                    }

                    copyData = null;

                }

                tempHref = null;


            }
            catch (Newtonsoft.Json.JsonReaderException z)
            {
                Console.WriteLine("ObserverNetwokSendJson->readToDiskAndSend: " + " критическая ошибка чтения файлов с диска");
            }


        }

        private void sendLazyUpdate(CustomSendJsonEventArgs data)
        {
            string request = "LAZYSYNSINGLLIST";
            string parties = "LAZYSINGLUPDATEPARTIES";

            sendLazy(request, parties, data);
        }


        public string sendLazy(string request, string parties, CustomSendJsonEventArgs data)
        {
            try
            {
                bool check = checkData(data);
                string newVersionBases;

                //false - проверка не пройдена
                if (!check)
                {
                    newVersionBases = 0.ToString();
                }
                else
                {
                    newVersionBases = data.getTextMessage[1];
                }


                string[] usernameAndPassword = _sjrm._sqlLiteController.getSelect().GetUserSqlite();

                ServerTopicModel serverModel = _sjrm._supportTopicModel.CreateModelTopicParties(request, Convert.ToInt64(newVersionBases), usernameAndPassword[0], parties);

                if (data.getFileInfoArr != null)
                {
                    if (checkEmptyArr(data.getFileInfoArr))
                        serverModel.listFileInfo = data.getFileInfoArr;
                }

                string json = JsonConvert.SerializeObject(serverModel);



                if (_sjrm._mqclientController != null)
                {


                    if (serverModel.listFileInfo != null)
                    {
                        Console.WriteLine("serverModel parties***************** > размер передаваемого массива " + serverModel.listFileInfo.Length + " +++= parties " + parties);
                    }
                    else
                    {
                        Console.WriteLine("serverModel parties***************** > размер передаваемого массива " + 0 + " +++= parties " + parties);
                    }
                    //отправляет данные на сервер
                    _sjrm._mqclientController.SendMqLazyJsonServer(json);
                }


                //очистка памяти
                data.setFileInfoArr(null);
                json = null;

                return request;

            }
            catch (System.IndexOutOfRangeException a)
            {
                Console.WriteLine("ObserverNetworkSendJson->send: Ошибка " + a);
                return request;
            }


        }

        private bool checkData(CustomSendJsonEventArgs data)
        {
            bool check = false;

            if (2 < data.getTextMessage.Length)
            {
                check = true;
            }

            return check;
        }

        private void setListSavDiskFiles(List<string> tempHrefList)
        {
            _sjrm._saveJsonToDisk.setListSendLazyTempHref(tempHrefList);
        }

        public string send(string request, string parties, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }

        public string beginParties(string request, string parties, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }

        public string deleteBeginParties(string request, string parties, CustomSendJsonEventArgs data)
        {
            throw new NotImplementedException();
        }
    }
}
