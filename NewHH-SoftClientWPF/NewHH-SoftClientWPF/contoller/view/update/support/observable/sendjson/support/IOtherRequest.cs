﻿using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support.request
{
    public interface IOtherRequest
    {
        string alertError(string request, string parties, CustomSendJsonEventArgs data);
        string requestGenLink(string request, string parties, CustomSendJsonEventArgs data);
        string sendPUBLISHER(string request, string parties, CustomSendJsonEventArgs data);
    }
}
