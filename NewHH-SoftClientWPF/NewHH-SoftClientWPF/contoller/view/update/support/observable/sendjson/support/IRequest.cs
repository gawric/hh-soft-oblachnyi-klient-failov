﻿using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.view.update.support.observable.sendJson.support.request
{
    public interface IRequest
    {
        string send(string request, string parties, CustomSendJsonEventArgs data);
        string beginParties(string request, string parties, CustomSendJsonEventArgs data);
        string deleteBeginParties(string request, string parties, CustomSendJsonEventArgs data);

        string LazyUpdateParties(string request, CustomSendJsonEventArgs data);
        string LazyStopParties(string request, CustomSendJsonEventArgs data);

        string LazyEndUpdateParties(string request, CustomSendJsonEventArgs data);
        string  sendLazy(string request, string parties, CustomSendJsonEventArgs data);

    }
}
