﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{

    public class CreateSubscriptionMainForm
    {
        public event EventHandler OnSaved;

        public void updateForm(string nameForm, string[] textMessage)
        {

            CustomUpdateEventArgs sutom = new CustomUpdateEventArgs(nameForm, textMessage);
            OnSaved?.Invoke(this, sutom);
        }

  
    }
}
