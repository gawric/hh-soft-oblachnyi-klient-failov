﻿using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.subscription
{
    public class CreateSubscriptionViewTransfer
    {
        public event EventHandler OnSaved;
        public event EventHandler OnDelete;

        public void updateViewTrasfer(FolderNodesTransfer nodesTrasfer , string nameNodes)
        {
            CustomViewTransferArgs custom = new CustomViewTransferArgs(nodesTrasfer  , nameNodes);
            OnSaved?.Invoke(this, custom);
        }


        public void deleteViewTrasfer(long row_id, string location)
        {
            DeleteViewTransferArgs custom = new DeleteViewTransferArgs(location, row_id);
            OnDelete?.Invoke(this, custom);
        }
    }
}
