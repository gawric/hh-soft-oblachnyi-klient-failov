﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs
{
    public class DeleteViewTransferArgs : EventArgs
    {
        public string location;
        public long row_id;

        public DeleteViewTransferArgs(string location, long row_id)
        {
            this.row_id = row_id;
            this.location = location;
        }

        public string getLocation
        {
            get { return location; }
        }
        public long getRow_id
        {
            get { return row_id; }
        }
    }
}
