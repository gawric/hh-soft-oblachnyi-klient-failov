﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs
{
   public class CustomSendJsonEventArgs : EventArgs, ICloneable
    {
        //sendJson
        private string networ;

        //0 - ENDPARTIES/STARTPARTIES
        //1 - versionDatabases
        private string[] textMessage;

        //arr - fileinfo(2500 count)
        private FileInfoModel[] arr;


        //0 - name Form
        //1 - name Text
        public CustomSendJsonEventArgs(string networ, string[] textMessage, FileInfoModel[] arr)
        {
            this.networ = networ;
            this.textMessage = textMessage;
            this.arr = arr;
        }

        public string getNameForm
        {
            get { return networ; }
        }
        public string[] getTextMessage
        {
            get { return textMessage; }
        }
        public FileInfoModel[]  getFileInfoArr
        {
            get { return arr; }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void setFileInfoArr(FileInfoModel[] arr)
        {
            this.arr = arr;
        }
    }
}
