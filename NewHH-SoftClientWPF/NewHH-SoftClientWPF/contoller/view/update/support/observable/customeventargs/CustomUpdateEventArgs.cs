﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
    class CustomUpdateEventArgs : EventArgs
    {
        private string nameForm;
        private string[] textMessage;

        //0 - name Form
        //1 - name Text
        public CustomUpdateEventArgs(string nameForm, string[] textMessage)
        {
            this.nameForm = nameForm;
            this.textMessage = textMessage;
        }

        public string getNameForm
        {
            get { return nameForm; }
        }
        public string[] getTextMessage
        {
            get { return textMessage; }
        }
    }
}
