﻿using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using NewHH_SoftClientWPF.mvvm.model;

using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
   
    public class ObserverMainUpdate
    {
        private ObserverLoginFormUpdate _loginUpdateForm;
        private ObserverMainWindowFormUpdate _mainUpdateForm;
        private UpdateDataController _updateDataController;
        private ObserverUpdateTreeView _treeViewMainForm;

        public ObserverMainUpdate(Container cont)
        {
            _loginUpdateForm = cont.GetInstance<ObserverLoginFormUpdate>();
            _mainUpdateForm = cont.GetInstance<ObserverMainWindowFormUpdate>();
            _treeViewMainForm = cont.GetInstance<ObserverUpdateTreeView>();
            _updateDataController = cont.GetInstance<UpdateDataController>();

        }


        public void ObserverMainUpdateHandler(CreateSubscriptionMainForm subject)
        {
          
            subject.OnSaved += updateViewHandler;
        }


     
            //data - nameForm
            //data - args[0] - viewName
            //date - args[1] - data
            private void updateViewHandler(object sender, EventArgs e)
            {

            CustomUpdateEventArgs data = (CustomUpdateEventArgs)e;
          
            if(data.getNameForm.Equals("MainForm"))
            {
                string viewElemetns = data.getTextMessage[0];
                string viewNewData = data.getTextMessage[1];
             
                _mainUpdateForm.updateView(viewElemetns, viewNewData);
            } 
            
        }

     
    }
}
