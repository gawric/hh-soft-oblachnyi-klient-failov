﻿using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using NewHH_SoftClientWPF.contoller.update.updatedata;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.support.observable
{
    class ObserverLoginUpdate
    {

        private ObserverLoginFormUpdate _loginUpdateForm;
        private ObserverMainWindowFormUpdate _mainUpdateForm;
        private UpdateDataController _updateDataController;
        private ObserverUpdateTreeView _treeViewMainForm;

        public ObserverLoginUpdate(Container cont)
        {
            _loginUpdateForm = cont.GetInstance<ObserverLoginFormUpdate>();
            _mainUpdateForm = cont.GetInstance<ObserverMainWindowFormUpdate>();
            _treeViewMainForm = cont.GetInstance<ObserverUpdateTreeView>();
            _updateDataController = cont.GetInstance<UpdateDataController>();

        }


        public void ObserverLoginUpdateHandler(CreateSubscriptionLoginForm subject)
        {

            subject.OnSaved += updateViewHandler;
        }

        //data - nameForm
        //data - args[0] - viewName
        //date - args[1] - data
        private void updateViewHandler(object sender, EventArgs e)
        {

            CustomUpdateEventArgs data = (CustomUpdateEventArgs)e;
            // Console.WriteLine("Сработало Update LoginPage");

            if (data.getNameForm.Equals("LoginForm"))
            {
                string viewElemetns = data.getTextMessage[0];
                string viewNewData = data.getTextMessage[1];

                _loginUpdateForm.updateView(viewElemetns, viewNewData);

            }
           

        }

    }
}
