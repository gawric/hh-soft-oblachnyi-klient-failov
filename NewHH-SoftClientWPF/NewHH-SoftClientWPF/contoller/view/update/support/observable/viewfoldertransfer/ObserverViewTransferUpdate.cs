﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sorted.comparer;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.support.observable.customeventargs;
using NewHH_SoftClientWPF.contoller.update.support.observable.subscription;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.contoller.update.support.observable.viewfoldertransfer
{
    //Конечная реализация все идет через
    //UpdateViewTransferController ->CreateSubscriptionViewTransfer->ObserverViewTransferUpdate (здесь реализуем слушателя)
    //Обрабатывает создание новых нодов в ViewTransfer
    public class ObserverViewTransferUpdate
    {
        private ViewTransferViewModel _viewTransferModel;
        private SqliteController _sqlliteController;



        public ObserverViewTransferUpdate(Container cont)
        {
            _viewTransferModel = cont.GetInstance<ViewTransferViewModel>();
            _sqlliteController = cont.GetInstance<SqliteController>();
        }

        public void ObserverUpdateHandlerTreeView(CreateSubscriptionViewTransfer subjectViewTransfer)
        {

            subjectViewTransfer.OnSaved += addNodesToListTransferHandler;
        }

        public void ObserverDeleteHandlerTreeView(CreateSubscriptionViewTransfer subjectViewTransfer)
        {

            subjectViewTransfer.OnDelete += deleteNodesToListTransferHandler;
        }

        private void deleteNodesToListTransferHandler(object sender, EventArgs e)
        {
            DeleteViewTransferArgs data = (DeleteViewTransferArgs)e;



            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate
                {
                    if (_viewTransferModel.ListView.Items != null)
                    {

                        SortableObservableCollection<FolderNodesTransfer> listViewTrasnfer = _viewTransferModel.ListView.Items;



                        deleteItemListTransfer(data.row_id, listViewTrasnfer);
                        deleteItemSqlliteSinglRows(data.row_id);

                        IComparer<FolderNodesTransfer> comparer = new CompareViewTransfer<FolderNodesTransfer>();
                        listViewTrasnfer.Sort((x => x), comparer);

                    }

                });
            }

        }

        private void addNodesToListTransferHandler(object sender, EventArgs e)
        {

            CustomViewTransferArgs data = (CustomViewTransferArgs)e;

            FolderNodesTransfer trans = data.getNodesTransfer;
            trans.Row_id = _sqlliteController.getSelect().getLastRow_idToListFilesTransfer() + 1;

            if (App.Current != null)
            {
                App.Current.Dispatcher.Invoke(delegate 
                {
                    if (_viewTransferModel.ListView.Items != null)
                    {

                        SortableObservableCollection<FolderNodesTransfer> listViewTrasnfer = _viewTransferModel.ListView.Items;



                        //добавление в view
                        listViewTrasnfer.Add(trans);
                      
                        //добавление в sqllite
                        insertSqllite(trans);


                        IComparer<FolderNodesTransfer> comparer = new CompareViewTransfer<FolderNodesTransfer>();
                        listViewTrasnfer.Sort((x => x), comparer);
                        Console.WriteLine();
                        
                    }

                });
            }
            
            

        }

        private void deleteItemListTransfer(long row_id , SortableObservableCollection<FolderNodesTransfer> listViewTrasnfer)
        {
            FolderNodesTransfer deleteItem = null;

            foreach(FolderNodesTransfer item in listViewTrasnfer)
            {
                if(item.Row_id.Equals(row_id))
                {
                    deleteItem = item;
                    break;
                }
            }

            if (deleteItem != null) listViewTrasnfer.Remove(deleteItem);
        }

        private void deleteItemSqlliteSinglRows(long row_id)
        {
            _sqlliteController.getDelete().RemoveListFilesTransferSingl(row_id);
        }

        private void insertSqllite(FolderNodesTransfer fnt)
        {
            FileInfoTransfer sqllitemodel = convertNodesTransferToFileInfoTransfer(fnt);
            FileInfoTransfer[] listFileTransfer = new FileInfoTransfer[1];
            listFileTransfer[0] = sqllitemodel;

            //если пока работали с формой кто-то сделал запись в наш id мы евеличиваем его на 1 еденицу
            if(_sqlliteController.getSelect().isExistListTransferToRow_id(sqllitemodel.row_id))
            {
                long new_row_id = _sqlliteController.getSelect().getLastRow_idToListFilesTransfer() + 1;
                listFileTransfer[0].row_id = (int)new_row_id;
                _sqlliteController.getInsert().InsertFileTransferTransaction(listFileTransfer);
            }
            else
            {
                _sqlliteController.getInsert().InsertFileTransferTransaction(listFileTransfer);
            }
            
        }

        private FileInfoTransfer convertNodesTransferToFileInfoTransfer(FolderNodesTransfer fnt)
        {
            FileInfoTransfer sqllite_model = new FileInfoTransfer();
            sqllite_model.row_id = (int)fnt.Row_id;
            sqllite_model.row_id_listFiles_users= (int)fnt.Row_id_listFiles_users;
            sqllite_model.filename = fnt.FolderName;
            sqllite_model.sizebyte = fnt.Sizebyte;
            sqllite_model.type = fnt.Type;
            sqllite_model.progress = fnt.progress;
            sqllite_model.uploadStatus = fnt.uploadStatus;
            sqllite_model.typeTransfer = fnt.TypeTransfer;

            return sqllite_model;
        }
      
    }
}
