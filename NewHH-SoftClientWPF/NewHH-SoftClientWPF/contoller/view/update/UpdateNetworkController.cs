﻿using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update.support.observable;
using NewHH_SoftClientWPF.contoller.update.support.observable.support;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NewHH_SoftClientWPF.contoller.update
{
     class UpdateNetworkController
    {

        private CreateSubscriptionTreeView updateHandler;
        private ObserverNetwokUpdate observer;


        public UpdateNetworkController(Container cont)
        {
            observer = cont.GetInstance<ObserverNetwokUpdate>();
            updateHandler = new CreateSubscriptionTreeView();
            //Отдельный слушатель на Дерево
            observer.ObserverMainUpdateHandler(updateHandler);
        }
      
        //Отправляет данные из MqBroker -> в форму для обновления данных SqLite и TreeView -> MainForm
        public void updateMqClientSubscrible(string json)
        {
            string nameForm = "MqClientGetBases";
            string nameElemetns = "TreeView1";
            string[] textMessage = { nameElemetns, json };
            SortableObservableCollection<FolderNodes> nodes = new SortableObservableCollection<FolderNodes>();
            TreeViewItem tvi = null;
            sendUpdateNetwork(nameForm, textMessage, nodes , tvi);
        }

        //вызываем из нужно участка кода для обновления View 
        //[0] - nameViewElements
        //[1] - data Elements
        public void sendUpdateNetwork(string nameForm, string[] textMessage, SortableObservableCollection<FolderNodes> nodes , TreeViewItem tvi)
        {
            
            updateHandler.updateTreeView(nameForm, textMessage, nodes , tvi);
        }

    }
}
