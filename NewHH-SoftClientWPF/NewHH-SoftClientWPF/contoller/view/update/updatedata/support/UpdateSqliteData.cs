﻿using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.updatedata
{
    class UpdateSqliteData
    {
        private SqliteController sqllitecontroller;

        public UpdateSqliteData(SqliteController sqllitecontroller)
        {
            this.sqllitecontroller = sqllitecontroller;
        }

        public void Start(List<FileInfoModel> listFiles , MainWindowViewModel _viewModelMain)
        {
            long versionServerBases;

            if (listFiles[0] != null)
            {
                 versionServerBases = listFiles[0].versionUpdateBases;
            }
            else
            {
                versionServerBases = 0;
            }
            
            
            bool check = sqllitecontroller.getSelect().existsLastVersionBases(versionServerBases);

            //проверяем свежая ли у нас база данных в нашем кэше
            if (check == true)
            {
                //полностью удаляет и вставляет новые данные в таблиц listFiles
                sqllitecontroller.UpdateFilesTableAllRows(listFiles ,  _viewModelMain);
            }
            else
            {
                //Здесь нужно реализовать замену базы кусками
                Console.WriteLine("false база данных тухлая");
                
                //Обновление базы без удаления старой
                UpdateSingl(listFiles, _viewModelMain);
            }
        }

        public void UpdateSingl(List<FileInfoModel> listFiles, MainWindowViewModel _viewModelMain)
        {
            try
            {
                sqllitecontroller.UpdateFilesTableCollectionRows(listFiles);
            }
            catch (SQLiteException s2)
            {
                Console.WriteLine("SqlliteController -> UpdateFilesTableCollectionRows - > Ошибка транзакции    /n" + s2.ToString());
                sqllitecontroller.UpdateFilesTableCollectionRows(listFiles);
            }
        }


        public void clearSqlliteAndAddFileInfo(List<FileInfoModel> listFiles , MainWindowViewModel _viewModelMain)
        {
            //полностью удаляет и вставляет новые данные в таблиц listFiles
            sqllitecontroller.UpdateFilesTableAllRows(listFiles ,  _viewModelMain);
        }



        public void removeSqlliteBases()
        {
            //полностью удаляет и вставляет новые данные в таблиц listFiles
            sqllitecontroller.RemoveFilesTableAllRows();
        }

        public void insertSqlliteFileInfo(List<FileInfoModel> listFiles , MainWindowViewModel _viewModelMain)
        {
            sqllitecontroller.InsertFilesTableAllRows(listFiles , _viewModelMain);
        }

        public void updateVerSqlliteFileInfo(long newVerSionServer)
        {
            sqllitecontroller.updateVeersionBases(newVerSionServer);
        }
    }
}
