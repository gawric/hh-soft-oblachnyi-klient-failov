﻿using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.dialog;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.operationContextMenu.upload
{
    public class ButtonUploadClick
    {
        private SqliteController _sqlLiteController;
        private MainWindowViewModel _viewModel;
        private WebDavClientController _webDavClient;
        private StackOperationUpDown _stackOperationUpDown;

        public ButtonUploadClick(Container cont)
        {
            _sqlLiteController = cont.GetInstance<SqliteController>();
            _viewModel = cont.GetInstance<MainWindowViewModel>();
            _webDavClient = cont.GetInstance<WebDavClientController>();
            _stackOperationUpDown = cont.GetInstance<StackOperationUpDown>();

        }

        public IOpenDialog CreateDialog()
        {
            IOpenDialog dialog = new OpenDialog();
            dialog.CreateDialog();
            return dialog;
        }
        private bool openDialog()
        {
            bool[] skipStatus = { false };
            OverwriteUpload skipWin = new OverwriteUpload(skipStatus);
            skipWin.ShowDialog();

            return skipStatus[0];
        }
        public IOpenDialog CreateFolderDialog()
        {
            IOpenDialog dialog = new OpenDialog();
            dialog.CreateFolderDialog();
            return dialog;
        }


        public FileInfoModel getPasteModel()
        {
            long row_id = _viewModel.ActiveNodeID[0];
            return _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_id);
        }

        public List<FileInfoModel> getChildrenPasteModel()
        {
            long row_id = _viewModel.ActiveNodeID[0];
            //все дети папки для вставки, что-бы проверить существуют файлы или папки с таким же названием
           return _sqlLiteController.getSelect().GetSqlLiteParentIDFileInfoList(row_id);
        }

        public bool isDublication(string[] selectHref , List<FileInfoModel> parentListPasteModel)
        {
            //класс для поиска дублей
            SotredSourceHrefToDouble searchDouble = new SotredSourceHrefToDouble();
            return  searchDouble.isExistPasteModel(selectHref, parentListPasteModel);
        }

        public void startTrainingUpload(bool check , string[] selectHref , FileInfoModel pasteModel)
        {
            //check - найден дубликат нужно открыть форму предупреждение, что будет пропущенно
            if (check)
            {
                startSkip( selectHref,  pasteModel);
            }
            else
            {
                startUpload(selectHref, pasteModel , false);
            }
        }

        public void startTrainingUploadScanFolder(string[] selectHref, FileInfoModel pasteModel)
        {
            _stackOperationUpDown.addStackUploadScanFolder(selectHref, pasteModel);
        }



        private void startSkip(string[] selectHref, FileInfoModel pasteModel)
        {
            //если мы нажали кнопку пропустить храним статус здесь
            if (openDialog())
            {
                _stackOperationUpDown.addStackUpload(selectHref, pasteModel);
            }
            else
            {
                Console.WriteLine("MainwWindows->ButtonInCloud_Click: Нашли совпадение и решили не продолжать");
            }
        }

      
        private void startUpload(string[] selectHref, FileInfoModel pasteModel , bool isScanFolder)
        {
            //если дубликатов нету
            _stackOperationUpDown.addStackUpload(selectHref, pasteModel);
        }
    }
}
