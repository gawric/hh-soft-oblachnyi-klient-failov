﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.navigation.mainwindows.support
{
    class NavigationMainSupport
    {
        List<string[]> _listPath;
        private SearchTreeViewNavigation _navigationSerachTreeView;

        public NavigationMainSupport(List<string[]> _listPath , SearchTreeViewNavigation navigationSerachTreeView)
        {
            this._listPath = _listPath;
            this._navigationSerachTreeView = navigationSerachTreeView;
        }

        public void FolderRootScan(FolderNodes item , List<string[]> _listPath , int[]ind)
        {
            if (item.Children.Count > 0)
            {
                for (int f = 0; f < item.Children.Count; f++)
                {
                    FolderNodes fol = item.Children[f];

                    string folderPath = null;
                    //если l выйдет за пределы массива
                    if (ind[0] > _listPath.Count() - 1)
                    {

                        CheckError(folderPath , ind);
                    }
                    else
                    {
                        folderPath = _listPath[ind[0]][0];
                    }


                    if (_listPath != null)
                    {
                        if (_listPath.Count > 0)
                        {
                            if (folderPath != null)
                            {

                                //сравниваем полученное имя из дерева с именем из созданного пути до нужного нода
                                if (fol.FolderName.Equals(folderPath))
                                {

                                    CompareFoderName(fol, ind);
                                    break;
                                }


                            }


                        }
                        else
                        {
                            Console.WriteLine("NavigationMainWindowsController->SearchTreeview - >Поиск не возможен массив пустой");
                        }

                    }

                }
            }
        }


        private void CompareFoderName(FolderNodes fol , int[]ind)
        {
            Console.WriteLine("NavigationMainWindowsController->SearchTreeview - > Нашел вхождение по имени  " + fol.FolderName);
            fol.IsExpanded = true;

            int index_list = --ind[0];
            //Обрабатываем когда получаем 0 элемент
            if (index_list > -1)
            {
                IntemBigMinus1(fol, index_list , ind);
            }
            else
            {
                //если массив -1 
                //мы ищем начиная с 0 
                //и проверяем что 0 это и есть нужный обьект или идем дальше по детям
                string name = _listPath[0][0];
                string folderPath2 = name;
                IntemBinNeRavenMinus1(fol, folderPath2 , ind);

            }
        }
       

        private void IntemBigMinus1(FolderNodes fol, int index_list , int[]ind)
        {
            string name = _listPath[0][0];
            //listPath - 0 всегда конечная папка
            string finish_point = name;
            //выделяем только ее, что-бы остальные не попадали в наш массив открытых узлов
            if (finish_point != null)
            {
                if (finish_point.Equals(fol.FolderName))
                {
                    fol.IsSelected = true;
                }

            }


            string folderPath2 = _listPath[index_list][0];
            _navigationSerachTreeView.SearchTreeView(fol, folderPath2 , ind);
        }

        private void IntemBinNeRavenMinus1(FolderNodes fol, string folderPath2, int[] ind)
        {

            if (fol.FolderName.Equals(folderPath2))
            {
                fol.IsExpanded = true;
                fol.IsSelected = true;
                Console.WriteLine("NavigationMainWindowsController->SearchTreeview - > нашел вхождение при индексе массива -1");
            }
            else
            {
                _navigationSerachTreeView.SearchTreeView(fol, folderPath2, ind);
            }
        }


        private string CheckError(string folderPath , int[] ind)
        {
            if (_listPath.Count() == 0)
            {
                Console.WriteLine("NavigationMainWindowsController->SearchTreeview-> частая ошибка с -1 " + "l=" + 1 + " l индекс=" + ind[0] + " Размер массива: _listPath " + _listPath.Count);
                // l = _listPath[l].Count() - 1;
                // if(_listPath[0] != null)
                //{
                //folderPath = _listPath[0];
                //}

                return folderPath;
            }
            else
            {
                Console.WriteLine("NavigationMainWindowsController->SearchTreeview-> частая ошибка с -1 " + "l=" + 1 + " l индекс=" + ind[0] + " Размер массива: _listPath " + _listPath.Count);
                ind[0] = _listPath[ind[0]].Count() - 1;
                folderPath = _listPath[ind[0]][0];
                return folderPath;
            }
        }
    }


}
