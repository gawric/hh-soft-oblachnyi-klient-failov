﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.navigation.mainwindows.support
{
    class SearchTreeView2Navigation
    {
        private SearchTreeViewNavigation _navigationTreeView;
        private StatusOpenNode _statusOpenNode;
        private SearchNextPosition _searchNextPosition;

        public SearchTreeView2Navigation(SearchTreeViewNavigation _navigationTreeView , StatusOpenNode _statusOpenNode , SearchNextPosition _searchNextPosition)
        {
            this._navigationTreeView = _navigationTreeView;
            this._statusOpenNode = _statusOpenNode;
            this._searchNextPosition = _searchNextPosition;
        }
        //производит поиск root папки
        public void SearchTreeView2(ObservableCollection<FolderNodes> allTreeViewNodes, List<FileInfoModel> listPath2 , int[]l2 , int[] ind , BackNodesModel insert)
        {


            indexList(ref listPath2, ref l2, ref allTreeViewNodes);
            try
            {
                //Console.WriteLine(foundItem[0]);
                foreach (FolderNodes item in allTreeViewNodes)
                {
                    if (item.Children.Count > 0)
                    {
                        for (int f = 0; f < item.Children.Count; f++)
                        {
                            FolderNodes fol = item.Children[f];
                            string folderPath = listPath2[l2[0]].filename;
                            long row_id = listPath2[l2[0]].row_id;

                            if (listPath2 != null)
                            {
                                if (listPath2.Count > 0)
                                {
                                    //сравниваем полученное имя из дерева с именем из созданного пути до нужного нода
                                    if (fol.FolderName.Equals(folderPath) & fol.Row_id.Equals(row_id))
                                    {
                                        searchFinish(ref fol, ref l2, ref listPath2, ref allTreeViewNodes, ref insert, ref ind);
                                        break;
                                    }

                                }
                                else
                                {
                                    Console.WriteLine("SerachTreeView2Navigation -> SearchTreeView2  -> Поиск не возможен массив пустой часть 6");
                                }

                            }

                        }
                    }


                }
                listPath2.Clear();
            }
            catch (System.InvalidOperationException k5)
            {
                Console.WriteLine(k5);
            }
            Console.WriteLine();

        }

        private void indexList(ref List<FileInfoModel> listPath2 , ref int[] l2 , ref ObservableCollection<FolderNodes> allTreeViewNodes)
        {
            if (listPath2.Count == 0)
            {
                l2[0] = listPath2.Count;
            }
            else
            {
                checkExpandedRootDisk(allTreeViewNodes[0]);
                l2[0] = listPath2.Count - 1;
            }
        }

        private void searchFinish(ref FolderNodes fol , ref int[] l2 , ref List<FileInfoModel> listPath2 , ref ObservableCollection<FolderNodes> allTreeViewNodes , ref BackNodesModel insert , ref int[] ind)
        {
            fol.IsExpanded = true;

            int index_list = --l2[0];

            //Обрабатываем когда получаем 0 элемент
            if (index_list > -1)
            {
                finishNodes(ref listPath2, ref allTreeViewNodes, ref fol, ref index_list, ref l2, ref insert);
            }
            else
            {
                //если массив -1 
                //мы ищем начиная с 0 
                //и проверяем что 0 это и есть нужный обьект или идем дальше по детям
                string folderPath2 = listPath2[0].filename;
                long folderPathrow_id = listPath2[0].row_id;

                if (fol.FolderName.Equals(folderPath2) & fol.Row_id.Equals(folderPathrow_id))
                {
                    finishFirstNodes(ref fol, ref folderPath2);
                }
                else
                {
                    _navigationTreeView.SearchTreeView(fol, folderPath2, ind);
                }
               

            }
        }







        private void finishNodes(ref List<FileInfoModel> listPath2 , ref ObservableCollection<FolderNodes> allTreeViewNodes, ref FolderNodes fol , ref int index_list , ref int[] l2 , ref BackNodesModel insert)
        {
            //listPath - 0 всегда конечная папка
            string finish_point = listPath2[0].filename;
            long finish_row_id = listPath2[0].row_id;

            //выделяем только ее, что-бы остальные не попадали в наш массив открытых узлов
            if (finish_point.Equals(fol.FolderName) & finish_row_id.Equals(fol.FolderName))
            {
                fol.IsSelected = true;
            }

            string folderPath2 = listPath2[index_list].filename;
            long parentRowId1 = listPath2[index_list].row_id;
            Console.WriteLine("SearchTreeView2Navigation->SearchTreeView2:  Следущий шаг в поиске ищем  " + folderPath2);
            _searchNextPosition.SearchNextPositionTreeView2(fol, folderPath2, parentRowId1, listPath2, l2, insert);
        }

        private void finishFirstNodes(ref FolderNodes fol , ref string folderPath2)
        {
            fol.IsExpanded = true;
            fol.IsSelected = true;
            _navigationTreeView.checkNodes(fol).Wait();
            _statusOpenNode.isOpenNode = true;

            Console.WriteLine("SerachTreeView2Navigation -> SearchTreeView2 ->Папка поиска " + fol.FolderName + "  Якобы ищем folderPath2 " + folderPath2);
            Console.WriteLine("SerachTreeView2Navigation -> SearchTreeView2 -> нашел вхождение при индексе массива -1 часть 6");
        }

        private void checkExpandedRootDisk(FolderNodes fol)
        {
            if(fol != null)
            {
                if (fol.FolderName.Equals("Disk"))
                {
                    if (!fol.IsExpanded)
                    {
                        fol.IsExpanded = true;
                        fol.IsSelected = true;
                        _navigationTreeView.checkNodes(fol).Wait();

                    }
                }
            }
            
            
        }

        
    }
}
