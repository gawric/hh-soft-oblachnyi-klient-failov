﻿using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.navigation.mainwindows.support
{
    class SearchNextPosition
    {
        private StatusOpenNode _statusOpenNode;
        private MainWindowViewModel _viewModel;
        private ViewFolderViewModel _viewModelViewFolder;

        public SearchNextPosition(StatusOpenNode _statusOpenNode, MainWindowViewModel _viewModel, ViewFolderViewModel _viewModelViewFolder)
        {
            this._statusOpenNode = _statusOpenNode;
            this._viewModel = _viewModel;
            this._viewModelViewFolder = _viewModelViewFolder;
        }



        //перемещается по их детям для поиска след позиции
        public void SearchNextPositionTreeView2(FolderNodes _item, string _parent, long parentRow_id, List<FileInfoModel> listPath2, int[] l2, BackNodesModel insert)
        {
            //FolderNodes foundItem = null;
            try
            {
                Console.WriteLine("SearchNextPosition->SearchNextPositionTreeView2: Папка родитель++++++++++++++++++++++++++ " + _item.FolderName);
                //for (FolderNodes subItem in _item.Children) - было раньше до изменения

                for (int g = 0; g < _item.Children.Count; g++)
                {

                    try
                    {

                        FolderNodes subItem = updateIteration(ref _item, ref _parent, ref g);


                        if (!insert.isFinish)
                        {

                            if (subItem.FolderName.Equals(_parent) & subItem.Row_id.Equals(parentRow_id))
                            {
                                //не больше массива
                                //отсчитываем массив с конца к началу
                                if (l2[0] >= 0)
                                {

                                    if (l2[0] == 0)
                                    {


                                        subItem.IsExpanded = true;
                                        finish(ref listPath2, ref subItem, ref insert);
                                        break;
                                    }
                                    else
                                    {
                                        //foundItem = subItem;
                                        Console.WriteLine("SearchNextPosition->SearchTreeView2 - > Нашел вхождение по имени: продолжаем   Часть 3 " + subItem.FolderName);

                                        subItem.IsExpanded = true;
                                        iteration(ref l2, ref listPath2, subItem, insert);


                                    }
                                }
                                else
                                {
                                    Console.WriteLine("SearchNextPosition->SearchTreeView2: Поиск нужного файла закончен c проблемами Часть 4 " + subItem.FolderName);
                                }
                            }
                            else
                            {
                                //Console.WriteLine("NavigactionMainWindowsController - > SearchTreeView2 - > Не найден treeview  Часть 5");
                            }
                        }


                    }
                    catch (Exception s)
                    {
                        Console.WriteLine("SearchNextPosition->SearchTreeView2:  Ошибка поиска пути назад очистка массива MainWindows.xml");
                        Console.WriteLine(s);
                        listPath2.Clear();
                    }

                }
            }
            catch (Exception s)
            {
                Console.WriteLine("SearchNextPosition->SearchTreeView2: Ошибка поиска пути назад очистка массива MainWindows.xml");
                Console.WriteLine(s);
                listPath2.Clear();
            }


        }

        private void iteration(ref int[] l2 , ref List<FileInfoModel> listPath2 , FolderNodes subItem , BackNodesModel insert)
        {
            int index = --l2[0];
            string parent = listPath2[index].filename;
            long parentRow_id1 = listPath2[index].row_id;

            Task.Run(() => checkNodes2(subItem, insert));
            //checkNodes2(subItem).Wait();

            SearchNextPositionTreeView2(subItem, parent, parentRow_id1, listPath2, l2, insert);
        }



        private void finish(ref List<FileInfoModel> listPath2 , ref  FolderNodes subItem , ref BackNodesModel insert)
        {
            if (listPath2[0].filename.Equals(subItem.FolderName) & listPath2[0].row_id.Equals(subItem.Row_id))
            {
                subItem.IsSelected = true;
                _statusOpenNode.isOpenNode = true;
                insert.isFinish = true;

                Console.WriteLine("SearchNextPosition->SearchTreeView2 -> Поиск нужного файла закончен   " + subItem.FolderName + " isSelected " + subItem.IsSelected + "  isExpanded " + subItem.IsSelected + "Часть 1");
                //break;
            }
            else
            {
                Console.WriteLine("SearchNextPosition->SearchNextPositionTreeView2: Искомый обьект " + listPath2[0].filename + " текущий обьект " + subItem.FolderName);
                Console.WriteLine("SearchNextPosition->SearchTreeView2 -> Не прошли по всему массиву + Часть 2");
            }
        }

        private FolderNodes updateIteration(ref FolderNodes _item , ref string _parent , ref int g)
        {
            FolderNodes subItem = _item.Children[g];
            Console.WriteLine("SearchNextPosition->SearchNextPositionTreeView2: Ищем нужного нам ребенка Ищем " + _parent + " <<<<<<<  " + " Перебераемый ребенок " + subItem.FolderName);
            //дожидаемся загрузки нужного узла в TreeViewNodes
            g = waitingLoadedTreeViewNodes(ref _item, _statusOpenNode, ref g);
            subItem = _item.Children[g];
            Console.WriteLine("SearchNextPosition->SearchNextPositionTreeView2: Дождались ответа " + "Index цикла " + g + "Имя Items " + subItem.FolderName);

            return subItem;

        }

        private async Task checkNodes2(FolderNodes subItem , BackNodesModel insert)
        {
            int count = 0;

            while (true)
            {


                Console.WriteLine("Попытка номер " + count + " Ждем открытие nod и его детей SearchNextPosition-> checkNodes2");
                

                if (_statusOpenNode.isNewOpenNode)
                {
                    Console.WriteLine("SearchNextPosition-> checkNodes2 (часть 1) Узел открылся мы выделили -> " + _viewModel.ActiveNodeID[0]);
                    //нод открыт или нет
                    _statusOpenNode.isOpenNode = true;
                   
                    break;
                }
                else
                {
                   
                            long row_id_treeview = _viewModel.ActiveNodeID[0];
                            long row_id_viewfolder = _viewModelViewFolder.ActiveViewFolderNodeID[0];
                            //попытка обойти этот глюк нужно быть осторожным
                            if (row_id_treeview != row_id_viewfolder)
                            {
                                //Console.WriteLine("NavigationMainWIndowsController-> checkNodes2 (часть 3) Узел открылся мы выделили -> " + "Попытка с глюком открыть");
                                for (int p = 0; p < subItem.Children.Count; p++)
                                {
                                    if (subItem.Children[p].Row_id == row_id_viewfolder)
                                    {
                                        Console.WriteLine("SearchNextPosition-> checkNodes2 (часть 4) Урааа нашли нужный FolderNodes");
                                        subItem.Children[p].IsSelected = true;
                                    }
                                }

                            }
                            //subItem.IsSelected = true;
                            Console.WriteLine("SearchNextPosition-> checkNodes2 (часть 2) Узел открылся мы выделили -> " + _viewModel.ActiveNodeID[0]);
                            break;
                        
                   
                }

               
            }
        }

        private int waitingLoadedTreeViewNodes(ref FolderNodes subItem , StatusOpenNode _statusOpenNode , ref int index)
        {
            bool isStop = false;
            int close = 0;

            while (true)
            {
                if(isStop | close >= 400) break;
                

                if (_statusOpenNode.isNewOpenNode == true){
                    if(!subItem.Children[0].FolderName.Equals("Загрузка")) isStop = true; Console.WriteLine("SearchNextPosition-> waitingLoadedTreeViewNodes Узлы в TreeView загружены");
                }
                else{
                    Console.WriteLine("SearchNextPosition-> waitingLoadedTreeViewNodes: Ожидаем загрузки TreeviewNodes узла ->  его ID" + _viewModel.ActiveNodeID[0]);
                    index = 0;
                }

                Thread.Sleep(10);
                close++;

            }

            return index;
        }


    }

}
