﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.navigation.mainwindows.support
{
    class SearchTreeViewNavigation
    {
        private List<string[]> _listPath;
        private StatusOpenNode _statusOpenNode;

        public SearchTreeViewNavigation(List<string[]> _listPath , StatusOpenNode _statusOpenNode)
        {
            this._listPath = _listPath;
            this._statusOpenNode = _statusOpenNode;
        }


        //перемещается по их детям для поиска след позиции
        public void SearchTreeView(FolderNodes _item, string _parent , int[] ind)
        {

            try
            {
                foreach (FolderNodes subItem in _item.Children)
                {
                    try
                    {
                        if (subItem.FolderName.Equals(_parent))
                        {
                            //не больше массива
                            //отсчитываем массив с конца к началу
                            if (ind[0] >= 0)
                            {

                                //
                                if (ind[0] == 0)
                                {

                                    subItem.IsExpanded = true;

                                    if (_listPath[0].Equals(subItem.FolderName))
                                    {
                                        subItem.IsSelected = true;
                                        Console.WriteLine("SearchTreeViewNavigation->SearchTreeView: Поиск нужного файла закончен   " + subItem.FolderName);
                                    }
                                    else
                                    {
                                        Console.WriteLine("NavigationMainWIndowsController->SearchTreeView Не прошли по всему массиву");
                                    }


                                    break;
                                }
                                else
                                {
                                    //foundItem = subItem;

                                    subItem.IsExpanded = true;
                                    subItem.IsSelected = true;

                                    int count = --ind[0];
                                    Console.WriteLine("SearchTreeViewNavigation->SearchTreeView Нашел вхождение по имени продолжаем  " + subItem.FolderName + "  Индекс массива  " + count);
                                    string parent = _listPath[count][0];


                                    checkNodes(subItem).Wait();

                                    SearchTreeView(subItem, parent , ind);
                                }
                            }
                            else
                            {
                                Console.WriteLine("SearchTreeViewNavigation->SearchTreeView Поиск нужного файла закончен ");
                            }
                        }
                        else
                        {
                            //Console.WriteLine("NavigactionMainWindowsController - > SearchTreeView - > Не найден treeview  ");
                        }

                    }
                    catch (Exception s)
                    {
                        Console.WriteLine("SearchTreeViewNavigation->SearchTreeView Ошибка поиска пути назад очистка массива MainWindows.xml");
                        Console.WriteLine(s);
                        _listPath.Clear();
                    }

                }
            }
            catch (Exception s)
            {
                Console.WriteLine("SearchTreeViewNavigation->SearchTreeView Ошибка поиска пути назад очистка массива MainWindows.xml");
                Console.WriteLine(s);
                _listPath.Clear();
            }

        }


        //ждем открытие нода
        public async Task checkNodes(FolderNodes subItem)
        {
            int count = 0;

            while (true)
            {
                if (subItem.Children.Count > 1)
                {
                    break;
                }
                else
                {
                    if(_statusOpenNode.isNewOpenNode)
                    {
                        if (subItem.Children.Count > 1)
                        {
                            break;
                        }
                       
                    }
                    else
                    {
                        Console.WriteLine("SearchTreeViewNavigation->checkNodes: Ждем открытие нода");
                    }
                }
                Thread.Sleep(100);
                count++;
            }
        }

    }
}
