﻿using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sorted.comparer;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.update.treeview
{
    public class InitializingViewTransferController
    {
        public void initializingViewTrasfer(SqliteController _sqlliteController, ViewTransferViewModel _viewtModelTansfer)
        {
            SortableObservableCollection<FolderNodesTransfer> NodesViewTransfer = _viewtModelTansfer.ListView.Items;
            List<FolderNodesTransfer> sqlliteListViewTransfer = _sqlliteController.getSelect().getSqlliteAllListFIlesTransfer();

            for (int f = 0; f < sqlliteListViewTransfer.Count; f++)
            {
                FolderNodesTransfer smt = sqlliteListViewTransfer[f];
                smt.uploadStatus = TrasferCheckError(smt.uploadStatus);
                NodesViewTransfer.Add(smt);
            }
            IComparer<FolderNodesTransfer> comparer= new CompareViewTransfer<FolderNodesTransfer>();
            NodesViewTransfer.Sort((x => x), comparer);
        }


        private string TrasferCheckError(string statusUpload)
        {
            if (statusUpload.Equals("Запуск") | statusUpload.Equals("Поиск...") | statusUpload.Equals("Подготовка") | statusUpload.Equals("Ожидает"))
            {
                statusUpload = "Ошибка";
            }

            return statusUpload;
        }
    }
}
