﻿using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.statusWorker
{
    public class CopyStatusController
    {
        //отображает когда заканчивается копирование 
        public bool isCopyStatus { get; set; }

        //для функции, что мы выбрали вырезать, а не просто копировать
        public bool isMove{ get; set; }

        //отвечает за проверку, что все пакеты синхронизировались т.к используется после копирования еще и удаление
        public bool isMoveNetwork{ get; set; }

        public CopyStatusController(Container cont)
        {
            
        }
    }
}
