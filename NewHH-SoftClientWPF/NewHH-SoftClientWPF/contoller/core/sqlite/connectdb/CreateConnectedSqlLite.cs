﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.sqlite.filesystemdb;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sqlite.connectdb
{
    class CreateConnectedSqlLite
    {
        private string fileBasesDir = staticVariable.Variable.getBasesDir();
        private ExceptionController error;

        public CreateConnectedSqlLite(ExceptionController error )
        {
            this.error = error;
        }

        //создание бд. проверяет если она создана то нечего не делаем 
        public void CreateBases()
        {
            //если базы мы так и не нашли создаем ее и создаем структуру
            if (File.Exists(fileBasesDir) != true)
            {

                //создает бд
                CreateDataBases createDataBases = new CreateDataBases();
                createDataBases.StartSqliteBases(fileBasesDir);

                //создаем таблицу бд
                CreateTableDataBases createTableBases = new CreateTableDataBases();
                SQLiteConnection connect = getConnected(fileBasesDir);

                try
                {
                    createTableBases.StartCreateTable(connect);
                }
                catch (System.Data.SQLite.SQLiteException ex)
                {
                    error.sendError((int)CodeError.DATABASE_ERROR, "SqliteController -> createTableBases() Критическая Ошибка создания таблицы базы данных sqlite", ex.ToString());
                    throw new BaseException((int)CodeError.DATABASE_ERROR, "SqliteController -> createTableBases() Критическая Ошибка создания таблицы базы данных sqlite", ex.ToString());
                }


            }




        }


        private SQLiteConnection connect;

        //возвращает коннект к базе данных
        public SQLiteConnection getConnected(string currentFileDir)
        {
            //если он создан
            if (connect != null)
            {
                return connect;
            }
            else
            {
                connect = new SQLiteConnection("Data Source="+ currentFileDir + ";Version=3;Synchronous=OFF;journal mode=Off;cache_size = 5000");
                connect.Open();
                //Thread.Sleep(100);
                return connect;
            }

        }


    }
}
