﻿using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.insertSql
{
    public interface ISqlInsert
    {
        void insertUsernameAndPassword(string username, string password);
        void insertAutoScanInfo(AutoScanInfoModel autoScanInfoModel);
        void insertSystemSetting(SystemSettingModel ssm);

        void insertAutoScanTimemill(string autoScanMill);

        void insertListSavePc(List<long[][]> allListFull, int intId);

        void insertLocationDownload(string locationDownload);

        void insertScanFolder(string fullPath);

        void insertListScanFolder(ObservableCollection<ScanFolderModel> listScanFolder);

        void insertListFilesTempIndex(long index);

        void insertFileListSinglRows(FileInfoModel modelServer);

        void InsertFileInfoTransaction(List<FileInfoModel> list, MainWindowViewModel _viewModelMain);

        void InsertFileTempTransaction(FileInfoModel[] list, int numberOperation);

        void InsertAutoSaveFileTransaction(FileInfoModel[] list, int numberOperation);

        void InsertFileTransferTransaction(FileInfoTransfer[] list);

        void CopyTempInSavePc();

        void CopySavePcInTemp();
    }
}
