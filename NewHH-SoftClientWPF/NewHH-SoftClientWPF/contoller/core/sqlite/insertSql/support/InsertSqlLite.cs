﻿using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Model;
namespace NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb
{
    public class InsertSqlLite
    {
       
      
        //bool[0]   > пользователь не найден
        //bool[1]   > не верный пароль
        public void insertUsernameAndPassword(string username, string password, SQLiteConnection con)
        {
            bool[] array = new bool[2];
            string date = DateTime.Now.ToString();
           
                SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO users (username, password , authdate) VALUES (?,?,?)", con);

                SQLiteParameter p1 = new SQLiteParameter("username", username);
                SQLiteParameter p2 = new SQLiteParameter("password", password);
                SQLiteParameter p3 = new SQLiteParameter("authdate", date);

                insertSQL.Parameters.Add(p1);
                insertSQL.Parameters.Add(p2);
                insertSQL.Parameters.Add(p3);

                try
                {
                    insertSQL.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

        }

        public void copyTempSavePcToSavePc(SQLiteConnection con)
        {

            //Dictionary<long, long> listRow_idSaveToPc = new Dictionary<long, long>();


            string stm = "INSERT INTO listSaveToPc_listFiles SELECT * FROM listSaveTopPcTemp;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {

                    //SQLiteCommand command = new SQLiteCommand(stm, con);

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (System.Data.SQLite.SQLiteException s)
                    {
                        Debug.Print("SelectSaveToPc->copyTempSavePcToSavePc: критическая ошибка " + s.ToString());
                        throw;
                    }
                }
            }
            catch (Exception s)
            {
                Debug.Print("SelectSaveToPc->copyTempSavePcToSavePc: критическая ошибка " + s.ToString());
                throw;
            }


        }

        public void copySavePcToTempSavePc(SQLiteConnection con)
        {



            string stm = "INSERT INTO listSaveTopPcTemp SELECT * FROM listSaveToPc_listFiles;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {

                    SQLiteCommand command = new SQLiteCommand(stm, con);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (System.Data.SQLite.SQLiteException)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }


        }


        public void insertAutoScanTime(string autoScanMill , SQLiteConnection con)
        {
         
            SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO systemSetting (autoScan) VALUES (?)", con);
            SQLiteParameter p1 = new SQLiteParameter("autoScan", autoScanMill);

            insertSQL.Parameters.Add(p1);


            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public void insertAutoScanInfo(AutoScanInfoModel autoScanInfoModel, SQLiteConnection con)
        {

            SQLiteCommand insertSQL = new SQLiteCommand("REPLACE INTO autoScanInfo (row_id , folder , files , lastDataScan ,  timeSpent,  allCount) VALUES (?,?,?,?,?,?);", con);
            SQLiteParameter p1 = new SQLiteParameter("row_id", SqlDbType.Int);
            SQLiteParameter p2 = new SQLiteParameter("folder", SqlDbType.Int);
            SQLiteParameter p3 = new SQLiteParameter("files", SqlDbType.Int);
            SQLiteParameter p4 = new SQLiteParameter("lastDataScan", autoScanInfoModel.lastDataScan);
            SQLiteParameter p5 = new SQLiteParameter("timeSpent", autoScanInfoModel.timeSpent);
            SQLiteParameter p6 = new SQLiteParameter("allCount", SqlDbType.Int);

            p1.Value = 1;
            p2.Value = autoScanInfoModel.folder;
            p3.Value = autoScanInfoModel.files;
            p6.Value = autoScanInfoModel.allCount;

            insertSQL.Parameters.Add(p1);
            insertSQL.Parameters.Add(p2);
            insertSQL.Parameters.Add(p3);
            insertSQL.Parameters.Add(p4);
            insertSQL.Parameters.Add(p5);
            insertSQL.Parameters.Add(p6);


            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }



        public void insertListSavePc(List<long[][]> allListFull , SQLiteConnection con , int intId)
        {
            try
            {
                SQLiteTransaction transaction = con.BeginTransaction();
                long[][] list = allListFull[0];



                for (int a = 0; a < list.Length; a++)
                {


                    if (list[a] != null)
                    {
                        long row_id = list[a][0];

                        string insertString = "INSERT OR IGNORE INTO listSaveToPc_listFiles (row_id_listfiles , location , parent , filename , type , sizebyte , saveToPc ) SELECT listFiles_users.row_id , listFiles_users.location , listFiles_users.parent , listFiles_users.filename , listFiles_users.type , listFiles_users.sizebyte , '" + intId + "' FROM listFiles_users WHERE listFiles_users.row_id='" + row_id + "';";
                        SQLiteCommand command = new SQLiteCommand(insertString, con);

                        command.ExecuteNonQuery();
                        command.Dispose();
                    }


                }

                transaction.Commit();
                transaction.Dispose();
            }
            catch (SQLiteException)
            {
                throw;
            }


        }

        //Задать новую папку хранения данных
        public void insertLocationDownload(string locationDownload , SQLiteConnection con)
        {
            SQLiteCommand insertSQL = new SQLiteCommand("REPLACE INTO location (locationDownload) VALUES (?)", con);
            SQLiteParameter p1 = new SQLiteParameter("locationDownload", locationDownload);

            insertSQL.Parameters.Add(p1);
     

            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void insertScanFolder(string pathFolder, SQLiteConnection con)
        {
            SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO scanFolder(fullPath) VALUES (?);", con);
            SQLiteParameter p1 = new SQLiteParameter("fullPath", pathFolder);
            insertSQL.Parameters.Add(p1);

            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void insertListScanFolder(ObservableCollection<ScanFolderModel> listScanFolder, SQLiteConnection con)
        {
            try
            {
                SQLiteTransaction transaction = con.BeginTransaction();

                foreach (ScanFolderModel path in listScanFolder)
                {
                    SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO scanFolder(fullPath) VALUES (?);", con);
                    SQLiteParameter p1 = new SQLiteParameter("fullPath", path.fullPath);
                    insertSQL.Parameters.Add(p1);


                    insertSQL.ExecuteNonQuery();
                }

                transaction.Commit();
                transaction.Dispose();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public void insertSystemSetting(SystemSettingModel ssm, SQLiteConnection con)
        {
            //SQLiteCommand insertSQL = new SQLiteCommand("REPLACE INTO autoScanInfo (row_id , folder , files , lastDataScan ,  timeSpent,  allCount) VALUES (?,?,?,?,?,?);", con);
            SQLiteCommand insertSQL = new SQLiteCommand("REPLACE INTO systemSetting(row_id , autoScan ,autoStartApplication , startFolder) VALUES (?,?,?,?);", con);

            SQLiteParameter p1 = new SQLiteParameter("row_id", SqlDbType.Int);
            p1.Value = 1;
            SQLiteParameter p2 = new SQLiteParameter("autoScan", ssm.autoScan);

            SQLiteParameter p3 = new SQLiteParameter("autoStartApplication", SqlDbType.Int);
            p3.Value = ssm.autoStartApplication;

            SQLiteParameter p4 = new SQLiteParameter("startFolder", SqlDbType.Int);
            p4.Value = ssm.startFolder;




            insertSQL.Parameters.Add(p1);
            insertSQL.Parameters.Add(p2);
            insertSQL.Parameters.Add(p3);
            insertSQL.Parameters.Add(p4);


            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public void insertListFilesTempIndex(long index ,  SQLiteConnection con)
        {
            SQLiteCommand insertSQL = new SQLiteCommand("REPLACE INTO listFilesRowIdTemp (row_id , row_id_temp) VALUES (1,?);", con);
            SQLiteParameter p1 = new SQLiteParameter("row_id_temp", SqlDbType.Int);
            p1.Value = index;
            insertSQL.Parameters.Add(p1);


            try
            {
                insertSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public void insertFileListSinglRows(FileInfoModel modelServer, SQLiteConnection con)
        {

            try
            {
                //string insertString = "INSERT INTO listFiles_users (row_id , filename , createDate , changeDate , lastOpenDate , attribute , location , type , sizebyte , parent , versionUpdateBases , versionUpdateRows , user_id , changeRows) SELECT (?,?,?,?,?,?,?,?,?,?,?,?,?,?)  WHERE NOT EXISTS(SELECT "+ modelServer.row_id + " FROM listFiles_users WHERE row_id = " + modelServer.row_id + "); ";
                string insertString = "REPLACE INTO listFiles_users (row_id , filename , createDate , changeDate , lastOpenDate , attribute , location , type , sizebyte , parent , versionUpdateBases , versionUpdateRows , user_id , changeRows , saveTopc ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                SQLiteCommand command = new SQLiteCommand();

                //все конвертируем в стринг т.к sqlite не умеет хранить long 
                int row_id_server = (int)modelServer.row_id;
                string filename_server = modelServer.filename.ToString();
                string createDate_server = modelServer.createDate.ToString();
                string changeDate_server = modelServer.changeDate.ToString();
                string lastOpenDate_server = modelServer.lastOpenDate.ToString();
                string attribute_server = modelServer.attribute.ToString();
                string location_server = modelServer.location.ToString();
                string type_server = modelServer.type.ToString();
                string sizebyte_server = modelServer.sizebyte.ToString();
                string parent_server = modelServer.parent.ToString();
                string versionUpdateBases_server = modelServer.versionUpdateBases.ToString();
                string versionUpdateRows_server = modelServer.versionUpdateRows.ToString();
                string user_id_server = modelServer.user_id.ToString();
                string changeRows = "OK";


                SQLiteParameter p0 = new SQLiteParameter("row_id", SqlDbType.Int);
                SQLiteParameter p1 = new SQLiteParameter("filename", filename_server);
                SQLiteParameter p2 = new SQLiteParameter("createDate", createDate_server);
                SQLiteParameter p3 = new SQLiteParameter("changeDate", changeDate_server);
                SQLiteParameter p4 = new SQLiteParameter("lastOpenDate", lastOpenDate_server);
                SQLiteParameter p5 = new SQLiteParameter("attribute", attribute_server);
                SQLiteParameter p6 = new SQLiteParameter("location", location_server);
                SQLiteParameter p7 = new SQLiteParameter("type", type_server);
                SQLiteParameter p8 = new SQLiteParameter("sizebyte", sizebyte_server);
                SQLiteParameter p9 = new SQLiteParameter("parent", parent_server);
                SQLiteParameter p10 = new SQLiteParameter("versionUpdateBases", versionUpdateBases_server);
                SQLiteParameter p11 = new SQLiteParameter("versionUpdateRows", versionUpdateRows_server);
                SQLiteParameter p12 = new SQLiteParameter("user_id", user_id_server);
                SQLiteParameter p13 = new SQLiteParameter("changeRows", changeRows);
                SQLiteParameter p14 = new SQLiteParameter("saveTopc", SqlDbType.Int);
                p14.Value = modelServer.saveTopc;
                p0.Value = row_id_server;

                command.CommandText = insertString;
                command.Connection = con;

                command.Parameters.Add(p0);
                command.Parameters.Add(p1);
                command.Parameters.Add(p2);
                command.Parameters.Add(p3);
                command.Parameters.Add(p4);
                command.Parameters.Add(p5);
                command.Parameters.Add(p6);
                command.Parameters.Add(p7);
                command.Parameters.Add(p8);
                command.Parameters.Add(p9);
                command.Parameters.Add(p10);
                command.Parameters.Add(p11);
                command.Parameters.Add(p12);
                command.Parameters.Add(p13);
                command.Parameters.Add(p14);
              


                command.ExecuteNonQuery();
                command.Dispose();

            }
            catch (SQLiteException)
            {
                throw;
            }



        }
        int index2 = 0;

        //numberOperation - номер операции(т.е что-бы одновременно хранить несколько временных данных)
        public void insertFileTempTransaction(FileInfoModel[] list, SQLiteConnection con , int numberOperation)
        {
            try
            {
               
                SQLiteTransaction transaction = con.BeginTransaction();

                for (int a = 0; a < list.Length; a++)
                {
                    

                    if (list[a] != null)
                    {
                        FileInfoModel modelServer = list[a];

                        string insertString = "INSERT INTO listFilesTemp(row_id , filename , createDate , changeDate , lastOpenDate , attribute , location , type , sizebyte , parent , versionUpdateBases , versionUpdateRows , user_id , changeRows , numberOperation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        SQLiteCommand command = new SQLiteCommand();



                        //все конвертируем в стринг т.к sqlite не умеет хранить long 
                        int row_id_server = (int)modelServer.row_id;
                        string filename_server = modelServer.filename.ToString();
                        string createDate_server = modelServer.createDate.ToString();
                        string changeDate_server = modelServer.changeDate.ToString();
                        string lastOpenDate_server = modelServer.lastOpenDate.ToString();
                        string attribute_server = modelServer.attribute.ToString();
                        string location_server = modelServer.location.ToString();
                        string type_server = modelServer.type.ToString();
                        string sizebyte_server = modelServer.sizebyte.ToString();
                        string parent_server = modelServer.parent.ToString();
                        string versionUpdateBases_server = modelServer.versionUpdateBases.ToString();
                        string versionUpdateRows_server = modelServer.versionUpdateRows.ToString();
                        string user_id_server = modelServer.user_id.ToString();
                        string changeRows = "DELETE";


                        SQLiteParameter p0 = new SQLiteParameter("row_id", SqlDbType.Int);
                        SQLiteParameter p1 = new SQLiteParameter("filename", filename_server);
                        SQLiteParameter p2 = new SQLiteParameter("createDate", createDate_server);
                        SQLiteParameter p3 = new SQLiteParameter("changeDate", changeDate_server);
                        SQLiteParameter p4 = new SQLiteParameter("lastOpenDate", lastOpenDate_server);
                        SQLiteParameter p5 = new SQLiteParameter("attribute", attribute_server);
                        SQLiteParameter p6 = new SQLiteParameter("location", location_server);
                        SQLiteParameter p7 = new SQLiteParameter("type", type_server);
                        SQLiteParameter p8 = new SQLiteParameter("sizebyte", sizebyte_server);
                        SQLiteParameter p9 = new SQLiteParameter("parent", parent_server);
                        SQLiteParameter p10 = new SQLiteParameter("versionUpdateBases", versionUpdateBases_server);
                        SQLiteParameter p11 = new SQLiteParameter("versionUpdateRows", versionUpdateRows_server);
                        SQLiteParameter p12 = new SQLiteParameter("user_id", user_id_server);
                        SQLiteParameter p13 = new SQLiteParameter("changeRows", changeRows);
                        SQLiteParameter p14 = new SQLiteParameter("numberOperation", SqlDbType.Int);
                        p14.Value = numberOperation;
                        p0.Value = row_id_server;

                        command.CommandText = insertString;
                        command.Connection = con;

                        command.Parameters.Add(p0);
                        command.Parameters.Add(p1);
                        command.Parameters.Add(p2);
                        command.Parameters.Add(p3);
                        command.Parameters.Add(p4);
                        command.Parameters.Add(p5);
                        command.Parameters.Add(p6);
                        command.Parameters.Add(p7);
                        command.Parameters.Add(p8);
                        command.Parameters.Add(p9);
                        command.Parameters.Add(p10);
                        command.Parameters.Add(p11);
                        command.Parameters.Add(p12);
                        command.Parameters.Add(p13);
                        command.Parameters.Add(p14);


                        command.ExecuteNonQuery();
                        command.Dispose();
                    }
                    

                }


                Console.WriteLine("Пакет обработан WebDavClient Calss_ InsertSqlLite ->  insertFileTempTransaction " + "  индекс массива " + index2);
                transaction.Commit();
                transaction.Dispose();
                list = null;


            }
            catch (SQLiteException)
            {
                throw;
            }

          
        }


        public void insertAutoSaveFileTransaction(FileInfoModel[] list, SQLiteConnection con, int numberOperation)
        {
            try
            {
              
                SQLiteTransaction transaction = con.BeginTransaction();

                for (int a = 0; a < list.Length; a++)
                {


                    if (list[a] != null)
                    {
                        FileInfoModel modelServer = list[a];

                        string insertString = "INSERT INTO listScanFolder_ListFiles(row_id_listfiles , filename , createDate , changeDate , lastOpenDate , attribute , location , type , sizebyte , parent , versionUpdateBases , versionUpdateRows , user_id , changeRows , numberOperation) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        SQLiteCommand command = new SQLiteCommand();



                        //все конвертируем в стринг т.к sqlite не умеет хранить long 
                        int row_id_server = (int)modelServer.row_id;
                        string filename_server = modelServer.filename.ToString();
                        string createDate_server = modelServer.createDate.ToString();
                        string changeDate_server = modelServer.changeDate.ToString();
                        string lastOpenDate_server = modelServer.lastOpenDate.ToString();
                        string attribute_server = modelServer.attribute.ToString();
                        string location_server = modelServer.location.ToString();
                        string type_server = modelServer.type.ToString();
                        string sizebyte_server = modelServer.sizebyte.ToString();
                        string parent_server = modelServer.parent.ToString();
                        string versionUpdateBases_server = modelServer.versionUpdateBases.ToString();
                        string versionUpdateRows_server = modelServer.versionUpdateRows.ToString();
                        string user_id_server = modelServer.user_id.ToString();
                        string changeRows = "DELETE";


                        SQLiteParameter p0 = new SQLiteParameter("row_id_listfiles", SqlDbType.Int);
                        SQLiteParameter p1 = new SQLiteParameter("filename", filename_server);
                        SQLiteParameter p2 = new SQLiteParameter("createDate", createDate_server);
                        SQLiteParameter p3 = new SQLiteParameter("changeDate", changeDate_server);
                        SQLiteParameter p4 = new SQLiteParameter("lastOpenDate", lastOpenDate_server);
                        SQLiteParameter p5 = new SQLiteParameter("attribute", attribute_server);
                        SQLiteParameter p6 = new SQLiteParameter("location", location_server);
                        SQLiteParameter p7 = new SQLiteParameter("type", type_server);
                        SQLiteParameter p8 = new SQLiteParameter("sizebyte", sizebyte_server);
                        SQLiteParameter p9 = new SQLiteParameter("parent", parent_server);
                        SQLiteParameter p10 = new SQLiteParameter("versionUpdateBases", versionUpdateBases_server);
                        SQLiteParameter p11 = new SQLiteParameter("versionUpdateRows", versionUpdateRows_server);
                        SQLiteParameter p12 = new SQLiteParameter("user_id", user_id_server);
                        SQLiteParameter p13 = new SQLiteParameter("changeRows", changeRows);
                        SQLiteParameter p14 = new SQLiteParameter("numberOperation", SqlDbType.Int);
                        p14.Value = numberOperation;
                        p0.Value = row_id_server;

                        command.CommandText = insertString;
                        command.Connection = con;

                        command.Parameters.Add(p0);
                        command.Parameters.Add(p1);
                        command.Parameters.Add(p2);
                        command.Parameters.Add(p3);
                        command.Parameters.Add(p4);
                        command.Parameters.Add(p5);
                        command.Parameters.Add(p6);
                        command.Parameters.Add(p7);
                        command.Parameters.Add(p8);
                        command.Parameters.Add(p9);
                        command.Parameters.Add(p10);
                        command.Parameters.Add(p11);
                        command.Parameters.Add(p12);
                        command.Parameters.Add(p13);
                        command.Parameters.Add(p14);


                        command.ExecuteNonQuery();
                        command.Dispose();
                    }


                }


                Console.WriteLine("Пакет обработан WebDavClient Calss_ InsertSqlLite ->  insertAutoSaveFileTransaction " + "  индекс массива " + index2);
                transaction.Commit();
                transaction.Dispose();
                list = null;


            }
            catch (SQLiteException)
            {
             
                throw;
            }


        }


        public void insertFileTransferTransaction(FileInfoTransfer[] list, SQLiteConnection con)
        {
            try
            {
               
                SQLiteTransaction transaction = con.BeginTransaction();

                for (int a = 0; a < list.Length; a++)
                {


                    if (list[a] != null)
                    {
                        FileInfoTransfer modelServer = list[a];

                        string insertString = "INSERT INTO listFilesTransfer(row_id , row_id_listFiles_users , filename , sizebyte , type , progress , uploadStatus , typeTransfer) VALUES (?,?,?,?,?,?,?,?);";
                        SQLiteCommand command = new SQLiteCommand();
                        
                        //все конвертируем в стринг т.к sqlite не умеет хранить long 
                        int row_id_listfiles_users = modelServer.row_id_listFiles_users;
                        int row_id_transfer = modelServer.row_id;
                        string filename_server = modelServer.filename.ToString();
                        string sizebyte = modelServer.sizebyte;
                        string type = modelServer.type;
                        double progress_server = modelServer.progress;
                        string uploadStatus = modelServer.uploadStatus;
                        string typeTransfer = modelServer.typeTransfer;

                        SQLiteParameter p0 = new SQLiteParameter("row_id", SqlDbType.Int);
                        SQLiteParameter p1 = new SQLiteParameter("row_id_listFiles_users", SqlDbType.Int);
                        SQLiteParameter p2 = new SQLiteParameter("filename", filename_server);
                        SQLiteParameter p3 = new SQLiteParameter("sizebyte", sizebyte);
                        SQLiteParameter p4 = new SQLiteParameter("type", type);
                        SQLiteParameter p5 = new SQLiteParameter("progress", SqlDbType.Real);
                        SQLiteParameter p6 = new SQLiteParameter("uploadStatus", uploadStatus);
                        SQLiteParameter p7 = new SQLiteParameter("typeTransfer", typeTransfer);

                        p0.Value = row_id_transfer;
                        p1.Value = row_id_listfiles_users;
                        p5.Value = progress_server;

                        command.CommandText = insertString;
                        command.Connection = con;

                        command.Parameters.Add(p0);
                        command.Parameters.Add(p1);
                        command.Parameters.Add(p2);
                        command.Parameters.Add(p3);
                        command.Parameters.Add(p4);
                        command.Parameters.Add(p5);
                        command.Parameters.Add(p6);
                        command.Parameters.Add(p7);

                        command.ExecuteNonQuery();
                        command.Dispose();
                    }


                }


                Console.WriteLine("Пакет обработан WebDavClient Calss_ InsertSqlLite ->  insertFileTransferTransaction " + "  индекс массива " + index2);
                transaction.Commit();
                transaction.Dispose();
                list = null;


            }
            catch (SQLiteException)
            {
              
                throw;
            }


        }





        public void insertFileListTransaction(List<FileInfoModel> list , SQLiteConnection con , MainWindowViewModel _viewModelMain)
        {


            if (list.Count <= 0) return;
            SQLiteTransaction transaction = con.BeginTransaction();
            long row_id_error = 0;
            string filename = "";
            try
            {

                int k = 0;
                list.ForEach(delegate (FileInfoModel modelServer)
                {
                    


                        if (modelServer != null)
                        {
                           //if (modelServer.row_id != -1)
                          // {

                    
                            double procent = k++ / 10;
                            _viewModelMain.pg = procent;
                            row_id_error = modelServer.row_id;

                            string insertString = "INSERT INTO listFiles_users (row_id , filename , createDate , changeDate , lastOpenDate , attribute , location , type , sizebyte , parent , versionUpdateBases , versionUpdateRows , user_id , changeRows , saveTopc) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                            SQLiteCommand command = new SQLiteCommand();

                            //все конвертируем в стринг т.к sqlite не умеет хранить long 
                            int row_id_server = (int)modelServer.row_id;
                            string filename_server = modelServer.filename.ToString();
                            filename = filename_server;
                            string createDate_server = modelServer.createDate.ToString();
                            string changeDate_server = modelServer.changeDate.ToString();
                            string lastOpenDate_server = modelServer.lastOpenDate.ToString();
                            string attribute_server = modelServer.attribute.ToString();
                            string location_server = modelServer.location.ToString();
                            string type_server = modelServer.type.ToString();
                            string sizebyte_server = modelServer.sizebyte.ToString();
                            string parent_server = modelServer.parent.ToString();
                            string versionUpdateBases_server = modelServer.versionUpdateBases.ToString();
                            string versionUpdateRows_server = modelServer.versionUpdateRows.ToString();
                            string user_id_server = modelServer.user_id.ToString();
                            string changeRows = "OK";




                            SQLiteParameter p0 = new SQLiteParameter("row_id", SqlDbType.Int);
                            SQLiteParameter p1 = new SQLiteParameter("filename", filename_server);
                            SQLiteParameter p2 = new SQLiteParameter("createDate", createDate_server);
                            SQLiteParameter p3 = new SQLiteParameter("changeDate", changeDate_server);
                            SQLiteParameter p4 = new SQLiteParameter("lastOpenDate", lastOpenDate_server);
                            SQLiteParameter p5 = new SQLiteParameter("attribute", attribute_server);
                            SQLiteParameter p6 = new SQLiteParameter("location", location_server);
                            SQLiteParameter p7 = new SQLiteParameter("type", type_server);
                            SQLiteParameter p8 = new SQLiteParameter("sizebyte", sizebyte_server);
                            SQLiteParameter p9 = new SQLiteParameter("parent", parent_server);
                            SQLiteParameter p10 = new SQLiteParameter("versionUpdateBases", versionUpdateBases_server);
                            SQLiteParameter p11 = new SQLiteParameter("versionUpdateRows", versionUpdateRows_server);
                            SQLiteParameter p12 = new SQLiteParameter("user_id", user_id_server);
                            SQLiteParameter p13 = new SQLiteParameter("changeRows", changeRows);


                            SQLiteParameter p14 = new SQLiteParameter("saveTopc", SqlDbType.Int);
                            p14.Value = modelServer.saveTopc;
                            p0.Value = row_id_server;

                            command.CommandText = insertString;
                            command.Connection = con;

                            command.Parameters.Add(p0);
                            command.Parameters.Add(p1);
                            command.Parameters.Add(p2);
                            command.Parameters.Add(p3);
                            command.Parameters.Add(p4);
                            command.Parameters.Add(p5);
                            command.Parameters.Add(p6);
                            command.Parameters.Add(p7);
                            command.Parameters.Add(p8);
                            command.Parameters.Add(p9);
                            command.Parameters.Add(p10);
                            command.Parameters.Add(p11);
                            command.Parameters.Add(p12);
                            command.Parameters.Add(p13);
                            command.Parameters.Add(p14);

                          
                            command.ExecuteNonQuery();
                            command.Dispose();
                        
                       // }

                    }
                        else
                        {
                            // Console.WriteLine("Одно из значений оказалось пустым Calss_ InsertSqlLite -> insertFileListTransaction");
                        }


                    
                    

                });


                Console.WriteLine("Пакет обработан 1000 Calss_ InsertSqlLite -> insertFileListTransaction");
                transaction.Commit();
                list = null;


            }
            catch (SQLiteException f)
            {
                Console.WriteLine("SqlliteCOntroller->InsertSqllite: ошибка insert row_id: "+ row_id_error + " filename "+ filename + "\n "+f.ToString());

               // throw;
            }
        }
    }
}
