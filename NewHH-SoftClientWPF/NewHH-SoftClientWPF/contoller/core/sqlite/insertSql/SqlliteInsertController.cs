﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.core.sqlite.insertSql.support;
using NewHH_SoftClientWPF.contoller.core.sqlite.stacksql;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.contoller.util.enumSystem;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.insertSql
{
    public class SqlliteInsertController:  ISqlInsert
    {
        
        private ExceptionController error;
        private StackSql stackSql;

        public SqlliteInsertController(ExceptionController error , StackSql stackSql)
        {
            this.error = error;
            this.stackSql = stackSql;
        }

        

        //вставляет юзера в базу данных
        public void insertUsernameAndPassword(string username, string password)
        {
            stackSql.AddItemStack(new StackItemSql<object>(username, password, (int)SqlCode.InsertUsernameAndPassword));
             //dirs.Add(new StackItemSql<object>(username, password, (int)SqlCode.InsertUsernameAndPassword));
          
                //insertSqlite.insertUsernameAndPassword(username, password, classcon.GetConnectionSqlite());
        }


     
        public void insertAutoScanInfo(AutoScanInfoModel autoScanInfoModel)
        {
            stackSql.AddItemStack(new StackItemSql<object>(autoScanInfoModel, (int)SqlCode.InsertAutoScanInfo));
         
            //insertSqlite.insertAutoScanInfo(autoScanInfoModel , classcon.GetConnectionSqlite());
        }

        public void insertSystemSetting(SystemSettingModel ssm)
        {
            //insertSqlite.insertSystemSetting(ssm, classcon.GetConnectionSqlite());
            stackSql.AddItemStack(new StackItemSql<object>(ssm, (int)SqlCode.InsertSystemSetting));
       
        }



        public void insertAutoScanTimemill(string autoScanMill)
        {
            //insertSqlite.insertAutoScanTime(autoScanMill, classcon.GetConnectionSqlite());
            stackSql.AddItemStack(new StackItemSql<object>(autoScanMill, (int)SqlCode.InsertAutoScanTimemill));
         
        }


        public void insertListSavePc(List<long[][]> allListFull, int intId)
        {
            //insertSqlite.insertListSavePc(allListFull, classcon.GetConnectionSqlite(), intId);
            stackSql.AddItemStack(new StackItemSql<object>(allListFull, intId, (int)SqlCode.InsertListSavePc));
           

        }



        //вставляет юзера в базу данных
        public void insertLocationDownload(string locationDownload)
        {
            stackSql.AddItemStack(new StackItemSql<object>(locationDownload, (int)SqlCode.InsertLocationDownload));
      
        }

        public void insertScanFolder(string fullPath)
        {
            //insertSqlite.insertScanFolder(fullPath, classcon.GetConnectionSqlite());
            stackSql.AddItemStack(new StackItemSql<object>(fullPath, (int)SqlCode.InsertScanFolder));
        
        }

        public void insertListScanFolder(ObservableCollection<ScanFolderModel> listScanFolder)
        {

            // insertSqlite.insertListScanFolder(listScanFolder, classcon.GetConnectionSqlite());
            stackSql.AddItemStack(new StackItemSql<object>(listScanFolder, (int)SqlCode.InsertListScanFolder));
        }



        
        public void insertListFilesTempIndex(long index)
        {

            //insertSqlite.insertListFilesTempIndex(index, classcon.GetConnectionSqlite());
            stackSql.AddItemStack(new StackItemSql<object>(index, (int)SqlCode.InsertListFilesTempIndex));
   
        }
       
        public void insertFileListSinglRows(FileInfoModel modelServer)
        {
            //insertSqlite.insertFileListSinglRows(modelServer, classcon.GetConnectionSqlite());
            stackSql.AddItemStack(new StackItemSql<object>(modelServer, (int)SqlCode.InsertFileListSinglRows));
      
        }
        public void InsertFileInfoTransaction(List<FileInfoModel> list, MainWindowViewModel viewModelMain)
        {
            Debug.Print("Вставка InsertFileInfoTransaction");
            stackSql.AddItemStack(new StackItemSql<object>(list, viewModelMain , (int)SqlCode.InsertFileInfoTransaction));
        }

        //Добавляет во временное хранилище 
        //Полученные файлы после сканирования папки "На удаление"
        public void InsertFileTempTransaction(FileInfoModel[] arr, int numberOperation)
        {
            List<FileInfoModel> list = arr.ToList();
            //insertSqlite.insertFileTempTransaction(list, classcon.GetConnectionSqlite() ,  numberOperation);
            stackSql.AddItemStack(new StackItemSql<object>(list, numberOperation, (int)SqlCode.InsertFileTempTransaction));
        }

        public void InsertAutoSaveFileTransaction(FileInfoModel[] arr, int numberOperation)
        {
            // insertSqlite.insertAutoSaveFileTransaction(list, classcon.GetConnectionSqlite(), numberOperation);
            List<FileInfoModel> list = arr.ToList();
            stackSql.AddItemStack(new StackItemSql<object>(list, numberOperation, (int)SqlCode.InsertAutoSaveFileTransaction));
        }

        //Добавляет в хранилище transfer
        public void InsertFileTransferTransaction(FileInfoTransfer[] arr)
        {
            //insertSqlite.insertFileTransferTransaction(list, classcon.GetConnectionSqlite());
            List<FileInfoTransfer> list = arr.ToList();
            stackSql.AddItemStack(new StackItemSql<object>(list, (int)SqlCode.InsertFileTransferTransaction));
        }

        public void CopyTempInSavePc()
        {
            stackSql.AddItemStack(new StackItemSql<object>(new List<FileInfoTransfer>() , (int)SqlCode.CopyTempInSavePc));
        }

        public void CopySavePcInTemp()
        {
            stackSql.AddItemStack(new StackItemSql<object>(new List<FileInfoTransfer>(), (int)SqlCode.CopySavePcToTempSavePc));
        }

        private BaseException generatedError(int codeError, string textError, string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError);
        }
        //Нужна задержка что-бы данные успели добавиться в базу т.к дальше идет проверка, что данные были добавлены и нужно что-то с ними делать
        //Если добавлять без задержки мы не сможем найти данные т.к они еще не добавились
      
    }
}
