﻿using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.stacksql
{
    public interface IStackSql
    {
        void AddItemStack(StackItemSql<object> item);
    }
}
