﻿using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.stacksql.support.insert
{
    public interface IInsertStack
    {
         void TwoParamContains(StackItemSql<object> sqlItem);
         void SingleParamContains(StackItemSql<object> sqlItem);
    }
}
