﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.contoller.util.enumSystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.stacksql.support.insert
{
    public class InsertStack<T> : IInsertStack
    {
        private Dictionary<int, odiData2> dictData1;
        private Dictionary<int, odiData1> dictData2;
        private delegate int odiData2(T data1, T data2);
        private delegate int odiData1(T data1);
        private ConnectedSqllite classcon;

        private InsertSqlLite sqlInsert;

        public InsertStack(InsertSqlLite sqlInsert , ConnectedSqllite classcon)
        {
            this.sqlInsert = sqlInsert;
            this.classcon = classcon;
            InitDict1();
            InitDict2();
        }
        public void TwoParamContains(StackItemSql<object> sqlItem)
        {
            if (dictData1.ContainsKey(sqlItem.GetOperation()))
            {
                int s = sqlItem.GetOperation();
                dictData1[s]((T)sqlItem.GetData1(), (T)sqlItem.GetData2());
            }
        }

        public void SingleParamContains(StackItemSql<object> sqlItem)
        {
            if (dictData2.ContainsKey(sqlItem.GetOperation()))
            {
                int s = sqlItem.GetOperation();
                dictData2[s]((T)sqlItem.GetData1());
            }
        }


        //вставляет юзера в базу данных
        public int insertUsernameAndPassword(T username, T password)
        {
            string user_convert = (string)(object)username;
            string password_convert = (string)(object)username;

            sqlInsert.insertUsernameAndPassword(user_convert, password_convert, classcon.GetConnectionSqlite());

            return (int)SqlCode.InsertUsernameAndPassword;
        }

        //копирует из таблицы в др.таблицу
        public int CopyTempInSavePc(T data1)
        {
            try
            {
                sqlInsert.copyTempSavePcToSavePc(classcon.GetConnectionSqlite());
                return (int)SqlCode.CopyTempInSavePc;
            }
            catch (SqlException ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> copyTempInSavePc() Критическая Ошибка выборки данных", ex.ToString());
                return (int)SqlCode.CopyTempInSavePc;
            }

        }

        public int CopySavePcToTempSavePc(T data1)
        {
            try
            {
                sqlInsert.copySavePcToTempSavePc(classcon.GetConnectionSqlite());
                return (int)SqlCode.CopyTempInSavePc;
            }
            catch (SqlException ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> copyTempInSavePc() Критическая Ошибка выборки данных", ex.ToString());
                return (int)SqlCode.CopyTempInSavePc;
            }

        }
        public int insertAutoScanInfo(T data1)
        {
            try
            {
                AutoScanInfoModel autoScanInfoModel = (AutoScanInfoModel)(object)data1;

                sqlInsert.insertAutoScanInfo(autoScanInfoModel, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertAutoScanInfo;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertAutoScanInfo;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertAutoScanInfo() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public int insertSystemSetting(T data1)
        {
            try
            {
                SystemSettingModel ssm = (SystemSettingModel)(object)data1;
                sqlInsert.insertSystemSetting(ssm, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertSystemSetting;
            }
            catch (Exception ex)
            {

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertAutoScanInfo() Критическая Ошибка вставки данных", ex.ToString());
                return (int)SqlCode.InsertSystemSetting;
            }

        }



        public int insertAutoScanTimemill(T data1)
        {
            try
            {
                string autoScanMill = (string)(object)data1;
                sqlInsert.insertAutoScanTime(autoScanMill, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertAutoScanTimemill;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertAutoScanTimemill;
                // generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertUsernameAndPassword() Критическая Ошибка вставки данных", ex.ToString());

            }

        }


        public int insertListSavePc(T allListFull, T intId)
        {
            try
            {
                List<long[][]> allListFullOrig = (List<long[][]>)(object)allListFull;
                int intIdOrig = (int)(object)intId;

                sqlInsert.insertListSavePc(allListFullOrig, classcon.GetConnectionSqlite(), intIdOrig);

                return (int)SqlCode.InsertListSavePc;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertListSavePc;
                // generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertListSavePc() Критическая Ошибка вставки данных", ex.ToString());
            }

        }

        //вставляет юзера в базу данных
        public int insertLocationDownload(T data1)
        {
            try
            {
                string locationDownload = (string)(object)data1;
                sqlInsert.insertLocationDownload(locationDownload, classcon.GetConnectionSqlite());
                return (int)SqlCode.InsertLocationDownload;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertLocationDownload;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertLocationDownload() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public int insertScanFolder(T data1)
        {
            try
            {
                string fullPath = (string)(object)data1;
                sqlInsert.insertScanFolder(fullPath, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertScanFolder;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertScanFolder;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertScanFolder() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public int insertListScanFolder(T data1)
        {
            try
            {
                ObservableCollection<ScanFolderModel> listScanFolder = (ObservableCollection<ScanFolderModel>)(object)data1;
                sqlInsert.insertListScanFolder(listScanFolder, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertListScanFolder;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertListScanFolder;

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertScanFolder() Критическая Ошибка вставки данных", ex.ToString());

            }

        }




        public int insertListFilesTempIndex(T data1)
        {
            try
            {
                long index = (long)(object)data1;
                sqlInsert.insertListFilesTempIndex(index, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertListFilesTempIndex;
            }
            catch (Exception ex)
            {

                return (int)SqlCode.InsertListFilesTempIndex;
                // generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> insertLocationDownload() Критическая Ошибка вставки данных", ex.ToString());

            }

        }

        public int insertFileListSinglRows(T data1)
        {
            try
            {
                FileInfoModel modelServer = (FileInfoModel)(object)data1;
                sqlInsert.insertFileListSinglRows(modelServer, classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertFileListSinglRows;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertFileListSinglRows;

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileInfoTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }
        }
        public int InsertFileInfoTransaction(T data1, T data2)
        {

            try
            {

                List<FileInfoModel> list = (List<FileInfoModel>)(object)data1;
                MainWindowViewModel _viewModelMain = (MainWindowViewModel)(object)data2;


                sqlInsert.insertFileListTransaction(list, classcon.GetConnectionSqlite(), _viewModelMain);

                return (int)SqlCode.InsertFileInfoTransaction;

            }
            catch (Exception ex)
            {

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileInfoTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());
                return (int)SqlCode.InsertFileInfoTransaction;
            }
        }

        //Добавляет во временное хранилище 
        //Полученные файлы после сканирования папки "На удаление"
        public int InsertFileTempTransaction(T data1, T data2)
        {

            try
            {
                List<FileInfoModel> list = (List<FileInfoModel>)(object)data1;
                int numberOperation = (int)(object)data2;

                sqlInsert.insertFileTempTransaction(list.ToArray(), classcon.GetConnectionSqlite(), numberOperation);

                return (int)SqlCode.InsertFileTempTransaction;
            }
            catch (Exception ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileTempTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());
                return (int)SqlCode.InsertFileTempTransaction;

            }

        }

        public int InsertAutoSaveFileTransaction(T data1, T data2)
        {

            try
            {
                List<FileInfoModel> list = (List<FileInfoModel>)(object)data1;
                int numberOperation = (int)(object)data2;


                sqlInsert.insertAutoSaveFileTransaction(list.ToArray(), classcon.GetConnectionSqlite(), numberOperation);

                return (int)SqlCode.InsertAutoSaveFileTransaction;
            }
            catch (Exception ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileTempTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

                return (int)SqlCode.InsertAutoSaveFileTransaction;

            }

        }

        //Добавляет в хранилище transfer
        public int InsertFileTransferTransaction(T data1)
        {

            try
            {
                List<FileInfoTransfer> list = (List<FileInfoTransfer>)(object)data1;
                sqlInsert.insertFileTransferTransaction(list.ToArray(), classcon.GetConnectionSqlite());

                return (int)SqlCode.InsertFileTransferTransaction;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.InsertFileTransferTransaction;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> InsertFileInfoTransaction() Критическая Ошибка вставка данных в базу", ex.ToString());

            }

        }

        private void InitDict1()
        {
            dictData1 = new Dictionary<int, odiData2>();
            dictData1.Add((int)SqlCode.InsertUsernameAndPassword, insertUsernameAndPassword);
            dictData1.Add((int)SqlCode.InsertListSavePc, insertListSavePc);
            dictData1.Add((int)SqlCode.InsertFileInfoTransaction, InsertFileInfoTransaction);
            dictData1.Add((int)SqlCode.InsertFileTempTransaction, InsertFileTempTransaction);
            dictData1.Add((int)SqlCode.InsertAutoSaveFileTransaction, InsertAutoSaveFileTransaction);
            

        }

        private void InitDict2()
        {
            dictData2 = new Dictionary<int, odiData1>();
            dictData2.Add((int)SqlCode.InsertAutoScanInfo, insertAutoScanInfo);
            dictData2.Add((int)SqlCode.InsertSystemSetting, insertSystemSetting);
            dictData2.Add((int)SqlCode.InsertAutoScanTimemill, insertAutoScanTimemill);
            dictData2.Add((int)SqlCode.InsertLocationDownload, insertLocationDownload);
            dictData2.Add((int)SqlCode.InsertScanFolder, insertScanFolder);
            dictData2.Add((int)SqlCode.InsertListScanFolder, insertListScanFolder);
            dictData2.Add((int)SqlCode.InsertListFilesTempIndex, insertListFilesTempIndex);
            dictData2.Add((int)SqlCode.InsertFileListSinglRows, insertFileListSinglRows);
            dictData2.Add((int)SqlCode.InsertFileTransferTransaction, InsertFileTransferTransaction);
            dictData2.Add((int)SqlCode.CopyTempInSavePc, CopyTempInSavePc);
            dictData2.Add((int)SqlCode.CopySavePcToTempSavePc, CopySavePcToTempSavePc);

        }
    }
}
