﻿using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.stacksql.support.delete
{
    public interface IDeleteStack
    {
        void TwoParamContains(StackItemSql<object> sqlItem);
        void SingleParamContains(StackItemSql<object> sqlItem);
    }
}
