﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.contoller.util.enumSystem;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.stacksql.support.delete
{
    public class DeleteStack<T> : IDeleteStack
    {
        private Dictionary<int, odiData2> dictDelData1;
        private Dictionary<int, odiData1> dictDelData2;
        private delegate int odiData2(T data1, T data2);
        private delegate int odiData1(T data1);
        private ConnectedSqllite classcon;
        private DeleteSqllite sqlDelete;

        public DeleteStack(DeleteSqllite sqlDelete, ConnectedSqllite classcon)
        {
            this.sqlDelete = sqlDelete;
            this.classcon = classcon;
            InitDict1();
            InitDict2();
        }

        public void TwoParamContains(StackItemSql<object> sqlItem)
        {
            try
            {

                if (dictDelData1.ContainsKey(sqlItem.GetOperation()))
                {
                    int s = sqlItem.GetOperation();
                    dictDelData1[s]((T)sqlItem.GetData1(), (T)sqlItem.GetData2());
                }

            }
            catch(Exception ex)
            {
                Debug.Print("DeleteStack->TwoParamContains" + ex.ToString());
            }
           
        }

        public void SingleParamContains(StackItemSql<object> sqlItem)
        {
            try
            {
                if (dictDelData2.ContainsKey(sqlItem.GetOperation()))
                {
                    int s = sqlItem.GetOperation();
                    dictDelData2[s]((T)sqlItem.GetData1());
                }
                else
                {
                    //Debug.Print("DeleteStack->SingleParamContains: Не критическая ошибка попытка обратиться к несуществующем id" + sqlItem.GetOperation());
                }
            }
            catch(Exception ex)
            {
                Debug.Print("DeleteStack->SingleParamContains" + ex.ToString());
            }
           
        }

        public int RemoveTableslistFilesTemp(T data1)
        {
            try
            {
                string ev = (string)(object)data1;
                sqlDelete.DeleteListFilesTempTable(classcon.GetConnectionSqlite(), ev);
                return (int)SqlCode.RemoveTableslistFilesTemp;
            }
            catch (Exception ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeTableslistFilesTemp() Критическая Ошибка удаления данных", ex.ToString());
                return (int)SqlCode.RemoveTableslistFilesTemp;
            }

        }

        private int DeleteAllRows(T data1)
        {
            string clearTable = (string)(object)data1;
            sqlDelete.DeleteAllRowsTables(classcon.GetConnectionSqlite(), clearTable);

            return (int)SqlCode.DeleteAllRows;
        }

        public int DeleteListFilesTransaction(T data1)
        {
            List<FileInfoModel> list = (List<FileInfoModel>)(object)data1;
            sqlDelete.deleteFileListTransaction(list, classcon.GetConnectionSqlite());

            return (int)SqlCode.DeleteListFilesTransaction;
        }

        public int DeletelistSaveToPc(T data1)
        {
            List<long[][]> allListFull = (List<long[][]>)(object)data1;
            sqlDelete.deleteRowslistSaveToPc_listFiles(allListFull, classcon.GetConnectionSqlite());

            return (int)SqlCode.DeletelistSaveToPc;
        }
        //не нужен параметр
        public int ClearLocation(T data1)
        {
            try
            {
                sqlDelete.DeleteLocation(classcon.GetConnectionSqlite());
                return (int)SqlCode.ClearLocation;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.ClearLocation;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTablesUsers() Критическая Ошибка удаления данных", ex.ToString());
            }

        }
        //не нужен параметр
        public int ClearListScanFolder(T data1)
        {
            try
            {
                sqlDelete.DeleteListScanFolder(classcon.GetConnectionSqlite());
                return (int)SqlCode.ClearListScanFolder;
            }
            catch (Exception ex)
            {

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearListFolder() Критическая Ошибка удаления данных", ex.ToString());
                return (int)SqlCode.ClearListScanFolder;
            }
        }

        //не нужен параметр
        public int  ClearTablesUsers(T data1)
        {
            try
            {
                sqlDelete.DeleteAllRowsTables(classcon.GetConnectionSqlite(), "users");

                return (int)SqlCode.ClearListScanFolder;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.ClearListScanFolder;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTablesUsers() Критическая Ошибка удаления данных", ex.ToString());
            }

        }


        //не нужен параметр
        public int ClearScanFolder(T data1)
        {
            try
            {
                sqlDelete.DeleteScanFolder(classcon.GetConnectionSqlite());

                return (int)SqlCode.ClearScanFolder;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.ClearScanFolder;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearListFolder() Критическая Ошибка удаления данных", ex.ToString());
            }
        }

        //не нужен параметр
        public int ClearTableslistFilesTemp(T data1)
        {
            try
            {
                sqlDelete.DeleteAllRowsTablesTemp(classcon.GetConnectionSqlite(), "listFilesTemp");

                return (int)SqlCode.ClearTableslistFilesTemp;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.ClearTableslistFilesTemp;
                // generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistFilesTemp() Критическая Ошибка удаления данных", ex.ToString());
            }

        }
        public int ClearTableslistFiles(T data1)
        {
            try
            {
                sqlDelete.DeleteAllRowsTables(classcon.GetConnectionSqlite(), "listFiles_users");

                return (int)SqlCode.ClearTableslistFiles;
            }
            catch (Exception ex)
            {

                // generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistFiles() Критическая Ошибка удаления данных", ex.ToString());

                return (int)SqlCode.ClearTableslistFiles;
            }

        }

        //очищает таблицу listPcSaveTemp
        public int ClearTablesPcTemp(T data1)
        {
            try
            {
                sqlDelete.DeleteAllRowsTables(classcon.GetConnectionSqlite(), "listSaveTopPcTemp");
                return (int)SqlCode.ClearTablesPcTemp;
            }
            catch (Exception ex)
            {

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistFiles() Критическая Ошибка удаления данных", ex.ToString());
                return (int)SqlCode.ClearTablesPcTemp;

            }

        }

        //очищает таблицу listFilesTransfer
        public int ClearTableslistTransfer(T data1)
        {
            try
            {
                sqlDelete.DeleteAllRowsTables(classcon.GetConnectionSqlite(), "listFilesTransfer");

                return (int)SqlCode.ClearTableslistTransfer;
            }
            catch (Exception ex)
            {

                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> clearTableslistTransfer() Критическая Ошибка удаления данных", ex.ToString());

                return (int)SqlCode.ClearTableslistTransfer;
            }

        }

  
        

        public int DeletePcSaveRowsLocation(T data1)
        {
            try
            {
                string oldLocation = (string)(object)data1;
                sqlDelete.DeleteSavePcSinglRowsLocation(oldLocation, classcon.GetConnectionSqlite());
                return (int)SqlCode.DeletePcSaveRowsLocation;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.DeletePcSaveRowsLocation;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        public int DeleteSavePcSinglRows(T data1)
        {
            try
            {
                long listFilesRow_id = (long)(object)data1;
                sqlDelete.DeleteSavePcSinglRows(listFilesRow_id, classcon.GetConnectionSqlite());

                return (int)SqlCode.DeleteSavePcSinglRows;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.DeleteSavePcSinglRows;
                // generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
            }

        }

        public int RemoveListFilesTransferSingl(T data1)
        {
            try
            {
                long row_id = (long)(object)data1;
                sqlDelete.DeleteListFilesTansferTableSinglRows(row_id, classcon.GetConnectionSqlite());

                return (int)SqlCode.RemoveListFilesTransferSingl;
            }
            catch (Exception ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
                return (int)SqlCode.RemoveListFilesTransferSingl;
            }

        }

        public int RemoveSinglItemScanFolder(T data1)
        {
            try
            {
                string fullPath = (string)(object)data1;
                sqlDelete.DeleteSinglItemScanFolder(fullPath, classcon.GetConnectionSqlite());
                return (int)SqlCode.RemoveSinglItemScanFolder;
            }
            catch (Exception ex)
            {
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeListFilesTransferSingl() Критическая Ошибка удаления данных", ex.ToString());
                return (int)SqlCode.RemoveSinglItemScanFolder;
            }

        }

        //очищает таблицу listFilesTemp
        public int RemoveTablelistFileTempChangeRows(T data1 , T data2)
        {
            try
            {
                string ev = (string)(object)data1;
                int numberOperation = (int)(object)data2;
                sqlDelete.DeleteListFileTempChangeRows(classcon.GetConnectionSqlite(), ev, numberOperation);
                return (int)SqlCode.RemoveTablelistFileTempChangeRows;
            }
            catch (Exception ex)
            {
                return (int)SqlCode.RemoveTablelistFileTempChangeRows;
                //generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> removeTablelistFileTempChangeRows() Критическая Ошибка удаления данных", ex.ToString());
            }

        }






        private void InitDict1()
        {
            dictDelData1 = new Dictionary<int, odiData2>();
            dictDelData1.Add((int)SqlCode.RemoveTablelistFileTempChangeRows, RemoveTablelistFileTempChangeRows);
        }

        private void InitDict2()
        {
            dictDelData2 = new Dictionary<int, odiData1>();

            dictDelData2.Add((int)SqlCode.RemoveTableslistFilesTemp, RemoveTableslistFilesTemp);
            dictDelData2.Add((int)SqlCode.DeletelistSaveToPc, DeletelistSaveToPc);
            dictDelData2.Add((int)SqlCode.DeleteListFilesTransaction, DeleteListFilesTransaction);
            dictDelData2.Add((int)SqlCode.DeleteAllRows, DeleteAllRows);
            dictDelData2.Add((int)SqlCode.ClearLocation, ClearLocation);
            dictDelData2.Add((int)SqlCode.ClearListScanFolder, ClearListScanFolder);
            dictDelData2.Add((int)SqlCode.ClearScanFolder, ClearScanFolder);
            dictDelData2.Add((int)SqlCode.ClearTableslistFilesTemp, ClearTableslistFilesTemp);
            dictDelData2.Add((int)SqlCode.DeletePcSaveRowsLocation, DeletePcSaveRowsLocation);
            dictDelData2.Add((int)SqlCode.DeleteSavePcSinglRows, DeleteSavePcSinglRows);
            dictDelData2.Add((int)SqlCode.RemoveListFilesTransferSingl, RemoveListFilesTransferSingl);
            dictDelData2.Add((int)SqlCode.ClearTableslistFiles, ClearTableslistFiles);
            dictDelData2.Add((int)SqlCode.ClearTablesPcTemp, ClearTablesPcTemp);
            dictDelData2.Add((int)SqlCode.ClearTableslistTransfer, ClearTableslistTransfer);
            dictDelData2.Add((int)SqlCode.ClearTablesUsers, ClearTablesUsers);

        }

    }
}
