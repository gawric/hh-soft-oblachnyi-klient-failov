﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.core.sqlite.stacksql.support.delete;
using NewHH_SoftClientWPF.contoller.core.sqlite.stacksql.support.insert;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.contoller.util.enumSystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoScanModel;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using NewHH_SoftClientWPF.mvvm.model.systemSettingModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.insertSql.support
{
    public abstract class AbstractSqlStack<T>
    {
        public ConnectedSqllite classcon { get; set; }
        public InsertSqlLite sqlInsert { get; set; }
        public bool isInsert { get; set; }

        private IInsertStack insertStack;
        private IDeleteStack deleteStack;
        public AbstractSqlStack(ConnectedSqllite classcon , InsertSqlLite sqlInsert , DeleteSqllite sqlDelete)
        {
            this.classcon = classcon;
            this.sqlInsert = sqlInsert;
            insertStack = new InsertStack<object>(sqlInsert , classcon);
            deleteStack = new DeleteStack<object>(sqlDelete, classcon);

        }

        public void Worker(StackItemSql<object> sqlItem)
        {
            isInsert = true;

            insertStack.TwoParamContains(sqlItem);
            insertStack.SingleParamContains(sqlItem);

            deleteStack.TwoParamContains(sqlItem);
            deleteStack.SingleParamContains(sqlItem);

            isInsert = false;

        }

       
    }
}
