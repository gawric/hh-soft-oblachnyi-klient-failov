﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.core.sqlite.insertSql.support;
using NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb;
using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.stacksql
{
    public class StackSql: AbstractSqlStack<object> , IStackSql
    {
        //private ConnectedSqllite classcon;
        private BlockingCollection<StackItemSql<object>> dirs;
        public StackSql(ConnectedSqllite classcon)
            : base(classcon, new InsertSqlLite() , new DeleteSqllite())
        {
            this.dirs = new BlockingCollection<StackItemSql<object>>();
            StartStack();
        }
        private void StartStack()
        {
            try
            {

                Task.Run(() =>
                {
                    while (true)
                    {
                        StackItemSql<object> item = dirs.Take();
                        Worker(item);
                    }

                });

            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("StackSql->startStackNetwork: Бесконечный цикл завершен!");
            }
        }

        public void AddItemStack(StackItemSql<object> item )
        {
            dirs.Add(item);
            WaitInsert();
        }

        private void WaitInsert()
        {
            int autoexit = 0;

            while (true)
            {
               // Debug.Print("StackSql:->WaitInsert Ожидание завершения вставки данных " + isInsert);
                if (!isInsert)
                {
                    isInsert = false;
                    Debug.Print("SqlliteInsertController:->WaitInsert Завершили ожидание вставки " + isInsert);
                    break;
                }
                autoexit++;

                if (autoexit > 100)
                {
                    Debug.Print("SqlliteInsertController:->WaitInsert Завершили время вышло!!! " + isInsert);
                    break;
                }

                Thread.Sleep(25);

            }

        }
    }
}
