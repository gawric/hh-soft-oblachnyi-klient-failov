﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.sqlite.updaterowsdb
{
    class UpdateSqllite
    {

        public void FileRows(FileInfoModel modelServer, SQLiteConnection con)
        {
            string updateString = "UPDATE listFiles_users SET row_id=? , filename=? , createDate=? , changeDate=? , lastOpenDate=? , attribute=? , location=? , type=? , sizebyte=? , parent=? , versionUpdateBases=? , versionUpdateRows=? , user_id=? , changeRows=? ,  saveTopc=? WHERE row_id = '" + modelServer.row_id + "';";
            SQLiteCommand command = new SQLiteCommand();

            //все конвертируем в стринг т.к sqlite не умеет хранить long 
            int row_id_server = (int)modelServer.row_id;
            string filename_server = modelServer.filename.ToString();
            string createDate_server = modelServer.createDate.ToString();
            string changeDate_server = modelServer.changeDate.ToString();
            string lastOpenDate_server = modelServer.lastOpenDate.ToString();
            string attribute_server = modelServer.attribute.ToString();
            string location_server = modelServer.location.ToString();
            string type_server = modelServer.type.ToString();
            string sizebyte_server = modelServer.sizebyte.ToString();
            string parent_server = modelServer.parent.ToString();
            string versionUpdateBases_server = modelServer.versionUpdateBases.ToString();
            string versionUpdateRows_server = modelServer.versionUpdateRows.ToString();
            string user_id_server = modelServer.user_id.ToString();
            string changeRows = "OK";
            int saveTopc = modelServer.saveTopc;

            SQLiteParameter p0 = new SQLiteParameter("row_id", SqlDbType.Int);
            SQLiteParameter p1 = new SQLiteParameter("filename", filename_server);
            SQLiteParameter p2 = new SQLiteParameter("createDate", createDate_server);
            SQLiteParameter p3 = new SQLiteParameter("changeDate", changeDate_server);
            SQLiteParameter p4 = new SQLiteParameter("lastOpenDate", lastOpenDate_server);
            SQLiteParameter p5 = new SQLiteParameter("attribute", attribute_server);
            SQLiteParameter p6 = new SQLiteParameter("location", location_server);
            SQLiteParameter p7 = new SQLiteParameter("type", type_server);
            SQLiteParameter p8 = new SQLiteParameter("sizebyte", sizebyte_server);
            SQLiteParameter p9 = new SQLiteParameter("parent", parent_server);
            SQLiteParameter p10 = new SQLiteParameter("versionUpdateBases", versionUpdateBases_server);
            SQLiteParameter p11 = new SQLiteParameter("versionUpdateRows", versionUpdateRows_server);
            SQLiteParameter p12 = new SQLiteParameter("user_id", user_id_server);
            SQLiteParameter p13 = new SQLiteParameter("changeRows", changeRows);
            SQLiteParameter p14 = new SQLiteParameter("saveTopc", saveTopc);
            p0.Value = row_id_server;

            command.CommandText = updateString;
            command.Connection = con;

            command.Parameters.Add(p0);
            command.Parameters.Add(p1);
            command.Parameters.Add(p2);
            command.Parameters.Add(p3);
            command.Parameters.Add(p4);
            command.Parameters.Add(p5);
            command.Parameters.Add(p6);
            command.Parameters.Add(p7);
            command.Parameters.Add(p8);
            command.Parameters.Add(p9);
            command.Parameters.Add(p10);
            command.Parameters.Add(p11);
            command.Parameters.Add(p12);
            command.Parameters.Add(p13);
            command.Parameters.Add(p14);


            command.ExecuteNonQuery();
            command.Dispose();
        }


        public void UpdateTransferProgress(long row_id_transfer , string sizebytes , double progress , string uploadStatus, SQLiteConnection con)
        {
            string updateString = "UPDATE listFilesTransfer SET  progress=? , uploadStatus=? , sizebyte=? WHERE row_id = '" + row_id_transfer + "';";
            SQLiteCommand command = new SQLiteCommand();
            
            SQLiteParameter p0 = new SQLiteParameter("progress", SqlDbType.Real);
            SQLiteParameter p1 = new SQLiteParameter("uploadStatus", uploadStatus);
            SQLiteParameter p2 = new SQLiteParameter("sizebyte", sizebytes);

            p0.Value = progress;

            command.CommandText = updateString;
            command.Connection = con;

            command.Parameters.Add(p0);
            command.Parameters.Add(p1);
            command.Parameters.Add(p2);


            command.ExecuteNonQuery();
            command.Dispose();
        }

        public void updateListSaveToPc(long row_id_listfiles, int intSave , string type ,SQLiteConnection con)
        {
            string updString = "UPDATE listSaveToPc_listFiles SET saveToPc=? , type=? WHERE row_id_listfiles = '" + row_id_listfiles + "';";
            SQLiteCommand command = new SQLiteCommand();

            SQLiteParameter p0 = new SQLiteParameter("saveToPc", SqlDbType.Real);
            SQLiteParameter p1 = new SQLiteParameter("type", type);
        

            p0.Value = intSave;
   

            command.CommandText = updString;
            command.Connection = con;

            command.Parameters.Add(p0);
            command.Parameters.Add(p1);
         


            command.ExecuteNonQuery();
            command.Dispose();
        }


        public void updateSystemSettingAutoStart(string autostartmill, SQLiteConnection con)
        {

            string updString = "UPDATE systemSetting SET autoScan=?;";
            SQLiteCommand command = new SQLiteCommand();       
            SQLiteParameter p1 = new SQLiteParameter("autoScan", autostartmill);
            
            command.CommandText = updString;
            command.Connection = con;
            
            command.Parameters.Add(p1);



            command.ExecuteNonQuery();
            command.Dispose();
        }

        public void updateScanFolderLocation(string location , long row_id, SQLiteConnection con)
        {

            string updString = "UPDATE scanFolder SET location=? WHERE row_id='" + row_id + "';";
            SQLiteCommand command = new SQLiteCommand();
            SQLiteParameter p1 = new SQLiteParameter("location", location);

            command.CommandText = updString;
            command.Connection = con;

            command.Parameters.Add(p1);



            command.ExecuteNonQuery();
            command.Dispose();
        }

        public void updateLocationAndTypeSaveToPc(long row_id_listfiles, int intSave, string type, string location, SQLiteConnection con)
        {
            string updString = "UPDATE listSaveToPc_listFiles SET saveToPc=? , type=? , location=? WHERE row_id_listfiles = '" + row_id_listfiles + "';";
            SQLiteCommand command = new SQLiteCommand();

            SQLiteParameter p0 = new SQLiteParameter("saveToPc", SqlDbType.Real);
            SQLiteParameter p1 = new SQLiteParameter("type", type);
            SQLiteParameter p2 = new SQLiteParameter("location", location);

            p0.Value = intSave;


            command.CommandText = updString;
            command.Connection = con;

            command.Parameters.Add(p0);
            command.Parameters.Add(p1);
            command.Parameters.Add(p2);


            command.ExecuteNonQuery();
            command.Dispose();
        }

        public void updateLocationAndTypeAutoSaveFolder(long row_id_listfiles, int intSave, string type, string location, SQLiteConnection con)
        {
            string updString = "UPDATE listScanFolder_ListFiles SET saveToPc=? , type=? , location=? WHERE row_id_listfiles = '" + row_id_listfiles + "';";
            SQLiteCommand command = new SQLiteCommand();

            SQLiteParameter p0 = new SQLiteParameter("saveToPc", SqlDbType.Real);
            SQLiteParameter p1 = new SQLiteParameter("type", type);
            SQLiteParameter p2 = new SQLiteParameter("location", location);

            p0.Value = intSave;


            command.CommandText = updString;
            command.Connection = con;

            command.Parameters.Add(p0);
            command.Parameters.Add(p1);
            command.Parameters.Add(p2);


            command.ExecuteNonQuery();
            command.Dispose();
        }


        public void updateTypeListFiles(long row_id, string type, SQLiteConnection con)
        {
            string updString = "UPDATE listFiles_users SET type=? WHERE row_id = '" + row_id + "';";
            SQLiteCommand command = new SQLiteCommand();
            SQLiteParameter p0 = new SQLiteParameter("type", type);


            command.CommandText = updString;
            command.Connection = con;

            command.Parameters.Add(p0);

            command.ExecuteNonQuery();
            command.Dispose();
        }

        public void updateRow_idSaveToPc(long listFilesRowid, long row_id , SQLiteConnection con)
        {
            string updString = "UPDATE listSaveToPc_listFiles SET row_id_listfiles=? WHERE row_id = '" + row_id + "';";
            string updParent = "UPDATE listSaveToPc_listFiles SET row_id_listfiles=? WHERE parent = '" + row_id + "';";

            SQLiteCommand command = new SQLiteCommand();
            SQLiteParameter p0 = new SQLiteParameter("row_id_listfiles", SqlDbType.Real);
            p0.Value = listFilesRowid;
            command.CommandText = updString;
            command.Connection = con;
            command.Parameters.Add(p0);


            SQLiteCommand commandParent = new SQLiteCommand();
            SQLiteParameter p0_ = new SQLiteParameter("row_id_listfiles", SqlDbType.Real);
            p0_.Value = listFilesRowid;
            commandParent.CommandText = updParent;
            commandParent.Connection = con;
            commandParent.Parameters.Add(p0);




            command.ExecuteNonQuery();
            command.Dispose();


            commandParent.ExecuteNonQuery();
            commandParent.Dispose();
        }


        public void updateListAllSaveToPc(int intSave, string type, SQLiteConnection con)
        {
            string updString = "UPDATE listSaveToPc_listFiles SET saveToPc=? , type=?';";
            SQLiteCommand command = new SQLiteCommand();

            SQLiteParameter p0 = new SQLiteParameter("saveToPc", SqlDbType.Real);
            SQLiteParameter p1 = new SQLiteParameter("type", type);

            p0.Value = intSave;


            command.CommandText = updString;
            command.Connection = con;

            command.Parameters.Add(p0);
            command.Parameters.Add(p1);


            command.ExecuteNonQuery();
            command.Dispose();
        }






        public void UpdateUserIdListFiles(long user_id, SQLiteConnection con)
        {
            string sql = "UPDATE listFiles_users SET user_id=? WHERE user_id != '" + user_id + "';";

            SQLiteCommand command = new SQLiteCommand();
            SQLiteParameter p0 = new SQLiteParameter("user_id", user_id.ToString());
     

            command.CommandText = sql;
            command.Connection = con;

            command.Parameters.Add(p0);
     


            command.ExecuteNonQuery();
            command.Dispose();
        }


        public void UpdateVersionBases(long newVerSionServer ,SQLiteConnection con)
        {
            try
            {
                string sql = "UPDATE listFiles_users SET versionUpdateBases=?;";

                SQLiteCommand command = new SQLiteCommand();
                SQLiteParameter p0 = new SQLiteParameter("versionUpdateBases", newVerSionServer.ToString());


                command.CommandText = sql;
                command.Connection = con;

                command.Parameters.Add(p0);



                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (System.Data.SQLite.SQLiteException s)
            {
                Debug.Print("UpdateSqllite->UpdateVersionBases: критическая ошибка " + s.ToString());
            }
           
        }
    }
}
