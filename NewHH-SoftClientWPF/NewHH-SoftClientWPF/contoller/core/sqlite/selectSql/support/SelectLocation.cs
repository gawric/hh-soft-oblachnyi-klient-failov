﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.contoller.sqlite.connectdb;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
   public  class SelectLocation
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
 

       public SelectLocation(ExceptionController error , ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }
        //проверяет существует ли пользователь и корректно введен пароль
        public FileInfoModel getSearchNodesByLocation(string location)
        {
            try
            {
                return getSearchByLocation(location, classcon.GetConnectionSqlite());
            }
            catch (SqlException ex)
            {
                generatedError((int)CodeError.DATABASE_ERROR, "SqliteController -> getSearchNodesByLocation() Критическая Ошибка выборки данных", ex.ToString());

                return null;
            }

        }



       


        //List<> получаем все возможные записи по parentID
        public FileInfoModel getSearchByLocation(string location, SQLiteConnection con)
        {

            FileInfoModel clientModel = null;

            string stm = "SELECT * FROM listFiles_users WHERE location = '" + location + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {


                            clientModel = new FileInfoModel();
                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.filename = rdr["filename"].ToString();
                            clientModel.createDate = rdr["createDate"].ToString();
                            clientModel.changeDate = rdr["changeDate"].ToString();
                            clientModel.lastOpenDate = rdr["lastOpenDate"].ToString();
                            clientModel.attribute = rdr["attribute"].ToString();
                            clientModel.location = rdr["location"].ToString();
                            clientModel.type = rdr["type"].ToString();
                            clientModel.sizebyte = long.Parse(rdr["sizebyte"].ToString());
                            clientModel.parent = long.Parse(rdr["parent"].ToString());
                            clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                            clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                            clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                            clientModel.changeRows = rdr["changeRows"].ToString();
                            clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);


                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return clientModel;
        }

        public TempFilesModel getSearchByLocationToTempFilesModel(string location, SQLiteConnection con)
        {

            TempFilesModel clientModel = null;

            string stm = "SELECT row_id,type,location FROM listFiles_users WHERE location = '" + location + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {
                            clientModel = new TempFilesModel();
                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.type = rdr["type"].ToString();
                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return clientModel;
        }


        private BaseException generatedError(int codeError, string textError, string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError);
        }


     

    }
}
