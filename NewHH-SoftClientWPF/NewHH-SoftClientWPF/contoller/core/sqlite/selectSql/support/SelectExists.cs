﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
    public class SelectExists
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
        public SelectExists(ExceptionController error , ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }


        //есть ли такая запись в таблице listFiles -> Row_id
        public bool existsListFilesRow_id(long Row_id, SQLiteConnection con)
        {

            bool check = false;

            string stm = "SELECT row_id FROM listFiles_users WHERE row_id='" + Row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {

                            check = true;

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }


        //получает user_id в текущей базе клиента
        public long existUserIdListFiles(SQLiteConnection con)
        {
            long user_id = 0;

            string stm = "SELECT user_id FROM listFiles_users LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {

                            user_id = long.Parse(rdr["user_id"].ToString());
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return user_id;
        }

        //bool - true база данных пустая
        public bool existsEmptyTables(SQLiteConnection con)
        {
            bool check = true;

            string stm = "SELECT row_id FROM listFiles_users LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        if (rdr.StepCount > 0)
                        {
                            check = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }

        //bool<> получаем флаг есть ли у данного row_id дети
        public bool existSqlLiteNodeChildren(long Row_id, SQLiteConnection con)
        {

            bool check = false;

            string stm = "SELECT parent FROM listFiles_users WHERE parent='" + Row_id + "' LIMIT 1;";

            try
            {
                SQLiteCommand cmd = new SQLiteCommand(stm, con);
                SQLiteDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    check = true;
                }


                cmd.Dispose();
                rdr.Close();

            }
            catch (Exception)
            {

                throw;
            }


            return check;


        }

        public bool isExistsListTransferToRow_id(long row_id, SQLiteConnection con)
        {
            bool check = false;

            string stm = "SELECT row_id FROM listFilesTransfer WHERE row_id='" + row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            check = true;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }

        public bool existSystemSetting(SQLiteConnection con)
        {
            bool check = false;

            string stm = "SELECT * FROM systemSetting;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            check = true;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }

        //bool  true нашли
        //bool false не нашли
        //existRow_id - предназначена для друкого
        public bool existSinglRow_id(long row_id, SQLiteConnection con)
        {
            bool check = false;

            string stm = "SELECT row_id FROM listFiles_users WHERE row_id='" + row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            check = true;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }

        //true пустая
        public bool existClearListScanFolder(SQLiteConnection con)
        {
         

            string stm = "SELECT COUNT(*) FROM listScanFolder_ListFiles;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            string count = rdr[0].ToString();
                            if (count == null) return false;
                            if (Convert.ToInt32(count) == 0) return true;
                            return false;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return true; 
        }

        //bool   > База данных явялется самой новой или нет
        public bool existRow_id(long row_id, SQLiteConnection con)
        {
            bool check = false;

            string stm = "SELECT row_id FROM listFiles_users LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //если результат пустой значит наша база данных чистая 
                        //перезаписываем ее
                        if (rdr.StepCount == 0)
                        {
                            check = true;
                        }
                        else
                        {

                            while (rdr.Read())
                            {
                                check = true;
                            }


                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }

        //bool   > База данных явялется самой новой или нет
        public bool existVersionUpdateBases(long lastVersionBases, SQLiteConnection con)
        {
            bool check = false;

            string stm = "SELECT versionUpdateBases FROM listFiles_users LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //если результат пустой значит наша база данных чистая 
                        //перезаписываем ее
                        if (rdr.StepCount == 0)
                        {
                            check = true;
                        }
                        else
                        {

                            while (rdr.Read())
                            {

                                long currentVersionBases = long.Parse(rdr["versionUpdateBases"].ToString());
                                //если база полученная от сервера больше чем текущая, то мы обновляем всю базу данных
                                if (lastVersionBases > currentVersionBases)
                                {
                                    check = true;
                                }

                            }


                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return check;
        }


        //bool[0]   > пользователь не найден
        //bool[1]   > не верный пароль
        public bool[] existUsernameAndCheckPassword(string username, string password, SQLiteConnection con)
        {
            bool[] array = new bool[2];

            string stm = "SELECT * FROM users WHERE username='" + username + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            string serverUsername = (string)rdr["username"];
                            string serverPassword = (string)rdr["password"];

                            array[0] = true;

                            if (serverPassword.Equals(password))
                            {
                                array[1] = true;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return array;
        }





    }
}
