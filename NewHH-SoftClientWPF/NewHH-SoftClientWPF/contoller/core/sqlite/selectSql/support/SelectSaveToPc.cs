﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using NewHH_SoftClientWPF.mvvm.model.saveToPc;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
    public class SelectSaveToPc
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
        public SelectSaveToPc(ExceptionController error, ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }

        public List<long> getAllSaveToPcListFilesId(SQLiteConnection con)
        {

            List<long> listSaveToPc = new List<long>();

            string stm = "SELECT row_id_listfiles FROM listSaveToPc_listFiles;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                listSaveToPc.Add(Convert.ToInt32(rdr["row_id_listfiles"]));
                            }

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return listSaveToPc;
        }

        public Dictionary<string, long> getAllSaveToPcListFilesLocation(SQLiteConnection con)
        {

            Dictionary<string, long> listSaveToPc = new Dictionary<string, long>();

            string stm = "SELECT location , row_id FROM listSaveToPc_listFiles;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                string location = rdr["location"].ToString();
                                long row_id = Convert.ToInt32(rdr["row_id"]);

                                if (!listSaveToPc.ContainsKey(location))
                                {
                                    listSaveToPc.Add(location, row_id);
                                }

                            }

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return listSaveToPc;
        }

        public Dictionary<long, long> getAllSaveToPcListFilesRow_id(SQLiteConnection con)
        {

            Dictionary<long, long> listRow_idSaveToPc = new Dictionary<long, long>();

            string stm = "SELECT row_id_listfiles FROM listSaveToPc_listFiles;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                long listFilesRow_id = Convert.ToInt32(rdr["row_id_listfiles"]);
                                if (!listRow_idSaveToPc.ContainsKey(listFilesRow_id)) listRow_idSaveToPc.Add(listFilesRow_id, listFilesRow_id);

                            }

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return listRow_idSaveToPc;
        }

        public Dictionary<long, long> getAllAutoSaveListFilesRow_id(SQLiteConnection con)
        {

            Dictionary<long, long> listRow_idSaveToPc = new Dictionary<long, long>();

            string stm = "SELECT row_id_listfiles FROM listScanFolder_ListFiles;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                long listFilesRow_id = Convert.ToInt32(rdr["row_id_listfiles"]);
                                if (!listRow_idSaveToPc.ContainsKey(listFilesRow_id)) listRow_idSaveToPc.Add(listFilesRow_id, listFilesRow_id);

                            }

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return listRow_idSaveToPc;
        }



        

       

        public long getSqlLiteSavetoPcRoid(string row_location, SQLiteConnection con)
        {

            long originalRowId = 0;

            string stm = "SELECT row_id_listfiles FROM listSaveToPc_listFiles WHERE location='" + row_location + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                originalRowId = Convert.ToInt32(rdr["row_id_listfiles"]);

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return originalRowId;
        }


        public string getSqlLiteSavetoPcType(string row_location, SQLiteConnection con)
        {

            string originalType = "";

            string stm = "SELECT type FROM listSaveToPc_listFiles WHERE location='" + row_location + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                originalType = rdr["type"].ToString();

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return originalType;
        }


        public string getSqlLiteSavetoPcType(long row_id_listfiles, SQLiteConnection con)
        {

            string originalType = "";

            string stm = "SELECT row_id_listfiles , type FROM listSaveToPc_listFiles WHERE row_id_listfiles='" + row_id_listfiles + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                originalType = rdr["type"].ToString();

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return originalType;
        }

        public string getSqlLiteAutoSaveFolder(long row_id_listfiles, SQLiteConnection con)
        {

            string originalType = "";

            string stm = "SELECT row_id_listfiles , type FROM listScanFolder_ListFiles WHERE row_id_listfiles='" + row_id_listfiles + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                originalType = rdr["type"].ToString();

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return originalType;
        }


        public List<SaveToPcModel> getSqlLiteParentIDSavePcMinimal(long ParentId, SQLiteConnection con)
        {

            List<SaveToPcModel> pcList = new List<SaveToPcModel>();

            string stm = "SELECT row_id_listfiles , saveToPc , parent , type FROM listSaveToPc_listFiles WHERE parent='" + ParentId + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                SaveToPcModel clientModel = new SaveToPcModel();

                                clientModel.row_id_listfiles = Convert.ToInt32(rdr["row_id_listfiles"]);
                                clientModel.saveToPc = Convert.ToInt32(rdr["saveToPc"]);
                                clientModel.type = rdr["type"].ToString();

                                pcList.Add(clientModel);

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return pcList;
        }

        public List<AutoSaveFolderModel> getSqlLiteParentIDAutoSaveFolderMinimal(long parentId, SQLiteConnection con)
        {

            List<AutoSaveFolderModel> pcList = new List<AutoSaveFolderModel>();

            string stm = "SELECT row_id_listfiles , saveToPc , parent , type FROM listScanFolder_ListFiles WHERE parent='" + parentId + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                AutoSaveFolderModel clientModel = new AutoSaveFolderModel();

                                var row_id_listfiles = rdr["row_id_listfiles"];
                                string strpc = rdr["saveToPc"].ToString();
                                //Рабочий но не очень красивый!
                               // if(strpc.Equals("")) return pcList;
                               // if(row_id_listfiles == null || strpc == null) return pcList;

                               if(row_id_listfiles == null || String.IsNullOrEmpty(strpc)) return pcList;

                               //Debug.Print("SelectSaveTopc->getSqlLiteParentIDAutoSaveFolderMinimal: saveToPc: " + strpc);
                                //Debug.Print("SelectSaveTopc->getSqlLiteParentIDAutoSaveFolderMinimal: saveToPc: " + row_id_listfiles);
                                var saveToPc = Convert.ToInt32(strpc);
                                clientModel.row_id_listfiles = Convert.ToInt32(row_id_listfiles);
                                clientModel.saveToPc = saveToPc;
                                clientModel.type = rdr["type"].ToString();

                                pcList.Add(clientModel);

                            }

                        }


                    }
                }
            }
            catch (Exception)
            {
                throw;
            }


            return pcList;
        }

        //List<> получаем все возможные записи по parentID
        public List<FileInfoModel> getSqlLiteRootFileInfoModelList(long ParentId, SQLiteConnection con)
        {

            List<FileInfoModel> fileList = new List<FileInfoModel>();

            string stm = "SELECT * FROM listFiles_users WHERE parent='" + ParentId + "' ORDER BY filename;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                if (string.IsNullOrEmpty(rdr["row_id"].ToString()) != true)
                                {
                                    FileInfoModel clientModel = new FileInfoModel();

                                    clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                                    clientModel.filename = rdr["filename"].ToString();
                                    clientModel.createDate = rdr["createDate"].ToString();
                                    clientModel.changeDate = rdr["changeDate"].ToString();
                                    clientModel.lastOpenDate = rdr["lastOpenDate"].ToString();
                                    clientModel.attribute = rdr["attribute"].ToString();
                                    clientModel.location = rdr["location"].ToString();
                                    clientModel.type = rdr["type"].ToString();
                                    clientModel.sizebyte = long.Parse(rdr["sizebyte"].ToString());
                                    clientModel.parent = long.Parse(rdr["parent"].ToString());
                                    clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                                    clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                                    clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                                    clientModel.changeRows = rdr["changeRows"].ToString();

                                    if (string.IsNullOrEmpty(rdr["saveTopc"].ToString()) != true)
                                    {
                                        clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);
                                    }
                                    else
                                    {
                                        //0 - не хранится на сервере
                                        clientModel.saveTopc = Convert.ToInt32(0);
                                    }
                                    fileList.Add(clientModel);
                                }
                                else
                                {
                                    Console.WriteLine("SelectSqlite-> getSqlLiteRootFileInfoModelList-> получили Null");
                                }


                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }







    }
}
