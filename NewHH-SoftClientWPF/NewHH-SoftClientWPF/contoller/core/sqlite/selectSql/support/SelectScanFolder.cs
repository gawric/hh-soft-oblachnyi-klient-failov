﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model.settingViewModel;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
    public class SelectScanFolder
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;

        public SelectScanFolder(ExceptionController error, ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }


        public List<ScanFolderModel> getAllScanFolder( SQLiteConnection con)
        {

            List<ScanFolderModel> clientList = new List<ScanFolderModel>();

            string stm = "SELECT * FROM scanFolder;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {
                            ScanFolderModel clientModel = new ScanFolderModel();
                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.fullPath = rdr["fullPath"].ToString();

                            if(rdr["location"] != null)
                            {
                                clientModel.location = rdr["location"].ToString();
                                clientList.Add(clientModel);
                            }
                            
                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return clientList;
        }

        private BaseException generatedError(int codeError, string textError, string trhowError)
        {
            error.sendError((int)CodeError.DATABASE_ERROR, textError, trhowError);
            return new BaseException((int)CodeError.DATABASE_ERROR, textError, trhowError);
        }

    }
}
