﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support
{
    public class SelectFileInfo_Users
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
        static object locker = new object();
        public SelectFileInfo_Users(ExceptionController error, ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }

        //long id - текущий версия базы данных
        public long getVersionBasesClient(SQLiteConnection con)
        {

            long versionClientBases = -1;

            string stm = "SELECT versionUpdateBases FROM listFiles_users LIMIT 1;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {
                            versionClientBases = long.Parse(rdr["versionUpdateBases"].ToString());
                        }



                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return versionClientBases;
        }


        public Dictionary<long, long> getAllRow_id_long(SQLiteConnection con)
        {

            Dictionary<long, long> array = new Dictionary<long, long>();

            string stm = "SELECT row_id,location FROM listFiles_users;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            long row_id_sqllite = Convert.ToInt32(rdr["row_id"]);
                            //string location = rdr["location"].ToString();

                            array.Add(row_id_sqllite, row_id_sqllite);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return array;
        }

        public Dictionary<string, long> getAllHref(SQLiteConnection con, Dictionary<long, string> listDoubleHref)
        {

            Dictionary<string, long> array = new Dictionary<string, long>();
            int index = 0;
            string stm = "SELECT row_id,location FROM listFiles_users;";

            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            long row_id_sqllite = Convert.ToInt32(rdr["row_id"]);
                            string location = rdr["location"].ToString();

                            if (array.ContainsKey(location))
                            {
                                Console.WriteLine("SelectSqllite->getAllHref: Ошибка Такой LocationID найден в базе Будем ее удалять! Отправили в массив для удаления на сервере");
                                listDoubleHref.Add(row_id_sqllite, location);
                                array.Add(location + index, row_id_sqllite);
                            }
                            else
                            {
                                array.Add(location, row_id_sqllite);
                            }
                            index++;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return array;
        }

        public long getSizeBytesToRow_id(SQLiteConnection con, long row_id)
        {
            lock (locker)
            {
                long sizebyte = 0;

                string stm = "SELECT row_id,sizebyte FROM listFiles_users WHERE row_id='" + row_id + "';";
                try
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                    {
                        using (SQLiteDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {

                                sizebyte = Convert.ToInt64(rdr["sizebyte"].ToString());

                            }
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }

                return sizebyte;
            }

        }

        public FileInfoModel getFileInfoSearchVersionUpdateRows(long Row_id, long updateVersionRows, SQLiteConnection con)
        {

            FileInfoModel clientModel = null;

            string stm = "SELECT * FROM listFiles_users WHERE row_id='" + Row_id + "';";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {

                            clientModel = new FileInfoModel();
                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.filename = rdr["filename"].ToString();
                            clientModel.createDate = rdr["createDate"].ToString();
                            clientModel.changeDate = rdr["changeDate"].ToString();
                            clientModel.lastOpenDate = rdr["lastOpenDate"].ToString();
                            clientModel.attribute = rdr["attribute"].ToString();
                            clientModel.location = rdr["location"].ToString();
                            clientModel.type = rdr["type"].ToString();
                            clientModel.sizebyte = long.Parse(rdr["sizebyte"].ToString());
                            clientModel.parent = long.Parse(rdr["parent"].ToString());
                            clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                            clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                            clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                            clientModel.changeRows = rdr["changeRows"].ToString();

                            if (string.IsNullOrEmpty(rdr["saveTopc"].ToString()) != true)
                            {
                                clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);
                            }
                            else
                            {
                                //0 - не хранится на сервере
                                clientModel.saveTopc = Convert.ToInt32(0);
                            }


                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return clientModel;
        }


        public long getVersionRows(SQLiteConnection con)
        {
            long numberOperation = -1;

            string stm = "SELECT MAX(CAST(versionUpdateRows as int)) FROM listFiles_users;";

            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            try
                            {
                                if (rdr.GetValue(0) != DBNull.Value)
                                {
                                    // numberOperation = Convert.ToInt32(rdr["numberOperation"].ToString()); 
                                    numberOperation = long.Parse(rdr.GetValue(0).ToString());
                                }

                            }
                            catch (System.IndexOutOfRangeException)
                            {
                                throw;
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }


            return numberOperation;
        }


        public long getSizeListFiles(SQLiteConnection con)
        {
            long allSize = 0;

            string stm = "SELECT sizebyte FROM listFiles_users;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            long size = long.Parse(rdr["sizebyte"].ToString());
                            allSize = allSize + size;
                        }

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return allSize;
        }


        //List<> получаем все возможные записи по parentID
        public List<FileInfoModel> getSqlLiteRootFileInfoModelListMINIMAL(long ParentId, SQLiteConnection con)
        {

            List<FileInfoModel> fileList = new List<FileInfoModel>();

            string stm = "SELECT row_id , filename , location , type , parent , versionUpdateBases , saveTopc , versionUpdateRows FROM listFiles_users WHERE parent='" + ParentId + "' ORDER BY filename;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        //проверка на nul
                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {

                                if (string.IsNullOrEmpty(rdr["row_id"].ToString()) != true)
                                {
                                    FileInfoModel clientModel = new FileInfoModel();

                                    clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                                    clientModel.filename = rdr["filename"].ToString();
                                    clientModel.location = rdr["location"].ToString();
                                    clientModel.type = rdr["type"].ToString();
                                    clientModel.parent = long.Parse(rdr["parent"].ToString());
                                    clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                                    clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                                    if (string.IsNullOrEmpty(rdr["saveTopc"].ToString()) != true)
                                    {
                                        clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);
                                    }
                                    else
                                    {
                                        //0 - не хранится на сервере
                                        clientModel.saveTopc = Convert.ToInt32(0);
                                    }
                                    fileList.Add(clientModel);
                                }
                                else
                                {
                                    Console.WriteLine("SelectSqlite-> getSqlLiteRootFileInfoModelList-> получили Null");
                                }


                            }

                        }


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }


        //List<> получаем только id номера и статус > папка или файл (тонкая выборка экономит память клиента)
        public List<long[]> getSqlLiteParentIdLong(long ParentId, SQLiteConnection con)
        {
            //0 - id
            //1 - type
            List<long[]> fileList = new List<long[]>();

            string stm = "SELECT row_id,type FROM listFiles_users WHERE parent='" + ParentId + "';";

            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {



                            long row_id = Convert.ToInt32(rdr["row_id"]);
                            long type = 0;

                            string type_str = rdr["type"].ToString();

                            if (staticVariable.Variable.isFolder(type_str))
                            {
                                type = 1;
                            }
                            else
                            {
                                type = 0;
                            }

                            long[] arr = { row_id, type };

                            fileList.Add(arr);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }


        //List<> получаем все возможные записи по parentID
        public List<FileInfoModel> getSqlLiteNewRootFileInfoModelList(long ParentId, SQLiteConnection con)
        {

            List<FileInfoModel> fileList = new List<FileInfoModel>();

            string stm = "SELECT * FROM listFiles_users WHERE parent <= '" + ParentId + "' ORDER BY filename;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {

                            FileInfoModel clientModel = new FileInfoModel();

                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.filename = rdr["filename"].ToString();
                            clientModel.createDate = rdr["createDate"].ToString();
                            clientModel.changeDate = rdr["changeDate"].ToString();
                            clientModel.lastOpenDate = rdr["lastOpenDate"].ToString();
                            clientModel.attribute = rdr["attribute"].ToString();
                            clientModel.location = rdr["location"].ToString();
                            clientModel.type = rdr["type"].ToString();
                            clientModel.sizebyte = long.Parse(rdr["sizebyte"].ToString());
                            clientModel.parent = long.Parse(rdr["parent"].ToString());
                            clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                            clientModel.versionUpdateRows = long.Parse(rdr["versionUpdateRows"].ToString());
                            clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                            clientModel.changeRows = rdr["changeRows"].ToString();
                            clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);
                            fileList.Add(clientModel);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }

        public List<FileInfoModel> getSqlLiteNewRootFileInfoModelListFilterToFOlderMINIMAL(long ParentId, SQLiteConnection con)
        {

            List<FileInfoModel> fileList = new List<FileInfoModel>();

            string stm = "SELECT row_id,location,type,filename,parent,versionUpdateBases,user_id,saveTopc   FROM listFiles_users WHERE parent <= '" + ParentId + "' AND type = 'folder' ORDER BY filename;";
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {


                        while (rdr.Read())
                        {

                            FileInfoModel clientModel = new FileInfoModel();

                            clientModel.row_id = Convert.ToInt32(rdr["row_id"]);
                            clientModel.location = rdr["location"].ToString();
                            clientModel.type = rdr["type"].ToString();
                            clientModel.filename = rdr["filename"].ToString();
                            clientModel.parent = long.Parse(rdr["parent"].ToString());
                            clientModel.versionUpdateBases = long.Parse(rdr["versionUpdateBases"].ToString());
                            clientModel.user_id = long.Parse(rdr["user_id"].ToString());
                            clientModel.saveTopc = Convert.ToInt32(rdr["saveTopc"]);

                            fileList.Add(clientModel);

                        }

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return fileList;
        }







    }
}
