﻿using NewHH_SoftClientWPF.contoller.core.sqlite.connSql;
using NewHH_SoftClientWPF.controller.error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.selectSql.support.autoScanInfo
{
    public class SelectAutoScanInfo
    {
        private ExceptionController error;
        private ConnectedSqllite classcon;
        static object locker = new object();
        public SelectAutoScanInfo(ExceptionController error, ConnectedSqllite classcon)
        {
            this.error = error;
            this.classcon = classcon;
        }
    }
}
