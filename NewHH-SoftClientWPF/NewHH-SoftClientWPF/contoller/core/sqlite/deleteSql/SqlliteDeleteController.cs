﻿using NewHH_SoftClientWPF.contoller.core.sqlite.stacksql;
using NewHH_SoftClientWPF.contoller.util.enumSystem;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.sqlModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.deleteSql
{
    public class SqlliteDeleteController: ISqlDelete
    {
        private StackSql stackSql;

        public SqlliteDeleteController(StackSql stackSql)
        {
            this.stackSql = stackSql;
        }

        public void RemoveTableslistFilesTemp(string ev)
        {
            stackSql.AddItemStack(new StackItemSql<object>(ev, (int)SqlCode.RemoveTableslistFilesTemp));
        }

        
        public void DeleteAllRows(string clearTable)
        {
            stackSql.AddItemStack(new StackItemSql<object>(clearTable, (int)SqlCode.DeleteAllRows));
        }

        public void DeleteListFilesTransaction(List<FileInfoModel> list)
        {
            stackSql.AddItemStack(new StackItemSql<object>(list, (int)SqlCode.DeleteListFilesTransaction));
        }

        public void DeletelistSaveToPc(List<long[][]> allListFull)
        {
            stackSql.AddItemStack(new StackItemSql<object>(allListFull, (int)SqlCode.DeletelistSaveToPc));
        }
        //не нужен параметр
        public void ClearLocation()
        {
            stackSql.AddItemStack(new StackItemSql<object>("clear", (int)SqlCode.ClearLocation));

        }
        //не нужен параметр
        public void ClearListScanFolder()
        {
            stackSql.AddItemStack(new StackItemSql<object>("clear", (int)SqlCode.ClearListScanFolder));
        }

        //не нужен параметр
        public void ClearScanFolder()
        {
        
            stackSql.AddItemStack(new StackItemSql<object>("clear", (int)SqlCode.ClearScanFolder));
        }

        public void ClearTablesPcTemp()
        {
            stackSql.AddItemStack(new StackItemSql<object>("listSaveTopPcTemp", (int)SqlCode.ClearTablesPcTemp));
        }

        //не нужен параметр
        public void ClearTableslistFilesTemp()
        {
            stackSql.AddItemStack(new StackItemSql<object>("clear", (int)SqlCode.ClearTableslistFilesTemp));
        }

        public void ClearTableslistTransfer()
        {
            stackSql.AddItemStack(new StackItemSql<object>("listFilesTransfer", (int)SqlCode.ClearTableslistTransfer));
        }

        public void DeletePcSaveRowsLocation(string oldLocation)
        {
            stackSql.AddItemStack(new StackItemSql<object>(oldLocation, (int)SqlCode.DeletePcSaveRowsLocation));
        }

        public void DeleteSavePcSinglRows(long listFilesRow_id)
        {
            stackSql.AddItemStack(new StackItemSql<object>(listFilesRow_id, (int)SqlCode.DeleteSavePcSinglRows));
        }

        public void RemoveListFilesTransferSingl(long row_id)
        {
            stackSql.AddItemStack(new StackItemSql<object>(row_id, (int)SqlCode.RemoveListFilesTransferSingl));
        }

        public void RemoveSinglItemScanFolder(string fullPath)
        {
            stackSql.AddItemStack(new StackItemSql<object>(fullPath, (int)SqlCode.RemoveSinglItemScanFolder));
        }

        //очищает таблицу listFilesTemp
        public void RemoveTablelistFileTempChangeRows(string ev, int numberOperation)
        {
            stackSql.AddItemStack(new StackItemSql<object>(ev, numberOperation, (int)SqlCode.RemoveTablelistFileTempChangeRows));
        }

        public void ClearTableslistFiles()
        {
            stackSql.AddItemStack(new StackItemSql<object>("listFiles_users", (int)SqlCode.ClearTableslistFiles));
        }

        public void ClearTablesUsers()
        {
            stackSql.AddItemStack(new StackItemSql<object>("users", (int)SqlCode.ClearTablesUsers));
        }


    }
}
