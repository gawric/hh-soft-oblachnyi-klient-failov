﻿using NewHH_SoftClientWPF.mvvm.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.sqlite.deleteSql
{
    public interface ISqlDelete
    {
         void RemoveTableslistFilesTemp(string ev);
         void DeleteAllRows(string clearTable);
         void DeleteListFilesTransaction(List<FileInfoModel> list);
         void DeletelistSaveToPc(List<long[][]> allListFull);
         void ClearLocation();
         void ClearListScanFolder();
         void ClearScanFolder();
         void ClearTableslistFilesTemp();
         void DeletePcSaveRowsLocation(string oldLocation);
         void DeleteSavePcSinglRows(long listFilesRow_id);
         void RemoveListFilesTransferSingl(long row_id);
         void RemoveSinglItemScanFolder(string fullPath);
         void RemoveTablelistFileTempChangeRows(string ev, int numberOperation);
         void ClearTableslistFiles();
         void ClearTablesPcTemp();
         void ClearTableslistTransfer();
         void ClearTablesUsers();

    }
}
