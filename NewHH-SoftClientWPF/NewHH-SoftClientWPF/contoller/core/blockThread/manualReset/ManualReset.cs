﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.blockThread.manualReset
{
    public class ManualReset
    {
        private ManualResetEvent mre = new ManualResetEvent(false);

        public ManualReset()
        {

        }

        public void setMre()
        {
            mre.Set();
        }

        public void resetMre()
        {
            mre.Reset();
        }

        public ManualResetEvent getManualResetEvent()
        {
            return mre;
        }

        public void WaitOne()
        {
            mre.WaitOne();
        }
    }
}
