﻿using NewHH_SoftClientWPF.contoller.blockThread.manualReset;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.blockThread
{
    public class BlockingEventController
    {
        Dictionary<string, ManualReset> listManualReset = new Dictionary<string, ManualReset>();

        public BlockingEventController(Container cont)
        {

        }
        public void createManualResetEvent(string nameId)
        {
            if(isExistsNameId(nameId))
            {
                listManualReset[nameId] =  new ManualReset();

            }
            else
            {
                listManualReset.Add(nameId, new ManualReset());
            }
        }

        public ManualReset getManualReset(string nameId)
        {
            if (isExistsNameId(nameId))
            {
                return listManualReset[nameId];
            }
            else
            {
                Console.WriteLine("BlockingEventController->getManualReset: ошика ManualReset не найден в библиотеке" + nameId);
                return new ManualReset();
            }
        }

        public void setMRE(string nameId)
        {
            if (isExistsNameId(nameId))
                listManualReset[nameId].setMre();
        }

        public void resetMRE(string nameId)
        {
            if (isExistsNameId(nameId))
                listManualReset[nameId].resetMre();
        }

        public void waitOneMRE(string nameId)
        {
            if (isExistsNameId(nameId))
                listManualReset[nameId].WaitOne();
        }

        private bool isExistsNameId(string nameId)
        {
            if (listManualReset.ContainsKey(nameId))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
    }
}
