﻿using Apache.NMS;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.network.mqclient;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.sync;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using SimpleInjector;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.mqserver
{
    public class MqClientController
    {
        private MqClient _mqClient;
        private MqBroker _mqBroker;
        private UpdateNetworkController _updateNetworkController;
        private UpdateFormMainController _updateMainForm;
        private UpdateFormLoginController _updateLoginForm;
        private TransportStatusMqClient _statucMqClient;
        private CancellationController _cancellationController;
        private IProgress<bool> progress;
        private bool runningConnect = false;

        //статус была ли проведена первая синхронизация данных
        private bool firstSync = false;
  

        public MqClientController(Container cont)
        {
            _updateNetworkController = cont.GetInstance<UpdateNetworkController>();
            _updateMainForm = cont.GetInstance<UpdateFormMainController>();
            _updateLoginForm = cont.GetInstance<UpdateFormLoginController>();
            _statucMqClient = cont.GetInstance<TransportStatusMqClient>();
            _cancellationController = cont.GetInstance<CancellationController>();
            ExceptionController exceptionController = cont.GetInstance<ExceptionController>();

            createMqCLient(ref _mqClient, exceptionController, ref _mqBroker);
            Console.WriteLine("MqClientController running :-> Запуск клиента mqclient");
            progress = new Progress<bool>(ReportProgress);


        }

     

        public async Task<bool> CreateNewConnectMqClientAsync(string username, string password , FirstSyncFilesController firstSync)
        {
            //начинается подкл.
            progress.Report(true);
            try
            {
                //выдает ошибку если домен не доступен(проверка связи)
                IPHostEntry host = Dns.GetHostEntry(staticVariable.Variable.getHostExist());

                ActiveMqSubscribleModel activeMqSubscrible = new ActiveMqSubscribleModel();


                await CreateNewConnectedPool1(username, password, activeMqSubscrible);
                await CreateNewConnectedPool2(username, password, activeMqSubscrible);



               return StartRunningConnected(ref _mqClient, ref activeMqSubscrible, firstSync);


            }
            catch (Apache.NMS.NMSSecurityException ex)
            {
                Console.WriteLine("MqClientController-> CreateNewConnectMqClientAsync: " + ex.ToString());
                updateConnectionStatuErrorAuth();

                return false;
            }
            catch (Apache.NMS.NMSConnectionException con)
            {
                Console.WriteLine("MqClientController-> CreateNewConnectMqClientAsync: " + con.ToString());
                updateConnectionStatusNO();
                return false;
            }
            catch (System.Net.Sockets.SocketException host)
            {
                Console.WriteLine("MqClientController-> CreateNewConnectMqClientAsync: " + host.ToString());
                updateConnectionStatusNO();

                return false;
            }

        }

        private async Task CreateNewConnectedPool2(string username , string password , ActiveMqSubscribleModel activeMqSubscrible)
        {
            await _mqClient.CreateNewConnectMqCLient(username, password);

            if (_mqClient.getClient(0) != null)
            {
                // _mqBroker.StartAsyncListener(username, _mqClient.getClient(), _updateNetworkController);
                _mqBroker.StartAsyncLazyListener(username, _mqClient.getClient(0), _updateNetworkController, activeMqSubscrible);
            }

        }

        private async Task CreateNewConnectedPool1(string username, string password, ActiveMqSubscribleModel activeMqSubscrible)
        {
            await _mqClient.CreateNewConnectMqCLient(username, password);

            if (_mqClient.getClient(0) != null)
            {
                _mqBroker.StartAsyncListener(username, _mqClient.getClient(0), _updateNetworkController, activeMqSubscrible);
 
            }
        }
        private bool StartRunningConnected(ref MqClient _mqClient , ref ActiveMqSubscribleModel activeMqSubscrible , FirstSyncFilesController firstSync)
        {
            if (_mqClient.getClient(0) != null)
            {

                updateConnectionStatusOK();
                _updateLoginForm.updateCloseLoginForm();
                progress.Report(false);

                startRunningFirstSync(activeMqSubscrible, firstSync);
                return true;
            }
            else
            {
                Console.WriteLine("MqClientColtroller->CreateNewConnectMqClientAsync: Подключение создать не удалось!!!");
                return false;
            }
        }
        private void updateConnectionStatuErrorAuth()
        {
            string data = "Ошибка: не верный логин или пароль";
            _updateLoginForm.updateErrorLoginForm(data);
            _updateMainForm.updateTitleMainWindow("Ошибка: не верный логин или пароль");

            //_updateLoginForm.updateCreatLoginForm(data);

            progress.Report(false);
        }


        private void updateConnectionStatusNO()
        {
            
            string data = "Ошибка: не удалось подкл. к серверу";
            _updateLoginForm.updateErrorLoginForm(data);
            progress.Report(false);

        }

        private void startRunningFirstSync(ActiveMqSubscribleModel activeMqSubscrible , FirstSyncFilesController firstSync)
        {
            try
            {

                //начинаем первичную синхронизацию данных
                firstSync.startFirstSync(this, _updateMainForm, activeMqSubscrible);
                _updateMainForm.updateTitleMainWindow("Синхронизация");

            }
            catch (System.NullReferenceException f)
            {
                Console.WriteLine(f);
            }
         
        }
        private void updateConnectionStatusOK()
        {
            _updateMainForm.updateTitleMainWindow("Подключено");
            _updateLoginForm.updateButtonLoginForm("OK");
            _updateLoginForm.updatePgLoginForm("100");
        }


        public bool isFirstSyncFiles()
        {
            return firstSync;
        }

        public void setFirstSyncFiles(bool sync)
        {
            firstSync = sync;
        }

        //статус подключен клиент или нет
        public bool isStatusClientMqServer()
        {
            return _statucMqClient.isStatusConnectedServerMq();
        }
        public ISession getClient(int client)
        {
            return _mqClient.getClient(client);
        }

        public int getClientSize()
        {
            return _mqClient.getClientSize();
        }

        //статус идет подключение или нет
        //используется для оповещение внешних классов
        public bool isConnectedRunning()
        {
            return runningConnect;
        }
        public void setConnectedRunning(bool val)
        {
            runningConnect = val;
        }
        //срабатывает при изменении статуса подключения
         private void ReportProgress(bool value)
        {
            runningConnect = value;
        }

        public void SendMqJsonServer(string sendModel)
        {
            _mqBroker.SendAsyncMessage(_mqClient.getClient(0) , sendModel);
        }

        public void SendMqLazyJsonServer(string sendModel)
        {
            _mqBroker.SendAsyncLazyMessage(_mqClient.getClient(0), sendModel);
        }

        public int getSizeMqConnectec()
        {
            return _mqClient.getClientSize();
        }

        public void closeMqClient()
        {
            _mqClient.closeClient();
        }

        public void closeListerner()
        {
            _mqBroker.destroyAllListener();
        }

        private void createMqCLient(ref MqClient _mqClient, ExceptionController exceptionController, ref MqBroker _mqBroker)
        {
            if (_mqClient == null)
            {
                _mqClient = new MqClient(_statucMqClient);
                _mqBroker = new MqBroker(_cancellationController, exceptionController);
            }
        }

    }
}
