﻿using NewHH_SoftClientWPF.contoller.update;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.mqclient.support
{
    public class TransportStatusMqClient
    {
        private UpdateFormMainController updateForm;

        public TransportStatusMqClient(Container cont)
        {
            updateForm  = cont.GetInstance<UpdateFormMainController>();
        }
        private bool statusClient = false;

        public bool isStatusConnectedServerMq()
        {
                return statusClient;
        }
        //потоко безопасно на всякий случай
        public void setStatusConnectedServerMq(bool status)
        {
            lock (this)
                statusClient = status;
        }
        //когда падает подкл с инетом или сервером если не поставлен failover
        public void OnExceptionListener(Exception exception)
        {
            updateForm.updateTitleMainWindow("Нет подключения");
            setStatusConnectedServerMq(false);
        }
        //Когда поднимается Link с сервером
        public void OnConnectionResumedListener()
        {
            setStatusConnectedServerMq(true);
        }
        //когда падает Link c вкл failover
        public void OnConnectionInterruptedListener()
        {
            updateForm.updateTitleMainWindow("Нет подключения");
            setStatusConnectedServerMq(false);
        }

    }
}
