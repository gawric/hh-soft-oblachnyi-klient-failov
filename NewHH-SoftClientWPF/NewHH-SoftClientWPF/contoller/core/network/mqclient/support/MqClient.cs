﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.ActiveMQ.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.network.mqclient.support
{
    public class MqClient
    {
        //сессия работает с потками данных
        private ISession client;
        //проверяем есть ли подкл с сервером
        private TransportStatusMqClient statucMqClient;
        private Dictionary<int, ISession> warehoseSession = new Dictionary<int, ISession>();
        bool isFirstAdd = false;


        public MqClient(TransportStatusMqClient statusMqClient)
        {
            this.statucMqClient = statusMqClient;
            staticVariable.Variable.activemqConnectionId = new Dictionary<string, bool>();
        }
      

        private void setWarehouse(ISession session)
        {
            if(warehoseSession != null)
            {
                 if (isFirstAdd) { isFirstAdd = false; addWarehouseSession(0, session); } else { isFirstAdd = true; addWarehouseSession(1, session); }
            }
        }

        private void addWarehouseSession(int count , ISession session)
        {
            if (!warehoseSession.ContainsKey(count))
            {
                warehoseSession.Add(0, session);
            }
            else
            {
                warehoseSession[0] = session;
            }
        }

        //progressBool - отображает статус запуска подключения (если идет запуск подключения мы не пытаемся подключиться еще раз)
        public async Task<ISession> CreateNewConnectMqCLient(string username , string password)
        {
            return await Task.Run(() =>
             {
                 string URL = "failover:"+staticVariable.Variable.getDomenMqServer() + "" + staticVariable.Variable.getPortMqServer() + "" + "?transport.acceptInvalidBrokerCert=true";
                 IConnectionFactory connectionFactory = new ConnectionFactory(URL);


                 try
                 {
                     IConnection _connection = connectionFactory.CreateConnection(username, password);

                     _connection.ConnectionInterruptedListener += new ConnectionInterruptedListener(statucMqClient.OnConnectionInterruptedListener);
                     _connection.ConnectionResumedListener += new ConnectionResumedListener(statucMqClient.OnConnectionResumedListener);
                     _connection.ExceptionListener += new ExceptionListener(statucMqClient.OnExceptionListener);

                     _connection.Start();


                     client = _connection.CreateSession();

                   //присваиваем самую важную переменную clientID мы их создаем несколько шт.
                     if (!staticVariable.Variable.activemqConnectionId.ContainsKey(_connection.ClientId))
                         staticVariable.Variable.activemqConnectionId.Add(_connection.ClientId, false);

                     setWarehouse(client);

                     return client;

                 }
                 catch (Apache.NMS.NMSSecurityException z)
                 {
                     Console.WriteLine("MqClient->CreateNewConnectMqCLient: Ошибка не верный логин или пароль: "+ z);
                     return null;
                     //throw;
                 }
                 catch (Apache.NMS.NMSConnectionException)
                 {
                   
                     throw;
                 }

             });

                

        }
       

        public ISession getClient(int client)
        {
            return getClientWareHouse(client);
        }

        public void closeClient()
        {
            if (client != null)
            {
                foreach (var item in warehoseSession.Values)
                {
                    item.Close();
                }

                warehoseSession.Clear();
                client.Close();
                client = null;
            }
                
        }

        public int getClientSize()
        {
            if (warehoseSession != null) return warehoseSession.Count(); else { return 0; };

        }

        private ISession getClientWareHouse(int key)
        {
            ISession client = null;

            if (warehoseSession != null)
            {
                if (warehoseSession.ContainsKey(key))
                    client =  warehoseSession[key];
            }


            return client;


        }
    }
}
