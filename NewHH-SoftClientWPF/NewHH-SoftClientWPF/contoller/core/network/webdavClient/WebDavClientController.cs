﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;

using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload;
using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.statusOperation;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using NewHH_SoftClientWPF.staticVariable;
using Newtonsoft.Json;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebDAVClient;
using WebDAVClient.Model;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient
{
    public class WebDavClientController
    {
        private WebDavClient _webdavclient;
       
        private MainWindowViewModel _viewModel;
        private ViewTransferViewModel _viewTransferModel;
        private RenameFolderController _renameFolderController;
        private WebDavUpload _uploadWeb;
        private IWebDavDownload _downloadWeb;
        private UpdateNetworkJsonController _networkJsonController;
        private UpdateViewTransferController _updateTransferController;
        private ConvertIconType _convertIcon;
        private WebDavClientExist _webDavExist;
        private BlockingEventController _blockingEventController;
        private CancellationController _cancellationController;
        private StatusAllOperationModel _statusAllOperationModel;
        private ExceptionController _exceptionController;

        public WebDavClientController(Container cont)
        {
            if(cont != null)
            {
                //_statusTransaction = cont.GetInstance<StatusSqlliteTransaction>();

                _exceptionController = cont.GetInstance<ExceptionController>();
                _webdavclient = new WebDavClient(_exceptionController);
                //_copyWindows = cont.GetInstance<CopyMainWindow>();
                //_copyViewModel = cont.GetInstance<CopyViewModel>();
                _viewModel = cont.GetInstance<MainWindowViewModel>();
                _viewTransferModel = cont.GetInstance<ViewTransferViewModel>();
                _renameFolderController = cont.GetInstance<RenameFolderController>();
                _networkJsonController = cont.GetInstance<UpdateNetworkJsonController>();
                _uploadWeb = new WebDavUpload();
                _downloadWeb = new WebDavDownload();
                _updateTransferController = cont.GetInstance<UpdateViewTransferController>();
                _convertIcon = cont.GetInstance<ConvertIconType>();
                _webDavExist = cont.GetInstance<WebDavClientExist>();
                _blockingEventController = cont.GetInstance<BlockingEventController>();
                _cancellationController = cont.GetInstance<CancellationController>();
                _statusAllOperationModel = cont.GetInstance<StatusAllOperationModel>();
            }
           
           

        }

        public void uploadWebDav(SqliteController _sqlLiteController , string[] allListHref , FileInfoModel pasteModel , Dictionary<string,long> dictUploadHrefRow_id , InfoStackModel infoStack , List<FileInfoModel> tempFileSubsribleList)
        {
            UploadWebDavModel uploadModel = new UploadWebDavModel();
            UploadWebDavObjectModel uploadModelObj = new UploadWebDavObjectModel();


            string[] username = _sqlLiteController.getSelect().GetUserSqlite();

            uploadModel.infoStack = infoStack;
            uploadModel.username = username[0];
            uploadModel.password = username[1];
            uploadModel.allListHref = allListHref;
            uploadModel.pasteModel = pasteModel;
            uploadModel.checkParent = new SotredSourceHrefToDouble();
            uploadModel.allHrefFolderSqllite = _sqlLiteController.getSelect().getAllHrefFolder();
            uploadModel.dictUploadHrefRow_id = dictUploadHrefRow_id;
            uploadModel.tempFileSubsribleList = tempFileSubsribleList;

            uploadModelObj._webDavclient = _webdavclient;
            uploadModelObj.networkJsonController = _networkJsonController;
            uploadModelObj._sqlLiteController = _sqlLiteController;
            uploadModelObj._updateTransferController = _updateTransferController;
            uploadModelObj._transferViewNodes = _viewTransferModel.ListView.Items;
            uploadModelObj._cancellationController = createCancelationUpload(_cancellationController);
            uploadModelObj._blockingEventController = createBlockingUpload(_blockingEventController);
            uploadModelObj._exceptionController = _exceptionController;
            uploadModelObj.convertIcon = _convertIcon;
            uploadModelObj.webDavExist = _webDavExist;

   
          

            _uploadWeb.UploadFiles(uploadModel , uploadModelObj);
         
        }

        public WebDavUpload getWebDavUpload()
        {
            return _uploadWeb;
        }


        public void downloadWebDav(SqliteController _sqlLiteController , NavigationMainWindowsController _navigationController , List<FolderNodes> listDownload , Dictionary<string, long> dictDownloadHrefRow_id , ViewFolderViewModel _viewFolderModel , MainWindowViewModel _viewMain , bool isViewFolder)
        {

            DownloadWebDavModel model = new DownloadWebDavModel();
            model.convertIcon = _convertIcon;
            model._exceptionController = _exceptionController;
            model._viewFolderModel = _viewFolderModel;
            model._viewMain = _viewMain;
            model.isViewVolder = isViewFolder;
            string locationDownload = _sqlLiteController.getSelect().getLocationDownload();

            CancellationController _cancellationControllerDownload = createCancelationDownload(_cancellationController);
            BlockingEventController _blockingController = createBlockingDownload(_blockingEventController);


            //проверка что locationDownload заполненно и существует
            if (_downloadWeb.isCheckLocationDownload(locationDownload))
            {
                model._listDownload = listDownload;
                model._webDavclient = _webdavclient;
                string[] username = _sqlLiteController.getSelect().GetUserSqlite();
                model.username = username[0];
                model.password = username[1];
                model.locationDownload = locationDownload;
                model._sqlliteController = _sqlLiteController;
                model._navigationController = _navigationController;
                model._updateTransferController = _updateTransferController;
                model._transferViewNodes = _viewTransferModel.ListView.Items;
                model._cancellationController = _cancellationControllerDownload;
                model._blockingController = _blockingController;
                model.dictDownloadHrefRow_id = dictDownloadHrefRow_id;

                model._stackUpdateTreeViewIcon = new StackUpdateTreeViewIcon(model.convertIcon);

                _downloadWeb.download(model);

            }
            else
            {
                Console.WriteLine("WebDavClientController->DownloadWeDav-> Критическая ошибка!!! не определена папка для сохранения файлов на компьютере!!!");
                model._exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavClientController->DownloadWeDav: Критическая ошибка ", "Не определена папка для сохранения файлов на компьютере!!!");
            }
            
          

            
        }

        public WebDavClient getWebDavClient()
        {
            return _webdavclient;
        }

        public async void createNewConnect(string username, string password)
        {
            try
            {
                await _webdavclient.CreateNewConnectAsync(username, password);
            }
            catch (WebDAVClient.Helpers.WebDAVException s)
            {
                // string data = "Ошибка: не верный логин или пароль";
                // updateForm.updateErrorLoginForm(data);
                Console.WriteLine(s.ToString());
            }
        }

    
        //пересканируем файлы webdav с очисткой всей базы
        public void getListWebDavAsync(UpdateNetworkJsonController updateNetworkJsonController, long newVersionBases, string username, SupportServerTopicModel supportTopicModel, SqliteController _sqlLiteController)
        {


            Progress<double> progress = new Progress<double>();
      
            progress.ProgressChanged += (s, e) =>
            {
                _viewModel.pg = e;
            };

            Task.Run(async () =>
            {
                try
                {
                    WebDavGetAllList getScanRootFolder = new WebDavGetAllList();
                    WebDavGetAllListModel model = createModel(ref updateNetworkJsonController, ref newVersionBases, ref username, ref supportTopicModel, ref _sqlLiteController, ref _webdavclient, ref progress, ref _viewModel ,ref  _exceptionController);
                    getScanRootFolder.ClearBasesRescanWebDavAllListStart(model);
                }
                catch (System.Net.Http.HttpRequestException g)
                {
                    // _viewModel.Titlename = "Критическая ошибка нет подключения к WebDavServer";
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController->getListWebDavAsync: Критическая ошибка нет подключения к WebDavServer ", g.ToString());
                }
                catch (System.InvalidOperationException g)
                {
                    Console.WriteLine("WebDavClientController->getListWebDavAsync: "+g.ToString());
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController->getListWebDavAsync: Критическая ошибка сканирования", g.ToString());
                }


            });

        }

        private WebDavGetAllListModel createModel(ref UpdateNetworkJsonController updateNetworkJsonController , ref long newVersionBases , ref string username , ref SupportServerTopicModel supportTopicModel , ref SqliteController _sqlLiteController , ref WebDavClient _webdavclient , ref Progress<double> progress , ref MainWindowViewModel _viewModel, ref ExceptionController _exceptionController)
        {
            WebDavGetAllListModel model = new WebDavGetAllListModel();
            model.updateNetworkJsonController = updateNetworkJsonController;
            model.newVersionBases = newVersionBases;
            model.username = username;
            model.supportTopicModel = supportTopicModel;
            model._sqlLiteController = _sqlLiteController;
            model._webdavclient = _webdavclient;
            model.progress = progress;
            model._viewModel = _viewModel;
            model._allOperationModel = _statusAllOperationModel;
            model._cancellationController = createCancellation();
            model._exceptionController = _exceptionController;

            return model;

        }

        private CancellationController createCancellation()
        {
            _cancellationController.removeObjectWarehouse(Variable.autoSyncClearBasesCancellationTokenId);
            _cancellationController.addWarehouse(Variable.autoSyncClearBasesCancellationTokenId, new CancellationTokenSource());

            return _cancellationController;
        }

        public async Task<bool> isExistLocation(PasteFilesAsyncModel PasteModel , Dictionary<FolderNodes, bool> allCopyNodes, Dictionary<FileInfoModel, bool> allPasteNodes)
        {
            List<string> listLocation = merge(allCopyNodes, allPasteNodes);
            listLocation.Add(PasteModel.pasteFolder.Location);

            return await PasteModel._webDavClientExist.isExistAllDirectory(listLocation);
        }

        public async Task<List<string>> getListNoExist(PasteFilesAsyncModel PasteModel, Dictionary<FolderNodes, bool> allCopyNodes, Dictionary<FileInfoModel, bool> allPasteNodes)
        {
            List<string> listLocation = merge(allCopyNodes, allPasteNodes);
            listLocation.Add(PasteModel.pasteFolder.Location);
            return await PasteModel._webDavClientExist.getListExist(listLocation);
        }

       

        public async Task StartCopy(PasteFilesAsyncModel PasteModel , long[][][] allChildrenList , bool isMove)
        {
            try
            {
                CopyViewModel status = PasteModel.status;
                IProgress<CopyViewModel> progress = PasteModel.progress;

                status.StatusWorker = "Отправка файлов на сервер";
                progress.Report(status);

              

               
                    if (isMove)
                    {
                        WebDavCopyFiles webDavCopy = new WebDavCopyFiles(PasteModel.convertIcon , PasteModel._exceptionController);
                        webDavCopy.MoveWebDavAndJson(PasteModel, isMove, _webdavclient, allChildrenList);

                    }
                    else
                    {

                        WebDavCopyFiles webDavCopy = new WebDavCopyFiles(PasteModel.convertIcon , PasteModel._exceptionController);
                        webDavCopy.CopyWebDavAndJson(PasteModel, isMove, _webdavclient, allChildrenList);

                    }
                
                
                


            }
            catch (System.Data.SQLite.SQLiteException g)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController->StartCopy: Критическая ошибка копирования ", g.ToString());
            }
            catch (AggregateException g)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController->StartCopy: Критическая ошибка копирования ", g.ToString());
            }
            catch (WebDAVClient.Helpers.WebDAVException g)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController->StartCopy: Критическая ошибка копирования ", g.ToString());
            }
            catch (System.InvalidOperationException g)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController->StartCopy: Критическая ошибка копирования ", g.ToString());
            }
         
        }

        

        private void updateInitProgressStatus(CopyViewModel status , string StatusWorker , string where)
        {
            status.where = where;
            status.StatusWorker = "init";
        }

        private void updateRunningProgressStatus(CopyViewModel status, string StatusWorker, IProgress<CopyViewModel> progress)
        {
            status.StatusWorker = StatusWorker;
            progress.Report(status);
        }

       

        public async Task getFolder(string folder)
        {
            IEnumerable<Item> folderC = await _webdavclient.getListFilesAsync(folder);
          
        }

        public async Task StartDeleteWebDavClient(DeleteFilesAsyncModel DeleteModel ,  List<long[][]>allList, int numberOperation)
        {
            try
            {
                WebDavDeleteFiles DeleteWebDav = new WebDavDeleteFiles(_exceptionController);
                await DeleteWebDav.StartDeleteWebDavClient(DeleteModel, allList, numberOperation, _webdavclient);
            }
            catch(WebDAVClient.Helpers.WebDAVException d)
            {
                Console.WriteLine("WebDavClientController->StartDeleteWebDavClient: Ошибка удаления файлов с удаленного компьютера "+d.ToString());
            }
          
        }

        public Client CreateClient(string username , string password)
        {
            try
            {
                return _webdavclient.CreateNewConnect(username, password);
            }
            catch(WebDAVClient.Helpers.WebDAVException s )
            {
                Console.WriteLine(s);
                return null;
               
            }
           
        }


        public void RenameWebDavClient(RenameWebDavModel renameModel ,  List<FileInfoModel> renameList)
        {
            WebDavRenameFiles _webDavRename = new WebDavRenameFiles(_exceptionController);
            renameModel._webdavclient = _webdavclient;
            _webDavRename.StartRenameWebDav(renameModel ,  renameList , _renameFolderController);
        }



        public Client getConnect()
        {
            return _webdavclient.getConnect();          
        }



        private List<string> merge(Dictionary<FolderNodes, bool> allCopyNodes, Dictionary<FileInfoModel, bool> allPasteNodes)
        {
            List<string> listExist = new List<string>();
            listExist = addListCopy(allCopyNodes, listExist);
            listExist = addListPaste(allPasteNodes, listExist);

            return listExist;

        }

        private List<string> addListCopy(Dictionary<FolderNodes, bool> dict, List<string> listExist)
        {
            foreach (KeyValuePair<FolderNodes, bool> item in dict)
            {
                listExist.Add(item.Key.Location);
            }

            return listExist;
        }

        private List<string> addListPaste(Dictionary<FileInfoModel, bool> dict, List<string> listExist)
        {
            foreach (KeyValuePair<FileInfoModel, bool> item in dict)
            {
                listExist.Add(item.Key.location);
            }

            return listExist;
        }

        private CancellationController createCancelationUpload(CancellationController _cancellationController)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            _cancellationController.removeObjectWarehouse(staticVariable.Variable.uploadCancelationTokenId);
            _cancellationController.addWarehouse(staticVariable.Variable.uploadCancelationTokenId, cts);

            return _cancellationController;
        }

        private CancellationController createCancelationDownload(CancellationController _cancellationController)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            _cancellationController.removeObjectWarehouse(staticVariable.Variable.downloadCancelationTokenId);
            _cancellationController.addWarehouse(staticVariable.Variable.downloadCancelationTokenId, cts);

            return _cancellationController;
        }

        private BlockingEventController createBlockingUpload(BlockingEventController _blockingEventController)
        {
            _blockingEventController.createManualResetEvent(staticVariable.Variable.blockingUploadTokenId);
            _blockingEventController.getManualReset(staticVariable.Variable.blockingUploadTokenId).setMre();

            return _blockingEventController;
        }

        private BlockingEventController createBlockingDownload(BlockingEventController _blockingEventController)
        {
            _blockingEventController.createManualResetEvent(staticVariable.Variable.blockingDownloadTokenId);
            _blockingEventController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).setMre();

            return _blockingEventController;
        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }



    }
}
