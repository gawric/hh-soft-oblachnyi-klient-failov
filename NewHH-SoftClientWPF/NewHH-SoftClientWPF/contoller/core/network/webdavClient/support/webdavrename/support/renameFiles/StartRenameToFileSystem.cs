﻿using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.contoller.util.filesystem.convert;
using NewHH_SoftClientWPF.contoller.util.filesystem.move;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support
{
    public class StartRenameToFileSystem : IRenameFiles
    {
    
        public  void renameFileSystem(RenameWebDavModel renameModel)
        {
            ConvertLocationWebDavToFileSystem convert = new ConvertLocationWebDavToFileSystem();
            MoveFolderFileSystem mffs = new MoveFolderFileSystem();
            string destTemp = checkDisk(renameModel.pasteLocation, renameModel.pasteName);

            string locationDownload = renameModel._sqliteController.getSelect().getLocationDownload();
            string sourcesFilePath = convert.convertLocationToHref(renameModel.oldLocation, locationDownload);
            string destinationFilePath = convert.convertLocationToHref(destTemp, locationDownload);



            mffs.move(sourcesFilePath, destinationFilePath);

        }

        public Task start(List<FileInfoModel> renameList, RenameWebDavModel renameModel, RenameFolderController _renameFolderController)
        {
            throw new NotImplementedException();
        }

        public Task startTraining(RenameWebDavModel renameModel, List<FileInfoModel> renameList, RenameFolderController _renameFolderController)
        {
            throw new NotImplementedException();
        }

        private string checkDisk(string destinationFilePath, string pasteName)
        {
            if (staticVariable.UtilMethod.isDisk(ref destinationFilePath))
            {
                return staticVariable.UtilMethod.getRootLocation(true, ref destinationFilePath) + pasteName;
            }
            else
            {
                return destinationFilePath + pasteName;
            }
        }
    }
}
