﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.webdavClient;
using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support
{
    public class StartRenameFilesWebDavAsync : IRenameFiles
    {
        private ExceptionController exceptionContoller;
        public StartRenameFilesWebDavAsync(ref ExceptionController exceptionContoller)
        {
                this.exceptionContoller = exceptionContoller;
        }
        public async Task start(List<FileInfoModel> renameList, RenameWebDavModel renameModel, RenameFolderController _renameFolderController)
        {
            try
            {

                for (int h = 0; h < renameList.Count; h++)
                {
                    if (renameList[h] != null)
                    {

                        FileInfoModel item = renameList[h];
                        //существует ли физически на диск данный файл
                        bool check = await isExistsWebDav(renameModel._webDavClientExist, item.location);

                        if (check)
                        {
                            if (staticVariable.Variable.isFolder(item.type))
                            {
                                isFolder(renameModel, item, _renameFolderController);
                            }
                            else
                            {
                                isFiles(renameModel, item, _renameFolderController);
                            }
                        }
                        else
                        {

                            sendToDelete(item.location, renameModel, item);
                            Console.WriteLine("WebDavRenameFiles: StartRenameAsync-> Такой файл не найден на удаленном диске! Путь к файлу: " + item.location);
                        }



                    }

                }
            }
            catch (NetworkException)
            {
                throw;
            }

        }

        private async Task<bool> isExistsWebDav(WebDavClientExist _webDavClientExist , string location)
        {
            bool check = false;
            if(_webDavClientExist != null) check =  await _webDavClientExist.isExist(location);

            return check;
        }
        private void sendToDelete(string location, RenameWebDavModel renameModel, FileInfoModel item)
        {
            FileInfoModel[] arr = new FileInfoModel[1000];
            long newVersionbases = getVersionDbClient(renameModel._sqliteController);
            FileInfoModel model = getFimToLocation(renameModel._sqliteController, location);
            model.filename = item.filename;
            model.changeRows = "DELETE";
            arr[1] = model;

            sendBegin(renameModel._updateNetworkJsonController, newVersionbases);
            sendEnd(renameModel._updateNetworkJsonController, ref arr, newVersionbases);
        }


        private void sendBegin(UpdateNetworkJsonController updateNetworkJsonController, long newVersionbases)
        {
            if(updateNetworkJsonController != null) updateNetworkJsonController.updateSendJsonDELETEBEGINPARTIES(null, newVersionbases);
        }

        private void sendEnd(UpdateNetworkJsonController updateNetworkJsonController,ref FileInfoModel[] arr, long newVersionbases)
        {
            if (updateNetworkJsonController != null) updateNetworkJsonController.updateSendJsonDELETEENDPARTIES(arr, newVersionbases);
        }

        private FileInfoModel getFimToLocation(SqliteController sql , string location)
        {
            FileInfoModel fim = new FileInfoModel();
            if (sql != null) fim = sql.getSelect().getSearchNodesByLocation(location);

            return fim;
        }
        private long getVersionDbClient(SqliteController sql)
        {
            long ver = 0;
            if(sql != null) ver =  sql.getSelect().getSqlLiteVersionDataBasesClient();

            return ver;

        }


        private void isFiles(RenameWebDavModel renameModel, FileInfoModel item, RenameFolderController _renameFolderController)
        {
            MoveFilesAsyncModel moveModel = convertRenameModelToMoveFilesAsyncModel(false, item.filename, item.location, renameModel);

            renameModel._webdavclient.moveFilesAsync(moveModel, renameModel.client).Wait();

            item = ConvertLocationURLToLocationNewNameURL(item, renameModel.oldFolderName);
        }

        //если это папка продолжаем сканирование
        private void isFolder(RenameWebDavModel renameModel, FileInfoModel item, RenameFolderController _renameFolderController)
        {
            bool ok_rename = true;

            MoveFilesAsyncModel moveModel = convertRenameModelToMoveFilesAsyncModel(true, item.filename + "/", item.location, renameModel);
            try
            {
                renameModel._webdavclient.moveFilesAsync(moveModel, renameModel.client).Wait();

                item = ConvertLocationURLToLocationNewNameURL(item, renameModel.oldFolderName);

                string newLocation = item.location;

                if (ok_rename)
                    _renameFolderController.StartSendJsonRenameFolder(renameModel, newLocation, item.versionUpdateBases);

            }
            catch (NetworkException z)
            {
                // Console.WriteLine("WebDavRenameFiles-> isFolder->Ошибка копирования папки " + d);

                ok_rename = false;
                throw;
            }

        }

        private FileInfoModel ConvertLocationURLToLocationNewNameURL(FileInfoModel item, string oldFolderName)
        {
            //преобразует в формат URL имя
            string updateOldName = staticVariable.Variable.EscapeUrlString(oldFolderName);
            string updateNewName = staticVariable.Variable.EscapeUrlString(item.filename);

            string escLocation = staticVariable.Variable.EscapeUrlString(item.location);

            string convert_location = "";
            try
            {
                if (staticVariable.Variable.isFolder(item.type))
                {

                    convert_location = createNewLocationRename(escLocation, updateOldName, convert_location, updateNewName);
                    // convert_location = item.location.Replace(updateOldName + "/", updateNewName + "/");
                }
                else
                {
                    convert_location = item.location.Replace(updateOldName, updateNewName);
                }

                item.location = convert_location;

            }
            catch (System.ArgumentOutOfRangeException s)
            {
                Console.WriteLine("WebDavRenameFiles->ConvertLocationURLToLocationNewNameURL: Аварийная ошибка нужно пересканирование всех папок  " + s);
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavRenameFiles-> ConvertLocationURLToLocationNewNameURL-> NullReferenceException", s.ToString());
            }


            return item;
        }

        private string createNewLocationRename(string escLocation, string updateOldName, string convert_location, string updateNewName)
        {
            //находим последнее вхождение т.к могу быть 2 папки с одним и тем же названием
            int lastIndex = escLocation.LastIndexOf(updateOldName);
            int sizeString = escLocation.Length - 1;
            //обрезаем последнее вхождение
            //string itemLocationClear = escLocation.Substring(lastIndex, sizeString - lastIndex);

            string rootItemLocationClear = escLocation.Substring(0, lastIndex);

            //добавляем в конец новое имя старое имя мы очищаем с помощью LastIndexOf
            return convert_location = rootItemLocationClear + updateNewName + "/";
        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            exceptionContoller.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }

        private MoveFilesAsyncModel convertRenameModelToMoveFilesAsyncModel(bool isFolder, string copyFolderName, string location, RenameWebDavModel renameModel)
        {
            MoveFilesAsyncModel moveModel = new MoveFilesAsyncModel();

            moveModel.isFolder = isFolder;
            moveModel.copyFolderName = copyFolderName;
            moveModel.location = location;
            moveModel.pasteLocation = renameModel.pasteLocation;
            moveModel.username = renameModel.username;
            moveModel.password = renameModel.password;
            moveModel.oldFolderName = renameModel.oldFolderName;

            return moveModel;
        }

        public void renameFileSystem(RenameWebDavModel renameModel)
        {
            throw new NotImplementedException();
        }

        public Task startTraining(RenameWebDavModel renameModel, List<FileInfoModel> renameList, RenameFolderController _renameFolderController)
        {
            throw new NotImplementedException();
        }
    }
}
