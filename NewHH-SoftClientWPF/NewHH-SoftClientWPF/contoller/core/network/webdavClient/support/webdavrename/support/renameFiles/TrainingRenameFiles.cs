﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support;
using NewHH_SoftClientWPF.contoller.operationContextMenu.rename;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavrename.support
{
    public class TrainingRenameFiles: IRenameFiles
    {
        private ExceptionController exceptionContoller;
        private StartRenameFilesWebDavAsync renameAsync;
        public TrainingRenameFiles(ref ExceptionController exceptionContoller)
        {
            this.exceptionContoller = exceptionContoller;
            this.renameAsync = new StartRenameFilesWebDavAsync(ref exceptionContoller);
        }

        

        //подготовка
        public async Task  startTraining(RenameWebDavModel renameModel, List<FileInfoModel> renameList, RenameFolderController _renameFolderController)
        {
            UpdateNetworkJsonController _updateNetworkJsonController = renameModel._updateNetworkJsonController;
            Client client = createConnected(renameModel._webdavclient, renameModel.username, renameModel.password);
            FileInfoModel[] renameArr = renameList.ToArray();
            long versionBases = GetVersionBases(renameList);
            renameModel.client = client;

            renameModel.allStatus.isRename = true;

            sendBegin(ref _updateNetworkJsonController, ref renameArr, ref versionBases);

            bool isSend = true;

            try
            {
                //отправка
                await renameAsync.start(renameList, renameModel, _renameFolderController);
            }
            catch (NetworkException z)
            {
                isSend = false;
            }
            catch (System.AggregateException z)
            {
                isSend = false; 
                generatedError((int)CodeError.NETWORK_ERROR, "TrainingRenameFiles-> Start startTraining   " , z.ToString());
            }


            if (!isSend)
                renameArr = null;


            sendEnd(ref _updateNetworkJsonController, ref renameArr, ref versionBases);

            renameModel.allStatus.isRename = false;
        }

        private Client  createConnected(WebDavClient _webdavclient  ,  string username , string password)
        {
            Client client = null;
            if(_webdavclient != null) client = _webdavclient.CreateNewConnect(username, password);
       
            return client;
        }
        private void sendBegin(ref UpdateNetworkJsonController _updateNetworkJsonController , ref FileInfoModel[] renameArr , ref long versionBases)
        {
            if(_updateNetworkJsonController != null) _updateNetworkJsonController.updateSendJsonRENAMESINGLBEGINUPDATEPARTIES(renameArr, versionBases);

        }

        private void sendEnd(ref UpdateNetworkJsonController _updateNetworkJsonController, ref FileInfoModel[] renameArr, ref long versionBases)
        {
            if (_updateNetworkJsonController != null) _updateNetworkJsonController.updateSendJsonRENAMESINGLENDUPDATEPARTIES(renameArr, versionBases);

        }
        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            exceptionContoller.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }

        private long GetVersionBases(List<FileInfoModel> renameList)
        {
            long versionBases = 0;

            if(renameList.Count > 0)
            {
                if (renameList[0] != null)
                {
                    versionBases = renameList[0].versionUpdateBases;
                }
            }
            
           

            return versionBases;
        }

       
        public void renameFileSystem(RenameWebDavModel renameModel)
        {
            throw new NotImplementedException();
        }

        public Task start(List<FileInfoModel> renameList, RenameWebDavModel renameModel, RenameFolderController _renameFolderController)
        {
            throw new NotImplementedException();
        }
    }
}
