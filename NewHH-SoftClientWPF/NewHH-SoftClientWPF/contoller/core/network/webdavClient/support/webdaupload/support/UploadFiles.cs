﻿using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support
{
    public class UploadFiles
    {
        public IDownload upload(UploadWebDavModel uploadModel, UploadWebDavObjectModel uploadModelObj, FileInfoModel fileInfoModel, WebDavHrefModel hrefModel)
        {
            FolderNodesTransfer folderNodesTransfer = createModelTransfer(uploadModelObj, fileInfoModel);
            transferControllerCreater(ref uploadModelObj, ref  folderNodesTransfer);

            IDownload statusDownload = createIDownload(uploadModelObj, fileInfoModel);
            updateProgressBar(ref uploadModelObj , ref uploadModel, ref statusDownload, ref folderNodesTransfer);

            uploadFiles(statusDownload, hrefModel, uploadModel.client);

            return statusDownload;
        }

        private void updateProgressBar(ref UploadWebDavObjectModel uploadModelObj , ref UploadWebDavModel uploadModel ,  ref IDownload statusDownload , ref FolderNodesTransfer folderNodesTransfer)
        {
            if (uploadModelObj._updateTransferController != null) uploadModelObj._updateTransferController.UpdateFilesProgressBar(statusDownload, uploadModelObj , uploadModel ,folderNodesTransfer.Row_id, folderNodesTransfer.Row_id_listFiles_users);
        }
                                                                                                               
        private void transferControllerCreater(ref UploadWebDavObjectModel uploadModelObj , ref FolderNodesTransfer folderNodesTransfer)
        {
            if(uploadModelObj._updateTransferController != null) uploadModelObj._updateTransferController.createProgressBar(folderNodesTransfer, "test");
        }
        private FolderNodesTransfer createModelTransfer(UploadWebDavObjectModel uploadModel, FileInfoModel fileInfoModel)
        {
            try
            {
                CreateModel createModel = new CreateModel(uploadModel.convertIcon);
                FolderNodesTransfer folderNodesTransfer = createModel.createViewTransferModel(fileInfoModel, fileInfoModel.filename, "Запуск", "upload", "upload");
                folderNodesTransfer.Row_id = getRowId(uploadModel);

                return folderNodesTransfer;

            }
            catch(System.IO.FileNotFoundException a)
            {
                Debug.Print("UploadFiles->createModelTransfer: "+a.ToString());
                return new FolderNodesTransfer();
            }
           
        }

        private long getRowId(UploadWebDavObjectModel uploadModelObject)
        {
            long row_id = 0;
            if(uploadModelObject._sqlLiteController != null) row_id = uploadModelObject._sqlLiteController.getSelect().getLastRow_idToListFilesTransfer() + 1;

            return row_id;
        }
       // int f = 0;
        private void uploadFiles(IDownload statusDownload, WebDavHrefModel hrefModel, Client client)
        {
            if(File.Exists(hrefModel.href))
            {
                using (var fileStream = File.OpenRead(hrefModel.href))
                {
                    try
                    {
                        //Debug.Print("UploadFiles->UploadFiles: передано на зугрузку " +f++);
                        client.Upload(statusDownload, hrefModel.pasteHref, fileStream, hrefModel.filename).Wait();
                    }
                    catch (System.Net.Http.HttpRequestException sa)
                    {
                        Console.WriteLine("WebDavController->UploadWebDav->StartUpload->UploadFiles HttpRequestException" + sa.ToString());
                        statusDownload.ChangeError();
                    }
                    catch (System.AggregateException sd)
                    {
                        Console.WriteLine("WebDavController->UploadWebDav->StartUpload->UploadFiles AggregateException: " + sd.ToString());
                        statusDownload.ChangeError();
                    }


                }
            }
            else
            {
                Debug.Print("UploadFiles->uploadFiles критическая ошибка не найден файл  " + hrefModel.href);
            }
            
        }

        private IDownload createIDownload(UploadWebDavObjectModel uploadModelObj, FileInfoModel fileInfoModel)
        {
            IDownload statusDownload = new Download();
            ManualResetEvent mre = uploadModelObj._blockingEventController.getManualReset(staticVariable.Variable.blockingUploadTokenId).getManualResetEvent();
            statusDownload.setMre(mre);
            statusDownload.setRow_id(fileInfoModel.row_id);
            statusDownload.setCancel(uploadModelObj._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.uploadCancelationTokenId));

            return statusDownload;
        }


    }
}
