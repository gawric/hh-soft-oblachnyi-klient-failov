﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;
using WebDAVClient.Progress;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support
{
    public class RunUploadFiles
    {
        public ExceptionController _exceptionController;

        public RunUploadFiles(ExceptionController exceptionController)
        {
            this._exceptionController = exceptionController;
        }
        public void run(ref UploadWebDavModel uploadModel , ref UploadWebDavObjectModel uploadModelObj, ref string href, ref int[] cnt, ref long version , IFileInfoProxy fi)
        {
            try
            {
                deleteTempPb(ref uploadModel , ref uploadModelObj ,  ref href);
                sendJsonUpdate(cnt, version, uploadModel , uploadModelObj);
                startUploadFiles(cnt, href, uploadModel , uploadModelObj , fi);

                cnt[0]++;
            }
            catch (Exception s)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "RunUploadFiles-> UplodFilesFirst: Критическая ошибка ", s.ToString());
            }
        }

        private void deleteTempPb(ref UploadWebDavModel uploadModel , ref UploadWebDavObjectModel uploadModelObj, ref string href)
        {
            Dictionary<string, long> dic = uploadModel.dictUploadHrefRow_id;
            if(uploadModelObj._updateTransferController != null) uploadModelObj._updateTransferController.deleteTempProgressBar(dic[href], href);
        }

        private void sendJsonUpdate(int[] cnt, long version, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadWebDavObj)
        {
            if (cnt[0] == staticVariable.Variable.CountFilesTrasferServer)
            {


                uploadWebDavObj.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(uploadModel.arr, version);

                for (int s = 0; s < uploadModel.arr.Length; s++)
                {
                    uploadModel.arr[s] = null;
                }

                cnt[0] = 0;

            }
        }

        private void startUploadFiles(int[] cnt, string href, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadWebDavObj ,  IFileInfoProxy fi)
        {
            try
            {
                startFilesUpload(cnt, href, uploadModel , uploadWebDavObj, fi);
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void startFilesUpload(int[] cnt, string href, UploadWebDavModel uploadModel , UploadWebDavObjectModel uploadWebDavObj , IFileInfoProxy fi)
        {
            try
            {
                
                SotredSourceHrefToDouble check = new SotredSourceHrefToDouble();

                WebDavHrefModel hrefModel = createWebDavModel(uploadModel, fi, href);
                FileInfoModel fileInfoModel = createFileInfoModelToFiles(uploadWebDavObj , uploadModel, hrefModel, fi.Name());


                if (fileInfoModel != null)
                {

                    uploadModel.arr = createArr(ref uploadModel);

                    bool isExist = isDublication(ref  uploadModel, ref  fileInfoModel);

                    if (isExist != true)
                    {
                        uploadModel.arr[cnt[0]] = fileInfoModel;

                        if (fi != null)
                        {
                            UploadFiles uploadFiles = new UploadFiles();
                            uploadModel._statusDownload = uploadFiles.upload(uploadModel, uploadWebDavObj, fileInfoModel, hrefModel);
                        }
                    }
                    else
                    {
                        // Console.WriteLine("WebDavUploadFiles->StartFilesUpload загрузка не возможна т.к такой файл существует на сервере");
                        throw new NetworkException((int)CodeError.NETWORK_ERROR, "WebDavUploadFiles->StartFilesUpload не критическая ошибка isExists Files", "Копируемый файл был найден на сервере webDav");
                    }

                }

            }
            catch (System.NullReferenceException)
            {
                throw;
            }
            catch (NetworkException)
            {
                throw;
            }
            catch (System.IO.FileNotFoundException)
            {
                throw;
            }


        }

        private bool isDublication(ref UploadWebDavModel uploadModel , ref FileInfoModel fileInfoModel)
        {
            return uploadModel.checkParent.checkPasteFolderToDouble(fileInfoModel.location, uploadModel.allHrefFolderSqllite);
        }

        private FileInfoModel[] createArr(ref UploadWebDavModel uploadModel)
        {
            if (uploadModel.arr == null)
            {
                uploadModel.arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            }

            return uploadModel.arr;
        }

        
     
      


        private FileInfoModel createFileInfoModelToFiles(UploadWebDavObjectModel uploadModelObj, UploadWebDavModel uploadWebDavModel , WebDavHrefModel hrefModel, string filename)
        {
            FileInfoModel model = new FileInfoModel();
            model.filename = filename;
            model.lastOpenDate = hrefModel.lastOpenDate;
            model.createDate = hrefModel.createDate;
            model.changeDate = hrefModel.changeDate;
            long rowNew_id = getLRow_id(uploadModelObj._sqlLiteController);
            long rowN = staticVariable.Variable.generatedNewRowId(rowNew_id, null);
            //Генерирует новые id номера
            model.row_id = rowN;
            insertRow(uploadModelObj._sqlLiteController, ref rowN);
            //generateNewIdUpload(uploadModel);
            model.type = Path.GetExtension(filename);
            model.parent = uploadWebDavModel.pasteModel.row_id;
            model.location = uploadWebDavModel.pasteModel.location + staticVariable.Variable.EscapeUrlString(filename);
            model.sizebyte = hrefModel.size;
            model.changeRows = "CREATE";
            model.attribute = "";
            model.versionUpdateBases = uploadWebDavModel.version_bases;

            model.user_id = getUserId(uploadModelObj._sqlLiteController);
            return model;
        }

        private long getUserId(SqliteController sqlcontr)
        {
            long user_id = 0;
            if(sqlcontr != null) user_id = sqlcontr.getSelect().existUserIdToListFiles();
            return user_id;
        }
        private void insertRow(SqliteController sqlcontr , ref long rowN)
        {
           if(sqlcontr != null) sqlcontr.getInsert().insertListFilesTempIndex(rowN);
        }
        private long getLRow_id(SqliteController sqlcontr)
        {
            long row_id = 0;
            if(sqlcontr != null) row_id = sqlcontr.getSelect().getLastRow_idFileInfoModel();
            return row_id;
        }

        private WebDavHrefModel createWebDavModel(UploadWebDavModel uploadModel, IFileInfoProxy fi, string href)
        {
            WebDavHrefModel hrefModel = createHrefModel(href, fi.Name(), uploadModel.pasteHref);
            hrefModel.changeDate = fi.LastWriteTime().ToString();
            hrefModel.lastOpenDate = fi.LastAccessTime().ToString();
            hrefModel.createDate = fi.CreationTime().ToString();
            hrefModel.size = fi.Length();

            return hrefModel;
        }

       

        private WebDavHrefModel createHrefModel(string href, string filename, string pasteHref)
        {
            WebDavHrefModel hrefModel = new WebDavHrefModel();
            hrefModel.href = href;
            hrefModel.filename = filename;
            hrefModel.pasteHref = pasteHref;
            return hrefModel;
        }


        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }

    }
}
