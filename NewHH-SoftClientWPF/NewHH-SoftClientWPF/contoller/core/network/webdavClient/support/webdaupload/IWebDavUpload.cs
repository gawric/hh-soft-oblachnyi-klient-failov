﻿using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload
{
    public interface IWebDavUpload
    {
       void uplodFilesFirst(string[] hrefPath, UploadWebDavModel uploadModel, UploadWebDavObjectModel uploadModelObj);
       Task uploadFolderFirst(string[] hrefPath, UploadWebDavModel uploadModel, long version, UploadWebDavObjectModel uploadModelObj);
    }
}
