﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support.webdavevent;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support
{
    //попытка отправлять заливаемые файлы пачками
    public class WebDavEventParties : IWebDavEvent
    {
        private EventHandler<EventScanFolderItemModel> eventAddItem;
        private InfoStackModel infoStack;
        private List<FileInfoModel> tempList = new List<FileInfoModel>();
        int allElements = 0;
        public WebDavEventParties(WebDavUploadFiles upload, EventHandler<EventScanFolderItemModel> eventUploadAddItem , InfoStackModel infoStack)
        {
            upload.beginEventHandler += c_BeginUpdateFolder;
            upload.updateEventHandler += c_UpdateFolder;
            upload.endEventHandler += c_EndUpdateFolder;
            this.eventAddItem = eventUploadAddItem;
            this.infoStack = infoStack;
        }

        void c_BeginUpdateFolder(object sender, UploadFolderEventHandler e)
        {
            if(IsEmptyStack() | IsFullArr(e.arr))
            {
                allElements = allElements + GetElements(e.arr);
                e.networkJsonController.updateSendJsonSINGLBEGINUPDATEPARTIES(e.arr, e.version);
            }
            else
            {
                AddListArr(e.arr);
            }

            PrintMessage();
        }

       
        void c_UpdateFolder(object sender, UploadFolderEventHandler e)
        {
            if (IsEmptyStack() | IsFullArr(e.arr))
            {
                allElements = allElements + GetElements(e.arr);
                e.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(e.arr, e.version);

                if (tempList.Count > 0)
                {
                    FileInfoModel[] arrTemp = tempList.ToArray();
                    GetElements(arrTemp);
                    e.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(arrTemp, e.version);
                }
            }
            else
            {
                AddListArr(e.arr);
            }

            PrintMessage();
        }
        void c_EndUpdateFolder(object sender, UploadFolderEventHandler e)
        {
            
            if (IsEmptyStack() | IsFullArr(e.arr))
            {

                allElements = allElements + GetElements(e.arr);
                e.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(e.arr, e.version);

                if(tempList.Count > 0)
                {
                    FileInfoModel[] arrTemp = tempList.ToArray();
                    e.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(arrTemp, e.version);
                }
                
            }
            else
            {
                AddListArr(e.arr);

            }

            PrintMessage();
        }


        private List<FileInfoModel> AddListArr(FileInfoModel[] arr)
        {
            if(arr != null)
            {
                foreach (FileInfoModel item in arr)
                {
                    if (item != null)
                    {
                        tempList.Add(item);
                    }
                    
                }
            }
            

            return tempList;
        }

        private void PrintMessage()
        {
            Debug.Print("WebDavEventParties->PrintMessage: Всего получили элементов формата JSON для уведомления сервера " + allElements);
        }
        private bool IsFullArr(FileInfoModel[] arr)
        {
            if(arr != null)
            {
                int size = SizeArr(arr);
                Debug.Print("WebDavEventparties->IsFullArrРазмер передаваемого массива "+ size);
                if (size >= staticVariable.Variable.CountFilesTrasferServer)
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsEmptyStack()
        {
            if(infoStack.filescount == 0)
            {
                return true;
            }

            return false;
        }
        private int SizeArr(FileInfoModel[] arr)
        {
            if (arr == null) return 0;

            return arr.Count(s => s != null);
          
        }

        private void addDataEvent(FileInfoModel[] arr)
        {
            if (arr != null)
            {
                EventScanFolderItemModel model = new EventScanFolderItemModel();
                model.arr = arr;
                OnThresholdReachedScanFolder(model);
            }

        }

        private int GetElements(FileInfoModel[] arr)
        {
            int tem = 0;
            foreach (FileInfoModel item in arr)
            {
                if(item != null) tem = tem + 1;

            }
            return tem;
        }

        protected virtual void OnThresholdReachedScanFolder(EventScanFolderItemModel e)
        {
            eventAddItem?.Invoke(this, e);
        }
    }
}
