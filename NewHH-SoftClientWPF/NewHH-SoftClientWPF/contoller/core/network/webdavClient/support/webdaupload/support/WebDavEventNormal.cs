﻿using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload.support
{
    public class WebDavEventNormal
    {
        private EventHandler<EventScanFolderItemModel> eventAddItem;

        public WebDavEventNormal(WebDavUploadFolders upload , EventHandler<EventScanFolderItemModel> eventUploadAddItem)
        {
            upload.beginEventHandler += c_BeginUpdateFolder;
            upload.updateEventHandler += c_UpdateFolder;
            upload.endEventHandler += c_EndUpdateFolder;
            this.eventAddItem = eventUploadAddItem;
        }

        public WebDavEventNormal(WebDavUploadFiles upload , EventHandler<EventScanFolderItemModel> eventUploadAddItem)
        {
            upload.beginEventHandler += c_BeginUpdateFolder;
            upload.updateEventHandler += c_UpdateFolder;
            upload.endEventHandler += c_EndUpdateFolder;
            this.eventAddItem = eventUploadAddItem;
        }


        void c_BeginUpdateFolder(object sender, UploadFolderEventHandler e)
        {
            addDataEvent(e.arr);
            e.networkJsonController.updateSendJsonSINGLBEGINUPDATEPARTIES(e.arr, e.version);
        }
        void c_UpdateFolder(object sender, UploadFolderEventHandler e)
        {
            addDataEvent(e.arr);
            e.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(e.arr, e.version);
        }
        void c_EndUpdateFolder(object sender, UploadFolderEventHandler e)
        {
            addDataEvent(e.arr);
            e.networkJsonController.updateSendJsonSINGLENDUPDATEPARTIES(e.arr, e.version);
        }

        private void addDataEvent(FileInfoModel[] arr)
        {
            if(arr != null)
            {
                EventScanFolderItemModel model = new EventScanFolderItemModel();
                model.arr = arr;
                OnThresholdReachedScanFolder(model);
            }
           
        }

        protected virtual void OnThresholdReachedScanFolder(EventScanFolderItemModel e)
        {
            eventAddItem?.Invoke(this, e);
        }

    }
}
