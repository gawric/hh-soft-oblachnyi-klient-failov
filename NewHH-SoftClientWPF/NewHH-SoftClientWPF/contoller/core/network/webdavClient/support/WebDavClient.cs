﻿using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.network.mqclient.support;
using NewHH_SoftClientWPF.contoller.network.mqserver;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.stream;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.servertopic;
using NewHH_SoftClientWPF.mvvm.model.severmodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebDAVClient;
using WebDAVClient.Model;
using WebDAVClient.Progress;
using WebDAVClient.Progress.model;


namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    public class WebDavClient
    {
        private Client client;
        private ExceptionController _exceptionController;

        public WebDavClient(ExceptionController exceptionController)
        {
            _exceptionController = exceptionController;
        }


        public async Task CreateNewConnectAsync(string username , string password)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateCertificate);
           
            try
            {
                TimeSpan uploadTimeOut = TimeSpan.FromMinutes(600);
                client = new Client(new NetworkCredential { UserName = username, Password = password } , uploadTimeOut);

                // Set basic information for WebDAV provider
                client.Server = staticVariable.Variable.getDomenWebDavServer();
                client.Port = staticVariable.Variable.getPortWebDavServer();
                client.BasePath = staticVariable.Variable.webdavnamespace;

            }
            catch(WebDAVClient.Helpers.WebDAVException)
            {
                throw;
            }
           

        }

        public Client CreateNewConnect(string username, string password)
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateCertificate);
            try
            {
                TimeSpan uploadTimeOut = TimeSpan.FromMinutes(600);
                client = new Client(new NetworkCredential { UserName = username, Password = password }, uploadTimeOut);

                // Set basic information for WebDAV provider
                client.Server = staticVariable.Variable.getDomenWebDavServer();
                client.Port = staticVariable.Variable.getPortWebDavServer();
                client.BasePath = staticVariable.Variable.webdavnamespace;
                
                return client;
            }
            catch (WebDAVClient.Helpers.WebDAVException)
            {
                throw;
            }
        }
     
        private string checkNormalizingPath(string location)
        {

            if(location.IndexOf("//") != -1)
            {
                location =  location.Replace("//" , "/");
            }

           
            return location;
        }

      
        //нужно проверить перехват
        public async Task<bool> deleteFilesAsync(string location, string username , string password , bool isFolder , Client client)
        {
               
               
                if (isFolder)
                {
                    try
                    {
                        // location = checkNormalizingPath(location);
                    
                         client.DeleteFolder(location).Wait();
                    }
                    catch(System.InvalidOperationException z)
                    {
                         generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->deleteFilesAsync: Критическая ошибка InvalidOperationException", z.ToString());
                    }
                    catch (WebDAVClient.Helpers.WebDAVException z)
                    {
                         generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->deleteFilesAsync: Критическая ошибка WebDAVException", z.ToString());
                    return false;
                    }

                    return true;
                }
            else
            {
                try
                {
                    //location = checkNormalizingPath(location);
                  
                    client.DeleteFile(location).Wait();
                }
                catch (System.InvalidOperationException d)
                {
                    Console.WriteLine(d.ToString());
                }
                catch (WebDAVClient.Helpers.WebDAVException s)
                {
                    Console.WriteLine(s);
                    return false;
                }
                catch (System.AggregateException s)
                {
                    Console.WriteLine(s.ToString());
                    return false;
                }
                catch (System.Threading.Tasks.TaskCanceledException s)
                {
                    Console.WriteLine(s);
                    return false;
                }


                client = null;

                return true;

            }
            
           


        }


       

        private async Task<bool> checkExists(string pasteLocation , string copyFolderName)
        {
            if (pasteLocation.Equals(staticVariable.Variable.nameRootLocationDisk)) return false;

            var folderFiles = await client.List(pasteLocation);
            var folder = folderFiles.FirstOrDefault(f => f.DisplayName.Equals(copyFolderName));
           
            if(folder != null)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        //нужно проверить перехват
        public async Task<bool> copyFilesAsync(bool isFolder , PasteFilesAsyncModel PasteModel , Client client)
        {

            string pasteLocation = PasteModel.pasteFolder.Location;
            string copyFolderName = PasteModel.currentCopyFolderNodes.FolderName;
            string location = PasteModel.currentCopyFolderNodes.Location;

            if (isFolder)
            {
                try
                {
                    //проверка на Disk 
                    staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pasteLocation), ref pasteLocation);

                    // client.CreateDir(pasteLocation, copyFolderName).Wait();
                    string dstPathFolder = pasteLocation + copyFolderName;

                    bool check = await checkExists(pasteLocation, copyFolderName);

                    if(check == true)
                    {
                        client.DeleteFolder(dstPathFolder).Wait();
                    }
                        
                    

                    client.CopyFolder(location , dstPathFolder).Wait();
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Копирование папки завершенно! " + location);
                    return true;
                }
                catch (System.InvalidOperationException d)
                {
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Ошибка копирования папки");
                    Console.WriteLine(d);
                    return false;
                    throw;

                }
                catch (WebDAVClient.Helpers.WebDAVException s)
                {
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Ошибка копирования папки");
                    Console.WriteLine(s);
                    return false;
                    throw;
                }
                catch (System.AggregateException s4)
                {
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Ошибка копирования папки");
                    Console.WriteLine(s4);
                    return false;
                    throw;

                }


            }
            else
            {
                try
                {
                    //проверка на Disk 
                    staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pasteLocation), ref pasteLocation);
                   
                    string dstPathFiles = pasteLocation + copyFolderName;

                    bool check = await checkExists(pasteLocation, copyFolderName);

                    if (check == true)
                    {
                        client.DeleteFile(dstPathFiles).Wait();
                    }

                  


                    client.CopyFile(location , dstPathFiles).Wait();
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Копирование файла завершенно! " + location);
                }
                catch (System.InvalidOperationException d)
                {
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Ошибка копирования файла");
                    return false;
                    throw;
                }
                catch (WebDAVClient.Helpers.WebDAVException s)
                {
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Ошибка копирования файла");
                    Console.WriteLine(s);
                    return false;
                    throw;
                }
                catch (System.AggregateException s)
                {
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> copyFilesAsync->Ошибка копирования файла");
                    Console.WriteLine(s.ToString());
                    return false;
                    throw;
                }
                catch (System.Threading.Tasks.TaskCanceledException s)
                {
                    Console.WriteLine(s);
                    return false;
                    throw;
                }


                client = null;

                return true;

            }




        }

        public void uploadFilesWebDav(string folderHref , string localFilesHref , string localFiles)
        {
            
            using (var fileStream = System.IO.File.OpenRead(localFilesHref))
            {
                IDownload down = new Download();
             


                client.Upload(down , folderHref, fileStream, localFiles).Wait();
            }
        }

        public async Task downloadFilesWebDav(Client client2 , string remotePathFiles, string localFilesHref , IDownload status , long size)
        {
            

            if (System.IO.File.Exists(localFilesHref))
            {
                //Другие FileStream при использовании жрут кучу ресурсов
                //открывает 68 потоков при копировании цельного файла на 1GB
                FileStream tempFile = File.OpenWrite(localFilesHref);

                try
                {
                    Stream stream = await client2.Download(remotePathFiles);
                    await StreamExtensionController.CopyToAsync(stream, tempFile, status, size);
                    tempFile.Dispose();
                    tempFile.Close();
                    stream.Dispose();
                    stream.Close();
                  

                }
                catch (System.Net.Http.HttpRequestException s)
                {
                    throw;
                }
                catch (System.NullReferenceException s)
                {
                    throw;
                }
            }
            else
            {
             
  
                Console.WriteLine("WebDavClient->downloadFIlesWEbDav: Статус создания файла "+ System.IO.File.Exists(localFilesHref));
                Console.WriteLine("WebDavClient->downloadFIlesWEbDav: ошибка не нашли созданый файл на компьютере!!!");
                Console.WriteLine("WebDavClient->downloadFIlesWEbDav: путь к файлу "+ localFilesHref + "Lenght файл" + localFilesHref.Length);
            }
           

           

        }









        //нужно проверить перехват
        public async Task<bool> moveFilesAsync(MoveFilesAsyncModel modelMove , Client client)
        {
            bool isFolder = modelMove.isFolder;
            string pasteLocation = modelMove.pasteLocation;
            string location = modelMove.location;
            string oldFolderName = modelMove.oldFolderName;
            string copyFolderName = modelMove.copyFolderName;


            if (isFolder)
            {
                try
                {
                   

                    staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pasteLocation),ref pasteLocation);

                    string dstPathFolder = pasteLocation + copyFolderName;
                    string dstPathFolderEscape = staticVariable.Variable.EscapeUrlString(dstPathFolder);

                    bool check = await checkExists(pasteLocation, copyFolderName);

                    if (check == true)
                    {
                        client.DeleteFolder(dstPathFolderEscape).Wait();
                    }


                    Console.WriteLine("location: "+location);
                    Console.WriteLine("dstPathFolderEscape: " + dstPathFolderEscape);
                    client.MoveFolder(location, dstPathFolderEscape).Wait();
                 
                    return true;
                }
                catch (System.InvalidOperationException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка InvalidOperationException", z.ToString());
                    throw;

                }
                catch (WebDAVClient.Helpers.WebDAVException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка WebDAVException", z.ToString());
                    throw;
                }
                catch (System.AggregateException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка AggregateException", z.ToString());
                    throw;
                }


            }
            else
            {
                try
                {
                    staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pasteLocation), ref pasteLocation);

                    string dstPathFiles = pasteLocation + copyFolderName;
                    string dstPathFilesEscape = staticVariable.Variable.EscapeUrlString(dstPathFiles);
                    bool check = await checkExists(pasteLocation, copyFolderName);

                    if (check == true)
                    {
                        client.DeleteFile(dstPathFiles).Wait();
                    }

                    client.MoveFile(location, dstPathFilesEscape).Wait();
                    Console.WriteLine("WebDavClient->getPasteFolderWebDavAsync-> moveFilesAsync->Перенос файла завершен! " + location);
                }
                catch (System.InvalidOperationException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка InvalidOperationException", z.ToString());
                    return false;
                    throw;
                }
                catch (WebDAVClient.Helpers.WebDAVException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка WebDAVException", z.ToString());
                    return false;
                    throw;
                }
                catch (System.AggregateException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка AggregateException", z.ToString());
                    return false;
                    throw;
                }
                catch (System.Threading.Tasks.TaskCanceledException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->moveFilesAsync: Критическая ошибка TaskCanceledException", z.ToString());
                    return false;
                    throw;
                }


                client = null;

                return true;

            }




        }


       

        public async Task<IEnumerable<Item>> getListFilesAsync(string location)
        {
           
            try
            {
                return await client.List(location);
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine(s1);
                return null;
            }


        }

        public async Task<Item> getFolderTest(string location)
        {

            try
            {
                return await client.GetFolder(location);
            }
            catch (WebDAVClient.Helpers.WebDAVException s1)
            {
                Console.WriteLine(s1);
                return null;
            }


        }



        public async Task<FileInfoModel[]> getAllFilesWebDavAsync(WebDavGetAllListModel model)
        {
            //каждые 1000 итераций будет скидывать инфу на сервак (нужен для подсчета итерациий)
            //создаем массив т.к примитив не передает ввиде ссылки, а передается как новый примитив(не сохроняет порядочность строя)
            int[] CountArray = {1};
            FileInfoModelSupport modelSupport = new FileInfoModelSupport();
            Dictionary<string, int> parentArray = new Dictionary<string, int>();
            FileInfoModel[] containerAllFiles = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            if (client == null) throw new  System.NullReferenceException("WebDavClient->getAllFilesWebDavAsync: Критическая ошибка не создано подключение!!!");
            IEnumerable<Item> files =  await client.List();
            //Рутовая папка откуда будет все добавляться
            containerAllFiles[0] = modelSupport.getRootFileInfoModel(model.newVersionBases);

            await allFilesRecursivAsync(files, containerAllFiles, model.updateNetworkJsonController, parentArray,  modelSupport , model.newVersionBases, CountArray , model.progress, model.username, model.supportTopicModel , model._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.autoSyncClearBasesCancellationTokenId));

            parentArray = null;

            try
            {
               
                //в самом конце в массиве остается часть не перезаписанных данных их нужно обнулить
                for(CountArray[0] = CountArray[0]; CountArray[0] <  containerAllFiles.Length; CountArray[0]++)
                {
                    containerAllFiles[CountArray[0]] = null;
                }
            }
            catch(System.IndexOutOfRangeException s)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->getAllFilesWebDavAsync: Критическая ошибка IndexOutOfRangeException", s.ToString());
            }
            catch(System.AggregateException f)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->getAllFilesWebDavAsync: Критическая ошибка System.AggregateException", f.ToString());
            }
            catch(System.NullReferenceException s)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->getAllFilesWebDavAsync: Критическая ошибка System.NullReferenceException", s.ToString());
            }
           
            return containerAllFiles;
        }

        public async Task<FileInfoModel[]> sendWebDavDeleteNotExistsFiles(long newVersionBases , UpdateNetworkJsonController networkJson, IProgress<double> progress , SupportServerTopicModel supportTopicModel, SqliteController _sqlLiteController, long[] sqlArray)
        {
            int k = 0;
            FileInfoModel[] containerAllFiles = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];

            foreach (long item in sqlArray)
            {
               

                try
                {
                    int f = k / 10;
                    progress.Report(f);

                    if (k == staticVariable.Variable.CountFilesTrasferServer)
                    {

                        k = 0;
                        networkJson.updateSendJsonLAZYSINGLUPDATEPARTIES(containerAllFiles, newVersionBases);
                        progress.Report(0);


                        for (int z = 0; z < containerAllFiles.Length; z++)
                        {
                            containerAllFiles[z] = null;
                        }
                        Thread.Sleep(1000);
                    }

                    long delete_row_id = item;

                    FileInfoModel sqlFileInfo = _sqlLiteController.getSelect().getSearchNodesByRow_IdFileInfoModel(delete_row_id);

                    //Нельзя удалить -1(это рутовая папка а именно Disk)
                    if(sqlFileInfo.row_id != -1)
                    {
                        sqlFileInfo.changeRows = "DELETE";
                        containerAllFiles[k] = sqlFileInfo;
                    }
                   
                }
                catch (System.IndexOutOfRangeException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->sendWebDavDeleteNotExistsFiles: Критическая ошибка IndexOutOfRangeException", z.ToString());
                }
                catch (System.NullReferenceException z)
                {
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->sendWebDavDeleteNotExistsFiles: Критическая ошибка NullReferenceException", z.ToString());
                }
                k++;
            }

            
            return containerAllFiles;
            
        }

        public async Task<FileInfoModel[]> getAllFilesWebDavAsyncNoDelete(long newVersionBases, UpdateNetworkJsonController networkJson, Progress<double> progress, string username, SupportServerTopicModel supportTopicModel , SqliteController _sqlLiteController , Dictionary<long, long> sqlArray , CancellationToken _cancellationToken)
        {
            //каждые 1000 итераций будет скидывать инфу на сервак (нужен для подсчета итерациий)
            //создаем массив т.к примитив не передает ввиде ссылки, а передается как новый примитив(не сохроняет порядочность строя)
            int[] CountArray = { 1 };

            //содержит все location и row_id не найденные в базе т.е новые row_id полученные из сканирования
            Dictionary<string, long> parentArray = new Dictionary<string, long>();

           
            FileInfoModel[] containerAllFiles = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            IEnumerable<Item> files = await client.List();

            FileInfoModelSupport modelSupport = new FileInfoModelSupport();
            //Рутовая папка откуда будет все добавляться
            containerAllFiles[0] = modelSupport.getRootFileInfoModel(newVersionBases);

            if (_cancellationToken.IsCancellationRequested)
            {
                return new FileInfoModel[0];
            }

            await allFilesRecursivAsyncNoDelete(files, containerAllFiles, networkJson, parentArray, modelSupport, newVersionBases, CountArray, progress, username, supportTopicModel ,  _sqlLiteController , sqlArray , _cancellationToken);

           

            try
            {

                //в самом конце в массиве остается часть не перезаписанных данных их нужно обнулить
                for (CountArray[0] = CountArray[0]; CountArray[0] < containerAllFiles.Length; CountArray[0]++)
                {
                    containerAllFiles[CountArray[0]] = null;
                }
            }
            catch (System.IndexOutOfRangeException s)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavClient->getAllFilesWebDavAsyncNoDelete: Критическая ошибка IndexOutOfRangeException", s.ToString());
            }
            parentArray = null;

            return containerAllFiles;
        }
       // int fs = 0;
        //FileInfoModelSupport(class) обработка полученных от webDavClient файлов и перевод их в массив FileInfoFolder
        public async Task allFilesRecursivAsyncNoDelete(IEnumerable<Item> files, FileInfoModel[] containerAllFiles, UpdateNetworkJsonController networkJson, Dictionary<string, long> parentArray, FileInfoModelSupport modelSupport, long newVersionBases, int[] CountArray, Progress<double> progress, string username, SupportServerTopicModel supportTopicModel , SqliteController _sqlLiteController , Dictionary<long, long> sqlArray , CancellationToken _cancellationToken)
        {

            foreach (WebDAVClient.Model.Item modelWebDav in files)
            {
               // Console.WriteLine("Отменить операцию ? "+ _cancellationToken.IsCancellationRequested);
                if(_cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                //modelWebDav.ContentType = modelSupport.ConvertDisplayNameToType(modelWebDav.DisplayName, checkFolder);
                //ContentLenght - null если папка
                //ContentLenght - 0 и выше файл
                if (modelWebDav.ContentLength == null)
                {
                    modelWebDav.ContentType = "folder";
                  //  modelSupport.convertModelToFileInfoModelNoDelete( containerAllFiles, networkJson, modelWebDav, modelSupport, newVersionBases, CountArray, username, progress, supportTopicModel ,  _sqlLiteController , sqlArray,  parentArray);
                    IEnumerable<Item> files2 = await client.List(modelWebDav.Href);

                    await allFilesRecursivAsyncNoDelete(files2, containerAllFiles, networkJson, parentArray, modelSupport, newVersionBases, CountArray, progress, username, supportTopicModel ,  _sqlLiteController , sqlArray , _cancellationToken);
                    //Console.WriteLine("Кол-во сканируемых файлов "+ fs++);
                }
                else
                {
                    modelWebDav.ContentType = System.IO.Path.GetExtension(modelWebDav.DisplayName);
                   // modelSupport.convertModelToFileInfoModelNoDelete(containerAllFiles, networkJson, modelWebDav, modelSupport, newVersionBases, CountArray, username, progress, supportTopicModel , _sqlLiteController , sqlArray,  parentArray);
                   // Console.WriteLine("Кол-во сканируемых файлов " + fs++);
                }

            }

        }


        //FileInfoModelSupport(class) обработка полученных от webDavClient файлов и перевод их в массив FileInfoFolder
        public async Task allFilesRecursivAsync(IEnumerable<Item> files , FileInfoModel[] containerAllFiles, UpdateNetworkJsonController networkJson,  Dictionary<string, int>  parentArray, FileInfoModelSupport modelSupport , long newVersionBases , int[] CountArray, Progress<double>progress ,  string username , SupportServerTopicModel supportTopicModel , CancellationToken tokenCancel)
        {
           
            foreach (WebDAVClient.Model.Item modelWebDav in files)
            {

                if(tokenCancel.IsCancellationRequested)
                {
                    files = null;
                    parentArray = null;
                    break;
                }
                
                //modelWebDav.ContentType = modelSupport.ConvertDisplayNameToType(modelWebDav.DisplayName, checkFolder);
                //ContentLenght - null если папка
                //ContentLenght - 0 и выше файл
                if (modelWebDav.ContentLength == null)
                {
                    modelWebDav.ContentType = "folder";
                    modelSupport.convertModelToFileInfoModel(parentArray, containerAllFiles, networkJson ,modelWebDav, modelSupport , newVersionBases , CountArray , username , progress, supportTopicModel);
                    IEnumerable<Item> files2 = await client.List(modelWebDav.Href);
                    await allFilesRecursivAsync(files2, containerAllFiles, networkJson, parentArray, modelSupport , newVersionBases , CountArray , progress, username , supportTopicModel , tokenCancel);
                    staticVariable.Variable.allCountAutoScanInfo = staticVariable.Variable.allCountAutoScanInfo + 1;
                }
                else
                {
                    modelWebDav.ContentType = System.IO.Path.GetExtension(modelWebDav.DisplayName);
                    modelSupport.convertModelToFileInfoModel(parentArray, containerAllFiles, networkJson , modelWebDav, modelSupport , newVersionBases , CountArray, username , progress, supportTopicModel);
                    staticVariable.Variable.allCountAutoScanInfo = staticVariable.Variable.allCountAutoScanInfo + 1;
                }

            }
        
        }

        public Client getConnect()
        {
            return client;
        }

        private static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            Console.WriteLine("Issued by: " + certificate.Issuer);
            Console.WriteLine("Issued to: " + certificate.Subject);
            Console.WriteLine("Valid until: " + certificate.GetExpirationDateString());

            if (sslPolicyErrors == SslPolicyErrors.None)
                Console.WriteLine("Valid server certificate");


            return true;
        }


        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }




    }


}
