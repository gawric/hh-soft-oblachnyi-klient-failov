﻿using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support
{
    public interface IInfoStak
    {
        InfoStackModel GetInfoStak(ref BlockingCollection<StackNetworkModel> dirs);
    }
}
