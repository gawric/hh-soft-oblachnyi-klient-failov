﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.util.infosystem;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using SimpleInjector;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support.StackRun;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.stack
{
    public class StackOperationUpDown
    {
        private BlockingCollection<StackNetworkModel> dirs;
        private SqliteController sqlLiteController;
        private ViewFolderViewModel viewModel;
        private MainWindowViewModel viewMain;
        private WebDavClientController webDavClient;
        private NavigationMainWindowsController navigationMainController;
        private bool isStopStack = false;
        private bool isRunningStack = false;
        private ConvertIconType convertIcon;
        private UpdateViewTransferController updateTransferController;
        private StackRun stackRun;
        private SupportUpDownStack suds;
        private bool isSubscrible = false;


        public StackOperationUpDown(Container cont)
        {
            
            dirs = new BlockingCollection<StackNetworkModel>();

             
            this.sqlLiteController = cont.GetInstance<SqliteController>();
            this.webDavClient = cont.GetInstance<WebDavClientController>();
            this.navigationMainController = cont.GetInstance<NavigationMainWindowsController>();
            this.convertIcon = cont.GetInstance<ConvertIconType>();
            this.updateTransferController = cont.GetInstance<UpdateViewTransferController>();
            this.viewMain = cont.GetInstance<MainWindowViewModel>();
            this.viewModel = cont.GetInstance<ViewFolderViewModel>();

            stackRun = new StackRun();
            suds = new SupportUpDownStack(sqlLiteController, updateTransferController, convertIcon, viewMain);

            subscribleEventRebuildingSqlSize(ref suds, ref stackRun);

            StakRunObjectModel model = suds.createModel(sqlLiteController, webDavClient, navigationMainController, viewModel, viewMain);
            
            startStack(model);
        }

        private void subscribleEventRebuildingSqlSize(ref SupportUpDownStack suds , ref StackRun stackRun)
        {
            stackRun.eventRebuilding += suds.rebuildingSqlSize;
            
        }

      
      
        public void addStackUpload(string[] selectHref , FileInfoModel pasteFolder)
        {
            StackNetworkModel modelUpload = new StackNetworkModel();
            modelUpload.hrefUpload = selectHref;
            modelUpload.pasteFolder = pasteFolder;
            modelUpload.dictHrefUploadRowId = new Dictionary<string, long>();
            modelUpload.isUploadScanFolder = false;
            suds.createUploadTempPb(selectHref , ref modelUpload);

            dirs.Add(modelUpload);
            
        }
        public void addStackUploadScanFolder(string[] selectHref, FileInfoModel pasteFolder)
        {
            StackNetworkModel modelUpload = new StackNetworkModel();
            modelUpload.hrefUpload = selectHref;
            modelUpload.pasteFolder = pasteFolder;
            modelUpload.dictHrefUploadRowId = new Dictionary<string, long>();
            modelUpload.isUploadScanFolder = true;

            suds.createUploadTempPb(selectHref, ref modelUpload);
            subscribleScanFolderEvent(ref webDavClient, modelUpload.isUploadScanFolder);

            dirs.Add(modelUpload);

        }

        public void addStackDownload(List<FolderNodes> listDownload , bool isViewFolder)
        {
            StackNetworkModel modelDownload = new StackNetworkModel();
            modelDownload.listDownload = listDownload;
            modelDownload.dictHrefDownloadRowId = new Dictionary<string, long>();
            modelDownload.isViewFolder = isViewFolder;
            modelDownload.countDownload = listDownload.Count;
            createDownloadTempPb(ref listDownload , ref modelDownload);
            dirs.Add(modelDownload);

         
        }

        private void createDownloadTempPb(ref List<FolderNodes> listNodes , ref StackNetworkModel modelDownload)
        {
            foreach(FolderNodes item in listNodes)
            {
                long row_id = suds.createTempProgress("download", item.FolderName, item.Type);
                modelDownload.dictHrefDownloadRowId.Add(item.Location , row_id);
            }
            
        }

        private void startStack(StakRunObjectModel modelObject)
        {
            if (!isRunningStack)
            {
                isRunningStack = true;
                Task.Run(() => stackRun.startStackNetwork(ref  dirs, ref  isRunningStack, ref  isStopStack,  modelObject));
            }
        }

        
        public void setStopStack(bool isStop)
        {
            isStopStack = isStop;
        }


        private void subscribleScanFolderEvent(ref WebDavClientController _webDavClient, bool isAutoScanFolder)
        {
            if (isAutoScanFolder)
            {
                if(!isSubscrible)
                {
                    isSubscrible = true;
                    SupportUpDownStack suds = new SupportUpDownStack(sqlLiteController, updateTransferController, convertIcon, viewMain);
                    _webDavClient.getWebDavUpload().eventAddItem += suds.addItemScanFolder;
                }
               
            }

        }


    }
}

