﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.stack;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.stack.support
{
    public class InfoStak: IInfoStak
    {

        public InfoStackModel GetInfoStak(ref BlockingCollection<StackNetworkModel>  dirs)
        {
            StackNetworkModel[] arrData = dirs.ToArray<StackNetworkModel>();
            int count = GetCountStak(ref dirs);
            int folder = GetCountFolder(arrData);
            int files = GetCountFiles(arrData);
            arrData = null;
            return CreateModel(count , folder , files);

        }

        private InfoStackModel CreateModel(int count , int folder , int files)
        {
            InfoStackModel model = new InfoStackModel();
            model.allcount = count;
            model.foldercount = folder;
            model.filescount = files;
            
            return model;
        }

        private int GetCountFolder(StackNetworkModel[] arrData)
        {
            int count = 0;
            foreach (StackNetworkModel model in arrData)
            {
                string[] uploadHref = model.hrefUpload;

                foreach(string href in uploadHref)
                {
                    if(IsFolder(href))
                    {
                        count++;
                    }
                }
            }

            return count;
        }
        private int GetCountFiles(StackNetworkModel[] arrData)
        {
            int count = 0;
            foreach (StackNetworkModel model in arrData)
            {
                string[] uploadHref = model.hrefUpload;

                foreach (string href in uploadHref)
                {
                    if (IsFiles(href))
                    {
                        count++;
                    }
                }
            }

            return count;
        }
        private int GetCountStak(ref BlockingCollection<StackNetworkModel> _dirs)
        {
            return _dirs.Count;
        }

        private bool IsFolder(string hrefUpload)
        {
            if(Directory.Exists(hrefUpload))
            {
                return true;
            }
            return false;
        }

        private bool IsFiles(string hrefUpload)
        {
            if (File.Exists(hrefUpload))
            {
                return true;
            }
            return false;
        }
    }
}
