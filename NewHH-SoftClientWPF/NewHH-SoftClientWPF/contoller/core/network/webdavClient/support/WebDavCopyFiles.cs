﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    class WebDavCopyFiles
    {
        private CreateModel createModel;
        private ExceptionController _exceptionController;

        public WebDavCopyFiles(ConvertIconType convertIcon , ExceptionController exceptionController)
        {
            createModel = new CreateModel(convertIcon);
            _exceptionController = exceptionController;
        }
        public void MoveWebDavAndJson(PasteFilesAsyncModel PasteModel ,  bool isMove, WebDavClient _webdavclient , long[][][] allChildrenList)
        {
            try
            {
                CopyViewModel status = PasteModel.status;
                IProgress<CopyViewModel> progress = PasteModel.progress;
                ScanSqliteAndSendJson sqlistScanAndSendJson = new ScanSqliteAndSendJson();
                //Отправляем их на сервер для обработки отсортированный 
                sqlistScanAndSendJson.StartSendPaste(allChildrenList, PasteModel);

                status.StatusWorker = "Переносим файлы";
                progress.Report(status);

                StartPasteOrMoveWebDavClient(PasteModel, isMove, _webdavclient).Wait();
            }
            catch(Exception z)
            {
                generatedError((int)CodeError.NETWORK_ERROR, "WebDavCopyFiles->MoveWebDavAndJson Критическая ошибка: Exception", z.ToString());
            }
            
        }
        

        public void CopyWebDavAndJson(PasteFilesAsyncModel PasteModel  , bool isMove, WebDavClient _webdavclient, long[][][] allChildrenList)
        {

            CopyViewModel status = PasteModel.status;
            IProgress<CopyViewModel> progress = PasteModel.progress;

            ScanSqliteAndSendJson sqlistScanAndSendJson = new ScanSqliteAndSendJson();
            //Отправляем их на сервер для обработки отсортированный 
            sqlistScanAndSendJson.StartSendPaste(allChildrenList, PasteModel);

            status.StatusWorker = "Копирование файлов";
            progress.Report(status);
            //Начинаем копирование
            StartPasteOrMoveWebDavClient(PasteModel, isMove, _webdavclient).Wait();


            status.StatusWorker = "Копирование завершенно";
            progress.Report(status);
        }

        

    private async Task StartPasteOrMoveWebDavClient(PasteFilesAsyncModel PasteModel ,  bool isMove , WebDavClient _webdavclient)
    {

            Dictionary<FolderNodes, bool> allCopyNodes = PasteModel.allCopyNodes;

                foreach (var item in allCopyNodes)
                {
                    PasteModel.currentCopyFolderNodes = item.Key;
                    Start(PasteModel, isMove, _webdavclient);
                }
    }

    private void Start(PasteFilesAsyncModel PasteModel, bool isMove, WebDavClient _webdavclient)
    {
            Client client = _webdavclient.CreateNewConnect(PasteModel.username, PasteModel.password);

            try
            {
                if (PasteModel.currentCopyFolderNodes.Location!= null)
                {
                    if (PasteModel.copyLocation.Equals("") != true)
                    {

                        //перемещать или копировать
                        if (isMove)
                        {
                            Console.WriteLine("WebDavClientController -> StartPasteWebDavClient -> Перенос файла " + PasteModel.currentCopyFolderNodes);

                            if (staticVariable.Variable.isFolder(PasteModel.type))
                            {
                                MoveFilesAsyncModel MoveFilesModel = createModel.convertRenameModelToMoveFilesAsyncModel(true, PasteModel.currentCopyFolderNodes.FolderName, PasteModel.currentCopyFolderNodes.Location, PasteModel);
                                _webdavclient.moveFilesAsync(MoveFilesModel, client).Wait();
                            }
                            else
                            {
                                MoveFilesAsyncModel MoveFilesModel = createModel.convertRenameModelToMoveFilesAsyncModel(false, PasteModel.currentCopyFolderNodes.FolderName, PasteModel.currentCopyFolderNodes.Location, PasteModel);
                                _webdavclient.moveFilesAsync(MoveFilesModel, client).Wait();
                            }

                        }
                        else
                        {
                            Console.WriteLine("WebDavClientController -> StartPasteWebDavClient -> Копирование файла " + PasteModel.copyLocation);

                            if (staticVariable.Variable.isFolder(PasteModel.type))
                            {
                                _webdavclient.copyFilesAsync(true, PasteModel, client).Wait();
                            }
                            else
                            {
                                _webdavclient.copyFilesAsync(false, PasteModel, client).Wait();
                            }

                        }


                    }
                    else
                    {
                       // Console.WriteLine("WebDavClientController -> StartPasteWebDavClient ->  Ошибка копирования файла location не найден " + PasteModel.copyLocation);

                        generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> Start ->  Ошибка копирования файла location не найден", "Path файла "+ PasteModel.copyLocation);
                    }
                }
                else
                {
                    //Console.WriteLine("WebDavClientController -> StartPasteWebDavClient ->  Ошибка Копирования файла  location не найден" + PasteModel.copyLocation);
                    generatedError((int)CodeError.NETWORK_ERROR, "WebDavClientController -> Start ->  Ошибка копирования файла location не найден", "Path файла " + PasteModel.copyLocation);
                }


            }
            catch (AggregateException)
            {
                throw;
            }
            catch (WebDAVClient.Helpers.WebDAVException)
            {
                throw;

            }
            catch (System.InvalidOperationException)
            {
                throw;
            }

            //Console.WriteLine("Копирование  завершенно! " + copyFolderlocation);
        }
        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            _exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }
    }
}
