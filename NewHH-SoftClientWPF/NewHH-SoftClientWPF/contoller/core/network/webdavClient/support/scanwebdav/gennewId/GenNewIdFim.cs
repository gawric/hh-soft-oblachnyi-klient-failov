﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.staticVariable;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.genNewId
{
    public class GenNewIdFim
    {
       
        public void genNewLocation(ref string g3, ref FileInfoModel[] arr, ref int cnt, ref FileInfoModel currentNodes, ref PasteFilesAsyncModel pasteModel , string pasteLocation)
        {
            try
            {
                //если g3 равен 0 то это корневая папка
                if (g3.Length != 0)
                {

                    if (staticVariable.Variable.isFolder(arr[cnt].type))
                    {
                        arr[cnt].location = generatedNewLocation(currentNodes, pasteModel , pasteLocation);
                    }
                    else
                    {
                        //здесь кроется главная проблема
                        //когда переносишь папку с файлами (получается путь \имя файла а нужно имя папки + \ + имя файла)
                        //а когда перемещаешь только файлы (получается путь только имя файла в нужную папку)

                        // string newlocation = currentNodes.location.Replace(pasteModel.copyOrCutRootLocation, ""); 
                        // newlocation = pasteModel.pasteFolder.Location + newlocation;
                        // currentNodes.location = newlocation;

                        arr[cnt].location = generatedNewLocation(currentNodes, pasteModel , pasteLocation);

                    }

                }
                else
                {
                    arr[cnt].location = convertLocationType(arr[cnt].type, currentNodes, pasteModel);
                }
            }
            catch(System.IO.FileNotFoundException a)
            {
                Debug.Print("GenNewIdFin->genNewLocation: Критическая ошибка "+ a.ToString());
            }
           

            Console.WriteLine();
        }

        private string convertLocationType(string type, FileInfoModel currentNodes, PasteFilesAsyncModel pasteModel)
        {
            if (staticVariable.Variable.isFolder(type))
            {
                string encoded_url_name = Variable.EscapeUrlString(currentNodes.filename);
                string pastelocation = pasteModel.pasteFolder.Location;
                string convertPasteLocation = staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pastelocation), ref pastelocation);
                return convertPasteLocation + encoded_url_name + "/";
            }
            else
            {
                string encoded_url_name = Variable.EscapeUrlString(currentNodes.filename);
                string pastelocation = pasteModel.pasteFolder.Location;
                string convertPasteLocation = staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pastelocation), ref pastelocation);
                return convertPasteLocation + encoded_url_name;
            }
        }


        private string generatedNewLocation(FileInfoModel currentNodes, PasteFilesAsyncModel pasteModel , string pasteLocation)
        {
            string newlocation = currentNodes.location.Replace(pasteModel.copyOrCutRootLocation, "");
       
            staticVariable.UtilMethod.getRootLocation(staticVariable.UtilMethod.isDisk(ref pasteLocation), ref pasteLocation);
            newlocation = pasteLocation + newlocation;
            currentNodes.location = newlocation;

            return newlocation;
        }
    }
}
