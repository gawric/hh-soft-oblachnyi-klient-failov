﻿using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.uploadModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.scanupload
{
    public class SupportScanUpload
    {
        bool[] stop;
        private Client client;
        public SupportScanUpload(ref bool[] stop , Client client)
        {
            this.stop = stop;
            this.client = client;
        }
     

        public async Task multiUpdate(ScanUploadWebDavModel uploadFolderModel, IDirectoryInfoProxy fi, int[] cont, IDirectoryInfoProxy di)
        {
            FileInfoModel sourceModel = createNewFileInfoModel(uploadFolderModel, fi);


            bool[] isExistSqllite = { false };
            isExistSqllite[0] = uploadFolderModel.checkParent.checkPasteFolderToDouble(sourceModel.location, uploadFolderModel.allHrefFolder);


            await createFolder(isExistSqllite, sourceModel, uploadFolderModel, cont);

            updatePb(uploadFolderModel);
            cancel(ref uploadFolderModel,ref  di, ref fi, ref stop);
        }

        public void cancel(ref ScanUploadWebDavModel uploadFolderModel, ref IDirectoryInfoProxy di, ref IDirectoryInfoProxy fi, ref bool[] stop)
        {
            CancellationToken cancellation = uploadFolderModel._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.uploadCancelationTokenId);

            if (cancellation.IsCancellationRequested)
            {
                stop[0] = true;
                di = null;
                fi = null;
                uploadFolderModel.allFolderDictionary = null;
                uploadFolderModel.allHrefFolder = null;

            }
        }


        private FileInfoModel createNewFileInfoModel(ScanUploadWebDavModel uploadFolderModel, IDirectoryInfoProxy di)
        {
            WebDavHrefModel hrefModel = new WebDavHrefModel();

            hrefModel.changeDate = di.LastWriteTime().ToString();
            hrefModel.lastOpenDate = di.LastAccessTime().ToString();
            hrefModel.size = 0;
            hrefModel.createDate = di.CreationTime().ToString();
            hrefModel.filename = di.Name();

            return createFileInfoModelToFolder(uploadFolderModel, hrefModel);
        }

        public async Task singleUpdate(ScanUploadWebDavModel uploadFolderModel, IDirectoryInfoProxy fi, int[] cont)
        {

            //Обновляем счетчик в progress
            insetCountToProgressBarFolder(uploadFolderModel);


            FileInfoModel sourceModel = createNewFileInfoModel(uploadFolderModel, fi);

            bool[] isExistSqllite = { false };


            isExistSqllite[0] = uploadFolderModel.checkParent.checkPasteFolderToDouble(sourceModel.location, uploadFolderModel.allHrefFolder);


            await createFolder(isExistSqllite, sourceModel, uploadFolderModel, cont);

            uploadFolderModel.currentCountFilesToFolder[0]++;

            cancel(ref uploadFolderModel, ref fi, ref fi, ref stop);
        }

        private void updatePb(ScanUploadWebDavModel uploadFolderModel)
        {
            uploadFolderModel.currentCountFilesToFolder[0]++;

            //Обновляем счетчик в progress
            insetCountToProgressBarFolder(uploadFolderModel);
        }

        private async Task createFolder(bool[] isExistSqllite, FileInfoModel sourceModel, ScanUploadWebDavModel uploadFolderModel, int[] cont)
        {
            //Блокировка потока что-бы можно было создавать паузу
            uploadFolderModel._blockingEventController.waitOneMRE(staticVariable.Variable.blockingUploadTokenId);

            //есть ли такая папка в нашей базе данных = false нету 
            if (isExistSqllite[0] != true)
            {
                sendJsonUpdate(cont, uploadFolderModel.verison_dabases, uploadFolderModel);
                createDirectoryToWebDav(uploadFolderModel);
                insertArr(uploadFolderModel, cont, sourceModel);
                cont[0]++;
            }
            else
            {
                bool isExistWebDav = await uploadFolderModel.checkParent.isExistsWebDavClient(sourceModel.location, uploadFolderModel.webDavExist);

                //если запись в нашей базе есть но на сервер нет
                if (!isExistWebDav)
                {
                
                    //создаем
                    createDirectoryToWebDav(uploadFolderModel);

                    //удаляем из нашей базы и в будущем пересоздаем
                    sendDeleteJson(uploadFolderModel, sourceModel);

                    //добавляем для будущей отправки на сервер и пересоздание
                    insertArr(uploadFolderModel, cont, sourceModel);
                }

            }
        }


        private FileInfoModel createFileInfoModelToFolder(ScanUploadWebDavModel uploadModel, WebDavHrefModel hrefModel)
        {
            FileInfoModel model = new FileInfoModel();
            model.filename = hrefModel.filename;
            model.lastOpenDate = hrefModel.lastOpenDate;
            model.createDate = hrefModel.createDate;
            model.changeDate = hrefModel.changeDate;

          
            //Генерирует новые id номера
            model.row_id = staticVariable.Variable.generatedNewRowId(getLastRowId(uploadModel), null);
            insertTempIdex(uploadModel);

            model.type = "folder";
            model.parent = uploadModel.pasteModel.row_id;
            model.location = uploadModel.pasteModel.location + staticVariable.Variable.EscapeUrlString(hrefModel.filename);
            model.sizebyte = hrefModel.size;
            model.changeRows = "CREATE";
            model.attribute = "";
            model.versionUpdateBases = uploadModel.verison_dabases;
            //генерация родителя
            string parentHref = CreateNewDirectoryToWebDav(uploadModel);
            //создание нового пути
            model.location = parentHref + staticVariable.Variable.EscapeUrlString(hrefModel.filename) + "/";
            model.parent = searchParentFileInfo(uploadModel, parentHref, model);
            model.user_id = getListFiles(uploadModel);

            return model;
        }

        private void insertTempIdex(ScanUploadWebDavModel uploadModel)
        {
            if(uploadModel._sqlLiteController != null) uploadModel._sqlLiteController.getInsert().insertListFilesTempIndex(staticVariable.Variable.getIdDatabses());
        }
        private long getLastRowId(ScanUploadWebDavModel uploadModel)
        {
            long row_id = 0;
            if (uploadModel._sqlLiteController != null) row_id = uploadModel._sqlLiteController.getSelect().getLastRow_idFileInfoModel();

            return row_id;
        }

        private long getListFiles(ScanUploadWebDavModel uploadModel)
        {
           // long row_id = 0;
            if(uploadModel._sqlLiteController != null)return   uploadModel._sqlLiteController.getSelect().existUserIdToListFiles();

            return 0;
        }
     

        //allFolderDictionary - хранятся все папки найденные в рутовой папке
        //что-бы файлы могли найти своих родителей
        private long searchParentFileInfo(ScanUploadWebDavModel uploadModel, string parenthref, FileInfoModel model)
        {
            

            if(uploadModel._sqlLiteController != null)
            {
                FileInfoModel parentSql = uploadModel._sqlLiteController.getSelect().getSearchNodesByLocation(parenthref);

                if (parentSql != null)
                {
                    if (uploadModel.allFolderDictionary.ContainsKey(parenthref) != true)
                    {
                        uploadModel.allFolderDictionary.Add(parenthref, parentSql.row_id);
                    }

                    if (uploadModel.allFolderDictionary.ContainsKey(model.location) != true)
                    {
                        uploadModel.allFolderDictionary.Add(model.location, model.row_id);
                    }


                    return parentSql.row_id;
                }
                else
                {

                    if (uploadModel.allFolderDictionary.ContainsKey(model.location) != true)
                    {
                        uploadModel.allFolderDictionary.Add(model.location, model.row_id);
                    }



                    return uploadModel.allFolderDictionary[parenthref];
                }
            }
            else
            {
                return 0;
            }
            
        }


        private void insetCountToProgressBarFolder(ScanUploadWebDavModel uploadFolderModel)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = uploadFolderModel.currentCountFilesToFolder[0];
            args.sizeFilesFolder = uploadFolderModel.allCountFilesToFolder;
            args.training = true;

            if (uploadFolderModel._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.uploadCancelationTokenId).IsCancellationRequested)
            {
                args.isStop = true;
            }

            //отправка в обработку
            uploadFolderModel.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        private string CreateNewDirectoryToWebDav(ScanUploadWebDavModel uploadFolderModel)
        {
            //полный путь к папке изменяем все на unix систему
            string clearLocalHref = uploadFolderModel.clientLocalDirectory.Replace("\\", "/");

            //папка откуда все копируется
            string clearRootHref = uploadFolderModel.clientRootLocalDirectory.Replace("\\", "/");
            //находим последнее вхождение обычно это имя файла
            int indexSl = clearLocalHref.LastIndexOf("/") + 1;

            //отсикаем конечное название оставляем путь до parent папки
            string clearLocalHrefNewEnd = clearLocalHref.Substring(0, indexSl);

            //находим главную папку где нажали кнопку копировать и меняем на рутовый путь сервера
            string newhref = clearLocalHrefNewEnd.Replace(clearRootHref, uploadFolderModel.pasteRootWebDavDirectory);


            return staticVariable.Variable.EscapeUrlString(newhref);
        }


        private void createDirectoryToWebDav(ScanUploadWebDavModel uploadFolderModel)
        {
            string newWebDavDirectory = CreateNewDirectoryToWebDav(uploadFolderModel);
            try
            {
                if(client != null) client.CreateDir(newWebDavDirectory, uploadFolderModel.folderName).Wait();

            }
            catch (Exception df)
            {
                Console.WriteLine("ScanUploadDirectory->CreateDirectoryToWebDav-> " + df.ToString());
            }

        }

        //запускать только после проверки на 1000 индекс у arr массива
        private void insertArr(ScanUploadWebDavModel uploadFolderModel, int[] cont, FileInfoModel sourceModel)
        {
            uploadFolderModel.arr[cont[0]] = sourceModel;
        }


        private void sendDeleteJson(ScanUploadWebDavModel uploadFolderModel, FileInfoModel sourceModel)
        {
            FileInfoModel[] arr = new FileInfoModel[staticVariable.Variable.CountFilesTrasferServer];
            long versionBases = uploadFolderModel._sqlLiteController.getSelect().getSqlLiteVersionDataBasesClient();
            arr[0] = sourceModel;
            uploadFolderModel.networkJsonController.updateSendJsonDELETEBEGINPARTIES(arr, versionBases);
            uploadFolderModel.networkJsonController.updateSendJsonDELETEENDPARTIES(arr, versionBases);
        }

        private void sendJsonUpdate(int[] cnt, long version, ScanUploadWebDavModel uploadModel)
        {
            if (cnt[0] == staticVariable.Variable.CountFilesTrasferServer)
            {


                //uploadModel.networkJsonController.updateSendJsonSINGLUPDATEPARTIES(uploadModel.arr, version);
                AddUpdateEvent(version, uploadModel.arr, uploadModel);
                for (int s = 0; s < uploadModel.arr.Length; s++)
                {
                    uploadModel.arr[s] = null;
                }

                cnt[0] = 0;

            }
        }


        public void AddUpdateEvent(long version, FileInfoModel[] arr, ScanUploadWebDavModel uploadModel)
        {
            UploadFolderEventHandler args = new UploadFolderEventHandler();
            args.version = version;
            args.arr = arr;
            args.networkJsonController = uploadModel.networkJsonController;
            OnEventUpdate(args, uploadModel);
        }

        protected virtual void OnEventUpdate(UploadFolderEventHandler e, ScanUploadWebDavModel uploadModel)
        {
            uploadModel.updateEventHandler?.Invoke(this, e);
        }


    }
}
