﻿using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.exception.support.networkException;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.scanwebdav.scanupload;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.util.filesystem.proxy;
using NewHH_SoftClientWPF.controller.error;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel.scanWebDav;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support
{
    public class ScanUploadDirectory
    {
       // private  Client client;
       // private UpdateNetworkJsonController networkJsonController;
        private ExceptionController exceptionController;
        private SupportScanUpload supportScanUpload;
        bool[] stop = { false };
        public ScanUploadDirectory(Client client, ExceptionController exceptionController)
        {
            // this.client =  client;
            // this.networkJsonController = networkJsonController;
             this.exceptionController = exceptionController;
             this.supportScanUpload = new SupportScanUpload(ref stop , client);
        }
       

        public async Task<FileInfoModel[]> uploadRecursiveScanFolderInClient(ScanUploadWebDavModel uploadFolderModel,  int[]cont)
        {
            try
            {
                IDirectoryInfoProxy di = getDirectory(uploadFolderModel.hrefFolder);

                waitOne(ref uploadFolderModel);
                supportScanUpload.cancel(ref uploadFolderModel, ref di , ref di , ref stop);
             

                if (di != null)
                {
                    foreach (DirectoryInfo fi in di.GetDirectories())
                    {
                        if(stop[0] == true)
                            break;

                        IDirectoryInfoProxy fiP = new DirectoryInfoProxy(fi.FullName);

                        if (fi.GetDirectories().Length > 0)
                        {
                            reassignFiles(fiP, ref uploadFolderModel);
                            await supportScanUpload.multiUpdate( uploadFolderModel , fiP ,  cont,  di);
                            await uploadRecursiveScanFolderInClient(uploadFolderModel , cont);

                        }
                        else
                        {
                            if(stop[0] == true)
                                break;

                            reassignFiles(fiP, ref uploadFolderModel);
                            await supportScanUpload.singleUpdate(uploadFolderModel , fiP , cont);
                        }

                    }
                }
            }
            catch(System.NullReferenceException npe)
            {
                stop[0] = true;
                //Console.WriteLine("ScanUploadDirectory->uploadRecursiveScanFolderInClient: Ошибка NPE !!! " + npe.ToString());
                generatedError((int)CodeError.NETWORK_ERROR, "ScanUploadDirectory->uploadRecursiveScanFolderInClient: Ошибка NullPointException !!!", npe.ToString());
            }
            catch (System.IndexOutOfRangeException idex)
            {
                stop[0] = true;
                //Console.WriteLine("ScanUploadDirectory->UploadRecursiveScanFolderInClient: Ошибка NPE !!! " + idex.ToString());
                generatedError((int)CodeError.NETWORK_ERROR, "ScanUploadDirectory->uploadRecursiveScanFolderInClient: Ошибка IndexOutOfRangeException !!!", idex.ToString());
            }
            catch (System.ArgumentException argument)
            {
                stop[0] = true;
                //Console.WriteLine("ScanUploadDirectory->UploadRecursiveScanFolderInClient: Ошибка System.ArgumentException !!! " + argument.ToString());
                generatedError((int)CodeError.NETWORK_ERROR, "ScanUploadDirectory->uploadRecursiveScanFolderInClient: Ошибка ArgumentException !!!", argument.ToString());
            }
            catch (System.Collections.Generic.KeyNotFoundException key)
            {
                Console.WriteLine("ScanUploadDirectory->UploadRecursiveScanFolderInClient: Ошибка System.ArgumentException !!! " + key.ToString());
                generatedError((int)CodeError.NETWORK_ERROR, "ScanUploadDirectory->uploadRecursiveScanFolderInClient: Ошибка KeyNotFoundException !!!", key.ToString());
            }

            return uploadFolderModel.arr;

        }

    
        private void waitOne(ref ScanUploadWebDavModel uploadFolderModel )
        {
            //Блокировка потока что-бы можно было создавать паузу
            uploadFolderModel._blockingEventController.waitOneMRE(staticVariable.Variable.blockingUploadTokenId);
        }
        public void reassignFiles(IDirectoryInfoProxy fiP, ref ScanUploadWebDavModel uploadFolderModel)
        {
            uploadFolderModel.hrefFolder = fiP.FullName();
            uploadFolderModel.folderName = fiP.Name();
            uploadFolderModel.clientLocalDirectory = fiP.FullName();
        }

        private NetworkException generatedError(int codeError, string textError, string trhowError)
        {
            exceptionController.sendError(codeError, textError, trhowError);
            return new NetworkException(codeError, textError, trhowError);
        }

      
      
        private IDirectoryInfoProxy getDirectory(string href)
        {
            return new DirectoryInfoProxy(href);
        }
    }
}
