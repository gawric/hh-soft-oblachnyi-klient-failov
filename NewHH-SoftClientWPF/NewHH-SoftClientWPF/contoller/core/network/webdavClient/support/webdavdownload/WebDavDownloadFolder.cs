﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.core.exception;
using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavdownload.support;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.navigation.mainwindows;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload.support;
using NewHH_SoftClientWPF.contoller.scanner;
using NewHH_SoftClientWPF.contoller.sorted;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.contoller.update.updateProgressBar;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.download.supportDownload;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.support;
using NewHH_SoftClientWPF.contoller.view.update.updateProgressBarTransfer.download.supportDownload;
using NewHH_SoftClientWPF.contoller.view.updateIcon.updateIconTreeView.stack;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.scannerrecursiveLocalFolder;
using NewHH_SoftClientWPF.mvvm.model.sqlliteRecursiveAllChildren;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDAVClient.Progress.model;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload
{
    public class WebDavDownloadFolder
    {
        private SupportDownload support;
        private SupportDownloadChangeIcon supportIcon; 

        public WebDavDownloadFolder()
        {
            support = new SupportDownload();
            supportIcon = new SupportDownloadChangeIcon();
        }

        public void trainingDownloadFolder(DownloadWebDavModel model , FolderNodes downloadNodes)
        {
            DownloadWebDavModel model2 = createNewDownloadWebDavModel(model);
            //создает папки на компьютере до места куда будем заливать
            createFolderToFolderSource(model2, downloadNodes);
            trainingDownloadFolders(model2, downloadNodes);

            cancelChangeIcon( model,  downloadNodes);
            model2 = null;
        }

      

        private void trainingDownloadFolders(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            try
            {
               

                //Array[0] - все каталоги и файлы из базы данных
                List<long[][]> allListFull = new List<long[][]>();
                string location = downloadNodes.Location;
                
                createProgress(model, downloadNodes);

                if (cancel(model))
                {
                    insetStopToProgressBarFolder(model, true, false);
                    return;
                }

                

                ScannerSqlliteRecursiveAllChildrenModel ssracm = createChildrenModel(ref allListFull, ref location, model);
                allListFull = getAllChildren(ssracm);

               // addRefresh(model, downloadNodes , allListFull);


                //получаем размер копируемой папки и кол-во файлов
                model.modelSizebyteAndCount = model._sqlliteController.getSelect().getSqlliteSizeFolderAndCount(allListFull[0]);
             
                //создаем папки  куда будем заливать все файлы
                createParentFolderToLocalFolder(allListFull[0], model, downloadNodes);
            
                startDownload(allListFull[0], model, downloadNodes);

                allListFull.Clear();
                allListFull = null;
                ssracm.allParent.Clear();
                ssracm = null;


            }
            catch (System.IO.PathTooLongException z)
            {
               // Console.WriteLine("WebDavDownload->trainingDownloadFolders->createParentFolderToLocalFolder: Критическая ошибка путь файла слишком длинный для этой файловой системы   " + z.ToString());
                model._exceptionController.sendError((int)CodeError.NETWORK_ERROR , "WebDavDownload->trainingDownloadFolders->createParentFolderToLocalFolder: Критическая ошибка путь файла слишком длинный для этой файловой системы " , z.ToString());
            }
            catch (System.IO.FileNotFoundException z)
            {
               // Console.WriteLine("WebDavDownload->trainingDownloadFolders->createParentFolderToLocalFolder: Критическая ошибка путь файла слишком длинный для этой файловой системы   " + z.ToString());
                model._exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavDownload->trainingDownloadFolders->createParentFolderToLocalFolder: Критическая ошибка путь файла слишком длинный для этой файловой системы ", z.ToString());
            }
            catch (System.IO.DirectoryNotFoundException z)
            {
                // Console.WriteLine("WebDavDownload->trainingDownloadFolders->createParentFolderToLocalFolder: Критическая ошибка путь файла слишком длинный для этой файловой системы   " + z.ToString());
                model._exceptionController.sendError((int)CodeError.NETWORK_ERROR, "WebDavDownload->trainingDownloadFolders->createParentFolderToLocalFolder: Критическая ошибка путь файла слишком длинный для этой файловой системы ", z.ToString());
            }

        }

       
      
        public List<long[][]> getAllChildren(ScannerSqlliteRecursiveAllChildrenModel ssracm)
        {
            ScannerSqlliteRecursiveAllChildren ssrach = new ScannerSqlliteRecursiveAllChildren();
            return ssrach.getScanAllChildren(ssracm);
        }

        public ScannerSqlliteRecursiveAllChildrenModel createChildrenModel(ref List<long[][]> allListFull, ref string location, DownloadWebDavModel downloadModel)
        {
            ScannerSqlliteRecursiveAllChildrenModel model = new ScannerSqlliteRecursiveAllChildrenModel();
         
            model.allListFull = allListFull;
            model.location = location;
            int[] p = { 0 };
            model.p = p;
            model._sqlLiteController = downloadModel._sqlliteController;
            model.progress = new Progress<CopyViewModel>();
            model.progressLabel = "search children"; 
            //все записи из базы
            model.allParent = downloadModel._sqlliteController.getSelect().getAllParentListRowId();
            model._mre = new ManualResetEvent(true);
            model._cancelToken = new CancellationToken();

            return model;
        }

        public bool cancel(DownloadWebDavModel model)
        {
            return model._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.downloadCancelationTokenId).IsCancellationRequested;
        }

        private DownloadWebDavModel createNewDownloadWebDavModel(DownloadWebDavModel model)
        {
            DownloadWebDavModel newModel = new DownloadWebDavModel();

            newModel._webDavclient = model._webDavclient;
            newModel.client = model.client;
            newModel.donwloadSizeFiles = model.donwloadSizeFiles;
            newModel._listDownload = model._listDownload;
            newModel.username = model.username;
            newModel.password = model.password;
            newModel.locationDownload = model.locationDownload;
            newModel._sqlliteController = model._sqlliteController;
            newModel.modelSizebyteAndCount = model.modelSizebyteAndCount;
            newModel._transferViewNodes = model._transferViewNodes;

            newModel.currentCountFiles = model.currentCountFiles;
            newModel.convertIcon = model.convertIcon;
            newModel._navigationController = model._navigationController;
            newModel._updateTransferController = model._updateTransferController;
            newModel._cancellationController = createCancelationDownload(model._cancellationController);
            newModel._blockingController = createBlockingDownload(model._blockingController);
            newModel._exceptionController = model._exceptionController;

            newModel.folderTreeViewPgEventHandle = model.folderTreeViewPgEventHandle;
            newModel._stackUpdateTreeViewIcon = model._stackUpdateTreeViewIcon;
            newModel._viewFolderModel = model._viewFolderModel;
            newModel._viewMain = model._viewMain;
            newModel.isViewVolder = model.isViewVolder;
            return newModel;


        }

       

        private CancellationController createCancelationDownload(CancellationController _cancellationController)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            _cancellationController.removeObjectWarehouse(staticVariable.Variable.downloadCancelationTokenId);
            _cancellationController.addWarehouse(staticVariable.Variable.downloadCancelationTokenId, cts);

            return _cancellationController;
        }
        private BlockingEventController createBlockingDownload(BlockingEventController _blockingEventController)
        {
            _blockingEventController.createManualResetEvent(staticVariable.Variable.blockingDownloadTokenId);
            _blockingEventController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).setMre();

            return _blockingEventController;
        }



        private void startDownload(long[][] allFiles, DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            //сбрасываем подсчет подготовки файлов
            model.currentCountFiles[0] = 0;

            //заполняем все файлы нужными данными с сервера WebDav
            downloadAllParentFiles(allFiles, model, downloadNodes);
        }

        
        private void downloadAllParentFiles(long[][] allFiles, DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            try
            {
                bool[] checkError = { false };

                for (int z = 0; z < allFiles.Length; z++)
                {
                    long row_idParent = allFiles[z][0];

                    model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).WaitOne();

                    if(cancel(model))
                    {
                        insetStopToProgressBarFolder(model, true, false);
                        break;
                    }

                   

                    FileInfoModel parentModel = model._sqlliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_idParent);
                    string localFilesHref = convertLocationToHref(parentModel.location, model, downloadNodes);

                    if (staticVariable.Variable.isFolder(parentModel.type) != true)
                    {

                        try
                        {
                           // countThread();
                            startDownload(model, parentModel, localFilesHref, checkError).Wait();
                            supportIcon.addOkViewFolderFiles(model, downloadNodes, parentModel);
                        }
                        catch(System.AggregateException a)
                        {
                            Console.WriteLine("WebDavDownloadFolder-> downloadAllParentFiles: Не удалось найти директорию "+a);
                        }
                        

                        if (checkError[0] == true)
                            break;

                        updatePb(model);
                    }
                }

                pbOkIf0Files(model.modelSizebyteAndCount.sizeCountFiles , model);
                   
            }
            catch(System.NotSupportedException g)
            {
                Console.WriteLine("WebDavDownloadFolder->downloadAllParentFiles->:  Критическая ошибка " + g);
            }
            
        }

        private void pbOkIf0Files(long sizeCountFiles , DownloadWebDavModel model)
        {
            if(sizeCountFiles == 0)
            {
                model.currentCountFiles[0] = 5;
                model.modelSizebyteAndCount.sizeCountFiles = 5;
                insetCountToProgressBarFolder(model, false, false);
            }
        }

        private async Task startDownload(DownloadWebDavModel model , FileInfoModel parentModel , string localFilesHref , bool[] checkError)
        {
            try
            {
               
                if(model.client == null) model.client = model._webDavclient.CreateNewConnect(model.username, model.password);

                SendPbDetails sendStatus = new SendPbDetails();
                subscribleDetailsPb(sendStatus, model);

                sendStatus.setRow_id(model.modelSource.Row_id);
                sendStatus.setCancel(model._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.downloadCancelationTokenId));
                sendStatus.setMre(model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).getManualResetEvent());

                
                await model._webDavclient.downloadFilesWebDav(model.client, parentModel.location, localFilesHref, sendStatus, parentModel.sizebyte);
                sendStatus = null;

            }
            catch (System.Net.Http.HttpRequestException s)
            {
                checkError[0] = true;
                updatePbError(model);
            }
            catch (System.NullReferenceException s)
            {
                checkError[0] = true;
                updatePbError(model);
            }
            catch (System.AggregateException s)
            {
                checkError[0] = true;
              //  updatePbError(model);
            }
            finally
            {
               // model.client = null;
                
            }

        }
        private void subscribleDetailsPb(SendPbDetails sendStatus , DownloadWebDavModel model)
        {
            long row_id_transfer = model.modelSource.Row_id;
            NodesPb nodesPb = new NodesPb(model._transferViewNodes , row_id_transfer, model.convertIcon);
            UpdatePbDetails update = new UpdatePbDetails(nodesPb, model.modelSource, row_id_transfer, model);

            sendStatus.Completed += new EventHandler<DownloadStateEventArgs>(update.c_StateComplete);
            sendStatus.StateChanged += new EventHandler<DownloadStateEventArgs>(update.c_StateChanged);
            sendStatus.StateError += new EventHandler<DownloadStateEventArgs>(update.c_StateError);


        }
        private void updatePb(DownloadWebDavModel model)
        {
            model.currentCountFiles[0]++;
            insetCountToProgressBarFolder(model, false, false);
        }
        private void updatePbError(DownloadWebDavModel model)
        {
            model.currentCountFiles[0]++;
            insetCountToProgressBarFolder(model, false, true);
        }

        private void createFolderToFolderSource(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            
            FileInfoModel[] arrPath = support.getPathRootToNodes(model, downloadNodes);

            //создаем папки до папки назначения //rootfolder/folder1/папка назначения//
            support.createFoldersToLocalFolder(model , arrPath, model.locationDownload, false);
        }
        

        private void createParentFolderToLocalFolder(long[][] allFiles, DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            try
            {
                model.currentCountFiles = new int[1];

                //создаем все папки и файлы пока пустые
                createParent(allFiles, model, downloadNodes);


            }
            catch (System.IO.PathTooLongException s)
            {
                insetCountToProgressBarFolder(model, true, true);
                throw;
            }
            catch (System.IO.DirectoryNotFoundException a)
            {
                insetCountToProgressBarFolder(model, true, true);
                throw;
            }

        }

        private void createProgress(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            CreateModel createModel = new CreateModel(model.convertIcon);
            //начинаем сканирование
            FolderNodesTransfer modelSource = createModel.createTransferModel(downloadNodes.Row_id, downloadNodes.FolderName, 0, "download" , "folder");

            long new_row = model._sqlliteController.getSelect().getLastRow_idFileInfoModel();
            model._sqlliteController.getInsert().insertListFilesTempIndex(new_row);
            modelSource.Row_id = staticVariable.Variable.generatedNewRowId(new_row, null) + 1;
            //добавление в listTransfer
            model._updateTransferController.createProgressBar(modelSource, modelSource.FolderName);

            //подписка на изменения
            model._updateTransferController.UpdateDownloadFolderProgressBar(model, modelSource.Row_id, downloadNodes.Row_id);

            model.currentCountFiles = new int[1];
            model.modelSizebyteAndCount = new RecursiveLocalModel();
            model.modelSizebyteAndCount.sizeCountFiles = 0;

            insetCountToProgressBarFolder(model, true, false);
            model.modelSource = modelSource;
        }

        private void createParent(long[][] allFiles, DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            try
            {
                if (isStop(model)) return;


                for (int k = 0; k < allFiles.Length; k++)
                {


                    checkPause(model);
                    FileInfoModel parentModel = getParentModel(allFiles,  model,  k);
                    string href = convertLocationToHref(parentModel.location, model, downloadNodes);

                   

                    if (staticVariable.Variable.isFolder(parentModel.type))
                    {
                       // Console.WriteLine("Проверка что файл найден и мы его обрабатываем  "+ parentModel.filename +"  его ID "+ parentModel.row_id);
                        supportIcon.addOkFolder(model, downloadNodes, parentModel);

                        if (System.IO.Directory.Exists(href) != true)
                        {
                            checkPause(model);
                            if (isStop(model)) break;

                            var mydirectory = System.IO.Directory.CreateDirectory(href);
                            
                        }

                        updatePb0Files(model);
                    }
                    else
                    {
                        checkPause(model);
                        if (isStop(model)) break;

                        if (System.IO.File.Exists(href) != true)
                        {
                               FileStream fs = File.Create(href);
                               fs.Close();
                        }

                        model.currentCountFiles[0]++;
                        insetCountToProgressBarFolder(model, true, false);
                    }


                }


            }
            catch(System.NotSupportedException a)
            {
                Console.WriteLine("WebDavDownloadFolder->createParent: Критическая ошибка!"+ a.ToString());
                throw;
            }
            catch (System.IO.DirectoryNotFoundException a)
            {
                Console.WriteLine("WebDavDownloadFolder-> createParent: Не удалось найти часть пути " + a);
               // Console.WriteLine("WebDavDownloadFolder-> createParent: Размер пути " + href.Length);
                throw;
            }
        }

     

        private bool isStop(DownloadWebDavModel model)
        {
            bool check = false;

            if (cancel(model))
            {
                insetStopToProgressBarFolder(model, true, false);
                check= true;
            }

            return check;
        }

        private void checkPause(DownloadWebDavModel model)
        {
            model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).WaitOne();
        }
       private FileInfoModel getParentModel(long[][] allFiles, DownloadWebDavModel model, int index)
       {
            long row_idParent = allFiles[index][0];
            FileInfoModel parentModel = model._sqlliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(row_idParent);

            return parentModel;

       }
        private void updatePb0Files(DownloadWebDavModel model)
        {
            //если в этой папке нет файлов
            if (model.modelSizebyteAndCount.sizeCountFiles == 0)
            {
                model.currentCountFiles[0]++;
                insetCountToProgressBarFolder(model, true, false);
            }
        }
        private void insetCountToProgressBarFolder(DownloadWebDavModel model, bool training, bool error)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = model.currentCountFiles[0];
            args.sizeFilesFolder = model.modelSizebyteAndCount.sizeCountFiles;
            args.training = training;
            args.errorStatus = error;
            //отправка в обработку
            model.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        private void insetStopToProgressBarFolder(DownloadWebDavModel model, bool training, bool error)
        {
            UpdateViewTransferFolderPgArgs args = new UpdateViewTransferFolderPgArgs();
            args.currentFilesFolder = model.currentCountFiles[0];
            args.sizeFilesFolder = model.modelSizebyteAndCount.sizeCountFiles;
            args.training = training;
            args.errorStatus = error;
            args.isStop = true;
            //отправка в обработку
            model.folderTreeViewPgEventHandle?.Invoke(this, args);
        }

        private string convertLocationToHref(string parentLocation, DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            string remoteWebDav = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;


            string hrefLocal = parentLocation.Replace(remoteWebDav, model.locationDownload + "\\");
            hrefLocal = hrefLocal.Replace("/", "\\");
          
            string hrefLocalDecode = staticVariable.Variable.DecodeUrlString(hrefLocal);


            return hrefLocalDecode;
        }


        private void cancelChangeIcon(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            if (cancel(model))
            {
                supportIcon.cancelUpdateIconCloud(model, downloadNodes);
            }
        }

        public void addRefresh(DownloadWebDavModel model, FolderNodes downloadNodes, List<long[][]> allListFull)
        {
            supportIcon.addRefresh( model,  downloadNodes,  allListFull);
        }



    }
}
