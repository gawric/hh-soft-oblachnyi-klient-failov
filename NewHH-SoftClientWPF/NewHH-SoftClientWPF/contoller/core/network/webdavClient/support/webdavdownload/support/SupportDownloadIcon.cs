﻿using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using NewHH_SoftClientWPF.mvvm.supportForm.mainForm.eventMain.other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavdownload.support
{
    public class SupportDownloadIcon
    {

        public void addRefreshPcSave(DownloadWebDavModel model, ref FolderNodes downloadNodes)
        {
          
            List<long[][]> allListFull = getAllList(model, ref downloadNodes);

            addRefresh(model, downloadNodes, allListFull);
        }

        private void addRefresh(DownloadWebDavModel model, FolderNodes downloadNodes, List<long[][]> allListFull)
        {
            deleteRefresh(model, allListFull);
            addRefreshSqllite(model, allListFull, staticVariable.Variable.refreshIconId);
            addRefreshIcon(model, downloadNodes);

            changeTypePcSqllite(model, allListFull, staticVariable.Variable.refreshIconId);
        }

        private void deleteRefresh(DownloadWebDavModel model, List<long[][]> allListFull)
        {
            model._sqlliteController.getDelete().DeletelistSaveToPc(allListFull);
        }

        private void addRefreshSqllite(DownloadWebDavModel model, List<long[][]> allListFull, int intId)
        {
            model._sqlliteController.getInsert().insertListSavePc(allListFull, intId);
        }

        private void changeTypePcSqllite(DownloadWebDavModel model, List<long[][]> allListFull, int intId)
        {
            long[][] list = allListFull[0];

            for (int a = 0; a < list.Length; a++)
            {

                if (list[a] != null)
                {
                    long row_id_listfiles = list[a][0];
                    string type = model._sqlliteController.getSelect().getSaveToPcType(row_id_listfiles);
                    string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, intId);

                    model._sqlliteController.updateListSaveToPc(row_id_listfiles, newtype, intId);
                }


            }


        }

        private void addRefreshIcon(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            List<long> list = new List<long>();
            list.Add(downloadNodes.Row_id);
            model._stackUpdateTreeViewIcon.TraverseTreeList(model._viewMain.NodesView.Items, list, staticVariable.Variable.refreshIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model._viewFolderModel.NodesView.Items, downloadNodes.Row_id, staticVariable.Variable.refreshIconId);
        }


        public void addOkFolder(DownloadWebDavModel model, FolderNodes downloadNodes, FolderNodesTransfer modelSource)
        {
            string type = model._sqlliteController.getSelect().getSaveToPcType(downloadNodes.Row_id);
            string newtype = model._stackUpdateTreeViewIcon.changeIconReturn(type, staticVariable.Variable.okIconId);
            model._sqlliteController.updateListSaveToPc(downloadNodes.Row_id, newtype, staticVariable.Variable.okIconId);
            model._stackUpdateTreeViewIcon.TraverseTreeSingl(model._viewMain.NodesView.Items, downloadNodes.Row_id, staticVariable.Variable.refreshIconId);
            model._stackUpdateTreeViewIcon.TraverseViewSingl(model._viewFolderModel.NodesView.Items, downloadNodes.Row_id, staticVariable.Variable.okIconId);

        }

        private List<long[][]> getAllList(DownloadWebDavModel model, ref FolderNodes downloadNodes)
        {
            long[] listFilesRows = { downloadNodes.Row_id, 0 };
            long[][] list = { listFilesRows };
            List<long[][]> allListFull = new List<long[][]>();
            allListFull.Add(list);

            return allListFull;
        }




    }
}
