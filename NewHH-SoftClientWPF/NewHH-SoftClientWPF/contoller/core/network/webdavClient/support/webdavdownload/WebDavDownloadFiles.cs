﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdavdownload.support;
using NewHH_SoftClientWPF.contoller.model;
using NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload.support;
using NewHH_SoftClientWPF.contoller.update.updateProgressBarTransfer.download.supportDownload;
using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.model.webDavModel;
using System.IO;
using WebDAVClient;

namespace NewHH_SoftClientWPF.contoller.network.webdavClient.support.webdavdownload
{
    public class WebDavDownloadFiles
    {

        private SupportDownload support;
  
        private SupportDownloadIcon supportDownloadIcon;

        public WebDavDownloadFiles()
        {
            this.support  = new SupportDownload();
            supportDownloadIcon = new SupportDownloadIcon();


        }

        public void trainingDownloadFiles(DownloadWebDavModel model , FolderNodes downloadNodes)
        {
            FileInfoModel[] arrPath = createFolderToFilesSource(model, downloadNodes);
            string localFilesHref = generatedEndPath(arrPath, model.locationDownload);
            createFiles(localFilesHref);

            trainingDownloadFiles(model, downloadNodes, localFilesHref);
            cancelChangeIcon(model, downloadNodes);
        }


        private void cancelChangeIcon(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            if (cancel(model))
            {
                if(downloadNodes != null)
                {
                    model._sqlliteController.getDelete().DeleteSavePcSinglRows(downloadNodes.Row_id);
                }
                
            }
        }
       

        private void createFiles(string localFilesHref)
        {
            if(!File.Exists(localFilesHref))
            {
                FileStream stm = File.Create(localFilesHref);
                stm.Close();
            }
        }
        private FileInfoModel[] createFolderToFilesSource(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            FileInfoModel[] arrPath = support.getPathRootToNodes(model, downloadNodes);
            support.createFoldersToLocalFolder(model , arrPath, model.locationDownload, true);

            return arrPath;
        }

      

        private string generatedEndPath(FileInfoModel[] arrPath, string locationDownload)
        {
            string local = locationDownload;

            foreach (FileInfoModel item in arrPath)
            {
                local += "\\" + item.filename;
            }

            return local;
        }

        private void trainingDownloadFiles(DownloadWebDavModel model, FolderNodes downloadNodes, string localFilesHref)
        {
            createClientWebDav(model);
            getSizeFileServer(model, downloadNodes);

            FolderNodesTransfer modelSource = createProgressFiles(model, downloadNodes);
            model._updateTransferController.createProgressBar(modelSource, "test");

            startDownload(model, downloadNodes, modelSource, localFilesHref);
        }



        private void createClientWebDav(DownloadWebDavModel model)
        {
            Client client = model._webDavclient.CreateNewConnect(model.username, model.password);
            model.client = client;

        }



        private void getSizeFileServer(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            FileInfoModel serverModel = model._sqlliteController.getSelect().getSearchNodesByRow_IdFileInfoModel(downloadNodes.Row_id);
            model.donwloadSizeFiles = serverModel.sizebyte;

        }



        //Добавляем в прогресс новую запись
        private FolderNodesTransfer createProgressFiles(DownloadWebDavModel model, FolderNodes downloadNodes)
        {
            CreateModel createModel = new CreateModel(model.convertIcon);
            //создание модели для вставки в ProgressBar
            FolderNodesTransfer modelSource = createModel.createTransferModel(downloadNodes.Row_id, downloadNodes.FolderName, model.donwloadSizeFiles, "download" , "files");
            modelSource.Row_id = model._sqlliteController.getSelect().getLastRow_idToListFilesTransfer() + 1;

            return modelSource;
        }

        private void startDownload(DownloadWebDavModel model, FolderNodes downloadNodes, FolderNodesTransfer modelSource, string localFilesHref)
        {
            SendProcentCopyFilesToPb sendProcent = new SendProcentCopyFilesToPb();
            model._updateTransferController.UpdateDownloadFilesProgressBar(sendProcent , model , modelSource.Row_id);

            sendProcent.setCancel(model._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.downloadCancelationTokenId));
            sendProcent.setMre(model._blockingController.getManualReset(staticVariable.Variable.blockingDownloadTokenId).getManualResetEvent());

            sendProcent.setRow_id(modelSource.Row_id);
            model._webDavclient.downloadFilesWebDav(model.client, downloadNodes.Location, localFilesHref, sendProcent, model.donwloadSizeFiles).Wait();
            supportDownloadIcon.addOkFolder(model, downloadNodes, modelSource);
        
        }

        public bool cancel(DownloadWebDavModel model)
        {
            return model._cancellationController.getCancellationTokenToWarehouse(staticVariable.Variable.downloadCancelationTokenId).IsCancellationRequested;
        }

    }
}
