﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.shell.windows
{
    //Не используется в будущем будем применять для обновления папок и файлов
    public class RefreshExplorer
    {
        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        private static extern uint SHGetSpecialFolderLocation(
            IntPtr hWnd,
            uint nFolder,
            out IntPtr Pidl);

        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        static extern void SHChangeNotify(
            int wEventId,
            uint uFlags,
            IntPtr dwItem1,
            IntPtr dwItem2);

        private const int SHCNE_ASSOCCHANGED = 0x08000000;
       // private const int SHCNE_ASSOCCHANGED = 0x7FFFFFFF;
        private const int SHCNE_UPDATEDIR = 0x00001000;
        private const int SHCNF_IDLIST = 0x0000;
        private const int CSIDL_DRIVES = 0x0011; //My computer
        //Обновляет только панели рабочего стола.
        public  void Refresh()
        {
            IntPtr pidl = IntPtr.Zero;
            Guid newGuid = Guid.Parse("{679f85cb-0220-4080-b29b-5540cc05aab6}");
            string du = newGuid.ToString();
             SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, IntPtr.Zero, IntPtr.Zero);
            //SHGetSpecialFolderLocation(IntPtr.Zero, CSIDL_DRIVES, out pidl);
            SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, pidl , IntPtr.Zero) ;
           // SHChangeNotify(SHCNE_UPDATEDIR, SHCNF_IDLIST, "shell:::{679f85cb-0220-4080-b29b-5540cc05aab6}", null);
            Console.WriteLine("Done!");
            return;
        }

        public void Refresh2()
        {
            try
            {
                Guid CLSID_ShellApplication = new Guid("13709620-C279-11CE-A49E-444553540000");
                Type shellApplicationType = Type.GetTypeFromCLSID(CLSID_ShellApplication, true);
                dynamic shellApplication = Activator.CreateInstance(shellApplicationType);
                dynamic windows = shellApplication.Windows();
                for (int i = 0; i < windows.Count; i++)
                    windows.Item(i).Refresh();
            }
            catch(System.Runtime.InteropServices.COMException a)
            {
                Console.WriteLine(a.ToString());
            }
          
        }
    }
}
