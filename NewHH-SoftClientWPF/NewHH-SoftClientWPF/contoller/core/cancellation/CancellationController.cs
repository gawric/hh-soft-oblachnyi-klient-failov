﻿using NewHH_SoftClientWPF.contoller.sorted;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.cancellation
{
    public class CancellationController
    {
        private Dictionary<string, CancellationTokenSource> warehouseObj;

        public CancellationController(Container cont)
        {
            warehouseObj = new Dictionary<string, CancellationTokenSource>();
        }

        public void addWarehouse(string nameId , CancellationTokenSource canselSource)
        {
            if(!isExistsNameId(nameId))
            {
                warehouseObj.Add(nameId, canselSource);
            }
        }

      
        public void removeObjectWarehouse(string nameId)
        {
            if(isExistsNameId(nameId))
            {
                warehouseObj.Remove(nameId);
            }
        }

        public void startCansel(string nameId)
        {
            if (isExistsNameId(nameId))
            {
                warehouseObj[nameId].Cancel();
                //Console.WriteLine();
            }
            else
            {
                //Console.WriteLine("CancellationController->startCansel: Отменить сканирование не удалось! не найден nameId");
            }
        }

        public void startAllCansel()
        {
            foreach (var item in warehouseObj) { if (item.Value != null) item.Value.Cancel(); };
        }

        public CancellationToken getCancellationTokenToWarehouse(string nameId)
        {
            if (isExistsNameId(nameId))
            {
               return  warehouseObj[nameId].Token;
            }
            else
            {
                return new CancellationToken();
            }
        }

        private bool isExistsNameId(string nameId)
        {
            if (warehouseObj.ContainsKey(nameId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
