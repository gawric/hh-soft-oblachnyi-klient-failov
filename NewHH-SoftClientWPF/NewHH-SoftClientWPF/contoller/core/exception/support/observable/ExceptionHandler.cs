﻿using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.error.support.observable
{
    
   public class ExceptionHandler
   {

            public string Name { get; set; }


            public event EventHandler OnSaved;


            public ExceptionHandler(string name)
            {
                if (string.IsNullOrEmpty(name)) throw new ArgumentNullException(nameof(name));
                Name = name;
            }


            public void updateError(int errorCode, string errorText, string throwError)
            {

                CustomErrorEventArgs sutom = new CustomErrorEventArgs( errorCode,  errorText,  throwError);
                OnSaved?.Invoke(this, sutom);
            }


            public override string ToString()
            {
                return Name;
            }
   }
    
}
