﻿using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.error.support.observable
{
    
        public class CustomErrorEventArgs : EventArgs
        {

            private int errorCode;
            private string errorText;
            private string throwError;



            public CustomErrorEventArgs(int errorCode , string errorText , string throwError)
            {
                this.errorCode = errorCode;
                this.errorText = errorText;
                this.throwError = throwError;

            }

            public int getErrorCode
            {
                get { return errorCode; }
            }

            public string getErrorText
            {
                get { return errorText; }
            }
            public string getThrowError
            {
                get { return throwError; }
            }

    }
    
}
