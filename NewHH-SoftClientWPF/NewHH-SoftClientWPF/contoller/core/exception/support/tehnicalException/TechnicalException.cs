﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.exception.support.tehnicalException
{
   public class TechnicalException : Exception
    {
        
            private BaseErrorCode errorCode;
            private String description;
            private String errorThrow;

            public TechnicalException() { }
            protected TechnicalException(SerializationInfo info, StreamingContext context) : base(info, context) { }
            public TechnicalException(string message) : base(message) { }

            public TechnicalException(int errorCode, String message, String errorThrow) : base(message)
            {
                this.errorCode = new BaseErrorCode(errorCode);
                this.errorThrow = errorThrow;
                this.description = message;
            }

            public BaseErrorCode getErrorCode()
            {
                return errorCode;
            }
        
    }
}
