﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace NewHH_SoftClientWPF.contoller.core.exception.support.networkException
{
    public class NetworkException : Exception
    {
        private BaseErrorCode errorCode;
        private String description;
        private String errorThrow;

        public NetworkException() { }
        protected NetworkException(SerializationInfo info, StreamingContext context) : base(info, context) { }
        public NetworkException(string message) : base(message) { }

        public NetworkException(int errorCode, String message, String errorThrow) : base(message)
        {
            this.errorCode = new BaseErrorCode(errorCode);
            this.errorThrow = errorThrow;
            this.description = message;
        }

        public BaseErrorCode getErrorCode()
        {
            return errorCode;
        }
    }
}
