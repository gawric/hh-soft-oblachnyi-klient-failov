﻿
using NewHH_SoftClientWPF.contoller.core.exception.support.basesException;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.error.support.observable;
using NewHH_SoftClientWPF.contoller.update;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.controller.error
{
    //обработчик ошибок
    public class ExceptionController
    {
        private ExceptionHandler _errorHandler;
        private ConvertIconType _convertIcon;
        private UpdateNetworkJsonController _sendJson;
        private ErrorAlertWindowViewModel _viewModelError;


        public ExceptionController(Container cont)
        {
            if(cont != null)
            {
                _convertIcon = cont.GetInstance<ConvertIconType>();
                _sendJson = cont.GetInstance<UpdateNetworkJsonController>();
                _viewModelError = cont.GetInstance<ErrorAlertWindowViewModel>();
                registerObservable();
            }
           
        }

        public void registerObservable()
        {
            ErrorAlertModelObj errorAlertWindowsModel = createErrorAlertModel();
            BitmapSource map =  getMap();
            setImageAttention(map);
            getErrorHandler();
            new Observer(_errorHandler, errorAlertWindowsModel , _viewModelError);
        }
        public void sendError(int errorCode, string errorText, string throwError)
        {
            if(_errorHandler != null) _errorHandler.updateError(errorCode, errorText, throwError);
        }

        private ErrorAlertModelObj createErrorAlertModel()
        {
            ErrorAlertModelObj errorAlertModelObj = new ErrorAlertModelObj();
            errorAlertModelObj._convertIcon = _convertIcon;
            errorAlertModelObj._sendJson = _sendJson;
      
            errorAlertModelObj._dispatcherThread = Thread.CurrentThread;
            return errorAlertModelObj;
        }

        private void setImageAttention(BitmapSource map)
        {
            _viewModelError.ImageAttention = map;
        }
        private void getErrorHandler()
        {
            _errorHandler = new ExceptionHandler("ErrorSub");
        }

        private BitmapSource getMap()
        {
            BitmapSource map = _convertIcon.getIconType(".attention");
            map.Freeze();

            return map;
        }
    }
}
