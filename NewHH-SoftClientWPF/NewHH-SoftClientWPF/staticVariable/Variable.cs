﻿using NewHH_SoftClientWPF.mvvm.model;
using Syroot.Windows.IO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.staticVariable
{
    public static class Variable
    {
        private static readonly object balanceLock = new object();

        public static readonly int minThreadPool = 20;
        private static long id_databases = 0;
        private static string currentFileDir;
        private static string currentBATDir;
        public static string dirRootCatalog { get; set; }
        private static string currentJsonDir;
        private static string domenWebDavserver = "https://hh-soft.ru";
        // static string domenWebDavserver = "https://localhost";
        private static string domenMQserver = "ssl://hh-soft.ru:";
        private static string existsHostName = "www.hh-soft.ru";
        private static int portwebdavserver = 9091;
        //private static int portwebdavserver = 8080;
        private static int portmqserver = 61714;
        public static bool LoginAlarm = false;
        private static string username { get; set; }
        private static string password;
      
        private static string sendTopic = "topic://VirtualTopic.Commit_Changes_Users";
        public static string  sendTopicLazy = "topic://VirtualTopic.Lazy_Changes_Users";
        private static string modelSendTopic = "com.WebDavSpring.WebDavSpring.model.FileInfoObjectTopic";
        public static string nameRootDisk = "Disk";
        public static string nameRootLocationDisk = "disk";
        private static long user_id;
        public static string hrefAutoUpdaterWebDav = "https://localhost:9091/adminwebdav/admin/public/update.xml";
        //public static string hrefAutoUpdaterWebDav = "https://hh-soft.ru:9091/adminwebdav/admin/public/update.xml";

        //id - для поиска CanselationToken
        public static string reponceNoDeleteBasesCanselationTokenID = "ResponceServerScheduleNoDeleteBases";
        public static string uploadCancelationTokenId = "UploadCancelationTokenId";
        public static string downloadCancelationTokenId = "DownloadCancelationTokenId";
        public static string WatchFileSystemCancelationTokenId = "WatchFileSystemCancelationTokenId";


        public static string blockingUploadTokenId = "BlockingUploadTokenId";
        public static string blockingDownloadTokenId = "BlockingDownloadTokenId";
        public static string autoSyncClearBasesCancellationTokenId = "AutoSyncClearBasesCancellationTokenId";
        public static string scannerConnectServerControllerTokenId = "ScannerConnectServerControllerTokenId";
        public static string listenerServerTokenId = "ListenerServerTokenId";
        //Сколько файлов и папок было просканировано автосканером
        public static int allCountAutoScanInfo = 0;
        public static void setWebDavNameSpace(string username)
        {
            webdavnamespace = webdavrootnamespace + username + "/";
            Console.WriteLine("Перезаписано webDavNameSpace: "+ webdavnamespace);
        }

        public static string runningAutoSyncBlockingId = "RunningAutoSync";
        //id - для поиска в BlockingEvent
        public static string autoSyncToTimeBlockingId = "AutoSyncToTime";

        //используется при сканировании без очистки всей базы данных
        //это тестовое имя дальше я думаю определять где содержатся файлы в webdav через имя пользователя
        //обязательно вконце нужно ставить /
        public static string webdavnamespace;
        public static string webdavrootnamespace;
        //нельзя одновременно запускать несколько рекурсий
        public static bool isRecursive { get; set; }

 
        public static int CountFilesTrasferServer = 1000;
        //количество файлов когда достигнет данного предела, все будет выгружено в базу(Используется для ускорения сохронения длинных задач)
        public static int countLazyParties = 4000;

        //connectionId присваивается сервером получаем из клиента
        //я создаю несколько подключений к серверу
        public static Dictionary<string, bool> activemqConnectionId;
        //публицист клиент или просто слушатель
        public static bool activemqIsPublisher = false;
        //сервер занят или нет(обновляется вместе с Publisher запросом)
        public static bool activemqIsWorking = false;
        //перенес в базу хренине данных
        public static int startMillisecondAutoSync;
        public static string defaultPathDownload = KnownFolders.Downloads.Path;
        public static string defaultPathNameDownload = "hh_disk";
        public static int refreshIconId = 3;
        public static int okIconId = 2;
        public static int normalIconId = 1;

        public static void setIdDataBases(long row_id)
        {
            lock (balanceLock)
            {
                id_databases = row_id;
            }
            
        }

        public static long getIdDatabses()
        {
            return id_databases;
        }

        public static bool isFolder(string folder)
        {
            bool check = false;
            if(folder != null) if (folder.LastIndexOf("folder") != -1) check = true;
           
            return check;
        }

        public static bool isTypeCloud(string folder)
        {
            bool check = false;

            if (folder.LastIndexOf("ok") != -1 | folder.LastIndexOf("refresh") != -1) check = true;
            return check;
        }

        public static bool isTypeRefresh(string folder)
        {
            bool check = false;

            if (folder.LastIndexOf("refresh") != -1) check = true;
            return check;
        }



        //самый правильный момент кодировки
        //остальные кодирует любо с большой буквы либо за место пробелов ставят +
        public static string EscapeUrlString(string copyFolderName)
        {
            
            string escape = System.Web.HttpUtility.UrlPathEncode(copyFolderName);
            string escapesharp = escape.Replace("#", "%23");
            return escapesharp.Replace("*", "%2A");
        }

        
        //самый правильный момент кодировки
        //остальные кодирует любо с большой буквы либо за место пробелов ставят +
        public static string DecodeUrlString(string copyFolderName)
        {
            string copyFolderNameEscapeSpace = copyFolderName.Replace("+", "%2B");
            return System.Web.HttpUtility.UrlDecode(copyFolderNameEscapeSpace);
        }

   
        public static long getUser_id()
        {
            return user_id;
        }
        public static void setUser_id(long user_id_server)
        {
            user_id = user_id_server;
        }

        public static string getRootDiskTree()
        {
            return nameRootDisk;
        }

        public static string getRootLocationDisk()
        {
            return nameRootLocationDisk;
        }



        public static string getSendTopic()
        {
            return sendTopic;
        }

        public static string getModelSendTopic()
        {
            return modelSendTopic;
        }


        

       

        
        public static string getHostExist()
        {
            return existsHostName;
        }
        public static string getUsername()
        {
            return username;
        }
        public static string getPassword()
        {
            return password;
        }
        public static int getPortMqServer()
        {
            return portmqserver;
        }
        public static int getPortWebDavServer()
        {
            return portwebdavserver;
        }
        public static string getDomenWebDavServer()
        {
            return domenWebDavserver;
        }
        public static string getDomenMqServer()
        {
            return domenMQserver;
        }

        public static string getRootDirProgramm()
        {
            
                string firstFileFir = System.AppDomain.CurrentDomain.BaseDirectory;

                int end = firstFileFir.Length;
                string endstr = firstFileFir.Substring(end - 1);

                //не всегда возвращает в конце \\ 
                //поэтому мы проверяем
                if (endstr.Equals("\\"))
                {
                    return System.AppDomain.CurrentDomain.BaseDirectory;
                }
                else
                {
                    return System.AppDomain.CurrentDomain.BaseDirectory + "\\";
                }

                
        }

        //текукаталог
        public static string getBasesDir()
        {
            if (string.IsNullOrEmpty(currentFileDir))
            {
                

                string firstFileFir = System.AppDomain.CurrentDomain.BaseDirectory;

                int end = firstFileFir.Length;
                string endstr = firstFileFir.Substring(end - 1);

                //не всегда возвращает в конце \\ 
                //поэтому мы проверяем
                if(endstr.Equals("\\"))
                {
                    dirRootCatalog = System.AppDomain.CurrentDomain.BaseDirectory;
                   

                    currentFileDir = System.AppDomain.CurrentDomain.BaseDirectory  +"sqlite.db3";
                }
                else
                {
                    dirRootCatalog = System.AppDomain.CurrentDomain.BaseDirectory;
                    

                    currentFileDir = System.AppDomain.CurrentDomain.BaseDirectory +"\\"+ "sqlite.db3";
                }
             

                //где храниться бд
                return currentFileDir;
                
            }
            else
            {
                return currentFileDir;
            }

        }

        public static string getBATfolder()
        {
            if (string.IsNullOrEmpty(currentBATDir))
            {


                string firstFileFir = System.AppDomain.CurrentDomain.BaseDirectory;

                int end = firstFileFir.Length;
                string endstr = firstFileFir.Substring(end - 1);

                //не всегда возвращает в конце \\ 
                //поэтому мы проверяем
                if (endstr.Equals("\\"))
                {
                    dirRootCatalog = System.AppDomain.CurrentDomain.BaseDirectory;


                    currentBATDir = System.AppDomain.CurrentDomain.BaseDirectory + "scripts"+"\\";
                }
                else
                {
                    dirRootCatalog = System.AppDomain.CurrentDomain.BaseDirectory;


                    currentBATDir = System.AppDomain.CurrentDomain.BaseDirectory + "\\" + "scripts"+"\\";
                }


                //где храниться бд
                return currentBATDir;

            }
            else
            {
                return currentBATDir;
            }

        }



        public static string getBasesTempJson()
        {
            if (string.IsNullOrEmpty(currentJsonDir))
            {
                //где храниться бд
                return currentJsonDir = System.AppDomain.CurrentDomain.BaseDirectory + "temp.json";
            }
            else
            {
                return currentJsonDir;
            }

        }

       

        static long current_row = 0;
        static long[] temp_row = { 0 };

        //попытка оптимизировать синхронизацию, что-бы не проходить каждый раз новый массив
        //мы запоминаем последний id номер и дальше его увеличиваем на 1 еденицу вверх
        //нужно очень аккуратно отнестиcь к данной правке!
        public static long generatedNewRowId(long id_databases, FileInfoModel[] containerAllFiles)
        {
            setIdDataBases(id_databases);

           // Console.WriteLine("Входящая  id_databases: " + id_databases);
           // Console.WriteLine("+++++++++++++++++++++++++++++++");

            if (current_row >= id_databases)
            {
               // Console.WriteLine("StaticVatiable->Variable->generatedNewRowId currentrow + 1 "+ current_row );
                temp_row[0] = current_row + 1;

                current_row = temp_row[0];
                id_databases = temp_row[0];
            }
            else
            {
                if (containerAllFiles != null)
                {
                    //ищем в массиве вновь созданных нодов если там кто-то с id номером больше чем в базе данных
                    for (int d = 0; d < containerAllFiles.Length; d++)
                    {
                        FileInfoModel model = containerAllFiles[d];

                        if (model != null)
                        {
                            if (model.row_id >= id_databases)
                            {

                                id_databases = model.row_id + 1;
                                current_row = model.row_id + 1;
                                //Console.WriteLine("Новый генерируемый ID  " + id_databases);
                            }
                        }

                    }

                }
                else
                {
                    current_row = id_databases + 1;
                    id_databases = id_databases + 1;
                }

            }

           // Console.WriteLine("Новая Current: "+current_row);
           // Console.WriteLine("Новая id_databases: " + id_databases);
           // Console.WriteLine("+++++++++++++++++++++++++++++++");

            setIdDataBases(id_databases);
            //Console.WriteLine("StativVatiable->Variable->generatedNewRowId Итоговый ID id_databases " + id_databases);
          
            return staticVariable.Variable.getIdDatabses();

        }

        public static int s = 0;

    }
}
