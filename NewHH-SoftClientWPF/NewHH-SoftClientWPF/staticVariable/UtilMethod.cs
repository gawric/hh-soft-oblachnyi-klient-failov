﻿using NewHH_SoftClientWPF.mvvm.model;
using NewHH_SoftClientWPF.mvvm.model.autoSaveFolder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NewHH_SoftClientWPF.staticVariable
{
    public static class UtilMethod
    {

        public static string GetMachineName()
        {
            return "MyPc" + Environment.MachineName;
        }
        //конвертор байтов
        public static string FormatBytes(long bytes)
        {
            string[] Suffix = { "Б", "КБ", "МБ", "ГБ", "ТБ" };
            int i;
            double dblSByte = bytes;
            for (i = 0; i < Suffix.Length && bytes >= 1024; i++, bytes /= 1024)
            {
                dblSByte = bytes / 1024.0;
            }

            return String.Format("{0:0.##} {1}", dblSByte, Suffix[i]);
        }
        //в будущем под сном это нужно для теста
        public static Dictionary<string, HrefModel> dict { get; set; }
        public static bool fsrun {get;set;}

        public static void WriteLog(string writePath, string info)
        {
            using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
            {
                sw.WriteLine(info);
            }
        }
        //конвертор байтов
        public static double FormatBytesNoSuffix(long bytes)
        {
                return bytes / 1024.0 / 1024.0 / 1024.0;
        }

        public static class BitmapConversion
        {
            public static BitmapImage BitmapToBitmapSource(Uri uriSource)
            {
                // return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                //  source.GetHbitmap(),
                //  IntPtr.Zero,
                //  Int32Rect.Empty,
                //  BitmapSizeOptions.FromEmptyOptions());

                BitmapImage source2 = new BitmapImage(uriSource);
                using (Stream stream = source2.StreamSource)
                {
                    try
                    {

                        source2.StreamSource = stream;
                        source2.CacheOption = BitmapCacheOption.None;    // not a mistake - see below

                    }
                    catch (System.InvalidOperationException a)
                    {
                        Console.WriteLine("Variable->BitmapToBitmapSource " + a);
                    }

                }

                return source2;
            }
        }

        public static UIElement GetByUid(DependencyObject rootElement, string uid)
        {
            foreach (UIElement element in LogicalTreeHelper.GetChildren(rootElement).OfType<UIElement>())
            {
                if (element.Uid == uid)
                    return element;
                UIElement resultChildren = GetByUid(element, uid);
                if (resultChildren != null)
                    return resultChildren;
            }
            return null;
        }

        public static bool isDisk(ref string copyOrCutRootLocation)
        {
            bool check = false;
            if (copyOrCutRootLocation.Equals(Variable.nameRootDisk) || copyOrCutRootLocation.Equals(Variable.nameRootLocationDisk)) check = true;

            return check;

        }

        public static bool isRootId(long row_id)
        {
            bool check = false;
            if (row_id.Equals(-1)) check = true;

            return check;
        }

        public static FileInfoModel getRootFolderNodes()
        {
            FileInfoModel fol = new FileInfoModel();
            fol.row_id = -1;
            fol.filename = "Disk";
            string empty = "";
            fol.location = getRootLocation(true, ref empty);
            fol.parent = -2;
            fol.type = "folder";

            return fol;
        }
        public static string getRootLocation(bool isDisk, ref string copyOrCutRootLocation)
        {

            if (isDisk)
            {
                copyOrCutRootLocation = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;
            }

            return copyOrCutRootLocation;
        }

        public static string getRootLocationAutoSaveFolder()
        {
           string machinename = GetMachineName();
           string empty = "";
           return Variable.EscapeUrlString(staticVariable.UtilMethod.getRootLocation(true, ref empty) + machinename);
        }

        public static string convertLocationToHref(string parentLocation, string location)
        {
            if (parentLocation != null)
            {
                string remoteWebDav = staticVariable.Variable.getDomenWebDavServer() + ":" + staticVariable.Variable.getPortWebDavServer() + "/" + staticVariable.Variable.webdavnamespace;


                string hrefLocal = parentLocation.Replace(remoteWebDav, location + "\\");
                hrefLocal = hrefLocal.Replace("/", "\\");

                string hrefLocalDecode = staticVariable.Variable.DecodeUrlString(hrefLocal);


                return hrefLocalDecode;
            }
            else
            {
                Console.WriteLine("ExistSavePcEvent->convertLocationToHref: parentLocation не определен!!!" + parentLocation);
                return "";
            }

        }

        public static  string ConvertHrefToLocationFolder(string clientLocalDirectory, string clientRootLocalDirectory, string pasteRootWebDavDirectory)
        {

            if (clientRootLocalDirectory.Equals("")) throw new ArgumentException("SubscribleData-> ConvertHrefToLocationFolder: Критическая ошибка нет аргумента");

            //полный путь к папке изменяем все на unix систему
            string clearLocalHref = clientLocalDirectory.Replace("\\", "/");

            //папка откуда все копируется
            string clearRootHref = clientRootLocalDirectory.Replace("\\", "/");

            //находим главную папку где нажали кнопку копировать и меняем на рутовый путь сервера
            string newhref = clearLocalHref.Replace(clearRootHref, pasteRootWebDavDirectory + "/");


            return staticVariable.Variable.EscapeUrlString(newhref);

        }

        public static string ConvertHrefToLocation(string clientLocalDirectory, string clientRootLocalDirectory, string pasteRootWebDavDirectory)
        {
            //полный путь к папке изменяем все на unix систему
            string clearLocalHref = clientLocalDirectory.Replace("\\", "/");

            //папка откуда все копируется
            string clearRootHref = clientRootLocalDirectory.Replace("\\", "/");

            //находим главную папку где нажали кнопку копировать и меняем на рутовый путь сервера
            string newhref = clearLocalHref.Replace(clearRootHref, pasteRootWebDavDirectory);


            return staticVariable.Variable.EscapeUrlString(newhref);
        }

        public static string GetParentLocation(string location)
        {
            return null;
        }

        public static HrefModel GetFileInfoParent(string path)
        {

            if (File.Exists(path))
            {
                FileInfo fi1 = new FileInfo(path);
                HrefModel fileModel = new HrefModel();

                fileModel.changeDate = fi1.LastWriteTimeUtc.ToString();
                fileModel.lastOpenDate = fi1.LastAccessTime.ToString();
                fileModel.filename = fi1.Name;
                fileModel.href = fi1.FullName;
                fileModel.createDate = fi1.CreationTime.ToString();
                fileModel.parentHref = fi1.Directory.FullName;
                fileModel.pasteHref = fi1.Directory.FullName;
                fileModel.type = fi1.Extension;
                fileModel.size = fi1.Length;

                return fileModel;
            }
            else
            {
                if (Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    HrefModel folderModel = new HrefModel();

                    folderModel.changeDate = di.LastWriteTime.ToString();
                    folderModel.lastOpenDate = di.LastAccessTime.ToString();
                    folderModel.filename = di.Name;
                    folderModel.href = di.FullName + "\\";
                    folderModel.createDate = di.CreationTime.ToString();
                    folderModel.parentHref = di.Parent.FullName;
                    folderModel.pasteHref = di.Parent.FullName;
                    folderModel.type = "folder";

                    return folderModel;
                }

            }

            throw new InvalidOperationException("SubscribeData->getFimSql: Критическая ошибка не нашли на копьютере папку с намими файлами!!!");

        }

        public static HrefModel GetFileInfo(string path)
        {
            if (File.Exists(path))
            {
                FileInfo fi1 = new FileInfo(path);
                HrefModel fileModel = new HrefModel();

                fileModel.changeDate = fi1.LastWriteTimeUtc.ToString();
                fileModel.lastOpenDate = fi1.LastAccessTime.ToString();
                fileModel.filename = fi1.Name;
                fileModel.href = fi1.FullName;
                fileModel.createDate = fi1.CreationTime.ToString();
                fileModel.parentHref = fi1.Directory.FullName;
                fileModel.pasteHref = fi1.Directory.FullName;
                fileModel.type = fi1.Extension;
                fileModel.size = fi1.Length;

                return fileModel;
            }
            else
            {
                if (Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    HrefModel folderModel = new HrefModel();

                    folderModel.changeDate = di.LastWriteTime.ToString();
                    folderModel.lastOpenDate = di.LastAccessTime.ToString();
                    folderModel.filename = di.Name;
                    folderModel.href = di.FullName;
                    folderModel.createDate = di.CreationTime.ToString();
                    folderModel.parentHref = di.Parent.FullName;
                    folderModel.pasteHref = di.Parent.FullName;
                    folderModel.type = "folder";

                    return folderModel;
                }

            }

            return GetEmptyModel(path);
        }

        private static HrefModel GetEmptyModel(string href)
        {
            HrefModel hrefModel = new HrefModel();
            hrefModel.href = href;
            hrefModel.filename = "";

            return hrefModel;
        }


    }
}
