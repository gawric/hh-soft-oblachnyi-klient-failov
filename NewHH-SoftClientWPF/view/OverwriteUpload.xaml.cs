﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF
{
    /// <summary>
    /// Логика взаимодействия для OverwriteUpload.xaml
    /// </summary>
    public partial class OverwriteUpload : Window
    {
        private bool[] check;

        public OverwriteUpload(bool[] check)
        {
            InitializeComponent();
            this.check = check;
        }


        private void Button_Click_OK(object sender, RoutedEventArgs e)
        {
            this.check[0] = true;
            SystemCommands.CloseWindow(this);
        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            this.check[0] = false;
            SystemCommands.CloseWindow(this);
        }
    }
}
