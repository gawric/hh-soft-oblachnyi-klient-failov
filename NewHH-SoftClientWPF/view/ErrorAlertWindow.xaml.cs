﻿using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.errorViewModel;
using NewHH_SoftClientWPF.mvvm.supportForm.errorAlertWindowForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace NewHH_SoftClientWPF.view
{
    /// <summary>
    /// Логика взаимодействия для ErrorAlertWindow.xaml
    /// </summary>
    public partial class ErrorAlertWindow : Window
    {
        private InitializingErrorForm _initializingErrorForm;
        private ErrorAlertModelObj _errorAlertModelObj;
        private SupportEventPartErrorAlertForm _sepeaf;
        private ErrorAlertWindowViewModel _viewModel;

        public ErrorAlertWindow(ErrorAlertModelObj errorAlertModelObj , ErrorAlertWindowViewModel viewModel)
        {
            createObeject(errorAlertModelObj , viewModel);
            DataContext = viewModel;
            createView();
            InitializeComponent();
        }


        private void Button_Click_Send(object sender, RoutedEventArgs e)
        {
            
            

            try
            {
                //нужен дебаг!!!
                _sepeaf.buttonSendError(sender, e);
                _errorAlertModelObj._errorAlertWindow.Close();
           
            }
            catch (System.Threading.Tasks.TaskCanceledException z)
            {
                Console.WriteLine("ErrorAlertWindow->Button_ClickCancel " + z.ToString());
            }
            catch (System.InvalidOperationException z)
            {
                Console.WriteLine("ErrorAlertWindow->Button_ClickCancel " + z.ToString());
            }


        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Threading.Tasks.TaskCanceledException z)
            {
                Console.WriteLine("ErrorAlertWindow->Button_ClickCancel "+z.ToString());
            }
            catch(System.Exception d)
            {
                Console.WriteLine("ErrorAlertWindow->Button_ClickCancel " + d.ToString());
            }
           
        }

       

        private void createObeject(ErrorAlertModelObj errorAlertModelObj , ErrorAlertWindowViewModel viewModel)
        {
            _errorAlertModelObj = errorAlertModelObj;
            _errorAlertModelObj._errorAlertWindow = this;
            _sepeaf = new SupportEventPartErrorAlertForm(_errorAlertModelObj , viewModel);
            _viewModel = viewModel;
        }

        
        private void createView()
        {
            
                _initializingErrorForm = new InitializingErrorForm(_errorAlertModelObj , _viewModel);
                this.Loaded += _initializingErrorForm.ErrorAlertWindow_load;
            
              
        }
    }
}
