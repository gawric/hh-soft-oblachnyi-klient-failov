﻿using NewHH_SoftClientWPF.contoller.blockThread;
using NewHH_SoftClientWPF.contoller.cancellation;
using NewHH_SoftClientWPF.contoller.createmodel.convert;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer;
using NewHH_SoftClientWPF.contoller.operationContextMenu.viewTransfer.support;
using NewHH_SoftClientWPF.contoller.sorted.comparer;
using NewHH_SoftClientWPF.contoller.sqlite;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.listtransfermodel;
using NewHH_SoftClientWPF.mvvm.model.treemodel;
using NewHH_SoftClientWPF.mvvm.supportForm.viewTransfer;
using SimpleInjector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF
{
    /// <summary>
    /// Логика взаимодействия для ViewTransfer.xaml
    /// </summary>
    public partial class ViewTransfer : Page
    {
        private ViewTransferViewModel _viewModel;
        private SqliteController _sqllite;
        private ConvertIconType _convertIcon;
        private ViewTransferContextMenu _viewContextMenu;
        private BlockingEventController _blockingController;
        private CancellationController _cancellationController;
        private SupportViewTransfer _supportViewTransfer;
        private SupportEventPart1ViewTransfer _supportEventPart1ViewTransfer;


        public ViewTransfer(Container cont)
        {
            createObject(cont);
            DataContext = _viewModel;
            InitializeComponent();
            createInitializing();
               
        }

       
        //DataContext для TreeView
        public ListTransferViewModel ListModel
        {
            get
            {
                return _viewModel.ListView;
            }
        }
        
        
        //реализация отмены выделенной upload
        private void StopUploadContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewTransfer.StopUploadContextMenu_Click( sender,  e);
        }

        //реализация оставноки выделенной upload
        private void PauseUploadContextMenu_Click(object sender, RoutedEventArgs e)
        {

            _supportEventPart1ViewTransfer.PauseUploadContextMenu_Click( sender,  e);

        }
        

        //реализация удаления 1-ого нода
        private void DeleteSinglContextMenu_Click(object sender, RoutedEventArgs e)
        {
            _supportEventPart1ViewTransfer.DeleteSinglContextMenu_Click( sender,  e);
        }

        //реализация удаления всех нодов
        private void DeleteAllContextMenu_Click(object sender, RoutedEventArgs e)
        {

            _supportEventPart1ViewTransfer.DeleteAllContextMenu_Click(sender, e);

        }
        
        private void openContextMenu(object sender, ContextMenuEventArgs e)
        {

            _supportEventPart1ViewTransfer.OpenContextMenu( sender,  e);
        }

      
        private void createObject(Container cont)
        {
            _viewModel = cont.GetInstance<ViewTransferViewModel>();
            _viewContextMenu = cont.GetInstance<ViewTransferContextMenu>();
            _blockingController = cont.GetInstance<BlockingEventController>();

            _sqllite = cont.GetInstance<SqliteController>();
            _viewModel.Titlename = "Статус копирования";
            _viewModel.ListView = new ListTransferViewModel();
            _convertIcon = cont.GetInstance<ConvertIconType>();
            _cancellationController = cont.GetInstance<CancellationController>();
            _supportEventPart1ViewTransfer = cont.GetInstance<SupportEventPart1ViewTransfer>();
            _supportViewTransfer = new SupportViewTransfer();
        }

        private void createInitializing()
        {
            InitializingViewTransfer _initTransfer = new InitializingViewTransfer(this);
            _initTransfer.InitializingContextMenu(_viewContextMenu, _convertIcon);
            _supportEventPart1ViewTransfer.injectElemetns(this , _supportViewTransfer);
        }
    }
}
