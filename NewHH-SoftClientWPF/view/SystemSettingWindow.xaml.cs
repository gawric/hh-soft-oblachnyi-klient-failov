﻿using NewHH_SoftClientWPF.contoller.core.network.webdavClient.support.webdaupload;
using NewHH_SoftClientWPF.mvvm;
using NewHH_SoftClientWPF.mvvm.model.settingModel;
using NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm;
using NewHH_SoftClientWPF.mvvm.supportForm.systemSettingWindowForm.eventSettingForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NewHH_SoftClientWPF.view
{
    /// <summary>
    /// Логика взаимодействия для SystemSettingWindow.xaml
    /// </summary>
    public partial class SystemSettingWindow : Window
    {
        private SupportEventPart1SettingForm _sep1sf;
        private SystemSettingModelObj _ssmo;
        private InitializingSettingForm _initializingSettingForm;

        public SystemSettingWindow(SystemSettingModelObj ssmo)
        {
            _ssmo = ssmo;
            createObeject();
            createView();
            DataContext = _ssmo._swvm;
            InitializeComponent();
            listpath.ItemsSource = _ssmo._swvm.obsScanFol;
        }

        private void ButtonChangeDisk_Click(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickChangeDiskEvent(sender, e);
        }

        private void CheckBoxAutoStart_Click(object sender, RoutedEventArgs e)
        {
            _sep1sf.CheckBoxAutoStart(sender, e);
        }

        private void ButtonDeleteScanFolder(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickDeleteFolderScan(listpath, e);
        }

        private void ButtonAddScanFolder(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickAddFolderScan(sender, e);
        }


        private void ButtonMountDisk_Click(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickMountDiskEvent(sender, e);
        }

        private void ButtonUnmountDisk_Click(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickUnmountDiskEvent(sender, e);
        }

        private void Button_Click_Change_TimeAutoScan(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickChangeTimeAutScan(sender, e);
        }

        private void Button_Click_Ok(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickOkEvent(sender, e);
            this.Close();
        }

        private void Button_Click_Cancel(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickCancelEvent(sender, e);
            this.Close();
        }

        private void Button_Click_Commit(object sender, RoutedEventArgs e)
        {
            _sep1sf.ClickCommitEvent(sender, e);
        }

        private void CheckBoxScanFolder(object sender, RoutedEventArgs e)
        {
            _sep1sf.CheckBoxScanFolder(sender, e);
        }

        private void createObeject()
        {
            _ssmo._swvm = new SettingWindowViewModel();
            _sep1sf = new SupportEventPart1SettingForm(_ssmo);
            
        }
        private void createView()
        {
            _initializingSettingForm = new InitializingSettingForm(_ssmo._swvm, _ssmo);
            this.Loaded += _initializingSettingForm.SettingWindow_load;
        }

    }
}
